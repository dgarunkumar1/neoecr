var Accordion = function (el, multiple) {
    this.el = el || {};
    this.multiple = multiple || false;

    // Variables privadas
    var links = this.el.find('.link');
    links.on('click', { el: this.el, multiple: this.multiple }, this.dropdown)
    $('.active-link .link span').css('color', '#FFFFFF')


    $(".deactive-link").click(function () {
        $('.active-link').removeAttr('class');
    });

    $(".active-link").click(function (e) {
        $('.active-link').removeAttr('class');
    });

    $(".grid-section").click(function (e) {
        e.stopPropagation();
    });
}
$(".btn-secondary").click(function () {
    $(".grid-section").attr('onclick', ' ').unbind('click');
    $(".active-link").attr('onclick', '').unbind('click');
});

Accordion.prototype.dropdown = function (e) {
    var $el = e.data.el;
    $this = $(this),
		$next = $this.next();
    $prev = $this.prev();

    $next.slideToggle();
    $this.parent().toggleClass('open');
    if (!e.data.multiple) {
        $el.find('.grid-section').not($next).slideUp().parent().removeClass('open');

        $('.active-link .link span').css('color', '#292b2c')

    };
}
var accordion = new Accordion($('#accordion'), false);



