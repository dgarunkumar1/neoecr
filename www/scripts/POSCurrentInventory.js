﻿function VD() {
    var vd_ = window.localStorage.getItem("UUID");
    //var vdSplit_ = window.location.pathname.split('/');
    //if (vdSplit_.length > 2) {
    //    vd_ = vdSplit_[1];
    //}
    return vd_;
}
var graphData;
function floorFigure(figure, decimals) {
    if (!decimals) decimals = 2;
    var d = Math.pow(10, decimals);
    return (parseInt(figure * d) / d).toFixed(decimals);
};
function arrangeCurrentInventoryData(resp) {

    var result = resp;
    var Primaryid = "";
    //$('#tbldiv').append("<thead><tr><th style='width:10px' ></th><th>Return No.</th><th>Return Date</th><th>Ref. Invoice No.</th><th>Ref. Invoice Date</th><th>Till</th><th>Base Total</th><th>Tax Total</th><th>Net Total</th></tr></thead>");
    $('#tbldiv').append("<thead><tr><th style='width:10px' ></th><th>" + $('#hdnItem').val() + "</th><th>" + $('#hdnUOM').val() + "</th><th>" + $('#hdnQty').val() + "</th><th>" + $('#hdnPrice').val() + "</th><th>" + $('#hdnAmount').val() + "</th><th style='display:none'>" + $('#hdnItemId').val() + "</th><th style='display:none'>" + $('#hdnUomId').val() + "</th></tr></thead>");
    $('#tbldiv').append("<tbody>");
    var cc_ = '';
    for (var i = 0; i < result.length; i++) {

        //var val_ = parseFloat(parseFloat(result[i].C9) - parseFloat(result[i].C11)).toFixed(2);
        cc_ = cc_ + '<tr id="dt_Parrent' + i + '" data-tt-parent="Paging"><td><div class="tt" data-tt-id="root' + i + '" data-tt-parent=""><div class="content" style=display:none id="root_' + i + '">+</div></div></td><td>' + result[i].C1 + '</td><td>' + result[i].C3 + '</td><td>' + result[i].C4 + '</td><td>' +parseFloat(result[i].C6).toFixed(4) + '</td><td>' +parseFloat(result[i].C5).toFixed(4) + '</td><td style="text-align:right;display:none">' + result[i].C0 + '</td><td style="text-align:right;display:none">' + result[i].C2 + '</td></tr>';
        cc_ = cc_ + '<tr data-tt-parent="childPaging"><td><div class="tt" data-tt-id="jacob" data-tt-parent="root' + i + '"></div></td><td colspan="9" ><table id="tblChild' + i + '" data-tt-parent="root' + i + '" class="table table-bordered" >';
        //cc_ = cc_ + "<tr><th>Item Code</th><th>Item Name</th><th>UOM</th><th>Qty</th><th>Price</th><th>Disc(%)</th><th>Base Total</th><th>Reason</th></tr>";
        //cc_ = cc_ + "<tr><th>" + $('#hdnItem').val() + "</th><th>" + $('#POSSearchTitleItemName').val() + "</th><th>" + $('#hdnUOM').val() + "</th><th>" + $('#hdnQty').val() + "</th><th>" + $('#hdnPrice').val() + "</th><th>" + $('#hdnDisc').val() + "</th><th>" + $('#hdnBaseTot').val() + "</th><th>" + $('#hdnReason').val() + "</th></tr>";
        //<th>Discount</th>


        //for (var j = 0; j < result[i].objdblclickParams.length; j++) {
        //    if (resp[i].C2 == result[i].objdblclickParams[j].ReturnId) {
        //        for (var k = 0; k < result[i].objdblclickParams[j].LISTD.length; k++) {


        //            cc_ = cc_ + '<tr><td>' + result[i].objdblclickParams[j].LISTD[k].ITEM_CODE + '</td><td>' + result[i].objdblclickParams[j].LISTD[k].ITEM_NAME + '</td><td>' + result[i].objdblclickParams[j].LISTD[k].UOM_CODE.split('/')[0] + '</td><td style="text-align:right">' + result[i].objdblclickParams[j].LISTD[k].RETURN_QTY + '</td><td style="text-align:right">' + result[i].objdblclickParams[j].LISTD[k].RETURN_ITEMS_PRICE + '</td><td style="text-align:right">' + result[i].objdblclickParams[j].LISTD[k].DiscountPer + '</td><td style="text-align:right">' + result[i].objdblclickParams[j].LISTD[k].Total_Amount + '</td><td>' + result[i].objdblclickParams[j].LISTD[k].Return_Reason + '</td></tr>';
        //            //<td>' + result[i].oblist[j].C4 + '</td>
        //        }
        //    }
        //}
        cc_ = cc_ + '</table></td></tr>';

    }
    cc_ = cc_ + "</tbody>";
    $("#tbldiv tbody").append(cc_);
    //$('#tbldiv').append("</tbody>");
    $('#tbldiv').treetable();
}

function CurrentInventory() {
    theamLoad();
    var btnExp = document.getElementById("btnExp");
    btnExp.style.display = "block";
    if (window.localStorage.getItem('RPPAGECODE') == 'POSCINV') {
        var SubOrgId = '';
        var HashVal = (window.localStorage.getItem("hashValues"));
        var HashValues;
        if (HashVal != null && HashVal != '') {
            HashValues = HashVal.split('~');
            getResources(HashValues[7], "POSCurrentInventoryReport");
            SubOrgId = HashValues[1];
            $('#hdnDateFromate').val(HashValues[2]);
            $('#hdnPaging').val(HashValues[19]);

        }



    }

}
function theamLoad() {
    if (window.localStorage.getItem("Theam") != null)
        $('head').append('<link rel="stylesheet" href=' + window.localStorage.getItem("Theam") + ' type="text/css" />');
}
function print() {
    var btnCol = document.getElementById("btnCol");
    btnCol.style.display = "block";

    var btnExp = document.getElementById("btnExp");
    btnExp.style.display = "none";
    $('#hdnScreenmode').val("print");
    $('#tbldiv').html("");
    GetUnpaging();
}
$('#btnCol').click(function () {

    var btnCol = document.getElementById("btnCol");
    btnCol.style.display = "none";

    var btnExp = document.getElementById("btnExp");
    btnExp.style.display = "block";


    var $el = $('#tbldiv tbody>tr');

    $el.addClass("tt-hide");

    for (var i = 0; i < $('#tbldiv tbody>tr').length; i++) {
        var tr = $('#tbldiv tbody>tr[id="dt_Parrent' + i + '"]');
        //var tr = $(table).find("tr[id='dt_Parrent" + i + "']");
        //var tdid = $($el[i]).find("td[id='dt_Parrent" + i + "']");
        $(tr).removeClass("tt-hide");
        $('#root_' + i + '').html("+");
    }

    $('#dt_Parrent1').removeClass("tt-hide");
    //GetPaging();

});
$('#btnExp').click(function () {


    var btnCol = document.getElementById("btnCol");
    btnCol.style.display = "block";

    var btnExp = document.getElementById("btnExp");
    btnExp.style.display = "none";

    $('#tbldiv').html("");
    $('#hdnScreenmode').val("");
    GetUnpaging();

});
function exportexcel() {
    var btnCol = document.getElementById("btnCol");
    btnCol.style.display = "block";

    var btnExp = document.getElementById("btnExp");
    btnExp.style.display = "none";
    $('#hdnScreenmode').val("excel");
    $('#tbldiv').html("");
    GetUnpaging();
    //$("#div_grid").table2excel({
    //    name: "Table2Excel",
    //    filename: "DailyCollectionsByTenderType",
    //    fileext: ".xls"
    //});


}
function loadGrid() {
    //   $('#div_graph1').css("display", "none");
    //$('#div_grid').css("display", "block");

    var elemGrid = document.getElementById("div_grid");
    elemGrid.style.display = "block";

    var elemGraph = document.getElementById("div_graph");
    elemGraph.style.display = "none";
}
function GetUnpaging() {
    var SubOrgId = '';
    var HashVal = (window.localStorage.getItem("hashValues"));
    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');
        SubOrgId = HashValues[1];
        $('#hdnDateFromate').val(HashValues[2]);
        $('#hdnPaging').val(HashValues[19]);
    }
    if (window.localStorage.getItem('RPfrmDate') != null) {
        $('#INVLISlblDate').text('(' + ConvertDateAndReturnDate($('#hdnDateFromate').val(), window.localStorage.getItem('RPfrmDate')) + ' to ' + ConvertDateAndReturnDate($('#hdnDateFromate').val(), window.localStorage.getItem('RPtoDate')) + ')');
    }
    var FromDate = (window.localStorage.getItem("RPfrmDate"));
    var ToDate = (window.localStorage.getItem("RPtoDate")) + ' 23:59:59';
    var RefNo = (window.localStorage.getItem("RPrefNo"));
    var PageCode = (window.localStorage.getItem("RPPAGECODE"));
    var grid = "";

    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetReport,
        data: "PageId=" + PageCode + "&subOrg=" + SubOrgId + "&frmDate=" + FromDate + "&toDate=" + ToDate + "&refNo=" + RefNo + "&DateFormate=" + $('#hdnDateFromate').val() + "&VD=" + VD(),
        success: function (resp) {
            arrangeCurrentInventoryData(resp)

            var $el = $('#tbldiv tbody>tr.tt-hide');
            $el.removeClass("tt-hide");

            for (var i = 0; i < $el.length; i++) {
                $('#root_' + i + '').html("-");
            }
            $('#DivPager').css("display", "none");
            if ($('#hdnScreenmode').val() == "print") {
                var divContents = document.getElementById("div_grid").innerHTML;
                var printWindow = window.open('', '', 'height=200,width=450');
                printWindow.document.write('<html><head><title>DailyCollectionsSummaryByTenderTypeReport</title>');
                printWindow.document.write('</head><body >');
                printWindow.document.write(divContents);
                printWindow.document.write('</body></html>');
                printWindow.document.close();
                printWindow.print();
            }
            if ($('#hdnScreenmode').val() == "excel") {
                var data_type = 'data:application/vnd.ms-excel';
                var table_div = document.getElementById('tbldiv');
                var table_html = table_div.outerHTML.replace(/ /g, '%20');
                var a = document.createElement('a');
                a.href = data_type + ', ' + table_html;
                a.download = 'Sales-Return' + '.xls';
                a.click();
            }
        }
    });

   
}
function GetPaging() {
    $('table#tbldiv').each(function () {

        var currentPage = 0;
        var numPerPage = $('#hdnPaging').val();
        var $table = $(this);
        var $tr = $table.find('tbody tr');

        $table.bind('repaginate', function () {
            $table.find('tbody tr[data-tt-parent="Paging"]').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
        });
        //  
        $table.trigger('repaginate');
        var numRows = $table.find('tbody tr[data-tt-parent="Paging"]').length;
        var numPages = Math.ceil(numRows / numPerPage);

        $("#DivPager").remove();

        var $pager = $('<div id="DivPager" class="pager"></div>');
        for (var page = 0; page < numPages; page++) {
            $('<span class="page-number"></span>').text(page + 1).bind('click', {
                newPage: page
            }, function (event) {
                currentPage = event.data['newPage'];
                $table.trigger('repaginate');
                $(this).addClass('active').siblings().removeClass('active');
            }).appendTo($pager).addClass('clickable');
        }
        $pager.insertAfter($table).find('span.page-number:first').addClass('active');

    });

}

function loadGraphData() {
    var param = { CounterId: window.localStorage.getItem('COUNTERID'), ShiftId: window.localStorage.getItem('SHIFTID'), SalespersonId: window.localStorage.getItem('CashierID') };
    var color = Chart.helpers.color;
    var barChartData = '';
    var elemGrid = document.getElementById("div_grid");
    elemGrid.style.display = "none";

    var elemGraph = document.getElementById("div_graph");
    elemGraph.style.display = "block";

    var resp = graphData;


    var x = []; var y = [];


    for (var i = 0; i < resp.length; i++) {
        x.push(resp[i].C3);
        y.push(parseFloat(resp[i].C12).toFixed(2));
    }

    barChartData = {
        labels: x,
        datasets: [{
            label: 'Sale',
            backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
            borderColor: window.chartColors.blue,
            borderWidth: 1,
            data: y
        },
          {
              type: 'line',
              label: 'Line View',
              borderColor: window.chartColors.red,
              borderWidth: 2,
              fill: false,
              data: y,

          }
        ]
    }

    var ctx = document.getElementById("canvas").getContext("2d");
    window.myBar = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            responsive: true,
            title: {
                display: true,
                text: ''
            },
        }
    });
}

function getResources(langCode, pageCode) {
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.getResource,
        data: "langCode=" + langCode + "&pageCode=" + pageCode,
        success: function (resp) {
            setLabels(resp);
        }
    });
}
function ConvertDateAndReturnDate(formates, dateText) {
    var hdnformat = formates;
    var txtMainDate = '';
    var splitc;
    if (hdnformat == 'd/m/Y' || hdnformat == 'm/d/Y' || hdnformat == 'Y/d/m' || hdnformat == 'Y/m/d' || hdnformat == 'M/d/Y' || hdnformat == 'd/M/Y') {
        splitc = '/';
    }
    else if (hdnformat == 'd-m-Y' || hdnformat == 'm-d-Y' || hdnformat == 'Y-d-m' || hdnformat == 'Y-m-d' || hdnformat == 'M-d-Y' || hdnformat == 'd-M-Y') {
        splitc = '-';
    }
    else if (hdnformat == 'd,m,Y' || hdnformat == 'm,d,Y' || hdnformat == 'Y,d,m' || hdnformat == 'Y,m,d' || hdnformat == 'M,d,Y' || hdnformat == 'd,M,Y') {

        splitc = ',';
    }
    else if (hdnformat == 'd m Y' || hdnformat == 'm d Y' || hdnformat == 'Y d m' || hdnformat == 'Y m d' || hdnformat == 'M d Y' || hdnformat == 'd M Y') {

        splitc = ' ';
    }
    else if (hdnformat == 'd.m.Y' || hdnformat == 'm.d.Y' || hdnformat == 'Y.d.m' || hdnformat == 'Y.m.d' || hdnformat == 'M.d.Y' || hdnformat == 'd.M.Y') {
        splitc = '.';
    };
    var sym = '-';
    if (dateText.indexOf('/') != -1)
        sym = '/';
    var vardatesplit = dateText.split(sym);
    var varM;
    var varMn;
    var varfformatS = hdnformat.split(splitc);
    if (varfformatS[0] == 'M' || varfformatS[1] == 'M') {

        if (vardatesplit[1] != null)
            varM = vardatesplit[1];
        if (vardatesplit[0] != null)
            varM = vardatesplit[0];

        switch (varM) {
            case 'Jan': varMn = '01';
                break;
            case 'Feb': varMn = '02';
                break;
            case 'Mar': varMn = '03';
                break;
            case 'Apr': varMn = '04';
                break;
            case 'May': varMn = '05';
                break;
            case 'Jun': varMn = '06';
                break;
            case 'Jul': varMn = '07';
                break;
            case 'Aug': varMn = '08';
                break;
            case 'Sep': varMn = '09';
                break;
            case 'Oct': varMn = '10';
                break;
            case 'Nov': varMn = '11';
                break;
            case 'Dec': varMn = '12';
                break;
        }

    }
    if (varfformatS[0] == 'd' && (varfformatS[1] == 'm' || varfformatS[1] == 'M') && varfformatS[2] == 'Y') {
        if (varfformatS[1] == 'M') {
            txtMainDate = varMn + splitc + vardatesplit[1] + splitc + vardatesplit[0];
        }
        else {
            txtMainDate = vardatesplit[2] + splitc + vardatesplit[1] + splitc + vardatesplit[0];
        }
    }
    else if ((varfformatS[0] == 'm' || varfformatS[0] == 'M') && varfformatS[1] == 'd' && varfformatS[2] == 'Y') {
        if (varfformatS[0] == 'M') {
            txtMainDate = varMn + splitc + vardatesplit[2] + splitc + vardatesplit[0];
        }
        else {
            txtMainDate = vardatesplit[1] + splitc + vardatesplit[2] + splitc + vardatesplit[0];
        }
    }
    else if (varfformatS[0] == 'Y' && varfformatS[1] == 'd' && varfformatS[2] == 'm') {
        txtMainDate = vardatesplit[0] + splitc + vardatesplit[2] + splitc + vardatesplit[1];
    }
    else if (varfformatS[0] == 'Y' && varfformatS[1] == 'm' && varfformatS[2] == 'd') {
        txtMainDate = vardatesplit[0] + splitc + vardatesplit[1] + splitc + vardatesplit[2];
    }
    else {
        txtMainDate = vardatesplit[0] + splitc + vardatesplit[1] + splitc + vardatesplit[2];
    }
    return txtMainDate;
}

function setLabels(labelData) {

    $('#hdnUOM').val(labelData['alert2']);
    $('#hdnAmount').val(labelData['amount']);
     $('#hdnItem').val(labelData['item']);
    $('#hdnQty').val(labelData['qty']);
    //$('#POSSearchTitleItemName').val(labelData['name']);
    $('#hdnUOM').val(labelData['uom']);
    $('#hdnPrice').val(labelData['price']);
   
    jQuery("label[for='lblTitle'").html(labelData['CurrentInventory']);
    jQuery("label[for='lblSalesReturnReport'").html(labelData['CurrentInventory']);

    //$('#hdnThameChange').val(labelData["alert5"]);


    var SubOrgId = '';
    var HashVal = (window.localStorage.getItem("hashValues"));
    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');

        SubOrgId = HashValues[1];


    }
    var FromDate = (window.localStorage.getItem("RPfrmDate"));
    var ToDate = (window.localStorage.getItem("RPtoDate")) + ' 23:59:59';
    var RefNo = (window.localStorage.getItem("RPrefNo"));
    var PageCode = (window.localStorage.getItem("RPPAGECODE"));
    var grid = "";

    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetReport,
        data: "PageId=" + PageCode + "&subOrg=" + SubOrgId + "&frmDate=" + FromDate + "&toDate=" + ToDate + "&refNo=" + RefNo + "&DateFormate=" + $('#hdnDateFromate').val() + "&VD=" + VD(),
        success: function (resp) {
            //alert(JSON.stringify(resp));

            graphData = resp;
            arrangeCurrentInventoryData(resp);
            if (window.localStorage.getItem('RPfrmDate') != null) {

                $('#INVLISlblDate').text('(' + ConvertDateAndReturnDate($('#hdnDateFromate').val(), window.localStorage.getItem('RPfrmDate')) + ' ' + $('#hdnSalesTo').val() + ' ' + ConvertDateAndReturnDate($('#hdnDateFromate').val(), window.localStorage.getItem('RPtoDate')) + ')');

            }
        }
    });
}
