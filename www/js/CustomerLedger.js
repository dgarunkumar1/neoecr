﻿var graphData;
function VD() {
    var vd_ = window.localStorage.getItem("UUID");
    return vd_;
}
function floorFigure(figure, decimals) {
    if (!decimals) decimals = 2;
    var d = Math.pow(10, decimals);
    return (parseInt(figure * d) / d).toFixed(decimals);
};
if (window.localStorage.getItem("Theam").split('-')[3] == '1.css') {
    $('#IMG1').attr("src", "images/mmlogo-inner2.png");
} else {
    $('#IMG1').attr("src", "images/mmlogo-inner1.png");
}

function InvMail() {
    $('#txtBccMailId').val('');
    $('#txtBody').val('');


    window.localStorage.setItem('ctCheck', 1);
    $('#ModalINVMail').modal('show');
    //   tenderAddpartial(TenderID, 1, '');
    $('#txtMailId').val('');
    //  $('#FileFormat').val('0');
    $('#txtMailId').focus();
    $('#FileFormat').attr("readonly", "true");
}
function SendINVMail(MailId, BccMailId, Format, Body) {
    window.localStorage.setItem('ctCheck', 1);
    if ($('#txtMailId').val() == "") {
        alert($('#hdnEnterMail').val(), "", "neo ecr", "Ok");
    }
    else {
        
        
        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.SendMailArabic,
            data: "language=" + HashValues[7] + "&PageId=" + window.localStorage.getItem('RPPAGECODE') + "&subOrg=" + HashValues[1] + "&frmDate=" + window.localStorage.getItem('RPfrmDate') + "&toDate=" + window.localStorage.getItem('RPtoDate') + ' 23:59:59' + "&DateFormate=" + $('#hdnDateFromate').val() + "&VD=" + VD() + "&MailId=" + MailId + "&Format=" + Format + "&BCMailId=" + BccMailId + "&Body=" + Body + "",
            success: function (resp) {
               
                if (resp == '1000') {
                    alert($('#hdnMailSuccess').val(), "", "neo ecr", "Ok");
                    $('#ModalINVMail').modal('hide');
                }
                else {
                    alert($('#hdnMailnotSuccess').val(), "", "neo ecr", "Ok");
                    $('#ModalINVMail').modal('hide');
                }
            },
            error: function (e) {
               
                alert($('#hdnInvalidData').val(), "", "neo ecr", "Ok");
                $('#ModalINVMail').modal('hide');
            }
        });
    }
}



function arrangeCustLedgerData(resp) {
    graphData = resp;
    var result = resp;
    var Primaryid = "";
    var LanType = "";
    $('#tbldiv thead tr').remove();
    $('#tbldiv tbody tr').remove();
    $('#tbldiv thead').append("<tr><th>" + $('#hdnMobile').val() + "</th><th>" + $('#hdnName').val() + "</th><th>" + $('#hdnOpenBal').val() + "</th><th>" + $('#hdnDrAmt').val() + "</th><th>" + $('#hdnCrAmt').val() + "</th><th>" + $('#hdnCloseBal').val() + "</th></tr>");

    var cc_ = '';
    var Dr_Cr = '';
    var ClosingBalance = '0.00';
    debugger;
    for (var i = 0; i < result.length; i++) {
        if (parseFloat(result[i].C5).toFixed(2) > 0) {
            Dr_Cr = "Dr";
            ClosingBalance = parseFloat(result[i].C5).toFixed(2);
        }
        else {
            Dr_Cr = "Cr";
            ClosingBalance = -parseFloat(result[i].C5).toFixed(2);
        }
        cc_ = cc_ + '<tr  id="dt_Parrent' + i + '" ><td onclick="treeclick(this);">' + result[i].C0 + '</td><td>' + result[i].C1 + '</td><td id="ColAlign">' + parseFloat(result[i].C2).toFixed(2) + '&nbsp' + 'Dr' + '</td><td id="ColAlign">' + parseFloat(result[i].C3).toFixed(2) + '&nbsp' + 'Dr' + '</td><td id="ColAlign">' + parseFloat(result[i].C4).toFixed(2) + '&nbsp' + 'Cr' + '</td><td id="ColAlign">' + parseFloat(ClosingBalance).toFixed(2) + '&nbsp' + Dr_Cr + '</td></tr>';
        //cc_ = cc_ + '</table></td></tr>';
    }
    $("#tbldiv tbody").append(cc_);
}
    function CustomerLedgerData() {
        debugger;
    theamLoad();
    $('#hdnPaging').val('1');
    var btnExp = document.getElementById("btnInvExp");
    btnExp.style.display = "block";
    if (window.localStorage.getItem('RPPAGECODE') == 'POSCUSTLDGR') {
        var SubOrgId = '';
        var HashVal = (window.localStorage.getItem("hashValues"));
        var HashValues;
        if (HashVal != null && HashVal != '') {
            HashValues = HashVal.split('~');
            SubOrgId = HashValues[1];
            $('#hdnDateFromate').val(HashValues[2]);
        }
        var PageIndex = $('#hdnPaging').val();
        var PageCount = $('#txt_pageCount').val();

        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.GetReport_V01,
            data: "PageId=" + window.localStorage.getItem('RPPAGECODE') + "&subOrg=" + SubOrgId + "&frmDate=" + window.localStorage.getItem('RPfrmDate') + "&toDate=" + window.localStorage.getItem('RPtoDate') + ' 23:59:59' + "&refNo=" + window.localStorage.getItem('RPrefNo') + "&DateFormate=" + $('#hdnDateFromate').val() + "&VD=" + VD() + "&PageIndex=" + PageIndex + "&PageCount=" + PageCount + "&language=" + HashValues[7] + "",
            success: function (resp) {
                graphData = resp;
                if (resp != '') {
                    jQuery("label[for='lbl_invPageOf'").html(resp[0].C9);
                }
                getResources(HashValues[7], "POSCUSTLDGR", resp, 1);
                $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());

                if ($('#hdnTotalPagesCount').val() != $('#hdnPaging').val()) {
                    $('#li_PRPrev i').removeAttr('style', 'color:#919598 !important');
                    $('#li_PRPrev').attr('onclick', 'nextPRInvl()');
                    $('#li_PRLast i').removeAttr('style', 'color:#919598 !important');
                    $('#li_PRLast').attr('onclick', 'nextPRLastInvl()');
                }
                else {
                    $('#li_PRPrev i').attr('style', 'color:#919598 !important');
                    $('#li_PRPrev').attr('onclick', 'return false');
                    $('#li_PRLast').attr('onclick', 'return false');
                    $('#li_PRLast i').attr('style', 'color:#919598 !important');
                }
            }
        });
    }
}

function theamLoad() {
    if (window.localStorage.getItem("Theam") != null)
        $('head').append('<link rel="stylesheet" href=' + window.localStorage.getItem("Theam") + ' type="text/css" />');
}
$('#btnCol').click(function () {

    var btnCol = document.getElementById("btnCol");
    btnCol.style.display = "none";

    var btnExp = document.getElementById("btnExp");
    btnExp.style.display = "block";


    var $el = $('#tbldiv tbody>tr');

    $el.addClass("tt-hide");

    for (var i = 0; i < $('#tbldiv tbody>tr').length; i++) {
        var tr = $('#tbldiv tbody>tr[id="dt_Parrent' + i + '"]');
        //var tr = $(table).find("tr[id='dt_Parrent" + i + "']");
        //var tdid = $($el[i]).find("td[id='dt_Parrent" + i + "']");
        $(tr).removeClass("tt-hide");
        $('#root_' + i + '').html("+");
    }

    $('#dt_Parrent1').removeClass("tt-hide");
    //GetPaging();

});
$('#btnExp').click(function () {
    var btnCol = document.getElementById("btnCol");
    btnCol.style.display = "block";
    var btnExp = document.getElementById("btnExp");
    btnExp.style.display = "none";
    $('#tbldiv').html("");
    $('#hdnScreenmode').val("");
    GetUnpaging();

});

function Tenderexportexcel()
{
    loadInvoiceGridExtend();
    var data_type = 'data:application/vnd.ms-excel';
    var table_div = document.getElementById('tbldiv');
    var table_html = table_div.outerHTML.replace(/ /g, '%20');
    var a = document.createElement('a');
    a.href = data_type + ', ' + table_html;
    var pageName_ = "exported_table_";
    pageName_ = "Daily-Collection-By-Tender-Type";
    a.download = pageName_ + '.xls';
    a.click();
    loadInvoiceGridCompress();
}

function TenderPrint() {
    var btnCol = document.getElementById("btnInvCol");
    loadInvoiceGridExtend();
    var divContents = document.getElementById("div_grid").innerHTML;
    var printWindow = window.open('', '', 'height=200,width=450');
    printWindow.document.write('<html><head><title>Daily Collection By Tender Type</title>');
    printWindow.document.write('</head><body >');
    printWindow.document.write(divContents);
    printWindow.document.write('</body></html>');
    loadInvoiceGridCompress();
    printWindow.document.close();
    printWindow.print();
}
function exportexcel() {
    var btnCol = document.getElementById("btnCol");
    btnCol.style.display = "block";

    var btnExp = document.getElementById("btnExp");
    btnExp.style.display = "none";
    $('#hdnScreenmode').val("excel");
    $('#tbldiv').html("");
    GetUnpaging();
   
}
var specialElementHandlers = {
    '#editor': function (element, renderer) {
        return true;
    }
};

margins = { top: 15, left: 10, width: 1000, bottom: 50 };

$('#btn').click(function () {
    // $('#btnExp').click();
    var doc = new jsPDF('p', 'pt', 'a4', true);
    source = $('#div_grid')[0];
    doc.fromHTML(source, margins.left, margins.top, {
        'width': margins.width,
        'elementHandlers': specialElementHandlers
    });
    doc.save('DailyCollectionsByTenderType.pdf');
});
function GetPaging() {
    $('table#tbldiv').each(function () {

        var currentPage = 0;
        var numPerPage = $('#hdnPaging').val();
        var $table = $(this);
        var $tr = $table.find('tbody tr');

        $table.bind('repaginate', function () {
            $table.find('tbody tr[data-tt-parent="Paging"]').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
        });
        //  
        $table.trigger('repaginate');
        var numRows = $table.find('tbody tr[data-tt-parent="Paging"]').length;
        var numPages = Math.ceil(numRows / numPerPage);

        $("#DivPager").remove();

        var $pager = $('<div id="DivPager" class="pager"></div>');
        for (var page = 0; page < numPages; page++) {
            $('<span class="page-number"></span>').text(page + 1).bind('click', {
                newPage: page
            }, function (event) {
                currentPage = event.data['newPage'];
                $table.trigger('repaginate');
                $(this).addClass('active').siblings().removeClass('active');
            }).appendTo($pager).addClass('clickable');
        }
        $pager.insertAfter($table).find('span.page-number:first').addClass('active');

    });

}

function GetUnpaging() {
    if (window.localStorage.getItem('RPPAGECODE') == 'POSTTDS') {
        var SubOrgId = '';
        var HashVal = (window.localStorage.getItem("hashValues"));
        var HashValues;
        if (HashVal != null && HashVal != '') {
            HashValues = HashVal.split('~');
            SubOrgId = HashValues[1];
            $('#hdnDateFromate').val(HashValues[2]);
        }
        if (window.localStorage.getItem('RPfrmDate') != null) {
            $('#INVLISlblDate').text('(' + ConvertDateAndReturnDate($('#hdnDateFromate').val(), window.localStorage.getItem('RPfrmDate')) + ' to ' + ConvertDateAndReturnDate($('#hdnDateFromate').val(), window.localStorage.getItem('RPtoDate')) + ')');
        }
        var FromDate = (window.localStorage.getItem("RPfrmDate"));
        var ToDate = (window.localStorage.getItem("RPtoDate")) + ' 23:59:59';
        var RefNo = (window.localStorage.getItem("RPrefNo"));
        var PageCode = (window.localStorage.getItem("RPPAGECODE"));
        var grid = "";
        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.GetReport,
            data: "PageId=" + PageCode + "&subOrg=" + SubOrgId + "&frmDate=" + FromDate + "&toDate=" + ToDate + "&refNo=" + RefNo + "&DateFormate=" + $('#hdnDateFromate').val() + "&VD=" + VD(),
            success: function (resp) {
                var graphData = resp;
                //getResources(HashValues[7], "POSTenderTypewiseCollectionReport", resp, 2);
                getResources(HashValues[7], "POSCUSTLDGR", resp, 1);
                //arrangeDailyCollectionData(resp);
                //var $el = $('#tbldiv tbody>tr.tt-hide');
                //$el.removeClass("tt-hide");

                //for (var i = 0; i < $el.length; i++) {
                //    $('#root_' + i + '').html("-");
                //}
                //$('#DivPager').css("display", "none");
                //if ($('#hdnScreenmode').val() == "print")
                //{
                //    var divContents = document.getElementById("div_grid").innerHTML;
                //    var printWindow = window.open('', '', 'height=200,width=450');
                //    printWindow.document.write('<html><head><title>DailyCollectionsSummaryByTenderTypeReport</title>');
                //    printWindow.document.write('</head><body >');
                //    printWindow.document.write(divContents);
                //    printWindow.document.write('</body></html>');
                //    printWindow.document.close();
                //    printWindow.print();
                //}
                //if ($('#hdnScreenmode').val() == "excel") {
                //    var data_type = 'data:application/vnd.ms-excel';
                //    var table_div = document.getElementById('tbldiv');
                //    var table_html = table_div.outerHTML.replace(/ /g, '%20');
                //    var a = document.createElement('a');
                //    a.href = data_type + ', ' + table_html;
                //    a.download = 'exported_table_' + Math.floor((Math.random() * 9999999) + 1000000) + '.xls';
                //    a.click();
                //}
            }
        });
    }

}

function ConvertDateAndReturnDate(formates, dateText) {
    var hdnformat = formates;
    var txtMainDate = '';
    var splitc;
    if (hdnformat == 'd/m/Y' || hdnformat == 'm/d/Y' || hdnformat == 'Y/d/m' || hdnformat == 'Y/m/d' || hdnformat == 'M/d/Y' || hdnformat == 'd/M/Y') {
        splitc = '/';
    }
    else if (hdnformat == 'd-m-Y' || hdnformat == 'm-d-Y' || hdnformat == 'Y-d-m' || hdnformat == 'Y-m-d' || hdnformat == 'M-d-Y' || hdnformat == 'd-M-Y') {
        splitc = '-';
    }
    else if (hdnformat == 'd,m,Y' || hdnformat == 'm,d,Y' || hdnformat == 'Y,d,m' || hdnformat == 'Y,m,d' || hdnformat == 'M,d,Y' || hdnformat == 'd,M,Y') {

        splitc = ',';
    }
    else if (hdnformat == 'd m Y' || hdnformat == 'm d Y' || hdnformat == 'Y d m' || hdnformat == 'Y m d' || hdnformat == 'M d Y' || hdnformat == 'd M Y') {

        splitc = ' ';
    }
    else if (hdnformat == 'd.m.Y' || hdnformat == 'm.d.Y' || hdnformat == 'Y.d.m' || hdnformat == 'Y.m.d' || hdnformat == 'M.d.Y' || hdnformat == 'd.M.Y') {
        splitc = '.';
    };
    var sym = '-';
    if (dateText.indexOf('/') != -1)
        sym = '/';
    var vardatesplit = dateText.split(sym);
    var varM;
    var varMn;
    var varfformatS = hdnformat.split(splitc);
    if (varfformatS[0] == 'M' || varfformatS[1] == 'M') {

        if (vardatesplit[1] != null)
            varM = vardatesplit[1];
        if (vardatesplit[0] != null)
            varM = vardatesplit[0];

        switch (varM) {
            case 'Jan': varMn = '01';
                break;
            case 'Feb': varMn = '02';
                break;
            case 'Mar': varMn = '03';
                break;
            case 'Apr': varMn = '04';
                break;
            case 'May': varMn = '05';
                break;
            case 'Jun': varMn = '06';
                break;
            case 'Jul': varMn = '07';
                break;
            case 'Aug': varMn = '08';
                break;
            case 'Sep': varMn = '09';
                break;
            case 'Oct': varMn = '10';
                break;
            case 'Nov': varMn = '11';
                break;
            case 'Dec': varMn = '12';
                break;
        }

    }
    if (varfformatS[0] == 'd' && (varfformatS[1] == 'm' || varfformatS[1] == 'M') && varfformatS[2] == 'Y') {
        if (varfformatS[1] == 'M') {
            txtMainDate = varMn + splitc + vardatesplit[1] + splitc + vardatesplit[0];
        }
        else {
            txtMainDate = vardatesplit[2] + splitc + vardatesplit[1] + splitc + vardatesplit[0];
        }
    }
    else if ((varfformatS[0] == 'm' || varfformatS[0] == 'M') && varfformatS[1] == 'd' && varfformatS[2] == 'Y') {
        if (varfformatS[0] == 'M') {
            txtMainDate = varMn + splitc + vardatesplit[2] + splitc + vardatesplit[0];
        }
        else {
            txtMainDate = vardatesplit[1] + splitc + vardatesplit[2] + splitc + vardatesplit[0];
        }
    }
    else if (varfformatS[0] == 'Y' && varfformatS[1] == 'd' && varfformatS[2] == 'm') {
        txtMainDate = vardatesplit[0] + splitc + vardatesplit[2] + splitc + vardatesplit[1];
    }
    else if (varfformatS[0] == 'Y' && varfformatS[1] == 'm' && varfformatS[2] == 'd') {
        txtMainDate = vardatesplit[0] + splitc + vardatesplit[1] + splitc + vardatesplit[2];
    }
    else {
        txtMainDate = vardatesplit[0] + splitc + vardatesplit[1] + splitc + vardatesplit[2];
    }
    return txtMainDate;
}
function loadTenderGrid() {
    $('#li-Excel i').removeAttr('style', 'color:#919598 !important');
    $('#li-Excel').attr('onclick', 'GetDowloadFile()');
    $('#li-Refresh i').removeAttr('style', 'color:#919598 !important');
    $('#li-Refresh').attr('onclick', 'PageISalestaxRPBind()');
    $('#li_expand i').removeAttr('style', 'color:#919598 !important');
    $('#li_expand').attr('onclick', 'loadInvoiceGridExtend()');
    $('#li_compress i').removeAttr('style', 'color:#919598 !important');
    $('#li_compress').attr('onclick', 'loadInvoiceGridCompress()');
    $('#li_Print i').removeAttr('style', 'color:#919598 !important');
    $('#li_Print').attr('onclick', 'TenderPrint()');
    var elemGrid = document.getElementById("div_grid");
    elemGrid.style.display = "block";

    var elemGraph = document.getElementById("div_graph1");
    elemGraph.style.display = "none";
}

function print() {
    var btnCol = document.getElementById("btnCol");
    btnCol.style.display = "block";

    var btnExp = document.getElementById("btnExp");
    btnExp.style.display = "none";
    $('#hdnScreenmode').val("print");
    $('#tbldiv').html("");
    GetUnpaging();
}
function loadInvoicebyPageIndexStatic() {
    var Ttype = $('#hdnTtype').val();
    var pageIndex = 1;
    if ($('#txt_PRPPageIndex').val() != '') {
        if ($('#txt_PRPPageIndex').val() != '0') {
            pageIndex = $('#txt_PRPPageIndex').val();
        }
    }
    $('#hdnPaging').val(pageIndex);
    var btnExp = document.getElementById("btnInvExp");
    btnExp.style.display = "block";
         var SubOrgId = '';
        var HashVal = (window.localStorage.getItem("hashValues"));
        var HashValues;
        if (HashVal != null && HashVal != '') {
            HashValues = HashVal.split('~');
            SubOrgId = HashValues[1];
            $('#hdnDateFromate').val(HashValues[2]);
        }
        var PageIndex = $('#hdnPaging').val();
        var PageCount = $('#txt_pageCount').val();

        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.GetReport_V01,
            data: "PageId=" + window.localStorage.getItem('RPPAGECODE') + "&subOrg=" + SubOrgId + "&frmDate=" + window.localStorage.getItem('RPfrmDate') + "&toDate=" + window.localStorage.getItem('RPtoDate') + ' 23:59:59' + "&refNo=" + window.localStorage.getItem('RPrefNo') + "&DateFormate=" + $('#hdnDateFromate').val() + "&VD=" + VD() + "&PageIndex=" + PageIndex + "&PageCount=" + PageCount + "&Ttype=" + Ttype + "&language=" + HashValues[7] + "",
            success: function (resp) {
                if (resp != '') {
                    jQuery("label[for='lbl_invPageOf'").html(resp[0].C9);//resp[0].C2);
                }
                //getResources(HashValues[7], "POSTenderTypewiseCollectionReport", resp, 1);
                getResources(HashValues[7], "POSCUSTLDGR", resp, 1);
                $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());
            }
        });
        $('#txt_PRPPageIndex').val("");
}

function Tenderexportexcel() {
    loadInvoiceGridExtend();
    var data_type = 'data:application/vnd.ms-excel';
    var table_div = document.getElementById('tbldiv');
    var table_html = table_div.outerHTML.replace(/ /g, '%20');
    var a = document.createElement('a');
    a.href = data_type + ', ' + table_html;
    var pageName_ = "exported_table_";
    pageName_ = "Tender-Type-Wise-Collection-Daily-Summary";
    a.download = pageName_ + '.xls';
    var d = $('#div_tbData');
    let options = {
        documentSize: 'A4',
        type: 'share',
        fileName: 'Customer Ledger.pdf'
    }
    var lable_ = "<div><b>Customer Ledger" + $('#INVLISlblDate').text() + "</b></div></br>";
    pdf.fromData(lable_ + d[0].innerHTML, options)
        .then((stats) => console.log('status', stats))   // ok..., ok if it was able to handle the file to the OS.  
        .catch((err) => console.err(err));
    loadInvoiceGridCompress();
}
function prevPRFirstInvl() {
    window.localStorage.setItem('ctCheck', 1);
    CustomerLedgerData();
   
}
function prevPRInvl() {
    window.localStorage.setItem('ctCheck', 1);
    $('#hdnPaging').val(parseFloat($('#hdnPaging').val()) - 1);
    if ($('#txt_pageCount').val() == "") {
        $('#txt_pageCount').val('10');
        $('#hdnPaging').val('1');
    }
    if (parseFloat($('#hdnPaging').val()) == 0) {
        $('#hdnPaging').val('1');
    }
    if ($('#hdnTotalPagesCount').val() != $('#hdnPaging').val()) {
        $('#li_PRPrev i').removeAttr('style', 'color:#919598 !important');
        $('#li_PRPrev').attr('onclick', 'nextPRInvl()');
        $('#li_PRLast i').removeAttr('style', 'color:#919598 !important');
        $('#li_PRLast').attr('onclick', 'nextPRLastInvl()');
    }
    var btnExp = document.getElementById("btnInvExp");
    btnExp.style.display = "block";
    if (window.localStorage.getItem('RPPAGECODE') == 'POSCUSTLDGR') {
        var SubOrgId = '';
        var HashVal = (window.localStorage.getItem("hashValues"));
        var HashValues;
        if (HashVal != null && HashVal != '') {
            HashValues = HashVal.split('~');

            SubOrgId = HashValues[1];
            $('#hdnDateFromate').val(HashValues[2]);

        }

        var PageIndex = $('#hdnPaging').val();
        var PageCount = $('#txt_pageCount').val();

        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.GetReport_V01,
            data: "PageId=" + window.localStorage.getItem('RPPAGECODE') + "&subOrg=" + SubOrgId + "&frmDate=" + window.localStorage.getItem('RPfrmDate') + "&toDate=" + window.localStorage.getItem('RPtoDate') + ' 23:59:59' + "&refNo=" + window.localStorage.getItem('RPrefNo') + "&DateFormate=" + $('#hdnDateFromate').val() + "&VD=" + VD() + "&PageIndex=" + PageIndex + "&PageCount=" + PageCount + "&language=" + HashValues[7] + "",
            success: function (resp) {
                graphData = resp;
                jQuery("label[for='lbl_invPageOf'").html(resp[0].C9);
                getResources5(HashValues[7], "POSInvoiceListingReport", resp, 1);
                $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());


            }

        });
    }
    //if (window.localStorage.getItem('RPPAGECODE') == 'POSTINL') {
    //    var SubOrgId = '';
    //    var HashVal = (window.localStorage.getItem("hashValues"));
    //    var HashValues;
    //    if (HashVal != null && HashVal != '') {
    //        HashValues = HashVal.split('~');

    //        SubOrgId = HashValues[1];
    //        $('#hdnDateFromate').val(HashValues[2]);

    //    }

    //    var PageIndex = $('#hdnPaging').val();
    //    var PageCount = $('#txt_pageCount').val();

    //    $.ajax({
    //        type: 'GET',
    //        url: CashierLite.Settings.GetReport_V01,
    //        data: "PageId=" + window.localStorage.getItem('RPPAGECODE') + "&subOrg=" + SubOrgId + "&frmDate=" + window.localStorage.getItem('RPfrmDate') + "&toDate=" + window.localStorage.getItem('RPtoDate') + ' 23:59:59' + "&refNo=" + window.localStorage.getItem('RPrefNo') + "&DateFormate=" + $('#hdnDateFromate').val() + "&VD=" + VD2() + "&PageIndex=" + PageIndex + "&PageCount=" + PageCount + "",
    //        success: function (resp) {
    //            graphData = resp;
    //            jQuery("label[for='lbl_invPageOf'").html(resp[0].C11);
    //            getResources4(HashValues[7], "POSTillInvoiceListingReport", resp, 1)
    //            $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());


    //        }

    //    });
    //}
}

function nextPRInvl() {
    window.localStorage.setItem('ctCheck', 1);
    $('#hdnPaging').val(parseFloat($('#hdnPaging').val()) + 1);
    if ($('#txt_pageCount').val() == "") {
        $('#txt_pageCount').val('10');
        $('#hdnPaging').val('1');
    }
    var btnExp = document.getElementById("btnInvExp");
    btnExp.style.display = "block";
    if (window.localStorage.getItem('RPPAGECODE') == 'POSCUSTLDGR') {
        var SubOrgId = '';
        var HashVal = (window.localStorage.getItem("hashValues"));
        var HashValues;
        if (HashVal != null && HashVal != '') {
            HashValues = HashVal.split('~');

            SubOrgId = HashValues[1];
            $('#hdnDateFromate').val(HashValues[2]);

        }

        var PageIndex = $('#hdnPaging').val();
        var PageCount = $('#txt_pageCount').val();

        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.GetReport_V01,
            data: "PageId=" + window.localStorage.getItem('RPPAGECODE') + "&subOrg=" + SubOrgId + "&frmDate=" + window.localStorage.getItem('RPfrmDate') + "&toDate=" + window.localStorage.getItem('RPtoDate') + ' 23:59:59' + "&refNo=" + window.localStorage.getItem('RPrefNo') + "&DateFormate=" + $('#hdnDateFromate').val() + "&VD=" + VD() + "&PageIndex=" + PageIndex + "&PageCount=" + PageCount + "&language=" + HashValues[7] + "",
            success: function (resp) {
                graphData = resp;
                jQuery("label[for='lbl_invPageOf'").html(resp[0].C9);
               // getResources5(HashValues[7], "POSInvoiceListingReport", resp, 1);
                  getResources(HashValues[7], "POSCUSTLDGR", resp, 1);
                $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());


            }

        });
    }

    if ($('#hdnTotalPagesCount').val() == $('#hdnPaging').val()) {
        $('#li_PRPrev i').attr('style', 'color:#919598 !important');
        $('#li_PRPrev').attr('onclick', 'return false');
        $('#li_PRLast').attr('onclick', 'return false');
        $('#li_PRLast i').attr('style', 'color:#919598 !important');
    }
}
function nextPRLastInvl() {
    window.localStorage.setItem('ctCheck', 1);
    var lastPageIndex = $('#lbl_invPageOf')[0].innerText.split(' ')[2];
    if (lastPageIndex == '') {
        lastPageIndex = '1';
    }
    $('#hdnPaging').val(parseFloat(lastPageIndex));
    if ($('#txt_pageCount').val() == "") {
        $('#txt_pageCount').val('10');
        $('#hdnPaging').val('1');
    }
    var btnExp = document.getElementById("btnInvExp");
    btnExp.style.display = "block";
    if (window.localStorage.getItem('RPPAGECODE') == 'POSCUSTLDGR') {
        var SubOrgId = '';
        var HashVal = (window.localStorage.getItem("hashValues"));
        var HashValues;
        if (HashVal != null && HashVal != '') {
            HashValues = HashVal.split('~');
            SubOrgId = HashValues[1];
            $('#hdnDateFromate').val(HashValues[2]);
        }
        var PageIndex = $('#hdnPaging').val();
        var PageCount = $('#txt_pageCount').val();
        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.GetReport_V01,
            data: "PageId=" + window.localStorage.getItem('RPPAGECODE') + "&subOrg=" + SubOrgId + "&frmDate=" + window.localStorage.getItem('RPfrmDate') + "&toDate=" + window.localStorage.getItem('RPtoDate') + ' 23:59:59' + "&refNo=" + window.localStorage.getItem('RPrefNo') + "&DateFormate=" + $('#hdnDateFromate').val() + "&VD=" + VD() + "&PageIndex=" + PageIndex + "&PageCount=" + PageCount + "&language=" + HashValues[7] + "",
            success: function (resp) {
                graphData = resp;
                jQuery("label[for='lbl_invPageOf'").html(resp[0].C9);
                getResources(HashValues[7], "POSCUSTLDGR", resp, 1);
                $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());
            }

        });
    }
    if ($('#hdnTotalPagesCount').val() == $('#hdnPaging').val()) {
        $('#li_PRPrev i').attr('style', 'color:#919598 !important');
        $('#li_PRPrev').attr('onclick', 'return false');
        $('#li_PRLast').attr('onclick', 'return false');
        $('#li_PRLast i').attr('style', 'color:#919598 !important');
    }
}
function prevPRFirstInvl() {
    window.localStorage.setItem('ctCheck', 1);
    CustomerLedgerData();
}
function loadGraphData() {

    $('#li-Excel i').attr('style', 'color:#919598 !important');
    $('#li-Excel').attr('onclick', 'return false');
    $('#li-Refresh i').attr('style', 'color:#919598 !important');
    $('#li-Refresh').attr('onclick', 'return false');
    $('#li_expand i').attr('style', 'color:#919598 !important');
    $('#li_expand').attr('onclick', 'return false');
    $('#li_compress i').attr('style', 'color:#919598 !important');
    $('#li_compress').attr('onclick', 'return false');
    $('#li_Print i').attr('style', 'color:#919598 !important');
    $('#li_Print').attr('onclick', 'return false');
    var GlanType="";
    var param = { CounterId: window.localStorage.getItem('COUNTERID'), ShiftId: window.localStorage.getItem('SHIFTID'), SalespersonId: window.localStorage.getItem('CashierID') };
    var color = Chart.helpers.color;
    var barChartData = '';
    var GrColorTr = '';
    if (window.localStorage.getItem("Theam").split('-')[3] == '2.css') { GrColorTr = '#ffffff' } else { GrColorTr = '#000' }

    var elemGrid = document.getElementById("div_grid");
    elemGrid.style.display = "none";
    var elemGraph = document.getElementById("div_graph1");
    elemGraph.style.display = "block";
    var resp = graphData;
    var x = []; var y = [];
    for (var i = 0; i < resp.length; i++) {
      // if (HashValues[7] == "ar-SA") {
            if (resp[i].C0 == "CASH" || resp[i].C0 == "Cash") {
                GlanType = $('#hdnECcash').val();
            }
            if (resp[i].C0 == "CARD" || resp[i].C0 == "Card") {
                GlanType = $('#hdnECCard').val();
            }
            if (resp[i].C0 == "E-PAY" || resp[i].C0 == "E-wallet") {
                GlanType = $('#hdnECIPay').val();
            }
        //}
        //else
        //    GlanType = result[i].C0;
       
        x.push(GlanType);
        y.push(parseFloat(resp[i].C1).toFixed(2));
    }
    barChartData = {
        labels: x,
        datasets: [{
            label: $('#hdnSales').val(),
            backgroundColor: ["#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276"], //color(window.chartColors.red).alpha(0.5).rgbString(),
            borderColor: ["#ff7276", "#ff999c", "#ffb3b5", "#ffccce", "#ffe6e6", "#ff7276", "#ff7276", "#ff7276"],//window.chartColors.red,
            borderWidth: 1,
            data: y
        },
          {
              type: 'line',
              label: $('#hdnLineView').val(),
              borderColor: window.chartColors.red,
              borderWidth: 2,
              fill: false,
              data: y,

          }
        ],
       
    }

    var ctx = document.getElementById("canvas").getContext("2d");
    window.myBar = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            legend: {
                labels: {
                    fontColor: GrColorTr,
                    fontSize: 12
                }
            },
            responsive: true,
            title: {
                display: true,
                text: ''
            },
            scales: {
                yAxes: [{
                    ticks: {
                        min: 0,
                        // stepSize: 1,
                        fontColor: GrColorTr,
                        fontSize: 14
                    }
                    ,
                    gridLines: {
                        color: GrColorTr
                        //    lineWidth: 2,
                        //    zeroLineColor: "#000",
                        //    zeroLineWidth: 2
                    },
                    stacked: true
                }],
                xAxes: [{
                    ticks: {
                        autoSkip: false,
                        maxRotation: 60,
                        minRotation: 60,
                        fontColor: GrColorTr,
                        fontSize: 14
                    },
                    gridLines: {
                        color: GrColorTr,
                        lineWidth: 2
                    }
                }]
            }
        }
    });
}

function loadMainData() {
    var elemGrid = document.getElementById("div_grid");
    elemGrid.style.display = "block";
    var elemGraph = document.getElementById("div_graph1");
    elemGraph.style.display = "none";
}
function getResources(langCode, pageCode, data, sts) {
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.getResource,
        data: "langCode=" + langCode + "&pageCode=" + pageCode,
        success: function (resp) {
            setLabelsCustomerLedger(resp, data, sts);
        }
    });
}

function setLabelsCustomerLedger(labelData, data, sts) {
    jQuery("label[for='lblLocationwiseProfitAnalysis']").html(labelData["LocwiseProfitAnlys"]);
    jQuery("label[for='lblItemCategoryProfitAnalysis']").html(labelData["ItemCatgPrftAnlys"]);
    
    jQuery("label[for='lblTitle']").html(labelData["PosCustLdger"]);
    $('#hdnMobile').val(labelData["mobile"]);
    $('#hdnName').val(labelData["name"]); 
    $('#hdnOpenBal').val(labelData["OpeningBalance"]);
    $('#hdnDrAmt').val(labelData["InvoiceAmount"]);
    $('#hdnCrAmt').val(labelData["alert28"]);
    $('#hdnCloseBal').val(labelData["ClosingBalance"]);
    jQuery("label[for='lbl_page']").html(labelData["page"]);
    jQuery("label[for='lblMail']").html(labelData["Mail"]);
    jQuery("label[for='ToMail']").html(labelData["POSTo"]);
    jQuery("label[for='Bcc']").html(labelData["BCC"]);
    jQuery("label[for='lblFormat']").html(labelData["format"]);
    jQuery("label[for='lblBody']").html(labelData["body"]);
    jQuery("label[for='lblsend']").html(labelData["Send"]);
    jQuery("label[for='lblClose']").html(labelData["Close"]);
    $('#hdnChPwdSaveSucc').val(labelData["saveSuccess"]);
    $('#hdnThameChange').val(labelData["alert1"]);
    $('#hdnLineView').val(labelData["alert4"]);
    $('#hdnEnterMail').val(labelData["enterMail"]);
    $('#hdnMailSuccess').val(labelData["MailSuccess"]);
    $('#hdnMailnotSuccess').val(labelData["MailnotSuccess"]);
    $('#hdnInvalidData').val(labelData["InvalidData"]);
    $('#hdnECcash').val(labelData["POSCash"]);
    $('#hdnECIPay').val(labelData["POSEWallet"]);
    $('#hdnECCard').val(labelData["POSEcard"]);
    $('#hdnMExcel').val(labelData["Excel"]);
    getLoadExdata();
    arrangeCustLedgerData(data);
    if (sts == 2) {
        var $el = $('#tbldiv tbody>tr.tt-hide');
        $el.removeClass("tt-hide");

        for (var i = 0; i < $el.length; i++) {
            $('#root_' + i + '').html("-");
        }
        $('#DivPager').css("display", "none");
        if ($('#hdnScreenmode').val() == "print") {
            var divContents = document.getElementById("div_grid").innerHTML;
            var printWindow = window.open('', '', 'height=200,width=450');
            printWindow.document.write('<html><head><title>Customer Ledger</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(divContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
        }
        if ($('#hdnScreenmode').val() == "excel") {
            var data_type = 'data:application/vnd.ms-excel';
            var table_div = document.getElementById('tbldiv');
            var table_html = table_div.outerHTML.replace(/ /g, '%20');
            var a = document.createElement('a');
            a.href = data_type + ', ' + table_html;
            a.download = 'Tender-Type-Wise-Collection-Daily-Summary' + '.xls';
            a.click();
        }
    }
    if (window.localStorage.getItem('RPfrmDate') != null) {
        $('#INVLISlblDate').text('(' + ConvertDateAndReturnDate($('#hdnDateFromate').val(), window.localStorage.getItem('RPfrmDate')) + ' ' + $('#hdnSalesTo').val() + ' ' + ConvertDateAndReturnDate($('#hdnDateFromate').val(), window.localStorage.getItem('RPtoDate')) + ')');
    }
}
function getLoadExdata()
{
//document.getElementById("FileFormat").options.length = 0;
$("#FileFormat").append($("<option></option>").val("0").html("----select----"));
$("#FileFormat").append($("<option></option>").val("1").html($('#hdnMExcel').val()));
 $("#FileFormat option[value='1']").attr('selected','selected');
}
function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}