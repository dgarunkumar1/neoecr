﻿function VD() {
    window.localStorage.setItem('ctCheck', 1);
    var vd_ = window.localStorage.getItem("UUID");
    //var vdSplit_ = window.location.pathname.split('/');
    //if (vdSplit_.length > 2) {
    //    vd_ = vdSplit_[1];
    //}
    return vd_;
}
var SubOrgId;
var companyId;
var HashVal = (window.localStorage.getItem("hashValues"));
var HashValues;

function theamLoad() {
    window.localStorage.setItem('ctCheck', 1);
    if (window.localStorage.getItem("Theam") != null)
        $('head').append('<link rel="stylesheet" href=' + window.localStorage.getItem("Theam") + ' type="text/css" />');
}
function GetLocwiseAvgTransValRPT() {
    window.localStorage.setItem('ctCheck', 1);
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');
        SubOrgId = HashValues[1];

    }
    theamLoad();
    getResources(HashValues[7], "POSLocationwiseAvgTransactionValueRPT");
   
}
function arrangementLocAvgTransValRPBind(resp) {
    window.localStorage.setItem('ctCheck', 1);
    $('#tbldiv').append("<tr><th>" + $("#hdnLocation").val() + "</th><th>" + $("#hdnWTD").val() + "</th><th>" + $("#hdnMTD").val() + "</th><th>" + $("#hdnYTD").val() + "</th></tr>");
    $('#tbldiv').append("<tbody>");
    var cc_ = '';
    for (var i = 0; i < resp.length; i++) {
        cc_ = cc_ + "<tr><td>" + resp[i].Location_Name + "</td><td>" + parseFloat(resp[i].WTD).toFixed(2) + "</td><td>" + parseFloat(resp[i].MTD).toFixed(2) + "</td><td>" + parseFloat(resp[i].YTD).toFixed(2) + "</td></tr>"
    }
    $('#tbldiv').append(cc_);
    $('#tbldiv').append("</tbody>");

}
function loadGraphDataWDR() {
    window.localStorage.setItem('ctCheck', 1);
    var param = { VD: "VD()" };
    var color = Chart.helpers.color;
    var barChartData = '';
    var elemGrid = document.getElementById("div_grid");
    elemGrid.style.display = "none";

    var elemGraph = document.getElementById("div_graph1");
    elemGraph.style.display = "block";

    var resp = graphData;

    var x = []; var y = []; var x1 = []; var x2 = []; var x3 = [];


    for (var i = 0; i < resp.length; i++) {
        x.push(resp[i].Location_Name);
        x1.push(resp[i].MTD);
        x2.push(resp[i].YTD);
        x3.push(resp[i].WTD);

        //x.push(resp[i].Location_Name);
    }

    barChartData = {
        labels: x,
        datasets: [{
            label: $("#hdnWTD").val(),
            backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
            borderColor: window.chartColors.blue,
            borderWidth: 1,
            data: x3
        },
        {
            label: $("#hdnMTD").val(),//'MTD',
            backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
            borderColor: window.chartColors.red,
            borderWidth: 1,
            data: x1
        },
        {
            label: $("#hdnYTD").val(),//'YTD',
            backgroundColor: color(window.chartColors.black).alpha(0.5).rgbString(),
            borderColor: window.chartColors.black,
            borderWidth: 1,
            data: x2
        }
        ]
    }

    var ctx = document.getElementById("canvas").getContext("2d");
    window.myBar = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            responsive: true,
            title: {
                display: true,
                text: ''
            },
        }
    });
}

function exportexcel() {
    window.localStorage.setItem('ctCheck', 1);
    var data_type = 'data:application/vnd.ms-excel';
    var table_div = document.getElementById('tbldiv');
    var table_html = table_div.outerHTML.replace(/ /g, '%20');
    var a = document.createElement('a');
    a.href = data_type + ', ' + table_html;
    a.download = "Location_Wise_Average_Invoice_Value" + Math.floor((Math.random() * 9999999) + 1000000) + '.xls';
    a.click();

}
function getResources(langCode, pageCode) {
    window.localStorage.setItem('ctCheck', 1);
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.getResource,
        data: "langCode=" + langCode + "&pageCode=" + pageCode,
        success: function (resp) {
            setLabels(resp);

            //$('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());
        }
    });
}
function setLabels(labelData) {
    window.localStorage.setItem('ctCheck', 1);
    jQuery("label[for='lblTitle'").html(labelData['LocWisavgtrsval']);
    jQuery("label[for='lblLocWisavgtrsval'").html(labelData['LocWisavgtrsval']);
    $('#hdnLocation').val(labelData['alert1']);
    $('#hdnWTD').val(labelData['WTD']);
    $('#hdnMTD').val(labelData['MTD']);
    $('#hdnYTD').val(labelData['YTD']);
    $("#hdnLocWisavgtrsval").val(labelData['LocWisavgtrsval']);

    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');
        companyId = HashValues[0];
    }
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetLocationwiseAvgTransactionValue,
        data: "VD=" + VD() + "&orgId=" + companyId,
        success: function (resp) {
            graphData = resp;
            //getResources5(HashValues[7], "POSLocationwiseAvgTransactionValueRPT", resp, 1);

            arrangementLocAvgTransValRPBind(resp);
        }
    });
}