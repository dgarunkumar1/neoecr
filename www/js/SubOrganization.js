﻿function GetSubOrganizationData() {
    window.localStorage.setItem('ctCheck', 1);
    if (window.localStorage.getItem('network') == '1') {
        GetLoginUserName();
        defaultdata();
        $('#hdnPageIndex').val('1');
        $('#hdnFilterType').val('Summary');
        GetSummaryGridData('Summary');
        $('#lblLastUpdate').css("display", "none");
    }
    else {
        window.open('DashBoard.html', '_self', 'location=no');
    }
}
GetCityData();
function GetCityData() {
    var CityMain = "";
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetCityData,
        data: "VD=" + window.localStorage.getItem("UUID") + "",
        success: function (resp) {
            var len = resp.length;
            $("#txtCity").empty();
            $("#txtCity").append("<option value=0>Select City</option>");
            for (var i = 0; i < len; i++) {
                $("#txtCity").append("<option value='" + resp[i]["CityId"] + "'>" + resp[i]["CityName"] + "</option>");
                if (CityMain != '')
                    CityMain = CityMain + resp[i]["CityId"] + '~' + resp[i]["CityName"] + '$';
                else
                    CityMain = resp[i]["CityId"] + '~' + resp[i]["CityName"] + '$';
            };
            window.localStorage.setItem("FillCityMain", CityMain);
        }, error: function (e) {

        }
    });
}
try {
    $("#txtCity").autocomplete({

        dataFilter: function (data) {
            return data
        },
        source: function (req, add) {
            window.localStorage.setItem('ctCheck', 1);
            var v = (window.localStorage.getItem("FillCityMain"));
            jsonObj = [];
            if (v != "") {
                var cus_ = v.split('$');
                for (var i = 0; i < cus_.length; i++) {
                    var cusChek = cus_[i].split('~');
                    if (cusChek.length > 1) {
                        item = {
                        }
                        item["value"] = cusChek[1];
                        item["data"] = cusChek[0];
                        jsonObj.push(item);
                    }
                }
            }
            add($.map(jsonObj, function (el) {
                var re = $.ui.autocomplete.escapeRegex(req.term);
                if (re.trim() != "") {
                    if (el.value.toLowerCase().indexOf(re.toLowerCase()) != -1) {
                        return {
                            label: el.value,
                            val: el.data
                        };
                    }
                }
                else {
                    return {
                        label: el.value,
                        val: el.data
                    };
                }
            }));
        },
        change: function (e, i) {
            if (i.item) {
            }
            else {
                $('#txtCity').val('');
                $('#hdnCityId').val('');
            }
        },
        select: function (event, ui) {
            $('#txtCity').val(ui.item.label);
            $('#hdnCityId').val(ui.item.val);

        }
    });

}
catch (a) {

}

function GetLoginUserName() {
    window.localStorage.setItem('ctCheck', 1);
    var sess = CashierLite.Session.getInstance().get();
    if (sess != null) {
        if (sess.userName != "") {
            document.getElementById("lblName").innerHTML = sess.userName;
        }
        else {
            window.open('Login.html', '_self', 'location=no');
        }
    }
    else {
        window.open('Login.html', '_self', 'location=no');
    }
    theamLoad();
}
function theamLoad() {
    window.localStorage.setItem('ctCheck', 1);
    if (window.localStorage.getItem("Theam") != null)
        $('head').append('<link rel="stylesheet" href=' + window.localStorage.getItem("Theam") + ' type="text/css" />');
}

function defaultdata() {
    window.localStorage.setItem('ctCheck', 1);
    var Paging = ""; var CompanyId = ""; var SubOrgId = ''; var Orgcode = ''; var OBS = ''; var CashierID = window.localStorage.getItem("CashierID"); var Userid = ''; var StoreMailId= '';
    var Lang = '';
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        getResources(hashSplit[7], "POSStore");
        CompanyId = hashSplit[0];
        SubOrgId = hashSplit[1];
        $('#hdnOrgId').val(CompanyId);
        $('#hdnSubOrgId').val(SubOrgId);
        OBS = hashSplit[9];
        Orgcode = hashSplit[17];
        Userid = hashSplit[14];
        Lang = hashSplit[7];
        Paging = hashSplit[19];
        StoreMailId = hashSplit[6];
        $('#hdnStoreMailId').val(StoreMailId);
        
    }
    document.getElementById("lblMTillCode").innerHTML = hashSplit[28];
    $('#hdnOBS').val(OBS);
    $('#hdnUserId').val(Userid);
    $('#hdnLanguage').val(Lang);
    $('#hdnPaging').val(Paging);
    $('#SummaryGrid').css("display", "block");
    $("#ContentDet").css('display', 'none');
    $('#Create').css("display", "block");
    $('#Edit').css("display", "none");
    $('#Cancel').css("display", "none");
    $('#btnSave').css("display", "none");
    $('#Delete').css("display", "none");
    $('#Status').css("display", "none");
    $('#divActive').css("display", "none");
}
$("#btnActive").click(function () {

    $('#hdnScreenMode').val("Active");
    Save('Mode');
});
$("#btnInActive").click(function () {
    $('#hdnScreenMode').val("InActive");
    Save('Mode');
});
$("#btnCreate").click(function () {
    window.localStorage.setItem('ctCheck', 1);
    clearControls_Store();
    defaultdata();
    EnableFormfileds(false);
    $('#lblLastUpdate').css("display", "none");
    $('#trSBInnerRow li span#lblLastUpdatedUser').html("");
    $('#trSBInnerRow li span#lblLastUpdatedDate').html("");
    $('#trSBInnerRow li span#lblMode').html($('#hdnNew').val());
    $('#hdnMode').val('New');
    $('#txtSubCode').removeAttr("disabled", "disabled");
    $('#txtSubName').removeAttr("disabled", "disabled");
    $("#txtAlias").removeAttr("disabled", "disabled");
    $("#btnCreate").attr('style', 'display:none;');
    $("#ShowMenuModes").attr('style', 'display:block;');
    $("#btnSave").attr('style', 'display:block;');
    $("#btnEdit").attr('style', 'display:none;');
    $("#btnDelete").attr('style', 'display:none;');
    $("#btnCancel").attr('style', 'display:block;');
    $('#SummaryGrid').css("display", "none");
    $("#ContentDet").css('display','block');
    $("#divStatusGroup").attr('style', 'display:none;');
    $("#divNavBarRight").attr('style', 'display:none;');

    $('#txtSubCode').prop("disabled", true);
    $('#PrimeInfo').addClass('open');
    $('#divPrimeInformation').attr('style', 'display:block');
});
$('#btnSave').click(function () {
    window.localStorage.setItem('ctCheck', 1);
   
    if ($('#txtSubName').val() == "" && $('#txtSubName').val().trim().length == 0) {
     

        navigator.notification.alert( $("#hdnEntrName").val() , "", "neo ecr", "Ok");

        return false;
    }
    if ($('#txtAddress').val() == "") {
       
        navigator.notification.alert( $("#hdnPleaseEnter").val() + ' ' + $('#hdnAddress').val() , "", "neo ecr", "Ok");

        return false;
        }
    if ($('#txtCity').val() == "") {
        

        navigator.notification.alert( $("#hdnPleaseFill").val() + ' ' + $('#hdnCity').val() , "", "neo ecr", "Ok");

        return false;
        }
    Save('');
});
function Save(upCheck) {
    window.localStorage.setItem('ctCheck', 1);
    Jsonobj = [];
    var sOrg = {};
    if ($('#hdnTransId').val() == "") {
        sOrg["TransId"] = "";
    }
    else {
        sOrg["TransId"] = $('#hdnTransId').val();
    }
    sOrg["CompanyId"] = $('#hdnOrgId').val();
    sOrg["SubOrgId"] = $('#hdnSubOrgId').val();
    sOrg["Code"] = $('#txtSubCode').val();
    sOrg["Name"] = $('#txtSubName').val();
    sOrg["Alias"] = $('#txtAlias').val();
    sOrg["Mobile"] = $('#txtMobile').val();
    sOrg["Address"] = $('#txtAddress').val();
    sOrg["CityId"] = $('#hdnCityId').val();
    sOrg["StoreMailId"] = $('#hdnStoreMailId').val();
    
    sOrg["Status"] = "1";
    if (upCheck != '') {
        if ($('#hdnScreenMode').val() == "Active")
            sOrg["Status"] = "1";
        else
            sOrg["Status"] = "2";
    }
    sOrg["VD"] = vd_;
    Jsonobj.push(sOrg);
    var ItemsData = new Object();
    ItemsData.Data = JSON.stringify(Jsonobj);
    var ComplexObject = new Object();
    ComplexObject.ItemsData = ItemsData;
    var data = JSON.stringify(ComplexObject);
    $.ajax({
        type: 'POST',
        url: CashierLite.Settings.SubOrgSave,
        data: data,
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function (resp) {

            window.localStorage.setItem('ctCheck', 1);

            resp = resp.SubOrgSaveResult;
            var result = resp.split('~');
             
            if (result[0] == "100000") {           
                if ($('#hdnScreenMode').val() == "Active") {

                   
                    navigator.notification.alert( result[1] + ' ' + $('#hdnRecActSucc').val()     , "", "neo ecr", "Ok");

                    $("#btnCancel").click();
                }
                else if ($('#hdnScreenMode').val() == "InActive") {

                    
                    navigator.notification.alert( result[1] + ' ' + $('#hdnRecInactSucc').val()    , "", "neo ecr", "Ok");

                    $("#btnCancel").click();
                }
                else { 
                    navigator.notification.alert(result[1] + ' ' + $('#hdnRecSaveSucc').val()   , "", "neo ecr", "Ok");

                    if ($('#hdnTransId').val() == "") {
                        $("#btnCreate").click();
                    }
                    else {
                        $("#btnCancel").click();
                    }
                }
                clearControls_Store();
                GetSummaryGridData('Summary');
            }
            if (result[0] == "100001") {
                
                navigator.notification.alert(result[1] + ' ' + $('#hdnCodeAlreadyExist').val()  , "", "neo ecr", "Ok");

            }
            if (result[0] == "1000020") {
                
                navigator.notification.alert($('#hdnStoresCount').val() , "", "neo ecr", "Ok");

            }
        },
        error: function (e) {
            
            navigator.notification.alert($('#hdnInvalData').val() , "", "neo ecr", "Ok");

        }
    });
    $('#PrimeInfo').addClass('open');
    $('#divPrimeInformation').attr('style', 'display:block');
};

function GetSummaryGridData(FilterType) {
    window.localStorage.setItem('ctCheck', 1);
    var Index = $('#hdnPageIndex').val();
    var MethodType = "";
    var FindGo = "";
    var QryType = "";
    if (FilterType == 'FindGo') {
        FindGo = $('#txtFindGo').val();
        Index = 1;

    }
    if (FilterType == 'Summary') {
        $('#txtFindGo').val('');
    }

    else {
        FindGo = $('#txtFindGo').val();
    }
    if (FilterType.indexOf('Index') != -1) {
        FilterType = FilterType.split('~')[0];
        MethodType = "next";
    }

    var HashVal = (window.localStorage.getItem("hashValues"));
    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');
    }
    var SummaryTable = '';
    Jsonobj = [];
    var SubOrg = {};
    SubOrg.SubOrgID = $('#hdnSubOrgId').val();
    var vd_ = window.localStorage.getItem("UUID");;

    SubOrg.VD = vd_;
    SubOrg.Index = Index;
    SubOrg.FilterType = FilterType;
    SubOrg.FindGoVal = FindGo;
    SubOrg.item_Id = "";

    Jsonobj.push(SubOrg);
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetAllStoresWithPaging,
        data: "SubOrg=" + JSON.stringify(Jsonobj),
        success: function (resp) {
            var result = resp;
            if (result.length > 0) {
                window.localStorage.setItem("CounterDefFilterGrid", JSON.stringify(result))
                $('#tblSummaryGrid >tbody').children().remove();

                for (var i = 0; i < result.length; i++) {
                    SummaryTable += "<tr onclick='DoubleClick(this)'><td style='display:none'>" + result[i].StoreId + "</td><td>" + result[i].Code + "</td><td>" + result[i].Name + "</td><td>" + result[i].status + "</td></tr>";
                }

                $('#tblSummaryGrid tbody').remove();
                SummaryTable += "<tbody>" + SummaryTable + "</tbody>";
                $('#tblSummaryGrid').append(SummaryTable);
                window.localStorage.setItem("CounterDefGrid", JSON.stringify(SummaryTable));
                //GetPaging1();
                $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());
            }
            else {

                $('#tblSummaryGrid tbody').remove();
                SummaryTable += "<tbody>" + SummaryTable + "</tbody>";
                $('#tblSummaryGrid').append(SummaryTable);
                if (MethodType == "next") {
                    var pageIndexCreate = parseFloat($('#hdnPageIndex').val()) - 1;
                    $('#hdnPageIndex').val(pageIndexCreate);
                }

            }
        },
        error: function (e) {
           
            navigator.notification.alert($('#hdnInvalData').val() , "", "neo ecr", "Ok");

        }

    });
}
function DoubleClick(tr) {
    window.localStorage.setItem('ctCheck', 1);
    var id = $(tr).find('td:first').html();
    var vd_ = window.localStorage.getItem("UUID");;

    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetAllStores,
        data: "ItemId=" + id + "&VD=" + vd_,
        success: function (resp) {

            if (resp.length != 0) {
                $('#hdnTransId').val(resp[0].StoreId);
                $('#txtSubCode').val(resp[0].Code);
                $("#txtSubName").val(resp[0].Name);
                $('#txtMobile').val(resp[0].Mobile);
                $("#txtAddress").val(resp[0].Address);
                $('#hdnCityId').val(resp[0].CityId);
                $('#txtCity').val(resp[0].City);
                $('#txtAlias').val(resp[0].Alias);

                $('#lblLastUpdate').css("display", "block");
                $('#trSBInnerRow li span#lblLastUpdate').html($('#trSBInnerRow li span#lblLastUpdate').html());
                $('#trSBInnerRow li span#lblLastUpdatedUser').html(resp[0].LastUpdateduser);
                $('#trSBInnerRow li span#lblLastUpdatedDate').html(resp[0].LastUpdateduserDate);
                $('#trSBInnerRow li span#lblMode').html($('#hdnView').val());
                if (resp[0].status != "Active") {
                    $("#btnCreate").attr('style', 'display:block;');
                    $("#ShowMenuModes").attr('style', 'display:block;');
                    $("#btnSave").attr('style', 'display:none;');
                    $("#btnEdit").attr('style', 'display:none;');
                    $("#btnDelete").attr('style', 'display:none;');
                    $("#btnCancel").attr('style', 'display:block;');
                    $("#SummaryGrid").attr('style', 'display:none;');
                    $("#NewPage").attr('style', 'display:block;');
                    $("#divStatusGroup").attr('style', 'display:block;');
                    $("#divNavBarRight").attr('style', 'display:none;');
                    $("#btnInActive").addClass("disabledHyperlink");
                    $("#btnActive").removeClass("disabledHyperlink");
                }
                else {
                    $("#btnInActive").removeClass("disabledHyperlink");
                    $("#btnActive").addClass("disabledHyperlink");
                    $("#btnCreate").attr('style', 'display:block;');
                    $("#ShowMenuModes").attr('style', 'display:block;');
                    $("#btnSave").attr('style', 'display:none;');
                    $("#btnEdit").attr('style', 'display:block;');
                    $("#btnDelete").attr('style', 'display:none;');
                    $("#btnCancel").attr('style', 'display:block;');
                    $("#SummaryGrid").attr('style', 'display:none;');
                    $("#NewPage").attr('style', 'display:block;');
                    $("#divStatusGroup").attr('style', 'display:block;');
                    $("#divNavBarRight").attr('style', 'display:none;');
                }             
                $('#inp').prop('disabled', true);
            }
        },
        error: function (e) {
            
            navigator.notification.alert($('#hdnInvalData').val() , "", "neo ecr", "Ok");

        }
    });
    $('#SummaryGrid').css("display", "none");
    $("#ContentDet").css('display', 'block');

    $('#PrimeInfo').addClass('open');
    $('#divPrimeInformation').attr('style', 'display:block');
    EnableFormfileds(true);
}
$('#txtFindGo').keyup(function () {
    window.localStorage.setItem('ctCheck', 1);
    result = [];
    result = JSON.parse((window.localStorage.getItem("CounterDefFilterGrid")));
    if ($('#txtFindGo').val().trim() == "" || result.length <= 0) {

        GetSummaryGridData('Summary');
        return false;
    }
    else {

        GetSummaryGridData('FindGo');
    }
})
$('#btnAll').click(function () {

    $("#btnCancel").click();

    GetSummaryGridData('Summary');
});
function getResources(langCode, pageCode) {
    window.localStorage.setItem('ctCheck', 1);
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.getResource,
        data: "langCode=" + langCode + "&pageCode=" + pageCode,
        success: function (resp) {
            setLabels(resp);
        }
    });
}

function setLabels(labelData) {
    window.localStorage.setItem('ctCheck', 1);

    jQuery("label[for='lblStore'").html(labelData["subOrganization"]);
    jQuery("label[for='lblPrimeInformation'").html(labelData["primeInformation"]);
    jQuery("label[for='lblSubCode'").html(labelData["code"]);
    jQuery("label[for='lblSubName'").html(labelData["name"]);
    jQuery("label[for='lblAlias'").html(labelData["Alias"]);
    jQuery("label[for='lblMobile'").html(labelData["mobile"]);
    jQuery("label[for='lblAddress'").html(labelData["Address"]);
    jQuery("label[for='lblCity'").html(labelData["city"]);
    
    jQuery("label[for='lblType'").html(labelData["type"]);
    jQuery("label[for='lblCode'").html(labelData["code"]);
    jQuery("label[for='lblName'").html(labelData["name"]);
    jQuery("label[for='lblProcessStatus'").html(labelData["status"]);

    $('#txtSubCode').attr('placeholder', labelData["PleaseEnter"] + ' ' + labelData["code"]);
    $('#txtSubName').attr('placeholder', labelData["PleaseEnter"] + ' ' + labelData["name"]);
    $('#txtAlias').attr('placeholder', labelData["PleaseEnter"] + ' ' + labelData["Alias"]);
    $('#txtMobile').attr('placeholder', labelData["PleaseEnter"] + ' ' + labelData["mobile"]);
    $('#txtAddress').attr('placeholder', labelData["PleaseEnter"] + ' ' + labelData["Address"]);
    $('#txtCity').attr('placeholder', labelData["plzFill"] + " " + labelData["city"]);
    
    $('#hdnRecSaveSucc').val(labelData["RecordsavedSucc"]);
    $('#hdnRecActSucc').val(labelData["alert6"]);
    $('#hdnRecInactSucc').val(labelData["alert7"]);
    $('#hdnCodeAlreadyExist').val(labelData["alert9"]);
    $('#hdnEntrCode').val(labelData["alert10"]);
    $('#hdnEntrName').val(labelData["alert11"]);
    $('#hdnAddress').val(labelData["Address"]);
    $('#hdnCity').val(labelData["city"]);
    $('#hdnPleaseEnter').val(labelData["PleaseEnter"]);
    $('#hdnPleaseFill').val(labelData["plzFill"]);
    

    jQuery("label[for='lblSCPopPwd'").html(labelData["pSettings"]);
    jQuery("label[for='lblLanguage'").html(labelData["lang"]);
    jQuery("label[for='lblDateFormat'").html(labelData["format"]);
    jQuery("label[for='lblPopOk'").html(labelData["ok"]);
    jQuery("label[for='lblPopClose'").html(labelData["cancel"]);
    $('#txtFindGo').attr('placeholder', labelData["searchHere"]);
    jQuery("label[for='lblSettings'").html(labelData["settings"]);
    jQuery("label[for='lblThemes'").html(labelData["themes"]);
    jQuery("label[for='lblTheme1'").html(labelData["theme1"]);
    jQuery("label[for='lblTheme2'").html(labelData["theme2"]);
    jQuery("label[for='lblTheme3'").html(labelData["theme3"]);
    jQuery("label[for='lblTheme4'").html(labelData["theme4"]);
    jQuery("label[for='lblChangePassword'").html(labelData["changePassword"]);
    jQuery("label[for='lblCreate'").html(labelData["create"]);
    jQuery("label[for='lblSave'").html(labelData["save"]);
    jQuery("label[for='lblEdit'").html(labelData["edit"]);
    jQuery("label[for='lblDelete'").html(labelData["delete"]);
    jQuery("label[for='lblCancel'").html(labelData["cancel"]);
    jQuery("label[for='lblStatus'").html(labelData["status"]);
    jQuery("label[for='lblActive'").html(labelData["active"]);
    jQuery("label[for='lblInactive'").html(labelData["inactive"]);
    jQuery("label[for='lblOk'").html(labelData["ok"]);
    $('#hdnChPwdSaveSucc').val(labelData["alert8"]);
    $('#hdnStoresCount').val(labelData["StoresCount"]);
}
$("#txtMobile").bind('cut copy paste', function (e) {
    e.preventDefault();
});
$("#txtMobile").keypress(function (event) {
    if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1)) {
            return false;
        }
        event.preventDefault();
    }
    if (this.value.indexOf(".") > -1 && (this.value.split('.')[1].length > 1)) {
        return false;
        event.preventDefault()
    }
});

$("#btnCancel").click(function () {
    window.localStorage.setItem('ctCheck', 1);    
    clearControls_Store();
    $('#lblLastUpdate').css("display", "none");    
    $('#trSBInnerRow li span#lblLastUpdatedUser').html("");
    $('#trSBInnerRow li span#lblLastUpdatedDate').html("");
    $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());
    $("#btnCreate").attr('style', 'display:block;');
    $("#ShowMenuModes").attr('style', 'display:none;');    
    $("#NewPage").attr('style', 'display:none;');
    $("#divNavBarRight").attr('style', 'display:block;');
    $('#SummaryGrid').css("display", "block");
    $("#ContentDet").css('display', 'none');
    GetSummaryGridData('Summary');
});
$('#btnEdit').click(function () {
    window.localStorage.setItem('ctCheck', 1);
    Edit();
    EnableFormfileds(false);
    $('#txtSubCode').prop("disabled", true);
});
function Edit() {
    window.localStorage.setItem('ctCheck', 1);
    $('#trSBInnerRow li span#lblMode').html($('#hdnEdit').val());
    $("#btnCreate").attr('style', 'display:none;');
    $("#ShowMenuModes").attr('style', 'display:block;');
    $("#btnSave").attr('style', 'display:block;');
    $("#btnEdit").attr('style', 'display:none;');
    $("#btnDelete").attr('style', 'display:none;');
    $("#btnDelete").attr('style', 'border-left:1px solid #cac8c8;');
    $("#btnCancel").attr('style', 'display:block;');
    $("#divStatusGroup").attr('style', 'display:none;');
    $("#divNavBarRight").attr('style', 'display:none;');
    $('#PrimeInfo').addClass('open');
    $('#divPrimeInformation').attr('style', 'display:block');
}
function clearControls_Store() {
    window.localStorage.setItem('ctCheck', 1);
    $('#hdnTransId').val("");
    $('#hdnCityId').val("");
    $('#txtSubCode').val("");
    $('#txtSubName').val("");
    $("#txtAlias").val("");
    $('#txtMobile').val("");
    $('#txtAddress').val("");
    $("#txtCity").val("");
    $("#hdnScreenMode").val("");    
}
function EnableFormfileds(Status) {
    $('#txtSubCode').prop("disabled", Status);
    $('#txtSubName').prop("disabled", Status);
    $('#txtAlias').prop("disabled", Status);
    $('#txtMobile').prop("disabled", Status);
    $('#txtAddress').prop("disabled", Status);
    $('#txtCity').prop("disabled", Status);
}
function prevItem() {
    var pageIndexCreate = parseFloat($('#hdnPageIndex').val()) - 1;
    if (parseFloat(pageIndexCreate) == 0) {
        pageIndexCreate = '1';
    }
    $('#hdnPageIndex').val(pageIndexCreate);
    var FilterType = $("#hdnFilterType").val();
    GetSummaryGridData(FilterType);
    window.localStorage.setItem('ctCheck', 1);
}
function nextItem() {
    window.localStorage.setItem('ctCheck', 1);
    var pageIndexCreate = parseFloat($('#hdnPageIndex').val()) + 1;

    $('#hdnPageIndex').val(pageIndexCreate);
    var FilterType = $('#hdnFilterType').val();
    GetSummaryGridData(FilterType + '~Index');
}