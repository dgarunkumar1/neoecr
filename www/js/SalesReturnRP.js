﻿function VD() {
    window.localStorage.setItem('ctCheck', 1);
    var vd_ = window.localStorage.getItem("UUID");
    //var vdSplit_ = window.location.pathname.split('/');
    //if (vdSplit_.length > 2) {
    //    vd_ = vdSplit_[1];
    //}
    return vd_;
}
var graphData;
function floorFigure(figure, decimals) {
    window.localStorage.setItem('ctCheck', 1);
    if (!decimals) decimals = 2;
    var d = Math.pow(10, decimals);
    return (parseInt(figure * d) / d).toFixed(decimals);
};

function InvMail() {
    $('#txtBccMailId').val('');
    $('#txtBody').val('');


    window.localStorage.setItem('ctCheck', 1);
    $('#ModalINVMail').modal('show');
    //   tenderAddpartial(TenderID, 1, '');
    $('#txtMailId').val('');
    //  $('#FileFormat').val('0');
    $('#txtMailId').focus();
    $('#FileFormat').attr("readonly", "true");
}
function SendINVMail(MailId, BccMailId, Format, Body) {
    window.localStorage.setItem('ctCheck', 1);
    if ($('#txtMailId').val() == "") {
        navigator.notification.alert($('#hdnEnterMail').val(), "", "neo ecr", "Ok");
    }
    else {
        var options = { dimBackground: true };
        SpinnerPlugin.activityStart("Please wait...", options);
        $.ajax({
            type: 'GET',
           url: CashierLite.Settings.SendMailArabic,
            data: "language=" + HashValues[7] + "&PageId=" + window.localStorage.getItem('RPPAGECODE') + "&subOrg=" +  HashValues[1]  + "&frmDate=" + window.localStorage.getItem('RPfrmDate') + "&toDate=" + window.localStorage.getItem('RPtoDate') + ' 23:59:59' + "&DateFormate=" + $('#hdnDateFromate').val() + "&VD=" + VD() + "&MailId=" + MailId + "&Format=" + Format + "&BCMailId=" + BccMailId + "&Body=" + Body + "",

            success: function (resp) {
                SpinnerPlugin.activityStop();
                if (resp == '1000') {
                    navigator.notification.alert($('#hdnMailSuccess').val(), "", "neo ecr", "Ok");
                    $('#ModalINVMail').modal('hide');
                }
                else {
                    navigator.notification.alert($('#hdnMailnotSuccess').val(), "", "neo ecr", "Ok");
                    $('#ModalINVMail').modal('hide');
                }
            },
            error: function (e) {
                SpinnerPlugin.activityStop();
                navigator.notification.alert($('#hdnInvalidData').val(), "", "neo ecr", "Ok");
                $('#ModalINVMail').modal('hide');
            }
        });
    }
}

function arrangeSalesReturnsData(resp) {
    window.localStorage.setItem('ctCheck', 1);
    graphData = resp;
    var result = resp;
    var Primaryid = "";
    //$('#tbldiv').append("<thead><tr><th style='width:10px' ></th><th>Return No.</th><th>Return Date</th><th>Ref. Invoice No.</th><th>Ref. Invoice Date</th><th>Till</th><th>Base Total</th><th>Tax Total</th><th>Net Total</th></tr></thead>");
    //$('#tbldiv').append("<tbody>");
    $('#tbldiv thead tr').remove();
    $('#tbldiv tbody tr').remove();
    $('#tbldiv thead').append("<tr><th>" + $('#hdnReturnNo').val() + "</th><th>" + $('#hdnReturnDt').val() + "</th><th>" + $('#hdnRefInvoiceNo').val() + "</th><th>" + $('#hdnRefInvoiceDt').val() + "</th><th>" + $('#hdnTill').val() + "</th><th>" + $('#hdnBaseTot').val() + "</th><th style='display:none'>" + $('#hdnTaxTot').val() + "</th><th>" + $('#hdnTaxAmount').val() + "</th><th>" + $('#hdnNetTot').val() + "</th><th></th></tr>");
    var cc_ = '';
    if (HashValues[7] == 'ar-SA') {
      
        for (var i = 0; i < result.length; i++) {

            var val_ = parseFloat(result[i].C17).toFixed(2);
            cc_ = cc_ + '<tr id="dt_Parrent' + i + '" ><td onclick="treeclick(this);">' + result[i].C3 + '</td><td>' + result[i].C4 + '&nbsp' + '</td><td>' + result[i].C6 + '</td><td>' + result[i].C7 + '&nbsp' + '</td><td>' + result[i].C8 + '</td><td style="text-align:left;">' + val_ + '&nbsp' + '</td><td style="text-align:left;display:none">' + result[i].C11 + '&nbsp' + '</td><td style="text-align:left">' + result[i].C14 + '&nbsp' + '</td><td style="text-align:left">' + result[i].C12 + '&nbsp' + '</td><td style="text-align:left"><input type="button" class="btn btn-primary"  onclick = "printInvoiceSRP(\'' + result[i].C3 + '\')"  value=' + $('#hdnPrint').val() + ' /> </td></tr>';
            cc_ = cc_ + '<tr style="display:none" id="dt_Parrent' + i + '_0" data-tt-parent="childPaging"><td colspan="11" ><table id="tblChild' + i + '" data-tt-parent="root' + i + '" class="table table-bordered" >';
            cc_ = cc_ + "<tr><th>" + $('#hdnItem').val() + "</th><th>" + $('#POSSearchTitleItemName').val() + "</th><th>" + $('#hdnUOM').val() + "</th><th>" + $('#hdnQty').val() + "</th><th>" + $('#hdnPrice').val() + "</th><th>" + $('#hdnDisc').val() + "</th><th>" + $('#hdnItemAmount').val() + "</th><th>" + $('#hdnReason').val() + "</th></tr>";
            for (var j = 0; j < result[i].objdblclickParams.length; j++) {
                if (resp[i].C2 == result[i].objdblclickParams[j].ReturnId) {
                    for (var k = 0; k < result[i].objdblclickParams[j].LISTD.length; k++) {
                        cc_ = cc_ + '<tr><td style="width:12%">' + result[i].objdblclickParams[j].LISTD[k].ITEM_CODE + '</td><td style="width:33%;text-align:right;">' + result[i].objdblclickParams[j].LISTD[k].ITEM_NAME.split('/')[1] + ' / ' + result[i].objdblclickParams[j].LISTD[k].ITEM_NAME.split('/')[0] + '</td><td style="width:6%">' + result[i].objdblclickParams[j].LISTD[k].UOM_CODE.split('/')[0] + '&nbsp' + '</td><td style="text-align:left;width:13%;">' + result[i].objdblclickParams[j].LISTD[k].RETURN_QTY + '&nbsp' + '</td><td style="text-align:left;width:13%;">' + result[i].objdblclickParams[j].LISTD[k].RETURN_ITEMS_PRICE + '&nbsp' + '</td><td style="text-align:left;width:13%;">' + result[i].objdblclickParams[j].LISTD[k].DiscountPer + '&nbsp' + '</td><td style="text-align:left;width:13%;">' + result[i].objdblclickParams[j].LISTD[k].Total_Amount + '&nbsp' + '</td><td style="width:13%;">' + result[i].objdblclickParams[j].LISTD[k].Return_Reason + '</td></tr>';
                       
                    }
                }
            }
            cc_ = cc_ + '</table></td></tr>';

        }
    }
    else
    {
       // $('#IMG1').attr("src", "images/mmlogo-inner1.png");
        for (var i = 0; i < result.length; i++) {

            var val_ = parseFloat(result[i].C17).toFixed(2);

            cc_ = cc_ + '<tr id="dt_Parrent' + i + '" ><td onclick="treeclick(this);">' + result[i].C3 + '</td><td>' + result[i].C4 + '&nbsp' + '</td><td>' + result[i].C6 + '</td><td>' + result[i].C7 + '&nbsp' + '</td><td>' + result[i].C8 + '</td><td style="text-align:right;">' + val_ + '&nbsp' + '</td><td style="text-align:right;display:none">' + result[i].C11 + '&nbsp' + '</td><td style="text-align:right">' + result[i].C14 + '&nbsp' + '</td><td style="text-align:right">' + result[i].C12 + '&nbsp' + '</td><td style="text-align:right"><input type="button" class="btn btn-primary"  onclick = "printInvoiceSRP(\'' + result[i].C3 + '\')"value=' + $('#hdnPrint').val() + ' /> </td></tr>';
            cc_ = cc_ + '<tr style="display:none" id="dt_Parrent' + i + '_0" data-tt-parent="childPaging"><td colspan="11" ><table id="tblChild' + i + '" data-tt-parent="root' + i + '" class="table table-bordered" >';
            cc_ = cc_ + "<tr><th>" + $('#hdnItem').val() + "</th><th>" + $('#POSSearchTitleItemName').val() + "</th><th>" + $('#hdnUOM').val() + "</th><th>" + $('#hdnQty').val() + "</th><th>" + $('#hdnPrice').val() + "</th><th>" + $('#hdnDisc').val() + "</th><th>" + $('#hdnItemAmount').val() + "</th><th>" + $('#hdnReason').val() + "</th></tr>";
            for (var j = 0; j < result[i].objdblclickParams.length; j++) {
                if (resp[i].C2 == result[i].objdblclickParams[j].ReturnId) {
                    for (var k = 0; k < result[i].objdblclickParams[j].LISTD.length; k++) {
                        cc_ = cc_ + '<tr><td style="width:12%">' + result[i].objdblclickParams[j].LISTD[k].ITEM_CODE + '</td><td style="width:33%;text-align:left;">' + result[i].objdblclickParams[j].LISTD[k].ITEM_NAME + '</td><td style="width:6%">' + result[i].objdblclickParams[j].LISTD[k].UOM_CODE.split('/')[0] + '&nbsp' + '</td><td style="text-align:right;width:13%;">' + result[i].objdblclickParams[j].LISTD[k].RETURN_QTY + '&nbsp' + '</td><td style="text-align:right;width:13%;">' + result[i].objdblclickParams[j].LISTD[k].RETURN_ITEMS_PRICE + '&nbsp' + '</td><td style="text-align:right;width:13%;">' + result[i].objdblclickParams[j].LISTD[k].DiscountPer + '&nbsp' + '</td><td style="text-align:right;width:13%;">' + result[i].objdblclickParams[j].LISTD[k].Total_Amount + '&nbsp' + '</td><td style="width:13%;">' + result[i].objdblclickParams[j].LISTD[k].Return_Reason + '</td></tr>';
                        //<td>' + result[i].oblist[j].C4 + '</td>
                    }
                }
            }
            cc_ = cc_ + '</table></td></tr>';

        }
    }
 //   cc_ = cc_ + "</tbody>";
   $("#tbldiv tbody").append(cc_);
    //$('#tbldiv').append("</tbody>");
  //  $('#tbldiv').treetable();
}
function printUSBCreditNotetd(invoiceId) {
    window.localStorage.setItem('ctCheck', 1);
    var PrinterName_ = "InnerPrinter";
    if (window.localStorage.getItem("PrinterName") != null) {
        PrinterName_ = window.localStorage.getItem("PrinterName");
    }
    // BTPrinter.connect(function (data) {
    // }, function (err) {
    // }, PrinterName_)
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.printUSBSalesReturn,
       // data: "InvoiceId=" + invoiceId + "&VD=" + window.localStorage.getItem("UUID"),
       data: "ReturnId=" + invoiceId + "&VD=" + window.localStorage.getItem("UUID"),
        success: function (resp) 
        { 
                var dataMain = resp; 
                var qrData = "";
                qrData = dataMain.split('Š')[1];
                dataMain = dataMain.split('Š')[0];
                // try {
                //     if (PrinterName_ != "InnerPrinter") {
                //         BTPrinter.printText(function (data) {
                //             //alert('Printed Data')
                //         }, function (err) {
                //             //alert(err)
                //         }, dataMain);
                //         //printQrCodeRP(invoiceId);
                //         dataTT(qrData);
                //         BTPrinter.printPOSCommand(function (data) {
                //         }, function (err) {
                //         }, "0C");
                //         BTPrinter.printPOSCommand(function (data) {
                //         }, function (err) {
                //         }, "0A");
                //         BTPrinter.printPOSCommand(function () { }, function () { }, "1D");//FEED PAPER AND CUT

                //     } 
                //     if (PrinterName_ == "InnerPrinter") {
                    navigator.sunmiInnerPrinter.printerInit();
                    navigator.sunmiInnerPrinter.printerStatusStartListener();
                    navigator.sunmiInnerPrinter.printOriginalText(dataMain); 
                    navigator.sunmiInnerPrinter.setAlignment(1);
                    navigator.sunmiInnerPrinter.printQRCode(qrData,10,10); 
                    navigator.sunmiInnerPrinter.hasPrinter();
                    window.localStorage.setItem('PrintCut','1');
                    navigator.sunmiInnerPrinter.printerStatusStopListener();
                        //printQrCodeRP(invoiceId);
                //     } 
                //     BTPrinter.disconnect(function (data) {
                //     }, function (err) {
                //     }, PrinterName_); 
                // } catch (a) {

                // } 
            
        }, error: function (e) {
            alert(e);
        }
    });
}
PaperCutCheck();
function PaperCutCheck()
 {
        try
        {
            if(window.localStorage.getItem('PrintCut')=='1')
            {
                navigator.sunmiInnerPrinter.printOriginalText("                      "); 
                navigator.sunmiInnerPrinter.printOriginalText("                      "); 
                navigator.sunmiInnerPrinter.printOriginalText("                      "); 
                navigator.sunmiInnerPrinter.printOriginalText("                      "); 
                window.localStorage.setItem('PrintCut','0');
                navigator.sunmiInnerPrinter.sendRAWData("HVZCAA=="); 
            }
        }
        catch(a)
        {

        }
        setTimeout('PaperCutCheck()', 1000);
}
function dataTT(invData) {
    BTPrinter.printBase64(function (data) {
    }, function (err) {
    }, invData, '1');//base64 string, align 
}
function printQrCodeRP(invoiceId) {
    window.localStorage.setItem('ctCheck', 1);
    const justify_center = '\x1B\x61\x01';
    const justify_left = '\x1B\x61\x00';
    const qr_model = '\x32';          // 31 or 32
    const qr_size = '\x08';          // size
    const qr_eclevel = '\x33';          // error correction level (30, 31, 32, 33 - higher)
    const qr_data = invoiceId;
    const qr_pL = String.fromCharCode((qr_data.length + 3) % 256);
    const qr_pH = String.fromCharCode((qr_data.length + 3) / 256);

    BTPrinter.printText(data => {
        this.msg = '';
    }, err => {
        this.msg = '';
    }, justify_center +
    '\x1D\x28\x6B\x04\x00\x31\x41' + qr_model + '\x00' +        // Select the model
    '\x1D\x28\x6B\x03\x00\x31\x43' + qr_size +                  // Size of the model
    '\x1D\x28\x6B\x03\x00\x31\x45' + qr_eclevel +               // Set n for error correction
    '\x1D\x28\x6B' + qr_pL + qr_pH + '\x31\x50\x30' + qr_data + // Store data 
    '\x1D\x28\x6B\x03\x00\x31\x51\x30' +                        // Print
    '\n\n\n' +
        justify_left);
}
function printInvoiceSRP(invoiceId) {
    window.localStorage.setItem('ctCheck', 1);
    printUSBCreditNotetd(invoiceId)
}
//function printUSBCreditNotetd(ReturnId) {
//    window.localStorage.setItem('ctCheck', 1);
//    $.ajax({
//        type: 'GET',
//        url: CashierLite.Settings.printUSBSalesReturn,
//        data: "ReturnId=" + ReturnId + "&VD=" + window.localStorage.getItem("UUID"),
//        success: function (resp) {
//            try {
//                //window.cordova.plugins.generic.printer.usb.scan(
//                //    function (result) {
//                //        var printerName = '';
//                //        //printerName = printer;
//                var data = resp;
//                //        //var printerName = 'GP-80';
//                //        var printerName = result[0]["productName"];
//                //        window.cordova.plugins.generic.printer.usb.print(printerName, data);
//                //    }, function (error) {
//                //        alert('Printer: connect fail: ' + error);
//                //    }
//                //);

//                BTPrinter.list(function (data) {

//                }, function (err) {
//                    //alert("Error");

//                })
//                BTPrinter.connect(function (data) {
//                    //alert("Success");
//                    //alert(data)
//                }, function (err) {
//                    //alert("Error");
//                    //alert(err)
//                }, "InnerPrinter")

//                BTPrinter.printText(function (data) {
//                    //alert("Success");
//                    //alert(data)
//                }, function (err) {
//                    //alert("Error");
//                    //alert(err)
//                }, data)

//                //BTPrinter.print(function (data) {
//                //    alert("Success");
//                //    alert(data)
//                //}, function (err) {
//                //    alert("Error");
//                //    alert(err)
//                //}, "Base64 String of Image")

//                BTPrinter.printPOSCommand(function (data) {
//                    //alert("Success");
//                    //alert(data)
//                }, function (err) {
//                    //alert("Error");
//                    //alert(err)
//                }, "0C")


//            } catch (a) {

//            }
//        }, error: function (e) {

//        }
//    });
//}
function treeclick(data) {
    window.localStorage.setItem('ctCheck', 1);
       var d_ = data.parentNode;
    if ($('#' + d_.id + '_0')[0].style.display != '')
        $('#' + d_.id + '_0')[0].style.display = '';
    else
        $('#' + d_.id + '_0')[0].style.display = 'none';
}

function SalesReturnsData() {

    window.localStorage.setItem('ctCheck', 1);
    var HashVal = (window.localStorage.getItem("hashValues"));

    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');
        $('#hdnDateFromate').val(HashValues[2])
        if (HashValues[7] == 'ar-SA') {
            $('html,body').append('<link rel="stylesheet" href="Styles/css/bootstrap-rtl.css" type="text/css" />');
            $('html,body').append('<link rel="stylesheet" href="Styles/css/bootstrap-flipped.css" type="text/css" />');
            $('head').append('<script src="scripts/bootstrap-dialog-SA.min.js"></script>');
        }

    }

    theamLoad();
    $('#hdnPaging').val('1');
    var btnExp = document.getElementById("btnExp");
    btnExp.style.display = "block";
 
   
        var SubOrgId = '';
        var HashVal = (window.localStorage.getItem("hashValues"));
        var HashValues;
        if (HashVal != null && HashVal != '') {
            HashValues = HashVal.split('~');
         //   getResources(HashValues[7], "POSSalseReturnReport");
            SubOrgId = HashValues[1];
            $('#hdnDateFromate').val(HashValues[2]);
         //         $('#hdnPaging').val(HashValues[19]);
        }
        var PageIndex = $('#hdnPaging').val();
        var PageCount = $('#txt_pageCount').val();

        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.GetReport_V01,
            data: "PageId=" + window.localStorage.getItem('RPPAGECODE') + "&subOrg=" + SubOrgId + "&frmDate=" + window.localStorage.getItem('RPfrmDate') + "&toDate=" + window.localStorage.getItem('RPtoDate') + ' 23:59:59' + "&refNo=" + window.localStorage.getItem('RPrefNo') + "&DateFormate=" + $('#hdnDateFromate').val() + "&VD=" + VD() + "&PageIndex=" + PageIndex + "&PageCount=" + PageCount +"&language=" + HashValues[7] + "",
            success: function (resp) {
                graphData = resp;
                if (resp != '') {
                    jQuery("label[for='lbl_invPageOf'").html(resp[0].C15);
                   
                }
                getResources(HashValues[7], "POSSalseReturnReport", resp, 1);
                $('#hdnTotalPagesCount').val(resp[0].C16);
                $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());
                if ($('#hdnTotalPagesCount').val() != $('#hdnPaging').val()) {
                    $('#li_PRPrev i').removeAttr('style', 'color:#919598 !important');
                    $('#li_PRPrev').attr('onclick', 'nextPRInvl()');
                    $('#li_PRLast i').removeAttr('style', 'color:#919598 !important');
                    $('#li_PRLast').attr('onclick', 'nextPRLastInvl()');
                }
                else {
                    $('#li_PRPrev i').attr('style', 'color:#919598 !important');
                    $('#li_PRPrev').attr('onclick', 'return false');
                    $('#li_PRLast').attr('onclick', 'return false');
                    $('#li_PRLast i').attr('style', 'color:#919598 !important');
                }
            }

        });
       
    }

function theamLoad() {
    window.localStorage.setItem('ctCheck', 1);
    if (window.localStorage.getItem("Theam") != null)
        $('head').append('<link rel="stylesheet" href=' + window.localStorage.getItem("Theam") + ' type="text/css" />');
}
if (window.localStorage.getItem("Theam").split('-')[3] == '1.css') {
    $('#IMG1').attr("src", "images/mmlogo-inner2.png");
} else {
    $('#IMG1').attr("src", "images/mmlogo-inner1.png");
}
function print() {
    window.localStorage.setItem('ctCheck', 1);
    var btnCol = document.getElementById("btnCol");
    btnCol.style.display = "block";

    var btnExp = document.getElementById("btnExp");
    btnExp.style.display = "none";
    $('#hdnScreenmode').val("print");
    $('#tbldiv').html("");
    GetUnpaging();
}
$('#btnCol').click(function () {
    window.localStorage.setItem('ctCheck', 1);
    var btnCol = document.getElementById("btnCol");
    btnCol.style.display = "none";

    var btnExp = document.getElementById("btnExp");
    btnExp.style.display = "block";


    var $el = $('#tbldiv tbody>tr');

    $el.addClass("tt-hide");

    for (var i = 0; i < $('#tbldiv tbody>tr').length; i++) {
        var tr = $('#tbldiv tbody>tr[id="dt_Parrent' + i + '"]');
        //var tr = $(table).find("tr[id='dt_Parrent" + i + "']");
        //var tdid = $($el[i]).find("td[id='dt_Parrent" + i + "']");
        $(tr).removeClass("tt-hide");
        $('#root_' + i + '').html("+");
    }

    $('#dt_Parrent1').removeClass("tt-hide");
    //GetPaging();

});
$('#btnExp').click(function () {

    window.localStorage.setItem('ctCheck', 1);
    var btnCol = document.getElementById("btnCol");
    btnCol.style.display = "block";

    var btnExp = document.getElementById("btnExp");
    btnExp.style.display = "none";

    $('#tbldiv').html("");
    $('#hdnScreenmode').val("");
    GetUnpaging();

});
function exportexcel() {
    window.localStorage.setItem('ctCheck', 1);
    var btnCol = document.getElementById("btnCol");
    btnCol.style.display = "block";

    var btnExp = document.getElementById("btnExp");
    btnExp.style.display = "none";
    $('#hdnScreenmode').val("excel");
    $('#tbldiv').html("");
    GetUnpaging();
    //$("#div_grid").table2excel({
    //    name: "Table2Excel",
    //    filename: "DailyCollectionsByTenderType",
    //    fileext: ".xls"
    //});


}
function loadGrid() {
    window.localStorage.setItem('ctCheck', 1);
    $('#li-Excel i').removeAttr('style', 'color:#919598 !important');
    $('#li-Excel').attr('onclick', 'GetDowloadFile()');
    $('#li-Refresh i').removeAttr('style', 'color:#919598 !important');
    $('#li-Refresh').attr('onclick', 'SalesReturnsData()');
    $('#li_expand i').removeAttr('style', 'color:#919598 !important');
    $('#li_expand').attr('onclick', 'loadGridExtend()');
    $('#li_compress i').removeAttr('style', 'color:#919598 !important');
    $('#li_compress').attr('onclick', 'loadGridCompress()');
    $('#li_Print i').removeAttr('style', 'color:#919598 !important');
    $('#li_Print').attr('onclick', 'SRRPrint()');
    $('#txt_PRPPageIndex').removeAttr('disabled', 'true');
    $('#btnPage').removeAttr('disabled', 'true');
    $('#li_PRFirst i').removeAttr('style', 'color:#919598 !important');
    $('#li_PRFirst').attr('onclick', 'prevPRFirstInvl()');
    $('#li_PRNext i').removeAttr('style', 'color:#919598 !important');
    $('#li_PRNext').attr('onclick', 'prevPRInvl()');
    $('#li_PRPrev i').removeAttr('style', 'color:#919598 !important');
    $('#li_PRPrev').attr('onclick', 'nextPRInvl()');
    $('#li_PRLast i').removeAttr('style', 'color:#919598 !important');
    $('#li_PRLast').attr('onclick', 'nextPRLastInvl()');
    $('#lbl_invPageOf').removeAttr('style', 'color:#919598 !important');
    $('#lbl_page').removeAttr('style', 'color:#919598 !important');

      var elemGrid = document.getElementById("div_grid");
    elemGrid.style.display = "block";

    var elemGraph = document.getElementById("div_graph");
    elemGraph.style.display = "none";
}
function GetUnpaging() {
    window.localStorage.setItem('ctCheck', 1);
    var SubOrgId = '';
    var HashVal = (window.localStorage.getItem("hashValues"));
    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');
        SubOrgId = HashValues[1];
        $('#hdnDateFromate').val(HashValues[2]);
        $('#hdnPaging').val(HashValues[19]);
    }
    if (window.localStorage.getItem('RPfrmDate') != null) {
        $('#INVLISlblDate').text('(' + ConvertDateAndReturnDate($('#hdnDateFromate').val(), window.localStorage.getItem('RPfrmDate')) + ' to ' + ConvertDateAndReturnDate($('#hdnDateFromate').val(), window.localStorage.getItem('RPtoDate')) + ')');
    }
    var FromDate = (window.localStorage.getItem("RPfrmDate"));
    var ToDate = (window.localStorage.getItem("RPtoDate")) + ' 23:59:59';
    var RefNo = (window.localStorage.getItem("RPrefNo"));
    var PageCode = (window.localStorage.getItem("RPPAGECODE"));
    var grid = "";
    
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetReport,
        data: "PageId=" + PageCode + "&subOrg=" + SubOrgId + "&frmDate=" + FromDate + "&toDate=" + ToDate + "&refNo=" + RefNo + "&DateFormate=" + $('#hdnDateFromate').val() + "&VD="+VD(),
        success: function (resp) {
            arrangeSalesReturnsData(resp)
           
            var $el = $('#tbldiv tbody>tr.tt-hide');
            $el.removeClass("tt-hide");

            for (var i = 0; i < $el.length; i++) {
                $('#root_' + i + '').html("-");
            }
            $('#DivPager').css("display", "none");
            if ($('#hdnScreenmode').val() == "print") {
                var divContents = document.getElementById("div_grid").innerHTML;
                var printWindow = window.open('', '', 'height=200,width=450');
                printWindow.document.write('<html><head><title>DailyCollectionsSummaryByTenderTypeReport</title>');
                printWindow.document.write('</head><body >');
                printWindow.document.write(divContents);
                printWindow.document.write('</body></html>');
                printWindow.document.close();
                printWindow.print();
            }
            if ($('#hdnScreenmode').val() == "excel") {
                var data_type = 'data:application/vnd.ms-excel';
                var table_div = document.getElementById('tbldiv');
                var table_html = table_div.outerHTML.replace(/ /g, '%20');
                var a = document.createElement('a');
                a.href = data_type + ', ' + table_html;
                a.download = 'Sales-Return' + '.xls';
                a.click();
            }
        }
    });
  
}

function ConvertDateAndReturnDate(formates, dateText) {
    window.localStorage.setItem('ctCheck', 1);
    var hdnformat = formates;
    var txtMainDate = '';
    var splitc;
    if (hdnformat == 'd/m/Y' || hdnformat == 'm/d/Y' || hdnformat == 'Y/d/m' || hdnformat == 'Y/m/d' || hdnformat == 'M/d/Y' || hdnformat == 'd/M/Y') {
        splitc = '/';
    }
    else if (hdnformat == 'd-m-Y' || hdnformat == 'm-d-Y' || hdnformat == 'Y-d-m' || hdnformat == 'Y-m-d' || hdnformat == 'M-d-Y' || hdnformat == 'd-M-Y') {
        splitc = '-';
    }
    else if (hdnformat == 'd,m,Y' || hdnformat == 'm,d,Y' || hdnformat == 'Y,d,m' || hdnformat == 'Y,m,d' || hdnformat == 'M,d,Y' || hdnformat == 'd,M,Y') {

        splitc = ',';
    }
    else if (hdnformat == 'd m Y' || hdnformat == 'm d Y' || hdnformat == 'Y d m' || hdnformat == 'Y m d' || hdnformat == 'M d Y' || hdnformat == 'd M Y') {

        splitc = ' ';
    }
    else if (hdnformat == 'd.m.Y' || hdnformat == 'm.d.Y' || hdnformat == 'Y.d.m' || hdnformat == 'Y.m.d' || hdnformat == 'M.d.Y' || hdnformat == 'd.M.Y') {
        splitc = '.';
    };
    var sym = '-';
    if (dateText.indexOf('/') != -1)
        sym = '/';
    var vardatesplit = dateText.split(sym);
    var varM;
    var varMn;
    var varfformatS = hdnformat.split(splitc);
    if (varfformatS[0] == 'M' || varfformatS[1] == 'M') {

        if (vardatesplit[1] != null)
            varM = vardatesplit[1];
        if (vardatesplit[0] != null)
            varM = vardatesplit[0];

        switch (varM) {
            case 'Jan': varMn = '01';
                break;
            case 'Feb': varMn = '02';
                break;
            case 'Mar': varMn = '03';
                break;
            case 'Apr': varMn = '04';
                break;
            case 'May': varMn = '05';
                break;
            case 'Jun': varMn = '06';
                break;
            case 'Jul': varMn = '07';
                break;
            case 'Aug': varMn = '08';
                break;
            case 'Sep': varMn = '09';
                break;
            case 'Oct': varMn = '10';
                break;
            case 'Nov': varMn = '11';
                break;
            case 'Dec': varMn = '12';
                break;
        }

    }
    if (varfformatS[0] == 'd' && (varfformatS[1] == 'm' || varfformatS[1] == 'M') && varfformatS[2] == 'Y') {
        if (varfformatS[1] == 'M') {
            txtMainDate = varMn + splitc + vardatesplit[1] + splitc + vardatesplit[0];
        }
        else {
            txtMainDate = vardatesplit[2] + splitc + vardatesplit[1] + splitc + vardatesplit[0];
        }
    }
    else if ((varfformatS[0] == 'm' || varfformatS[0] == 'M') && varfformatS[1] == 'd' && varfformatS[2] == 'Y') {
        if (varfformatS[0] == 'M') {
            txtMainDate = varMn + splitc + vardatesplit[2] + splitc + vardatesplit[0];
        }
        else {
            txtMainDate = vardatesplit[1] + splitc + vardatesplit[2] + splitc + vardatesplit[0];
        }
    }
    else if (varfformatS[0] == 'Y' && varfformatS[1] == 'd' && varfformatS[2] == 'm') {
        txtMainDate = vardatesplit[0] + splitc + vardatesplit[2] + splitc + vardatesplit[1];
    }
    else if (varfformatS[0] == 'Y' && varfformatS[1] == 'm' && varfformatS[2] == 'd') {
        txtMainDate = vardatesplit[0] + splitc + vardatesplit[1] + splitc + vardatesplit[2];
    }
    else {
        txtMainDate = vardatesplit[0] + splitc + vardatesplit[1] + splitc + vardatesplit[2];
    }
    return txtMainDate;
}

function GetPaging() {
    window.localStorage.setItem('ctCheck', 1);
    $('table#tbldiv').each(function () {

        var currentPage = 0;
        var numPerPage = $('#hdnPaging').val();
        var $table = $(this);
        var $tr = $table.find('tbody tr');

        $table.bind('repaginate', function () {
            $table.find('tbody tr[data-tt-parent="Paging"]').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
        });
        //  
        $table.trigger('repaginate');
        var numRows = $table.find('tbody tr[data-tt-parent="Paging"]').length;
        var numPages = Math.ceil(numRows / numPerPage);

        $("#DivPager").remove();

        var $pager = $('<div id="DivPager" class="pager"></div>');
        for (var page = 0; page < numPages; page++) {
            $('<span class="page-number"></span>').text(page + 1).bind('click', {
                newPage: page
            }, function (event) {
                currentPage = event.data['newPage'];
                $table.trigger('repaginate');
                $(this).addClass('active').siblings().removeClass('active');
            }).appendTo($pager).addClass('clickable');
        }
        $pager.insertAfter($table).find('span.page-number:first').addClass('active');

    });

}

function loadGraphData() {
    window.localStorage.setItem('ctCheck', 1);
    $('#li-Excel i').attr('style', 'color:#919598 !important');
    $('#li-Excel').attr('onclick', 'return false');
    $('#li-Refresh i').attr('style', 'color:#919598 !important');
    $('#li-Refresh').attr('onclick', 'return false');
    $('#li_expand i').attr('style', 'color:#919598 !important');
    $('#li_expand').attr('onclick', 'return false');
    $('#li_compress i').attr('style', 'color:#919598 !important');
    $('#li_compress').attr('onclick', 'return false');
    $('#li_Print i').attr('style', 'color:#919598 !important');
    $('#li_Print').attr('onclick', 'return false');
    $('#txt_PRPPageIndex').attr('disabled', 'true');
    $('#btnPage').attr('disabled', 'true');
    $('#li_PRFirst i').attr('style', 'color:#919598 !important');
    $('#li_PRFirst').attr('onclick', 'return false');
    $('#li_PRNext i').attr('style', 'color:#919598 !important');
    $('#li_PRNext').attr('onclick', 'return false');
    $('#li_PRPrev i').attr('style', 'color:#919598 !important');
    $('#li_PRPrev').attr('onclick', 'return false');
    $('#li_PRLast i').attr('style', 'color:#919598 !important');
    $('#li_PRLast').attr('onclick', 'return false');
    $('#lbl_invPageOf').attr('style', 'color:#919598 !important');
    $('#lbl_page').attr('style', 'color:#919598 !important');

    var param = { CounterId: window.localStorage.getItem('COUNTERID'), ShiftId: window.localStorage.getItem('SHIFTID'), SalespersonId: window.localStorage.getItem('CashierID') };
    var color = Chart.helpers.color;
    var barChartData = '';
    var GrColorSr = '';
    if (window.localStorage.getItem("Theam").split('-')[3] == '2.css') { GrColorSr = '#ffffff' } else { GrColorSr = '#000' }

    var elemGrid = document.getElementById("div_grid");
    elemGrid.style.display = "none";

    var elemGraph = document.getElementById("div_graph");
    elemGraph.style.display = "block";

    var resp = graphData;

    
    var x = []; var y = [];


    for (var i = 0; i < resp.length; i++) {
        x.push(resp[i].C3);
        y.push(parseFloat(resp[i].C12).toFixed(2));
    }

    barChartData = {
        labels: x,
        datasets: [{
           label: $('#hdnSales').val(),
            backgroundColor: ["#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276"], //color(window.chartColors.red).alpha(0.5).rgbString(),
            borderColor: ["#ff7276", "#ff999c", "#ffb3b5", "#ffccce", "#ffe6e6", "#ff7276", "#ff7276", "#ff7276"],//window.chartColors.red,
            borderWidth: 1,
            data: y
        },
          {
              type: 'line',
             label: $('#hdnLineView').val(),
              borderColor: window.chartColors.red,
              borderWidth: 2,
              fill: false,
              data: y,

          }
        ]
    }

    var ctx = document.getElementById("canvas").getContext("2d");
    window.myBar = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            legend: {
                labels: {
                    fontColor: GrColorSr,
                    fontSize: 12
                }
            },
            responsive: true,
            title: {
                display: true,
                text: ''
            },
            
scales: {
            yAxes: [{
                ticks: {
                    min: 0,
                    // stepSize: 1,
                    fontColor: GrColorSr,
                    fontSize: 14
                }
                ,
                gridLines: {
                    color: GrColorSr
                    //    lineWidth: 2,
                    //    zeroLineColor: "#000",
                    //    zeroLineWidth: 2
                },
                stacked: true
            }],
            xAxes: [{
                ticks: {
                    autoSkip: false,
                    maxRotation: 60,
                    minRotation: 60,
                    fontColor: GrColorSr,
                    fontSize: 14
                },
                gridLines: {
                    color: GrColorSr,
                    lineWidth: 2
                }
            }]
        }
        }
    });
}

function getResources(langCode, pageCode, data, sts) {
    window.localStorage.setItem('ctCheck', 1);
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.getResource,
        data: "langCode=" + langCode + "&pageCode=" + pageCode,
        success: function (resp) {
            setLabels(resp,data,1);
        }
    });
}

function setLabels(labelData,data,sts) {
    window.localStorage.setItem('ctCheck', 1);
    $('#hdnReturnNo').val(labelData['alert1']);
    $('#hdnReturnDt').val(labelData['alert2']);
    $('#hdnRefInvoiceNo').val(labelData['alert3']);
    $('#hdnRefInvoiceDt').val(labelData['alert4']);
    $('#hdnTill').val(labelData['till']);
    $('#hdnBaseTot').val(labelData['total']);
    $('#hdnTaxTot').val(labelData['tax']);
    $('#hdnTaxAmount').val(labelData["tax"]);
    $('#hdnNetTot').val(labelData['netTotal']);
    $('#hdnItem').val(labelData['item'] +' '+ labelData['code']);
    $('#hdnQty').val(labelData['qty']);
    $('#POSSearchTitleItemName').val(labelData['name']);
    $('#hdnUOM').val(labelData['uom']);
    $('#hdnPrice').val(labelData['price']);
    $('#hdnDisc').val(labelData['disc']);
   $('#hdnItemAmount').val(labelData['amount']);
    $('#hdnReason').val(labelData['remarks']);
    $('#hdnChPwdSaveSucc').val(labelData["saveSuccess"]);
    $('#hdnSalesTo').val(labelData['POSTo']);
    $('#INVLISlblDate').val(labelData['POSTo']);
    
    jQuery("label[for='lblTitle'").html(labelData['salesReturn']);
    jQuery("label[for='lblSalesReturnReport'").html(labelData['salesReturn']);
    jQuery("label[for='lbl_page'").html(labelData["page"]);
    jQuery("label[for='lblMail'").html(labelData["Mail"]);
    jQuery("label[for='ToMail'").html(labelData["POSTo"]);
    jQuery("label[for='Bcc'").html(labelData["BCC"]);
    jQuery("label[for='lblFormat'").html(labelData["format"]);
    jQuery("label[for='lblBody'").html(labelData["body"]);
    jQuery("label[for='lblsend'").html(labelData["Send"]);
    jQuery("label[for='lblClose'").html(labelData["Close"]);
    $('#hdnSales').val(labelData["alert6"]);
    $('#hdnLineView').val(labelData["alert7"]);
    $('#hdnEnterMail').val(labelData["enterMail"]);
    $('#hdnMailSuccess').val(labelData["MailSuccess"]);
    $('#hdnMailnotSuccess').val(labelData["MailnotSuccess"]);
    $('#hdnInvalidData').val(labelData["InvalidData"]);
    $('#hdnThameChange').val(labelData["alert5"]);
    $('#hdnPrint').val(labelData["print"]);
$('#hdnMExcel').val(labelData["Excel"]);
getLoadExdata();
    arrangeSalesReturnsData(data);
    if (sts == 2) {
        var $el = $('#tbldiv tbody>tr.tt-hide');
        $el.removeClass("tt-hide");

        for (var i = 0; i < $el.length; i++) {
            $('#root_' + i + '').html("-");
        }
    }

    $('#INVLISlblDate').text('(' + ConvertDateAndReturnDate($('#hdnDateFromate').val(), window.localStorage.getItem('RPfrmDate')) + ' ' + $('#hdnSalesTo').val() + ' ' + ConvertDateAndReturnDate($('#hdnDateFromate').val(), window.localStorage.getItem('RPtoDate')) + ')');
                 
           
}
function getLoadExdata()
{
//document.getElementById("FileFormat").options.length = 0;
$("#FileFormat").append($("<option></option>").val("0").html("----select----"));
$("#FileFormat").append($("<option></option>").val("1").html($('#hdnMExcel').val()));
 $("#FileFormat option[value='1']").attr('selected','selected');
}
function SRRexportexcel() {
    window.localStorage.setItem('ctCheck', 1);
    loadGridExtend();
    var data_type = 'data:application/vnd.ms-excel';
    var table_div = document.getElementById('tbldiv');
    var table_html = table_div.outerHTML.replace(/ /g, '%20');
    var a = document.createElement('a');
    a.href = data_type + ', ' + table_html;
    var pageName_ = "exported_table_";

    pageName_ = "Sales-Return";

    a.download = pageName_ + '.xls';
    //a.click();


    var d = $('#div_tbData');
    let options = {
        documentSize: 'A4',
        type: 'share',
        fileName: 'Sales-Return.pdf'
    }
    var lable_ = "<div><b>Sales Return " + $('#INVLISlblDate').text() + "</b></div></br>";
    pdf.fromData(lable_ + d[0].innerHTML, options)
        .then((stats) => console.log('status', stats))   // ok..., ok if it was able to handle the file to the OS.  
        .catch((err) =>console.err(err));


    loadGridCompress();
}

function SRRPrint() {
    window.localStorage.setItem('ctCheck', 1);
    var btnCol = document.getElementById("btnInvCol");

    loadGridExtend();
    var divContents = document.getElementById("div_grid").innerHTML;
    var printWindow = window.open('', '', 'height=200,width=450');
    printWindow.document.write('<html><head><title>Sales Return</title>');
    printWindow.document.write('</head><body >');
    printWindow.document.write(divContents);
    printWindow.document.write('</body></html>');
    loadGridCompress();
    printWindow.document.close();
    printWindow.print();

}

function loadGridExtend() {
    window.localStorage.setItem('ctCheck', 1);
    var myTab = document.getElementById('tbldiv');
    for (i = 1; i < myTab.rows.length; i++) {
        var objCells = myTab.rows.item(i).cells;
        var id_ = myTab.rows.item(i).cells[0].parentNode.id + '_0';
        $('#' + id_)[0].style.display = '';
        i++;
    }
    var li_expand = document.getElementById("li_expand");
    li_expand.style.display = "none";
    var li_compress = document.getElementById("li_compress");
    li_compress.style.display = "inline-block";

}
function loadGridCompress() {
    window.localStorage.setItem('ctCheck', 1);
    var myTab = document.getElementById('tbldiv');
    for (i = 1; i < myTab.rows.length; i++) {
        var objCells = myTab.rows.item(i).cells;
        var id_ = myTab.rows.item(i).cells[0].parentNode.id + '_0';
        $('#' + id_)[0].style.display = 'none';
        i++;
    }
    var li_expand = document.getElementById("li_expand");
    li_expand.style.display = "inline-block";
    var li_compress = document.getElementById("li_compress");
    li_compress.style.display = "none";
}
function prevPRFirstInvl() {
    window.localStorage.setItem('ctCheck', 1);
    SalesReturnsData();   

}
function prevPRInvl() {
    window.localStorage.setItem('ctCheck', 1);
    $('#hdnPaging').val(parseFloat($('#hdnPaging').val()) - 1);
    if ($('#txt_pageCount').val() == "") {
        $('#txt_pageCount').val('10');
        $('#hdnPaging').val('1');
    }
    if (parseFloat($('#hdnPaging').val()) == 0) {
        $('#hdnPaging').val('1');
    }
    if ($('#hdnTotalPagesCount').val() != $('#hdnPaging').val()) {
        $('#li_PRPrev i').removeAttr('style', 'color:#919598 !important');
        $('#li_PRPrev').attr('onclick', 'nextPRInvl()');
        $('#li_PRLast i').removeAttr('style', 'color:#919598 !important');
        $('#li_PRLast').attr('onclick', 'nextPRLastInvl()');
    }
    var btnExp = document.getElementById("btnExp");
    btnExp.style.display = "block";
   
        var SubOrgId = '';
        var HashVal = (window.localStorage.getItem("hashValues"));
        var HashValues;
        if (HashVal != null && HashVal != '') {
            HashValues = HashVal.split('~');

            SubOrgId = HashValues[1];
            $('#hdnDateFromate').val(HashValues[2]);

        }

        var PageIndex = $('#hdnPaging').val();
        var PageCount = $('#txt_pageCount').val();


        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.GetReport_V01,
            data: "PageId=" + window.localStorage.getItem('RPPAGECODE') + "&subOrg=" + SubOrgId + "&frmDate=" + window.localStorage.getItem('RPfrmDate') + "&toDate=" + window.localStorage.getItem('RPtoDate') + ' 23:59:59' + "&refNo=" + window.localStorage.getItem('RPrefNo') + "&DateFormate=" + $('#hdnDateFromate').val() + "&VD=" + VD() + "&PageIndex=" + PageIndex + "&PageCount=" + PageCount +"&language=" + HashValues[7] + "",
            success: function (resp) {
                graphData = resp;
                if (resp != '') {
                    jQuery("label[for='lbl_invPageOf'").html(resp[0].C15);
                }
                getResources(HashValues[7], "POSSalseReturnReport", resp, 1);
                $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());
            }
        });

    }
  
function nextPRInvl() {
    window.localStorage.setItem('ctCheck', 1);
    $('#hdnPaging').val(parseFloat($('#hdnPaging').val()) + 1);
    if ($('#txt_pageCount').val() == "") {
        $('#txt_pageCount').val('10');
        $('#hdnPaging').val('1');
    }
    var btnExp = document.getElementById("btnExp");
    btnExp.style.display = "block";
    
        var SubOrgId = '';
        var HashVal = (window.localStorage.getItem("hashValues"));
        var HashValues;
        if (HashVal != null && HashVal != '') {
            HashValues = HashVal.split('~');

            SubOrgId = HashValues[1];
            $('#hdnDateFromate').val(HashValues[2]);

        }

        var PageIndex = $('#hdnPaging').val();
        var PageCount = $('#txt_pageCount').val();

        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.GetReport_V01,
            data: "PageId=" + window.localStorage.getItem('RPPAGECODE') + "&subOrg=" + SubOrgId + "&frmDate=" + window.localStorage.getItem('RPfrmDate') + "&toDate=" + window.localStorage.getItem('RPtoDate') + ' 23:59:59' + "&refNo=" + window.localStorage.getItem('RPrefNo') + "&DateFormate=" + $('#hdnDateFromate').val() + "&VD=" + VD() + "&PageIndex=" + PageIndex + "&PageCount=" + PageCount + "&language=" + HashValues[7] + "",
            success: function (resp) {
                graphData = resp;
                if (resp != '') {
                    jQuery("label[for='lbl_invPageOf'").html(resp[0].C15);
                }
                getResources(HashValues[7], "POSSalseReturnReport", resp, 1);
                $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());
            }
        });
        if ($('#hdnTotalPagesCount').val() == $('#hdnPaging').val()) {
            $('#li_PRPrev i').attr('style', 'color:#919598 !important');
            $('#li_PRPrev').attr('onclick', 'return false');
            $('#li_PRLast').attr('onclick', 'return false');
            $('#li_PRLast i').attr('style', 'color:#919598 !important');
        }
}
function nextPRLastInvl() {
    window.localStorage.setItem('ctCheck', 1);
    var lastPageIndex = $('#lbl_invPageOf')[0].innerText.split(' ')[2];
    if (lastPageIndex == '') {
        lastPageIndex = '1';
    }
    $('#hdnPaging').val(parseFloat(lastPageIndex));
    if ($('#txt_pageCount').val() == "") {
        $('#txt_pageCount').val('10');
        $('#hdnPaging').val('1');
    }
    var btnExp = document.getElementById("btnExp");
    btnExp.style.display = "block";
    var SubOrgId = '';
    var HashVal = (window.localStorage.getItem("hashValues"));
    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');

        SubOrgId = HashValues[1];
        $('#hdnDateFromate').val(HashValues[2]);

    }

    var PageIndex = $('#hdnPaging').val();
    var PageCount = $('#txt_pageCount').val();

    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetReport_V01,
        data: "PageId=" + window.localStorage.getItem('RPPAGECODE') + "&subOrg=" + SubOrgId + "&frmDate=" + window.localStorage.getItem('RPfrmDate') + "&toDate=" + window.localStorage.getItem('RPtoDate') + ' 23:59:59' + "&refNo=" + window.localStorage.getItem('RPrefNo') + "&DateFormate=" + $('#hdnDateFromate').val() + "&VD=" + VD() + "&PageIndex=" + PageIndex + "&PageCount=" + PageCount + "&language=" + HashValues[7] + "",
        success: function (resp) {
            graphData = resp;
            if (resp != '') {
                jQuery("label[for='lbl_invPageOf'").html(resp[0].C15);
            }
            getResources(HashValues[7], "POSSalseReturnReport", resp, 1);
            $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());
        }
    });
if($('#hdnTotalPagesCount').val() == $('#hdnPaging').val()) {
       $('#li_PRPrev i').attr('style', 'color:#919598 !important');
            $('#li_PRPrev').attr('onclick', 'return false');
            $('#li_PRLast').attr('onclick', 'return false');
       $('#li_PRLast i').attr('style', 'color:#919598 !important');
}
}
function loadSRRbyPageIndexStatic() {
    window.localStorage.setItem('ctCheck', 1);
            var pageIndexT_ = 1;
            if ($('#txt_PRPPageIndex').val() != '') {
                if ($('#txt_PRPPageIndex').val() != '0') {
                    pageIndexT_ = $('#txt_PRPPageIndex').val();
                }
            }
            $('#hdnPaging').val(pageIndexT_);
            var btnExp = document.getElementById("btnExp");
            btnExp.style.display = "block";
                var SubOrgId = '';
                var HashVal = (window.localStorage.getItem("hashValues"));
                var HashValues;
                if (HashVal != null && HashVal != '') {
                    HashValues = HashVal.split('~');

                    SubOrgId = HashValues[1];
                    $('#hdnDateFromate').val(HashValues[2]);

                }

                var PageIndex = $('#hdnPaging').val();
                var PageCount = $('#txt_pageCount').val();
                $.ajax({
                    type: 'GET',
                    url: CashierLite.Settings.GetReport_V01,
                    data: "PageId=" + window.localStorage.getItem('RPPAGECODE') + "&subOrg=" + SubOrgId + "&frmDate=" + window.localStorage.getItem('RPfrmDate') + "&toDate=" + window.localStorage.getItem('RPtoDate') + ' 23:59:59' + "&refNo=" + window.localStorage.getItem('RPrefNo') + "&DateFormate=" + $('#hdnDateFromate').val() + "&VD=" + VD() + "&PageIndex=" + PageIndex + "&PageCount=" + PageCount + "&language=" + HashValues[7] + "",
                    success: function (resp) {
                        graphData = resp;
                        if (resp != '') {
                            jQuery("label[for='lbl_invPageOf'").html(resp[0].C15);
                        }
                        getResources(HashValues[7], "POSSalseReturnReport", resp, 1);
                        $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());
                    }
                });
                txt_PRPPageIndex.val('');
                if ($('#hdnTotalPagesCount').val() == $('#hdnPaging').val()) {
                    $('#li_PRPrev i').attr('style', 'color:#919598 !important');
                    $('#li_PRPrev').attr('onclick', 'return false');
                    $('#li_PRLast').attr('onclick', 'return false');
                    $('#li_PRLast i').attr('style', 'color:#919598 !important');
                }
        }



