﻿$("#divNavBarRight").attr('style', 'display:block;');
//Page Load Function
function MainCounterData() {
    window.localStorage.setItem('ctCheck', 1);
    GetLoginUserName();
    defaultdata();
    $('#hdnPageIndex').val('1');
    $('#hdnFilterType').val('Summary');
    GetSummaryGridData('Summary');
    GetCounterType();
    GetAutoCodeGen($('#hdnBranchId').val(), '3');
    $('#lblLastUpdate').css("display", "none");

    $('#trSBInnerRow li span#lblMode').html('Default');    
}

function defaultdata() {
    window.localStorage.setItem('ctCheck', 1);
    var Paging = ""; var CompanyId = ""; var SubOrgId = ''; var Orgcode = ''; var OBS = ''; var CashierID = window.localStorage.getItem("CashierID"); var Userid = '';
    var Lang = '';
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        getResources(hashSplit[7], "POSCounterDefination");
        CompanyId = hashSplit[0];
        SubOrgId = hashSplit[1];
        OBS = hashSplit[9];
        Orgcode = hashSplit[17];
        Userid = hashSplit[14];
        Lang = hashSplit[7];
        Paging = hashSplit[19];
    }
    $('#CompanyId').val(CompanyId);
    $('#hdnBranchId').val(SubOrgId);
    $('#hdnOBS').val(OBS);
    $('#hdnUserId').val(Userid);
    //$('#hdnBranchId').val("004c6087-45a7-468b-bdd8-46b4a1dcf647");
    $('#txtPosCode').val(Orgcode);
    $('#hdnLanguage').val(Lang);
    $('#hdnPaging').val(Paging);
    $('#Content_two').css("display", "none");
    $('#Create').css("display", "block");
    // $('#Create').css("display", "block");
    $('#Edit').css("display", "none");
    $('#Cancel').css("display", "none");
    $('#btnSave').css("display", "none");
    $('#Delete').css("display", "none");
    $('#Status').css("display", "none");
    $('#divActive').css("display", "none");
}
function GetLoginUserName() {
    window.localStorage.setItem('ctCheck', 1);
    var sess = CashierLite.Session.getInstance().get();
    if (sess != null) {
        if (sess.userName != "") {
            document.getElementById("lblName").innerHTML = sess.userName;
        }
        else {
            window.open('Login.html', '_self', 'location=no');
        }
    }
    else {
        window.open('Login.html', '_self', 'location=no');
    }
    theamLoad();
}
function theamLoad() {
    window.localStorage.setItem('ctCheck', 1);
    if (window.localStorage.getItem("Theam") != null)
        $('head').append('<link rel="stylesheet" href=' + window.localStorage.getItem("Theam") + ' type="text/css" />');
}

//For Invoice Link
function GetShiftData() {
    window.localStorage.setItem('ctCheck', 1);
    if (window.localStorage.getItem("CashierID") != null) {

        var SubOrgId = ''; var CashierID = window.localStorage.getItem("CashierID");
        var HashValues = (window.localStorage.getItem("hashValues"));
        if (HashValues != null && HashValues != '') {
            var hashSplit = HashValues.split('~');
            SubOrgId = hashSplit[1];
        }
        var vd_ =  window.localStorage.getItem("UUID");
        //var vdSplit_ = window.location.pathname.split('/');
        //if (vdSplit_.length > 2) {
        //    vd_ = vdSplit_[1];
        //}
        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.ShiftCheck,
            data: "CashierID=" + CashierID + "&OrgId=" + SubOrgId + "&VD=" + vd_,
            success: function (resp) {
                var result = resp;
                if (result.length > 0) {
                    var ACTIVE = result[0].ACTIVE;
                    var COUNTERID = result[0].COUNTERID;
                    var COUNTERNAME = result[0].COUNTERNAME;
                    var COUNTERNUMBER = result[0].COUNTERNUMBER;
                    var COUNTERTYPE = result[0].COUNTERTYPE;
                    var End_Time = result[0].End_Time;
                    var Is_Next_Day = result[0].Is_Next_Day;
                    var SHIFTID = result[0].SHIFTID;
                    var Shift_Code = result[0].Shift_Code;
                    var Shift_Name = result[0].Shift_Name;
                    var Start_Time = result[0].Start_Time;
                    var StageAlert = result[0].StageAlert;

                    window.localStorage.setItem('ACTIVE', result[0].ACTIVE);
                    window.localStorage.setItem('COUNTERID', result[0].COUNTERID);
                    window.localStorage.setItem('COUNTERNAME', result[0].COUNTERNAME);
                    window.localStorage.setItem('COUNTERNUMBER', result[0].COUNTERNUMBER);
                    window.localStorage.setItem('COUNTERTYPE', result[0].COUNTERTYPE);
                    window.localStorage.setItem('End_Time', result[0].End_Time);
                    window.localStorage.setItem('Is_Next_Day', result[0].Is_Next_Day);
                    window.localStorage.setItem('SHIFTID', result[0].SHIFTID);
                    window.localStorage.setItem('Shift_Code', result[0].Shift_Code);
                    window.localStorage.setItem('Shift_Name', result[0].Shift_Name);
                    window.localStorage.setItem('Start_Time', result[0].Start_Time);
                    var StageAlert = result[0].StageAlert;
                    if (StageAlert == '1') {
                        if (ACTIVE.toLowerCase() == 'y') {
                            window.open('posmain.html?#', '_self', 'location=no');
                        }
                        else {
                            //var $textAndPic = $('<div></div>');
                            //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                            ////$textAndPic.append('<div class="alert-message">dont have permission to access invoice</div>');
                            //$textAndPic.append('<div class="alert-message">' + $('#hdnNPermInv').val() + '</div>');
                            //BootstrapDialog.alert($textAndPic);
                            navigator.notification.alert($('#hdnNPermInv').val(), "", "neo ecr", "Ok");
                        }
                    }
                    else if (StageAlert == '2') {
                        //var $textAndPic = $('<div></div>');
                        //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                        ////$textAndPic.append('<div class="alert-message">You are not assigned any Counter so you can\'t do the Transaction</div>');
                        //$textAndPic.append('<div class="alert-message">' + $('#hdnNAsnCounterTran').val() + '</div>');
                        //BootstrapDialog.alert($textAndPic);
                        navigator.notification.alert($('#hdnNAsnCounterTran').val(), "", "neo ecr", "Ok");

                    }
                }
                else {
                    //var $textAndPic = $('<div></div>');
                    //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                    //$textAndPic.append('<div class="alert-message">' + $('#hdnNPermInv').val() + '</div>');
                    //BootstrapDialog.alert($textAndPic);
                    navigator.notification.alert($('#hdnNPermInv').val(), "", "neo ecr", "Ok");

                }
            },
            error: function (e) {
                //var $textAndPic = $('<div></div>');
                //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                //$textAndPic.append('<div class="alert-message">' + $('#hdnInvalData').val() + '</div>');
                //BootstrapDialog.alert($textAndPic);
                navigator.notification.alert($('#hdnInvalData').val(), "", "neo ecr", "Ok");

            }
        });
    }
    else {
        //var $textAndPic = $('<div></div>');
        //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        ////$textAndPic.append('<div class="alert-message">Please contact Admin.</div>');
        //$textAndPic.append('<div class="alert-message">' + $('#hdnContactAdmin').val() + '</div>');
        //BootstrapDialog.alert($textAndPic);
        navigator.notification.alert($('#hdnContactAdmin').val(), "", "neo ecr", "Ok");

    }


}
function GetCounterType() {
    window.localStorage.setItem('ctCheck', 1);
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        SubOrg_Id = hashSplit[1];
    }
    var vd_ = window.localStorage.getItem("UUID");
    //var vdSplit_ = window.location.pathname.split('/');
    //if (vdSplit_.length > 2) {
    //    vd_ = vdSplit_[1];
    //}
    Jsonobj = [];
    var SubOrg = {};
    SubOrg.SubOrgID = $('#hdnBranchId').val();
    SubOrg.VD = vd_;
    Jsonobj.push(SubOrg);
     
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetCounterTypes,
        data: "SubOrg=" + JSON.stringify(Jsonobj),
        success: function (resp) {
            var CounterData = resp;
            document.getElementById("ddlCounterType").options.length = 0;
            $("#ddlCounterType").append($("<option value=''  selected ></option>").val(0).html('--' + $("#hdnSelect").val() + '--'));//this is for adding select after clearing
            var MainData = '';
            jsonObj = [];
            for (var i = 0; i < CounterData.length; i++) {
                var CounterId = CounterData[i].CounterID;
                var CounterName = CounterData[i].Code + "/" + CounterData[i].Name;
                //if (MainData != '')
                //    MainData = MainData + CusId + ':' + CusName + ',';
                //else
                //    MainData =  CusId + ':' + CusName + ',';
                item = {}
                item["code"] = CounterId;
                item["name"] = CounterName;

                jsonObj.push(item);
                $("#ddlCounterType").append($("<option></option>").val(CounterId).html(CounterName));
            };
            //window.localStorage.setItem("FillCustomer", jsonObj);
        },
        error: function (e) {
            //var $textAndPic = $('<div></div>');
            //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
            //$textAndPic.append('<div class="alert-message">' + $('#hdnInvalData').val() + '</div>');
            //BootstrapDialog.alert($textAndPic);
            navigator.notification.alert($('#hdnInvalData').val(), "", "neo ecr", "Ok");

        }
    });

}
//End Page Load function
//Create Function

//End
$(document).ready(function () {
    //$('table#tblSummaryGrid').tableSearch({
    //    searchText: 'Search Filter',
    //    placeholder: 'Input Value'
    //});
    $('#txtPhoneNo').keypress(function (e) {
        if ((e.which != 46 || $(this).val().indexOf('.') != -1) && (e.which < 48 || e.which > 57)) {
            e.preventDefault();
            //return false;
        }
    });
});
//Summary grid click function
function DoubleClick(tr) {
    window.localStorage.setItem('ctCheck', 1);
    var id = $(tr).find('td:first').html();
    var vd_ = window.localStorage.getItem("UUID");
    //var vdSplit_ = window.location.pathname.split('/');
    //if (vdSplit_.length > 2) {
    //    vd_ = vdSplit_[1];
    //}
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetCounterById,
        data: { CounterId: id, VD: vd_ },
        success: function (resp) {
            if (resp.length != 0) {
                $('#txtPosCode').val(resp[0].POSCode);
                $('#txtCounterCode').val(resp[0].Code);
                $('#txtCounterName').val(resp[0].Name);
                $('#txtAliasName').val(resp[0].Alias);
                $("#txtDescription").val(resp[0].Description);
                $('#txtPhoneNo').val(resp[0].phone_no);
                $('#ddlCounterType').val(resp[0].CounterType);
                $('#hdnBranchId').val(resp[0].Branchid);
                $('#hdnTransId').val(resp[0].Counter_ID);
                $('#hdnLastUpdatedDate').val(resp[0].LastUpdatedDate);
                $('#lblLastUpdate').css("display", "block");
                $('#trSBInnerRow li span#lblLastUpdate').html($('#trSBInnerRow li span#lblLastUpdate').html());
                $('#trSBInnerRow li span#lblLastUpdatedUser').html(resp[0].LastUpdateduser);
                $('#trSBInnerRow li span#lblLastUpdatedDate').html(resp[0].LastUpdatedDate);
                //$('#trSBInnerRow li span#lblMode').html("View");
                $('#trSBInnerRow li span#lblMode').html($('#hdnView').val());
                if (resp[0].Status == "N") {
                    $("#btnCreate").attr('style', 'display:block;');
                    $("#ShowMenuModes").attr('style', 'display:block;');
                    $("#btnSave").attr('style', 'display:none;');
                    $("#btnEdit").attr('style', 'display:none;');
                    //$("#btnDelete").attr('style', 'display:none;');
                    $("#btnCancel").attr('style', 'display:block;');
                    $("#SummaryGrid").attr('style', 'display:none;');
                    $("#NewPage").attr('style', 'display:block;');
                    $("#divStatusGroup").attr('style', 'display:block;');
                    $("#divNavBarRight").attr('style', 'display:none;');
                    $("#btnInActive").addClass("disabledHyperlink");
                    $("#btnActive").removeClass("disabledHyperlink");
                }
                else {
                    $("#btnInActive").removeClass("disabledHyperlink");
                    $("#btnActive").addClass("disabledHyperlink");
                    $("#btnCreate").attr('style', 'display:block;');
                    $("#ShowMenuModes").attr('style', 'display:block;');
                    $("#btnSave").attr('style', 'display:none;');
                    $("#btnEdit").attr('style', 'display:block;');
                    //$("#btnDelete").attr('style', 'display:block;');
                    $("#btnCancel").attr('style', 'display:block;');
                    $("#SummaryGrid").attr('style', 'display:none;');
                    $("#NewPage").attr('style', 'display:block;');
                    $("#divStatusGroup").attr('style', 'display:block;');
                    $("#divNavBarRight").attr('style', 'display:none;');
                }
                $('#txtCounterCode').attr("disabled", "disabled");
                $('#txtCounterName').attr("disabled", "disabled");
                $('#txtAliasName').attr("disabled", "disabled");
                $("#txtDescription").attr("disabled", "disabled");
                $('#txtPhoneNo').attr("disabled", "disabled");
                $('#ddlCounterType').attr("disabled", "disabled");
            }
        },
        error: function (e) {
            //var $textAndPic = $('<div></div>');
            //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
            //$textAndPic.append('<div class="alert-message">' + $('#hdnInvalData').val() + '</div>');
            //BootstrapDialog.alert($textAndPic);
            navigator.notification.alert($('#hdnInvalData').val(), "", "neo ecr", "Ok");

        }
    });

    $('#PrimeInfo').addClass('open');
    $('#divPrimeInformation').attr('style', 'display:block');
}
//End
//Edit Function
function Edit() {
    window.localStorage.setItem('ctCheck', 1);
    //$('#trSBInnerRow li span#lblMode').html("Edit");
    $('#trSBInnerRow li span#lblMode').html($('#hdnEdit').val());
    $("#btnCreate").attr('style', 'display:none;');
    $("#ShowMenuModes").attr('style', 'display:block;');
    $("#btnSave").attr('style', 'display:block;');
    $("#btnEdit").attr('style', 'display:none;');
    //$("#btnDelete").attr('style', 'display:block;');
    //$("#btnDelete").attr('style', 'border-left:1px solid #cac8c8;');
    $("#btnCancel").attr('style', 'display:block;');
    $("#divStatusGroup").attr('style', 'display:none;');
    $('#txtCounterName').removeAttr("disabled", "disabled");
    $('#txtAliasName').removeAttr("disabled", "disabled");

    $("#txtDescription").removeAttr("disabled", "disabled");
    $('#txtPhoneNo').removeAttr("disabled", "disabled");
    $('#ddlCounterType').attr("disabled", "disabled");
    $("#divNavBarRight").attr('style', 'display:none;');

    $('#PrimeInfo').addClass('open');
    $('#divPrimeInformation').attr('style', 'display:block');
}
//End
//Delete function
function Delete() {
    window.localStorage.setItem('ctCheck', 1);
    if ($('#hdnTransId').val() != "") {
        $('#SummaryGrid').css("display", "block");
        $('#NewPage').css("display", "none");
        $("#btnCreate").attr('style', 'display:block;');
        $("#ShowMenuModes").attr('style', 'display:none;');
        $("#divNavBarRight").attr('style', 'display:block;');
    }
    if (confirm($('#hdnDeleteConfirm').val())) {
        var vd_ = window.localStorage.getItem("UUID");
        //var vdSplit_ = window.location.pathname.split('/');
        //if (vdSplit_.length > 2) {
        //    vd_ = vdSplit_[1];
        //}
        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.DeleteCounter,
            data: { Counter_Id: $('#hdnTransId').val(), LastUpdatedDate: $('#hdnLastUpdatedDate').val(), VD: vd_ },
            success: function (resp) {
                var result = resp.split(',');
                if (result[0] == "100000") {
                    GetSummaryGridData('Summary');
                    //var $textAndPic = $('<div></div>');
                    //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-info-circle info-alertSuc"></i>');
                    ////$textAndPic.append('<div class="alert-message">' + $('#txtCounterCode').val() + " Record deleted Successfully" + '</div>');
                    //$textAndPic.append('<div class="alert-message">' + $('#txtCounterCode').val() +' '+ $('#hdnRecDelSucc').val() + '</div>');
                    //BootstrapDialog.alert($textAndPic);
                    navigator.notification.alert($('#txtCounterCode').val() + ' ' + $('#hdnRecDelSucc').val(), "", "neo ecr", "Ok");

                    clearControls();

                }
            },
            error: function (e) {
                //var $textAndPic = $('<div></div>');
                //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                ////$textAndPic.append('<div class="alert-message">Invalid Data</div>');
                //$textAndPic.append('<div class="alert-message">' + $('#hdnInvalData').val() + '</div>');
                //BootstrapDialog.alert($textAndPic);
                navigator.notification.alert($('#hdnInvalData').val(), "", "neo ecr", "Ok");

            }
        });
    } else {
        return false;
    }
}
//End
function clearControls() {
    window.localStorage.setItem('ctCheck', 1);
    $('#txtCounterCode').val("");
    $('#txtCounterName').val("");
    $('#txtAliasName').val("");
    $("#txtDescription").val("");
    $('#txtPhoneNo').val("");
    $('#ddlCounterType').val("0");
    $('#hdnTransId').val("");
    $('#hdnLastUpdatedDate').val("");
    $('#hdnScreenMode').val("");

}
function FilterSummaryGrid(ControlData, ColumnData) {
    window.localStorage.setItem('ctCheck', 1);
    $('#txtFindGo').val('');
    result = [];
    result = JSON.parse((window.localStorage.getItem("CounterDefFilterGrid")));
    var SummaryTable;

    if (ColumnData == 'Status') {
        $("#hdnFilterType").val(ControlData.toLowerCase());
        GetSummaryGridData(ControlData.toLowerCase())
        
    }
    else
        if (ColumnData == 'CREATEDDATE') {
            $("#hdnFilterType").val(ControlData);
            GetSummaryGridData(ControlData)
        }

    //for (var i = 0; i < result.length; i++) {

    //    if (ColumnData == 'Status') {
    //        var Status_ = result[i].Status;
    //        if (Status_.toLowerCase() == ControlData.toLowerCase()) {
    //            SummaryTable = SummaryTable + "<tr onclick='DoubleClick(this)'><td style='display:none'>" + result[i].Counter_ID + "</td><td>" + result[i].Code + "</td><td>" + result[i].Name + "</td><td>" + result[i].CounterType.split('/')[1] + "</td><td>" + result[i].Status + "</td></tr>"
    //        }


    //    }
    //    else if (ColumnData == 'CREATEDDATE') {
    //        var CDate = result[i].CREATEDDATE;
    //        var Date_ = CDate.split('-')
    //        //var CREATEDDATE = new Date(CDate);
    //        var CurDate = new Date();
    //        if (ControlData == 'Today') {
    //            if (Date_[0] == CurDate.getDate()) {
    //                SummaryTable = SummaryTable + "<tr onclick='DoubleClick(this)'><td style='display:none'>" + result[i].Counter_ID + "</td><td>" + result[i].Code + "</td><td>" + result[i].Name + "</td><td>" + result[i].CounterType.split('/')[1] + "</td><td>" + result[i].Status + "</td></tr>"
    //            }
    //        }
    //        else if (ControlData == 'Yesterday') {
    //            if ((Date_[0]) == (CurDate.getDate() - 1)) {
    //                SummaryTable = SummaryTable + "<tr onclick='DoubleClick(this)'><td style='display:none'>" + result[i].Counter_ID + "</td><td>" + result[i].Code + "</td><td>" + result[i].Name + "</td><td>" + result[i].CounterType.split('/')[1] + "</td><td>" + result[i].Status + "</td></tr>"
    //            }
    //        }
    //        else if (ControlData == 'Last7') {
    //            if (Date_[0] >= (CurDate.getDate() - 7)) {
    //                SummaryTable = SummaryTable + "<tr onclick='DoubleClick(this)'><td style='display:none'>" + result[i].Counter_ID + "</td><td>" + result[i].Code + "</td><td>" + result[i].Name + "</td><td>" + result[i].CounterType.split('/')[1] + "</td><td>" + result[i].Status + "</td></tr>"
    //            }
    //        }
    //        else if (ControlData == 'CMonth') {
    //            if ((Date_[1]) == (CurDate.getMonth() + 1)) {
    //                SummaryTable = SummaryTable + "<tr onclick='DoubleClick(this)'><td style='display:none'>" + result[i].Counter_ID + "</td><td>" + result[i].Code + "</td><td>" + result[i].Name + "</td><td>" + result[i].CounterType.split('/')[1] + "</td><td>" + result[i].Status + "</td></tr>"
    //            }
    //        }
    //    }
    //}
    //$('#tblSummaryGrid tbody').remove();
    //SummaryTable += "<tbody>" + SummaryTable + "</tbody>";
    //$('#tblSummaryGrid').append(SummaryTable);
    //Paging
    //GetPaging1();
}
//GetSummaryGridData
function prev() {
    window.localStorage.setItem('ctCheck', 1);
    var pageIndexCreate = parseFloat($('#hdnPageIndex').val()) - 1;
    if (parseFloat(pageIndexCreate) == 0) {
        pageIndexCreate = '1';
    }
    $('#hdnPageIndex').val(pageIndexCreate);
    var FilterType = $("#hdnFilterType").val();
    GetSummaryGridData(FilterType);
   
}
function next() {
    window.localStorage.setItem('ctCheck', 1);
    var pageIndexCreate = parseFloat($('#hdnPageIndex').val()) + 1;
    
    $('#hdnPageIndex').val(pageIndexCreate);
    var FilterType = $('#hdnFilterType').val();
    GetSummaryGridData(FilterType+'~Index');
    
}
function GetSummaryGridData(FilterType) {
    window.localStorage.setItem('ctCheck', 1);
   // $('#txtFindGo').val('');
    // $('#hdnPageIndex').val('1');
    var Index = $('#hdnPageIndex').val();
    var MethodType = "";
    var FindGo = "";
    var QryType = "";
    if (FilterType == 'FindGo') {
        FindGo = $('#txtFindGo').val();
       
    }
    if (FilterType == 'Summary') {
        $('#txtFindGo').val('');
    }

    else
    {
        FindGo = $('#txtFindGo').val();
    }
    if (FilterType.indexOf('Index') != -1) {
        FilterType = FilterType.split('~')[0];
        MethodType = "next";
    }
    
    var HashVal = (window.localStorage.getItem("hashValues"));
    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');
    }
    var SummaryTable = '';
    Jsonobj = [];
    var SubOrg = {};
    SubOrg.SubOrgID = $('#hdnBranchId').val();
    var vd_ = window.localStorage.getItem("UUID");
    //var vdSplit_ = window.location.pathname.split('/');
    //if (vdSplit_.length > 2) {
    //    vd_ = vdSplit_[1];
    //}
    SubOrg.VD = vd_;
    SubOrg.Index = Index;
    SubOrg.FilterType = FilterType;
    SubOrg.FindGoVal = FindGo;
    Jsonobj.push(SubOrg);
    //Jsonobj.push()
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetAllCounters,
        data: "SubOrg=" + JSON.stringify(Jsonobj),
        success: function (resp) {
            var result = resp;
            if (result.length > 0) {

           
            window.localStorage.setItem("CounterDefFilterGrid", JSON.stringify(result))
            $('#tblSummaryGrid >tbody').children().remove();

            for (var i = 0; i < result.length; i++) {
                SummaryTable += "<tr onclick='DoubleClick(this)'><td style='display:none'>" + result[i].Counter_ID + "</td><td>" + result[i].Code + "</td><td>" + result[i].Name + "</td><td>" + result[i].CounterType.split('/')[1] + "</td><td  style='display:none'>" + result[i].Status + "</td><td style='display:none'>" + result[i].Alias + "</td></tr>";
            }
            $('#tblSummaryGrid tbody').remove();
            SummaryTable += "<tbody>" + SummaryTable + "</tbody>";
            $('#tblSummaryGrid').append(SummaryTable);
            window.localStorage.setItem("CounterDefGrid", JSON.stringify(SummaryTable));
            //GetPaging1();
            $('#trSBInnerRow li span#lblMode').html('');
            }
            else
            {
               
                if (MethodType == "next") {
                    var pageIndexCreate = parseFloat($('#hdnPageIndex').val()) - 1;
                    $('#hdnPageIndex').val(pageIndexCreate);
                }
                //var $textAndPic = $('<div></div>');
                //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                ////$textAndPic.append('<div class="alert-message">Invalid Data</div>');
                //$textAndPic.append('<div class="alert-message">No Data Found</div>');
                //BootstrapDialog.alert($textAndPic);
                navigator.notification.alert("No Data Found", "", "neo ecr", "Ok");
            }
        },
        error: function (e) {
            //var $textAndPic = $('<div></div>');
            //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
            //$textAndPic.append('<div class="alert-message">' + $('#hdnInvalData').val() + '</div>');
            //BootstrapDialog.alert($textAndPic);
            navigator.notification.alert($('#hdnInvalData').val(), "", "neo ecr", "Ok");

        }

    });
}
//SummaryGrid Paging
function GetPaging1() {
    window.localStorage.setItem('ctCheck', 1);
    $('table#tblSummaryGrid').each(function () {
        var currentPage = 0;
        var numPerPage = $('#hdnPaging').val();
        var $table = $(this);
        $table.bind('repaginate', function () {
            $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
        });
        $table.trigger('repaginate');
        var numRows = $table.find('tbody tr').length;
        var numPages = Math.ceil(numRows / numPerPage);
        $("#DivPager").remove();
        var $pager = $('<div id="DivPager" class="pager"></div>');
        for (var page = 0; page < numPages; page++) {
            $('<span class="page-number"></span>').text(page + 1).bind('click', {
                newPage: page
            }, function (event) {
                currentPage = event.data['newPage'];
                $table.trigger('repaginate');
                $(this).addClass('active').siblings().removeClass('active');
            }).appendTo($pager).addClass('clickable');
        }
        $pager.insertAfter($table).find('span.page-number:first').addClass('active');
    });
}
$('#txtFindGo').keyup(function () {
    window.localStorage.setItem('ctCheck', 1);
    result = [];
    result = JSON.parse((window.localStorage.getItem("CounterDefFilterGrid")));
    if ($('#txtFindGo').val().trim() == "" || result.length <= 0) {
        
        GetSummaryGridData('Summary');
        return false;
    }
    else {
        //var SummaryTable;
        //for (var i = 0; i < result.length; i++) {
        //    var Code = result[i].Code;
        //    var Name = result[i].Name;
        //    var Status = result[i].Status;

        //    var CounterType = result[i].CounterType;
        //    if (Code.toLowerCase().indexOf($('#txtFindGo').val().trim().toLowerCase()) != -1 || Name.toLowerCase().indexOf($('#txtFindGo').val().trim().toLowerCase()) != -1 || Status.toLowerCase().indexOf($('#txtFindGo').val().trim().toLowerCase()) != -1 || CounterType.toLowerCase().indexOf($('#txtFindGo').val().trim().toLowerCase()) != -1) {
        //        SummaryTable = SummaryTable + "<tr onclick='DoubleClick(this)'><td style='display:none'>" + result[i].Counter_ID + "</td><td>" + result[i].Code + "</td><td>" + result[i].Name + "</td><td>" + result[i].CounterType.split('/')[1] + "</td><td>" + result[i].Status + "</td></tr>";
        //    }
        //}
        //$('#tblSummaryGrid tbody').remove();
        //SummaryTable += "<tbody>" + SummaryTable + "</tbody>";
        //$('#tblSummaryGrid').append(SummaryTable);


        //$('#txtFindGo').val('');
        //Paging

        //GetPaging1();
        GetSummaryGridData('FindGo');

    }
})


//Filters
$('#btnAll').click(function () {
    window.localStorage.setItem('ctCheck', 1);
    $("#btnCancel").click();
    GetSummaryGridData('Summary');
});
$('#btnToday').click(function () {
    window.localStorage.setItem('ctCheck', 1);
    $("#btnCancel").click();
    FilterSummaryGrid('Today', 'CREATEDDATE')
});
$('#btnYesterday').click(function () {
    window.localStorage.setItem('ctCheck', 1);
    $("#btnCancel").click();
    FilterSummaryGrid('Yesterday', 'CREATEDDATE')
});
$('#btnLast7days').click(function () {
    window.localStorage.setItem('ctCheck', 1);
    $("#btnCancel").click();
    FilterSummaryGrid('Last7', 'CREATEDDATE')

});
$('#btnCurrentMonth').click(function () {
    window.localStorage.setItem('ctCheck', 1);
    $("#btnCancel").click();
    FilterSummaryGrid('CMonth', 'CREATEDDATE')
});
$('#divActives').click(function () {
    window.localStorage.setItem('ctCheck', 1);
    $("#btnCancel").click();
    FilterSummaryGrid('Active', 'Status')
});
$('#divInActives').click(function () {
    window.localStorage.setItem('ctCheck', 1);
    $("#btnCancel").click();
    FilterSummaryGrid('Inactive', 'Status')
});
function Save() {
    window.localStorage.setItem('ctCheck', 1);
    if ($('#hdnTransId').val() != "") {
        $("#btnCreate").attr('style', 'display:block;');
        $("#ShowMenuModes").attr('style', 'display:none;');
        $("#SummaryGrid").attr('style', 'display:block;');
        $("#NewPage").attr('style', 'display:none;');
        $("#divNavBarRight").attr('style', 'display:block;');
        //$('#trSBInnerRow li span#lblLastUpdate').html("");
        $('#lblLastUpdate').css("display", "none");
        $('#trSBInnerRow li span#lblLastUpdatedUser').html("");
        $('#trSBInnerRow li span#lblLastUpdatedDate').html("");
        //$('#trSBInnerRow li span#lblMode').html("Default");
        $('#trSBInnerRow li span#lblMode').html('');
    }
    Jsonobj = [];
    var item = {};
    item["TransId"] = $('#hdnTransId').val();
    item["SubOrgID"] = $('#hdnBranchId').val();
    item["CompanyId"] = $('#CompanyId').val();
    item["CounterNumber"] = $('#txtCounterCode').val();
    item["CounterName"] = $('#txtCounterName').val();
    item["AliasName"] = $('#txtAliasName').val();
    item["DESCRIPTION"] = $("#txtDescription").val();
    item["PHONE_NO"] = $('#txtPhoneNo').val();
    item["UserId"] = $('#hdnUserId').val();
    item["OBS"] = $('#hdnOBS').val();
    item["LANGID"] = $('#hdnLanguage').val();
    item["LASTUPDATEDDATE"] = $('#hdnLastUpdatedDate').val();
    if ($("#txtCounterCode").val().trim() == '' && $("#hdnMode").val() == 'New' && $('#hdnCounterNo').val() == 'Y') {
        //  GenerateCode(strOrgCode, strVarCode, strParamType, strTable, strColumn, USERID, BRANCHID, COMPANYID, LANGUAGE);
        var HashVal = (window.localStorage.getItem("hashValues"));
        var HashValues;
        if (HashVal != null && HashVal != '') {
            HashValues = HashVal.split('~');
        }
        //AutoCode
        var icode = 'POSCOUDF';
        var strTable = 'RATABLE013';
        var strColumn = 'COLUMN003';
        GenerateCode(HashValues[1], icode, 'Y', strTable, strColumn, HashValues[14], HashValues[1], HashValues[5], HashValues[7]);
    }
    if ($('#hdnScreenMode').val() == "Active") {
        item["Status"] = "Y";
    }
    else if ($('#hdnScreenMode').val() == "InActive") {
        item["Status"] = "N";
    }
    else if ($('#hdnScreenMode').val() == "") {
        item["Status"] = "Y";
    }
    var vd_= window.localStorage.getItem("UUID");
    //var vdSplit_ = window.location.pathname.split('/');
    //if (vdSplit_.length > 2) {
    //    vd_ = vdSplit_[1];
    //}
    item["VD"] = vd_;
    Jsonobj.push(item);
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.CounterSave,
        data: "CounterTypeId=" + $('#ddlCounterType option:selected').val() + "&ItemsData= " + JSON.stringify(Jsonobj),
        success: function (resp) {
            var result = resp.split(',');
            if (result[0] == "100000") {
                if ($('#hdnScreenMode').val() == "Active") {
                    //var $textAndPic = $('<div></div>');
                    //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-info-circle info-alertSuc"></i>');
                    ////$textAndPic.append('<div class="alert-message">' + $('#txtCounterCode').val() + " Record Activated Successfully" + '</div>');
                    //$textAndPic.append('<div class="alert-message">' + $('#txtCounterCode').val() + ' ' + $('#hdnRecActSucc').val() + '</div>');
                    //BootstrapDialog.alert($textAndPic);
                    navigator.notification.alert($('#txtCounterCode').val() + ' ' + $('#hdnRecActSucc').val(), "", "neo ecr", "Ok");

                }
                else if ($('#hdnScreenMode').val() == "InActive") {
                    //var $textAndPic = $('<div></div>');
                    //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-info-circle info-alertSuc"></i>');
                    ////$textAndPic.append('<div class="alert-message">' + $('#txtCounterCode').val() + " Record Inactivated Successfully" + '</div>');
                    //$textAndPic.append('<div class="alert-message">' + $('#txtCounterCode').val() + ' ' + $('#hdnRecInactSucc').val() + '</div>');
                    //BootstrapDialog.alert($textAndPic);
                    navigator.notification.alert($('#txtCounterCode').val() + ' ' + $('#hdnRecInactSucc').val(), "", "neo ecr", "Ok");

                }
                else {
                    //var $textAndPic = $('<div></div>');
                    //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-info-circle info-alertSuc"></i>');
                    ////$textAndPic.append('<div class="alert-message">' + $('#txtCounterCode').val() + " Record saved Successfully" + '</div>');
                    //$textAndPic.append('<div class="alert-message">' + $('#txtCounterCode').val() + ' ' + $('#hdnRecSaveSucc').val() + '</div>');
                    //BootstrapDialog.alert($textAndPic);
                    navigator.notification.alert($('#txtCounterCode').val() + ' ' + $('#hdnRecSaveSucc').val(), "", "neo ecr", "Ok");

                }

                clearControls();
                GetSummaryGridData('Summary');

            }
            if (result[0] == "100001") {
                //var $textAndPic = $('<div></div>');
                //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                ////$textAndPic.append('<div class="alert-message">' + $('#txtCounterCode').val() + " Code already exists" + '</div>');
                //$textAndPic.append('<div class="alert-message">' + $('#txtCounterCode').val() + ' ' + $('#hdnCodeAlreadyExist').val() + '</div>');
                //BootstrapDialog.alert($textAndPic);
                navigator.notification.alert($('#txtCounterCode').val() + ' ' + $('#hdnCodeAlreadyExist').val(), "", "neo ecr", "Ok");

            }
        },
        error: function (e) {
            //var $textAndPic = $('<div></div>');
            //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
            ////$textAndPic.append('<div class="alert-message">Invalid Data</div>');
            //$textAndPic.append('<div class="alert-message">' + $('#hdnInvalData').val() + '</div>');
            //BootstrapDialog.alert($textAndPic);
            navigator.notification.alert( $('#hdnInvalData').val(), "", "neo ecr", "Ok");

        }
    });

    $('#PrimeInfo').addClass('open');
    $('#divPrimeInformation').attr('style', 'display:block');
}
$("#btnActive").click(function () {
    window.localStorage.setItem('ctCheck', 1);
    $('#hdnScreenMode').val("Active");
    Save();
});
//InActive Record
$("#btnInActive").click(function () {
    $('#hdnScreenMode').val("InActive");
    Save();
});
$("#btnCreate").click(function () {
    clearControls();
    defaultdata();
    //GetAutoCodeGen($('#hdnBranchId').val(), '3');
    $('#lblLastUpdate').css("display", "none");
    //$('#trSBInnerRow li span#lblLastUpdate').html("");
    $('#trSBInnerRow li span#lblLastUpdatedUser').html("");
    $('#trSBInnerRow li span#lblLastUpdatedDate').html("");
    //$('#trSBInnerRow li span#lblMode').html("New");
    $('#trSBInnerRow li span#lblMode').html($('#hdnNew').val());
    $('#hdnMode').val('New');
    $('#txtCounterCode').removeAttr("disabled", "disabled");
    $('#txtCounterName').removeAttr("disabled", "disabled");
    $("#txtDescription").removeAttr("disabled", "disabled");
    $('#txtPhoneNo').removeAttr("disabled", "disabled");
    $('#ddlCounterType').removeAttr("disabled", "disabled");
    $("#btnCreate").attr('style', 'display:none;');
    $("#ShowMenuModes").attr('style', 'display:block;');
    $("#btnSave").attr('style', 'display:block;');
    $("#btnEdit").attr('style', 'display:none;');
    $("#btnDelete").attr('style', 'display:none;');
    $("#btnCancel").attr('style', 'display:block;');
    $("#SummaryGrid").attr('style', 'display:none;');
    $("#NewPage").attr('style', 'display:block;');
    $("#divStatusGroup").attr('style', 'display:none;');
    $("#divNavBarRight").attr('style', 'display:none;');
    var counterN = $('#hdnCounterNo').val();
    if ($("#hdnMode").val() == 'New' && counterN == 'Y') {
        $('#txtCounterCode').prop("disabled", true);  // Document No. Text Box
    }
    else {
        $('#txtCounterCode').prop("disabled", false);
    }

    $('#PrimeInfo').addClass('open');
    $('#divPrimeInformation').attr('style', 'display:block');
});
$("#btnCancel").click(function () {
    clearControls();
    $('#lblLastUpdate').css("display", "none");
    //$('#trSBInnerRow li span#lblLastUpdate').html("");
    $('#trSBInnerRow li span#lblLastUpdatedUser').html("");
    $('#trSBInnerRow li span#lblLastUpdatedDate').html("");
    //$('#trSBInnerRow li span#lblMode').html("Default");
    $('#trSBInnerRow li span#lblMode').html('');
    $("#btnCreate").attr('style', 'display:block;');
    $("#ShowMenuModes").attr('style', 'display:none;');
    $("#SummaryGrid").attr('style', 'display:block;');
    $("#NewPage").attr('style', 'display:none;');
    $("#divNavBarRight").attr('style', 'display:block;');
    //$("#btnSave").attr('style', 'display:none;');
    //$("#btnEdit").attr('style', 'display:none;');
    //$("#btnCancel").attr('style', 'display:none;');
    //$("#PrimeInfo").addClass("active-link");
    //$("#ShippingInfo").addClass("deactive-link");
});
$('#btnSave').click(function () {
    if ($("#txtCounterCode").prop("disabled") == false && $('#txtCounterCode').val() == "") {
        //var $textAndPic = $('<div></div>');
        //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        ////$textAndPic.append('<div class="alert-message">Please enter Code</div>');
        //$textAndPic.append('<div class="alert-message">' + $('#hdnEntrCode').val() + '</div>');
        //BootstrapDialog.alert($textAndPic);
        navigator.notification.alert($('#hdnEntrCode').val(), "", "neo ecr", "Ok");

        return false;
    }
    else if ($('#txtCounterName').val() == "" && $('#txtCounterName').val().trim().length == 0) {
        //var $textAndPic = $('<div></div>');
        //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        ////$textAndPic.append('<div class="alert-message">Please enter Name</div>');
        //$textAndPic.append('<div class="alert-message">' + $('#hdnEntrName').val() + '</div>');
        //BootstrapDialog.alert($textAndPic);
        navigator.notification.alert($('#hdnEntrName').val(), "", "neo ecr", "Ok");

        return false;
    }

    else if ($('#ddlCounterType option:selected').val() == "0") {
        //var $textAndPic = $('<div></div>');
        //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        ////$textAndPic.append('<div class="alert-message">Please select Type</div>');
        //$textAndPic.append('<div class="alert-message">' + $('#hdnSelType').val() + '</div>');
        //BootstrapDialog.alert($textAndPic);
        navigator.notification.alert($('#hdnSelType').val(), "", "neo ecr", "Ok");

        return false;
    }
    else if ($('#').val() == "") {
        //var $textAndPic = $('<div></div>');
        //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        ////$textAndPic.append('<div class="alert-message">Please enter Phone No.</div>');
        //$textAndPic.append('<div class="alert-message">' + $('#hdnSelPhoneNo').val() + '</div>');
        //BootstrapDialog.alert($textAndPic);
        navigator.notification.alert($('#hdnSelPhoneNo').val(), "", "neo ecr", "Ok");

        return false;
    }

    else {
        Save();
    }
});
$('#btnEdit').click(function () {
    Edit();
});
$('#btnDelete').click(function () {
    Delete();
});

$('#hdnCounterNo').val();
function GetAutoCodeGen(str, iScreenCode) {  //str Suborrganization , icode PageCode
    //icode = '18';
    //str = $('#hdnBranchId').val();
    var vd_ = window.localStorage.getItem("UUID");
    //var vdSplit_ = window.location.pathname.split('/');
    //if (vdSplit_.length > 2) {
    //    vd_ = vdSplit_[1];
    //}
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetAutoGenHashTableCheck,
        data: "SubOrgId=" + str + "&iScreenCode=" + iScreenCode + "&VD=" + vd_,
        success: function (resp) {
            var CounterNo = resp;
            if (CounterNo.length == 1) {
                $('#hdnCounterNo').val(CounterNo[0].PageName);
            }
        },
        error: function (e) {
        }
    });
}
function GenerateCode(strOrgCode, strVarCode, strParamType, strTable, strColumn, USERID, BRANCHID, COMPANYID, LANGUAGE) {
    var vd_ = window.localStorage.getItem("UUID");
    //var vdSplit_ = window.location.pathname.split('/');
    //if (vdSplit_.length > 2) {
    //    vd_ = vdSplit_[1];
    //}
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GenerateCode,
        data: "strOrgCode=" + strOrgCode + "&strVarCode=" + strVarCode + "&strParamType=" + strParamType + "&strTable=" + strTable + "&strColumn=" + strColumn + "&USERID=" + USERID + "&BRANCHID=" + BRANCHID + "&COMPANYID=" + COMPANYID + "&LANGUAGE=" + LANGUAGE + "&VD=" + vd_,

        success: function (resp) {
            var CounterNo = resp;
            if (CounterNo.length == 1) {
                $('#txtCounterOpeningNo').val(CounterNo[0].PageName);
            }
        },
        error: function (e) {
        }
    });
}
function getResources(langCode, pageCode) {

    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.getResource,
        data: "langCode=" + langCode + "&pageCode=" + pageCode,
        success: function (resp) {
            setLabels(resp);
        }
    });
}

function setLabels(labelData) {
    jQuery("label[for='lblUploadUserImage'").html(labelData["UploadUserImage"]);
    jQuery("label[for='lblSubcription'").html(labelData["Renewal"]);
    jQuery("label[for='lblLocationwiseProfitAnalysis'").html(labelData["LocwiseProfitAnlys"]);
    jQuery("label[for='lblItemCategoryProfitAnalysis'").html(labelData["ItemCatgPrftAnlys"]);
    jQuery("label[for='lblSCPopPwd'").html(labelData["pSettings"]);
    jQuery("label[for='lblLanguage'").html(labelData["lang"]);
    jQuery("label[for='lblDateFormat'").html(labelData["format"]);
    jQuery("label[for='lblPopOk'").html(labelData["ok"]);
    jQuery("label[for='lblPopClose'").html(labelData["cancel"]);
    $('#txtFindGo').attr('placeholder', labelData["searchHere"]);
    jQuery("label[for='lblSettings'").html(labelData["settings"]);
    jQuery("label[for='lblThemes'").html(labelData["themes"]);
    jQuery("label[for='lblTheme1'").html(labelData["theme1"]);
    jQuery("label[for='lblTheme2'").html(labelData["theme2"]);
    jQuery("label[for='lblTheme3'").html(labelData["theme3"]);
    jQuery("label[for='lblTheme4'").html(labelData["theme4"]);
    jQuery("label[for='lblChangePassword'").html(labelData["changePassword"]);
    jQuery("label[for='lblTill'").html(labelData["till"]);
    jQuery("label[for='lblCreate'").html(labelData["create"]);
    jQuery("label[for='lblSave'").html(labelData["save"]);
    jQuery("label[for='lblEdit'").html(labelData["edit"]);
    jQuery("label[for='lblDelete'").html(labelData["delete"]);
    jQuery("label[for='lblCancel'").html(labelData["cancel"]);
    jQuery("label[for='lblStatus'").html(labelData["status"]);
    jQuery("label[for='lblToggleDropdown'").html(labelData["toggleDropdown"]);
    jQuery("label[for='lblActive'").html(labelData["active"]);
    jQuery("label[for='lblInactive'").html(labelData["inactive"]);
    jQuery("label[for='lblFilter'").html(labelData["filter"]);
    jQuery("label[for='lblToggleDropdown'").html(labelData["toggleDropdown"]);
    jQuery("label[for='lblAll'").html(labelData["all"]);
    jQuery("label[for='lblToday'").html(labelData["toDay"]);
    jQuery("label[for='lblYesterDay'").html(labelData["yesterDay"]);
    jQuery("label[for='lblLastSevenDays'").html(labelData["lastSevenDays"]);
    jQuery("label[for='lblCurrentMonth'").html(labelData["currentMonth"]);
    jQuery("label[for='lblToggleActive'").html(labelData["active"]);
    jQuery("label[for='lblToggleInactive'").html(labelData["inactive"]);
    jQuery("label[for='lblConfigurators'").html(labelData["configurators"]);
jQuery("label[for='lblSubConfigurators'").html(labelData["POSEwalletConSts"]);
jQuery("label[for='lblInvoiceSummary'").html(labelData["category"]);
    jQuery("label[for='lblPrintCustomize'").html(labelData["printCustamize"]);
    jQuery("label[for='lblBarcodeGenerator'").html(labelData["barCodeGenerator"]);
    jQuery("label[for='lblMasters'").html(labelData["masters"]);
    jQuery("label[for='lblTillType'").html(labelData["tillType"]);
    jQuery("label[for='lblTill'").html(labelData["till"]);
    jQuery("label[for='lblTillPermissions'").html(labelData["tillPermissions"]);
    jQuery("label[for='lblCustomer'").html(labelData["customer"]);
    jQuery("label[for='lblTenderType'").html(labelData["tenderType"]);
    jQuery("label[for='lblTransactions'").html(labelData["transactions"]);
    jQuery("label[for='lblInvoce'").html(labelData["invoice"]);
    jQuery("label[for='lblSalesReturn'").html(labelData["salesReturn"]);
    jQuery("label[for='lblPosting'").html(labelData["posting"]);
    jQuery("label[for='lblTillOpening'").html(labelData["tillOpening"]);
    jQuery("label[for='lblTillClosing'").html(labelData["tillClosing"]);
    jQuery("label[for='lblAccountPosting'").html(labelData["accountPosting"]);
    jQuery("label[for='lblReports'").html(labelData["reports"]);
    jQuery("label[for='lblInvoiceListing'").html(labelData["invoiceListing"]);
    jQuery("label[for='lblTillInvoiceListing'").html(labelData["tillInvoiceListing"]);
    jQuery("label[for='lblTenderTypeWiseCollectionDailySummary'").html(labelData["tenderTypeWiseCollectionDailySummary"]);
    jQuery("label[for='lblSalesReturns'").html(labelData["salesReturns"]);
    jQuery("label[for='lblCode'").html(labelData["code"]);
    jQuery("label[for='lblName'").html(labelData["name"]);
    jQuery("label[for='lblType'").html(labelData["type"]);
    jQuery("label[for='lblProcessStatus'").html(labelData["processStatus"]);
    jQuery("label[for='lblPrimeInformation'").html(labelData["primeInformation"]);
    jQuery("label[for='lblOrganization'").html(labelData["subOrganization"]);
    jQuery("label[for='lblDetCode'").html(labelData["code"]);
    jQuery("label[for='lblDetName'").html(labelData["name"]);
    jQuery("label[for='lblDetType'").html(labelData["type"]);
    jQuery("label[for='lblPhone'").html(labelData["phone"]);
    jQuery("label[for='lblDescription'").html(labelData["description"]);
    jQuery("label[for='lblSCPopPwd'").html(labelData["pSettings"]);
    jQuery("label[for='lblDtFormat'").html(labelData["format"]);
    jQuery("label[for='lblOk'").html(labelData["ok"]);
    jQuery("label[for='lblClose'").html(labelData["closed"]);
    jQuery("label[for='lblLanguage'").html(labelData["lang"]);
    jQuery("label[for='lblLoyaltyPoints'").html(labelData["LoyaltyPoints"]);
    jQuery("label[for='lblDispatch'").html(labelData["Dispatch"]);
    $('#lblNavigationUrl').text(labelData["pos"] + "  >>  " + labelData["masters"] + "  >>  " + labelData["till"]);
    $('#lblLastUpdate').text(labelData["lastUpdated"]);
    $('#lblLastUpdate').css("display", "none");
    jQuery("label[for='lblAnalyticalReports'").html(labelData["AnalyticalReports"]);
    jQuery("label[for='lblItemCategorySalesAnalysis'").html(labelData["ItemCategorySalesAnalysis"]);
    jQuery("label[for='lblWeekDaysRevenue'").html(labelData["WeekdaysRevenue"]);
    jQuery("label[for='lblBucketAnalysis'").html(labelData["BucketAnalysis"]);
    jQuery("label[for='lblLocationwiseAverageInvoiceValue'").html(labelData["LocationwiseAverageInvoiceValue"]);
    jQuery("label[for='lblItem'").html(labelData["item"]);
    jQuery("label[for='lblReceipt'").html(labelData["receipt"]);
    jQuery("label[for='lblVATReturns'").html(labelData["VATReturns"]);
    jQuery("label[for='lblUserPermissions'").html(labelData["UserPermissions"]);
    jQuery("label[for='lblPrintPoleConfig'").html(labelData["PrintPoleConfig"]);
    jQuery("label[for='lblCurrentInventory'").html(labelData["CurrentInventory"]);
    $('#hdnNPermInv').val(labelData["alert1"]);
    $('#hdnNAsnCounterTran').val(labelData["alert2"]);
    $('#hdnInvalData').val(labelData["alert3"]);
    $('#hdnContactAdmin').val(labelData["alert4"]);
    $('#hdnRecSaveSucc').val(labelData["alert8"]);
    $('#hdnRecActSucc').val(labelData["alert6"]);
    $('#hdnRecInactSucc').val(labelData["alert7"]);
    $('#hdnCodeAlreadyExist').val(labelData["alert9"]);
    $('#hdnRecDelSucc').val(labelData["alert5"]);
    $('#hdnHaveRefRec').val(labelData["reference"]);
    $('#hdnEntrCode').val(labelData["alert10"]);
    $('#hdnEntrName').val(labelData["alert11"]);
    $('#hdnSelType').val(labelData["alert12"]);
    $('#hdnSelPhoneNo').val(labelData["plzFill"] + labelData["phone"]);
    $('#hdnChPwdSaveSucc').val(labelData["alert8"]);
    $('#hdnThameChange').val(labelData["alert13"]);
    $('#hdnNew').val(labelData["new1"]);
    $('#hdnView').val(labelData["view"]);
    $('#hdnEdit').val(labelData["edit"]);
    $('#hdnDefault').val(labelData["default1"]);
    $('#hdnNotAssignCountrToTrans').val(labelData["permissionsAssisted"]);
    $('#hdnDeleteConfirm').val(labelData["wantToDelete"]);
    //jQuery("label[for='lblOk'").html(labelData[""]);
    //jQuery("label[for='lblOk'").html(labelData[""]);
    //jQuery("label[for='lblOk'").html(labelData[""]);
    $('#hdnSelect').val(labelData["select"]);
    $('#hdnImageSave').val(labelData["ImageSave"]);
    jQuery("label[for='lblUpload'").html(labelData["upload"]);
    jQuery("label[for='lblDetAliasName'").html(labelData["Alias"]);
$('#hdnSelImage').val(labelData["SelUserImage"]);
jQuery("label[for='lblCustLDGR'").html(labelData["PosCustLdger"]);

}