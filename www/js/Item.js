﻿$("#divNavBarRight").attr('style', 'display:block;');
//$('#txtTaxValue').val(5);
//$("#txtTaxValue").val(15);
//Page Load Function
function MainCounterData() {
    window.localStorage.setItem('ctCheck', 1);
    if (window.localStorage.getItem('network') == '1') {
    GetLoginUserName();
    defaultdata();
    $('#hdnPageIndex').val('1');
    $('#hdnFilterType').val('Summary');
    GetSummaryGridData('Summary'); 
    $('#lblLastUpdate').css("display", "none");
    document.getElementById("inp").addEventListener("change", readFile);
    document.getElementById("PPinp").addEventListener("change", readFilePP); 
    }
    else {
        window.open('DashBoard.html', '_self', 'location=no');
    }


}
$('#linkDownloadPath').click(function () {

 
    window.open(CashierLite.Settings.fieDownloadPath, "_self", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=400,height=400");
})
 
function openFileOption() {
    window.localStorage.setItem('ctCheck', 1);
    document.getElementById("inp").click();
}
function openFileOptionPP() {
    window.localStorage.setItem('ctCheck', 1);
    $('#tblGrid tbody').children().remove();
    $('#lblitemsCount')[0].textContent = "";
    $('#ModalUploadPopup').modal('show'); 
}
function openFileOptionPPOpen() {
    window.localStorage.setItem('ctCheck', 1);
    document.getElementById("PPinp").click();
}
function readFile() {
    window.localStorage.setItem('ctCheck', 1);
    if (this.files && this.files[0]) {
        var FR = new FileReader();
        FR.onload = function (e) {

            document.getElementById("img").src = e.target.result;
            document.getElementById("b64").innerHTML = e.target.result;
        } 
        FR.readAsDataURL(this.files[0]);
    }

}
function readFilePP() {
    window.localStorage.setItem('ctCheck', 1);
    if (this.files && this.files[0]) {
        var FR = new FileReader();
       // FR.addEventListener("load", function (e) {
            FR.onload = function (e) {
            document.getElementById("PPb64").innerHTML = e.target.result;
            uploadData();
        }
        FR.readAsDataURL(this.files[0]);

    }

}
function defaultdata() {
    window.localStorage.setItem('ctCheck', 1);
    var Paging = ""; var CompanyId = ""; var SubOrgId = ''; var Orgcode = ''; var OBS = ''; var CashierID = window.localStorage.getItem("CashierID"); var Userid = '';
    var Lang = '';
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        getResources(hashSplit[7], "POSCounterDefination");
        CompanyId = hashSplit[0];
        SubOrgId = hashSplit[1];
        OBS = hashSplit[9];
        Orgcode = hashSplit[17];
        Userid = hashSplit[14];
        Lang = hashSplit[7];
        Paging = hashSplit[19];
    }
    $('#CompanyId').val(CompanyId);
    $('#hdnBranchId').val(SubOrgId);
    $('#hdnOBS').val(OBS);
    $('#hdnUserId').val(Userid);
    //$('#hdnBranchId').val("004c6087-45a7-468b-bdd8-46b4a1dcf647");
    $('#txtPosCode').val(Orgcode);
    $('#hdnLanguage').val(Lang);
    $('#hdnPaging').val(Paging);
    $('#Content_two').css("display", "none");
    $('#Create').css("display", "block");
    // $('#Create').css("display", "block");
    $('#Edit').css("display", "none");
    $('#Cancel').css("display", "none");
    $('#btnSave').css("display", "none");
    $('#Delete').css("display", "none");
    $('#Status').css("display", "none");
    $('#divActive').css("display", "none");


}

function taxApplicabilty_Item() {
    window.localStorage.setItem('ctCheck', 1);
    if ($('#ChckTaxAp').prop('checked') == true) {
        $('#ChckTaxOnMRPAp').prop('checked', true);
        $('#ChckTaxOnMRPAp').prop('disabled', false);
        $('#lblTaxValue').css('display', 'block');
        $('#txtTaxValue').css('display', 'block');
        $('#txtTaxValue').val(15);
    }
    else {
        $('#ChckTaxOnMRPAp').prop('checked', false);
        //$("#ChckTaxOnMRPAp").attr("disabled", "disabled");
        $('#ChckTaxOnMRPAp').prop('disabled', true);
        $('#lblTaxValue').css('display', 'none');
        $('#txtTaxValue').css('display', 'none');

    }
}

function IsNumeric_Item(e) {
    window.localStorage.setItem('ctCheck', 1);
    var specialKeys = new Array();
    specialKeys.push(8);
    var keyCode = e.which ? e.which : e.keyCode

    var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1 || keyCode == 46);
    if (ret == false) {
        return ret;
    }



    var input_ = $("#txtPrice").val();
    if (keyCode == 46) {
        if (input_.indexOf('.') != -1) {
            return false;
        }
        if (input_.split('.')[0].length == 0) {

            return false;
        }

    }
    var position_ = e.target.selectionStart;
    var inuputArray_ = input_.split('');


    input_ = getKeypressValues(inuputArray_, e.key, position_)

    if (input_.indexOf('.') == -1) {
        if (input_.length <= 8) {
            return true;
        }
        if (keyCode == 46)
            return true;

        else {
            return false;

        }

    }
    else {
        if (input_.split('.')[0].length <= 8 && input_.split('.')[1].length <= 2) {
            return true;
        } else return false;



    }

}
function IsNumericKey(e) {
    var specialKeys = new Array();
    specialKeys.push(8);
    var keyCode = e.which ? e.which : e.keyCode

    var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1 || keyCode == 46);
    if (ret == false) {
        return ret;
    }
    else {

    }

}
function VD1() {
    window.localStorage.setItem('ctCheck', 1);
    var vd_ = window.localStorage.getItem("UUID");;
   
    return vd_;
}
 
$("#txtUOM").autocomplete({
    source: function (request, response) {
        var HashValues = (window.localStorage.getItem("hashValues"));
        if (HashValues != null && HashValues != '') {
            var hashSplit = HashValues.split('~');
            SubOrgId = hashSplit[1];
        }
        var param = "VD=" + VD1() + "&searchkey=" + $('#txtUOM').val() + "";
        $.ajax({
            url: CashierLite.Settings.GetUOM,
            data: param,
            dataType: "json",
            type: "Get",
            //contentType: "application/json; charset=utf-8",
            success: function (data) {
                window.localStorage.setItem('ctCheck', 1);
                response($.map(data, function (item) {
                    return {
                        label: item.UOMCode + '/' + item.UOMName,
                        val: item.UOMID
                    }
                }))
            },
            error: function (response) {
            },
            failure: function (response) {
            }
        });

    },
    change: function (e, i) {
        if (i.item) {

        }
        else {
            $('#txtUOM').val('');
            $('#hdnUOMId').val('');

        }
    },
    select: function (e, i) {
        $('#hdnUOMId').val(i.item.val);


    },
    minLength: 1
});



function GetLoginUserName() {
    window.localStorage.setItem('ctCheck', 1);
    var sess = CashierLite.Session.getInstance().get();
    if (sess != null) {
        if (sess.userName != "") {
            document.getElementById("lblName").innerHTML = sess.userName;
        }
        else {
            window.open('Login.html', '_self', 'location=no');
        }
    }
    else {
        window.open('Login.html', '_self', 'location=no');
    }
    theamLoad();
}
function theamLoad() {
    window.localStorage.setItem('ctCheck', 1);
    if (window.localStorage.getItem("Theam") != null)
        $('head').append('<link rel="stylesheet" href=' + window.localStorage.getItem("Theam") + ' type="text/css" />');
}

//For Invoice Link
function GetShiftData() {
    window.localStorage.setItem('ctCheck', 1);
    if (window.localStorage.getItem("CashierID") != null) {

        var SubOrgId = ''; var CashierID = window.localStorage.getItem("CashierID");
        var HashValues = (window.localStorage.getItem("hashValues"));
        if (HashValues != null && HashValues != '') {
            var hashSplit = HashValues.split('~');
            SubOrgId = hashSplit[1];
        }
        var vd_ = window.localStorage.getItem("UUID");;
         
        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.ShiftCheck,
            data: "CashierID=" + CashierID + "&OrgId=" + SubOrgId + "&VD=" + vd_,
            success: function (resp) {
                var result = resp;
                if (result.length > 0) {
                    var ACTIVE = result[0].ACTIVE;
                    var COUNTERID = result[0].COUNTERID;
                    var COUNTERNAME = result[0].COUNTERNAME;
                    var COUNTERNUMBER = result[0].COUNTERNUMBER;
                    var COUNTERTYPE = result[0].COUNTERTYPE;
                    var End_Time = result[0].End_Time;
                    var Is_Next_Day = result[0].Is_Next_Day;
                    var SHIFTID = result[0].SHIFTID;
                    var Shift_Code = result[0].Shift_Code;
                    var Shift_Name = result[0].Shift_Name;
                    var Start_Time = result[0].Start_Time;
                    var StageAlert = result[0].StageAlert;

                    window.localStorage.setItem('ACTIVE', result[0].ACTIVE);
                    window.localStorage.setItem('COUNTERID', result[0].COUNTERID);
                    window.localStorage.setItem('COUNTERNAME', result[0].COUNTERNAME);
                    window.localStorage.setItem('COUNTERNUMBER', result[0].COUNTERNUMBER);
                    window.localStorage.setItem('COUNTERTYPE', result[0].COUNTERTYPE);
                    window.localStorage.setItem('End_Time', result[0].End_Time);
                    window.localStorage.setItem('Is_Next_Day', result[0].Is_Next_Day);
                    window.localStorage.setItem('SHIFTID', result[0].SHIFTID);
                    window.localStorage.setItem('Shift_Code', result[0].Shift_Code);
                    window.localStorage.setItem('Shift_Name', result[0].Shift_Name);
                    window.localStorage.setItem('Start_Time', result[0].Start_Time);
                    var StageAlert = result[0].StageAlert;
                    if (StageAlert == '1') {
                        if (ACTIVE.toLowerCase() == 'y') {
                            window.open('posmain.html', '_self', 'location=no');
                        }
                        else {
                           

                            navigator.notification.alert($('#hdnNPermInv').val(), "", "neo ecr", "Ok");
                        }
                    }
                    else if (StageAlert == '2') {
                      
                        navigator.notification.alert($('#hdnNAsnCounterTran').val(), "", "neo ecr", "Ok");

                    }
                }
                else {
                    
                    navigator.notification.alert($('#hdnNPermInv').val(), "", "neo ecr", "Ok");

                }
            },
            error: function (e) { 
                navigator.notification.alert($('#hdnInvalData').val(), "", "neo ecr", "Ok");

            }
        });
    }
    else { 
        navigator.notification.alert($('#hdnContactAdmin').val(), "", "neo ecr", "Ok");

    }


}
function GetCounterType() {
    window.localStorage.setItem('ctCheck', 1);
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        SubOrg_Id = hashSplit[1];
    }
    var vd_ = window.localStorage.getItem("UUID");;
   
    Jsonobj = [];
    var SubOrg = {};
    SubOrg.SubOrgID = $('#hdnBranchId').val();
    SubOrg.VD = vd_;
    Jsonobj.push(SubOrg);

    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetCounterTypes,
        data: "SubOrg=" + JSON.stringify(Jsonobj),
        success: function (resp) {
            var CounterData = resp;
            document.getElementById("ddlCounterType").options.length = 0;
            $("#ddlCounterType").append($("<option value=''  selected ></option>").val(0).html('--' + $("#hdnSelect").val() + '--'));//this is for adding select after clearing
            var MainData = '';
            jsonObj = [];
            for (var i = 0; i < CounterData.length; i++) {
                var CounterId = CounterData[i].CounterID;
                var CounterName = CounterData[i].Code + "/" + CounterData[i].Name;
                 
                item = {}
                item["code"] = CounterId;
                item["name"] = CounterName;

                jsonObj.push(item);
                $("#ddlCounterType").append($("<option></option>").val(CounterId).html(CounterName));
            };
            //window.localStorage.setItem("FillCustomer", jsonObj);
        },
        error: function (e) { 
            navigator.notification.alert($('#hdnInvalData').val(), "", "neo ecr", "Ok");

        }
    });

}
 
$(document).ready(function () {

});
//Summary grid click function
function DoubleClick(tr) {
    window.localStorage.setItem('ctCheck', 1);
    var id = $(tr).find('td:first').html();
    var vd_ = window.localStorage.getItem("UUID");;
     
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetAllItems,
        data: "ItemId=" + id + "&VD=" + vd_,
        success: function (resp) {

            if (resp.length != 0) {
 
                $('#txtCounterCode').val(resp[0].ItemCode);
                $('#txtCounterName').val(resp[0].ItemName);
                $('#txtUOM').val(resp[0].UOMCode);
                $("#txtBarcode").val(resp[0].BarCode);
                $('#hdnTransId').val(resp[0].ItemId);
                $("#txtDescription").val(resp[0].desc);
                $('#hdnUOMId').val(resp[0].UOMID);
                $('#txtTaxValue').val(resp[0].TaxAmt);
                if (resp[0].path_ != "")
                    document.getElementById("img").src = resp[0].path_;
                else
                    document.getElementById("img").src = "images/IMGItem.png"; 
                $("#txtAliasName").val(resp[0].Alias);
                $('#txtPrice').val(parseFloat(resp[0].Price).toFixed(2));

                if (resp[0].TaxApp == "Y") {
                    $('#ChckTaxAp').prop('checked', true);
                    $('#ChckTaxOnMRPAp').prop('disabled', false);
                    $('#ChckTaxOnMRPAp').prop('checked', false);
                    $('#lblTaxValue').css('display', 'block');
                    $('#txtTaxValue').css('display', 'block');
                }
                else {
                    $('#ChckTaxAp').prop('checked', false);
                    $('#ChckTaxOnMRPAp').prop('disabled', true);
                    $('#ChckTaxOnMRPAp').prop('checked', false);
                    $('#lblTaxValue').css('display', 'none');
                    $('#txtTaxValue').css('display', 'none');

                }
                if (resp[0].TaxOnMRP == "R") {
                    $('#ChckTaxOnMRPAp').prop('checked', true);

                }
                else {
                    $('#ChckTaxOnMRPAp').prop('checked', false);

                }

                $('#lblLastUpdate').css("display", "block");
                $('#trSBInnerRow li span#lblLastUpdate').html($('#trSBInnerRow li span#lblLastUpdate').html());
                $('#trSBInnerRow li span#lblLastUpdatedUser').html(resp[0].LastUpdateduser);
                $('#trSBInnerRow li span#lblLastUpdatedDate').html(resp[0].LastUpdateduserDate);
                //$('#trSBInnerRow li span#lblMode').html("View");
                $('#trSBInnerRow li span#lblMode').html($('#hdnView').val());
                if (resp[0].Status != "Active") {
                    $("#btnCreate").attr('style', 'display:block;');
                    $("#ShowMenuModes").attr('style', 'display:block;');
                    $("#btnSave").attr('style', 'display:none;');
                    $("#btnEdit").attr('style', 'display:none;');
                    $("#btnDelete").attr('style', 'display:none;');
                    $("#btnCancel").attr('style', 'display:block;');
                    $("#SummaryGrid").attr('style', 'display:none;');
                    $("#NewPage").attr('style', 'display:block;');
                    $("#divStatusGroup").attr('style', 'display:block;');
                    $("#divNavBarRight").attr('style', 'display:none;');
                    $("#btnInActive").addClass("disabledHyperlink");
                    $("#btnActive").removeClass("disabledHyperlink");
                }
                else {
                    $("#btnInActive").removeClass("disabledHyperlink");
                    $("#btnActive").addClass("disabledHyperlink");
                    $("#btnCreate").attr('style', 'display:block;');
                    $("#ShowMenuModes").attr('style', 'display:block;');
                    $("#btnSave").attr('style', 'display:none;');
                    $("#btnEdit").attr('style', 'display:block;');
                    $("#btnDelete").attr('style', 'display:none;');
                    $("#btnCancel").attr('style', 'display:block;');
                    $("#SummaryGrid").attr('style', 'display:none;');
                    $("#NewPage").attr('style', 'display:block;');
                    $("#divStatusGroup").attr('style', 'display:block;');
                    $("#divNavBarRight").attr('style', 'display:none;');
                }
                $('#txtCounterCode').attr("disabled", "disabled");
                $('#txtCounterName').attr("disabled", "disabled");
                $("#txtDescription").attr("disabled", "disabled");
                $("#txtAliasName").attr("disabled", "disabled");
                $("#txtPrice").attr("disabled", "disabled");
                $('#txtTaxValue').attr("disabled", "disabled");
                $("#txtBarcode").attr("disabled", "disabled");
                $("#txtUOM").attr("disabled", "disabled");
                $('#ChckTaxOnMRPAp').prop('disabled', true);
                $('#ChckTaxAp').prop('disabled', true);
                $('#inp').prop('disabled', true); 
            }
        },
        error: function (e) { 
            navigator.notification.alert($('#hdnInvalData').val(), "", "neo ecr", "Ok");

        }
    });

    $('#PrimeInfo').addClass('open');
    $('#divPrimeInformation').attr('style', 'display:block');
}
//End
//Edit Function
function Edit() {
    window.localStorage.setItem('ctCheck', 1);
    //$('#trSBInnerRow li span#lblMode').html("Edit");
    $('#trSBInnerRow li span#lblMode').html($('#hdnEdit').val());
    $("#btnCreate").attr('style', 'display:none;');
    $("#ShowMenuModes").attr('style', 'display:block;');
    $("#btnSave").attr('style', 'display:block;');
    $("#btnEdit").attr('style', 'display:none;');
    $("#btnDelete").attr('style', 'display:none;');
    $("#btnDelete").attr('style', 'border-left:1px solid #cac8c8;');
    $("#btnCancel").attr('style', 'display:block;');
    $("#divStatusGroup").attr('style', 'display:none;');
    $('#txtCounterName').removeAttr("disabled", "disabled");
    $("#txtDescription").removeAttr("disabled", "disabled");
    //$('#txtPhoneNo').removeAttr("disabled", "disabled");
    //$('#ddlCounterType').removeAttr("disabled", "disabled");
    $("#divNavBarRight").attr('style', 'display:none;');

    $('#PrimeInfo').addClass('open');
    $('#divPrimeInformation').attr('style', 'display:block');


    $("#txtCounterName").removeAttr("disabled", "disabled");

    $("#txtAliasName").removeAttr("disabled", "disabled");
    $("#txtPrice").removeAttr("disabled", "disabled");
    $("#txtBarcode").removeAttr("disabled", "disabled");
    $("#txtUOM").attr("disabled", "disabled");
    $('#ChckTaxOnMRPAp').prop('disabled', false);
    $('#ChckTaxAp').prop('disabled', false);
    if ($('#ChckTaxAp').prop('checked') == false) {
        $('#ChckTaxOnMRPAp').prop('disabled', true);
        $('#lblTaxValue').css("dispaly", "none");
        $('#txtTaxValue').css("dispaly", "none");

    }
    else {
        $('#txtTaxValue').removeAttr("disabled", "disabled");
        //$('#txtTaxValue').val(15);
    }

    $("#inp").removeAttr("disabled", "disabled");
}
//End
//Delete function
function Delete() {
    window.localStorage.setItem('ctCheck', 1);
    if ($('#hdnTransId').val() != "") {
        $('#SummaryGrid').css("display", "block");
        $('#NewPage').css("display", "none");
        $("#btnCreate").attr('style', 'display:block;');
        $("#ShowMenuModes").attr('style', 'display:none;');
        $("#divNavBarRight").attr('style', 'display:block;');
    }
    if (confirm($('#hdnDeleteConfirm').val())) {
        var vd_ = window.localStorage.getItem("UUID");;
        //var vdSplit_ = window.location.pathname.split('/');
        //if (vdSplit_.length > 2) {
        //    vd_ = vdSplit_[1];
        //}
        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.DeleteCounter,
            data: { Counter_Id: $('#hdnTransId').val(), LastUpdatedDate: $('#hdnLastUpdatedDate').val(), VD: vd_ },
            success: function (resp) {
                var result = resp.split(',');
                if (result[0] == "100000") {

                    GetSummaryGridData('Summary'); 
                    navigator.notification.alert($('#txtCounterCode').val() + ' ' + $('#hdnRecDelSucc').val(), "", "neo ecr", "Ok");
                    clearControls_Item();

                }
            },
            error: function (e) { 
                navigator.notification.alert($('#hdnInvalData').val(), "", "neo ecr", "Ok");

            }
        });
    } else {
        return false;
    }
}
//End
function clearControls_Item() {
    window.localStorage.setItem('ctCheck', 1);
    $('#txtCounterCode').val("");
    $('#txtCounterName').val("");
    $("#txtDescription").val("");
    $('#txtBarcode').val("");
    //$('#ddlCounterType').val("0");
    $('#hdnTransId').val("");
    $('#hdnLastUpdatedDate').val("");
    $('#hdnScreenMode').val("");
    $("#txtUOM").val("");
    $("#txtAliasName").val("");
    $("#txtPrice").val("");
    $("#txtTaxValue").val(15);
    document.getElementById("b64").innerHTML = "";
    document.getElementById("img").src = "images/IMGItem.png";
    $('#inp')[0].value = ""
    $('#ChckTaxAp').prop('checked', true);
    $('#ChckTaxOnMRPAp').prop('checked', true);
    $('#ChckTaxOnMRPAp').prop('disabled', false);
    $('#lblTaxValue').css('display', 'block');
    $('#txtTaxValue').css('display', 'block');
    $('#txtUOM').val('EAC/Each');
    $('#hdnUOMId').val('1009');
}
function FilterSummaryGrid(ControlData, ColumnData) {
    window.localStorage.setItem('ctCheck', 1);
    $('#txtFindGo').val('');
    result = [];
    result = JSON.parse((window.localStorage.getItem("CounterDefFilterGrid")));
    var SummaryTable;
    if (ColumnData == 'Status') {
        $("#hdnFilterType").val(ControlData.toLowerCase());
        GetSummaryGridData(ControlData.toLowerCase())

    }
    else
        if (ColumnData == 'CREATEDDATE') {
            $("#hdnFilterType").val(ControlData);
            GetSummaryGridData(ControlData)
        }
    
}
function prevItem() {
    var pageIndexCreate = parseFloat($('#hdnPageIndex').val()) - 1;
    if (parseFloat(pageIndexCreate) == 0) {
        pageIndexCreate = '1';
    }
    $('#hdnPageIndex').val(pageIndexCreate);
    var FilterType = $("#hdnFilterType").val();
    GetSummaryGridData(FilterType);
    window.localStorage.setItem('ctCheck', 1);
}
function nextItem() {
    window.localStorage.setItem('ctCheck', 1);
    var pageIndexCreate = parseFloat($('#hdnPageIndex').val()) + 1;

    $('#hdnPageIndex').val(pageIndexCreate);
    var FilterType = $('#hdnFilterType').val();
    GetSummaryGridData(FilterType + '~Index');

}
//GetSummaryGridData
function GetSummaryGridData(FilterType) {
    window.localStorage.setItem('ctCheck', 1);
    var Index = $('#hdnPageIndex').val();
    var MethodType = "";
    var FindGo = "";
    var QryType = "";
    if (FilterType == 'FindGo') {
        FindGo = $('#txtFindGo').val();
        Index = 1;

    }
    if (FilterType == 'Summary') {
        $('#txtFindGo').val('');
    }

    else {
        FindGo = $('#txtFindGo').val();
    }
    if (FilterType.indexOf('Index') != -1) {
        FilterType = FilterType.split('~')[0];
        MethodType = "next";
    }



    // $('#txtFindGo').val('');
    var HashVal = (window.localStorage.getItem("hashValues"));
    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');
    }
    var SummaryTable = '';
    Jsonobj = [];
    var SubOrg = {};
    SubOrg.SubOrgID = $('#hdnBranchId').val();
    var vd_ = window.localStorage.getItem("UUID");;
    //var vdSplit_ = window.location.pathname.split('/');
    //if (vdSplit_.length > 2) {
    //    vd_ = vdSplit_[1];
    //}
    SubOrg.VD = vd_;
    SubOrg.Index = Index;
    SubOrg.FilterType = FilterType;
    SubOrg.FindGoVal = FindGo;
    SubOrg.item_Id = "";
    // var item_Id = "";
    Jsonobj.push(SubOrg);
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetAllItemsWithPaging,
        // data: "ItemId=" + item_Id + "&VD=" + vd_,
        data: "SubOrg=" + JSON.stringify(Jsonobj),
        success: function (resp) {
            var result = resp;
            if (result.length > 0) {
                window.localStorage.setItem("CounterDefFilterGrid", JSON.stringify(result))
                $('#tblSummaryGrid >tbody').children().remove();

                for (var i = 0; i < result.length; i++) {
                    SummaryTable += "<tr onclick='DoubleClick(this)'><td style='display:none'>" + result[i].ItemId + "</td><td>" + result[i].ItemCode + "</td><td>" + result[i].ItemName + "</td><td>" + result[i].Status + "</td></tr>";
                }

                $('#tblSummaryGrid tbody').remove();
                SummaryTable += "<tbody>" + SummaryTable + "</tbody>";
                $('#tblSummaryGrid').append(SummaryTable);
                window.localStorage.setItem("CounterDefGrid", JSON.stringify(SummaryTable));
                //GetPaging1();
                $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());
            }
            else {

                $('#tblSummaryGrid tbody').remove();
                SummaryTable += "<tbody>" + SummaryTable + "</tbody>";
                $('#tblSummaryGrid').append(SummaryTable);
                if (MethodType == "next") {
                    var pageIndexCreate = parseFloat($('#hdnPageIndex').val()) - 1;
                    $('#hdnPageIndex').val(pageIndexCreate);
                }
              
            }
        },
        error: function (e) {
           
            navigator.notification.alert($('#hdnInvalData').val(), "", "neo ecr", "Ok");

        }

    });
}

 
function GetPaging1() {
   

    let options = {
        numberPerPage: 10, //Cantidad de datos por pagina
        goBar: true, //Barra donde puedes digitar el numero de la pagina al que quiere ir
        pageCounter: true, //Contador de paginas, en cual estas, de cuantas paginas
    };

    let filterOptions = {
        el: '#searchBox' //Caja de texto para filtrar, puede ser una clase o un ID
    };

    paginate.init('#tblSummaryGrid', options, filterOptions);


    window.localStorage.setItem('ctCheck', 1);
}
$('#txtFindGo').keyup(function () {
    window.localStorage.setItem('ctCheck', 1);
    result = [];
    result = JSON.parse((window.localStorage.getItem("CounterDefFilterGrid")));
    if ($('#txtFindGo').val().trim() == "" || result.length <= 0) {

        GetSummaryGridData('Summary');
        return false;
    }
    else {
       
        GetSummaryGridData('FindGo');
    }
})


//Filters
$('#btnAll').click(function () {

    $("#btnCancel").click();

    GetSummaryGridData('Summary');
});
$('#btnToday').click(function () {
    $("#btnCancel").click();
    FilterSummaryGrid('Today', 'CREATEDDATE')
});
$('#btnYesterday').click(function () {
    $("#btnCancel").click();
    FilterSummaryGrid('Yesterday', 'CREATEDDATE')
});
$('#btnLast7days').click(function () {
    $("#btnCancel").click();
    FilterSummaryGrid('Last7', 'CREATEDDATE')

});
$('#btnCurrentMonth').click(function () {
    $("#btnCancel").click();
    FilterSummaryGrid('CMonth', 'CREATEDDATE')
});
$('#divActives').click(function () {
    $("#btnCancel").click();
    FilterSummaryGrid('Active', 'Status')
});
$('#divInActives').click(function () {
    $("#btnCancel").click();
    FilterSummaryGrid('Inactive', 'Status')
});



function Save(upCheck) {
    window.localStorage.setItem('ctCheck', 1);
    
    Jsonobj = [];
    var item = {};
    if ($('#hdnTransId').val() == "") {
        item["TransId"] = "";
    }
    else {
        item["TransId"] = $('#hdnTransId').val();
    }
    item["SubOrgID"] = $('#hdnBranchId').val();
    item["CompanyId"] = $('#CompanyId').val();
    item["CounterNumber"] = $('#txtCounterCode').val();
    item["CounterName"] = $('#txtCounterName').val();
    item["DESCRIPTION"] = $("#txtDescription").val();
    item["UserId"] = $('#hdnUserId').val();
    item["OBS"] = $('#hdnOBS').val();
    item["LANGID"] = $('#hdnLanguage').val();
    item["LASTUPDATEDDATE"] = $('#hdnLastUpdatedDate').val();
    item["Status"] = "Y";

    if (upCheck != '') {
        if ($('#hdnScreenMode').val() == "Active")
            item["Status"] = "Y";
        else
            item["Status"] = "N";
    }
    item["ItemCode"] = $('#txtCounterCode').val();
    item["ItemName"] = $('#txtCounterName').val();
    item["UOMID"] = $('#hdnUOMId').val();
    item["BarCode"] = $('#txtBarcode').val();
    item["Desc"] = $('#txtDescription').val();
    item["Alias"] = $('#txtAliasName').val();
    item["Price"] = "0"
    item["TAXAP"] = 'Y';
    item["TAXMRP"] = 'R';
     
    if ($('#ChckTaxOnMRPAp').prop('checked') == false) {
        item["TAXMRP"] = 'F';
    }
    if ($('#ChckTaxAp').prop('checked') == false) {
        item["TAXAP"] = 'N';
        item["TAXMRP"] = '';
        item["TaxAmt"] = "0";
    }
    else {
        item["TaxAmt"] = $('#txtTaxValue').val();
    }

    item["Price"] = $('#txtPrice').val();

    var vd_ = window.localStorage.getItem("UUID");;
    
    item["img"] = document.getElementById("b64").innerHTML;
    item["VD"] = vd_;
    Jsonobj.push(item);

    var ItemsData = new Object();
    ItemsData.Data = JSON.stringify(Jsonobj);
    var ComplexObject = new Object();
    //The property name "WebMethodArgName" must match the web method argument "WebMethodArgName"!
    ComplexObject.ItemsData = ItemsData;

    //Stringify and verify the object is foramtted as expected
    var data = JSON.stringify(ComplexObject);
    var options = { dimBackground: true };
    SpinnerPlugin.activityStart("Please wait...", options);
    $.ajax({
        type: 'POST',
        url: CashierLite.Settings.ItemSave,
        data: data,
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function (resp) {
           
            window.localStorage.setItem('ctCheck', 1);
            SpinnerPlugin.activityStop();
            resp = resp.ItemSaveResult;
            var result = resp.split('~');
            if (result[0] == "100000") {
                if ($('#hdnScreenMode').val() == "Active") { 

                    navigator.notification.alert($('#txtCounterCode').val() + ' ' + $('#hdnRecActSucc').val(), "", "neo ecr", "Ok");

                    $("#btnCancel").click();
                }
                else if ($('#hdnScreenMode').val() == "InActive") {
                  
                    navigator.notification.alert($('#txtCounterCode').val() + ' ' + $('#hdnRecInactSucc').val(), "", "neo ecr", "Ok");

                    //$("#btnCreate").click();
                    $("#btnCancel").click();
                }
                else {
                     
                    navigator.notification.alert($('#txtCounterCode').val() + ' ' + $('#hdnRecSaveSucc').val(), "", "neo ecr", "Ok");

                    if ($('#hdnTransId').val() == "") {
                        $("#btnCreate").click();
                    }
                    else {
                        $("#btnCancel").click();
                    }
                }

                clearControls_Item();
                //GetSummaryGridData();
                GetSummaryGridData('Summary');
            }
            if (result[0] == "100001") { 
                navigator.notification.alert($('#txtCounterCode').val() + ' ' + $('#hdnCodeAlreadyExist').val(), "", "neo ecr", "Ok");

            }
            if (result[0] == "100005") { 

                navigator.notification.alert($('#txtBarcode').val() + ' ' + $('#hdnbarCodeAlreadyExist').val(), "", "neo ecr", "Ok");
            }
            if (result[0] == "1000020") { 
                navigator.notification.alert($('#hdnInvalData').val() , "", "neo ecr", "Ok");

            }
        },
        error: function (e) {
            SpinnerPlugin.activityStop(); 
            navigator.notification.alert($('#hdnInvalData').val(), "", "neo ecr", "Ok");

        }
    });

    $('#PrimeInfo').addClass('open');
    $('#divPrimeInformation').attr('style', 'display:block');
}
function IsNumericUP(e) {
    window.localStorage.setItem('ctCheck', 1);
    var specialKeys = new Array();
    specialKeys.push(8);
    var keyCode = e.which ? e.which : e.keyCode
    var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1 || keyCode == 46);
    if (ret == false) {
        return ret;
    }
    else {

    }

} function IsNumericUPT(e) {
    window.localStorage.setItem('ctCheck', 1);
    var a = '#chek_tap_' + e.target.id.split('_')[0] + '_M';
    if ($(a)[0].disabled == true) {
        return false;
    }
    var specialKeys = new Array();
    specialKeys.push(8);
    var keyCode = e.which ? e.which : e.keyCode
    var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1 || keyCode == 46);
    if (ret == false) {
        return ret;
    }
    else {

    }

}

function taxMove(data) {
    window.localStorage.setItem('ctCheck', 1);
    if (data.children.length == 0) {
        var id_ = "<input type='text' value=" + data.innerHTML + ">";
        data.innerHTML = id_;
    }
    //else
    //{
    //    var dback_ = data.children[0].value;
    //    data.innerHTML = '';
    //    var id_ = "<input type='text' value=" + dback_ + ">";
    //    data.innerHTML = id_;
    //    
    //}
    //alert('daa');
}
function taxMoveout(data) {
    window.localStorage.setItem('ctCheck', 1);
    var data_ = data;
    if (data.children.length == 1) {
        var dback_ = data.children[0].value;
        data.innerHTML = '';
        var id_ = dback_;
        data.innerHTML = id_;
    }
}
function uploadData() {
    window.localStorage.setItem('ctCheck', 1);
    Jsonobj = [];
    var item = {};
    var vd_ = window.localStorage.getItem("UUID");;
    //var vdSplit_ = window.location.pathname.split('/');
    //if (vdSplit_.length > 2) {
    //    vd_ = vdSplit_[1];
    //}
    item["upload"] = document.getElementById("PPb64").innerHTML;
    item["PageCode"] = "Item";
    item["VD"] = vd_;
    Jsonobj.push(item);
    var ItemsData = new Object();
    ItemsData.Data = JSON.stringify(Jsonobj);
    var ComplexObject = new Object();
    //The property name "WebMethodArgName" must match the web method argument "WebMethodArgName"!
    ComplexObject.ItemsData = ItemsData;
    //Stringify and verify the object is foramtted as expected
    var data = JSON.stringify(ComplexObject);
    $('#div_upload').attr('style', 'display:block');
    document.getElementById("PPinp").addEventListener("change", readFilePP);

    $.ajax({
        type: 'POST',
        url: CashierLite.Settings.ItemSaveUpload,
        data: data,
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function (resp) {

            var xmlDoc = $.parseXML(resp.ItemSaveUploadResult);
            var xml = $(xmlDoc);
            var Clength = 0;
            var data_ = xml.find("data");
             
            resp = resp.ItemSaveUploadResult;
            var result = resp.split(',');
            $('#div_upload').attr('style', 'display:none');
            if (result[0] == "100000") { 

                navigator.notification.alert($('#txtCounterCode').val() + ' ' + $('#hdnRecSaveSucc').val(), "", "neo ecr", "Ok");

                $("#btnCreate").click();
            }
            if (result[0] == "100001") { 

                navigator.notification.alert($('#txtCounterCode').val() + ' ' + $('#hdnCodeAlreadyExist').val(), "", "neo ecr", "Ok");

            }
            if (result[0] == "1000020") { 
                navigator.notification.alert($('#hdnInvalData').val(), "", "neo ecr", "Ok");
            }

            if (data_.length > 0) {
                var markup;
                $('#lblitemsCount')[0].textContent = $('#hdnitemsCount').val() + ' ' + data_.length;
                var SummaryTable = '';
                $('#tblGrid tbody').children().remove();
                for (var i = 0; i < data_.length; i++) {
                    var v = data_[i];

                    var taxVal_ = v.children[7].textContent;
                    var taxApplicable_ = '<input disabled onchange="chkOnchangeUP(this);"  type="checkbox" id="chek_tap_' + i + '" />';
                    var taxMode_ = '<input disabled type="checkbox" id="chek_tap_' + i + '_M" />';
                    if (v.children[5].textContent == "Y") {
                        taxApplicable_ = '<input disabled type="checkbox"  onchange="chkOnchangeUP(this);"  checked="checked" id="chek_tap_' + i + '" />';
                    }
                    if (v.children[6].textContent == "Y") {
                        var taxMode_ = '<input disabled type="checkbox" checked="checked" id="chek_tap_' + i + '_M" />';
                    }
                    //else {
                    //    taxVal_ = '0';
                    //}
                    if (v.children[5].textContent == "") {
                        taxApplicable_ = '<input disabled  onchange="chkOnchangeUP(this);"  type="checkbox" id="chek_tap_' + i + '"  />';
                        taxMode_ = '<input type="checkbox" disabled id="chek_tap_' + i + '_M" />';
                        //if (taxVal_ != '0') {
                        //    taxVal_ = "0";
                        //}
                    }
                    if (v.children[5].textContent == "N") {
                        //taxMode_ = '<input type="checkbox" id="chek_tap_' + i + '_M" />';
                        taxMode_ = '<input type="checkbox" disabled id="chek_tap_' + i + '_M" />';
                        //taxVal_ = "0";
                    }
                    //SummaryTable += "<tr><td><input type='checkbox' checked='checked' /></td><td>" + v.children[0].textContent + "</td><td>" + v.children[1].textContent + "</td><td>" + v.children[2].textContent + "</td><td>" + v.children[3].textContent + "</td><td><input onkeypress='return IsNumericUP(event);' style='text-align: right;width: 100%;height: 100%;' id='" + i + "' type='text' value='" + v.children[4].textContent + "'/></td><td>" + taxApplicable_ + "</td><td>" + taxMode_ + "</td><td><input onkeypress='return IsNumericUPT(event);' style='text-align: right;width: 100%;height: 100%;'  id='" + i + "_M' type='text' value='" + taxVal_ + "'/></td><td>" + v.children[8].textContent + "</td></tr>";
                    SummaryTable += "<tr><td><input type='checkbox' checked='checked' /></td><td>" + v.children[0].textContent + "</td><td>" + v.children[1].textContent + "</td><td>" + v.children[2].textContent + "</td><td>" + v.children[3].textContent + "</td><td>" + v.children[4].textContent + "</td><td>" + taxApplicable_ + "</td><td>" + taxMode_ + "</td><td>" + taxVal_ + "</td><td>" + v.children[8].textContent + "</td></tr>";

                }
                $('#tblGrid tbody').remove();
                SummaryTable += "<tbody>" + SummaryTable + "</tbody>";
                $('#tblGrid').append(SummaryTable);

                $('#chk_upload')[0].checked = true;
            }
            else {
                $('#tblGrid tbody').children().remove();
                $('#lblitemsCount')[0].textContent = "";
            }

        },
        error: function (e) { 
            navigator.notification.alert($('#hdnInvalData').val(), "", "neo ecr", "Ok");

        }
    });

    $('#PrimeInfo').addClass('open');
    $('#divPrimeInformation').attr('style', 'display:block');
}
function RefreshData() {
    window.localStorage.setItem('ctCheck', 1);
    location.reload();
}
function saveData() {
    window.localStorage.setItem('ctCheck', 1);
    var TB_DB = document.getElementById('tblGrid');
    var a = TB_DB.rows.length - 1;
   
    for (i = 1; i < TB_DB.rows.length; i++) {
        var objCells = TB_DB.rows.item(i).cells;
        var chkstatus1 = objCells["0"].childNodes["0"].checked;
        var chkstatus = chkstatus1;
        if (chkstatus1 == true) {
            chkstatus = true;
            i = TB_DB.rows.length;
        } 

    }
    if (a == 0) { 
        navigator.notification.alert($('#hdnPlsUpldatlst1item2save').val(), "", "neo ecr", "Ok");

    }
    else if (chkstatus == false) { 
        navigator.notification.alert($('#hdnPlsSelectItem').val(), "", "neo ecr", "Ok");

    }
    else {
        SaveUploadData();
    }
}
function SaveUploadData() {
    window.localStorage.setItem('ctCheck', 1);
    Jsonobj = [];
    $('#div_Save').attr('style', 'display:block');
    var myTab = document.getElementById('tblGrid');
    for (let i = 1; i < myTab.rows.length; i++) {
        var objCells = myTab.rows.item(i).cells;
        if (objCells.length > 0) {
            if (objCells.item(0).children[0].checked == true)
                try {
                    var itemCode_ = objCells.item(1).innerHTML;
                    var itemName_ = objCells.item(2).innerHTML;
                    var alias_ = objCells.item(3).innerHTML;
                    var uom_ = objCells.item(4).innerHTML;
                    var price = objCells.item(5).innerHTML;
                    var Tax = objCells.item(8).innerHTML;
                    var ChckTaxOnMRPAp = objCells.item(6).children[0].checked;
                    var ChckTaxAp = objCells.item(7).children[0].checked;
                    var barCode = objCells.item(9).innerHTML;
                    var item = {};
                    item["SubOrgID"] = $('#hdnBranchId').val();
                    item["CompanyId"] = $('#CompanyId').val();
                    item["UserId"] = $('#hdnUserId').val();
                    item["OBS"] = $('#hdnOBS').val();
                    item["LANGID"] = $('#hdnLanguage').val();
                    item["Status"] = "Y";
                    item["ItemCode"] = itemCode_;
                    item["ItemName"] = itemName_;
                    item["UOMID"] = uom_;
                    item["BarCode"] = barCode;
                    item["Desc"] = '';
                    item["Alias"] = alias_;
                    item["Price"] = "0"
                    item["TAXAP"] = 'Y';
                    item["TAXMRP"] = 'R';
                    item["Tax"] = Tax;
                    if (ChckTaxOnMRPAp == false) {
                        item["TAXAP"] = 'N';
                        item["TAXMRP"] = '';
                    }
                    if (ChckTaxAp == false) {

                        if (ChckTaxOnMRPAp == false) {
                            item["TAXMRP"] = '';
                        }
                        if (ChckTaxOnMRPAp == true) {
                            item["TAXMRP"] = 'F';
                        }

                    }
                    if (price != "") {
                        item["Price"] = price;
                    }
                    var vd_ = window.localStorage.getItem("UUID");;
                    //var vdSplit_ = window.location.pathname.split('/');
                    //if (vdSplit_.length > 2) {
                    //    vd_ = vdSplit_[1];
                    //}
                    item["img"] = "";
                    item["VD"] = vd_;
                    Jsonobj.push(item);
                }
                catch (e) {

                }
        }
    }
    var ItemsData = new Object();
    ItemsData.Data = JSON.stringify(Jsonobj);
    var ComplexObject = new Object();
    //The property name "WebMethodArgName" must match the web method argument "WebMethodArgName"!
    ComplexObject.ItemsData = ItemsData;
    //Stringify and verify the object is foramtted as expected
    var data = JSON.stringify(ComplexObject);

    $.ajax({
        type: 'POST',
        url: CashierLite.Settings.ItemSaveExcel,
        data: data,
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function (resp) {
            $('#div_Save').attr('style', 'display:none');
            resp = resp.ItemSaveExcelResult;
            var result = resp.split(',');
            if (result[0] == "100000") {

                if (result[1].toLowerCase() == "true") {
                    var $textAndPic = $('<div></div>');
                    var dupl_ = "";
                    if (result[2] != "") {
                        dupl_ = '(Duplicate Records Found)';
                        duplicatesBind(result[2]);
                    }
                    else {
                        $('#chk_upload')[0].checked = false;
                        $('#tblGrid tbody').children().remove();
                        $('#lblitemsCount')[0].textContent = "";
                    } 
                    navigator.notification.alert($('#txtCounterCode').val() + ' ' + $('#hdnItemsUploadMsg').val() + dupl_, "", "neo ecr", "Ok");

                    $("#btnCancel").click();

                    clearControls_Item();
                    //GetSummaryGridData();
                }
                if (result[1].toLowerCase() == "false") { 

                    navigator.notification.alert($('#txtCounterCode').val() + " Record saved Successfully", "", "neo ecr", "Ok");

                    clearControls_Item();
                    //GetSummaryGridData();
                }
                if (result[1] == "") {
                    if (result[2] != "") {
                        duplicatesBind(result[2]); 
                        navigator.notification.alert($('#hdnItemsUploadDuplicateMsg').val() , "", "neo ecr", "Ok");

                    }
                }

            }
            if (result[0] == "100001") { 
                navigator.notification.alert($('#txtCounterCode').val() + ' ' + $('#hdnCodeAlreadyExist').val(), "", "neo ecr", "Ok");

            }

        },
        error: function (e) { 
            navigator.notification.alert($('#hdnInvalData').val() , "", "neo ecr", "Ok");
        }
    });

    $('#PrimeInfo').addClass('open');
    $('#divPrimeInformation').attr('style', 'display:block');
}
function duplicatesBind(data) {

    window.localStorage.setItem('ctCheck', 1);


    var data_ = data.split('~');
    var markup;
    $('#lblitemsCount')[0].textContent = $('#hdnitemsCount').val() + ' ' + +(parseFloat(data_.length) - 1) + "(Duplicate)";
    var SummaryTable = '';
    $('#tblGrid tbody').children().remove();
    for (var i = 0; i < data_.length; i++) {
        var v = data_[i].split('|');
        if (v[0] != "") {
            if (v[0] != 'undefined') {
                 
                var taxVal_ = v[7];
                var taxApplicable_ = '<input disabled onchange="chkOnchangeUP(this);"  type="checkbox" id="chek_tap_' + i + '" />';
                var taxMode_ = '<input disabled type="checkbox" id="chek_tap_' + i + '_M" />';
                if (v[5] == "Y") {
                    taxApplicable_ = '<input disabled type="checkbox"  onchange="chkOnchangeUP(this);"  checked="checked" id="chek_tap_' + i + '" />';
                }
                if (v[6] == "Y") {
                    var taxMode_ = '<input disabled type="checkbox" checked="checked" id="chek_tap_' + i + '_M" />';
                }
                if (v[6] == "") {
                    taxApplicable_ = '<input disabled  onchange="chkOnchangeUP(this);"  type="checkbox" id="chek_tap_' + i + '"  />';
                    taxMode_ = '<input type="checkbox" disabled id="chek_tap_' + i + '_M" />';
                    if (taxVal_ != '0') {
                        taxVal_ = "";
                    }
                }
                if (v[6] == "N") {
                    //taxMode_ = '<input type="checkbox" id="chek_tap_' + i + '_M" />';
                    taxMode_ = '<input type="checkbox" disabled id="chek_tap_' + i + '_M" />';
                    taxVal_ = "";
                }
                //SummaryTable += "<tr><td><input type='checkbox' checked='checked' /></td><td>" + v.children[0].textContent + "</td><td>" + v.children[1].textContent + "</td><td>" + v.children[2].textContent + "</td><td>" + v.children[3].textContent + "</td><td><input onkeypress='return IsNumericUP(event);' style='text-align: right;width: 100%;height: 100%;' id='" + i + "' type='text' value='" + v.children[4].textContent + "'/></td><td>" + taxApplicable_ + "</td><td>" + taxMode_ + "</td><td><input onkeypress='return IsNumericUPT(event);' style='text-align: right;width: 100%;height: 100%;'  id='" + i + "_M' type='text' value='" + taxVal_ + "'/></td><td>" + v.children[8].textContent + "</td></tr>";
                SummaryTable += "<tr><td><input type='checkbox' checked='checked' /></td><td>" + v[0] + "</td><td>" + v[1] + "</td><td>" + v[2] + "</td><td>" + v[3] + "</td><td>" + v[4] + "</td><td>" + taxApplicable_ + "</td><td>" + taxMode_ + "</td><td>" + taxVal_ + "</td><td>" + v[8] + "</td></tr>";
            }
        }
    }
    $('#tblGrid tbody').remove();
    SummaryTable += "<tbody>" + SummaryTable + "</tbody>";
    $('#tblGrid').append(SummaryTable);

    $('#chk_upload')[0].checked = true;




}
function chkOnchange() {
    window.localStorage.setItem('ctCheck', 1);
    var m_ = $('#chk_upload');

    chekBasePPItems($('#chk_upload')[0].checked)
}
function chkOnchangeUP(data_) {

    window.localStorage.setItem('ctCheck', 1);
    var ChckTaxAp = '#' + data_.id;
    var ChckTaxOnMRPAp = '#' + data_.id + '_M'
    var txtTax = '#' + data_.id.split('_')[2] + '_M'
    if ($(ChckTaxAp).prop('checked') == true) {
        $(ChckTaxOnMRPAp).prop('checked', true);
        $(ChckTaxOnMRPAp).prop('disabled', false);

    }
    else {
        $(ChckTaxOnMRPAp).prop('checked', false);
        //$("#ChckTaxOnMRPAp").attr("disabled", "disabled");
        $(ChckTaxOnMRPAp).prop('disabled', true);
        $(txtTax).val('');


    }
    //chekBasePPItems($('#chk_upload')[0].checked)
}

function chekBasePPItems(type) {
    window.localStorage.setItem('ctCheck', 1);
    var TB_DB = document.getElementById('tblGrid');
    for (i = 1; i < TB_DB.rows.length; i++) {
        var objCells = TB_DB.rows.item(i).cells;
        if (objCells.length > 0) {
            if (type == false)
                objCells["0"].childNodes["0"].checked = false;
            else
                objCells["0"].childNodes["0"].checked = true;
        }
    }

} 
$("#btnActive").click(function () {

    $('#hdnScreenMode').val("Active");
    Save('Mode');
});
//InActive Record
$("#btnInActive").click(function () {
    $('#hdnScreenMode').val("InActive");
    Save('Mode');
});
$("#btnCreate").click(function () {
    window.localStorage.setItem('ctCheck', 1);
    clearControls_Item();
    defaultdata();
    //GetAutoCodeGen($('#hdnBranchId').val(), '3');
    $('#lblLastUpdate').css("display", "none");
    //$('#trSBInnerRow li span#lblLastUpdate').html("");
    $('#trSBInnerRow li span#lblLastUpdatedUser').html("");
    $('#trSBInnerRow li span#lblLastUpdatedDate').html("");
    //$('#trSBInnerRow li span#lblMode').html("New");
    $('#trSBInnerRow li span#lblMode').html($('#hdnNew').val());
    $('#hdnMode').val('New');
    $('#txtCounterCode').removeAttr("disabled", "disabled");
    $('#txtCounterName').removeAttr("disabled", "disabled");
    $("#txtDescription").removeAttr("disabled", "disabled");
    $("#inp").removeAttr("disabled", "disabled"); 
    $("#btnCreate").attr('style', 'display:none;');
    $("#ShowMenuModes").attr('style', 'display:block;');
    $("#btnSave").attr('style', 'display:block;');
    $("#btnEdit").attr('style', 'display:none;');
    $("#btnDelete").attr('style', 'display:none;');
    $("#btnCancel").attr('style', 'display:block;');
    $("#SummaryGrid").attr('style', 'display:none;');
    $("#NewPage").attr('style', 'display:block;');
    $("#divStatusGroup").attr('style', 'display:none;');
    $("#divNavBarRight").attr('style', 'display:none;');
    var counterN = $('#hdnCounterNo').val();
    if ($("#hdnMode").val() == 'New' && counterN == 'Y') {
        $('#txtCounterCode').prop("disabled", true);  // Document No. Text Box
    }
    else {
        $('#txtCounterCode').prop("disabled", false);
    }

    $('#PrimeInfo').addClass('open');
    $('#divPrimeInformation').attr('style', 'display:block');


    $("#txtAliasName").removeAttr("disabled", "disabled");
    $("#txtPrice").removeAttr("disabled", "disabled");
    $("#txtBarcode").removeAttr("disabled", "disabled");
    $("#txtTaxValue").removeAttr("disabled", "disabled");
    $("#txtUOM").removeAttr("disabled", "disabled");
    $('#ChckTaxOnMRPAp').prop('disabled', false);
    $('#ChckTaxAp').prop('disabled', false);
    $('#ChckTaxAp').prop('checked', true);
    //$('#ChckTaxAp').prop('checked') = true;
    $('#txtUOM').val('EAC/Each');
    $('#hdnUOMId').val('1009');
    $("#txtTaxValue").val(15);
});
$("#btnCancel").click(function () {

    window.localStorage.setItem('ctCheck', 1);
    //GetSummaryGridData();
    clearControls_Item();
    $('#lblLastUpdate').css("display", "none");
    //$('#trSBInnerRow li span#lblLastUpdate').html("");
    $('#trSBInnerRow li span#lblLastUpdatedUser').html("");
    $('#trSBInnerRow li span#lblLastUpdatedDate').html("");
    //$('#trSBInnerRow li span#lblMode').html("Default");
    $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());
    $("#btnCreate").attr('style', 'display:block;');
    $("#ShowMenuModes").attr('style', 'display:none;');
    $("#SummaryGrid").attr('style', 'display:block;');
    $("#NewPage").attr('style', 'display:none;');
    $("#divNavBarRight").attr('style', 'display:block;'); 
});
$('#btnSave').click(function () {
    window.localStorage.setItem('ctCheck', 1);
    if ($("#txtCounterCode").prop("disabled") == false && $('#txtCounterCode').val() == "") { 
        navigator.notification.alert($('#hdnEntrCode').val(), "", "neo ecr", "Ok");

        return false;
    }
    if ($('#txtCounterName').val() == "" && $('#txtCounterName').val().trim().length == 0) { 
        navigator.notification.alert($('#hdnEntrName').val(), "", "neo ecr", "Ok");

        return false;
    }

    if ($('#txtUOM').val() == "" && $('#txtUOM').val().trim().length == 0) { 
        navigator.notification.alert($('#hdnphUOMitem').val(), "", "neo ecr", "Ok");

        return false;
    }
    if ($('#txtBarcode').val() == "" && $('#txtBarcode').val().trim().length == 0) { 
        navigator.notification.alert($('#hdnphbarcode').val(), "", "neo ecr", "Ok");

        return false;
    }

    if ($('#txtBarcode').val().length < 8) {
        navigator.notification.alert($('#hdnBarcodeMaxlen').val(), "", "neo ecr", "Ok");     
        return false;

    } if ($('#txtBarcode').val().length > 16) {
        navigator.notification.alert($('#hdnBarcodeMaxlen').val(), "", "neo ecr", "Ok");
        return false;

    }
    if ($(ChckTaxAp).prop('checked') == true) { 
        if ($('#txtTaxValue').val() == "" || $('#txtTaxValue').val().trim().length == 0) { 
            navigator.notification.alert($('#hdnphTaxValue').val(), "", "neo ecr", "Ok");

            return false;
        }
    } 
    if ($('#txtPrice').val() == "" || $('#txtPrice').val().trim().length == 0) { 
        navigator.notification.alert($('#hdnphPriceItem').val(), "", "neo ecr", "Ok");

        return false;
    }


    Save('');

});
$('#btnEdit').click(function () {
    window.localStorage.setItem('ctCheck', 1);
    Edit();
});
$('#btnDelete').click(function () {
    window.localStorage.setItem('ctCheck', 1);
    Delete();
});

$('#hdnCounterNo').val();
function GetAutoCodeGen(str, iScreenCode) {  
    var vd_ = window.localStorage.getItem("UUID");;
   
    window.localStorage.setItem('ctCheck', 1);
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetAutoGenHashTableCheck,
        data: "SubOrgId=" + str + "&iScreenCode=" + iScreenCode + "&VD=" + vd_,
        success: function (resp) {
            var CounterNo = resp;
            if (CounterNo.length == 1) {
                $('#hdnCounterNo').val(CounterNo[0].PageName);
            }
        },
        error: function (e) {
        }
    });
}
function GenerateCode(strOrgCode, strVarCode, strParamType, strTable, strColumn, USERID, BRANCHID, COMPANYID, LANGUAGE) {
    var vd_ = window.localStorage.getItem("UUID");;
    //var vdSplit_ = window.location.pathname.split('/');
    //if (vdSplit_.length > 2) {
    //    vd_ = vdSplit_[1];
    //}
    window.localStorage.setItem('ctCheck', 1);
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GenerateCode,
        data: "strOrgCode=" + strOrgCode + "&strVarCode=" + strVarCode + "&strParamType=" + strParamType + "&strTable=" + strTable + "&strColumn=" + strColumn + "&USERID=" + USERID + "&BRANCHID=" + BRANCHID + "&COMPANYID=" + COMPANYID + "&LANGUAGE=" + LANGUAGE + "&VD=" + vd_,

        success: function (resp) {
            var CounterNo = resp;
            if (CounterNo.length == 1) {
                $('#txtCounterOpeningNo').val(CounterNo[0].PageName);
            }
        },
        error: function (e) {
        }
    });
}
function getResources(langCode, pageCode) {
    window.localStorage.setItem('ctCheck', 1);
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.getResource,
        data: "langCode=" + langCode + "&pageCode=" + pageCode,
        success: function (resp) {
            setLabels(resp);
        }
    });
}

function setLabels(labelData) {
    window.localStorage.setItem('ctCheck', 1);
    $('#hdnPlsSelectItem').val(labelData["PleaseSelect"] + ' ' + labelData["item"]);
    $('#hdnPlsUpldatlst1item2save').val(labelData["PlsUpldatlst1item2save"]);
    $('#hdnitemsCount').val(labelData["totalItems"]);
    jQuery("label[for='lblitemsCount'").html(labelData["totalItems"]);
    jQuery("label[for='lblDownload'").html(labelData["Download"]);
    jQuery("label[for='lblPlwaitwhlrndrngdata'").html(labelData["Plwaitwhlrndrngdata"]);
    jQuery("label[for='lblUploadItems'").html(labelData["UploadItems"]);
    $('#txtTaxValue').attr('placeholder', labelData["PleaseEnter"] + ' ' + labelData["TaxVAT"]);
    $('#txtPrice').attr('placeholder', labelData["PleaseEnter"] + ' ' + labelData["price"]);
    $('#txtCounterCode').attr('placeholder', labelData["PleaseEnter"] + ' ' + labelData["code"]);
    $('#txtCounterName').attr('placeholder', labelData["PleaseEnter"] + ' ' + labelData["name"]);
    $('#txtAliasName').attr('placeholder', labelData["PleaseEnter"] + ' ' + labelData["Alias"]);
    $('#txtUOM').attr('placeholder', labelData["alert10"] + ' ' + labelData["uom"]);
    $('#txtBarcode').attr('placeholder', labelData["PleaseEnter"] + ' ' + labelData["Barcode"]);
    $('#hdnphbarcode').val(labelData["PleaseEnter"] + ' ' + labelData["Barcode"]);
    $('#hdnphPriceItem').val(labelData["PleaseEnter"] + ' ' + labelData["price"]);
    $('#hdnphUOMitem').val(labelData["alert10"] + ' ' + labelData["uom"]);
    $('#hdnphTaxValue').val(labelData["PleaseEnter"] + ' ' + labelData["TaxVAT"]);
    $('#hdnItemsUploadMsg').val(labelData["UploadUserItems"]);
    $('#hdnItemsUploadDuplicateMsg').val(labelData["UploadDuplicateItems"]);

    $('#hdnPricegreaterthanzero').val(labelData["PriceGreaterthanZero"]);
    $('#hdnQtygreaterthanzero').val(labelData["QtyGreaterthanZero"]);
    $('#hdnTaxgreaterthanzero').val(labelData["ValGreaterthanZero"]);

    jQuery("label[for='lblSubcription'").html(labelData["Renewal"]);
    jQuery("label[for='lblLocationwiseProfitAnalysis'").html(labelData["LocwiseProfitAnlys"]);
    jQuery("label[for='lblItemCategoryProfitAnalysis'").html(labelData["ItemCatgPrftAnlys"]);
    jQuery("label[for='lblSCPopPwd'").html(labelData["pSettings"]);
    jQuery("label[for='lblLanguage'").html(labelData["lang"]);
    jQuery("label[for='lblDateFormat'").html(labelData["format"]);
    jQuery("label[for='lblPopOk'").html(labelData["ok"]);
    jQuery("label[for='lblPopClose'").html(labelData["cancel"]);
    $('#txtFindGo').attr('placeholder', labelData["searchHere"]);
    jQuery("label[for='lblSettings'").html(labelData["settings"]);
    jQuery("label[for='lblThemes'").html(labelData["themes"]);
    jQuery("label[for='lblTheme1'").html(labelData["theme1"]);
    jQuery("label[for='lblTheme2'").html(labelData["theme2"]);
    jQuery("label[for='lblTheme3'").html(labelData["theme3"]);
    jQuery("label[for='lblTheme4'").html(labelData["theme4"]);
    jQuery("label[for='lblChangePassword'").html(labelData["changePassword"]);
    jQuery("label[for='lblTill'").html(labelData["till"]);
    jQuery("label[for='lblCreate'").html(labelData["create"]);
    jQuery("label[for='lblSave'").html(labelData["save"]);
    jQuery("label[for='lblEdit'").html(labelData["edit"]);
    jQuery("label[for='lblDelete'").html(labelData["delete"]);
    jQuery("label[for='lblCancel'").html(labelData["cancel"]);
    jQuery("label[for='lblStatus'").html(labelData["status"]);
    jQuery("label[for='lblActive'").html(labelData["active"]);
    jQuery("label[for='lblInactive'").html(labelData["inactive"]);
    jQuery("label[for='lblFilter'").html(labelData["filter"]);
    jQuery("label[for='lblUpload'").html(labelData["upload"]);
    jQuery("label[for='lblToggleDropdown'").html(labelData["toggleDropdown"]);
    jQuery("label[for='lblAll'").html(labelData["all"]);
    jQuery("label[for='lblToday'").html(labelData["toDay"]);
    jQuery("label[for='lblYesterDay'").html(labelData["yesterDay"]);
    jQuery("label[for='lblLastSevenDays'").html(labelData["lastSevenDays"]);
    jQuery("label[for='lblCurrentMonth'").html(labelData["currentMonth"]);
    jQuery("label[for='lblToggleActive'").html(labelData["active"]);
    jQuery("label[for='lblToggleInactive'").html(labelData["inactive"]);
    jQuery("label[for='lblConfigurators'").html(labelData["configurators"]);
jQuery("label[for='lblSubConfigurators'").html(labelData["POSEwalletConSts"]);
jQuery("label[for='lblInvoiceSummary'").html(labelData["category"]);
    jQuery("label[for='lblPrintCustomize'").html(labelData["printCustamize"]);
    jQuery("label[for='lblBarcodeGenerator'").html(labelData["barCodeGenerator"]);
    jQuery("label[for='lblMasters'").html(labelData["masters"]);
    jQuery("label[for='lblTillType'").html(labelData["tillType"]);
    jQuery("label[for='lblTill'").html(labelData["till"]);
    jQuery("label[for='lblTillPermissions'").html(labelData["tillPermissions"]);
    jQuery("label[for='lblCustomer'").html(labelData["customer"]);
    jQuery("label[for='lblTenderType'").html(labelData["tenderType"]);
    jQuery("label[for='lblTransactions'").html(labelData["transactions"]);
    jQuery("label[for='lblInvoce'").html(labelData["invoice"]);
    jQuery("label[for='lblSalesReturn'").html(labelData["salesReturn"]);
    jQuery("label[for='lblPosting'").html(labelData["posting"]);
    jQuery("label[for='lblTillOpening'").html(labelData["tillOpening"]);
    jQuery("label[for='lblTillClosing'").html(labelData["tillClosing"]);
    jQuery("label[for='lblAccountPosting'").html(labelData["accountPosting"]);
    jQuery("label[for='lblReports'").html(labelData["reports"]);
    jQuery("label[for='lblInvoiceListing'").html(labelData["invoiceListing"]);
    jQuery("label[for='lblTillInvoiceListing'").html(labelData["tillInvoiceListing"]);
    jQuery("label[for='lblTenderTypeWiseCollectionDailySummary'").html(labelData["tenderTypeWiseCollectionDailySummary"]);
    jQuery("label[for='lblSalesReturns'").html(labelData["salesReturns"]);
    jQuery("label[for='lblCode'").html(labelData["code"]);
    jQuery("label[for='lblName'").html(labelData["name"]);
    jQuery("label[for='lblType'").html(labelData["type"]);
    jQuery("label[for='lblProcessStatus'").html(labelData["status"]);
    jQuery("label[for='lblPrimeInformation'").html(labelData["primeInformation"]);
    jQuery("label[for='lblOrganization'").html(labelData["subOrganization"]);
    jQuery("label[for='lblDetCode'").html(labelData["code"]);
    jQuery("label[for='lblDetName'").html(labelData["name"]);
    jQuery("label[for='lblDetType'").html(labelData["type"]);
    jQuery("label[for='lblPhone'").html(labelData["phone"]);
    jQuery("label[for='lblDescription'").html(labelData["description"]);
    jQuery("label[for='lblSCPopPwd'").html(labelData["pSettings"]);
    jQuery("label[for='lblDtFormat'").html(labelData["format"]);
    jQuery("label[for='lblOk'").html(labelData["ok"]);
    jQuery("label[for='lblClose'").html(labelData["closed"]);
    jQuery("label[for='lblLanguage'").html(labelData["lang"]);
    jQuery("label[for='lblLoyaltyPoints'").html(labelData["LoyaltyPoints"]);
    jQuery("label[for='lblDispatch'").html(labelData["Dispatch"]);
    $('#lblNavigationUrl').text(labelData["pos"] + "  >>  " + labelData["masters"] + "  >>  " + labelData["item"]);
    $('#lblLastUpdate').text(labelData["lastUpdated"]);
    $('#lblLastUpdate').css("display", "none");
    jQuery("label[for='lblAnalyticalReports'").html(labelData["AnalyticalReports"]);
    jQuery("label[for='lblItemCategorySalesAnalysis'").html(labelData["ItemCategorySalesAnalysis"]);
    jQuery("label[for='lblWeekDaysRevenue'").html(labelData["WeekdaysRevenue"]);
    jQuery("label[for='lblBucketAnalysis'").html(labelData["BucketAnalysis"]);
    jQuery("label[for='lblLocationwiseAverageInvoiceValue'").html(labelData["LocationwiseAverageInvoiceValue"]);
    jQuery("label[for='lblCurrentInventory'").html(labelData["CurrentInventory"]);
    jQuery("label[for='lblReceipt'").html(labelData["receipt"]);
    jQuery("label[for='lblVATReturns'").html(labelData["VATReturns"]);
    jQuery("label[for='lblItem'").html(labelData["item"]);
    $('#hdnNPermInv').val(labelData["alert1"]);
    $('#hdnNAsnCounterTran').val(labelData["alert2"]);
    $('#hdnInvalData').val(labelData["alert3"]);
    $('#hdnContactAdmin').val(labelData["alert4"]);
    $('#hdnRecSaveSucc').val(labelData["alert8"]);
    $('#hdnRecActSucc').val(labelData["alert6"]);
    $('#hdnRecInactSucc').val(labelData["alert7"]);
    $('#hdnCodeAlreadyExist').val(labelData["alert9"]);
    $('#hdnbarCodeAlreadyExist').val(labelData["Barcodeexists"]);

    $('#hdnRecDelSucc').val(labelData["alert5"]);
    $('#hdnHaveRefRec').val(labelData["reference"]);
    $('#hdnEntrCode').val(labelData["alert10"]);
    $('#hdnEntrName').val(labelData["alert11"]);
    $('#hdnSelType').val(labelData["alert12"]);
    $('#hdnSelPhoneNo').val(labelData["plzFill"] + labelData["phone"]);
    $('#hdnChPwdSaveSucc').val(labelData["alert8"]);
    $('#hdnThameChange').val(labelData["alert13"]);
    $('#hdnNew').val(labelData["new1"]);
    $('#hdnView').val(labelData["view"]);
    $('#hdnEdit').val(labelData["edit"]);
    $('#hdnDefault').val(labelData["default1"]);
    $('#hdnNotAssignCountrToTrans').val(labelData["permissionsAssisted"]);
    $('#hdnDeleteConfirm').val(labelData["wantToDelete"]);
    //jQuery("label[for='lblOk'").html(labelData[""]);
    //jQuery("label[for='lblOk'").html(labelData[""]);
    //jQuery("label[for='lblOk'").html(labelData[""]);
    $('#hdnSelect').val(labelData["select"]);
    jQuery("label[for='lblUOM'").html(labelData["uom"]);
    jQuery("label[for='lblAlias'").html(labelData["Alias"]);
    jQuery("label[for='lblbarCode'").html(labelData["Barcode"]);
    jQuery("label[for='lblTaxApp'").html(labelData["taxApplicabilty"]);
    jQuery("label[for='lblOnMRP'").html(labelData["OnMRP"]);
    jQuery("label[for='lblTaxValue'").html(labelData["TaxVAT"]);
    jQuery("label[for='lblPrice'").html(labelData["price"]);
    $('#lblTaxValue')[0].innerText = labelData["TaxVAT"];
    $('#hdnImageSave').val(labelData["ImageSave"]);
    jQuery("label[for='lblTaxValueupl'").html(labelData["TaxVAT"]);
    jQuery("label[for='lblUploadUserImage'").html(labelData["UploadUserImage"]);
    $('#hdnSelImage').val(labelData["SelUserImage"]);
    $('#hdnBarcodeMaxlen').val(labelData["alert14"]); 
    jQuery("label[for='lblCustLDGR'").html(labelData["PosCustLdger"]);
}