/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

// Wait for the deviceready event before using any of Cordova's device APIs.
// See https://cordova.apache.org/docs/en/latest/cordova/events/events.html#deviceready
document.addEventListener('deviceready', onDeviceReady, false);

function onDeviceReady() {
    // Cordova is now initialized. Have fun!  
    
        try
        {
        var bt = cordova.plugins.Apiplugin.getEnableBt;
        bt = bt(null, null); 
        }
        catch(a)
        {
        
    }
        try {
            speakWelcomeDas();
        }
        catch (a)
        {

        }
}
function speakWelcomeDas() {
    playAudio();
}
function playAudio() {
    if (window.localStorage.getItem("Play") == "0") {
        try {
            var HashVal = (window.localStorage.getItem("hashValues"));
            var HashValues;
            if (HashVal != null && HashVal != '') {
                HashValues = HashVal.split('~');
                if (HashValues[7] == 'ar-SA') {
                    var myMedia = new Media("/android_asset/www/audio/welcomearabic.mp3")
                    myMedia.play({ numberOfLoops: 1 })
                    window.localStorage.setItem("Play", "1");
                }
                else {
                    var myMedia = new Media("/android_asset/www/audio/welcome.mp3")
                    myMedia.play({ numberOfLoops: 1 })
                    window.localStorage.setItem("Play", "1");
                }
            }
        }
        catch (a) {
            window.localStorage.setItem("Play", "0");

        }
    }
}  
