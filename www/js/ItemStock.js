﻿$("#divNavBarRight").attr('style', 'display:block;');

function MainCounterData() {
    window.localStorage.setItem('ctCheck', 1);
    GetLoginUserName();
    defaultdata();
    GetSummaryGridData('Summary');
    GetAutoCodeGen($('#hdnBranchId').val(), '3');
    $('#lblLastUpdate').css("display", "none");

    var HashVal = (window.localStorage.getItem("hashValues"));
    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');

        $('#hdnDateFromate').val(HashValues[2]);
    }
    $("#txtCounterName").DatePicker({
        format: $('#hdnDateFromate').val(),
        changeMonth: 'true',
        changeYear: 'true',

    });  
}
$('#linkDownloadPath').click(function () {
    window.open(CashierLite.Settings.ReceiptfieDownloadPath, "_self", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=400,height=400");
})
document.getElementById("PPinp").addEventListener("change", readFilePP);
//function openFileOptionPP() {
//    window.localStorage.setItem('ctCheck', 1);
//   // $('#tblGrid tbody').children().remove();
//    //$('#lblitemsCount')[0].textContent = "";
//   // $('#ModalUploadPopup').modal('show');
//}
function openFileOptionPP() {
    window.localStorage.setItem('ctCheck', 1);
    document.getElementById("PPinp").click();
}
function readFilePP() {
    window.localStorage.setItem('ctCheck', 1);
    if (this.files && this.files[0]) {
        var FR = new FileReader();
        FR.onload = function (e) {
            document.getElementById("PPb64").innerHTML = e.target.result;
            uploadData();
        }
        FR.readAsDataURL(this.files[0]);
    }
}
function uploadData() {
    window.localStorage.setItem('ctCheck', 1);
    Jsonobj = [];
    var item = {};
    var vd_ = window.localStorage.getItem("UUID");;
    item["upload"] = document.getElementById("PPb64").innerHTML;
    item["PageCode"] = "POSReceipt";
    item["VD"] = vd_;
    Jsonobj.push(item);
    var ItemsData = new Object();
    ItemsData.Data = JSON.stringify(Jsonobj);
    var ComplexObject = new Object();
    //The property name "WebMethodArgName" must match the web method argument "WebMethodArgName"!
    ComplexObject.ItemsData = ItemsData;
    //Stringify and verify the object is foramtted as expected
    var data = JSON.stringify(ComplexObject);
    $('#div_upload').attr('style', 'display:block');
    document.getElementById("PPinp").addEventListener("change", readFilePP);

    $.ajax({
        type: 'POST',
        url: CashierLite.Settings.ItemSaveUpload,
        data: data,
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function (resp) {

            var xmlDoc = $.parseXML(resp.ItemSaveUploadResult);
            var xml = $(xmlDoc);
            var Clength = 0;
            var data_ = xml.find("data");

            resp = resp.ItemSaveUploadResult;
            var result = resp.split(',');
            $('#div_upload').attr('style', 'display:none');
            if (result[0] == "100000") {
             navigator.notification.alert($('#txtCounterCode').val() + ' ' + $('#hdnRecSaveSucc').val(), "", "neo ecr", "Ok");
                $("#btnCreate").click();
            }
            if (result[0] == "100001") {
             navigator.notification.alert( $('#txtCounterCode').val() + ' ' + $('#hdnCodeAlreadyExist').val(), "", "neo ecr", "Ok");

            }
            if (result[0] == "1000020") {
                 navigator.notification.alert($('#hdnInvalData').val(), "", "neo ecr", "Ok");
               
            }

            if (data_.length > 0) {
              //  $('#lblitemsCount')[0].textContent = $('#hdnitemsCount').val() + ' ' + data_.length;
                var markup = '';
                debugger;
                $('#itemCarttable tbody').children().remove();
                var mapCode = "";
                var Profile = "";
                for (var i = 0; i < data_.length; i++) {
                    var v = data_[i];
                    ItemId = v.children[7].textContent;
                    markup = "<tr id=" + 'tr_' + ItemId + "><td style='width:5% !important'><img src='images/delete.png' onclick='deleteRow(this)' /></td><td >" + v.children[4].textContent + "/" + v.children[5].textContent + "</td><td>" + v.children[1].textContent + "</td><td align='right'><input type='text' onkeyup='UpdateQtySubChange(this);' onkeypress='return IsNumericQTY(event,this,2);' style='text-align:right' class='form-control'  value=" + v.children[2].textContent + "  id='" + 'td_' + ItemId + "_QTY'></td><td style='display:none'>" + ItemId + "</td><td><input onkeyup='UpdatePriceSubChange(this);' onkeypress='return IsNumericQTY(event,this,2);' type='text' style='text-align:right' class='form-control'  value=" + v.children[3].textContent + "  id='" + 'td_' + ItemId + "'></td><td style='display:none'>" + v.children[8].textContent + "</td><td align='right' >" + toNumberDecimalTwo((v.children[2].textContent * v.children[3].textContent)) + "</td><td style='display:none'>" + mapCode + "</td><td style='display:none'>" + Profile + "</td><td style='display:none'>" + toNumberDecimalTwo((v.children[2].textContent * v.children[3].textContent)) + "</td><td style='display:none'></td><td style='display:none'></td></tr>"; // 258
                    $("#itemCarttable tbody").append(markup);
                   
                }
               // ActiveRowget(ItemId)
                $('#txtItemScan').val('');
                $('#txtSearchtem').val('');
                grandTotal();
               $('#PrimeInfo').removeClass('open');
                $('#divPrimeInformation').attr('style', 'display:none');
                $('#ShippingInfo').addClass('open');
                $('#divShippingInformation').attr('style', 'display:block');
            }
            else {
               
                $('#PrimeInfo').removeClass('open');
                $('#divPrimeInformation').attr('style', 'display:none');
                $('#ShippingInfo').addClass('open');
                $('#divShippingInformation').attr('style', 'display:block');
                navigator.notification.alert("Items not available", "", "neo ecr", "Ok");
            }


        },
        error: function (e) {
 navigator.notification.alert($('#hdnInvalData').val(), "", "neo ecr", "Ok");
        

        }
    });

    $('#PrimeInfo').addClass('open');
    $('#divPrimeInformation').attr('style', 'display:block');
}
function btnTheam_Click1() {
    theamStroreDB('custom-style-1.css');
}
function btnTheam_Click2() {

    theamStroreDB('custom-style-2.css');
}
function theamStroreDB(theamName) {
    window.localStorage.setItem('ctCheck', 1);
    if (window.localStorage.getItem('network') == '1') {
        var TheamVal = theamName;
        var LoginId = window.localStorage.getItem("Sno");
        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.LoginSetTheam,
            data: "uid=" + LoginId + "&theamval=" + TheamVal + "&VD=" + VD1(),
            success: function (resp) {
                navigator.notification.alert($('#hdnThameChange').val(), "", "neo ecr", "Ok");

            },
            error: function (e) {
            }
        });
    }
    else {
        navigator.notification.alert("can't Change Theme in offline network mode", "", "neo ecr", "Ok");
    }
}
function VD1() {
    window.localStorage.setItem('ctCheck', 1);
    var vd_ = window.localStorage.getItem("UUID");;
    return vd_;
}
var txt = '';
// $(function () {

//     $(document).on('swipe.pos.card', function (event) {
//     });
// });
function itemchecklocal() {
    window.localStorage.setItem('ctCheck', 1);
    if ($("#scannerInput").val().trim() != "") {
        var SubOrgId = '';
        var HashValues = (window.localStorage.getItem("hashValues"));
        if (HashValues != null && HashValues != '') {
            var hashSplit = HashValues.split('~');
            SubOrgId = hashSplit[1];
        }

        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.GetINVSacanItem,
            data: "subOrgId=" + SubOrgId + "&scanCode=" + $("#scannerInput").val() + "&VD=" + window.localStorage.getItem("UUID"),
            success: function (resp) {
                alert('2');
                if (resp.length > 0) {
                    $('#hdnStatusChk').val('0');
                    $("#scannerInput").val('');
                    $("#txtScanItem").val('');
                    var ItemsData = resp; 
                    var itemName = resp[0].Code + '/' + resp[0].Name;
                    var itemId = resp[0].ItemId;
                    var itemUOM = resp[0].UOMCode;
                    var itemUOMId = resp[0].UOMID;
                    var price = "0";
                    var Qty = "1";
                    var Amount = "1";
                    onSubDataScan(resp[0].Code, resp[0].ItemId, resp[0].Name, 0, 0, resp[0].UOMID, resp[0].UOMCode, "", "");
                }
                else {
                    $('#hdnStatusChk').val('0');
                }
            }
        });

    }
}
// $(function () {
//     $(document).pos();
//     $(document).on('scan.pos.barcode', function (event) { 
//         $("#txtScanItem").blur(); 
//         if ((event.code.length > 5) && (event.code.length < 17)) {
//             $("#txtScanItem").val(event.code); 
//             ItemdetectScan();
//         }
//         else {  
//             navigator.notification.alert("Invalid Barcode", "", "neo ecr", "Ok")
//         } 
//     });
//     $(document).on('swipe.pos.card', function (event) {
//     });
// });
function ItemdetectScan() { 
    window.localStorage.setItem('ctCheck', 1); 
    if ($("#scannerInput").val().trim() != "") {
        var SubOrgId = '';
        var HashValues = (window.localStorage.getItem("hashValues"));
        if (HashValues != null && HashValues != '') {
            var hashSplit = HashValues.split('~');
            SubOrgId = hashSplit[1];
        }

        $("#txtScanItem").val($("#scannerInput").val());
        if ($('#hdnStatusChk').val() == '0') {
            $('#hdnStatusChk').val('1');
            $.ajax({
                type: 'GET',
                url: CashierLite.Settings.GetINVSacanItem,
                data: "subOrgId=" + SubOrgId + "&scanCode=" + $("#scannerInput").val() + "&VD=" + VD1(),
                success: function (resp) { 
                    if (resp.length > 0) {
                        $('#hdnStatusChk').val('0');
                        $("#scannerInput").val(''); 
                        var ItemsData = resp;
                        $("#txtScanItem").val('');
                        var itemName = resp[0].Code + '/' + resp[0].Name;
                        var itemId = resp[0].ItemId;
                        var itemUOM = resp[0].UOMCode;
                        var itemUOMId = resp[0].UOMID;
                        var price = "0";
                        var Qty = "1";
                        var Amount = "1";
                        onSubDataScan(resp[0].Code, resp[0].ItemId, resp[0].Name, 0, 0, resp[0].UOMID, resp[0].UOMCode, "", "");
                    }
                    else {
                        $('#hdnStatusChk').val('0');
                    }
                }
            });
        }
    } 
}
function TwoDecimals(number) {
   window.localStorage.setItem('ctCheck', 1);
    var numberSplitd_ = number.toString().split('.');
    if (numberSplitd_.length > 1) {
        var subUpdatedNumber = '';
        var submNumberSplit = numberSplitd_[1];
        if (submNumberSplit.length >= 2) {
            if (submNumberSplit.length == 2) {

                // return parseFloat(number); // 258
                return (number);
            }
        }
    }
    else {
        //  return parseFloat(number + ".00"); // 258
        return (number + ".00");
    }
}
function toNumberDecimalTwo(number) {
    window.localStorage.setItem('ctCheck', 1);
    var numberSplitd_ = number.toString().split('.');
    if (numberSplitd_.length > 1) {
        var subUpdatedNumber = '';
        var submNumberSplit = numberSplitd_[1];
        if (submNumberSplit.length >= 2) {
            if (submNumberSplit.length == 2) {

                submNumberSplit = submNumberSplit + '00';
                // subUpdatedNumber = submNumberSplit.substring(0, 4);//258
                subUpdatedNumber = submNumberSplit.substring(0, 2); // 258
                //return parseFloat(numberSplitd_[0] + '.' + subUpdatedNumber);
                return (numberSplitd_[0] + '.' + subUpdatedNumber);
            }
            if (submNumberSplit.length == 3) {

                submNumberSplit = submNumberSplit + '0';
                // subUpdatedNumber = submNumberSplit.substring(0, 4); //258
                subUpdatedNumber = submNumberSplit.substring(0, 2); // 258
                //return parseFloat(numberSplitd_[0] + '.' + subUpdatedNumber); // 258
                return (numberSplitd_[0] + '.' + subUpdatedNumber);// 258
            }
            if (submNumberSplit.length >= 4) {

                submNumberSplit = submNumberSplit;
                // subUpdatedNumber = submNumberSplit.substring(0, 4); //258
                subUpdatedNumber = submNumberSplit.substring(0, 2); // 258
                // return parseFloat(numberSplitd_[0] + '.' + subUpdatedNumber);//258
                return (numberSplitd_[0] + '.' + subUpdatedNumber);//258
            }
        }
        else {
            // return parseFloat(number); // 258
            return (number); // 258
        }
    }
    else {
        //return parseFloat(number + ".00"); // 258
        return (number + ".00");
    }
}
function onSubDataScan(code, ItemId, Name, Price, Disc, UomID, UomCode, mapCode, Profile) {
    window.localStorage.setItem('ctCheck', 1);
    var passQTY = 1; var passPrice = Price; var baseCur_;
    var myTabChkQty = document.getElementById('itemCarttable');
    for (i = 1; i < myTabChkQty.rows.length; i++) {
        var objCells = myTabChkQty.rows.item(i).cells;
        if (ItemId == objCells.item(4).innerHTML) {
            passQTY = passQTY + parseFloat(objCells.item(2).innerHTML);
            passPrice = parseFloat(passPrice) + parseFloat(objCells.item(9).innerHTML);
        }
    }

    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        SubOrg_Id = hashSplit[1];
        baseCur_ = hashSplit[16];
    }

    var SubOrg = {
    };
    SubOrg.SubOrgID = $('#hdnBranchId').val();
    var valItemCheck_ = $('#itemIdhiddenCheck').val();
    var dicPrice_ = Price;
    var _mainDataCheck = '';


    Duplicate(ItemId, Price, Disc, mapCode, Profile, _mainDataCheck, '1');
    valItemCheck_ = $('#itemIdhiddenCheck').val();
    if (valItemCheck_ == '0') {
        //var markup = "<tr id=" + 'tr_' + ItemId + "><td style='width:20px !important'><img src='images/delete.png' onclick='deleteRow(this)'/></td><td >" + Name + "</td><td>" + UomCode + "</td><td align='right'><input type='number' onkeyup='UpdateQtySubChange(this);' onkeypress='return IsNumericQTY(event);' style='text-align:right' class='form-control'  value='1' id='" + 'td_' + ItemId + "_QTY'></td><td style='display:none'>" + ItemId + "</td><td><input onkeyup='UpdatePriceSubChange(this);' onkeypress='return IsNumericQTY(event);' type='number' style='text-align:right' class='form-control'  value='" + Price + "' id='" + 'td_' + ItemId + "'></td><td style='display:none'>" + UomID + "</td><td align='right' >" + toNumberDecimalTwo(Price) + "</td><td style='display:none'>" + mapCode + "</td><td style='display:none'>" + Profile + "</td><td style='display:none'>0</td><td style='display:none'>0</td></tr>";
        //var markup = "<tr id=" + 'tr_' + ItemId + "><td style='width:20px !important'><i class='fa fa-trash' onclick='deleteRow(this)' style='font-size:16px' id='btnrecDele'></i></td><td >" + code + " / " + Name + "</td><td>" + UomCode + "</td><td align='right'><input type='number' onkeyup='UpdateQtySubChange(this);' onkeypress='return IsNumericQTY(event);'        style='text-align:right' class='form-control'  value='1'    id='" + 'td_' + ItemId + "_QTY'></td><td style='display:none'>" + ItemId + "</td><td><input onkeyup='UpdatePriceSubChange(this);' onkeypress='return IsNumericQTY(event);'        type='number' style='text-align:right' class='form-control'  value='" + Price + "'   id='" + 'td_' + ItemId + "'></td><td style='display:none'>" + UomID + "</td><td align='right' >" + toNumberDecimalTwo(Price) + "</td><td style='display:none'>" + mapCode + "</td><td style='display:none'>" + Profile + "</td><td style='display:none'>0</td><td style='display:none'>0</td></tr>";
          var markup = "<tr id=" + 'tr_' + ItemId + "><td style='width:5%   !important'><i class='fa fa-trash' onclick='deleteRow(this)' style='font-size:16px' id='btnrecDele'></i></td><td >" + code + " / " + Name + "</td><td>" + UomCode + "</td><td align='right'><input type='text' onkeyup='UpdateQtySubChange(this);'   onkeypress='return IsNumericQTY(event,this,2);' style='text-align:right' class='form-control'  value='1.00' id='" + 'td_' + ItemId + "_QTY'></td><td style='display:none'>" + ItemId + "</td><td><input onkeyup='UpdatePriceSubChange(this);' onkeypress='return IsNumericQTY(event,this,2);' type='number' style='text-align:right' class='form-control'  value='" + Price + "'  id='" + 'td_' + ItemId + "'></td><td style='display:none'>" + UomID + "</td><td align='right' >" + toNumberDecimalTwo(Price) + "</td><td style='display:none'>" + mapCode + "</td><td style='display:none'>" + Profile + "</td><td style='display:none'>0</td><td style='display:none'></td><td style='display:none'></td></tr>"; // 258
        $("#itemCarttable tbody").append(markup);

    }
    $('#itemIdhiddenCheck').val('0');
    ActiveRowget(ItemId)
    $('#txtItemScan').val('');
    $('#txtSearchtem').val('');
    grandTotal(); // 258
}

function UpdateQtySubChange(data) {
    window.localStorage.setItem('ctCheck', 1);
    var d_ = data;
    var ogItemId = data.parentElement.parentElement.children[4].innerText
    var qtyId = 'td_' + ogItemId + '_QTY'
    var ogQty = $('#' + qtyId).val();

    var d1_ = ogQty.split('.');
    if (d1_.length == 2) {
        if (d1_[0].length == 9) {
            var strng = ogQty;
            $('#' + qtyId).val(strng.substring(0, strng.length - 1));
            return false;
        }
        if (d1_[1].length == 3) {
            var strng = ogQty;
            $('#' + qtyId).val(strng.substring(0, strng.length - 1));
            return false;
        }
    }
    if (d1_.length <= 1) {
        if (ogQty.length == 9) {
            var strng = ogQty;
            $('#' + qtyId).val(strng.substring(0, strng.length - 1));
            return false;
        }
    }





    var ogPrice = $('#td_' + ogItemId).val();
    if (ogQty.trim() == "") {
         ogQty = 0.00;// 258
    }
    if (ogPrice.trim() == "") {
        ogQty = 0.00;// 258
    }
    data.parentElement.parentElement.children[7].innerText = (parseFloat(parseFloat(ogQty) * parseFloat(ogPrice)).toFixed(4)).substring(0, (parseFloat(parseFloat(ogQty) * parseFloat(ogPrice)).toFixed(4)).length - 2);
    data.parentElement.parentElement.children[10].innerText = parseFloat(parseFloat(ogQty) * parseFloat(ogPrice)).toFixed(4);
    ActiveRowget(ogItemId);
    grandTotal();
}
function UpdatePriceSubChange(data) {
    window.localStorage.setItem('ctCheck', 1);
    var d_ = data;
    var ogItemId = data.parentElement.parentElement.children[4].innerText
    var qtyId = 'td_' + ogItemId + '_QTY'
    var ogQty = $('#' + qtyId).val();
    var ogPrice = $('#td_' + ogItemId).val();

    var d1_ = ogPrice.split('.');
    if (d1_.length == 2) {
        if (d1_[0].length == 9) {
            var strng = ogPrice;
            $('#td_' + ogItemId).val(strng.substring(0, strng.length - 1));
            return false;
        }
        if (d1_[1].length == 3) {
            var strng = ogPrice;
            $('#td_' + ogItemId).val(strng.substring(0, strng.length - 1));
            return false;
        }
    }
    if (d1_.length <= 1) {
        if (ogPrice.length == 9) {
            var strng = ogPrice;
            $('#td_' + ogItemId).val(strng.substring(0, strng.length - 1));
            return false;
        }
    }



    if (ogQty.trim() == "") {
        ogQty = 0.00;
    }
    if (ogPrice.trim() == "") {
        ogPrice = 0.00;
    }
    data.parentElement.parentElement.children[7].innerText = (parseFloat(parseFloat(ogQty) * parseFloat(ogPrice)).toFixed(4)).substring(0, (parseFloat(parseFloat(ogQty) * parseFloat(ogPrice)).toFixed(4)).length - 2);
    data.parentElement.parentElement.children[10].innerText = parseFloat(parseFloat(ogQty) * parseFloat(ogPrice)).toFixed(4);
    ActiveRowget(ogItemId);
    grandTotal();
}
try {
    $("#txtSearchtem").autocomplete({
        source: function (request, response) {

            var HashValues = (window.localStorage.getItem("hashValues"));
            if (HashValues != null && HashValues != '') {
                var hashSplit = HashValues.split('~');
                SubOrgId = hashSplit[1];
            }

            $.ajax({
                url: CashierLite.Settings.GetINVSearchItem,
                data: "subOrgId=" + SubOrgId + "&searchKey=" + $("#txtSearchtem").val() + "&VD=" + VD1(),
                dataType: "json",
                type: "Get",
                success: function (data) {

                    var d_ = data;
                    response($.map(data, function (item) {
                        return {
                            label: item.Code + '/' + item.Name + ' ' + item.Alias,
                            val: item.ItemId,
                            Code: item.Code,
                            Name: item.Name,
                            ItemId: item.ItemId,
                            UOMCode: item.UOMCode,
                            UOMID: item.UOMID,
                        }
                    }))
                },
                error: function (response) {
                },
                failure: function (response) {
                }
            });

        },
        change: function (e, i) {
            if (i.item) {

            }
            else {
                $('#txtSearchtem').val('');


            }
        },
        select: function (e, i) {
            var itemName = i.item.Code + '/' + i.item.Name;
            var itemId = i.item.ItemId;
            var itemUOM = i.item.UOMCode;
            var itemUOMId = i.item.UOMID;
            onSubDataScan(i.item.Code, i.item.ItemId, i.item.Name, "0.00", "0.00", i.item.UOMID, i.item.UOMCode, "", "");
            $(this).val("");

            var ogItemId = itemId
            var qtyId = 'td_' + ogItemId + '_QTY'

            $('#' + qtyId).focus();
            return false;
        },
        minLength: 1
    });
}
catch (e) {

}
function grandTotal() {
    window.localStorage.setItem('ctCheck', 1);
    var myTab = document.getElementById('itemCarttable');
    var mVal_ = "0";
    for (i = 1; i < myTab.rows.length; i++) {
        var objCells = myTab.rows.item(i).cells;
        if (objCells.length > 0) {
            //  mVal_ = parseFloat(mVal_) + parseFloat(objCells.item(7).innerHTML); // 258
            mVal_ = parseFloat(mVal_) + parseFloat(objCells.item(10).innerHTML); // 258
        }
    }
    $('#lblGrandTotalOriginal')[0].innerHTML = parseFloat(mVal_).toFixed(4);
    $('#lblGrandTotal')[0].innerHTML = parseFloat(mVal_).toFixed(4).substring(0, parseFloat(mVal_).toFixed(4).length - 2);

}

/*function IsNumericQTY(e) {
    window.localStorage.setItem('ctCheck', 1);
    var specialKeys = new Array();
    specialKeys.push(8);
    var keyCode = e.which ? e.which : e.keyCode

    var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1 || keyCode == 46);
    if (ret == false) {
        return ret;
    }
    else {

    }

}*/
function IsNumericQTY(e, ctrl, DecimalPlaces) {
    window.localStorage.setItem('ctCheck', 1);

    if ((e.which != 46 || $(ctrl).val().indexOf('.') != -1) && (e.which < 48 || e.which > 57)) {
        e.preventDefault();
        //return false;
    }

    var character = String.fromCharCode(e.keyCode);
    var newValue = ctrl.value + character;
    var newValue1 = "";
    if (isNaN(newValue)) {
        e.preventDefault();
        //return false;
    }

    var caratPos = ctrl.selectionStart;
    var dotPos = ctrl.value.indexOf(".");

    if (caratPos > dotPos && $(ctrl).val().indexOf('.') !== -1) {
        var star = ctrl.selectionStart;
        var end = ctrl.selectionEnd
        if (end - star > 0) {
            if (star != 0) {
                newValue1 = newValue.substring(0, star - 1);
            }
            if (end < newValue.length - 1)
                newValue1 = newValue1 + newValue.substring(end, newValue.length - 1);
        }
        else if (end == star) {
            newValue1 = newValue;
        }
        if (newValue1.indexOf('.') !== -1) {
            var number = newValue1.split('.');
            if (number.length == 2 && number[1].length > parseFloat(DecimalPlaces))
                e.preventDefault();
        }

    }


} // 258
function deleteRow(r) {
  window.localStorage.setItem('ctCheck', 1);
 if ($('#hdnMode').val() != "View")
        {
    var i = r.parentNode.parentNode.rowIndex;
    var mId = r.parentNode.parentNode.childNodes[4].innerText;
    $('#hdnOE').val($('#hdnOE').val()+r.parentNode.parentNode.childNodes[11].innerText + "~" + r.parentNode.parentNode.childNodes[12].innerText + "^");
    var idC_ = '#tr_' + mId;
    $(idC_).remove();
    $('#txtScanItem').focus();
    grandTotal();
    }
  
   /* var i = r.parentNode.parentNode.rowIndex;
    var mId = r.parentNode.parentNode.childNodes[4].innerText;
    var idC_ = '#tr_' + mId;
    $(idC_).remove();
    grandTotal();*/
}
function ActiveRowget(itemId) {
    window.localStorage.setItem('ctCheck', 1);
    var myTab = document.getElementById('itemCarttable');
    var m_ = "";
    for (i = 1; i < myTab.rows.length; i++) {
        var objCells = myTab.rows.item(i).cells;
        try {
            var v_ = 'tr_' + objCells.item(4).innerHTML;
            document.getElementById(v_).style.cssText = 'font-weight:normal';
            if (itemId == objCells.item(4).innerHTML) {

                document.getElementById(v_).style.cssText = 'font-weight:bold';

            }
            $("#txthdnActiveColumn").val("");

        }
        catch (r) {

        }
    }
}
function deActiveRowget() {
    window.localStorage.setItem('ctCheck', 1);
    var myTab = document.getElementById('itemCarttable');
    var m_ = "";
    for (i = 1; i < myTab.rows.length; i++) {
        var objCells = myTab.rows.item(i).cells;
        try {
            var v_ = 'tr_' + objCells.item(4).innerHTML;
            document.getElementById(v_).style.cssText = 'font-weight:normal';

        }
        catch (r) {

        }
    }
}
function EnableGrid() {
    $('#itemCarttable').find("input,img").attr("disabled", false);
}
function floorFigure(figure, decimals) {
    window.localStorage.setItem('ctCheck', 1);
    if (!decimals) decimals = 2;
    var d = Math.pow(10, decimals);
    return (parseInt(figure * d) / d).toFixed(decimals);
};
function Duplicate(ItemId, Price_, Disc_, MapCode_, Profile_, mainData_, QTY_) {
    window.localStorage.setItem('ctCheck', 1);

    var myTab = document.getElementById('itemCarttable');
    var m_ = "";
    $('#itemIdhiddenCheck').val('0');
    // LOOP THROUGH EACH ROW OF THE TABLE AFTER HEADER.
    var markup = "";
    for (i = 1; i < myTab.rows.length; i++) {
        // GET THE CELLS COLLECTION OF THE CURRENT ROW.
        var objCells = myTab.rows.item(i).cells;
        // LOOP THROUGH EACH CELL OF THE CURENT ROW TO READ CELL VALUES.
        if (ItemId == objCells.item(4).innerHTML) {
            var qtyId_ = '#td_' + ItemId + '_QTY';
            var priceId = '#td_' + ItemId;
            var getQty_ = '1.00';
            if ($(qtyId_).val() != "") {
                getQty_ = $(qtyId_).val();
            }
            var qty_ = parseFloat(getQty_) + parseFloat(QTY_);
            var Name = objCells.item(1).innerHTML;
            var UOMDY_ = objCells.item(2).innerHTML;
            var priceGet_ = $(priceId).val();
            var ammountGet = "0.00";
            if (priceGet_ != "") {

                ammountGet = parseFloat(qty_) * parseFloat(priceGet_);
            }
            // var UOMID_ = objCells.item(6).innerHTML;
            var UOMID_ = objCells.item(6).innerHTML;
            var RGuid_ = objCells.item(11).innerHTML; // 258
            var LastUpdatedDate_ = objCells.item(12).innerHTML; // 258
              markup = "<tr id=" + 'tr_' + ItemId + "><td style='width:5% !important'><i class='fa fa-trash' onclick='deleteRow(this)' style='font-size:16px' id='btnrecDele'></i></td><td >" + Name + "</td><td>" + UOMDY_ + "</td><td align='right'><input  onkeyup='UpdateQtySubChange(this);' onkeypress='return IsNumericQTY(event,this,2);' type='text' style='text-align:right' class='form-control'  value='" + parseFloat(TwoDecimals(qty_)).toFixed(2) + "' id='" + 'td_' 
              + ItemId + "_QTY'></td><td style='display:none'>" + ItemId + "</td><td><input onkeyup='UpdatePriceSubChange(this);' onkeypress='return IsNumericQTY(event,this,2);' type='text' style='text-align:right' class='form-control'  value='" 
              + parseFloat(TwoDecimals(priceGet_)).toFixed(2) + "' id='" + 'td_'
               + ItemId + "'></td><td style='display:none'>" + UOMID_ + "</td><td align='right' >" + toNumberDecimalTwo(ammountGet) + "</td><td style='display:none'>" + MapCode_ + "</td><td style='display:none'>" 
               + Profile_ + "</td><td style='display:none'>" + ammountGet + "</td><td style='display:none' >" + RGuid_ + "</td><td style='display:none' >" + LastUpdatedDate_ + "</td></tr>"; // 258
            var idC_ = '#tr_' + ItemId;
            $(idC_).remove();
            $('#itemIdhiddenCheck').val('1');

        }

    }
    $("#itemCarttable tbody").append(markup);
}
//}
function defaultdata() {
    window.localStorage.setItem('ctCheck', 1);
    var Paging = ""; var CompanyId = ""; var SubOrgId = ''; var Orgcode = ''; var OBS = ''; var CashierID = window.localStorage.getItem("CashierID"); var Userid = '';
    var Lang = '';
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        getResources(hashSplit[7], "POSCounterDefination");
        CompanyId = hashSplit[0];
        SubOrgId = hashSplit[1];
        OBS = hashSplit[9];
        Orgcode = hashSplit[17];
        Userid = hashSplit[14];
        Lang = hashSplit[7];
        Paging = hashSplit[19];
    }
    $('#CompanyId').val(CompanyId);
    $('#hdnBranchId').val(SubOrgId);
    $('#hdnOBS').val(OBS);
    $('#hdnUserId').val(Userid);
    $('#txtPosCode').val(Orgcode);
    $('#hdnLanguage').val(Lang);
    $('#hdnPaging').val(Paging);
    $('#Content_two').css("display", "none");
    $('#Create').css("display", "block");
    $('#Edit').css("display", "none");
    $('#Cancel').css("display", "none");
    $('#btnSave').css("display", "none");
    $('#Delete').css("display", "none");
    $('#Status').css("display", "none");
    $('#divActive').css("display", "none");
}
function GetLoginUserName() {
    window.localStorage.setItem('ctCheck', 1);
    var sess = CashierLite.Session.getInstance().get();
    if (sess != null) {
        if (sess.userName != "") {
            document.getElementById("lblName").innerHTML = sess.userName;
        }
        else {
            window.open('Login.html', '_self', 'location=no');
        }
    }
    else {
        window.open('Login.html', '_self', 'location=no');
    }
    theamLoad();
}
function theamLoad() {
    if (window.localStorage.getItem("Theam") != null)
        $('head').append('<link rel="stylesheet" href=' + window.localStorage.getItem("Theam") + ' type="text/css" />');
}

//For Invoice Link
function GetShiftData() {
    window.localStorage.setItem('ctCheck', 1);
    if (window.localStorage.getItem("CashierID") != null) {

        var SubOrgId = ''; var CashierID = window.localStorage.getItem("CashierID");
        var HashValues = (window.localStorage.getItem("hashValues"));
        if (HashValues != null && HashValues != '') {
            var hashSplit = HashValues.split('~');
            SubOrgId = hashSplit[1];
        }
        var vd_ = window.localStorage.getItem("UUID");
        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.ShiftCheck,
            data: "CashierID=" + CashierID + "&OrgId=" + SubOrgId + "&VD=" + vd_,
            success: function (resp) {
                var result = resp;
                if (result.length > 0) {
                    var ACTIVE = result[0].ACTIVE;
                    var COUNTERID = result[0].COUNTERID;
                    var COUNTERNAME = result[0].COUNTERNAME;
                    var COUNTERNUMBER = result[0].COUNTERNUMBER;
                    var COUNTERTYPE = result[0].COUNTERTYPE;
                    var End_Time = result[0].End_Time;
                    var Is_Next_Day = result[0].Is_Next_Day;
                    var SHIFTID = result[0].SHIFTID;
                    var Shift_Code = result[0].Shift_Code;
                    var Shift_Name = result[0].Shift_Name;
                    var Start_Time = result[0].Start_Time;
                    var StageAlert = result[0].StageAlert;

                    window.localStorage.setItem('ACTIVE', result[0].ACTIVE);
                    window.localStorage.setItem('COUNTERID', result[0].COUNTERID);
                    window.localStorage.setItem('COUNTERNAME', result[0].COUNTERNAME);
                    window.localStorage.setItem('COUNTERNUMBER', result[0].COUNTERNUMBER);
                    window.localStorage.setItem('COUNTERTYPE', result[0].COUNTERTYPE);
                    window.localStorage.setItem('End_Time', result[0].End_Time);
                    window.localStorage.setItem('Is_Next_Day', result[0].Is_Next_Day);
                    window.localStorage.setItem('SHIFTID', result[0].SHIFTID);
                    window.localStorage.setItem('Shift_Code', result[0].Shift_Code);
                    window.localStorage.setItem('Shift_Name', result[0].Shift_Name);
                    window.localStorage.setItem('Start_Time', result[0].Start_Time);
                    var StageAlert = result[0].StageAlert;
                    if (StageAlert == '1') {
                        if (ACTIVE.toLowerCase() == 'y') {
                            window.open('posmain.html?#', '_self', 'location=no');
                        }
                        else {
                            navigator.notification.alert($('#hdnNPermInv').val(), "", "neo ecr", "Ok");
                        }
                    }
                    else if (StageAlert == '2') {
                        navigator.notification.alert($('#hdnNAsnCounterTran').val(), "", "neo ecr", "Ok");

                    }
                }
                else {
                    navigator.notification.alert($('#hdnNPermInv').val(), "", "neo ecr", "Ok");

                }
            },
            error: function (e) {
                navigator.notification.alert($('#hdnInvalData').val(), "", "neo ecr", "Ok");

            }
        });
    }
    else {
        navigator.notification.alert($('#hdnContactAdmin').val(), "", "neo ecr", "Ok");

    }


}
function GetCounterType() {
    window.localStorage.setItem('ctCheck', 1);
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        SubOrg_Id = hashSplit[1];
    }
    var vd_ = window.localStorage.getItem("UUID");;
    Jsonobj = [];
    var SubOrg = {};
    SubOrg.SubOrgID = $('#hdnBranchId').val();
    SubOrg.VD = vd_;
    Jsonobj.push(SubOrg);

    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetCounterTypes,
        data: "SubOrg=" + JSON.stringify(Jsonobj),
        success: function (resp) {
            var CounterData = resp;
            document.getElementById("ddlCounterType").options.length = 0;
            $("#ddlCounterType").append($("<option value=''  selected ></option>").val(0).html('--' + $("#hdnSelect").val() + '--'));//this is for adding select after clearing
            var MainData = '';
            jsonObj = [];
            for (var i = 0; i < CounterData.length; i++) {
                var CounterId = CounterData[i].CounterID;
                var CounterName = CounterData[i].Code + "/" + CounterData[i].Name;
                item = {}
                item["code"] = CounterId;
                item["name"] = CounterName;

                jsonObj.push(item);
                $("#ddlCounterType").append($("<option></option>").val(CounterId).html(CounterName));
            };
        },
        error: function (e) {

            navigator.notification.alert($('#hdnInvalData').val(), "", "neo ecr", "Ok");

        }
    });

}
//End Page Load function
//Create Function

//End
$(document).ready(function () {

});
//Summary grid click function
function DoubleClick(tr) {
    window.localStorage.setItem('ctCheck', 1);
    $('#PrimeInfo').addClass('open');
    $('#divPrimeInformation').attr('style', 'display:block');
    $('#ShippingInfo').removeClass('open');
    $('#divShippingInformation').attr('style', 'display:none');
    var id = $(tr).find('td:first').html();
    Jsonobj = [];
    var SubOrg = {};
    SubOrg.TransId = id;
    var vd_ = window.localStorage.getItem("UUID");
    SubOrg.VD = vd_;
    Jsonobj.push(SubOrg);
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GETGRNData,
        data: "Params=" + JSON.stringify(Jsonobj),
        success: function (resp) {
            if (resp.length != 0) {

                $('#txtPosCode').val(resp[0].POSCode);
                $('#txtCounterCode').val(resp[0].Code);
                var CurDate = new Date(resp[0].Name);
                var DateWithFormate = CurDate.getFullYear() + '-' + ((CurDate.getMonth() + 1) < 10 ? '0' : '') + (CurDate.getMonth() + 1) + '-' + (CurDate.getDate() < 10 ? '0' : '') + CurDate.getDate();
                ConvertDateWithFormat($('#hdnDateFromate').val(), DateWithFormate, "txtCounterName");
                $("#txtDescription").val(resp[0].Description);
                $('#hdnBranchId').val(resp[0].Branchid);
                $('#hdnTransId').val(resp[0].Counter_ID);
                $('#hdnLastUpdatedDate').val(resp[0].LastUpdatedDate);
                $('#lblLastUpdate').css("display", "block");
                $('#trSBInnerRow li span#lblLastUpdate').html($('#trSBInnerRow li span#lblLastUpdate').html());
                $('#trSBInnerRow li span#lblLastUpdatedUser').html(resp[0].LastUpdateduser);
                $('#trSBInnerRow li span#lblLastUpdatedDate').html(resp[0].LastUpdatedDate);
                $('#lblGrandTotal')[0].innerHTML = parseFloat(resp[0].Amount);//258
                $('#lblGrandTotalOriginal')[0].innerHTML = parseFloat(resp[0].Amount).toFixed(4);
                $('#trSBInnerRow li span#lblMode').html($('#hdnView').val());
                if (resp[0].Status == "N") {
                    $("#btnCreate").attr('style', 'display:block;');
                    $("#ShowMenuModes").attr('style', 'display:block;');
                    $("#btnSave").attr('style', 'display:none;');
                    $("#btnEdit").attr('style', 'display:block;');
                    $("#btnDelete").attr('style', 'display:none;');
                    $("#btnDelete").attr('style', 'border-left:1px solid #cac8c8;');
                    $("#btnCancel").attr('style', 'display:block;');
                    $("#SummaryGrid").attr('style', 'display:none;');
                    $("#NewPage").attr('style', 'display:block;');
                    $("#divStatusGroup").attr('style', 'display:block;');
                    $("#divNavBarRight").attr('style', 'display:none;');
                    $("#btnInActive").addClass("disabledHyperlink");
                    $("#btnActive").removeClass("disabledHyperlink");
                }
                else {
                    $("#btnInActive").removeClass("disabledHyperlink");
                    $("#btnActive").addClass("disabledHyperlink");
                    $("#btnCreate").attr('style', 'display:block;');
                    $("#ShowMenuModes").attr('style', 'display:block;');
                    $("#btnSave").attr('style', 'display:none;');
                    $("#btnEdit").attr('style', 'display:block;');
                    $("#btnDelete").attr('style', 'display:block;');
                    $("#btnCancel").attr('style', 'display:block;');
                    $("#SummaryGrid").attr('style', 'display:none;');
                    $("#NewPage").attr('style', 'display:block;');
                    $("#divStatusGroup").attr('style', 'display:block;');
                    $("#divNavBarRight").attr('style', 'display:none;');
                }
                $('#txtCounterCode').attr("disabled", "disabled");
                $('#txtCounterName').attr("disabled", "disabled");
                $("#txtDescription").attr("disabled", "disabled");
                $("#itemCarttable tbody tr").remove();
                var daraItems_ = JSON.parse(resp[0].GRNItems);
                if (daraItems_.length > 0) {
                    for (var i = 0; i < daraItems_.length; i++) {
                        //var markup = "<tr id=" + 'tr_' + daraItems_[i].ItemId + " disabled><td style='width:5% !important' disabled ><img src='images/delete.png' onclick='deleteRow(this)' disabled  /></td><td >" + daraItems_[i].ItemCode + "/" + daraItems_[i].ItemName + "</td><td>" + daraItems_[i].UOM + "</td><td align='right'><input type='text' onkeyup='UpdateQtySubChange(this);' onkeypress='return IsNumericQTY(event);' style='text-align:right' class='form-control'  value='" + parseFloat(daraItems_[i].QTY).toFixed(2) + "' id='" + 'td_' + daraItems_[i].ItemId + "_QTY'  disabled ></td><td style='display:none'>" + daraItems_[i].ItemId + "</td><td><input onkeyup='UpdatePriceSubChange(this);' onkeypress='return IsNumericQTY(event);' type='text' style='text-align:right' class='form-control'  value='" + parseFloat(daraItems_[i].Price).toFixed(2) + "' id='" + 'td_' + daraItems_[i].ItemId + "' disabled></td><td style='display:none'></td><td align='right' >" + parseFloat(daraItems_[i].Amount).toFixed(2) + "</td><td style='display:none'></td><td style='display:none'></td><td style='display:none'>0</td><td style='display:none' >" + daraItems_[i].RGuid + "</td><td style='display:none' >" + daraItems_[i].LastUpdatedDate + "</td></tr>";//gopal
                          var markup = "<tr id=" + 'tr_' + daraItems_[i].ItemId + " disabled><td style='width:5% !important' disabled ><i class='fa fa-trash' onclick='deleteRow(this)' style='font-size:16px' id='btnrecDele'></i></td><td >" + daraItems_[i].ItemCode + "/" + daraItems_[i].ItemName + "</td><td>" + daraItems_[i].UOM + "</td><td align='right'><input type='text' onkeyup='UpdateQtySubChange(this);' onkeypress='return IsNumericQTY(event,this,2);' style='text-align:right' class='form-control'  value='" + parseFloat(daraItems_[i].QTY).toFixed(2) + "' id='" + 'td_' + daraItems_[i].ItemId + "_QTY'  disabled ></td><td style='display:none'>" + daraItems_[i].ItemId + "</td><td><input onkeyup='UpdatePriceSubChange(this);' onkeypress='return IsNumericQTY(event,this,2);' type='text' style='text-align:right' class='form-control'  value='" + parseFloat(daraItems_[i].Price).toFixed(2) + "' id='" + 'td_' + daraItems_[i].ItemId + "' disabled></td><td style='display:none'></td><td align='right' >" + toNumberDecimalTwo(daraItems_[i].Amount) + "</td><td style='display:none'></td><td style='display:none'></td><td style='display:none'>" + daraItems_[i].Amount + "</td><td style='display:none' >" + daraItems_[i].RGuid + "</td><td style='display:none' >" + daraItems_[i].LastUpdatedDate + "</td></tr>";//gopal //258
                        $("#itemCarttable tbody").append(markup);
                    }
                }
                var divScan = document.getElementById("divScan");
                divScan.style.display = "none";

            }
        },
        error: function (e) {
            navigator.notification.alert($('#hdnInvalData').val(), "", "neo ecr", "Ok");

        }
    });

    $('#PrimeInfo').addClass('open');
    $('#divPrimeInformation').attr('style', 'display:block');
 $('#hdnMode').val('View');
}
function ConvertDateWithFormatLocalRet(formates, dateText) {
    window.localStorage.setItem('ctCheck', 1);
    var hdnformat = formates;
    var splitc;
    if (hdnformat == 'd/m/Y' || hdnformat == 'm/d/Y' || hdnformat == 'Y/d/m' || hdnformat == 'Y/m/d' || hdnformat == 'M/d/Y' || hdnformat == 'd/M/Y') {
        splitc = '/';
    }
    else if (hdnformat == 'd-m-Y' || hdnformat == 'm-d-Y' || hdnformat == 'Y-d-m' || hdnformat == 'Y-m-d' || hdnformat == 'M-d-Y' || hdnformat == 'd-M-Y') {
        splitc = '-';
    }
    else if (hdnformat == 'd,m,Y' || hdnformat == 'm,d,Y' || hdnformat == 'Y,d,m' || hdnformat == 'Y,m,d' || hdnformat == 'M,d,Y' || hdnformat == 'd,M,Y') {

        splitc = ',';
    }
    else if (hdnformat == 'd m Y' || hdnformat == 'm d Y' || hdnformat == 'Y d m' || hdnformat == 'Y m d' || hdnformat == 'M d Y' || hdnformat == 'd M Y') {

        splitc = ' ';
    }
    else if (hdnformat == 'd.m.Y' || hdnformat == 'm.d.Y' || hdnformat == 'Y.d.m' || hdnformat == 'Y.m.d' || hdnformat == 'M.d.Y' || hdnformat == 'd.M.Y') {
        splitc = '.';
    };
    var sym = '-';
    if (dateText.indexOf('/') != -1)
        sym = '/';
    var vardatesplit = dateText.split(sym);
    var varM;
    var varMn;
    var varfformatS = hdnformat.split(splitc);
    if (varfformatS[0] == 'M' || varfformatS[1] == 'M') {

        if (vardatesplit[1] != null)
            varM = vardatesplit[1];
        if (vardatesplit[0] != null)
            varM = vardatesplit[0];

        switch (varM) {
            case 'Jan': varMn = '01';
                break;
            case 'Feb': varMn = '02';
                break;
            case 'Mar': varMn = '03';
                break;
            case 'Apr': varMn = '04';
                break;
            case 'May': varMn = '05';
                break;
            case 'Jun': varMn = '06';
                break;
            case 'Jul': varMn = '07';
                break;
            case 'Aug': varMn = '08';
                break;
            case 'Sep': varMn = '09';
                break;
            case 'Oct': varMn = '10';
                break;
            case 'Nov': varMn = '11';
                break;
            case 'Dec': varMn = '12';
                break;
        }

    }
    if (varfformatS[0] == 'd' && (varfformatS[1] == 'm' || varfformatS[1] == 'M') && varfformatS[2] == 'Y') {
        if (varfformatS[1] == 'M') {
            return (varMn + splitc + vardatesplit[1] + splitc + vardatesplit[0]);
        }
        else {
            return (vardatesplit[1] + splitc + vardatesplit[0] + splitc + vardatesplit[2]);
        }
    }
    else if ((varfformatS[0] == 'm' || varfformatS[0] == 'M') && varfformatS[1] == 'd' && varfformatS[2] == 'Y') {
        if (varfformatS[0] == 'M') {
            return (varMn + splitc + vardatesplit[2] + splitc + vardatesplit[0]);
        }
        else {
            return (vardatesplit[0] + splitc + vardatesplit[1] + splitc + vardatesplit[2]);
        }
    }
    else if (varfformatS[0] == 'Y' && varfformatS[1] == 'd' && varfformatS[2] == 'm') {
        return (vardatesplit[2] + splitc + vardatesplit[1] + splitc + vardatesplit[0]);
    }
    else if (varfformatS[0] == 'Y' && varfformatS[1] == 'm' && varfformatS[2] == 'd') {
        return (vardatesplit[2] + splitc + vardatesplit[0] + splitc + vardatesplit[1]);
    }
    else {
        return (vardatesplit[0] + splitc + vardatesplit[1] + splitc + vardatesplit[2]);
    }
}
function ConvertDateWithFormatLocal(formates, dateText, textdate) {
    window.localStorage.setItem('ctCheck', 1);
    var hdnformat = formates;
    var txtMainDate = textdate;
    var splitc;
    if (hdnformat == 'd/m/Y' || hdnformat == 'm/d/Y' || hdnformat == 'Y/d/m' || hdnformat == 'Y/m/d' || hdnformat == 'M/d/Y' || hdnformat == 'd/M/Y') {
        splitc = '/';
    }
    else if (hdnformat == 'd-m-Y' || hdnformat == 'm-d-Y' || hdnformat == 'Y-d-m' || hdnformat == 'Y-m-d' || hdnformat == 'M-d-Y' || hdnformat == 'd-M-Y') {
        splitc = '-';
    }
    else if (hdnformat == 'd,m,Y' || hdnformat == 'm,d,Y' || hdnformat == 'Y,d,m' || hdnformat == 'Y,m,d' || hdnformat == 'M,d,Y' || hdnformat == 'd,M,Y') {

        splitc = ',';
    }
    else if (hdnformat == 'd m Y' || hdnformat == 'm d Y' || hdnformat == 'Y d m' || hdnformat == 'Y m d' || hdnformat == 'M d Y' || hdnformat == 'd M Y') {

        splitc = ' ';
    }
    else if (hdnformat == 'd.m.Y' || hdnformat == 'm.d.Y' || hdnformat == 'Y.d.m' || hdnformat == 'Y.m.d' || hdnformat == 'M.d.Y' || hdnformat == 'd.M.Y') {
        splitc = '.';
    };
    var sym = '-';
    if (dateText.indexOf('/') != -1)
        sym = '/';
    var vardatesplit = dateText.split(sym);
    var varM;
    var varMn;
    var varfformatS = hdnformat.split(splitc);
    if (varfformatS[0] == 'M' || varfformatS[1] == 'M') {

        if (vardatesplit[1] != null)
            varM = vardatesplit[1];
        if (vardatesplit[0] != null)
            varM = vardatesplit[0];

        switch (varM) {
            case 'Jan': varMn = '01';
                break;
            case 'Feb': varMn = '02';
                break;
            case 'Mar': varMn = '03';
                break;
            case 'Apr': varMn = '04';
                break;
            case 'May': varMn = '05';
                break;
            case 'Jun': varMn = '06';
                break;
            case 'Jul': varMn = '07';
                break;
            case 'Aug': varMn = '08';
                break;
            case 'Sep': varMn = '09';
                break;
            case 'Oct': varMn = '10';
                break;
            case 'Nov': varMn = '11';
                break;
            case 'Dec': varMn = '12';
                break;
        }

    }
    if (varfformatS[0] == 'd' && (varfformatS[1] == 'm' || varfformatS[1] == 'M') && varfformatS[2] == 'Y') {
        if (varfformatS[1] == 'M') {
            $("#" + txtMainDate).val(varMn + splitc + vardatesplit[1] + splitc + vardatesplit[0]);
        }
        else {
            $("#" + txtMainDate).val(vardatesplit[1] + splitc + vardatesplit[0] + splitc + vardatesplit[2]);
        }
    }
    else if ((varfformatS[0] == 'm' || varfformatS[0] == 'M') && varfformatS[1] == 'd' && varfformatS[2] == 'Y') {
        if (varfformatS[0] == 'M') {
            $("#" + txtMainDate).val(varMn + splitc + vardatesplit[2] + splitc + vardatesplit[0]);
        }
        else {
            $("#" + txtMainDate).val(vardatesplit[0] + splitc + vardatesplit[1] + splitc + vardatesplit[2]);
        }
    }
    else if (varfformatS[0] == 'Y' && varfformatS[1] == 'd' && varfformatS[2] == 'm') {
        $("#" + txtMainDate).val(vardatesplit[2] + splitc + vardatesplit[1] + splitc + vardatesplit[0]);
    }
    else if (varfformatS[0] == 'Y' && varfformatS[1] == 'm' && varfformatS[2] == 'd') {
        $("#" + txtMainDate).val(vardatesplit[2] + splitc + vardatesplit[0] + splitc + vardatesplit[1]);
    }
    else {
        $("#" + txtMainDate).val(vardatesplit[0] + splitc + vardatesplit[1] + splitc + vardatesplit[2]);
    }
}
//End
//Edit Function
function Edit() {
   window.localStorage.setItem('ctCheck', 1);
    $('#trSBInnerRow li span#lblMode').html($('#hdnEdit').val());
    $("#btnCreate").attr('style', 'display:none;');
    $("#ShowMenuModes").attr('style', 'display:block;');
    $("#btnSave").attr('style', 'display:block;');
    $("#btnEdit").attr('style', 'display:none;');
    $("#btnDelete").attr('style', 'display:none;');// 258
   // $("#btnDelete").attr('style', 'border-left:1px solid #cac8c8;');
    $("#btnCancel").attr('style', 'display:block;');
    $("#divStatusGroup").attr('style', 'display:none;');
   // $('#txtCounterName').removeAttr("disabled", "disabled"); // 258
    $('#txtCounterName').attr("disabled", "disabled"); // 258
    $("#txtDescription").removeAttr("disabled", "disabled");
    $("#divNavBarRight").attr('style', 'display:none;');
    $('#PrimeInfo').addClass('open');
    $('#divPrimeInformation').attr('style', 'display:block');
    $('#ShippingInfo').removeClass('open');
    $('#divShippingInformation').attr('style', 'display:none');
    var divScan = document.getElementById("divScan"); // 258
    divScan.style.display = "block";// 258
    $('#hdnOE').val("");
    EnableGrid();
    $('#hdnMode').val('Edit');
}
//End
//Delete function
function Delete() {
    window.localStorage.setItem('ctCheck', 1);
    $('#hdnMode').val('Delete');
    if ($('#hdnTransId').val() != "") {
        $('#SummaryGrid').css("display", "block");
        $('#NewPage').css("display", "none");
        $("#btnCreate").attr('style', 'display:block;');
        $("#ShowMenuModes").attr('style', 'display:none;');
        $("#divNavBarRight").attr('style', 'display:block;');
    }
    if (confirm($('#hdnDeleteConfirm').val())) {
        var vd_ = window.localStorage.getItem("UUID");
  var myTab = document.getElementById('itemCarttable');
        var IN103 = {
        }
        IN103["TransId"] = $('#hdnTransId').val();
        IN103["LastUpdatedDate"] = $('#hdnLastUpdatedDate').val();
        objIN103 = [];
        objIN103.push(IN103);
        objIN104 = [];
        var ItemsCheck = 0;
        for (i = 1; i < myTab.rows.length; i++) {
            IN104 = {
            }
            var objCells = myTab.rows.item(i).cells;
            if (objCells.length > 0) {
                if (objCells.item(4).innerHTML != "") {

                    IN104["RGuid"] = objCells.item(11).innerHTML;
                    IN104["LastUpdatedDate"] = objCells.item(12).innerHTML;

                    objIN104.push(IN104);
                }
            }
        }

        var GRNParams = new Object();
        GRNParams.IN103 = JSON.stringify(objIN103);
        GRNParams.IN104 = JSON.stringify(objIN104);
        GRNParams.VD = window.localStorage.getItem("UUID");
       

        //Put the customer object into a container
        var ComplexObject = new Object();
        //The property name "WebMethodArgName" must match the web method argument "WebMethodArgName"!
        ComplexObject.GRNData = GRNParams;

        //Stringify and verify the object is foramtted as expected
        var data = JSON.stringify(ComplexObject);
        $.ajax({
            type: 'POST',
            url: CashierLite.Settings.GRNDelete,
            data: data,
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            success: function (resp) {
              /*  var result = resp.split(',');
                if (result[0] == "100000") {
                    GetSummaryGridData('Summary');
                    navigator.notification.alert($('#txtCounterCode').val() + ' ' + $('#hdnRecDelSucc').val(), "", "neo ecr", "Ok");
                    clearControls();
                }*/
                   if (resp.GRNDeleteResult == "100000") {
                    GetSummaryGridData('Summary');
                    navigator.notification.alert($('#txtCounterCode').val() + ' ' + $('#hdnRecDelSucc').val(), "", "neo ecr", "Ok");
                    clearControls();

                }
            },
            error: function (e) {
                navigator.notification.alert($('#hdnInvalData').val(), "", "neo ecr", "Ok");


            }
        });
    } else {
        return false;
    }
}
//End
function clearControls() {
    window.localStorage.setItem('ctCheck', 1);
    $('#txtCounterCode').val("");
    $("#txtDescription").val("");
    $('#itemCarttable tbody tr').remove();
    $('#lblGrandTotal')[0].innerHTML = "0.00";
    $('#lblGrandTotalOriginal')[0].innerHTML = "0.0000";

}
function FilterSummaryGrid(ControlData, ColumnData) {
    window.localStorage.setItem('ctCheck', 1);
    $('#txtFindGo').val('');
    result = [];
    result = JSON.parse((window.localStorage.getItem("CounterDefFilterGrid")));
    var SummaryTable;


    if (ColumnData == 'Status') {
        $("#hdnFilterType").val(ControlData.toLowerCase());
        GetSummaryGridData(ControlData.toLowerCase())

    }
    else
        if (ColumnData == 'CREATEDDATE') {
            $("#hdnFilterType").val(ControlData);
            GetSummaryGridData(ControlData)
        }

}
function Reciept_prev() {
    window.localStorage.setItem('ctCheck', 1);
    var pageIndexCreate = parseFloat($('#hdnPageIndex').val()) - 1;
    if (parseFloat(pageIndexCreate) == 0) {
        pageIndexCreate = '1';
    }
    $('#hdnPageIndex').val(pageIndexCreate);
    var FilterType = $("#hdnFilterType").val();
    GetSummaryGridData(FilterType);

}
function Reciept_next() {
    window.localStorage.setItem('ctCheck', 1);
    var pageIndexCreate = parseFloat($('#hdnPageIndex').val()) + 1;

    $('#hdnPageIndex').val(pageIndexCreate);
    var FilterType = $('#hdnFilterType').val();
    GetSummaryGridData(FilterType + '~Index');

}


function GetSummaryGridData(FilterType) {

    window.localStorage.setItem('ctCheck', 1);
    var Index = $('#hdnPageIndex').val();
    var MethodType = "";
    var FindGo = "";
    var QryType = "";
    if (FilterType == 'FindGo') {
        FindGo = $('#txtFindGo').val();
        Index = 1;
    }
    if (FilterType == 'Summary') {
        $('#txtFindGo').val('');
    }
    else {
        FindGo = $('#txtFindGo').val();
    }
    if (FilterType.indexOf('Index') != -1) {
        FilterType = FilterType.split('~')[0];
        MethodType = "next";
    }

    var HashVal = (window.localStorage.getItem("hashValues"));
    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');
    }
    var SummaryTable = '';
    Jsonobj = [];
    var SubOrg = {};
    SubOrg.SubOrgID =HashValues[1];// $('#hdnBranchId').val();
    var vd_ = window.localStorage.getItem("UUID");
    SubOrg.VD = vd_;
    SubOrg.Index = Index;
    SubOrg.FilterType = FilterType;
    SubOrg.FindGoVal = FindGo;
    Jsonobj.push(SubOrg);
    $("#hdnFilterType").val(FilterType);

    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GETGRNSummaryGridData,
        data: "Params=" + JSON.stringify(Jsonobj),
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function (resp) {
            var result = resp;
            if (result.length > 0) {
                window.localStorage.setItem("CounterDefFilterGrid", JSON.stringify(result))
                $('#tblSummaryGrid >tbody').children().remove();
           if(HashValues[7]=="ar-SA")
            {
               for (var i = 0; i < result.length; i++) {
                    var CurDate = new Date(result[i].Name);
                    var DateWithFormate = ((CurDate.getMonth() + 1) < 10 ? '0' : '') + (CurDate.getMonth() + 1) + '-' + (CurDate.getDate() < 10 ? '0' : '') + CurDate.getDate() + '-' + CurDate.getFullYear();
                   // SummaryTable += "<tr onclick='DoubleClick(this)'><td style='display:none'>" + result[i].Counter_ID + "</td><td>" + result[i].Code + "</td><td>" + ConvertDateWithFormatLocalRet($('#hdnDateFromate').val(), DateWithFormate) + "</td><td  text-align='right' style='text-align: end;'>" + parseFloat(result[i].CounterType).toFixed(2) + "</td></tr>";
                      SummaryTable += "<tr onclick='DoubleClick(this)'><td style='display:none'>" + result[i].Counter_ID + "</td><td>" + result[i].Code + "</td><td>" + ConvertDateWithFormatLocalRet($('#hdnDateFromate').val(), DateWithFormate) + "</td><td  style='text-align:left !important;'>" + toNumberDecimalTwo(result[i].CounterType) + "</td></tr>"; //258
                }
            }
            else
             {
                 for (var i = 0; i < result.length; i++) {
                    var CurDate = new Date(result[i].Name);
                    var DateWithFormate = ((CurDate.getMonth() + 1) < 10 ? '0' : '') + (CurDate.getMonth() + 1) + '-' + (CurDate.getDate() < 10 ? '0' : '') + CurDate.getDate() + '-' + CurDate.getFullYear();
                   // SummaryTable += "<tr onclick='DoubleClick(this)'><td style='display:none'>" + result[i].Counter_ID + "</td><td>" + result[i].Code + "</td><td>" + ConvertDateWithFormatLocalRet($('#hdnDateFromate').val(), DateWithFormate) + "</td><td  text-align='right' style='text-align: end;'>" + parseFloat(result[i].CounterType).toFixed(2) + "</td></tr>";
                      SummaryTable += "<tr onclick='DoubleClick(this)'><td style='display:none'>" + result[i].Counter_ID + "</td><td>" + result[i].Code + "</td><td>" + ConvertDateWithFormatLocalRet($('#hdnDateFromate').val(), DateWithFormate) + "</td><td  style='text-align:right !important;'>" + toNumberDecimalTwo(result[i].CounterType) + "</td></tr>"; //258
                }
              }

                $('#tblSummaryGrid tbody').remove();
                SummaryTable += "<tbody>" + SummaryTable + "</tbody>";
                $('#tblSummaryGrid').append(SummaryTable);
                window.localStorage.setItem("CounterDefGrid", JSON.stringify(SummaryTable));
                //GetPaging1();
                $('#trSBInnerRow li span#lblMode').html('');
            }
            else {

                if (MethodType == "next") {
                    var pageIndexCreate = parseFloat($('#hdnPageIndex').val()) - 1;
                    $('#hdnPageIndex').val(pageIndexCreate);
                }
                else
                    $('#tblSummaryGrid tbody').remove();

            }
        },
        error: function (e) {
            navigator.notification.alert($('#hdnInvalData').val(), "", "neo ecr", "Ok");
        }

    });
}
//SummaryGrid Paging
function GetPaging1() {
    window.localStorage.setItem('ctCheck', 1);
    $('table#tblSummaryGrid').each(function () {
        var currentPage = 0;
        var numPerPage = $('#hdnPaging').val();
        var $table = $(this);
        $table.bind('repaginate', function () {
            $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
        });
        $table.trigger('repaginate');
        var numRows = $table.find('tbody tr').length;
        var numPages = Math.ceil(numRows / numPerPage);
        $("#DivPager").remove();
        var $pager = $('<div id="DivPager" class="pager"></div>');
        for (var page = 0; page < numPages; page++) {
            $('<span class="page-number"></span>').text(page + 1).bind('click', {
                newPage: page
            }, function (event) {
                currentPage = event.data['newPage'];
                $table.trigger('repaginate');
                $(this).addClass('active').siblings().removeClass('active');
            }).appendTo($pager).addClass('clickable');
        }
        $pager.insertAfter($table).find('span.page-number:first').addClass('active');
    });
}
$('#txtFindGo').keyup(function () {
    window.localStorage.setItem('ctCheck', 1);
    result = [];
    result = JSON.parse((window.localStorage.getItem("CounterDefFilterGrid")));
    if ($('#txtFindGo').val().trim() == "" || result.length <= 0) {

        GetSummaryGridData('Summary');
        return false;
    }
    else {

        GetSummaryGridData('FindGo');

    }
})

function clickActive() {
    deActiveRowget();
}
//Filters
$('#btnAll').click(function () {
    GetSummaryGridData('Summary');
});
$('#btnToday').click(function () {
    FilterSummaryGrid('Today', 'CREATEDDATE')
});
$('#btnYesterday').click(function () {
    FilterSummaryGrid('Yesterday', 'CREATEDDATE')
});
$('#btnLast7days').click(function () {
    FilterSummaryGrid('Last7', 'CREATEDDATE')

});
$('#btnCurrentMonth').click(function () {
    FilterSummaryGrid('CMonth', 'CREATEDDATE')
});
$('#divActives').click(function () {
    FilterSummaryGrid('Active', 'Status')
});
$('#divInActives').click(function () {
    FilterSummaryGrid('Inactive', 'Status')
});
function Save() {

    var BRANCHID = ''; var COMPANYID = '';
    var CURRENCYID = '';
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        CURRENCYID = hashSplit[16];
        COMPANYID = hashSplit[0];
        BRANCHID = hashSplit[1];
    }

    var ItemData = ""; var CREATEDBY = window.localStorage.getItem('Sno');
    var CASHINVID = "";
    var myTab = document.getElementById('itemCarttable');
    var ItemId = ""; var Qty_ = "0"; var BasePrice = "0"; var Amount = "0"; var Disc_ = "0"; var taxTypeP_ = ''; var BaseAmount = '';
    var IN103 = {
    }
    var IN104 = {
    }
 var DOE = {
    }
    IN103["DNO"] = $('#txtCounterCode').val();
    IN103["DDate"] = GetDateWithYYYYMMDD($('#hdnDateFromate').val(), $('#txtCounterName').val());
    IN103["Desc"] = $('#txtDescription').val();
    IN103["CURRENCYID"] = CURRENCYID;
    IN103["BRANCHID"] = BRANCHID;
    IN103["COMPANYID"] = COMPANYID;
    IN103["CREATEDBY"] = CREATEDBY;
    IN103["UPDATEDSITE"] = 'DBMS_REPUTIL.GLOBAL_NAME';
    IN103["LANGID"] = 'en';
    IN103["Total"] = $('#lblGrandTotalOriginal')[0].innerHTML;
    IN103["TransId"] = $('#hdnTransId').val();
    IN103["LastUpdatedDate"] = $('#hdnLastUpdatedDate').val();
    objIN103 = [];
    objIN103.push(IN103);
    objIN104 = [];
    var ItemsCheck = 0;
    for (i = 1; i < myTab.rows.length; i++) {
        Tax_ = 0;
        IN104 = {
        }
        var objCells = myTab.rows.item(i).cells;
        if (objCells.length > 0) {
            if (objCells.item(4).innerHTML != "") {
                ItemsCheck = 1;
                ItemId = objCells.item(4).innerHTML;
                Qty_ = '#td_' + ItemId + '_QTY';
              //  Amount = objCells.item(7).innerHTML;
                  Amount = objCells.item(10).innerHTML; // 258
                BaseAmount = '#td_' + ItemId;
                IN104["ITEMID"] = ItemId;
                IN104["UOMID"] = objCells.item(6).innerHTML;
                IN104["DISCOUNT"] = 0;
                IN104["ITEMTAX"] = Tax_;
                IN104["QUANTITY"] = $(Qty_).val();
                IN104["PRICE"] = $(BaseAmount).val();
                IN104["Total"] = Amount;
                IN104["mapCode"] = "";
                IN104["BRANCHID"] = BRANCHID;
                IN104["COMPANYID"] = COMPANYID;
                IN104["CREATEDBY"] = CREATEDBY;
                IN104["UPDATEDSITE"] = 'DBMS_REPUTIL.GLOBAL_NAME';
                IN104["LANGID"] = 'en';
                IN104["RGuid"] = objCells.item(11).innerHTML;
                IN104["LastUpdatedDate"] = objCells.item(12).innerHTML;// $('#hdnLastUpdatedDate').val();
                objIN104.push(IN104);
                if ($(BaseAmount).val() == "") {
                    //alert('Please enter price');
                    navigator.notification.alert($('#hdnPricegreaterthanzero').val(), "", "neo ecr", "Ok");

                    return false;
                }
            }
        }
    }

    if (ItemsCheck == 0) {

        navigator.notification.alert($('#hdnPlsflItemgrid').val(), "", "neo ecr", "Ok");

        return false;
    }
     objDOE = [];//Gopal
    if ($('#hdnOE').val() != "") {
        var OER = $('#hdnOE').val().split('^');
        for (i = 0; i < OER.length - 1; i++) {
            if (OER[i] != "") {
                var OER1 = OER[i].split('~');

                DOE["Rid"] = OER1[0];
                DOE["LUD"] = OER1[1];
                objDOE.push(DOE);
            }
        }

    }
    var GRNParams = new Object();
    GRNParams.IN103 = JSON.stringify(objIN103);
    GRNParams.IN104 = JSON.stringify(objIN104);
    GRNParams.DOE = JSON.stringify(objDOE);//Gopal
    GRNParams.VD = VD1();
    //Put the customer object into a container
    var ComplexObject = new Object();
    //The property name "WebMethodArgName" must match the web method argument "WebMethodArgName"!
    ComplexObject.GRNData = GRNParams;
    //Stringify and verify the object is foramtted as expected
    var data = JSON.stringify(ComplexObject);
    var img = document.getElementById('loader');
    img.style.display = "none";
    $.ajax({
        type: 'POST',
        url: CashierLite.Settings.GRNSave,
        data: data,
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function (resp) {
            img.style.display = "none";

            var d_ = '';
            if (resp.GRNSaveResult.indexOf('~') != -1) {
                d_ = resp.GRNSaveResult.split('~')[0];
                counterCode = resp.GRNSaveResult.split('~')[1];
                resp.GRNSaveResult = d_;
            }
            if (resp.GRNSaveResult == "100002") {
                navigator.notification.alert('Document No. ' + counterCode + ' Aleady exists', "", "neo ecr", "Ok");
            }
            else if (resp.GRNSaveResult == "100001") {
                navigator.notification.alert('error...?', "", "neo ecr", "Ok");
            }
            else {
                navigator.notification.alert(counterCode + ' ' + $('#hdnRecSaveSucc').val(), "", "neo ecr", "Ok");
                clearControls();
            }
            $('#PrimeInfo').addClass('open');
            $('#divPrimeInformation').attr('style', 'display:block');
            $('#ShippingInfo').removeClass('open');
            $('#divShippingInformation').attr('style', 'display:none');
            $("#btnDelete").attr('style', 'display:none;');
        },
        error: function (e) {
            img.style.display = "none";

        }
    });
}
$("#btnActive").click(function () {
    $('#hdnScreenMode').val("Active");
    Save();
});
//InActive Record
$("#btnInActive").click(function () {
    $('#hdnScreenMode').val("InActive");
    Save();
});
$("#btnCreate").click(function () { 
    $('#PrimeInfo').addClass('open');
    $('#divPrimeInformation').attr('style', 'display:block');
    $('#ShippingInfo').removeClass('open');
    $('#divShippingInformation').attr('style', 'display:none');
    clearControls();
    defaultdata(); 
    $('#lblLastUpdate').css("display", "none"); 
    $('#trSBInnerRow li span#lblLastUpdatedUser').html("");
    $('#trSBInnerRow li span#lblLastUpdatedDate').html(""); 
    $('#trSBInnerRow li span#lblMode').html($('#hdnNew').val());
    $('#hdnMode').val('New');
    $('#txtCounterCode').removeAttr("disabled", "disabled");
    $('#txtCounterName').removeAttr("disabled", "disabled");
    $('#txtCounterCode').prop("disabled", true);
    $("#txtDescription").removeAttr("disabled", "disabled");
    $("#btnCreate").attr('style', 'display:none;');
    $("#ShowMenuModes").attr('style', 'display:block;');
    $("#btnSave").attr('style', 'display:block;');
    $("#btnEdit").attr('style', 'display:none;');
    $("#btnDelete").attr('style', 'display:none;');
    $("#btnCancel").attr('style', 'display:block;');
    $("#SummaryGrid").attr('style', 'display:none;');
    $("#NewPage").attr('style', 'display:block;');
    $("#divStatusGroup").attr('style', 'display:none;');
    $("#divNavBarRight").attr('style', 'display:none;');
    var divScan = document.getElementById("divScan");
    divScan.style.display = "block";
    $("#ShippingInfo").addClass("deactive-link");
    var CurDate = new Date();
    var DateWithFormate = CurDate.getFullYear() + '-' + (CurDate.getMonth() + 1) + '-' + CurDate.getDate();
    ConvertDateWithFormat($('#hdnDateFromate').val(), DateWithFormate, "txtCounterName");
 
});
$("#btnCancel").click(function () {
    clearControls();
    $('#lblLastUpdate').css("display", "none");
    //$('#trSBInnerRow li span#lblLastUpdate').html("");
    $('#trSBInnerRow li span#lblLastUpdatedUser').html("");
    $('#trSBInnerRow li span#lblLastUpdatedDate').html("");
    //$('#trSBInnerRow li span#lblMode').html("Default");
    $('#trSBInnerRow li span#lblMode').html('');
    $("#btnCreate").attr('style', 'display:block;');
    $("#ShowMenuModes").attr('style', 'display:none;');
    $("#SummaryGrid").attr('style', 'display:block;');
    $("#NewPage").attr('style', 'display:none;');
    $("#divNavBarRight").attr('style', 'display:block;');
    defaultdata();
    GetSummaryGridData('Summary');
    var divScan = document.getElementById("divScan");
    divScan.style.display = "block";

});
$('#btnSave').click(function () {

    if ($('#txtCounterName').val() == "" && $('#txtCounterName').val().trim().length == 0) {
        navigator.notification.alert('Please enter Document Date', "", "neo ecr", "Ok");
        return false;
    }
    if (new Date() < new Date(GetDateWithYYYYMMDD($('#hdnDateFormate').val(), ($('#txtCounterName').val())))) {
        navigator.notification.alert('Document Date should be lessthan or equal to current date', "", "neo ecr", "Ok");
        return false;
    }
    for (i = 1; i < itemCarttable.rows.length; i++) {
        var objCells = itemCarttable.rows.item(i).cells;

        if (objCells.length > 0) {
            if (objCells.item(4).innerHTML != "") {
                ItemsCheck = 1;
                ItemId = objCells.item(4).innerHTML;
                Qty_ = '#td_' + ItemId + '_QTY';
                var Qty_val = $(Qty_).val();
                BaseAmount = '#td_' + ItemId;
                var price_val = $(BaseAmount).val();

                if (Qty_val == null || Qty_val == "" || parseFloat(Qty_val) <= 0) {
                    navigator.notification.alert($('#hdnQtygreaterthanzero').val(), "", "neo ecr", "Ok");
                    return false;

                }
                if (price_val == null || price_val == "") {
                    navigator.notification.alert($('#hdnPricegreaterthanzero').val(), "", "neo ecr", "Ok");
                    return false;

                }
            }
        }

    }
    Save();
});
$('#btnEdit').click(function () {
    Edit();
});
$('#btnDelete').click(function () {
    Delete();
});

$('#hdnCounterNo').val();
function GetAutoCodeGen(str, iScreenCode) {  //str Suborrganization , icode PageCode 
    var vd_ = window.localStorage.getItem("UUID");;

    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetAutoGenHashTableCheck,
        data: "SubOrgId=" + str + "&iScreenCode=" + iScreenCode + "&VD=" + vd_,
        success: function (resp) {
            var CounterNo = resp;
            if (CounterNo.length == 1) {
                $('#hdnCounterNo').val(CounterNo[0].PageName);
            }
        },
        error: function (e) {
        }
    });
}
function GenerateCode(strOrgCode, strVarCode, strParamType, strTable, strColumn, USERID, BRANCHID, COMPANYID, LANGUAGE) {
    var vd_ = window.localStorage.getItem("UUID");
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GenerateCode,
        data: "strOrgCode=" + strOrgCode + "&strVarCode=" + strVarCode + "&strParamType=" + strParamType + "&strTable=" + strTable + "&strColumn=" + strColumn + "&USERID=" + USERID + "&BRANCHID=" + BRANCHID + "&COMPANYID=" + COMPANYID + "&LANGUAGE=" + LANGUAGE + "&VD=" + vd_,

        success: function (resp) {
            var CounterNo = resp;
            if (CounterNo.length > 1) {
                $('#txtCounterCode').val(CounterNo);


            }
        },
        error: function (e) {
        }
    });
}
function getResources(langCode, pageCode) {

    $.ajax({
        type: 'GET',
        
        url: CashierLite.Settings.getResource,
        data: "langCode=" + langCode + "&pageCode=" + pageCode,
        success: function (resp) {
            setLabels(resp);
        }
    });
}
function PageValidatebyPage(pagecode) {
    window.localStorage.setItem('ctCheck', 1);
    if (pagecode != '') {

        var LoginId = window.localStorage.getItem("Sno");
        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.PageValidatebyPage,
            data: "uid=" + LoginId + "&CounterId=" + window.localStorage.getItem('COUNTERTYPE') + "&PageCode=" + pagecode + "&VD=" + VD1(),
            success: function (resp) {

                var s_ = resp;

                var s0_ = resp.split('~');
                var s_ = s0_[1];
                var s1_ = s0_[0];

                if (s1_ == '2') {
                    window.localStorage.setItem("RoleId", "Admin");

                }
                else {
                    window.localStorage.setItem("RoleId", "User");
                }
                if (s_ == '1') {


                    if (pagecode == 'POSINVCFG') {
                        window.open('POSPrintCustomize.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSBCODG') {
                        window.open('POSBarcodeGenerator.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSCNTTYP') {
                        window.open('POSCounterType.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSCOUDF') {
                        window.open('PosCounterDef.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSCTRPRM') {
                        window.open('POSCounterPermissions.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSCUSTM') {
                        window.open('CustomerMaster.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSTENDERTYPE') {
                        window.open('TenderTypes.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSLOYPRG') {
                        window.open('LoyaltyPoints.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSIORCR') {
                        GetShiftData();
                    }
                    if (pagecode == 'POSSLSRT') {
                        window.open('SalesReturn.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSDISPATCH') {
                        window.open('POSDeliveryStatusDetails.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSCOUOP') {
                        dayCloserChecking();
                    }
                    if (pagecode == 'POSCOUCL') {
                        window.open('CounterClosing.html', '_self', 'location=no');
                    }

                    if (pagecode == 'POSACPO') {

                        getPOSPage('POSACPO');
                    }
                    if (pagecode == 'POSRPTINVLST') {

                        getPOSPage('POSTINL');
                    }
                    if (pagecode == 'POSRPTSLRT') {
                        getPOSPage('POSSDSEN');
                    }
                    if (pagecode == 'POSRPTINVL') {
                        getPOSPage('POSINLS');
                    }
                    if (pagecode == 'POSRPTDCTN') {
                        getPOSPage('POSTTDS');
                    }
                    if (pagecode == 'POSRPTCINV') {
                        getPOSPage('POSCINV');
                    }
                    if (pagecode == 'POSRPTDE') {
                        window.open('POSReport.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSRPTSGRW') {
                        getPOSPage('POSSGR');
                    }
                    if (pagecode == 'POSConfigT') {
                        window.open('Configurator.html', '_self', 'location=no')
                    }
                    if (pagecode == 'POSRPTICSA') {
                        getPOSPage('POSRPTICSA');
                    }
                    if (pagecode == 'POSRPTBAR') {
                        getPOSPage('POSRPTBAR');
                    }
                    if (pagecode == 'POSRPTWDRR') {
                        getPOSPage('POSRPTWDRR');
                    }
                    if (pagecode == 'POSRPTLWATV') {
                        getPOSPage('POSRPTLWATV');
                    }
                    if (pagecode == 'POSRPTICPA') {
                        getPOSPage('POSRPTICPA');
                    }
                    if (pagecode == 'POSRPTLWPAS') {
                        getPOSPage('POSRPTLWPAS');
                    }
                    if (pagecode == 'POSTVRR') {
                        getPOSPage('POSTVRR');
                    }
                }
                else {

                }
            },
            error: function (e) {
            }
        });

    }
}
function PageValidatebyPageDash(pagecode) {
    window.localStorage.setItem('ctCheck', 1);
    if (pagecode != '') {
        var LoginId = window.localStorage.getItem("Sno");

        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.PageValidatebyPage,
            data: "uid=" + LoginId + "&CounterId=" + window.localStorage.getItem('COUNTERTYPE') + "&PageCode=" + pagecode + "&VD=" + VD1(),
            success: function (resp) {

                var s0_ = resp.split('~');
                var s_ = s0_[1];
                var s1_ = s0_[0];
                if (s1_ == '2') {
                    window.localStorage.setItem("RoleId", "Admin");
                    //s_ = '1';
                }
                else {
                    window.localStorage.setItem("RoleId", "User");
                }
                if (s_ == '1') {
                    if (pagecode == 'POSINVCFG') {
                        window.open('POSPrintCustomize.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSBCODG') {
                        window.open('POSBarcodeGenerator.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSCNTTYP') {
                        window.open('POSCounterType.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSCOUDF') {
                        window.open('PosCounterDef.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSCTRPRM') {
                        window.open('POSCounterPermissions.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSCUSTM') {
                        window.open('CustomerMaster.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSTENDERTYPE') {
                        window.open('TenderTypes.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSLOYPRG') {
                        window.open('LoyaltyPoints.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSIORCR') {
                        GetShiftData();
                    }
                    if (pagecode == 'POSSLSRT') {
                        window.open('SalesReturn.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSDISPATCH') {
                        window.open('POSDeliveryStatusDetails.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSCOUOP') {
                        window.open('CounterOpening.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSCOUCL') {
                        window.open('CounterClosing.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSACPO') {
                        window.open('AccountPosting.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSRPTINVLST') {

                        getPOSPage('POSTINL');
                    }
                    if (pagecode == 'POSRPTSLRT') {
                        getPOSPage('POSSDSEN');
                    }
                    if (pagecode == 'POSRPTINVL') {
                        getPOSPage('POSINLS');
                    }
                    if (pagecode == 'POSRPTCINV') {
                        getPOSPage('POSCINV');
                    }
                    if (pagecode == 'POSRPTDCTN') {
                        getPOSPage('POSTTDS');
                    }
                    if (pagecode == 'POSRPTDE') {
                        window.open('POSReport.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSRPTSGRW') {
                        getPOSPage('POSSGR');
                    }
                    if (pagecode == 'POSRPTICSA') {
                        window.open('ItemCategorySalesAnalysis.html', '_self', 'location=no')
                    }
                    if (pagecode == 'POSRPTBAR') {
                        window.open('BulkAnalysisReport.html', '_self', 'location=no')
                    }
                    if (pagecode == 'POSRPTWDRR') {
                        window.open('POSWeekDaysRevenueRP.html', '_self', 'location=no')
                    }
                    if (pagecode == 'POSRPTLWATV') {
                        window.open('POSLocationwiseAvgTransactionValueRPT.html', '_self', 'location=no')
                    }
                    if (pagecode == 'POSRPTICPA') {
                        window.open('POSItemCategoryProfitAnalysis.html', '_self', 'location=no')
                    }
                    if (pagecode == 'POSRPTLWPAS') {
                        window.open('POSLocationwiseProfitAnalysis.html', '_self', 'location=no')
                    }
                }
                else {
                    navigator.notification.alert($('#hdnAccessDenied').val(), "", "neo ecr", "Ok");
                }
            },
            error: function (e) {
            }
        });

    }
}

function setLabels(labelData) {
    $('#hdnPlsflItemgrid').val(labelData["PlsflItemgrid"]);
    $('#hdnPricegreaterthanzero').val(labelData["PleaseEnter"] + ' ' + labelData["price"]);
    $('#hdnQtygreaterthanzero').val(labelData["QtyGreaterthanZero"]);
    jQuery("label[for='lblUploadUserImage'").html(labelData["UploadUserImage"]);
    //$('#txtScanItem').attr('placeholder', labelData["scanItem"] + ' ' + "(ctrl+b , s)");
   // $('#txtSearchtem').attr('placeholder', labelData["searchItems"] + ' ' + "(ctrl+q)");
    jQuery("label[for='lblSubcription'").html(labelData["Renewal"]);
    jQuery("label[for='lblLocationwiseProfitAnalysis'").html(labelData["LocwiseProfitAnlys"]);
    jQuery("label[for='lblItemCategoryProfitAnalysis'").html(labelData["ItemCatgPrftAnlys"]);
    jQuery("label[for='lblSCPopPwd'").html(labelData["pSettings"]);
    jQuery("label[for='lblLanguage'").html(labelData["lang"]);
    jQuery("label[for='lblDateFormat'").html(labelData["format"]);
    jQuery("label[for='lblPopOk'").html(labelData["ok"]);
    jQuery("label[for='lblPopClose'").html(labelData["cancel"]);
    $('#txtFindGo').attr('placeholder', labelData["searchHere"]);
    jQuery("label[for='lblSettings'").html(labelData["settings"]);
    jQuery("label[for='lblThemes'").html(labelData["themes"]);
    jQuery("label[for='lblTheme1'").html(labelData["theme1"]);
    jQuery("label[for='lblTheme2'").html(labelData["theme2"]);
    jQuery("label[for='lblTheme3'").html(labelData["theme3"]);
    jQuery("label[for='lblTheme4'").html(labelData["theme4"]);
    jQuery("label[for='lblChangePassword'").html(labelData["changePassword"]);
    jQuery("label[for='lblTill'").html(labelData["till"]);
    jQuery("label[for='lblCreate'").html(labelData["create"]);
    jQuery("label[for='lblSave'").html(labelData["save"]);
    jQuery("label[for='lblEdit'").html(labelData["edit"]);
    jQuery("label[for='lblDelete'").html(labelData["delete"]);
    jQuery("label[for='lblCancel'").html(labelData["cancel"]);
    jQuery("label[for='lblStatus'").html(labelData["status"]);
    jQuery("label[for='lblToggleDropdown'").html(labelData["toggleDropdown"]);
    jQuery("label[for='lblActive'").html(labelData["active"]);
    jQuery("label[for='lblInactive'").html(labelData["inactive"]);
    jQuery("label[for='lblFilter'").html(labelData["filter"]);
    jQuery("label[for='lblToggleDropdown'").html(labelData["toggleDropdown"]);
    jQuery("label[for='lblAll'").html(labelData["all"]);
    jQuery("label[for='lblToday'").html(labelData["toDay"]);
    jQuery("label[for='lblYesterDay'").html(labelData["yesterDay"]);
    jQuery("label[for='lblLastSevenDays'").html(labelData["lastSevenDays"]);
    jQuery("label[for='lblCurrentMonth'").html(labelData["currentMonth"]);
    jQuery("label[for='lblToggleActive'").html(labelData["active"]);
    jQuery("label[for='lblToggleInactive'").html(labelData["inactive"]);
    jQuery("label[for='lblConfigurators'").html(labelData["configurators"]); 
    jQuery("label[for='lblCustLDGR'").html(labelData["PosCustLdger"]);
    jQuery("label[for='lblSubConfigurators'").html(labelData["POSEwalletConSts"]);
    jQuery("label[for='lblInvoiceSummary'").html(labelData["category"]);
    jQuery("label[for='lblPrintCustomize'").html(labelData["printCustamize"]);
    jQuery("label[for='lblBarcodeGenerator'").html(labelData["barCodeGenerator"]);
    jQuery("label[for='lblMasters'").html(labelData["masters"]);
    jQuery("label[for='lblTillType'").html(labelData["tillType"]);
    jQuery("label[for='lblTill'").html(labelData["till"]);
    jQuery("label[for='lblTillPermissions'").html(labelData["tillPermissions"]);
    jQuery("label[for='lblCustomer'").html(labelData["customer"]);
    jQuery("label[for='lblTenderType'").html(labelData["tenderType"]);
    jQuery("label[for='lblTransactions'").html(labelData["transactions"]);
    jQuery("label[for='lblInvoce'").html(labelData["invoice"]);
    jQuery("label[for='lblSalesReturn'").html(labelData["salesReturn"]);
    jQuery("label[for='lblPosting'").html(labelData["posting"]);
    jQuery("label[for='lblTillOpening'").html(labelData["tillOpening"]);
    jQuery("label[for='lblTillClosing'").html(labelData["tillClosing"]);
    jQuery("label[for='lblAccountPosting'").html(labelData["accountPosting"]);
    jQuery("label[for='lblReports'").html(labelData["reports"]);
    jQuery("label[for='lblInvoiceListing'").html(labelData["invoiceListing"]);
    jQuery("label[for='lblTillInvoiceListing'").html(labelData["tillInvoiceListing"]);
    jQuery("label[for='lblTenderTypeWiseCollectionDailySummary'").html(labelData["tenderTypeWiseCollectionDailySummary"]);
    jQuery("label[for='lblSalesReturns'").html(labelData["salesReturns"]);
    jQuery("label[for='lblCode'").html(labelData["code"]);
    jQuery("label[for='lblName'").html(labelData["name"]);
    jQuery("label[for='lblType'").html(labelData["type"]);
    jQuery("label[for='lblProcessStatus'").html(labelData["processStatus"]);
    jQuery("label[for='lblPrimeInformation'").html(labelData["primeInformation"]);
    jQuery("label[for='lblOrganization'").html(labelData["subOrganization"]);
    jQuery("label[for='lblDetCode'").html(labelData["code"]);
    jQuery("label[for='lblDetName'").html(labelData["name"]);
    jQuery("label[for='lblDetType'").html(labelData["type"]);
    //jQuery("label[for='lblPhone'").html(labelData["phone"]);
    jQuery("label[for='lblDescription'").html(labelData["description"]);
    jQuery("label[for='lblSCPopPwd'").html(labelData["pSettings"]);
    jQuery("label[for='lblDtFormat'").html(labelData["format"]);
    jQuery("label[for='lblOk'").html(labelData["ok"]);
    jQuery("label[for='lblClose'").html(labelData["closed"]);
    jQuery("label[for='lblLanguage'").html(labelData["lang"]);
    jQuery("label[for='lblLoyaltyPoints'").html(labelData["LoyaltyPoints"]);
    jQuery("label[for='lblDispatch'").html(labelData["Dispatch"]);
    $('#lblNavigationUrl').text(labelData["pos"] + "  >>  " + labelData["transactions"] + "  >>  " + labelData["receipt"]);
    $('#lblLastUpdate').text(labelData["lastUpdated"]);
    $('#lblLastUpdate').css("display", "none");
    jQuery("label[for='lblAnalyticalReports'").html(labelData["AnalyticalReports"]);
    jQuery("label[for='lblItemCategorySalesAnalysis'").html(labelData["ItemCategorySalesAnalysis"]);
    jQuery("label[for='lblWeekDaysRevenue'").html(labelData["WeekdaysRevenue"]);
    jQuery("label[for='lblBucketAnalysis'").html(labelData["BucketAnalysis"]);
    jQuery("label[for='lblLocationwiseAverageInvoiceValue'").html(labelData["LocationwiseAverageInvoiceValue"]);
    jQuery("label[for='lblCurrentInventory'").html(labelData["CurrentInventory"]);
    jQuery("label[for='lblReceipt'").html(labelData["receipt"]);
    jQuery("label[for='lblVATReturns'").html(labelData["VATReturns"]);
    jQuery("label[for='lblItem'").html(labelData["item"]);
    jQuery("label[for='lblDnCode'").html(labelData["documentNo"]);
    jQuery("label[for='lblDDateName'").html(labelData["documentDate"]);
    jQuery("label[for='lblAmount'").html(labelData["amount"]);
    $('#hdnNPermInv').val(labelData["alert1"]);
    $('#hdnNAsnCounterTran').val(labelData["alert2"]);
    $('#hdnInvalData').val(labelData["alert3"]);
    $('#hdnContactAdmin').val(labelData["alert4"]);
    $('#hdnRecSaveSucc').val(labelData["alert8"]);
    $('#hdnRecActSucc').val(labelData["alert6"]);
    $('#hdnRecInactSucc').val(labelData["alert7"]);
    $('#hdnCodeAlreadyExist').val(labelData["alert9"]);
    $('#hdnRecDelSucc').val(labelData["alert5"]);
    $('#hdnHaveRefRec').val(labelData["reference"]);
    $('#hdnEntrCode').val(labelData["alert10"]);
    $('#hdnEntrName').val(labelData["alert11"]);
    $('#hdnSelType').val(labelData["alert12"]);
    //$('#hdnSelPhoneNo').val(labelData["plzFill"] + labelData["phone"]);
    $('#hdnChPwdSaveSucc').val(labelData["alert8"]);
    $('#hdnThameChange').val(labelData["alert13"]);
    $('#hdnNew').val(labelData["new1"]);
    $('#hdnView').val(labelData["view"]);
    $('#hdnEdit').val(labelData["edit"]);
    $('#hdnDefault').val(labelData["default1"]);
    $('#hdnNotAssignCountrToTrans').val(labelData["permissionsAssisted"]);
    $('#hdnDeleteConfirm').val(labelData["wantToDelete"]);
    $('#hdnSelect').val(labelData["select"]);
    jQuery("label[for='lblLinesItem'").html(labelData["linesItem"]);
    jQuery("label[for='lblItem'").html(labelData["item"]);
    jQuery("label[for='lblUOM'").html(labelData["uom"]);
    jQuery("label[for='lblRefQty'").html(labelData["refQty"]);
    jQuery("label[for='lblPrice'").html(labelData["price"]);
    jQuery("label[for='lblDisc'").html(labelData["disc"]);
    jQuery("label[for='lblQty'").html(labelData["qty"]);
    jQuery("label[for='lblTotal'").html(labelData["total"]);
    $('#hdnImageSave').val(labelData["ImageSave"]);
    $('#hdnSelImage').val(labelData["SelUserImage"]);

    jQuery("label[for='lblUpload'").html(labelData["upload"]);

    jQuery("label[for='lblCustLDGR'").html(labelData["PosCustLdger"]);

}