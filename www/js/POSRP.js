﻿var graphData;
var graphDataSRP;
var graphDataTAX;
function VD2() {
    window.localStorage.setItem('ctCheck', 1);
    var vd_ = window.localStorage.getItem("UUID");
    //var vdSplit_ = window.location.pathname.split('/');
    //if (vdSplit_.length > 2) {
    //    vd_ = vdSplit_[1];
    //}
    return vd_;
}
$('#lblreportFromDate').hide();
$('#lblreportToDate').hide();
$('#lblreportSearchBy').hide();
function GetMainData() {
    theamLoad();
    window.localStorage.setItem('ctCheck', 1);
    var HashVal = (window.localStorage.getItem("hashValues"));
    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');

        getResources2(HashValues[7], "POSReport");
        $('#hdnDateFromate').val(HashValues[2]);
    }
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < url.length; i++) {
        var urlparam = url[i].split('=');
        if (urlparam.length > 1) {
            var pagecode = urlparam[1];
        }
    }
    $("#FromDate").DatePicker({
        format: $('#hdnDateFromate').val(),
        changeMonth: 'true',
        changeYear: 'true',
        onSelect: function (dateText) {
            var vardatesplit = $('#FromDate').val().split('/');
            if (pagecode == "POSTVRR") {
                AddMonths(vardatesplit[0], vardatesplit[1], vardatesplit[2]);
            }
        }
    });
    $("#ToDate").DatePicker({
        format: $('#hdnDateFromate').val(),
        changeMonth: 'true',
        changeYear: 'true',

    });
    GetLoginUserName();




    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');
        $('#hdnDateFromate').val(HashValues[2])
        if (HashValues[7] == 'ar-SA') {
            
            $('html,body').append('<link rel="stylesheet" href="Styles/css/bootstrap-rtl.css" type="text/css" />');
            $('html,body').append('<link rel="stylesheet" href="Styles/css/bootstrap-flipped.css" type="text/css" />');
            $('head').append('<script src="scripts/bootstrap-dialog-SA.min.js"></script>');
        }
    }
    default_Data();
        
    var CurDate = new Date();
    var DateWithFormate = CurDate.getFullYear() + '-' + ((CurDate.getMonth() + 1) < 10 ? '0' : '') + (CurDate.getMonth() + 1) + '-' + (CurDate.getDate() < 10 ? '0' : '') + CurDate.getDate();
    var currMonth = ((CurDate.getMonth() + 1) < 10 ? '0' : '') + (CurDate.getMonth() + 1);
    if (pagecode == "POSTVRR") {
        getperiod(currMonth);
    }
    else {
        ConvertDateWithFormat($('#hdnDateFromate').val(), DateWithFormate, "FromDate");
        ConvertDateWithFormat($('#hdnDateFromate').val(), DateWithFormate, "ToDate");
    }
   
}
   
if (window.localStorage.getItem("Theam").split('-')[3] == '1.css') {
    $('#IMG1').attr("src", "images/mmlogo-inner2.png");
} else {
    $('#IMG1').attr("src", "images/mmlogo-inner1.png");
}
$('#ddPeriod').change(function () {
    var pvalue = $('#ddPeriod').val();
    var CurDate = new Date();
    if (pvalue == '1') {
        DateWithFormate1 = CurDate.getFullYear() + '-01-01';
        DateWithFormate2 = CurDate.getFullYear() + '-03-31';
        ConvertDateWithFormat($('#hdnDateFromate').val(), DateWithFormate1, "FromDate");
        ConvertDateWithFormat($('#hdnDateFromate').val(), DateWithFormate2, "ToDate");
    }
    else if (pvalue == '2') {
        DateWithFormate1 = CurDate.getFullYear() + '-04-01';
        DateWithFormate2 = CurDate.getFullYear() + '-06-30';
        ConvertDateWithFormat($('#hdnDateFromate').val(), DateWithFormate1, "FromDate");
        ConvertDateWithFormat($('#hdnDateFromate').val(), DateWithFormate2, "ToDate");
    }
    else if (pvalue == '3') {
        DateWithFormate1 = CurDate.getFullYear() + '-07-01';
        DateWithFormate2 = CurDate.getFullYear() + '-09-30';
        ConvertDateWithFormat($('#hdnDateFromate').val(), DateWithFormate1, "FromDate");
        ConvertDateWithFormat($('#hdnDateFromate').val(), DateWithFormate2, "ToDate");
    }
    else if (pvalue == '4') {
        DateWithFormate1 = CurDate.getFullYear() + '-10-01';
        DateWithFormate2 = CurDate.getFullYear() + '-12-31';
        ConvertDateWithFormat($('#hdnDateFromate').val(), DateWithFormate1, "FromDate");
        ConvertDateWithFormat($('#hdnDateFromate').val(), DateWithFormate2, "ToDate");
    }
});
function GetSubOrgData() {
    var OBSCode = (window.localStorage.getItem("hashValues"));
    if (OBSCode != null && OBSCode != '') {
        var OBS = OBSCode.split('~');
        var ss = OBS[0];
        var vd_ = window.localStorage.getItem("UUID");
        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.GetSubOrgData,
            data: { OBSCode: ss, VD: vd_ },
            success: function (resp) {
                var OrgData = resp;
                if (OrgData[0].OrgCode.split('/').length > 2) {
                    $('#txtSubOrganization').val(OrgData[0].OrgCode.split('/')[0] + '/' + OrgData[0].OrgCode.split('/')[1]);
                }
                else {

                    $('#txtSubOrganization').val(OrgData[0].OrgCode.split('/')[1]);
                }
            },
            error: function (e) {
            }
        });
    }
}
function getperiod(currMonth) {
    var CurDate = new Date();
    var DateWithFormate1 = "";
    var DateWithFormate2 = "";
    if (currMonth == '01' || currMonth == '02' || currMonth == '03') {
        $('#ddPeriod').val('1');
        DateWithFormate1 = CurDate.getFullYear() + '-01-01';
        DateWithFormate2 = CurDate.getFullYear() + '-03-31';
        ConvertDateWithFormat($('#hdnDateFromate').val(), DateWithFormate1, "FromDate");
        ConvertDateWithFormat($('#hdnDateFromate').val(), DateWithFormate2, "ToDate");
    }
    else if (currMonth == '04' || currMonth == '05' || currMonth == '06') {
        $('#ddPeriod').val('2');
        DateWithFormate1 = CurDate.getFullYear() + '-04-01';
        DateWithFormate2 = CurDate.getFullYear() + '-06-30';
        ConvertDateWithFormat($('#hdnDateFromate').val(), DateWithFormate1, "FromDate");
        ConvertDateWithFormat($('#hdnDateFromate').val(), DateWithFormate2, "ToDate");
    }
    else if (currMonth == '07' || currMonth == '08' || currMonth == '09') {
        $('#ddPeriod').val('3');
        DateWithFormate1 = CurDate.getFullYear() + '-07-01';
        DateWithFormate2 = CurDate.getFullYear() + '-09-30';
        ConvertDateWithFormat($('#hdnDateFromate').val(), DateWithFormate1, "FromDate");
        ConvertDateWithFormat($('#hdnDateFromate').val(), DateWithFormate2, "ToDate");
    }
    else if (currMonth == '10' || currMonth == '11' || currMonth == '12') {
        $('#ddPeriod').val('4');
        DateWithFormate1 = CurDate.getFullYear() + '-10-01';
        DateWithFormate2 = CurDate.getFullYear() + '-12-31';
        ConvertDateWithFormat($('#hdnDateFromate').val(), DateWithFormate1, "FromDate");
        ConvertDateWithFormat($('#hdnDateFromate').val(), DateWithFormate2, "ToDate");
    }
    
}

function AddMonths(WhichDay, WhichMonth, WhichYear) {
    var vaddDate = new Date(WhichYear, (WhichMonth - 1), WhichDay);
    vaddDate1 = new Date(vaddDate.setMonth((vaddDate.getMonth() + 3)));
    vaddDate2 = new Date(vaddDate1.setDate((vaddDate1.getDate() - 1)));
    var DateWF3 = vaddDate2.getFullYear() + '-' + ((vaddDate2.getMonth() + 1) < 10 ? '0' : '') + (vaddDate2.getMonth() + 1) + '-' + (vaddDate2.getDate() < 10 ? '0' : '') + vaddDate2.getDate();
    ConvertDateWithFormat($('#hdnDateFromate').val(), DateWF3, "ToDate")
}
function default_Data() {
    window.localStorage.setItem('ctCheck', 1);
    var Paging = ""; var SubOrgId = ''; var Orgcode = ''; var OBS = ''; var CashierID = window.localStorage.getItem("CashierID"); var Userid = '';
    var Lang = ''; var BaseCurr_Id = ''; var CURR_CODE = ''; var CompanyId = '';
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        CompanyId = hashSplit[0];
        SubOrgId = hashSplit[1];
        OBS = hashSplit[9];
        Orgcode = hashSplit[17];
        Userid = hashSplit[14];
        Lang = hashSplit[7];
        BaseCurr_Id = hashSplit[16];
        CURR_CODE = hashSplit[18];
        Paging = hashSplit[19];
    }
    $('#CompanyId').val(CompanyId);
    $('#hdnBranchId').val(SubOrgId);
    $('#hdnOBS').val(OBS);
    $('#hdnUserId').val(Userid);
   // $('#txtSubOrganization').val(Orgcode.split('/')[0].trim());
    GetSubOrgData();
    $('#hdnLanguage').val(Lang);
    $('#hdnPaging').val(Paging);
}
function GetLoginUserName() {
    window.localStorage.setItem('ctCheck', 1);
    var sess = CashierLite.Session.getInstance().get();
    if (sess != null) {
        if (sess.userName != "") {
            document.getElementById("lblName").innerHTML = sess.userName;
        }
        else {
            window.open('Login.html', '_self', 'location=no');
        }
    }
    else {
        window.open('Login.html', '_self', 'location=no');
    }
    theamLoad();
}
function theamLoad() {
    window.localStorage.setItem('ctCheck', 1);
    if (window.localStorage.getItem("Theam") != null)
        $('head').append('<link rel="stylesheet" href=' + window.localStorage.getItem("Theam") + ' type="text/css" />');
}

function getPOSPage(pagecode) {
   
    window.localStorage.setItem('ctCheck', 1);
    $('#lblreportFromDate').show();
    $('#lblreportToDate').show();
    $('#lblreportSearchBy').show();
    $('#lblreportSubOrganization').show();
    //alert(pagecode);

    //POSACPO
    if (pagecode == "POSACPO") {
        window.open('POSReport.html?PageId=POSACPO', '_self', 'location=no');
    } 
    if (pagecode == "POSINLS") {
        window.open('POSReport.html?PageId=POSINLS', '_self', 'location=no');
    } if (pagecode == "POSINLSM") {
        window.open('POSReport.html?PageId=POSINLSM', '_self', 'location=no');
    } if (pagecode == "POSTINL") {
        window.open('POSReport.html?PageId=POSTINL', '_self', 'location=no');
    } if (pagecode == "POSTTDS") {
        window.open('POSReport.html?PageId=POSTTDS', '_self', 'location=no');
    } if (pagecode == "POSSGR") {
        window.open('POSReport.html?PageId=POSSGR', '_self', 'location=no');
    } if (pagecode == "POSDEND") {
        window.open('POSReport.html?PageId=POSDEND', '_self', 'location=no');
    } if (pagecode == "POSSDSEN") {
        window.open('POSReport.html?PageId=POSSDSEN', '_self', 'location=no');
    } if (pagecode == "POSTVRR") {
        window.open('POSReport.html?PageId=POSTVRR', '_self', 'location=no');
    }
    if (pagecode == "POSCUSTLDGR") {
        window.open('POSReport.html?PageId=POSCUSTLDGR', '_self', 'location=no');
    }
    if (pagecode == "POSCINV") {
        window.open('POSReport.html?PageId=POSCINV', '_self', 'location=no');
    }
 if (pagecode == "POSCUSTLDGR") {
        window.open('POSReport.html?PageId=POSCUSTLDGR', '_self', 'location=no');
    }
    if (pagecode == "POSRPTICSA") {
        window.open('POSReport.html?PageId=POSRPTICSA', '_self', 'location=no');
        $('#lblreportFromDate').hide();
        $('#lblreportToDate').hide();
        $('#lblreportSearchBy').hide();
        $('#lblreportSubOrganization').show();
    }
    if (pagecode == "POSRPTBAR") {
        $('#lblreportFromDate').hide();
        $('#lblreportToDate').hide();
        $('#lblreportSubOrganization').show();
        $('#lblreportSearchBy').hide();
        window.open('POSReport.html?PageId=POSRPTBAR', '_self', 'location=no');
    } if (pagecode == "POSRPTWDRR") {
        $('#lblreportFromDate').hide();
        $('#lblreportToDate').hide();
        $('#lblreportSubOrganization').show();
        $('#lblreportSearchBy').hide();
        window.open('POSReport.html?PageId=POSRPTWDRR', '_self', 'location=no');
    }
    if (pagecode == "POSRPTLWATV") {
        $('#lblreportFromDate').hide();
        $('#lblreportToDate').hide();
        $('#lblreportSubOrganization').hide();
        $('#lblreportSearchBy').hide();
        window.open('POSReport.html?PageId=POSRPTLWATV', '_self', 'location=no');
    }
    if (pagecode == "POSRPTICPA") {
        $('#lblreportFromDate').hide();
        $('#lblreportToDate').hide();
        $('#lblreportSubOrganization').hide();
        $('#lblreportSearchBy').hide();
        window.open('POSReport.html?PageId=POSRPTICPA', '_self', 'location=no');
    }
    if (pagecode == "POSRPTLWPAS") {
        $('#lblreportFromDate').hide();
        $('#lblreportToDate').hide();
        $('#lblreportSubOrganization').hide();
        $('#lblreportSearchBy').hide();
        window.open('POSReport.html?PageId=POSRPTLWPAS', '_self', 'location=no');
    }

}
 
$('#btnAccountDetails').click(function () {
    window.localStorage.setItem('ctCheck', 1);
    loadG_MainData();
});



function loadG_MainData() {
    window.localStorage.setItem('ctCheck', 1);
    var sess = CashierLite.Session.getInstance().get();
    if (sess != null) {
        if (sess.userName != "") {
            document.getElementById("lblName").innerHTML = sess.userName;
        }
        else {
            window.open('Login.html', '_self', 'location=no');
        }
    }
    else {
        window.open('Login.html', '_self', 'location=no');
    }

    window.localStorage.setItem('RPfrmDate', GetDateWithYYYYMMDD($('#hdnDateFromate').val(), $('#FromDate').val()))
    window.localStorage.setItem('RPtoDate', GetDateWithYYYYMMDD($('#hdnDateFromate').val(), $('#ToDate').val()))
    window.localStorage.setItem('RPrefNo', $('#txtCode').val())
    if ($('#hdnpageCode').val() == "POSACPO") {

        var dateComaparison_ = checkFromTodates();
        if (dateComaparison_ == true) {
            window.localStorage.setItem('RPPAGECODE', 'POSACPO')
            window.open('SalesReceipt.html', '_blank');
        }
        else
            dateValidationAlert();

    }
    if ($('#hdnpageCode').val() == "POSINLS") {
        var dateComaparison_ = checkFromTodates();
        if (dateComaparison_ == true) {
            window.localStorage.setItem('RPPAGECODE', 'POSINLS')
            window.open('InvoiceListingReport.html', '_blank');
        }
        else
            dateValidationAlert();

    }
  
 if ($('#hdnpageCode').val() == "POSINLSM") {
        var dateComaparison_ = checkFromTodates();
     if (dateComaparison_ == true) { 
            window.localStorage.setItem('RPPAGECODE', 'POSINLSM');
            window.open('InvoiceSummaryReport.html', '_blank');
        }
        else
            dateValidationAlert();

    }
if ($('#hdnpageCode').val() == "POSCUSTLDGR") {
     var dateComaparison_ = checkFromTodates();
     if (dateComaparison_ == true) {
         window.localStorage.setItem('RPPAGECODE', 'POSCUSTLDGR');
         window.open('CustomerLedger.html', '_blank');
     }
     else
         dateValidationAlert();

 }
    if ($('#hdnpageCode').val() == "POSTINL") {

        var dateComaparison_ = checkFromTodates();
        if (dateComaparison_ == true) {
            window.localStorage.setItem('RPPAGECODE', 'POSTINL')
            window.open('TillInvoiceListingReport.html', '_blank');
        }
        else
            dateValidationAlert();
    }
    if ($('#hdnpageCode').val() == "POSSGR") {
        window.localStorage.setItem('RPPAGECODE', 'POSSGR')
        window.open('SalesGrowthReport.html', '_blank');
    }
    if ($('#hdnpageCode').val() == "POSSDSEN") {

        var dateComaparison_ = checkFromTodates();
        if (dateComaparison_ == true) {
            window.localStorage.setItem('RPPAGECODE', 'POSSDSEN')
            window.open('SalesReturnReport.html', '_blank');
        }
        else
            dateValidationAlert();


    }
    if ($('#hdnpageCode').val() == "POSCINV") {
        window.localStorage.setItem('RPPAGECODE', 'POSCINV')
        window.open('POSCurrentInventory.html', '_blank');
    }
    if ($('#hdnpageCode').val() == "POSDEND") {
        window.localStorage.setItem('RPPAGECODE', 'POSDEND')
        window.open('DayEndReport.html', '_blank');
    }
    if ($('#hdnpageCode').val() == "POSTTDS") {

        var dateComaparison_ = checkFromTodates();
        if (dateComaparison_ == true) {
            window.localStorage.setItem('RPPAGECODE', 'POSTTDS')
            window.open('DailyCollectionsSummaryByTenderTypeReport.html', '_blank');
        }
        else
            dateValidationAlert();


    }

    if ($('#hdnpageCode').val() == "POSRPTICSA") {
        window.localStorage.setItem('RPPAGECODE', 'POSRPTICSA')
        window.open('ItemCategorySalesAnalysis.html', '_blank');
    }
    if ($('#hdnpageCode').val() == "POSRPTBAR") {
        window.localStorage.setItem('RPPAGECODE', 'POSRPTBAR')
        window.open('BulkAnalysisReport.html', '_blank');
    }
    if ($('#hdnpageCode').val() == "POSRPTWDRR") {
        window.localStorage.setItem('RPPAGECODE', 'POSRPTWDRR')
        window.open('POSWeekDaysRevenueRP.html', '_blank');
    }
    if ($('#hdnpageCode').val() == "POSRPTLWATV") {
        window.localStorage.setItem('RPPAGECODE', 'POSRPTLWATV')
        window.open('POSLocationwiseAvgTransactionValueRPT.html', '_blank');
    }

    if ($('#hdnpageCode').val() == "POSRPTICPA") {
        window.localStorage.setItem('RPPAGECODE', 'POSRPTICPA')
        window.open('POSItemCategoryProfitAnalysis.html', '_blank');
    }
    if ($('#hdnpageCode').val() == "POSRPTLWPAS") {
        window.localStorage.setItem('RPPAGECODE', 'POSRPTLWPAS')
        window.open('POSLocationwiseProfitAnalysis.html', '_blank');
    }

    if ($('#hdnpageCode').val() == "POSTVRR") {

        var dateComaparison_ = checkFromTodates();
        if (dateComaparison_ == true) {
            window.localStorage.setItem('RPPAGECODE', 'POSTVRR')
            window.open('TaxReturns.html', '_blank');
        }
        else
            dateValidationAlert();

    }




}
function floorFigure(figure, decimals) {
    window.localStorage.setItem('ctCheck', 1);
    if (!decimals) decimals = 2;
    var d = Math.pow(10, decimals);
    return (parseInt(figure * d) / d).toFixed(decimals);
};
function checkFromTodates() {
    window.localStorage.setItem('ctCheck', 1);
    var fromDate = window.localStorage.getItem('RPfrmDate');
    var toDate = window.localStorage.getItem('RPtoDate');
    if (Date.parse(toDate) < Date.parse(fromDate)) {
        return false;
    }
    else
        return true;

}
function dateValidationAlert() {
    window.localStorage.setItem('ctCheck', 1); 
    navigator.notification.alert("To Date should not be less than From Date.", "", "neo ecr", "Ok");
}
function arrangementInvoiceListingRPBind(resp) {
    window.localStorage.setItem('ctCheck', 1);

    graphData = resp;
    var result = resp;
    var Primaryid = "";
    $('#tbldiv thead tr').remove();
    $('#tbldiv tbody tr').remove();
    if (result.length != 0) {
        $('#tbldiv thead').append("<tr><th colspan='6' style='text-align:right'>" + $('#hdnInvNo').val() + ":</th><th style='text-align:right'>" + parseFloat(result[0].C4).toFixed(2) + "</th></tr>");
        //$('#tbldiv').append("<thead><tr><th style='width:10px' ></th><th>Invoice No.</th><th>Invoice Date</th><th>Customer</th><th>Till</th><th>Cashier</th><th>Base Total</th><th>Tax Total</th><th>Net Total</th></tr></thead>");
        for (var i = 0; i < result[0].obTill.length; i++) {
            $('#tbldiv thead').append('<tr><th colspan="6" style="text-align:right">' + result[0].obTill[i].Disc + '</th><th style="text-align:right">' + parseFloat(result[0].obTill[i].Total).toFixed(2) + '</th></tr>');
            //<td>' + result[i].oblist[j].C4 + '</td>
        }
    }
    else {
        $('#tbldiv thead').append("<tr><th colspan='6' style='text-align:right'>" + $('#hdnInvNo').val() + ":</th><th style='text-align:right'>0.00</th></tr>");
        //for (var i = 0; i < result[0].obTill.length; i++) {
        $('#tbldiv thead').append('<tr><th colspan="6" style="text-align:right">Till</th><th style="text-align:right">0.00</th></tr>');
        //<td>' + result[i].oblist[j].C4 + '</td>
        // }
    }
    $('#tbldiv thead').append("<tr><th>" + $('#hdnInvoiceNo').val() + "</th><th>" + $('#hdnInvoiceDt').val() + '&nbsp' + "</th><th>" + $('#hdnTill').val() + "</th><th>" + $('#hdnCashier').val() + "</th><th>" + $('#hdnBaseTot').val() + "</th><th>" + $('#hdnTaxTot').val() + "</th><th>" + $('#hdnNetTot').val() + "</th></tr>");

    var cc_ = '';
    if (HashValues[7] == 'ar-SA') {
        for (var i = 0; i < result.length; i++) {

            $('#hdnInvoiceNo').val(result[i].C0);
            cc_ = cc_ + '<tr id="dt_Parrent' + i + '"><td  onclick="treeclick(this);">&nbsp' + $('#hdnInvoiceNo').val() + '</td><td>' + result[i].C6 + '&nbsp' + '</td><td>' + result[i].C9 + '</td><td>' + result[i].C10 + '&nbsp' + '</td><td style="text-align:left">' + parseFloat(result[i].C11).toFixed(2) + '&nbsp' + '</td><td style="text-align:left">' + result[i].C14 + '&nbsp' + '</td><td style="text-align:left">' + '&nbsp' + result[i].C12 + '</td><td><input type="button" class="btn btn-primary"  onclick = "printInvoiceRP(\'' + result[i].C0 + '\')"value=' + $('#hdnToolPrint').val() + ' /> </td></tr>';

            cc_ = cc_ + '<tr style="display:none" id="dt_Parrent' + i + '_0" data-tt-parent="childPaging"><td colspan="11" ><table id="tblChild' + i + '" data-tt-parent="root' + i + '" class="table table-bordered" >';
            cc_ = cc_ + "<tr><th>" + $('#hdnItemCode').val() + "</th><th>" + $('#hdnItemName').val() + "</th><th>" + $('#hdnUOM').val() + "</th><th>" + $('#hdnQty').val() + "</th><th>" + $('#hdnPrice').val() + "</th><th>" + $('#hdnDisc').val() + "</th><th>" + $('#hdnAmount').val() + "</th></tr>";

            for (var j = 0; j < result[i].objInvList.length; j++) {
                if (resp[i].C13 == result[i].objInvList[j].InvoiceId) {
                    cc_ = cc_ + '<tr><td style="width:12%;text-align:right;">&nbsp' + result[i].objInvList[j].ItemCode + '</td><td style="width:33%;text-align:right;">' + result[i].objInvList[j].ItemName.split('/')[1] + ' / ' + result[i].objInvList[j].ItemName.split('/')[0] + '</td><td style="width:6%;text-align:right;">' + result[i].objInvList[j].UOM + '</td><td style="text-align:left;width:13%">' + result[i].objInvList[j].QTY + '&nbsp' + '</td><td style="text-align:left;width:13%">' + '&nbsp' + result[i].objInvList[j].Rate + '&nbsp' + '</td><td style="text-align:left;width:13%">' + result[i].objInvList[j].Disc + '&nbsp' + '</td><td style="text-align:left;width:22%">' + parseFloat(result[i].objInvList[j].Total).toFixed(2) + '&nbsp' + '</td></tr>';
                }
            }


            cc_ = cc_ + '</table></td></tr>';

        }
    }
    else { 
        for (var i = 0; i < result.length; i++) {

            $('#hdnInvoiceNo').val(result[i].C0);
            cc_ = cc_ + '<tr id="dt_Parrent' + i + '"><td  onclick="treeclick(this);">&nbsp' + $('#hdnInvoiceNo').val() + '</td><td>' + result[i].C6 + '&nbsp' + '</td><td>' + result[i].C9 + '</td><td>' + result[i].C10 + '&nbsp' + '</td><td style="text-align:right">' + parseFloat(result[i].C11).toFixed(2) + '&nbsp' + '</td><td style="text-align:right">' + result[i].C14 + '&nbsp' + '</td><td style="text-align:right">' + '&nbsp' + result[i].C12 + '</td><td><input type="button" class="btn btn-primary"  onclick = "printInvoiceRP(\'' + result[i].C0 + '\')"value=' + $('#hdnToolPrint').val() + ' /> </td></tr>';

            cc_ = cc_ + '<tr style="display:none" id="dt_Parrent' + i + '_0" data-tt-parent="childPaging"><td colspan="11" ><table id="tblChild' + i + '" data-tt-parent="root' + i + '" class="table table-bordered" >';
            cc_ = cc_ + "<tr><th>" + $('#hdnItemCode').val() + "</th><th>" + $('#hdnItemName').val() + "</th><th>" + $('#hdnUOM').val() + "</th><th>" + $('#hdnQty').val() + "</th><th>" + $('#hdnPrice').val() + "</th><th>" + $('#hdnDisc').val() + "</th><th>" + $('#hdnAmount').val() + "</th></tr>";

            for (var j = 0; j < result[i].objInvList.length; j++) {
                if (resp[i].C13 == result[i].objInvList[j].InvoiceId) {
                    cc_ = cc_ + '<tr><td style="width:12%">&nbsp' + result[i].objInvList[j].ItemCode + '</td><td style="width:33%;text-align:left;">' + result[i].objInvList[j].ItemName + '</td><td style="width:6%">' + result[i].objInvList[j].UOM + '</td><td style="text-align:right;width:13%">' + result[i].objInvList[j].QTY + '&nbsp' + '</td><td style="text-align:right;width:13%">' + '&nbsp' + result[i].objInvList[j].Rate + '&nbsp' + '</td><td style="text-align:right;width:13%">' + result[i].objInvList[j].Disc + '&nbsp' + '</td><td style="text-align:right;width:22%">' + parseFloat(result[i].objInvList[j].Total).toFixed(2) + '&nbsp' + '</td></tr>';
                }
            }


            cc_ = cc_ + '</table></td></tr>';

        }
    }
    $("#tbldiv tbody").append(cc_);
}


function InvMail() {
    $('#txtBccMailId').val('');
    $('#txtBody').val('');
    window.localStorage.setItem('ctCheck', 1);
    $('#ModalINVMail').modal('show');
    $('#txtMailId').val('');
    $('#txtMailId').focus();
    $('#FileFormat').attr("readonly", "true");
}
function SendINVMail(MailId, BccMailId, Format, Body) {
    window.localStorage.setItem('ctCheck', 1);
    if ($('#txtMailId').val() == "") {
        navigator.notification.alert($('#hdnEnterMail').val(), "", "neo ecr", "Ok");
    }
    else {
        var options = { dimBackground: true };
        SpinnerPlugin.activityStart("Please wait...", options);
        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.SendMailArabic,
            data: "language=" + HashValues[7] + "&PageId=" + window.localStorage.getItem('RPPAGECODE') + "&subOrg=" + HashValues[1] + "&frmDate=" + window.localStorage.getItem('RPfrmDate') + "&toDate=" + window.localStorage.getItem('RPtoDate') + ' 23:59:59' + "&DateFormate=" + $('#hdnDateFromate').val() + "&VD=" + VD2() + "&MailId=" + MailId + "&Format=" + Format + "&BCMailId=" + BccMailId + "&Body=" + Body + "",
            success: function (resp) {
                SpinnerPlugin.activityStop();
                if (resp == '1000') {
                    navigator.notification.alert($('#hdnMailSuccess').val(), "", "neo ecr", "Ok");
                    $('#ModalINVMail').modal('hide');
                }
                else {
                    navigator.notification.alert($('#hdnMailnotSuccess').val(), "", "neo ecr", "Ok");
                    $('#ModalINVMail').modal('hide');
                }
            },
            error: function (e) {
                SpinnerPlugin.activityStop();
                navigator.notification.alert($('#hdnInvalidData').val(), "", "neo ecr", "Ok");
                $('#ModalINVMail').modal('hide');
            }
        });
    }
}

function printInternalUSBRP(invoiceId) {
    window.localStorage.setItem('ctCheck', 1);
    var PrinterName_ = "InnerPrinter";
    if (window.localStorage.getItem("PrinterName") != null) {
        PrinterName_ = window.localStorage.getItem("PrinterName");
    }
    //T2a
    // BTPrinter.connect(function (data) {
    // }, function (err) {
    // }, PrinterName_)
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.printUSB,
        data: "InvoiceId=" + invoiceId + "&VD=" + VD1(),
        success: function (resp) {
            try {
                var dataMain = resp; 
                var qrData = "";
                qrData = dataMain.split('Š')[1];
                dataMain = dataMain.split('Š')[0];
                //T2s
                try {
                    // if (PrinterName_ != "InnerPrinter") {
                    //     BTPrinter.printText(function (data) {
                    //         //alert('Printed Data')
                    //     }, function (err) {
                    //         //alert(err)
                    //     }, dataMain);
                    //     //printQrCodeRP(invoiceId);
                    //     dataTT(qrData);
                    //     BTPrinter.printPOSCommand(function (data) {
                    //     }, function (err) {
                    //     }, "0C");
                    //     BTPrinter.printPOSCommand(function (data) {
                    //     }, function (err) {
                    //     }, "0A");
                    //     BTPrinter.printPOSCommand(function () { }, function () { }, "1D");//FEED PAPER AND CUT
                    // } 
                    // if (PrinterName_ == "InnerPrinter") {
                        navigator.sunmiInnerPrinter.printerInit();
                        navigator.sunmiInnerPrinter.printerStatusStartListener();
                        navigator.sunmiInnerPrinter.printOriginalText(dataMain);
                        // navigator.sunmiInnerPrinter.printerStatusStopListener();
                        navigator.sunmiInnerPrinter.setAlignment(1);
                        navigator.sunmiInnerPrinter.printQRCode(qrData,10,10);
                        window.localStorage.setItem('PrintCut','1');
                        navigator.sunmiInnerPrinter.hasPrinter();
                        navigator.sunmiInnerPrinter.printerStatusStopListener();
                        //printQrCodeRP(invoiceId);
                    // }

                    // BTPrinter.disconnect(function (data) {
                    // }, function (err) {
                    // }, PrinterName_);


                } catch (a) {

                }

            } catch (a) {
                alert(a);
            }
        }, error: function (e) {
            alert(e);
        }
    });
} 
PaperCutCheck();
function PaperCutCheck() {
        try
        {
            if(window.localStorage.getItem('PrintCut')=='1')
            {
                navigator.sunmiInnerPrinter.printOriginalText("                          "); 
                navigator.sunmiInnerPrinter.printOriginalText("                      "); 
                navigator.sunmiInnerPrinter.printOriginalText("                      ");  
                window.localStorage.setItem('PrintCut','0');
                navigator.sunmiInnerPrinter.sendRAWData("HVZCAA=="); 
            }
        }
        catch(a)
        {

        }
        setTimeout('PaperCutCheck()', 1000);
}
function VD1() {
    window.localStorage.setItem('ctCheck', 1);
    var vd_ = window.localStorage.getItem("UUID");;
    return vd_;
}
function BufferToBase64(buf) {
    var binstr = Array.prototype.map.call(buf, function (ch) {
        return String.fromCharCode(ch);
    }).join('');
    return btoa(binstr);
}
 
function dataTT(invData) {
    BTPrinter.printBase64(function (data) {
    }, function (err) {
    }, invData, '1');//base64 string, align 
}
function printQrCodeRP(invoiceId) {
    window.localStorage.setItem('ctCheck', 1);
    const justify_center = '\x1B\x61\x01';
    const justify_left = '\x1B\x61\x00';
    const qr_model = '\x32';          // 31 or 32
    const qr_size = '\x08';          // size
    const qr_eclevel = '\x33';          // error correction level (30, 31, 32, 33 - higher)
    const qr_data = invoiceId;
    const qr_pL = String.fromCharCode((qr_data.length + 3) % 256);
    const qr_pH = String.fromCharCode((qr_data.length + 3) / 256);

    BTPrinter.printText(data => {
        this.msg = '';
    }, err => {
        this.msg = '';
    }, justify_center +
    '\x1D\x28\x6B\x04\x00\x31\x41' + qr_model + '\x00' +        // Select the model
    '\x1D\x28\x6B\x03\x00\x31\x43' + qr_size +                  // Size of the model
    '\x1D\x28\x6B\x03\x00\x31\x45' + qr_eclevel +               // Set n for error correction
    '\x1D\x28\x6B' + qr_pL + qr_pH + '\x31\x50\x30' + qr_data + // Store data 
    '\x1D\x28\x6B\x03\x00\x31\x51\x30' +                        // Print
    '\n\n\n' +
        justify_left);
}
function printInvoiceRP(invoiceId) {
    window.localStorage.setItem('ctCheck', 1);
    printInternalUSBRP(invoiceId)
}
function treeclick(data) {
    window.localStorage.setItem('ctCheck', 1);
    var d_ = data.parentNode;
    if ($('#' + d_.id + '_0')[0].style.display != '')
        $('#' + d_.id + '_0')[0].style.display = '';
    else
        $('#' + d_.id + '_0')[0].style.display = 'none';
}
function VD() {
    window.localStorage.setItem('ctCheck', 1);
    var vd_ = window.localStorage.getItem("UUID");
    return vd_;
}

function PageInvoiceListingRPBind() {

    var HashVal = (window.localStorage.getItem("hashValues"));
    window.localStorage.setItem('ctCheck', 1);
    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');
        $('#hdnDateFromate').val(HashValues[2])
        if (HashValues[7] == 'ar-SA') {
            $('html,body').append('<link rel="stylesheet" href="Styles/css/bootstrap-rtl.css" type="text/css" />');
            $('html,body').append('<link rel="stylesheet" href="Styles/css/bootstrap-flipped.css" type="text/css" />');
            $('head').append('<script src="scripts/bootstrap-dialog-SA.min.js"></script>');
        }

    }

    theamLoad();

    $('#hdnPaging').val('1');
    var btnExp = document.getElementById("btnInvExp");
    btnExp.style.display = "block";
    if (window.localStorage.getItem('RPPAGECODE') == 'POSINLS') {
        var SubOrgId = '';
        var HashVal = (window.localStorage.getItem("hashValues"));
        var HashValues;
        if (HashVal != null && HashVal != '') {
            HashValues = HashVal.split('~');

            SubOrgId = HashValues[1];
            $('#hdnDateFromate').val(HashValues[2]);

        }

        var PageIndex = $('#hdnPaging').val();
        var PageCount = $('#txt_pageCount').val();

        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.GetReport_V01,
            data: "PageId=" + window.localStorage.getItem('RPPAGECODE') + "&subOrg=" + SubOrgId + "&frmDate=" + window.localStorage.getItem('RPfrmDate') + "&toDate=" + window.localStorage.getItem('RPtoDate') + ' 23:59:59' + "&refNo=" + window.localStorage.getItem('RPrefNo') + "&DateFormate=" + $('#hdnDateFromate').val() + "&VD=" + VD2() + "&PageIndex=" + PageIndex + "&PageCount=" + PageCount + "&language=" + HashValues[7] + "",
            success: function (resp) {

                graphData = resp;
                if (resp != '') {
                   //if (HashValues[7] == 'ar-SA') 
                   
                    // $('#lbl_invPageOf').val(resp[0].C15.split('of')[0]+'من'+resp[0].C15.split('of')[1]);
                   // else
                    // $('#lbl_invPageOf').val(resp[0].C15.split('of')[0]+'of'+resp[0].C15.split('of')[1]);
                      
                    jQuery("label[for='lbl_invPageOf'").html(resp[0].C15);
                    $('#hdnTotalPagesCount').val(resp[0].C5);
                }

                getResources5(HashValues[7], "POSInvoiceListingReport", resp, 1);
                $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());

                if ($('#hdnTotalPagesCount').val() != $('#hdnPaging').val()) {
                    $('#li_PRPrev i').removeAttr('style', 'color:#919598 !important');
                    $('#li_PRPrev').attr('onclick', 'nextPRInvl()');
                    $('#li_PRLast i').removeAttr('style', 'color:#919598 !important');
                    $('#li_PRLast').attr('onclick', 'nextPRLastInvl()');
                }
                else {
                    $('#li_PRPrev i').attr('style', 'color:#919598 !important');
                    $('#li_PRPrev').attr('onclick', 'return false');
                    $('#li_PRLast').attr('onclick', 'return false');
                    $('#li_PRLast i').attr('style', 'color:#919598 !important');
                }
            }

        });
    }

}
function PageInvoiceListingRPBind_OLD() {
    window.localStorage.setItem('ctCheck', 1);
    theamLoad();
    var btnExp = document.getElementById("btnInvExp");
    btnExp.style.display = "block";
    if (window.localStorage.getItem('RPPAGECODE') == 'POSINLS') {
        var SubOrgId = '';
        var HashVal = (window.localStorage.getItem("hashValues"));
        var HashValues;
        if (HashVal != null && HashVal != '') {
            HashValues = HashVal.split('~');

            SubOrgId = HashValues[1];
            $('#hdnDateFromate').val(HashValues[2]);
            $('#hdnPaging').val(HashValues[19]);
        }



        var grid = "";
        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.GetReport,
            data: "PageId=" + window.localStorage.getItem('RPPAGECODE') + "&subOrg=" + SubOrgId + "&frmDate=" + window.localStorage.getItem('RPfrmDate') + "&toDate=" + window.localStorage.getItem('RPtoDate') + ' 23:59:59' + "&refNo=" + window.localStorage.getItem('RPrefNo') + "&DateFormate=" + $('#hdnDateFromate').val() + "&VD=" + VD2(),
            success: function (resp) {

                graphData = resp;
                getResources5(HashValues[7], "POSInvoiceListingReport", resp, 1);
                $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());
                //   arrangementInvoiceListingRPBind(resp);


            }

        });
    }
}
function PageISalesReceiptRPBind() {
    window.localStorage.setItem('ctCheck', 1);
    var HashVal = (window.localStorage.getItem("hashValues"));

    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');
        $('#hdnDateFromate').val(HashValues[2])
        if (HashValues[7] == 'ar-SA') {
            $('html,body').append('<link rel="stylesheet" href="Styles/css/bootstrap-rtl.css" type="text/css" />');
            $('html,body').append('<link rel="stylesheet" href="Styles/css/bootstrap-flipped.css" type="text/css" />');
            $('head').append('<script src="scripts/bootstrap-dialog-SA.min.js"></script>');
        }

    }

    theamLoad();
    var btnExp = document.getElementById("btnExp");
    btnExp.style.display = "block";
    if (window.localStorage.getItem('RPPAGECODE') == 'POSACPO') {
        var SubOrgId = '';
        var HashVal = (window.localStorage.getItem("hashValues"));
        var HashValues;
        if (HashVal != null && HashVal != '') {
            HashValues = HashVal.split('~');

            SubOrgId = HashValues[1];
            $('#hdnDateFromate').val(HashValues[2]);
            $('#hdnPaging').val(HashValues[19]);
        }
         
        GetAccountDetails(window.localStorage.getItem('RPfrmDate'), window.localStorage.getItem('RPtoDate'), "", "");


    }
}

function loadInvoicebyPageIndexStatic() {
    window.localStorage.setItem('ctCheck', 1);
    var pageIndexT_ = 1;
    if ($('#txt_PRPPageIndex').val() != '') {
        if ($('#txt_PRPPageIndex').val() != '0') {
            pageIndexT_ = $('#txt_PRPPageIndex').val();
        }
    }
    $('#hdnPaging').val(pageIndexT_);
    var btnExp = document.getElementById("btnInvExp");
    btnExp.style.display = "block";
    if (window.localStorage.getItem('RPPAGECODE') == 'POSINLS') {
        var SubOrgId = '';
        var HashVal = (window.localStorage.getItem("hashValues"));
        var HashValues;
        if (HashVal != null && HashVal != '') {
            HashValues = HashVal.split('~');

            SubOrgId = HashValues[1];
            $('#hdnDateFromate').val(HashValues[2]);

        }

        var PageIndex = $('#hdnPaging').val();
        var PageCount = $('#txt_pageCount').val();

        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.GetReport_V01,
            data: "PageId=" + window.localStorage.getItem('RPPAGECODE') + "&subOrg=" + SubOrgId + "&frmDate=" + window.localStorage.getItem('RPfrmDate') + "&toDate=" + window.localStorage.getItem('RPtoDate') + ' 23:59:59' + "&refNo=" + window.localStorage.getItem('RPrefNo') + "&DateFormate=" + $('#hdnDateFromate').val() + "&VD=" + VD2() + "&PageIndex=" + PageIndex + "&PageCount=" + PageCount + "&language=" + HashValues[7] + "",
            success: function (resp) {
                graphData = resp;
                jQuery("label[for='lbl_invPageOf'").html(resp[0].C15);
                getResources5(HashValues[7], "POSInvoiceListingReport", resp, 1);
                $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());

            }

        });
    }
}
function loadInvoiceGridExtend() {
    window.localStorage.setItem('ctCheck', 1);
    var myTab = document.getElementById('tbldiv');
    for (i = 3; i < myTab.rows.length; i++) {
        var objCells = myTab.rows.item(i).cells;
        var id_ = myTab.rows.item(i).cells[0].parentNode.id + '_0';
        $('#' + id_)[0].style.display = '';
        i++;
    }
    var li_expand = document.getElementById("li_expand");
    li_expand.style.display = "none";
    var li_compress = document.getElementById("li_compress");
    li_compress.style.display = "inline-block";

}
function loadTillInvoiceGridExtend() {
    window.localStorage.setItem('ctCheck', 1);
    var myTab = document.getElementById('tbldiv');
    for (i = 1; i < myTab.rows.length; i++) {
        var objCells = myTab.rows.item(i).cells;
        var id_ = myTab.rows.item(i).cells[0].parentNode.id;
        $('#' + id_)[0].style.display = '';
        i++;
    }
    var li_expand = document.getElementById("li_expand");
    li_expand.style.display = "none";
    var li_compress = document.getElementById("li_compress");
    li_compress.style.display = "inline-block";

}

function loadInvoiceGridCompress() {
    window.localStorage.setItem('ctCheck', 1);
    var myTab = document.getElementById('tbldiv');
    for (i = 1; i < myTab.rows.length; i++) {
        var objCells = myTab.rows.item(i).cells;
        var id_ = myTab.rows.item(i).cells[0].parentNode.id + '_0';
        $('#' + id_)[0].style.display = 'none';
        i++;
    }
    var li_expand = document.getElementById("li_expand");
    li_expand.style.display = "inline-block";
    var li_compress = document.getElementById("li_compress");
    li_compress.style.display = "none";
}
function loadTillInvoiceGridCompress() {
    window.localStorage.setItem('ctCheck', 1);
    var myTab = document.getElementById('tbldiv');
    for (i = 1; i < myTab.rows.length; i++) {
        var objCells = myTab.rows.item(i).cells;
        var id_ = myTab.rows.item(i).cells[0].parentNode.id;
        $('#' + id_)[0].style.display = 'none';
        i++;
    }
    var li_expand = document.getElementById("li_expand");
    li_expand.style.display = "inline-block";
    var li_compress = document.getElementById("li_compress");
    li_compress.style.display = "none";
}
function PageISalestaxRPBind() {
    window.localStorage.setItem('ctCheck', 1);
    theamLoad();
    var btnExp = document.getElementById("btnInvExp");
    btnExp.style.display = "block";
    if (window.localStorage.getItem('RPPAGECODE') == 'POSTVRR') {
        var SubOrgId = '';
        var HashVal = (window.localStorage.getItem("hashValues"));
        var HashValues;
        if (HashVal != null && HashVal != '') {
            HashValues = HashVal.split('~');

            SubOrgId = HashValues[1];
            $('#hdnDateFromate').val(HashValues[2]);
            $('#hdnPaging').val(HashValues[19]);
        }

        GetTaxDetails(window.localStorage.getItem('RPfrmDate'), window.localStorage.getItem('RPtoDate'), "", "");


    }
}
function exportexcelSRP() {
    window.localStorage.setItem('ctCheck', 1);
    var data_type = 'data:application/vnd.ms-excel';
    var table_div = document.getElementById('InventoryView');
    var table_html = table_div.outerHTML.replace(/ /g, '%20');
    var a = document.createElement('a');
    a.href = data_type + ', ' + table_html;
    a.download = 'Sales Revenue' + '.xls';
    a.click();

} function exportexcelTAX() {
    window.localStorage.setItem('ctCheck', 1);
    var data_type = 'data:application/vnd.ms-excel';
    var table_div = document.getElementById('InventoryView');
    var table_html = table_div.outerHTML.replace(/ /g, '%20');
    var a = document.createElement('a');
    a.href = data_type + ', ' + table_html;
    a.download = 'VAT Returns' + '.xls';
    a.click();

}
function loadGraphDataTAX() {
    window.localStorage.setItem('ctCheck', 1);
    var param = { CounterId: window.localStorage.getItem('COUNTERID'), ShiftId: window.localStorage.getItem('SHIFTID'), SalespersonId: window.localStorage.getItem('CashierID') };
    var color = Chart.helpers.color;
    var barChartData = '';
    var GrColorTax = '';
    if (window.localStorage.getItem("Theam").split('-')[3] == '2.css') { GrColorTax = '#ffffff' } else { GrColorTax = '#000' }

    var elemGrid = document.getElementById("div_grid");
    elemGrid.style.display = "none";

    var elemGraph = document.getElementById("div_graph1");
    elemGraph.style.display = "block";
     
    var resp = graphDataTAX;

    var x = []; var y = [];; var z = [];

    for (var i = 0; i < resp.length; i++) {
        x.push(resp[i].Desc);
        y.push(parseFloat(resp[i].Amount).toFixed(2));
        z.push(parseFloat(resp[i].TaxAmount).toFixed(2));

    } 
    barChartData = {
        labels: x,
        datasets: [{
            label: 'Type',
            backgroundColor: ["#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276"], //color(window.chartColors.red).alpha(0.5).rgbString(),
           borderColor: ["#ff7276", "#ff999c", "#ffb3b5", "#ffccce", "#ffe6e6", "#ff7276", "#ff7276", "#ff7276"],//window.chartColors.red,
            borderWidth: 1,
            data: y
        },
        {
            type: 'line',
            label: $('#hdnTAXAmount').val(),//'TAX Amount',
            borderColor: window.chartColors.white,
            borderWidth: 2,
            fill: false,
            data: z,

        }
            ,
        {
            type: 'line',
            label:  $('#hdnGAmt').val(),//' Amount',
            borderColor: window.chartColors.white,
            borderWidth: 1,
            fill: false,
            data: y,

        }
        ]
    }

    var ctx = document.getElementById("canvas").getContext("2d");
    window.myBar = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            legend: {
                labels: {
                    fontColor: GrColorTax,
                    fontSize: 12
                }
            },
            responsive: true,
            title: {
                display: true,
                text: ''
            },

            scales: {
                yAxes: [{
                    ticks: {
                        min: 0,
                        fontColor: GrColorTax,
                        fontSize: 14
                    }
                    ,
                    gridLines: {
                        color: GrColorTax
                       
                    },
                    stacked: true
                }],
                xAxes: [{
                    ticks: {
                        autoSkip: false,
                        maxRotation: 60,
                        minRotation: 60,
                        fontColor: GrColorTax,
                        fontSize: 14
                    },
                    gridLines: {
                        color: GrColorTax,
                        lineWidth: 2
                    }
                }]
            }
        }
    });


}
function loadGraphDataSRP() {
    window.localStorage.setItem('ctCheck', 1);
    $('#li-Excel i').attr('style', 'color:#919598 !important');
    $('#li-Excel').attr('onclick', 'return false');
    $('#li-Refresh i').attr('style', 'color:#919598 !important');
    $('#li-Refresh').attr('onclick', 'return false');
    $('#li_Print i').attr('style', 'color:#919598 !important');
    $('#li_Print').attr('onclick', 'return false');

    var param = { CounterId: window.localStorage.getItem('COUNTERID'), ShiftId: window.localStorage.getItem('SHIFTID'), SalespersonId: window.localStorage.getItem('CashierID') };
    var color = Chart.helpers.color;
    var barChartData = '';
    var GrColorSr = '';
    if (window.localStorage.getItem("Theam").split('-')[3] == '2.css') { GrColorSr = '#ffffff' } else { GrColorSr = '#000' }

    var elemGrid = document.getElementById("div_grid");
    elemGrid.style.display = "none";

    var elemGraph = document.getElementById("div_graph1");
    elemGraph.style.display = "block";


    //objInvView


    var resp = graphDataSRP[0].objInvView;

    var x = []; var y = [];; var z = [];
var TypeMoments="";
    for (var i = 0; i < resp.length; i++) {
if (resp[i].InventoryViewCode == "POS-Cash/POS-Cash")
                                            TypeMoments =$('#hdnPOSRCash').val();
                                        if (resp[i].InventoryViewCode == "POS-Credit Card/POS-Credit Card")
                                            TypeMoments =$('#hdnPOSRCard').val(); 
                                        if (resp[i].InventoryViewCode == "POS-Sales/POS-Sales/Revenue")
                                            TypeMoments =$('#hdnPOSRSales').val();
                                        if (resp[i].InventoryViewCode == "POS-Tax/POS-Tax")
                                            TypeMoments = $('#hdnPOSRTax').val();
                                        if (resp[i].InventoryViewCode == "POS-RoundOff/POS-RoundOff")
                                            TypeMoments = $('#hdnPOSRRound').val();
                                        if (resp[i].InventoryViewCode == "POS-E-Pay/POS-E-Pay")
                                            TypeMoments = $('#hdnPOSREPay').val();
                                     if (resp[i].InventoryViewCode == "POS-Credit Note/POS-Credit Note")
                                            TypeMoments = $('#hdnPOSRCNote').val();
                                      if (resp[i].InventoryViewCode == "POS-Credit Sales/POS-Credit Sales")
                                            TypeMoments = $('#hdnPOSCrSales').val();//'POS-ضريبة';
        x.push(TypeMoments);
        y.push(parseFloat(resp[i].CreditAmount).toFixed(2));
        z.push(parseFloat(resp[i].DebitAmount).toFixed(2));

    }
    resp = ''; 
    barChartData = {
        labels: x,
        datasets: [{
            label: $('#hdnAccount').val(),
            backgroundColor: ["#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276"], //color(window.chartColors.red).alpha(0.5).rgbString(),
            borderColor: ["#ff7276", "#ff999c", "#ffb3b5", "#ffccce", "#ffe6e6", "#ff7276", "#ff7276", "#ff7276"],//window.chartColors.red,
            borderWidth: 1,
            data: y
        },
        {
            type: 'line',
            label: $('#hdnDebitAmount').val(),
            borderColor: window.chartColors.orange,
            borderWidth: 2,
            fill: false,
            data: z,

        }
            ,
        {
            type: 'line',
            label: $('#hdnCreditAmount').val(),
            borderColor: window.chartColors.red,
            borderWidth: 1,
            fill: false,
            data: y,

        }
        ]
    }

    var ctx = document.getElementById("canvas").getContext("2d");
    window.myBar = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            legend: {
                labels: {
                    fontColor: GrColorSr,
                    fontSize: 12
                }
            },
            responsive: true,
            title: {
                display: true,
                text: ''
            },
            scales: {
                yAxes: [{
                    ticks: {
                        min: 0,
                        fontColor: GrColorSr,
                        fontSize: 14
                    }
                    ,
                    gridLines: {
                        color: GrColorSr
                       
                    },
                    stacked: true
                }],
                xAxes: [{
                    ticks: {
                        autoSkip: false,
                        maxRotation: 60,
                        minRotation: 60,
                        fontColor: GrColorSr,
                        fontSize: 14
                    },
                    gridLines: {
                        color: GrColorSr,
                        lineWidth: 2
                    }
                }]
            }
        }
    });
}
function GetTaxDetails(FromDate, ToDate, Status, PosId) {

    window.localStorage.setItem('ctCheck', 1);
    var Paging = ""; var SubOrgId = ''; var Orgcode = ''; var OBS = ''; var CashierID = window.localStorage.getItem("CashierID"); var Userid = '';
    var Lang = ''; var BaseCurr_Id = ''; var CURR_CODE = ''; var CompanyId = '';
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        CompanyId = hashSplit[0];
        SubOrgId = hashSplit[1];
        OBS = hashSplit[9];
        Orgcode = hashSplit[17];
        Userid = hashSplit[14];
        Lang = hashSplit[7];
        BaseCurr_Id = hashSplit[16];
        CURR_CODE = hashSplit[18];
        Paging = hashSplit[19];
    }

    $('#hdnBranchId').val(SubOrgId);
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetTaxData,
        data: "SubOrgId=" + $('#hdnBranchId').val() + "&FromDate=" + FromDate + "&ToDate=" + ToDate + ' 23:59:59' + "&Status=" + Status + "&VD=" + VD(),

        success: function (resp) {
            graphDataTAX = resp;
            var result = resp;
            if (result.length > 0) {
                debugger

                $('#InventoryView tbody').remove();



                $('#InventoryView').append("<tbody>");
                for (var i = 0; i < result.length; i++) {
                    //$('#InventoryView').append("<tr><td style='display:none'>" + result[i].objInvView[j].Int_Account_Id + "</td><td>" + result[i].objInvView[j].InventoryViewCode + "</td><td style='text-align:right'>" + parseFloat(result[i].objInvView[j].DebitAmount).toFixed(2) + "</td><td style='text-align:right'>" + parseFloat(result[i].objInvView[j].CreditAmount).toFixed(2) + "</td><td>" + result[i].objInvView[j].MomentType + "</td><td style='display:none'>" + parseFloat(result[i].objInvView[j].DebitAmount) + "</td><td style='display:none'>" + parseFloat(result[i].objInvView[j].CreditAmount) + "</td></tr>");
                    $('#InventoryView').append("<tr><td>" + result[i].Desc + "</td><td id='ColAlign'>" + result[i].Amount + "</td><td id='ColAlign'>" + result[i].TaxAmount + "</td></tr>");

                }
                $('#InventoryView').append("</tbody>");



                if (window.localStorage.getItem('RPfrmDate') != null) {
                    $('#INVLISlblDate').text('VAT Returns(' + ConvertDateAndReturnDate($('#hdnDateFromate').val(), window.localStorage.getItem('RPfrmDate')) + ' to ' + ConvertDateAndReturnDate($('#hdnDateFromate').val(), window.localStorage.getItem('RPtoDate')) + ')');
                }
            }
            else {
                $('#InventoryView tbody').remove();

                $('#InventoryView').append("<tbody>");
                for (var i = 0; i < 4; i++) {
                    $('#InventoryView').append("<tr><td style='display:none'></td><td></td><td></td><td></td><td></td></tr>");

                }

                $('#InventoryView').append("</tbody>");
            }
        },
        error: function (e) {
            //BootstrapDialog.alert('Invalid Data', null, 'Error');
        }
    });
    var btnExp = document.getElementById("btnInvExp");
    btnExp.style.display = "none";
}
function GetAccountDetails(FromDate, ToDate, Status, PosId) {
    window.localStorage.setItem('ctCheck', 1);

    var Paging = ""; var SubOrgId = ''; var Orgcode = ''; var OBS = ''; var CashierID = window.localStorage.getItem("CashierID"); var Userid = '';
    var Lang = ''; var BaseCurr_Id = ''; var CURR_CODE = ''; var CompanyId = '';
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        CompanyId = hashSplit[0];
        SubOrgId = hashSplit[1];
        OBS = hashSplit[9];
        Orgcode = hashSplit[17];
        Userid = hashSplit[14];
        Lang = hashSplit[7];
        BaseCurr_Id = hashSplit[16];
        CURR_CODE = hashSplit[18];
        Paging = hashSplit[19];
    }

    $('#hdnBranchId').val(SubOrgId);

    //getResourcesSDREV(Lang, "POSAccountPosting", 1)
    var pageCode = 'POSAccountPosting';
    $.ajax({
        type: 'GET',
        async:false,
        url: CashierLite.Settings.getResource,
        data: "langCode=" + Lang + "&pageCode=" + pageCode,
        success: function (resp) {
            setLabelsSDREV(resp, '1');


            $.ajax({
                type: 'GET',
                url: CashierLite.Settings.GetAccountDetails,
                data: "SubOrgId=" + $('#hdnBranchId').val() + "&FromDate=" + FromDate + "&ToDate=" + ToDate + ' 23:59:59' + "&Status=" + Status + "&VD=" + VD(),

                success: function (resp) {

                    graphDataSRP = resp;
                    var result = resp;
                    var TypeMoment="";
                    if (result.length > 0) {
                   if (HashValues.split('~')[7] == 'ar-SA') {
                        jQuery("label[for='lbl_invPageOf'").html(" 1  من 1");
                        }
                        else
                       {
                        jQuery("label[for='lbl_invPageOf'").html(" 1 of 1");
                       }
                        var INVtotaldebitAmmount = 0.00;
                        var INVTotalCreditAmmount = 0.00;
                        var GLtotaldebitAmmount = 0.00;
                        var GLTotalCreditAmmount = 0.00;

                        $('#InventoryView thead tr').remove();
                        $('#InventoryView tbody tr').remove();

                        $('#InventoryView').append("<thead><tr><th style='width:10px;display:none' ></th><th>" + $('#hdnAccount').val() + "</th><th>" + $('#hdnDebitAmount').val() + "</th><th>" + $('#hdnCreditAmount').val() + "</th><th style='display:none'>" + $('#hdnMovementType').val() + "</th></tr></thead>");

                        for (var i = 0; i < result.length; i++) {
                            $('#InventoryView tbody').remove();

                            if (result[i].objInvView.length > 0) {

                                $('#InventoryView').append("<tbody>");
                                for (var j = 0; j < result[i].objInvView.length; j++) {
                                    var DebitAmount = parseFloat(result[i].objInvView[j].DebitAmount).toFixed(2);
                                    var CreditAmount = parseFloat(result[i].objInvView[j].CreditAmount).toFixed(2);
                                        
                                        if (result[i].objInvView[j].InventoryViewCode == "POS-Cash/POS-Cash")
                                            TypeMoment =$('#hdnPOSRCash').val();// 'POS-نقدي';
                                        if (result[i].objInvView[j].InventoryViewCode == "POS-Credit Card/POS-Credit Card")
                                            TypeMoment =$('#hdnPOSRCard').val(); //'POS-بطاقة';
                                        if (result[i].objInvView[j].InventoryViewCode == "POS-Sales/POS-Sales/Revenue")
                                            TypeMoment =$('#hdnPOSRSales').val();// 'POS-قيمة';
                                        if (result[i].objInvView[j].InventoryViewCode == "POS-Tax/POS-Tax")
                                            TypeMoment = $('#hdnPOSRTax').val();//'POS-ضريبة';
                                        if (result[i].objInvView[j].InventoryViewCode == "POS-RoundOff/POS-RoundOff")
                                            TypeMoment = $('#hdnPOSRRound').val();//'POS-ضريبة';
                                        if (result[i].objInvView[j].InventoryViewCode == "POS-E-Pay/POS-E-Pay")
                                            TypeMoment = $('#hdnPOSREPay').val();//'POS-ضريبة';
                                     if (result[i].objInvView[j].InventoryViewCode == "POS-Credit Note/POS-Credit Note")
                                            TypeMoment = $('#hdnPOSRCNote').val();//'POS-ضريبة';
                                     if (result[i].objInvView[j].InventoryViewCode == "POS-Credit Sales/POS-Credit Sales")
                                            TypeMoment = $('#hdnPOSCrSales').val();//'POS-ضريبة';
                                    $('#InventoryView').append("<tr><td id='txtColAlign'>" + TypeMoment + "</td><td id='ColAlign'>" + DebitAmount + '&nbsp' + "</td><td id='ColAlign'>" + CreditAmount + '&nbsp' + "</td><td style='display:none'>" + TypeMoment + "</td></tr>");
                                }
                                var tr = $('#InventoryView').find("tbody tr"); 
                                INVtotaldebitAmmount = parseFloat(resp[0].objGLView[0].GLDebitAmount).toFixed(2);
                                INVTotalCreditAmmount = parseFloat(resp[0].objGLView[0].GLCreditAmount).toFixed(2);
                                $('#InventoryView').append("<tr><td style='display:none'></td><td style='font-weight:bold;text-align:center;'>" + $('#hdnTotal').val() + "</td><td id='ColAlign'>" + INVtotaldebitAmmount + "</td><td id='ColAlign'>" + INVTotalCreditAmmount + "</td><td style='display:none'></td></tr>");
                                $('#InventoryView').append("</tbody>");
                            }


                            if (window.localStorage.getItem('RPfrmDate') != null) {
                                $('#INVLISlblDate').text('(' + ConvertDateAndReturnDate($('#hdnDateFromate').val(), window.localStorage.getItem('RPfrmDate')) + ' ' + $('#hdnTo').val() + ' ' + ConvertDateAndReturnDate($('#hdnDateFromate').val(), window.localStorage.getItem('RPtoDate')) + ')');
                            }
                            if (result[i].objInvView.length == 0) {
                                $('#InventoryView').append("<tbody>");
                                for (var l = 0; l < 4; l++) {
                                    $('#InventoryView').append("<tr><td style='display:none'></td><td></td><td></td><td></td><td></td></tr>");
                                }
                                $('#InventoryView').append("</tbody>");
                            }


                        }


                    }
                    else {
                        $('#InventoryView tbody').remove();

                        $('#InventoryView').append("<tbody>");
                        for (var i = 0; i < 4; i++) {
                            $('#InventoryView').append("<tr><td style='display:none'></td><td></td><td></td><td></td><td></td></tr>");

                        }

                        $('#InventoryView').append("</tbody>");
                    }
                },
                error: function (e) {
                }
            });
        }
    });

    var btnExp = document.getElementById("btnExp");
    btnExp.style.display = "none";
}

function setLabelsSDREV(labelData, sts) {
    window.localStorage.setItem('ctCheck', 1);
    jQuery("label[for='lblLocationwiseProfitAnalysis'").html(labelData["LocwiseProfitAnlys"]);
    jQuery("label[for='lblItemCategoryProfitAnalysis'").html(labelData["ItemCatgPrftAnlys"]);

    jQuery("label[for='lblTitle'").html(labelData['accountPosting']);
    jQuery("label[for='lblVATReturns'").html(labelData["accountPosting"]);
     
    $('#hdnAccount').val(labelData["account"]);
    $('#hdnDebitAmount').val(labelData["debitAmount"]);
    $('#hdnCreditAmount').val(labelData["creditAmount"]);
    $('#hdnMovementType').val(labelData['movementType']);
    $('#hdnTotal').val(labelData["total"]);
    $('#hdnSalesRevenue').val(labelData["accountPosting"]);
    $('#hdnTo').val(labelData["POSTo"]);
    $('#hdnPOSRCash').val(labelData["POSRVSCash"]);
    $('#hdnPOSRCard').val(labelData["POSRVSCard"]);
    $('#hdnPOSRSales').val(labelData["POSRVSSales"]);
    $('#hdnPOSRTax').val(labelData["POSRVSTax"]);
$('#hdnPOSRRound').val(labelData["POSRVSRound"]);
$('#hdnPOSREPay').val(labelData["POSRVSEPay"]);
$('#hdnPOSCrSales').val(labelData["PosCrdSales"]);
$('#hdnPOSRCNote').val(labelData["POSRVCNotes"]);
    jQuery("label[for='lbl_page'").html(labelData["page"]);
    jQuery("label[for='lblMail'").html(labelData["Mail"]);
    jQuery("label[for='ToMail'").html(labelData["POSTo"]);
    jQuery("label[for='Bcc'").html(labelData["BCC"]);
    jQuery("label[for='lblFormat'").html(labelData["format"]);
    jQuery("label[for='lblBody'").html(labelData["body"]);
    jQuery("label[for='lblsend'").html(labelData["Send"]);
    jQuery("label[for='lblClose'").html(labelData["Close"]);
    $('#hdnEnterMail').val(labelData["enterMail"]);
    $('#hdnMailSuccess').val(labelData["MailSuccess"]);
    $('#hdnMailnotSuccess').val(labelData["MailnotSuccess"]);
    $('#hdnInvalidData').val(labelData["InvalidData"]);
$('#hdnMExcel').val(labelData["Excel"]);
getLoadExdata();
    if (sts == 2) {
        var $el = $('#tbldiv tbody>tr.tt-hide');
        $el.removeClass("tt-hide");

        for (var i = 0; i < $el.length; i++) {
            $('#root_' + i + '').html("-");
        }
        $('#DivPager').css("display", "none");
    }
}


function arrangeTillPageInvoiceListingRPBind(resp) {
    window.localStorage.setItem('ctCheck', 1);
    var result = resp;
    var Primaryid = ""; 
    var cc_ = '';

    for (var i = 0; i < result.length; i++) {
        cc_ = cc_ + '<tr id="dt_Parrent' + i + '" data-tt-parent="Paging"><td style="display:none"><div class="tt" data-tt-id="root' + i + '" data-tt-parent=""><div class="content" id="root_' + i + '">+</div></div></td><td onclick="treeclick(this);">' + result[i].C1.split('/')[1] + '</td><td style="text-align:right">' + result[i].C2 + '</td></tr>';
        cc_ = cc_ + '<tr  id="dt_Parrent' + i + '_0"  style="display:none" data-tt-parent="childPaging"><td style="display:none"><div class="tt" data-tt-id="jacob" data-tt-parent="root' + i + '"></div></td><td colspan="2" ><table id="tblChild' + i + '" data-tt-parent="root' + i + '" class="table table-bordered" >';
        cc_ = cc_ + "<tr><th>" + $('#hdnInvoiceNo').val() + "</th><th>" + $('#hdnInvoiceDt').val() + "</th><th>" + $('#hdnBaseTot').val() + "</th><th>" + $('#hdnTaxTot').val() + "</th><th>" + $('#hdnNetTot').val() + "</th></tr>";
        for (var j = 0; j < result[i].objRepparams.length; j++) {
            if (resp[i].C0 == result[i].objRepparams[j].Till_Id) {
                cc_ = cc_ + '<tr><td>' + result[i].objRepparams[j].C1 + '&nbsp' + '</td><td>' + result[i].objRepparams[j].C2 + '&nbsp' + '</td><td style="text-align:right">' + parseFloat(result[i].objRepparams[j].C3).toFixed(2) + '&nbsp' + '</td><td style="text-align:right">' + result[i].objRepparams[j].C4 + '&nbsp' + '</td><td style="text-align:right">' + result[i].objRepparams[j].C5 + '&nbsp' + '</td></tr>';
            }
        }
        cc_ = cc_ + '</table></td></tr>';

    }
    $("#tbldiv tbody").append(cc_);
}

var graphData1;


function TillPageInvoiceListingRPBind() {
    window.localStorage.setItem('ctCheck', 1);
    var HashVal = (window.localStorage.getItem("hashValues"));

    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');
        $('#hdnDateFromate').val(HashValues[2])
        if (HashValues[7] == 'ar-SA') {
            $('html,body').append('<link rel="stylesheet" href="Styles/css/bootstrap-rtl.css" type="text/css" />');
            $('html,body').append('<link rel="stylesheet" href="Styles/css/bootstrap-flipped.css" type="text/css" />');
            $('head').append('<script src="scripts/bootstrap-dialog-SA.min.js"></script>');
        }

    } 
    theamLoad();
    var btnExp = document.getElementById("btnInvExp");
    btnExp.style.display = "block";

    var SubOrgId = '';
    var HashVal = (window.localStorage.getItem("hashValues"));
    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');
        SubOrgId = HashValues[1];
        $('#hdnDateFromate').val(HashValues[2]);
        // $('#hdnPaging').val(HashValues[19]);
    } 
    var grid = "";
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetReport_V01,
        data: "PageId=" + window.localStorage.getItem('RPPAGECODE') + "&subOrg=" + SubOrgId + "&frmDate=" + window.localStorage.getItem('RPfrmDate') + "&toDate=" + window.localStorage.getItem('RPtoDate') + ' 23:59:59' + "&refNo=" + window.localStorage.getItem('RPrefNo') + "&DateFormate=" + $('#hdnDateFromate').val() + "&VD=" + VD2() + "&language=" + HashValues[7] + "",
        success: function (resp) {
            graphData1 = resp;
            if (resp.length > 0) {
                jQuery("label[for='lbl_invPageOf'").html(resp[0].C11);
            }
            getResources4(HashValues[7], "POSTillInvoiceListingReport", resp, 1)
            $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());
             
        }

    });

}

function openShortCutPopup() {
    window.localStorage.setItem('ctCheck', 1);
    $('#ModalShortCutPopup').modal('show');
}
function GetPaging2() {
    window.localStorage.setItem('ctCheck', 1);
    $('table#tbldiv').each(function () {

        var currentPage = 0;
        var numPerPage = $('#hdnPaging').val();
        var $table = $(this);
        var $tr = $table.find('tbody tr');

        $table.bind('repaginate', function () {
            $table.find('tbody tr[data-tt-parent="Paging"]').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
        });
        //  
        $table.trigger('repaginate');
        var numRows = $table.find('tbody tr[data-tt-parent="Paging"]').length;
        var numPages = Math.ceil(numRows / numPerPage);

        $("#DivPager").remove();

        var $pager = $('<div id="DivPager" class="pager"></div>');
        for (var page = 0; page < numPages; page++) {
            $('<span class="page-number"></span>').text(page + 1).bind('click', {
                newPage: page
            }, function (event) {
                currentPage = event.data['newPage'];
                $table.trigger('repaginate');
                $(this).addClass('active').siblings().removeClass('active');
            }).appendTo($pager).addClass('clickable');
        }
        $pager.insertAfter($table).find('span.page-number:first').addClass('active');

    });

}
function GetInvUnpaging() {
    window.localStorage.setItem('ctCheck', 1);
    if (window.localStorage.getItem('RPPAGECODE') == 'POSINLS') {
        InvoiceUnPaging();
    }
    if (window.localStorage.getItem('RPPAGECODE') == 'POSTINL') {
        TilInvoiceUnPaging();
    }
}
function InvoiceUnPaging() {
    window.localStorage.setItem('ctCheck', 1);
    $('#hdnPaging').val(parseFloat($('#hdnPaging').val()) - 1);
    if ($('#txt_pageCount').val() == "") {
        $('#txt_pageCount').val('10');
        $('#hdnPaging').val('1');
    }
    if (parseFloat($('#hdnPaging').val()) == 0) {
        $('#hdnPaging').val('1');
    }
    var btnExp = document.getElementById("btnInvExp");
    btnExp.style.display = "block";
    var SubOrgId = '';
    var HashVal = (window.localStorage.getItem("hashValues"));
    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');

        SubOrgId = HashValues[1];
        $('#hdnDateFromate').val(HashValues[2]);

    }

    var PageIndex = $('#hdnPaging').val();
    var PageCount = $('#txt_pageCount').val();

    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetReport_V01,
        data: "PageId=" + window.localStorage.getItem('RPPAGECODE') + "&subOrg=" + SubOrgId + "&frmDate=" + window.localStorage.getItem('RPfrmDate') + "&toDate=" + window.localStorage.getItem('RPtoDate') + ' 23:59:59' + "&refNo=" + window.localStorage.getItem('RPrefNo') + "&DateFormate=" + $('#hdnDateFromate').val() + "&VD=" + VD2() + "&PageIndex=" + PageIndex + "&PageCount=" + PageCount + "&language=" + HashValues[7] + "",
        success: function (resp) {
            graphData = resp;
            jQuery("label[for='lbl_invPageOf'").html(resp[0].C15);
            getResources5(HashValues[7], "POSInvoiceListingReport", resp, 2);
            $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());


        }

    });
}
function TilInvoiceUnPaging() {
    window.localStorage.setItem('ctCheck', 1);
    var SubOrgId = '';
    var HashVal = (window.localStorage.getItem("hashValues"));
    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');
        SubOrgId = HashValues[1];
        $('#hdnDateFromate').val(HashValues[2]);
        $('#hdnPaging').val(HashValues[19]);
    }
    if (window.localStorage.getItem('RPfrmDate') != null) {
        $('#INVLISlblDate').text('(' + ConvertDateAndReturnDate($('#hdnDateFromate').val(), window.localStorage.getItem('RPfrmDate')) + ' to ' + ConvertDateAndReturnDate($('#hdnDateFromate').val(), window.localStorage.getItem('RPtoDate')) + ')');
    }


    var grid = "";
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetReport,
        data: "PageId=" + window.localStorage.getItem('RPPAGECODE') + "&subOrg=" + SubOrgId + "&frmDate=" + window.localStorage.getItem('RPfrmDate') + "&toDate=" + window.localStorage.getItem('RPtoDate') + ' 23:59:59' + "&refNo=" + window.localStorage.getItem('RPrefNo') + "&DateFormate=" + $('#hdnDateFromate').val() + "&VD=" + VD2(),
        success: function (resp) {
            getResources4(HashValues[7], "POSTillInvoiceListingReport", resp, 2)
            
        }
    });
}
function Invprint() {
    window.localStorage.setItem('ctCheck', 1);
    var btnCol = document.getElementById("btnInvCol");
   
    loadInvoiceGridExtend();


    var divContents = document.getElementById("div_grid").innerHTML;
    var printWindow = window.open('', '', 'height=200,width=450');
    printWindow.document.write('<html><head><title>Invoice Listing</title>');
    printWindow.document.write('</head><body >');
    printWindow.document.write(divContents);
    printWindow.document.write('</body></html>');
    loadInvoiceGridCompress();
    printWindow.document.close();
    printWindow.print();


}

function TillInvoicePrint() {
    window.localStorage.setItem('ctCheck', 1);
    var btnCol = document.getElementById("btnInvCol");

    loadTillInvoiceGridExtend();


    var divContents = document.getElementById("div_grid").innerHTML;
    var printWindow = window.open('', '', 'height=200,width=450');
    printWindow.document.write('<html><head><title>Till Invoice Listing</title>');
    printWindow.document.write('</head><body >');
    printWindow.document.write(divContents);
    printWindow.document.write('</body></html>');
    loadTillInvoiceGridCompress();
    printWindow.document.close();
    printWindow.print();


}

function SRPrint() {
    window.localStorage.setItem('ctCheck', 1);
    var btnCol = document.getElementById("btnCol");

    var divContents = document.getElementById("div_grid").innerHTML;
    var printWindow = window.open('', '', 'height=200,width=450');
    printWindow.document.write('<html><head><title>Sales Revenue</title>');
    printWindow.document.write('</head><body >');
    printWindow.document.write(divContents);
    printWindow.document.write('</body></html>');
    //  loadInvoiceGridCompress();
    printWindow.document.close();
    printWindow.print();


}
$('#btnInvCol').click(function () {
    window.localStorage.setItem('ctCheck', 1);
    var btnCol = document.getElementById("btnInvCol");
    btnCol.style.display = "none";

    var btnExp = document.getElementById("btnInvExp");
    btnExp.style.display = "block";


    var $el = $('#tbldiv tbody>tr');

    $el.addClass("tt-hide");

    for (var i = 0; i < $('#tbldiv tbody>tr').length; i++) {
        var tr = $('#tbldiv tbody>tr[id="dt_Parrent' + i + '"]');
        //var tr = $(table).find("tr[id='dt_Parrent" + i + "']");
        //var tdid = $($el[i]).find("td[id='dt_Parrent" + i + "']");
        $(tr).removeClass("tt-hide");
        $('#root_' + i + '').html("+");
    }

    $('#dt_Parrent1').removeClass("tt-hide");
    //GetPaging();

});
$('#btnInvExp').click(function () {
    window.localStorage.setItem('ctCheck', 1);

    var btnCol = document.getElementById("btnInvCol");
    btnCol.style.display = "block";

    var btnExp = document.getElementById("btnInvExp");
    btnExp.style.display = "none";

    $('#tbldiv').html("");
    $('#hdnScreenmode').val("");
    GetInvUnpaging();

});
function Invoiceexportexcel() {
    window.localStorage.setItem('ctCheck', 1);
    
    loadInvoiceGridExtend();
    var data_type = 'data:application/vnd.ms-excel';
    var table_div = document.getElementById('tbldiv');
    var table_html = table_div.outerHTML.replace(/ /g, '%20');
    var a = document.createElement('a');
    a.href = data_type + ', ' + table_html;
    var pageName_ = "exported_table_";

    pageName_ = "Invoice-Listing";

    a.download = pageName_ + '.xls';
    //a.click();

    var d = $('#div_tbData');

    let options = {
        documentSize: 'A4',
        type: 'share',
        fileName: 'Invoice-Listing.pdf'
    }
    var lable_ = "<div><b>Invoice Listing" + $('#INVLISlblDate').text() + "</b></div></br>";
    pdf.fromData(lable_ + d[0].innerHTML, options)
        .then((stats) => console.log('status', stats))   // ok..., ok if it was able to handle the file to the OS.  
        .catch((err) => console.err(err));
     
    loadInvoiceGridCompress(); 
}

function TillInvoiceexportexcel() {
    window.localStorage.setItem('ctCheck', 1);
    loadTillInvoiceGridExtend();
    var data_type = 'data:application/vnd.ms-excel';
    var table_div = document.getElementById('tbldiv');
    var table_html = table_div.outerHTML.replace(/ /g, '%20');
    var a = document.createElement('a');
    a.href = data_type + ', ' + table_html;
    var pageName_ = "exported_table_";

    pageName_ = "Till-Invoice-Listing";

    a.download = pageName_ + '.xls';
    a.click();
    loadTillInvoiceGridCompress();
}
function SRexportexcel() {
    window.localStorage.setItem('ctCheck', 1);
    var data_type = 'data:application/vnd.ms-excel';
    var table_div = document.getElementById('InventoryView');
    var table_html = table_div.innerText.replace(/ /g, '%20');
    var a = document.createElement('a');
    a.href = data_type + ', ' + table_html;
    var pageName_ = "exported_table_";

    pageName_ = "Sales-Revenue";

    a.download = pageName_ + '.xls';
    //a.click();


    var d = $('#div_tbData');
    let options = {
        documentSize: 'A4',
        type: 'share',
        fileName: 'Sales-Revenue.pdf'
    }
    var lable_ = "<div><b>Sales Revenue " + $('#INVLISlblDate').text() + "</b></div></br>";
    pdf.fromData(lable_ + d[0].innerHTML, options)
        .then((stats) => console.log('status', stats))   // ok..., ok if it was able to handle the file to the OS.  
        .catch((err) => console.err(err));

    // loadInvoiceGridCompress();

}

function exportexcel() {
    window.localStorage.setItem('ctCheck', 1);
    $('#hdnScreenmode').val("excel");
    $('#tbldiv').html("");
    TilInvoiceUnPaging();
}
function nextPRInvl() {
    window.localStorage.setItem('ctCheck', 1);
    $('#hdnPaging').val(parseFloat($('#hdnPaging').val()) + 1);
    if ($('#txt_pageCount').val() == "") {
        $('#txt_pageCount').val('10');
        $('#hdnPaging').val('1');
    }
    var btnExp = document.getElementById("btnInvExp");
    btnExp.style.display = "block";
    if (window.localStorage.getItem('RPPAGECODE') == 'POSINLS') {
        var SubOrgId = '';
        var HashVal = (window.localStorage.getItem("hashValues"));
        var HashValues;
        if (HashVal != null && HashVal != '') {
            HashValues = HashVal.split('~');

            SubOrgId = HashValues[1];
            $('#hdnDateFromate').val(HashValues[2]);

        }

        var PageIndex = $('#hdnPaging').val();
        var PageCount = $('#txt_pageCount').val();

        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.GetReport_V01,
            data: "PageId=" + window.localStorage.getItem('RPPAGECODE') + "&subOrg=" + SubOrgId + "&frmDate=" + window.localStorage.getItem('RPfrmDate') + "&toDate=" + window.localStorage.getItem('RPtoDate') + ' 23:59:59' + "&refNo=" + window.localStorage.getItem('RPrefNo') + "&DateFormate=" + $('#hdnDateFromate').val() + "&VD=" + VD2() + "&PageIndex=" + PageIndex + "&PageCount=" + PageCount + "&language=" + HashValues[7] + "",
            success: function (resp) {
                graphData = resp;
                jQuery("label[for='lbl_invPageOf'").html(resp[0].C15);
                getResources5(HashValues[7], "POSInvoiceListingReport", resp, 1);
                $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());


            }

        });
    }

    if ($('#hdnTotalPagesCount').val() == $('#hdnPaging').val()) {
        $('#li_PRPrev i').attr('style', 'color:#919598 !important');
        $('#li_PRPrev').attr('onclick', 'return false');
        $('#li_PRLast').attr('onclick', 'return false');
        $('#li_PRLast i').attr('style', 'color:#919598 !important');
    }
}
function prevPRInvl() {
    window.localStorage.setItem('ctCheck', 1);
    $('#hdnPaging').val(parseFloat($('#hdnPaging').val()) - 1);
    if ($('#txt_pageCount').val() == "") {
        $('#txt_pageCount').val('10');
        $('#hdnPaging').val('1');
    }
    if (parseFloat($('#hdnPaging').val()) == 0) {
        $('#hdnPaging').val('1');
    }
    if ($('#hdnTotalPagesCount').val() != $('#hdnPaging').val()) {
        $('#li_PRPrev i').removeAttr('style', 'color:#919598 !important');
        $('#li_PRPrev').attr('onclick', 'nextPRInvl()');
        $('#li_PRLast i').removeAttr('style', 'color:#919598 !important');
        $('#li_PRLast').attr('onclick', 'nextPRLastInvl()');
    }
    var btnExp = document.getElementById("btnInvExp");
    btnExp.style.display = "block";
    if (window.localStorage.getItem('RPPAGECODE') == 'POSINLS') {
        var SubOrgId = '';
        var HashVal = (window.localStorage.getItem("hashValues"));
        var HashValues;
        if (HashVal != null && HashVal != '') {
            HashValues = HashVal.split('~');

            SubOrgId = HashValues[1];
            $('#hdnDateFromate').val(HashValues[2]);

        }

        var PageIndex = $('#hdnPaging').val();
        var PageCount = $('#txt_pageCount').val();

        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.GetReport_V01,
            data: "PageId=" + window.localStorage.getItem('RPPAGECODE') + "&subOrg=" + SubOrgId + "&frmDate=" + window.localStorage.getItem('RPfrmDate') + "&toDate=" + window.localStorage.getItem('RPtoDate') + ' 23:59:59' + "&refNo=" + window.localStorage.getItem('RPrefNo') + "&DateFormate=" + $('#hdnDateFromate').val() + "&VD=" + VD2() + "&PageIndex=" + PageIndex + "&PageCount=" + PageCount + "&language=" + HashValues[7] + "",
            success: function (resp) {
                graphData = resp;
                jQuery("label[for='lbl_invPageOf'").html(resp[0].C15);
                getResources5(HashValues[7], "POSInvoiceListingReport", resp, 1);
                $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());


            }

        });
    }
    //if (window.localStorage.getItem('RPPAGECODE') == 'POSTINL') {
    //    var SubOrgId = '';
    //    var HashVal = (window.localStorage.getItem("hashValues"));
    //    var HashValues;
    //    if (HashVal != null && HashVal != '') {
    //        HashValues = HashVal.split('~');

    //        SubOrgId = HashValues[1];
    //        $('#hdnDateFromate').val(HashValues[2]);

    //    }

    //    var PageIndex = $('#hdnPaging').val();
    //    var PageCount = $('#txt_pageCount').val();

    //    $.ajax({
    //        type: 'GET',
    //        url: CashierLite.Settings.GetReport_V01,
    //        data: "PageId=" + window.localStorage.getItem('RPPAGECODE') + "&subOrg=" + SubOrgId + "&frmDate=" + window.localStorage.getItem('RPfrmDate') + "&toDate=" + window.localStorage.getItem('RPtoDate') + ' 23:59:59' + "&refNo=" + window.localStorage.getItem('RPrefNo') + "&DateFormate=" + $('#hdnDateFromate').val() + "&VD=" + VD2() + "&PageIndex=" + PageIndex + "&PageCount=" + PageCount + "",
    //        success: function (resp) {
    //            graphData = resp;
    //            jQuery("label[for='lbl_invPageOf'").html(resp[0].C11);
    //            getResources4(HashValues[7], "POSTillInvoiceListingReport", resp, 1)
    //            $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());


    //        }

    //    });
    //}
}


function nextPRLastInvl() {
    window.localStorage.setItem('ctCheck', 1);
    var lastPageIndex = $('#lbl_invPageOf')[0].innerText.split(' ')[2];
    if (lastPageIndex == '') {
        lastPageIndex = '1';
    }
    $('#hdnPaging').val(parseFloat(lastPageIndex));
    if ($('#txt_pageCount').val() == "") {
        $('#txt_pageCount').val('10');
        $('#hdnPaging').val('1');
    }
    var btnExp = document.getElementById("btnInvExp");
    btnExp.style.display = "block";
    if (window.localStorage.getItem('RPPAGECODE') == 'POSINLS') {
        var SubOrgId = '';
        var HashVal = (window.localStorage.getItem("hashValues"));
        var HashValues;
        if (HashVal != null && HashVal != '') {
            HashValues = HashVal.split('~');

            SubOrgId = HashValues[1];
            $('#hdnDateFromate').val(HashValues[2]);

        }

        var PageIndex = $('#hdnPaging').val();
        var PageCount = $('#txt_pageCount').val();

        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.GetReport_V01,
            data: "PageId=" + window.localStorage.getItem('RPPAGECODE') + "&subOrg=" + SubOrgId + "&frmDate=" + window.localStorage.getItem('RPfrmDate') + "&toDate=" + window.localStorage.getItem('RPtoDate') + ' 23:59:59' + "&refNo=" + window.localStorage.getItem('RPrefNo') + "&DateFormate=" + $('#hdnDateFromate').val() + "&VD=" + VD2() + "&PageIndex=" + PageIndex + "&PageCount=" + PageCount + "&language=" + HashValues[7] + "",
            success: function (resp) {
                graphData = resp;
                jQuery("label[for='lbl_invPageOf'").html(resp[0].C15);
                getResources5(HashValues[7], "POSInvoiceListingReport", resp, 1);
                $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());


            }

        });
    }


    if ($('#hdnTotalPagesCount').val() == $('#hdnPaging').val()) {
        $('#li_PRPrev i').attr('style', 'color:#919598 !important');
        $('#li_PRPrev').attr('onclick', 'return false');
        $('#li_PRLast').attr('onclick', 'return false');
        $('#li_PRLast i').attr('style', 'color:#919598 !important');
    }

   
}
function prevPRFirstInvl() {
    window.localStorage.setItem('ctCheck', 1);
    PageInvoiceListingRPBind();
   
}
function loadInvoiceGrid() {
    window.localStorage.setItem('ctCheck', 1);
   
    if (window.localStorage.getItem('RPPAGECODE') == 'POSACPO') {
        $('#li-Excel i').removeAttr('style', 'color:#919598 !important');
        $('#li-Excel').attr('onclick', 'GetDowloadFile()');
        $('#li-Refresh i').removeAttr('style', 'color:#919598 !important');
        $('#li-Refresh').attr('onclick', 'PageISalesReceiptRPBind()');
        $('#li_Print i').removeAttr('style', 'color:#919598 !important');
        $('#li_Print').attr('onclick', 'SRPrint()');
    }
    if (window.localStorage.getItem('RPPAGECODE') == 'POSINLS') {
        $('#li-Excel i').removeAttr('style', 'color:#919598 !important');
        $('#li-Excel').attr('onclick', 'Invoiceexportexcel()');
        $('#li-Refresh i').removeAttr('style', 'color:#919598 !important');
        $('#li-Refresh').attr('onclick', 'PageInvoiceListingRPBind()');
        $('#li_expand i').removeAttr('style', 'color:#919598 !important');
        $('#li_expand').attr('onclick', 'loadInvoiceGridExtend()');
        $('#li_compress i').removeAttr('style', 'color:#919598 !important');
        $('#li_compress').attr('onclick', 'loadInvoiceGridCompress()');
        $('#li_Print i').removeAttr('style', 'color:#919598 !important');
        $('#li_Print').attr('onclick', 'Invprint()');
        $('#txt_PRPPageIndex').removeAttr('disabled', 'true');
        $('#btnPage').removeAttr('disabled', 'true');
        $('#li_PRFirst i').removeAttr('style', 'color:#919598 !important');
        $('#li_PRFirst').attr('onclick', 'prevPRFirstInvl()');
        $('#li_PRNext i').removeAttr('style', 'color:#919598 !important');
        $('#li_PRNext').attr('onclick', 'prevPRInvl()');
        $('#li_PRPrev i').removeAttr('style', 'color:#919598 !important');
        $('#li_PRPrev').attr('onclick', 'nextPRInvl()');
        $('#li_PRLast i').removeAttr('style', 'color:#919598 !important');
        $('#li_PRLast').attr('onclick', 'nextPRLastInvl()');
        $('#lbl_invPageOf').removeAttr('style', 'color:#919598 !important');
        $('#lbl_page').removeAttr('style', 'color:#919598 !important');
        if ($('#hdnTotalPagesCount').val() == $('#hdnPaging').val()) {
            $('#li_PRPrev i').attr('style', 'color:#919598 !important');
            $('#li_PRPrev').attr('onclick', 'return false');
            $('#li_PRLast').attr('onclick', 'return false');
            $('#li_PRLast i').attr('style', 'color:#919598 !important');
        }
    }
    if (window.localStorage.getItem('RPPAGECODE') == 'POSTINL') {
        $('#li-Excel i').removeAttr('style', 'color:#919598 !important');
        $('#li-Excel').attr('onclick', 'TillInvoiceexportexcel()');
        $('#li-Refresh i').removeAttr('style', 'color:#919598 !important');
        $('#li-Refresh').attr('onclick', 'loadInvoiceGrid()');
        $('#li_expand i').removeAttr('style', 'color:#919598 !important');
        $('#li_expand').attr('onclick', 'loadTillInvoiceGridExtend()');
        $('#li_compress i').removeAttr('style', 'color:#919598 !important');
        $('#li_compress').attr('onclick', 'loadTillInvoiceGridCompress()');
        $('#li_Print i').removeAttr('style', 'color:#919598 !important');
        $('#li_Print').attr('onclick', 'TillInvoicePrint()');
        $('#txt_PRPPageIndex').removeAttr('disabled', 'true');
        $('#btnPage').removeAttr('disabled', 'true');
        $('#li_PRFirst i').removeAttr('style', 'color:#919598 !important');
        $('#li_PRFirst').attr('onclick', 'prevPRFirstInvl()');
        $('#li_PRNext i').removeAttr('style', 'color:#919598 !important');
        $('#li_PRNext').attr('onclick', 'prevPRInvl()');
        $('#li_PRPrev i').removeAttr('style', 'color:#919598 !important');
        $('#li_PRPrev').attr('onclick', 'nextPRInvl()');
        $('#li_PRLast i').removeAttr('style', 'color:#919598 !important');
        $('#li_PRLast').attr('onclick', 'nextPRLastInvl()');
        $('#lbl_invPageOf').removeAttr('style', 'color:#919598 !important');
        $('#lbl_page').removeAttr('style', 'color:#919598 !important');
    }

    var elemGrid = document.getElementById("div_grid");
    elemGrid.style.display = "block";

    var elemGraph = document.getElementById("div_graph1");
    elemGraph.style.display = "none";
}



function loadGraphData2() {
    
    window.localStorage.setItem('ctCheck', 1);
    $('#li-Excel i').attr('style', 'color:#919598 !important');
    $('#li-Excel').attr('onclick', 'return false');
    $('#li-Refresh i').attr('style', 'color:#919598 !important');
    $('#li-Refresh').attr('onclick', 'return false');
    $('#li_expand i').attr('style', 'color:#919598 !important');
    $('#li_expand').attr('onclick', 'return false');
    $('#li_compress i').attr('style', 'color:#919598 !important');
    $('#li_compress').attr('onclick', 'return false');
    $('#li_Print i').attr('style', 'color:#919598 !important');
    $('#li_Print').attr('onclick', 'return false');
    $('#txt_PRPPageIndex').attr('disabled', 'true');
    $('#btnPage').attr('disabled', 'true');
    $('#li_PRFirst i').attr('style', 'color:#919598 !important');
    $('#li_PRFirst').attr('onclick', 'return false');
    $('#li_PRNext i').attr('style', 'color:#919598 !important');
    $('#li_PRNext').attr('onclick', 'return false');
    $('#li_PRPrev i').attr('style', 'color:#919598 !important');
    $('#li_PRPrev').attr('onclick', 'return false');
    $('#li_PRLast i').attr('style', 'color:#919598 !important');
    $('#li_PRLast').attr('onclick', 'return false');
    $('#lbl_invPageOf').attr('style', 'color:#919598 !important');
    $('#lbl_page').attr('style', 'color:#919598 !important');

    var param = { CounterId: window.localStorage.getItem('COUNTERID'), ShiftId: window.localStorage.getItem('SHIFTID'), SalespersonId: window.localStorage.getItem('CashierID') };
    var color = Chart.helpers.color;
    var barChartData = '';
    var GrColor = '';
    if (window.localStorage.getItem("Theam").split('-')[3] == '2.css') { GrColor = '#ffffff' } else { GrColor = '#000'}

    var elemGrid = document.getElementById("div_grid");
    elemGrid.style.display = "none";

    var elemGraph = document.getElementById("div_graph1");
    elemGraph.style.display = "block";

    var resp = graphData;

    var x = []; var y = [];


    for (var i = 0; i < resp.length; i++) {
        x.push(resp[i].C0);
        y.push(parseFloat(resp[i].C12).toFixed(2));
    }

    barChartData = {
        labels: x,
        datasets: [{
            label: $('#hdnSales').val(),
            backgroundColor: ["#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276","#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276"], //color(window.chartColors.red).alpha(0.5).rgbString(),
            borderColor: ["#ff7276", "#ff999c", "#ffb3b5", "#ffccce", "#ffe6e6", "#ff7276", "#ff7276", "#ff7276"],//window.chartColors.red,
            borderWidth: 1,
            data: y
        },
        {
            type: 'line',
            label: $('#hdnLineView').val(),
            borderColor: window.chartColors.red,
            borderWidth: 2,
            fill: false,
            data: y,

        }
        ]
    }

    var ctx = document.getElementById("canvas").getContext("2d");
    
    window.myBar = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            legend: {
                labels: {
                    fontColor: GrColor,
                    fontSize: 12
                }
            },
            responsive: true,
            title: {
                display: true,
                text: ''
            },
            scales: {
                yAxes: [{
                    ticks: {
                        min: 0,
                       // stepSize: 1,
                        fontColor: GrColor,
                        fontSize: 14
                    }
                    ,
                    gridLines: {
                        color: GrColor
                    //    lineWidth: 2,
                    //    zeroLineColor: "#000",
                    //    zeroLineWidth: 2
                    },
                    stacked: true
                }],
                xAxes: [{
                    ticks: {
                        autoSkip: false,
                        maxRotation: 60,
                        minRotation: 60,
                        fontColor: GrColor,
                        fontSize: 14
                    },
                    gridLines: {
                        color: GrColor,
                        lineWidth: 2
                    }
                }]
            }
        }
    });
}

function loadGraphData1() {
    window.localStorage.setItem('ctCheck', 1);
    var param = { CounterId: window.localStorage.getItem('COUNTERID'), ShiftId: window.localStorage.getItem('SHIFTID'), SalespersonId: window.localStorage.getItem('CashierID') };
    var color = Chart.helpers.color;
    var barChartData = '';
    var GrColorOne = '';
    if (window.localStorage.getItem("Theam").split('-')[3] == '2.css') { GrColorOne = '#ffffff' } else { GrColorOne = '#000' }
    var elemGrid = document.getElementById("div_grid");
    elemGrid.style.display = "none";

    var elemGraph = document.getElementById("div_graph1");
    elemGraph.style.display = "block";
    var resp = graphData1;
    var x = []; var y = [];
    for (var i = 0; i < resp.length; i++) {
        x.push(resp[i].C1);
        y.push(parseFloat(resp[i].C2).toFixed(2));
    }

    barChartData = {
        labels: x,
        datasets: [{
            label: $('#hdnSales').val(),
            backgroundColor: ["#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276"], //color(window.chartColors.red).alpha(0.5).rgbString(),
            borderColor: ["#ff7276", "#ff999c", "#ffb3b5", "#ffccce", "#ffe6e6", "#ff7276", "#ff7276", "#ff7276"],//window.chartColors.red,
            borderWidth: 1,
            data: y
        },
        {
            type: 'line',
            label: $('#hdnLineView').val(),
            borderColor: window.chartColors.white,
            borderWidth: 2,
            fill: false,
            data: y,

        }
        ]
    }

    var ctx = document.getElementById("canvas").getContext("2d");
    window.myBar = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            legend: {
                labels: {
                    fontColor: GrColorOne,
                    fontSize: 12
                }
            },
            responsive: true,
            title: {
                display: true,
                text: ''
            },
            scales: {
                yAxes: [{
                    ticks: {
                        min: 0,
                        // stepSize: 1,
                        fontColor: GrColorOne,
                        fontSize: 14
                    }
                    ,
                    gridLines: {
                        color: GrColorOne
                        //    lineWidth: 2,
                        //    zeroLineColor: "#000",
                        //    zeroLineWidth: 2
                    },
                    stacked: true
                }],
                xAxes: [{
                    ticks: {
                        autoSkip: false,
                        maxRotation: 60,
                        minRotation: 60,
                        fontColor: GrColorOne,
                        fontSize: 14
                    },
                    gridLines: {
                        color: GrColorOne,
                        lineWidth: 2
                    }
                }]
            }
        }
    });
}

function getResources2(langCode, pageCode) {
    window.localStorage.setItem('ctCheck', 1);
    $.ajax({
        type: 'GET',
        async: false,
        url: CashierLite.Settings.getResource,
        data: "langCode=" + langCode + "&pageCode=" + pageCode,
        success: function (resp) {

            setLabels2(resp);
            $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());
            var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < url.length; i++) {

                var urlparam = url[i].split('=');
                if (urlparam.length > 1) {
                    var pagecode = urlparam[1];
                    setLabels8(resp, pagecode);
                    $('#lblreportFromDate').show();
                    $('#lblreportToDate').show();
                    $('#lblreportSearchBy').show();
                    if (pagecode == "POSACPO") {
                        //$('#lblHeaderRPT').text('Invoice Listing');  
                        jQuery("label[for='lblHeaderRPT'").html($('#hdnAccountPosting').val());
                        $('#lblreportSearchBy').hide();
                        $('#hdnpageCode').val(pagecode);
                        break;
                    }
                    if (pagecode == "POSINLS") {
                        //$('#lblHeaderRPT').text('Invoice Listing');
                        jQuery("label[for='lblHeaderRPT'").html($('#hdnInvoiceListing').val());
                        $('#hdnpageCode').val(pagecode);
                        break;
                    }
                     if (pagecode == "POSINLSM") {
                        //$('#lblHeaderRPT').text('Invoice Listing');
                        jQuery("label[for='lblHeaderRPT'").html($('#hdnInvDetails').val());
                       // jQuery("label[for='lblHeaderRPT'").html('Invoice Summary');
                        $('#hdnpageCode').val(pagecode);
                        break;
                    }
                    if (pagecode == "POSTINL") {
                        //$('#lblHeaderRPT').text('Till - Invoice Listing');
                        //$('#lblHeaderRPT').text($('#hdnTillInvoiceListing').val());
                        $('#lblreportSearchBy').hide();
                        jQuery("label[for='lblHeaderRPT'").html($('#hdnTillInvoiceListing').val());
                        $('#hdnpageCode').val(pagecode);
                        break;
                    }
                      if (pagecode == "POSCUSTLDGR") {
                         //$('#lblHeaderRPT').text('Invoice Listing');
                         jQuery("label[for='lblHeaderRPT'").html($('#hdnCustLedger').val());
                         // jQuery("label[for='lblHeaderRPT'").html('Invoice Summary');
                         $('#hdnpageCode').val(pagecode);
                         break;
                     }
                    if (pagecode == "POSTTDS") {
                        jQuery("label[for='lblHeaderRPT'").html($('#hdnTenderTypeWiseCollectionDailySummary').val());
                        $('#hdnpageCode').val(pagecode);
                        break;
                    }
                    if (pagecode == "POSSGR") {
                        jQuery("label[for='lblHeaderRPT'").html($('#hdnSalsGrowth').val());
                        $('#hdnpageCode').val(pagecode);
                        break;
                    }
                    if (pagecode == "POSDEND") {
                        jQuery("label[for='lblHeaderRPT'").html($('#hdnDayEndRep').val());
                        $('#hdnpageCode').val(pagecode);
                        break;
                    }
                    if (pagecode == "POSSDSEN") {
                        jQuery("label[for='lblHeaderRPT'").html($('#hdnSalesReturns').val());
                        $('#hdnpageCode').val(pagecode);
                        break;
                    }
                    if (pagecode == "POSCINV") {
                        jQuery("label[for='lblHeaderRPT'").html($('#hdnCurrentInventory').val());
                        $('#lblreportToDate').hide();

                        $('#hdnpageCode').val(pagecode);
                        jQuery("label[for='lblFromDt'").html($('#hdnAsonDate').val());
                        break;
                    }
                    if (pagecode == "POSRPTICSA") {
                        $('#lblreportFromDate').hide();
                        $('#lblreportToDate').hide();
                        $('#lblreportSearchBy').hide();
                        jQuery("label[for='lblHeaderRPT'").html($('#hdnItemCategorySalesAnalysis').val());//"Item Category Sales Analysis");
                        $('#hdnpageCode').val(pagecode);
                        break;
                    }
                    if (pagecode == "POSRPTBAR") {
                        $('#lblreportFromDate').hide();
                        $('#lblreportToDate').hide();
                        $('#lblreportSearchBy').hide();
                        jQuery("label[for='lblHeaderRPT'").html($('#hdnBucketAnalysis').val());//"Bucket Analysis Report");
                        $('#hdnpageCode').val(pagecode);
                        break;
                    }
                    if (pagecode == "POSRPTWDRR") {
                        $('#lblreportFromDate').hide();
                        $('#lblreportToDate').hide();
                        $('#lblreportSearchBy').hide();
                        jQuery("label[for='lblHeaderRPT'").html($('#hdnWeekDaysRevenue').val());//"Week Days Revenue");
                        $('#hdnpageCode').val(pagecode);
                        break;
                    }
                    if (pagecode == "POSRPTLWATV") {
                        $('#lblreportFromDate').hide();
                        $('#lblreportToDate').hide();
                        $('#lblreportSearchBy').hide();
                        $('#lblreportSubOrganization').hide();
                        jQuery("label[for='lblHeaderRPT'").html($('#hdnLocationWiseAvgTransactionValue').val());//"Location Wise Avg Transaction Value");
                        $('#hdnpageCode').val(pagecode);
                        break;
                    }
                    if (pagecode == "POSRPTICPA") {
                        $('#lblreportFromDate').hide();
                        $('#lblreportToDate').hide();
                        $('#lblreportSubOrganization').show();
                        $('#lblreportSearchBy').hide();
                        jQuery("label[for='lblHeaderRPT'").html($('#hdnItemCatgPrftAnlys').val());//"Item Category Profit Analysis");
                        $('#hdnpageCode').val(pagecode);
                        break;
                    }
                    if (pagecode == "POSRPTLWPAS") {
                        $('#lblreportFromDate').hide();
                        $('#lblreportToDate').hide();
                        $('#lblreportSubOrganization').hide();
                        $('#lblreportSearchBy').hide();
                        jQuery("label[for='lblHeaderRPT'").html($('#hdnLocwiseProfitAnlys').val());// wise Profit Analysis");
                        $('#hdnpageCode').val(pagecode);
                        break;
                    }
                    if (pagecode == "POSTVRR") {
                        $('#lblreportFromDate').show();
                        $('#lblreportToDate').show();
                        $('#lblPeriod').show();
                        $('#lblreportSearchBy').hide();

                        $('#lblreportSearchBy').hide();
                        jQuery("label[for='lblHeaderRPT'").html($('#hdnVATReturns').val());// wise Profit Analysis");

                        $('#hdnpageCode').val(pagecode);
                    } 
                }
                else {
                    window.open('DashBoard.html', '_self', 'location=no');
                }
            }
        }
    });
}
function setLabels2(labelData) {
    window.localStorage.setItem('ctCheck', 1);
    jQuery("label[for='lblSubcription'").html(labelData["Renewal"]);
    $('#hdnImageSave').val(labelData["ImageSave"]);
    $('#hdnSelImage').val(labelData["SelUserImage"]);
    jQuery("label[for='lblUpload'").html(labelData["upload"]);
    jQuery("label[for='lblLocationwiseProfitAnalysis'").html(labelData["LocwiseProfitAnlys"]);
    jQuery("label[for='lblItemCategoryProfitAnalysis'").html(labelData["ItemCatgPrftAnlys"]);
    jQuery("label[for='lblSCPopPwd'").html(labelData["pSettings"]);
    jQuery("label[for='lblLanguage'").html(labelData["lang"]);
    jQuery("label[for='lblDateFormat'").html(labelData["format"]);
    jQuery("label[for='lblPopOk'").html(labelData["ok"]);
    jQuery("label[for='lblPopClose'").html(labelData["cancel"])
    jQuery("label[for='lblSettings'").html(labelData["settings"]);
    jQuery("label[for='lblThemes'").html(labelData["themes"]);
    jQuery("label[for='lblTheme1'").html(labelData["theme1"]);
    jQuery("label[for='lblTheme2'").html(labelData["theme2"]);
    jQuery("label[for='lblTheme3'").html(labelData["theme3"]);
    jQuery("label[for='lblTheme4'").html(labelData["theme4"]);
    jQuery("label[for='lblChangePassword'").html(labelData["changePassword"]);

    jQuery("label[for='lblConfigurators'").html(labelData["configurators"]);
jQuery("label[for='lblSubConfigurators'").html(labelData["POSEwalletConSts"]);
jQuery("label[for='lblInvoiceSummary'").html(labelData["category"]);
    jQuery("label[for='lblPrintCustomize'").html(labelData["printCustamize"]);
    jQuery("label[for='lblBarcodeGenerator'").html(labelData["barCodeGenerator"]);
    jQuery("label[for='lblMasters'").html(labelData["masters"]);
    jQuery("label[for='lblTillType'").html(labelData["tillType"]);
    jQuery("label[for='lblTill'").html(labelData["till"]);
    jQuery("label[for='lblTillPermissions'").html(labelData["tillPermissions"]);
    jQuery("label[for='lblCustomer'").html(labelData["customer"]);
    jQuery("label[for='lblTenderType'").html(labelData["tenderType"]);
    jQuery("label[for='lblTransactions'").html(labelData["transactions"]);
    jQuery("label[for='lblInvoce'").html(labelData["invoiceMenu"]);
    jQuery("label[for='lblSalesReturn'").html(labelData["salesReturn"]);
    jQuery("label[for='lblPosting'").html(labelData["posting"]);
    jQuery("label[for='lblTillOpening'").html(labelData["tillOpening"]);
    jQuery("label[for='lblTillClosing'").html(labelData["tillClosing"]);
    jQuery("label[for='lblAccountPosting'").html(labelData["accountPosting"]);
    jQuery("label[for='lblReports'").html(labelData["reports"]);
    jQuery("label[for='lblInvoiceListing'").html(labelData["invoiceListing"]);
    jQuery("label[for='lblTillInvoiceListing'").html(labelData["tillInvoiceListing"]);
    jQuery("label[for='lblTenderTypeWiseCollectionDailySummary'").html(labelData["tenderTypeWiseCollectionDailySummary"]);
    jQuery("label[for='lblSalesReturns'").html(labelData["salesReturns"]);
    jQuery("label[for='lblCurrentInventory'").html(labelData["CurrentInventory"]);

    jQuery("label[for='lblPrimeInformation'").html(labelData["primeInformation"]);
    jQuery("label[for='lblSubOrganization'").html(labelData["subOrganization"]);
    jQuery("label[for='lblFromDt'").html(labelData["fromDate"]);
    jQuery("label[for='lblToDt'").html(labelData["toDate"]);
    jQuery("label[for='lblSearchBy'").html(labelData["searchHere"]);
    jQuery("label[for='lblGenerateReport'").html(labelData["alert4"]);
    jQuery("label[for='lblLoyaltyPoints'").html(labelData["LoyaltyPoints"]);
    jQuery("label[for='lblDispatch'").html(labelData["Dispatch"]);
    $('#txtCode').attr('placeholder', labelData["disc"]);
    $('#hdnInvoiceListing').val(labelData["invoiceListing"]);
    $('#hdnInvDetails').val(labelData["category"]);
    $('#hdnTillInvoiceListing').val(labelData["tillInvoiceListing"]);
    $('#hdnCustLedger').val(labelData["PosCustLdger"]);
    $('#hdnTenderTypeWiseCollectionDailySummary').val(labelData["tenderTypeWiseCollectionDailySummary"]);
    $('#hdnSalesReturns').val(labelData["salesReturns"]);
    $('#hdnCurrentInventory').val(labelData["CurrentInventory"]);
    $('#hdnAccountPosting').val(labelData["accountPosting"]);
    $('#hdnVATReturns').val(labelData["VATReturns"]);
    $('#hdnSalsGrowth').val(labelData["alert1"]);
    $('#hdnDayEndRep').val(labelData["alert2"]);
    $('#hdnChPwdSaveSucc').val(labelData["saveSuccess"]);
    $('#hdnThameChange').val(labelData["alert3"]);
    $('#hdnNotAssignCountrToTrans').val(labelData["permissionsAssisted"]);
    $('#hdnDefault').val(labelData["default1"]);
    $('#hdnAsonDate').val(labelData["AsonDate"]);
    $('#hdnItemCategorySalesAnalysis').val(labelData["ItemCategorySalesAnalysis"])
    $('#hdnBucketAnalysis').val(labelData["BucketAnalysis"])
    $('#hdnWeekDaysRevenue').val(labelData["WeekdaysRevenue"])
    $('#hdnLocationWiseAvgTransactionValue').val(labelData["LocationwiseAverageInvoiceValue"])

    $('#hdnLocwiseProfitAnlys').val(labelData["LocwiseProfitAnlys"]);
    $('#hdnItemCatgPrftAnlys').val((labelData["ItemCatgPrftAnlys"]));

    jQuery("label[for='lblAnalyticalReports'").html(labelData["AnalyticalReports"]);
    jQuery("label[for='lblItemCategorySalesAnalysis'").html(labelData["ItemCategorySalesAnalysis"]);
    jQuery("label[for='lblWeekDaysRevenue'").html(labelData["WeekdaysRevenue"]);
    jQuery("label[for='lblBucketAnalysis'").html(labelData["BucketAnalysis"]);
    jQuery("label[for='lblLocationwiseAverageInvoiceValue'").html(labelData["LocationwiseAverageInvoiceValue"]);
    jQuery("label[for='lblItem'").html(labelData["item"]);
    jQuery("label[for='lblReceipt'").html(labelData["receipt"]);
    jQuery("label[for='lblVATReturns'").html(labelData["VATReturns"]);
    jQuery("label[for='lblUserPermissions'").html(labelData["user"]);
    jQuery("label[for='lblPrintPole'").html(labelData["PrintPoleConfig"]);
    jQuery("label[for='lblCurrentInventory'").html(labelData["CurrentInventory"]);
    jQuery("label[for='lblPeriod'").html(labelData["VPeriod"]);

$('#ddlQ1').val(labelData["VQOnes"]);
$('#ddlQ2').val(labelData["VQTwos"]);
$('#ddlQ3').val(labelData["VQThrees"]);
$('#ddlQ4').val(labelData["VQFour"]);
$('#hdnMExcel').val(labelData["Excel"]);
LoadQuarter();   
try
{
    jQuery("label[for='lblCustLDGR'").html(labelData["PosCustLdger"]);
}
catch(a)
{

}
}
function setLabels8(labelData, pagecode) {

    window.localStorage.setItem('ctCheck', 1);
    if (pagecode == "POSINLS")
        $('#lblNavigationUrl').text(labelData["pos"] + "  >>  " + labelData["reports"] + "  >>  " + labelData["invoiceListing"]);
    else if (pagecode == "POSTINL")
        $('#lblNavigationUrl').text(labelData["pos"] + "  >>  " + labelData["reports"] + "  >>  " + labelData["tillInvoiceListing"]);
    else if (pagecode == "POSTTDS")
        $('#lblNavigationUrl').text(labelData["pos"] + "  >>  " + labelData["reports"] + "  >>  " + labelData["tenderTypeWiseCollectionDailySummary"]);
    else if (pagecode == "POSSDSEN")
        $('#lblNavigationUrl').text(labelData["pos"] + "  >>  " + labelData["reports"] + "  >>  " + labelData["salesReturns"]);
    else if (pagecode == "POSCINV")
        $('#lblNavigationUrl').text(labelData["pos"] + "  >>  " + labelData["reports"] + "  >>  " + labelData["CurrentInventory"]);

    else if (pagecode == "POSRPTICSA")
        $('#lblNavigationUrl').text(labelData["pos"] + "  >>  " + labelData["AnalyticalReports"] + "  >>  " + labelData["ItemCategorySalesAnalysis"]);
    else if (pagecode == "POSRPTWDRR")
        $('#lblNavigationUrl').text(labelData["pos"] + "  >>  " + labelData["AnalyticalReports"] + "  >>  " + labelData["WeekdaysRevenue"]);
    else if (pagecode == "POSRPTBAR")
        $('#lblNavigationUrl').text(labelData["pos"] + "  >>  " + labelData["AnalyticalReports"] + "  >>  " + labelData["BucketAnalysis"]);
    else if (pagecode == "POSRPTLWATV")
        $('#lblNavigationUrl').text(labelData["pos"] + "  >>  " + labelData["AnalyticalReports"] + "  >>  " + labelData["LocationwiseAverageInvoiceValue"]);
    else if (pagecode == "POSRPTICPA")
        $('#lblNavigationUrl').text(labelData["pos"] + "  >>  " + labelData["AnalyticalReports"] + "  >>  " + labelData["ItemCatgPrftAnlys"]);
    else if (pagecode == "POSRPTLWPAS")
        $('#lblNavigationUrl').text(labelData["pos"] + "  >>  " + labelData["AnalyticalReports"] + "  >>  " + labelData["LocwiseProfitAnlys"]);
    else if (pagecode == "POSACPO")
        //$('#lblNavigationUrl').text(labelData["pos"] + "  >> Reports >> Sales Revenue");
        $('#lblNavigationUrl').text(labelData["pos"] + "  >> " + labelData["reports"] + " >> " + labelData["accountPosting"]);
    else if (pagecode == "POSTVRR")
        $('#lblNavigationUrl').text(labelData["pos"] + "  >> " + labelData["reports"] + " >> " + labelData["VATReturns"]);



}

function getResources4(langCode, pageCode, data, sts) {
    window.localStorage.setItem('ctCheck', 1);
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.getResource,
        data: "langCode=" + langCode + "&pageCode=" + pageCode,
        success: function (resp) {
            setLabels4(resp, data, sts);
            $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());
        }
    });
}
function getLoadExdata()
{
//document.getElementById("FileFormat").options.length = 0;
$("#FileFormat").append($("<option></option>").val("0").html("----select----"));
$("#FileFormat").append($("<option></option>").val("1").html($('#hdnMExcel').val()));
 $("#FileFormat option[value='1']").attr('selected','selected');
}
function LoadQuarter()
{
//document.getElementById("ddPeriod").options.length = 0;
$("#ddPeriod").append($("<option></option>").val("1").html($('#ddlQ1').val()));
$("#ddPeriod").append($("<option></option>").val("2").html($('#ddlQ2').val()));
$("#ddPeriod").append($("<option></option>").val("3").html($('#ddlQ3').val()));
$("#ddPeriod").append($("<option></option>").val("4").html($('#ddlQ4').val()));
 
}

function setLabels4(labelData, data, sts) {
    window.localStorage.setItem('ctCheck', 1);
    jQuery("label[for='lblTitle']").html(labelData['till'] + " " + labelData["invoiceListing"]);
    jQuery("label[for='lblTillInvoiceListingReport']").html(labelData['till'] + " " + labelData["invoiceListing"]);
    jQuery("label[for='lbl_page']").html(labelData["page"]);
    jQuery("label[for='lblMail']").html(labelData["Mail"]);
    jQuery("label[for='ToMail']").html(labelData["POSTo"]);
    jQuery("label[for='Bcc']").html(labelData["BCC"]);
    jQuery("label[for='lblFormat']").html(labelData["format"]);
    jQuery("label[for='lblBody']").html(labelData["body"]);
    jQuery("label[for='lblsend']").html(labelData["Send"]);
    jQuery("label[for='lblClose']").html(labelData["Close"]);
    //$('#hdnInvoiceListingReport').val(labelData['invoiceListing']);
    $('#hdnInvoiceNo').val(labelData['invoice']);
    $('#hdnInvoiceDt').val(labelData['date']);
    $('#hdnCustomer').val(labelData['customer']);
    $('#hdnTill').val(labelData['till']);
    $('#hdnCashier').val(labelData['cashier']);
    $('#hdnBaseTot').val(labelData['total']);
    $('#hdnTaxTot').val(labelData['tax']);
    $('#hdnNetTot').val(labelData['netTotal']);
    $('#hdnItemCode').val(labelData['item'] + " " + labelData['code']);
    $('#hdnItemName').val(labelData['name']);
    $('#hdnUOM').val(labelData['uom']);
    $('#hdnQty').val(labelData['qty']);
    $('#hdnPrice').val(labelData['price']);
    $('#hdnDisc').val(labelData['disc']);
    $('#hdnAmount').val(labelData['amount']);
    $('#hdnTenderType').val(labelData['tenderType']);
    $('#hdnChPwdSaveSucc').val(labelData["saveSuccess"]);
    $('#hdnThameChange').val(labelData["alert1"]);
    $('#hdnNotAssignCountrToTrans').val(labelData["permissionsAssisted"]);
    $('#hdnSalesTo').val(labelData['POSTo']);
    $('#hdnImageSave').val(labelData["ImageSave"]);
    $('#hdnSelImage').val(labelData["SelUserImage"]);
    $('#hdnInvNo').val(labelData["TotalInv"]);
    $('#hdnToolPrint').val(labelData["print"]);
    jQuery("label[for='lblUpload']").html(labelData["upload"]);
    jQuery("label[for='EMExcel']").text(labelData["Excel"]);
    $('#hdnMExcel').val(labelData["Excel"]);
    
    arrangeTillPageInvoiceListingRPBind(data);
    if (sts == 2) {
        var $el = $('#tbldiv tbody>tr.tt-hide');
        $el.removeClass("tt-hide");

        for (var i = 0; i < $el.length; i++) {
            $('#root_' + i + '').html("-");
        }
        $('#DivPager').css("display", "none");
       
    }
    if (window.localStorage.getItem('RPfrmDate') != null) {
        $('#INVLISlblDate').text('(' + ConvertDateAndReturnDate($('#hdnDateFromate').val(), window.localStorage.getItem('RPfrmDate')) + ' ' + $('#hdnSalesTo').val() + ' ' + ConvertDateAndReturnDate($('#hdnDateFromate').val(), window.localStorage.getItem('RPtoDate')) + ')');
    }

}

function getResources5(langCode, pageCode, data, sts) {
    window.localStorage.setItem('ctCheck', 1);
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.getResource,
        data: "langCode=" + langCode + "&pageCode=" + pageCode,
        success: function (resp) {
            setLabels5(resp, data, sts);
            $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());
        }
    });
}
function ConvertDateAndReturnDate(formates, dateText) {
    window.localStorage.setItem('ctCheck', 1);
    var hdnformat = formates;
    var txtMainDate = '';
    var splitc;
    if (hdnformat == 'd/m/Y' || hdnformat == 'm/d/Y' || hdnformat == 'Y/d/m' || hdnformat == 'Y/m/d' || hdnformat == 'M/d/Y' || hdnformat == 'd/M/Y') {
        splitc = '/';
    }
    else if (hdnformat == 'd-m-Y' || hdnformat == 'm-d-Y' || hdnformat == 'Y-d-m' || hdnformat == 'Y-m-d' || hdnformat == 'M-d-Y' || hdnformat == 'd-M-Y') {
        splitc = '-';
    }
    else if (hdnformat == 'd,m,Y' || hdnformat == 'm,d,Y' || hdnformat == 'Y,d,m' || hdnformat == 'Y,m,d' || hdnformat == 'M,d,Y' || hdnformat == 'd,M,Y') {

        splitc = ',';
    }
    else if (hdnformat == 'd m Y' || hdnformat == 'm d Y' || hdnformat == 'Y d m' || hdnformat == 'Y m d' || hdnformat == 'M d Y' || hdnformat == 'd M Y') {

        splitc = ' ';
    }
    else if (hdnformat == 'd.m.Y' || hdnformat == 'm.d.Y' || hdnformat == 'Y.d.m' || hdnformat == 'Y.m.d' || hdnformat == 'M.d.Y' || hdnformat == 'd.M.Y') {
        splitc = '.';
    };
    var sym = '-';
    if (dateText.indexOf('/') != -1)
        sym = '/';
    var vardatesplit = dateText.split(sym);
    var varM;
    var varMn;
    var varfformatS = hdnformat.split(splitc);
    if (varfformatS[0] == 'M' || varfformatS[1] == 'M') {

        if (vardatesplit[1] != null)
            varM = vardatesplit[1];
        if (vardatesplit[0] != null)
            varM = vardatesplit[0];

        switch (varM) {
            case 'Jan': varMn = '01';
                break;
            case 'Feb': varMn = '02';
                break;
            case 'Mar': varMn = '03';
                break;
            case 'Apr': varMn = '04';
                break;
            case 'May': varMn = '05';
                break;
            case 'Jun': varMn = '06';
                break;
            case 'Jul': varMn = '07';
                break;
            case 'Aug': varMn = '08';
                break;
            case 'Sep': varMn = '09';
                break;
            case 'Oct': varMn = '10';
                break;
            case 'Nov': varMn = '11';
                break;
            case 'Dec': varMn = '12';
                break;
        }

    }
    if (varfformatS[0] == 'd' && (varfformatS[1] == 'm' || varfformatS[1] == 'M') && varfformatS[2] == 'Y') {
        if (varfformatS[1] == 'M') {
            txtMainDate = varMn + splitc + vardatesplit[1] + splitc + vardatesplit[0];
        }
        else {
            txtMainDate = vardatesplit[2] + splitc + vardatesplit[1] + splitc + vardatesplit[0];
        }
    }
    else if ((varfformatS[0] == 'm' || varfformatS[0] == 'M') && varfformatS[1] == 'd' && varfformatS[2] == 'Y') {
        if (varfformatS[0] == 'M') {
            txtMainDate = varMn + splitc + vardatesplit[2] + splitc + vardatesplit[0];
        }
        else {
            txtMainDate = vardatesplit[1] + splitc + vardatesplit[2] + splitc + vardatesplit[0];
        }
    }
    else if (varfformatS[0] == 'Y' && varfformatS[1] == 'd' && varfformatS[2] == 'm') {
        txtMainDate = vardatesplit[0] + splitc + vardatesplit[2] + splitc + vardatesplit[1];
    }
    else if (varfformatS[0] == 'Y' && varfformatS[1] == 'm' && varfformatS[2] == 'd') {
        txtMainDate = vardatesplit[0] + splitc + vardatesplit[1] + splitc + vardatesplit[2];
    }
    else {
        txtMainDate = vardatesplit[0] + splitc + vardatesplit[1] + splitc + vardatesplit[2];
    }
    return txtMainDate;
}
function setLabels5(labelData, data, sts) {
    window.localStorage.setItem('ctCheck', 1);
    $('#hdnImageSave').val(labelData["ImageSave"]);
    $('#hdnSelImage').val(labelData["SelUserImage"]);
    jQuery("label[for='lblUpload'").html(labelData["upload"]);
    jQuery("label[for='lblTitle'").html(labelData["invoiceListing"]);
    jQuery("label[for='lblInvoiceListingReport'").html(labelData["invoiceListing"]);
    //$('#hdnInvoiceListingReport').val(labelData['invoiceListing']);
    jQuery("label[for='lbl_page'").html(labelData["page"]);
    $('#hdnof').val(labelData["of"]);
    jQuery("label[for='lblMail'").html(labelData["Mail"]);
    jQuery("label[for='ToMail'").html(labelData["POSTo"]);
    jQuery("label[for='Bcc'").html(labelData["BCC"]);
    jQuery("label[for='lblFormat'").html(labelData["format"]);
    jQuery("label[for='lblBody'").html(labelData["body"]);
    jQuery("label[for='lblsend'").html(labelData["Send"]);
    jQuery("label[for='lblClose'").html(labelData["Close"]);
    $('#hdnInvoiceNo').val(labelData['invoice']);
    $('#hdnInvoiceDt').val(labelData['date']);
    $('#hdnCustomer').val(labelData['customer']);
    $('#hdnTill').val(labelData['till']);
    $('#hdnCashier').val(labelData['cashier']);
    $('#hdnBaseTot').val(labelData['total']);
    $('#hdnTaxTot').val(labelData['tax']);
    $('#hdnNetTot').val(labelData['netTotal']);
    $('#hdnItemCode').val(labelData['item'] + ' ' + labelData['code']);
    $('#hdnItemName').val(labelData['name']);
    $('#hdnUOM').val(labelData['uom']);
    $('#hdnQty').val(labelData['qty']);
    $('#hdnPrice').val(labelData['price']);
    $('#hdnDisc').val(labelData['disc']);
    $('#hdnAmount').val(labelData['amount']);
    $('#hdnTenderType').val(labelData['tenderType']);
    $('#hdnChPwdSaveSucc').val(labelData["saveSuccess"]);
    $('#hdnThameChange').val(labelData["alert1"]);
    $('#hdnNotAssignCountrToTrans').val(labelData["permissionsAssisted"]);
    $('#hdnDefault').val("");
    $('#hdnSalesTo').val(labelData['POSTo']);
    $('#hdnSales').val(labelData["alert12"]);
    $('#hdnLineView').val(labelData["alert2"]);
    $('#hdnTotalInvoice').val(labelData["total"] + ' ' + labelData['invoice']);
    $('#hdnEnterMail').val(labelData["enterMail"]);
    $('#hdnMailSuccess').val(labelData["MailSuccess"]);
    $('#hdnMailnotSuccess').val(labelData["MailnotSuccess"]);
    $('#hdnInvalidData').val(labelData["InvalidData"]);
    $('#hdnInvNo').val(labelData["TotalInv"]);
    $('#hdnToolPrint').val(labelData["print"]);
    jQuery("label[for='EMExcel']").text(labelData["Excel"]);
    $('#hdnMExcel').val(labelData["Excel"]);
    getLoadExdata();
    arrangementInvoiceListingRPBind(data);

    if (sts == 2) {
        // arrangementInvoiceListingExcelBind(data);

        var $el = $('#tbldiv tbody>tr.tt-hide');
        $el.removeClass("tt-hide");

        for (var i = 0; i < $el.length; i++) {
            $('#root_' + i + '').html("-");
        }
        $('#DivPager').css("display", "none");
        if ($('#hdnScreenmode').val() == "print") {
            arrangementInvoiceListingRPBind(data);
            var divContents = document.getElementById("div_grid").innerHTML;
            var printWindow = window.open('', '', 'height=200,width=450');
            printWindow.document.write('<html><head><title>Invoice Listing</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(divContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();

        }
        if ($('#hdnScreenmode').val() == "excel") {
            arrangementInvoiceListingRPBind(data);
            var data_type = 'data:application/vnd.ms-excel';
            var table_div = document.getElementById('tbldiv');
            var table_html = table_div.outerHTML.replace(/ /g, '%20');
            var a = document.createElement('a');
            a.href = data_type + ', ' + table_html;
            a.download = 'Invoice-Listing' + '.xls';
            a.click();
            arrangementInvoiceListingRPBind(data);
        }

    }

    if (window.localStorage.getItem('RPfrmDate') != null) {
        $('#INVLISlblDate').text('(' + ConvertDateAndReturnDate($('#hdnDateFromate').val(), window.localStorage.getItem('RPfrmDate')) + ' ' + $('#hdnSalesTo').val() + ' ' + ConvertDateAndReturnDate($('#hdnDateFromate').val(), window.localStorage.getItem('RPtoDate')) + ')');
    }
}
function loadGraphDataTINV() {
    window.localStorage.setItem('ctCheck', 1);
    $('#li-Excel i').attr('style', 'color:#919598 !important');
    $('#li-Excel').attr('onclick', 'return false');
    $('#li-Refresh i').attr('style', 'color:#919598 !important');
    $('#li-Refresh').attr('onclick', 'return false');
    $('#li_expand i').attr('style', 'color:#919598 !important');
    $('#li_expand').attr('onclick', 'return false');
    $('#li_compress i').attr('style', 'color:#919598 !important');
    $('#li_compress').attr('onclick', 'return false');
    $('#li_Print i').attr('style', 'color:#919598 !important');
    $('#li_Print').attr('onclick', 'return false');
    $('#txt_PRPPageIndex').attr('disabled', 'true');
    $('#btnPage').attr('disabled', 'true');
    $('#li_PRFirst i').attr('style', 'color:#919598 !important');
    $('#li_PRFirst').attr('onclick', 'return false');
    $('#li_PRNext i').attr('style', 'color:#919598 !important');
    $('#li_PRNext').attr('onclick', 'return false');
    $('#li_PRPrev i').attr('style', 'color:#919598 !important');
    $('#li_PRPrev').attr('onclick', 'return false');
    $('#li_PRLast i').attr('style', 'color:#919598 !important');
    $('#li_PRLast').attr('onclick', 'return false');
    $('#lbl_invPageOf').attr('style', 'color:#919598 !important');
    $('#lbl_page').attr('style', 'color:#919598 !important');

    $('#txt_PRPPageIndex').val('');
    var param = { CounterId: window.localStorage.getItem('COUNTERID'), ShiftId: window.localStorage.getItem('SHIFTID'), SalespersonId: window.localStorage.getItem('CashierID') };
    var color = Chart.helpers.color;
    var barChartData = '';
    var GrColorInv = '';
    if (window.localStorage.getItem("Theam").split('-')[3] == '2.css') { GrColorInv = '#ffffff' } else { GrColorInv = '#000' }
    var elemGrid = document.getElementById("div_grid");
    elemGrid.style.display = "none";

    var elemGraph = document.getElementById("div_graph1");
    elemGraph.style.display = "block";

    var resp = graphData1;

    var x = []; var y = [];


    for (var i = 0; i < resp.length; i++) {
        x.push(resp[i].C1.split('/')[1]);
        y.push(parseFloat(resp[i].C2).toFixed(2));
    }

    barChartData = {
        labels: x,
        datasets: [{
            label: $('#hdnSales').val(),
            backgroundColor: ["#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276"], //color(window.chartColors.red).alpha(0.5).rgbString(),
            borderColor: ["#ff7276", "#ff999c", "#ffb3b5", "#ffccce", "#ffe6e6", "#ff7276", "#ff7276", "#ff7276"],//window.chartColors.red,
            borderWidth: 1,
            data: y
        },
        {
            type: 'line',
            label: $('#hdnLineView').val(),
            borderColor: window.chartColors.red,
            borderWidth: 2,
            fill: false,
            data: y,

        }
        ]
    }

    var ctx = document.getElementById("canvas").getContext("2d");
    window.myBar = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            legend: {
                labels: {
                    fontColor: GrColorInv,
                    fontSize: 12
                }
            },
            responsive: true,
            title: {
                display: true,
                text: ''
            },
            scales: {
                yAxes: [{
                    ticks: {
                        min: 0,
                        // stepSize: 1,
                        fontColor: GrColorInv,
                        fontSize: 14
                    }
                    ,
                    gridLines: {
                        color: GrColorInv
                        //    lineWidth: 2,
                        //    zeroLineColor: "#000",
                        //    zeroLineWidth: 2
                    },
                    stacked: true
                }],
                xAxes: [{
                    ticks: {
                        autoSkip: false,
                        maxRotation: 60,
                        minRotation: 60,
                        fontColor: GrColorInv,
                        fontSize: 14
                    },
                    gridLines: {
                        color: GrColorInv,
                        lineWidth: 2
                    }
                }]
            }
        }
    });
}

