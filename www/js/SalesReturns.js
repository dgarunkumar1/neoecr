﻿$("#divNavBarRight").attr('style', 'display:block;');
//Page Load Function


function MainCounterData() {

    window.localStorage.setItem('ctCheck', 1);
    GetLoginUserName();
    defaultdata();
    $('#hdnPageIndex').val('1');
    $('#hdnFilterType').val('Summary');
    GetSummaryGridData('Summary');
    GetCounterType();
    $('#lblLastUpdate').css("display", "none");
    var HashVal = (window.localStorage.getItem("hashValues"));
    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');
        $('#hdnDateFromate').val(HashValues[2])
        if (HashValues[7] == 'ar-SA') {
            $('html,body').append('<link rel="stylesheet" href="Styles/css/bootstrap-rtl.css" type="text/css" />');
            $('html,body').append('<link rel="stylesheet" href="Styles/css/bootstrap-flipped.css" type="text/css" />');
            $('head').append('<script src="scripts/bootstrap-dialog-SA.min.js"></script>');
        }

    }
}

function VD() {
    window.localStorage.setItem('ctCheck', 1);
    var vd_ = window.localStorage.getItem("UUID");
    return vd_;
}
$("#btnCreate").click(function () {
    window.localStorage.setItem('ctCheck', 1);
    $("#hdnTransId").val('');
    ClearTotalScreen();
    $("#hdnMode").val('New');
    EnableFormfileds(false);
    ShowMenuModes($("#hdnMode").val());
    var CurDate = new Date();
    var DateWithFormate = CurDate.getFullYear() + '-' + ((CurDate.getMonth() + 1) < 10 ? '0' : '') + (CurDate.getMonth() + 1) + '-' + (CurDate.getDate() < 10 ? '0' : '') + CurDate.getDate();
    ConvertDateWithFormat($('#hdnDateFromate').val(), DateWithFormate, "calDocumentDate");
    $('#lblLastUpdate').css("display", "none");
    $('#trSBInnerRow li span#lblLastUpdatedUser').html("");
    $('#trSBInnerRow li span#lblLastUpdatedDate').html("");
    $('#trSBInnerRow li span#lblMode').html($('#hdnNew').val());
    $("#btnSaveApprove").attr('style', 'display:block;');
    $("#btnApprove").attr('style', 'display:none;');
    $("#btnReject").attr('style', 'display:none;');
    $('#PrimeInfo').addClass('open');
    $('#ShippingInfo').removeClass('open');
    $('#divPrimeInformation').attr('style', 'display:block');
    $('#divShippingInformation').attr('style', 'display:none');
    $("#divNavBarRight").attr('style', 'display:none;');

});
$("#btnCancel").click(function () {
    window.localStorage.setItem('ctCheck', 1);
    $("#btnCreate").attr('style', 'display:block;');
    $("#ShowMenuModes").attr('style', 'display:none;');
    $("#SummaryGrid").attr('style', 'display:block;');
    $("#NewPage").attr('style', 'display:none;');
    $("#hdnTransId").val('');
    $("#hdnMode").val('Cancel');
    ShowMenuModes($("#hdnMode").val());
    $('#lblLastUpdate').css("display", "none");
    $('#trSBInnerRow li span#lblLastUpdatedUser').html("");
    $('#trSBInnerRow li span#lblLastUpdatedDate').html("");
    $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());
    $("#divNavBarRight").attr('style', 'display:block;');
    GetSummaryGridData('Summary');
});
$("#btnEdit").click(function () {
    $("#hdnMode").val('Update');
    window.localStorage.setItem('ctCheck', 1);
    $("#hdnMode").val('Edit');
    EnableFormfileds(false);
    ShowMenuModes($("#hdnMode").val());
    $('#trSBInnerRow li span#lblMode').html($('#hdnEdit').val());
    $('#PrimeInfo').addClass('open');
    $('#ShippingInfo').removeClass('open');
    $('#divPrimeInformation').attr('style', 'display:block');
    $('#divShippingInformation').attr('style', 'display:none');
    $("#divNavBarRight").attr('style', 'display:none;');

})

//lnklogout

//For Invoice Link
function GetShiftData() {
    window.localStorage.setItem('ctCheck', 1);
    if (window.localStorage.getItem("CashierID") != null) {

        var SubOrgId = ''; var CashierID = window.localStorage.getItem("CashierID");
        var HashValues = (window.localStorage.getItem("hashValues"));
        if (HashValues != null && HashValues != '') {
            var hashSplit = HashValues.split('~');
            SubOrgId = hashSplit[1];
        }
        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.ShiftCheck,
            data: "CashierID=" + CashierID + "&OrgId=" + SubOrgId + "&VD=" + VD(),
            success: function (resp) {
                var result = resp;
                if (result.length > 0) {
                    var ACTIVE = result[0].ACTIVE;
                    var COUNTERID = result[0].COUNTERID;
                    var COUNTERNAME = result[0].COUNTERNAME;
                    var COUNTERNUMBER = result[0].COUNTERNUMBER;
                    var COUNTERTYPE = result[0].COUNTERTYPE;
                    var End_Time = result[0].End_Time;
                    var Is_Next_Day = result[0].Is_Next_Day;
                    var SHIFTID = result[0].SHIFTID;
                    var Shift_Code = result[0].Shift_Code;
                    var Shift_Name = result[0].Shift_Name;
                    var Start_Time = result[0].Start_Time;
                    var StageAlert = result[0].StageAlert;

                    window.localStorage.setItem('ACTIVE', result[0].ACTIVE);
                    window.localStorage.setItem('COUNTERID', result[0].COUNTERID);
                    window.localStorage.setItem('COUNTERNAME', result[0].COUNTERNAME);
                    window.localStorage.setItem('COUNTERNUMBER', result[0].COUNTERNUMBER);
                    window.localStorage.setItem('COUNTERTYPE', result[0].COUNTERTYPE);
                    window.localStorage.setItem('End_Time', result[0].End_Time);
                    window.localStorage.setItem('Is_Next_Day', result[0].Is_Next_Day);
                    window.localStorage.setItem('SHIFTID', result[0].SHIFTID);
                    window.localStorage.setItem('Shift_Code', result[0].Shift_Code);
                    window.localStorage.setItem('Shift_Name', result[0].Shift_Name);
                    window.localStorage.setItem('Start_Time', result[0].Start_Time);
                    var StageAlert = result[0].StageAlert;
                    if (StageAlert == '1') {
                        if (ACTIVE.toLowerCase() == 'y') {
                            window.open('posmain.html', '_self', 'location=no');
                        }
                        else {

                            navigator.notification.alert($('#hdnNPermitAccInvoice').val(), "", "neo ecr", "Ok");
                        }
                    }
                    else if (StageAlert == '2') {

                        navigator.notification.alert($('#hdnNAssignCountrTrans').val(), "", "neo ecr", "Ok");
                    }
                }
                else {

                    navigator.notification.alert($('#hdnNPermitAccInvoice').val(), "", "neo ecr", "Ok");

                }
            },
            error: function (e) {

                navigator.notification.alert($('#hdnInvalidData').val(), "", "neo ecr", "Ok");

            }
        });
    }
    else {

        navigator.notification.alert($('#hdnPlzContactAdmin').val(), "", "neo ecr", "Ok");
    }
}
function SalesReturnMainData() {
    window.localStorage.setItem('ctCheck', 1);
    GetLoginUserName();
    ClearScreen();
    ClearTotalScreen();
    GetLoginUserName();
    PrintSLLoad();
    var HashVal = (window.localStorage.getItem("hashValues"));
    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');
        getResources(HashValues[7], "POSSalesReturn");
        $('#txtSubOrganization').val(HashValues[17]);
        $('#hdnSubOrganization').val(HashValues[1]);
        $('#hdnParentSubOrganization').val(HashValues[0]);
        $('#hdnDateFromate').val(HashValues[2]);
        GetSubOrgNameWithCur(HashValues[1]);
        $('#hdnPaging').val(HashValues[19]);
        $('#hdnDateFromate').val(HashValues[2]);
        $('#calDocumentDate').DatePicker({
            format: $('#hdnDateFromate').val(),
            changeMonth: 'true',
            changeYear: 'true',
        });
        $('#calRefInvoiceDate').DatePicker({
            format: $('#hdnDateFromate').val(),
            changeMonth: 'true',
            changeYear: 'true',
        });
    }
    GetSummaryGridData('Summary');
    EntryGridSettings(5);
    $('#myModalprint').hide();
    $('#lblLastUpdate').css("display", "none");
    var HashVal = (window.localStorage.getItem("hashValues"));
    window.localStorage.setItem('ctCheck', 1);
    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');
        $('#hdnDateFromate').val(HashValues[2])
        if (HashValues[7] == 'ar-SA') {
            $('html,body').append('<link rel="stylesheet" href="Styles/css/bootstrap-rtl.css" type="text/css" />');
            $('html,body').append('<link rel="stylesheet" href="Styles/css/bootstrap-flipped.css" type="text/css" />');
            $('head').append('<script src="scripts/bootstrap-dialog-SA.min.js"></script>');
        }
    }

}
//Return Type RadioButton Checked
$('#rdobtnReplace').change(function () {
    window.localStorage.setItem('ctCheck', 1);
    document.getElementById('rdobtnCash').checked = false;
    document.getElementById('rdobtnReplace').checked = true;
    if ($("#rdobtnReplace").prop('checked') == true) {
        $('#tableSalesRetunItems tr').each(function () {
            var RID = $(this).attr('id');
            if (RID != undefined) {
                $("#txtReturnQty" + RID).removeAttr("disabled");
            }
        });
    }
});
$('#rdobtnCash').change(function () {
    window.localStorage.setItem('ctCheck', 1);
    document.getElementById('rdobtnCash').checked = true;
    document.getElementById('rdobtnReplace').checked = false;
    if ($("#rdobtnCash").prop('checked') == true) {
        $('#tableSalesRetunItems tr').each(function () {
            var RID = $(this).attr('id');
            if (RID != undefined) {
                $("#txtReturnQty" + RID).attr("disabled", "disabled");
            }
        });
        CalculateTotalAmt();
    }
    EntryGridSettings(5);
    GetSalesReturnInvoiceItems($('#hdnRefInvoiceNo').val());
    if (copyItems != null || copyItems == '') {
        if (copyItems.length > 0) {
            for (var l = 0; l < copyItems.length; l++) {
                SummaryTable += "<tr id=\"" + l + "\" onmouseup=MouseOver(\"" + l + "\") ><td><input type='hidden' id=\"hdnItem" + l + "\" value=\"" + copyItems[l].ItemId + "\"/><input type='text'  id=\"txtItem" + l + "\"  value=\"" + copyItems[l].ItemName + "\" style='width:100%;height:30px;border:0px;' disabled/></td><td><input type='hidden' id=\"hdnUnit" + l + "\" value=\"" + copyItems[l].UnitId + "\"/><input type='text'  id=\"txtUom" + l + "\" value=\"" + copyItems[l].UnitName + "\" style='width:100%;height:30px;border:0px;' disabled/></td><td><input type='text'  id=\"txtRefQty" + l + "\" value=\"" + parseFloat(copyItems[l].InvItemQty).toFixed(2) + "\" style='width:100%;height:30px;border:0px;text-align:right;' disabled/></td><td><input type='text'  id=\"txtPrice" + l + "\" value=\"" + parseFloat(copyItems[l].Price).toFixed(2) + "\" style='width:100%;height:30px;border:0px;text-align:right;' disabled/></td><td><input type='text'  id=\"txtDiscAmt" + l + "\" value=\"" + parseFloat(copyItems[l].ItemDiscountPer).toFixed(2) + "\" style='width:100%;height:30px;border:0px;text-align:right;' disabled/><td><input  type='text' id=\"txtReturnQty" + l + "\" style='width:100%;height:30px;border:0px;text-align:right;' value=\"" + parseFloat(copyItems[l].InvItemQty).toFixed(2) + "\"  onkeyup='ReturnQtyChange(\"" + copyItems[l].Price + "\",\"" + copyItems[l].InvItemQty + "\",\"" + copyItems[l].ItemDiscountPer + "\")' disabled/></td><td><input  type='text' id=\"txtReturnAmt" + l + "\" style='width:100%;height:30px;border:0px;text-align:right;' value='0.00' disabled/></td><td><input  type='text' id=\"txtReason" + l + "\" style='width:100%;height:30px;border:0px;' placeholder=''/><input  type='text' id=\"hdnReason" + l + "\" style='width:100%;height:30px;border:0px;'/></td><td style='display:none;'><input  type='text' id=\"txtReturnType" + l + "\" style='width:100%;height:30px;border:0px;'/></td></tr>"
            };
            $('#tableSalesRetunItems tbody').remove();
            SummaryTable += "<tbody>" + SummaryTable + "</tbody>";
            $('#tableSalesRetunItems').append(SummaryTable);
            if (copyItems.length < 5)
                for (copyItems.length; copyItems.length < 5; copyItems.length++) {
                    AddNewRow();
                }
            SRReasonsAutofill(0, copyItems.length);
        }
    }
    if ($("#rdobtnCash").prop('checked') == true) {
        $('#tableSalesRetunItems tr').each(function () {
            var RID = $(this).attr('id');
            if (RID != undefined) {
                $("#txtReturnQty" + RID).attr("disabled", "disabled");
            }
        });
    }
});

//Auto Generation Check And Generate

function GetAutoCodeGen(str) {
    window.localStorage.setItem('ctCheck', 1);
    var icode = '7';
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetAutoGenHashTableCheck,
        data: "SubOrgId=" + str + "&iScreenCode=" + icode + "&VD=" + VD(),
        success: function (resp) {
            var CounterNo = resp;
            if (CounterNo.length == 1) {
                $("#hdnAutoCode").val(CounterNo[0].PageName);
            }
            else {
                $("#hdnAutoCode").val('N');
            }
        },
        error: function (e) {
            $("#hdnAutoCode").val('N');
        }
    });
}
function GenerateCode(strOrgCode, strVarCode, strParamType, strTable, strColumn, USERID, BRANCHID, COMPANYID, LANGUAGE) {
    window.localStorage.setItem('ctCheck', 1);
    var CounterNo_ = "0";
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GenerateCode,
        data: "strOrgCode=" + strOrgCode + "&strVarCode=" + strVarCode + "&strParamType=" + strParamType + "&strTable=" + strTable + "&strColumn=" + strColumn + "&USERID=" + USERID + "&BRANCHID=" + BRANCHID + "&COMPANYID=" + COMPANYID + "&LANGUAGE=" + LANGUAGE + "&VD=" + VD(),
        success: function (resp) {
            var CounterNo = resp;
            if (CounterNo.length > 1) {
                $('#txtSalesReturnNo').val(CounterNo);
                CounterNo_ = CounterNo;
                return CounterNo_;
            }
        },
        error: function (e) {
            CounterNo_ = "0";
        }
    });
    return CounterNo_;
}
//EntryGridSettings
function EntryGridSettings(count) {
    window.localStorage.setItem('ctCheck', 1);
    var SummaryTable = '';
    for (var l = 0; l <= count; l++) {
        SummaryTable += "<tr><td style='text-align:center;'><input type='checkbox'  id=\"chkItem" + l + "\"  onclick='return CheckBoxChecked();' /></td><td><input type='hidden' id=\"hdnItem" + l + "\" /><input type='text'  id=\"txtItem" + l + "\"  style='width:100%;height:30px;border:0px;' disabled/></td><td><input type='hidden' id=\"hdnUnit" + l + "\" /><input type='text'  id=\"txtUom" + l + "\" style='width:100%;height:30px;border:0px;' disabled/></td><td><input type='text'  id=\"txtRefQty" + l + "\" style='width:100%;height:30px;border:0px;text-align:right;' disabled/></td><td><input type='text'  id=\"txtPrice" + l + "\" style='width:100%;height:30px;border:0px;text-align:right;' disabled/></td><td><input type='text'  id=\"txtDiscAmt" + l + "\" style='width:100%;height:30px;border:0px;text-align:right;' disabled/><td><input  type='text' id=\"txtReturnQty" + l + "\" style='width:100%;height:30px;border:0px;text-align:right;' value=''  disabled/></td><td><input  type='text' id=\"txtReturnAmt" + l + "\" style='width:100%;height:30px;border:0px;text-align:right;' value='' disabled/></td><td><input  type='text' id=\"txtReason" + l + "\" style='width:100%;height:30px;border:0px;' disabled/></td><td style='display:none;'><input  type='text' id=\"txtReturnType" + l + "\" style='display:none;width:100%;height:30px;border:0px;' disabled/></td></tr>"
    }
    $('#tableSalesRetunItems tbody').remove();
    SummaryTable += "<tbody>" + SummaryTable + "</tbody>";
    $('#tableSalesRetunItems').append(SummaryTable);
}
//Add Row For TenderDetails
var NewRow = '';
function AddNewRow(l) {
    window.localStorage.setItem('ctCheck', 1);
    NewRow += "<tr><td style='text-align:center;'><input type='checkbox'  id=\"chkItem" + l + "\"   disabled/></td><td><input type='hidden' id=\"hdnItem" + l + "\" /><input type='text'  id=\"txtItem" + l + "\"  style='width:100%;height:30px;border:0px;' disabled/></td><td><input type='hidden' id=\"hdnUnit" + l + "\" /><input type='text'  id=\"txtUom" + l + "\" style='width:100%;height:30px;border:0px;' disabled/></td><td><input type='text'  id=\"txtRefQty" + l + "\" style='width:100%;height:30px;border:0px;text-align:right;' disabled/></td><td><input type='text'  id=\"txtPrice" + l + "\" style='width:100%;height:30px;border:0px;text-align:right;' disabled/></td><td><input type='text'  id=\"txtDiscAmt" + l + "\" style='width:100%;height:30px;border:0px;text-align:right;' disabled/><td><input  type='text' id=\"txtReturnQty" + l + "\" style='width:100%;height:30px;border:0px;text-align:right;' value=''  disabled/></td><td><input  type='text' id=\"txtReturnAmt" + l + "\" style='width:100%;height:30px;border:0px;text-align:right;' value='' disabled/></td><td><input  type='text' id=\"txtReason" + l + "\" style='width:100%;height:30px;border:0px;' disabled/></td><td style='display:none;'><input  type='text' id=\"txtReturnType" + l + "\" style='width:100%;height:30px;border:0px;display:none;' disabled/></td></tr>"

}
function ClearScreen() {
    window.localStorage.setItem('ctCheck', 1);
    $('#txtSubOrganization').val('');
    $('#hdnSubOrganization').val('');
    $('#hdnParentSubOrganization').val('');
    $('#txtCurrency').val('');
    $('#hdnCurrency').val('');
}
function ClearTotalScreen() {
    window.localStorage.setItem('ctCheck', 1);
    $('#rdobtnReplace').prop('checked', false)
    $('#rdobtnCash').prop('checked', true)
    ClearGrid();
    $('#txtTotal').val('0.00');
    $('#txtRemarks').val('');
    $('#calRefInvoiceDate').val('');
    $('#txtRefInvoiceNo').val('');
    $('#hdnRefInvoiceNo').val('');
    $('#txtSalesReturnNo').val('');
    $('#hdnTransId').val('');
    $('#hdnLastUpdatedBy').val('');
    $('#hdnLastUpdatedDate').val('');
    $('#hdnLastUpdatedByName').val('');
    var CurDate = new Date();
    var DateWithFormate = CurDate.getFullYear() + '-' + (CurDate.getMonth() + 1) + '-' + CurDate.getDate();
    ConvertDateWithFormat($('#hdnDateFromate').val(), DateWithFormate, "calDocumentDate")
    EntryGridSettings(5);
    $('#chkheader').removeAttr('checked', false)

}
//Get Login UserName
function GetLoginUserName() {
    window.localStorage.setItem('ctCheck', 1);
    var sess = CashierLite.Session.getInstance().get();
    if (sess != null) {
        if (sess.userName != "") {
            document.getElementById("lblName").innerHTML = sess.userName;
        }
        else {
            window.open('Login.html', '_self', 'location=no');
        }
    }
    else {
        window.open('Login.html', '_self', 'location=no');
    }
    theamLoad();
}
function theamLoad() {
    window.localStorage.setItem('ctCheck', 1);
    if (window.localStorage.getItem("Theam") != null)
        $('head').append('<link rel="stylesheet" href=' + window.localStorage.getItem("Theam") + ' type="text/css" />');
}
//Get Sub_OrgName and BaseCurrency
function GetSubOrgNameWithCur(SubOrgId) {
    window.localStorage.setItem('ctCheck', 1);
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetSubOrgCurrency,
        data: "SubOrgId=" + SubOrgId + "&VD=" + VD(),
        success: function (resp) {
            var CurrData = resp;
            window.localStorage.setItem("CurData", JSON.stringify(CurrData))
            for (var i = 0; i < CurrData.length; i++) {
                $('#txtCurrency').val(CurrData[0].CurrencyCode + " / " + CurrData[0].CurrencyName);
                $('#hdnCurrency').val(CurrData[0].CurrencyId);
                $("#txtCurrency").prop("disabled", true);
            }
        },
        error: function (e) {
        }
    });
}

//GetSummaryGridData
function prev_salesreturn() {
    window.localStorage.setItem('ctCheck', 1);
    var pageIndexCreate = parseFloat($('#hdnPageIndex').val()) - 1;
    if (parseFloat(pageIndexCreate) == 0) {
        pageIndexCreate = '1';
    }
    $('#hdnPageIndex').val(pageIndexCreate);
    var FilterType = $("#hdnFilterType").val();
    GetSummaryGridData(FilterType);

}
function next_salesreturn() {
    var pageIndexCreate = parseFloat($('#hdnPageIndex').val()) + 1;

    $('#hdnPageIndex').val(pageIndexCreate);
    var FilterType = $('#hdnFilterType').val();
    GetSummaryGridData(FilterType + '~Index');

}
function GetSummaryGridData(FilterType) {
    window.localStorage.setItem('ctCheck', 1);
    var Index = $('#hdnPageIndex').val();
    var MethodType = "";
    var FindGo = "";
    var QryType = "";
    if (FilterType == 'FindGo') {
        FindGo = $('#txtFindGo').val();
        Index = 1;
    }
    if (FilterType == 'Summary') {
        $('#txtFindGo').val('');
    }
    else {
        FindGo = $('#txtFindGo').val();
    }
    if (FilterType.indexOf('Index') != -1) {
        FilterType = FilterType.split('~')[0];
        MethodType = "next";
    }

    var HashVal = (window.localStorage.getItem("hashValues"));
    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');
    }
    var SummaryTable = '';
    Jsonobj = [];
    var SubOrg = {};
    SubOrg.SubOrgID = $('#hdnSubOrganization').val();
    var vd_ = window.localStorage.getItem("UUID");
    SubOrg.VD = vd_;
    SubOrg.Index = Index;
    SubOrg.FilterType = FilterType;
    SubOrg.FindGoVal = FindGo;
    Jsonobj.push(SubOrg);
    $("#hdnFilterType").val(FilterType);
    //Jsonobj.push()      
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetSalesReturnSummaryGrid,
        data: "SubOrgId=" + JSON.stringify(Jsonobj) + "&VD=" + VD(),
        success: function (resp) {
            var GridData = resp;
            if (GridData.length > 0) {

                window.localStorage.setItem("SaleReturnSummaryGrid", JSON.stringify(GridData))
                $('#tblSummaryGrid >tbody').children().remove();
                for (var i = 0; i < GridData.length; i++) {
                    var SRId = GridData[i].SALES_RETURN_ID;
                    var RETURN_BRANCH = GridData[i].RETURN_BRANCH;
                    var DocumentNo = GridData[i].RETURNCODE;
                    var DocumentDate = ConvertDateAndReturnDate($('#hdnDateFromate').val(), GridData[i].RETURNDATE);
                    var ReferenceType = GridData[i].REFERENCE_TYPE;
                    var REFERENCE_ID = GridData[i].REFERENCE_ID;
                    var Status = GridData[i].STATUS;
                    var CREATEDDATE = GridData[i].CREATEDDATE;

                    SummaryTable += "<tr onclick=GridSingleClick(\'" + SRId + "\')><td style='display:none;'>" + SRId + "</td><td>" + DocumentNo + "</td><td>" + DocumentDate + "</td><td>" + ReferenceType + "</td></tr>"
                };
                $('#tblSummaryGrid tbody').remove();
                SummaryTable += "<tbody>" + SummaryTable + "</tbody>";
                $('#tblSummaryGrid').append(SummaryTable);
                //Paging
                window.localStorage.setItem("POSSalesReturnSumGrd", JSON.stringify(SummaryTable));
                //GetPaging();
                $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());

            }
            else {

                if (MethodType == "next") {
                    var pageIndexCreate = parseFloat($('#hdnPageIndex').val()) - 1;
                    $('#hdnPageIndex').val(pageIndexCreate);
                }
                else
                    $('#tblSummaryGrid tbody').remove();
            }
        },
        error: function (e) {

            navigator.notification.alert($('#hdnInvalData').val(), "", "neo ecr", "Ok");

        }

    });
}
function FilterSummaryGrid(ControlData, ColumnData) {
    window.localStorage.setItem('ctCheck', 1);
    $('#txtFindGo').val('');
    FindGo = [];
    FindGo = JSON.parse((window.localStorage.getItem("SaleReturnSummaryGrid")));
    var SummaryTable;

    if (ColumnData == 'Status') {
        $("#hdnFilterType").val(ControlData.toLowerCase());
        GetSummaryGridData(ControlData.toLowerCase())

    }
    else
        if (ColumnData == 'CREATEDDATE') {
            $("#hdnFilterType").val(ControlData);
            GetSummaryGridData(ControlData)
        }
}

$('#txtFindGo').keyup(function () {
    FindGo = [];
    FindGo = JSON.parse((window.localStorage.getItem("SaleReturnSummaryGrid")));
    if ($('#txtFindGo').val().trim() == "" || FindGo.length <= 0) {

        GetSummaryGridData('Summary');
        return false;
    }
    else {

        GetSummaryGridData('FindGo');

    }
})

//Filters
$('#btnAll').click(function () {
    //$("#btnCancel").click();
    //GetSummaryGridData($("#hdnSubOrganization").val());
    GetSummaryGridData('Summary');
});
$('#btnToday').click(function () {
    //$("#btnCancel").click();
    FilterSummaryGrid('Today', 'CREATEDDATE')
});
$('#btnYesterday').click(function () {
    //$("#btnCancel").click();
    FilterSummaryGrid('Yesterday', 'CREATEDDATE')
});
$('#btnLast7days').click(function () {
    //$("#btnCancel").click();
    FilterSummaryGrid('Last7', 'CREATEDDATE')

});
$('#btnCurrentMonth').click(function () {
    //$("#btnCancel").click();
    FilterSummaryGrid('CMonth', 'CREATEDDATE')
});
//Summary Grid Click data Binding
function GridSingleClick(SalesReturnId) {
    ClearTotalScreen();
    $("#btnEdit").click();
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetSalesReturnDoubleClick,
        data: "SalesReturnId=" + SalesReturnId + "&VD=" + VD(),
        success: function (resp) {
            var GridData = resp;
            if (GridData.length > 0) {
                //Primary Info..
                $("#hdnTransId").val(GridData[0].SALES_RETURN_ID)
                $('#txtSubOrganization').val(GridData[0].ORGCODE + " / " + GridData[0].ORGNAME);
                $('#hdnSubOrganization').val(GridData[0].BRANCHID);
                $('#hdnParentSubOrganization').val(GridData[0].COMPANYID);

                $('#txtRefInvoiceNo').val(GridData[0].CASHINVNUMBER);
                $('#hdnRefInvoiceNo').val(GridData[0].CASHINVNUMBER);
                $('#calRefInvoiceDate').val(ConvertDateAndReturnDate($('#hdnDateFromate').val(), GridData[0].CASHINVDATE));
                $('#txtSalesReturnNo').val(GridData[0].SALES_RETURN_NO);
                $('#calDocumentDate').val(ConvertDateAndReturnDate($('#hdnDateFromate').val(), GridData[0].SALES_RETURN_DATE));
                $('#txtRemarks').val(GridData[0].Remarks);

                $('#hdnLastUpdatedDate').val(GridData[0].LASTUPDATEDDATE);
                $('#hdnLastUpdatedBy').val(GridData[0].LASTUPDATEDBY);
                $('#hdnLastUpdatedByName').val(GridData[0].LASTUPDATEDBYName);

                var Items = GridData[0].LISTD;
                var SummaryTable = '';
                if (Items.length > 0) {
                    //var GrandTotal = 0.00;
                    for (var l = 0; l < Items.length; l++) {
                        //var DecPlc = 2;
                        var i = Items[l].SALES_RETURN_ID;
                        var ITEMNAME = Items[l].ITEM_CODE + ' / ' + Items[l].ITEM_NAME;
                        var UNITNAME = Items[l].UOM_CODE;

                        SummaryTable += "<tr id=\"" + i + "\" onmouseup=MouseOver(\"" + i + "\")><td style='text-align:center;' >"
                        SummaryTable += parseFloat(Items[l].RETURN_QTY) > 0 ? "<input type='checkbox'  id=\"chkItem" + l + "\"  disabled checked/>" : "<input type='checkbox'  id=\"chkItem" + l + "\"  disabled checked/>";
                        SummaryTable += "</td><td><input type='hidden' id=\"hdnItem" + l + "\" value=\"" + Items[l].ITEM_ID + "\"/><input type='text'  id=\"txtItem" + l + "\"  value=\"" + ITEMNAME + "\" style='width:100%;height:30px;border:0px;' disabled/></td><td><input type='hidden' id=\"hdnUnit" + l + "\" value=\"" + Items[l].UOM_ID + "\"/><input type='text'  id=\"txtUom" + l + "\" value=\"" + UNITNAME + "\" style='width:100%;height:30px;border:0px;' disabled/></td><td><input type='text'  id=\"txtRefQty" + l + "\" value=\"" + parseFloat(Items[l].ORDERED_QTY).toFixed(2) + "\" style='width:100%;height:30px;border:0px;text-align:right;' disabled/></td><td><input type='text'  id=\"txtPrice" + l + "\" value=\"" + parseFloat(Items[l].RETURN_ITEMS_PRICE).toFixed(2) + "\" style='width:100%;height:30px;border:0px;text-align:right;' disabled/></td><td><input type='text'  id=\"txtDiscAmt" + l + "\" value=\"" + parseFloat(Items[l].DiscountPer).toFixed(2) + "\" style='width:100%;height:30px;border:0px;text-align:right;' disabled/><td><input  type='text' value=\"" + parseFloat(Items[l].RETURN_QTY).toFixed(2) + "\" id=\"txtReturnQty" + l + "\" style='width:100%;height:30px;border:0px;text-align:right;' value='0.00' onkeyup='ReturnQtyChange(\"" + Items[l].RETURN_ITEMS_PRICE + "\",\"" + Items[l].RETURN_QTY + "\",\"" + Items[l].ItemDiscountPer + "\",'" + parseFloat(Items[l].Total_Amount).toFixed(2) + "');return false;'  disabled/></td><td><input  type='text' id=\"txtReturnAmt" + l + "\" style='width:100%;height:30px;border:0px;text-align:right;' value=\"" + parseFloat(Items[l].Total_Amount).toFixed(2) + "\" disabled/></td><td><input  type='text' id=\"txtReason" + l + "\" style='width:100%;height:30px;border:0px;' value=\"" + Items[l].Return_Reason + "\" disabled/><input   type='text' id=\"hdnReason" + l + "\" style='width:100%;height:30px;border:0px;display:none;'/></td><td style='display:none;'><input  type='text' id=\"txtReturnType" + l + "\" style='width:100%;height:30px;border:0px;' disabled/></td></tr>"
                        //var GNet = parseFloat(Items[l].NETAMT);
                        //GrandTotal = parseFloat(GrandTotal) + parseFloat(GNet);
                    }
                    if (Items.length < 6) {
                        for (var i = Items.length; i < 6; i++) {
                            SummaryTable += "<tr id=\"" + '' + "\" onmouseup=MouseOver(\"" + '' + "\")><td style='text-align:center;'><input type='checkbox'  id=\"chkItem" + l + "\"  disabled /></td><td><input type='hidden' id=\"hdnItem" + '' + "\" value=\"" + '' + "\"/><input type='text'  id=\"txtItem" + '' + "\"  value=\"" + '' + "\" style='width:100%;height:30px;border:0px;' disabled/></td><td><input type='hidden' id=\"hdnUnit" + '' + "\" value=\"" + '' + "\"/><input type='text'  id=\"txtUom" + '' + "\" value=\"" + '' + "\" style='width:100%;height:30px;border:0px;' disabled/></td><td><input type='text'  id=\"txtRefQty" + '' + "\" value=\"" + '' + "\" style='width:100%;height:30px;border:0px;text-align:right;' disabled/></td><td><input type='text'  id=\"txtPrice" + '' + "\" value=\"" + '' + "\" style='width:100%;height:30px;border:0px;text-align:right;' disabled/></td><td><input type='text'  id=\"txtDiscAmt" + '' + "\" value=\"" + '' + "\" style='width:100%;height:30px;border:0px;text-align:right;' disabled/><td><input  type='text' value=\"" + '' + "\" id=\"txtReturnQty" + '' + "\" style='width:100%;height:30px;border:0px;text-align:right;' value='0.00' onkeyup='ReturnQtyChange(\"" + '' + "\",\"" + '' + "\",\"" + '' + "\",\"" + '' + "\")'  disabled/></td><td><input  type='text' id=\"txtReturnAmt" + '' + "\" style='width:100%;height:30px;border:0px;text-align:right;' value=\"" + '' + "\" disabled/></td><td><input  type='text' id=\"txtReason" + '' + "\" style='width:100%;height:30px;border:0px;' value=\"" + '' + "\" disabled/><input  type='text' id=\"hdnReason" + '' + "\" style='width:100%;height:30px;border:0px;display:none;'/></td><td style='display:none;'><input  type='text' id=\"txtReturnType" + '' + "\" style='width:100%;height:30px;border:0px;display:none;' disabled/></td></tr>"
                        }
                    }

                    $('#tableSalesRetunItems tbody').remove();
                    SummaryTable += "<tbody>" + SummaryTable + "</tbody>";
                    $('#tableSalesRetunItems').append(SummaryTable);
                }

                $('#txtTotal').val(parseFloat(GridData[0].CreditAmount).toFixed(2));

                if (GridData[0].RefundType == "0") {

                    document.getElementById('rdobtnCash').checked = true;
                    document.getElementById('rdobtnReplace').checked = false;
                }
                else if (GridData[0].RefundType == "1") {
                    //  $('#ddlExchangeType').val("1");   // comment this line
                    document.getElementById('rdobtnCash').checked = false;
                    document.getElementById('rdobtnReplace').checked = true;
                }
                //   else $('#ddlExchangeType').val("2");

                $("#hdnMode").val('Edit');
                ShowMenuModes($('#hdnMode').val());
                EnableFormfileds(true);
                $('#lblLastUpdate').css("display", "none");
                $('#trSBInnerRow li span#lblLastUpdate').html($('#trSBInnerRow li span#lblLastUpdate').html());
                $('#trSBInnerRow li span#lblLastUpdatedUser').html($('#hdnLastUpdatedByName').val());
                $('#trSBInnerRow li span#lblLastUpdatedDate').html($('#hdnLastUpdatedDate').val());
                //$('#trSBInnerRow li span#lblMode').html("View");
                $('#trSBInnerRow li span#lblMode').html($('#hdnView').val());
            }
            $('#lblLastUpdate').css("display", "block");
        },
        error: function (e) {
        }
    });
}
//SummaryGrid Paging
function GetPaging() {
    $('table#tblSummaryGrid').each(function () {
        var currentPage = 0;
        var numPerPage = $('#hdnPaging').val();
        var $table = $(this);
        $table.bind('repaginate', function () {
            $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
        });
        $table.trigger('repaginate');
        var numRows = $table.find('tbody tr').length;
        var numPages = Math.ceil(numRows / numPerPage);
        $("#DivPager").remove();
        var $pager = $('<div id="DivPager" class="pager"></div>');
        for (var page = 0; page < numPages; page++) {
            $('<span class="page-number"></span>').text(page + 1).bind('click', {
                newPage: page
            }, function (event) {
                currentPage = event.data['newPage'];
                $table.trigger('repaginate');
                $(this).addClass('active').siblings().removeClass('active');
            }).appendTo($pager).addClass('clickable');
        }
        $pager.insertAfter($table).find('span.page-number:first').addClass('active');
    });
}
//Get Invoice Number For Autofill
function GetRefInvoiceData(SubOrgId) {
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetSalesReturnInvoices,
        data: "SubOrgId=" + SubOrgId + "&VD=" + VD(),
        success: function (resp) {

            var Invoice = resp;
            jsonObjInv = [];
            for (var i = 0; i < Invoice.length; i++) {
                var CASHINVID = Invoice[i].CASHINVID;
                var CASHINVNUMBER = Invoice[i].CASHINVNUMBER;
                var CASHINVDATE = Invoice[i].CASHINVDATE;
                var CUSTOMER_ID = Invoice[i].CUSTOMER_ID;
                var CounterId = Invoice[i].CounterId;
                var ShiftId = Invoice[i].ShiftId;

                Inv = {}
                Inv["InvoiceId"] = CASHINVID;
                Inv["InvoiceNo"] = CASHINVNUMBER;
                Inv["InvoiceDate"] = CASHINVDATE;
                Inv["CUSTOMER_ID"] = CUSTOMER_ID;
                Inv["CounterId"] = CounterId;
                Inv["ShiftId"] = ShiftId;


                jsonObjInv.push(Inv);
            };
            window.localStorage.setItem("FillInvoices", jsonObjInv);
        },
        error: function (e) {
        }
    });
}

var Invoices = (window.localStorage.getItem("FillInvoices"));
jsonObjInv = [];
if (Invoices != "" && Invoices != null) {
    var Inv_ = Invoices.split('$');
    for (var i = 0; i < Inv_.length; i++) {
        var InvChek = Inv_[i].split(':');
        Inv = {}
        Inv["InvoiceId"] = InvChek[0];
        Inv["InvoiceNo"] = InvChek[1];
        Inv["InvoiceDate"] = InvChek[2];
        Inv["CUSTOMER_ID"] = InvChek[3];
        Inv["CounterId"] = InvChek[4];
        Inv["ShiftId"] = InvChek[5];
        Inv["ITEMMODE_ID"] = InvChek[6];
        Inv["ITEMMODE_TYPE"] = InvChek[7];
        jsonObjInv.push(Inv);
    }
}

$("#txtRefInvoiceNo").autocomplete({
    dataFilter: function (data) { return data },
    source: function (req, res) {
        var SubOrgId = '';
        var HashValues = (window.localStorage.getItem("hashValues"));
        if (HashValues != null && HashValues != '') {
            var hashSplit = HashValues.split('~');
            //SubOrgId = hashSplit[1] + ',' + hashSplit[16];
            SubOrgId = hashSplit[1];

        }
        $.ajax({
            url: CashierLite.Settings.GetSalesReturnInvoices,
            data: "subOrgId=" + SubOrgId + "&VD=" + VD1() + "&InvoiceNo=" + $('#txtRefInvoiceNo').val().trim(),
            success: function (resp) {
                if (resp != null) {
                    var Invoices = resp;
                    jsonObjInv = [];
                    if (Invoices != "" && Invoices != null) {
                        var Inv_ = Invoices;
                        for (var i = 0; i < Inv_.length; i++) {
                            var InvChek = Inv_[i];
                            Inv = {
                            }
                            Inv["InvoiceId"] = InvChek.CASHINVID;
                            Inv["InvoiceNo"] = InvChek.CASHINVNUMBER;
                            Inv["InvoiceDate"] = InvChek.CASHINVDATE;
                            Inv["CUSTOMER_ID"] = InvChek.CUSTID;
                            Inv["CounterId"] = InvChek.CounterId;
                            Inv["ShiftId"] = InvChek.ShiftId;
                            Inv["ITEMMODE_ID"] = InvChek.CUSTOMER_ID;
                            Inv["ITEMMODE_TYPE"] = InvChek.CASHINVOICETYPE;
                            jsonObjInv.push(Inv);
                        }


                        res($.map(jsonObjInv, function (el) {

                            return {
                                label: el.InvoiceNo,
                                val: el.InvoiceId,
                                Date: el.InvoiceDate,
                                Customer: el.CUSTOMER_ID,
                                Counter: el.CounterId,
                                Shift: el.ShiftId
                            };
                        }

                        ));





                    }
                }
            }
        });
    },
    change: function (e, i) {
        if (i.item) {
        }
        else {
            ClearGrid();
        }
    },
    select: function (event, ui) {
        ClearGrid();
        $('#txtRefInvoiceNo').val(ui.item.label);
        $('#hdnRefInvoiceNo').val(ui.item.val);

        $('#calRefInvoiceDate').val(ConvertDateAndReturnDate($('#hdnDateFromate').val(), ui.item.Date));
        $("#hdnCustomerId").val(ui.item.Customer);
        $("#hdnCounterId").val(ui.item.Counter);
        $("#hdnShiftId").val(ui.item.Shift);
        GetSalesReturnInvoiceItems(ui.item.val);
        GetSRReasons($('#hdnSubOrganization').val());
    }
});




var copyItems;
function GetSalesReturnInvoiceItems(InvoiceId) {
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetSalesReturnInvoiceItems,
        data: "InvoiceId=" + InvoiceId + "&VD=" + VD(),
        success: function (resp) {
            $('#tableSalesRetunItems tbody').empty();
            var SummaryTable = '';
            NewRow = '';
            var Items = resp;

            if (Items != null || Items == '') {
                if (Items.length > 0) {

                    for (var l = 0; l < Items.length; l++) {
                        if (Items[l].ITEMMODE_TYPE == 'S') {
                            SummaryTable += "<tr id=\"" + l + "\" onmouseup=MouseOver(\"" + l + "\")><td style='text-align:center;background:#eaeaea;color:black;'><input type='checkbox'  id=\"chkItem" + l + "\"  onclick='CheckBoxChecked(this,\"txtReturnQty" + l + "\"," + parseFloat(Items[l].InvItemQty).toFixed(2) + ",\"" + Items[l].Price + "\",\"" + Items[l].ItemDiscountPer + "\",\"" + Items[l].ItemId + "\",\"" + parseFloat(Items[l].Amount).toFixed(2) + "\",\"" + Items[l].ITEMMODE_ID + "\")'/></td>";

                            SummaryTable += "<td style='background:#eaeaea;'><input style='background:#eaeaea;' type='hidden' id=\"hdnItem" + l + "\" value=\"" + Items[l].ItemId + "\"/><input type='text'  id=\"txtItem" + l + "\"  value=\"" + Items[l].ItemName + "\" style='width:100%;height:30px;border:0px;background:#eaeaea;'   /></td><td style='background:#eaeaea;' ><input type='hidden' id=\"hdnUnit" + l + "\" value=\"" + Items[l].UnitId + "\"/><input type='text'  id=\"txtUom" + l + "\" value=\"" + Items[l].UnitName + "\" style='width:100%;height:30px;border:0px;background:#eaeaea;' disabled/></td><td style='background:#eaeaea;' ><input type='text'  id=\"txtRefQty" + l + "\" value=\"" + parseFloat(Items[l].InvItemQty).toFixed(2) + "\" style='width:100%;height:30px;border:0px;text-align:right;background:#eaeaea;' disabled/></td><td><input type='text'  id=\"txtPrice" + l + "\" value=\"" + parseFloat(Items[l].Price).toFixed(2) + "\" style='width:100%;height:30px;border:0px;text-align:right;background:#eaeaea;' disabled/></td><td><input type='text'  id=\"txtDiscAmt" + l + "\" value=\"" + parseFloat(Items[l].ItemDiscountPer).toFixed(2) + "\" style='width:100%;height:30px;border:0px;text-align:right;background:#eaeaea;' disabled/><td><input  type='text' id=\"txtReturnQty" + l + "\" style='width:100%;height:30px;border:0px;text-align:right;background:#eaeaea;' value=\"" + parseFloat(0).toFixed(2) + "\"  onkeypress='OnlyNumbersAllow(event)' onchange='ReturnQtyChange(\"" + Items[l].Price + "\",\"" + Items[l].InvItemQty + "\",\"" + Items[l].ItemDiscountPer + "\",\"" + parseFloat(Items[l].Amount).toFixed(2) + "\");return false;'   disabled/></td><td><input  type='text' id=\"txtReturnAmt" + l + "\" style='width:100%;height:30px;border:0px;text-align:right;background:#eaeaea;' value='" + parseFloat(Items[l].Amount).toFixed(2) + "' disabled/></td><td><input  type='text' id=\"txtReason" + l + "\" style='width:100%;height:30px;border:0px;background:#eaeaea;' disabled placeholder=''/><input  type='text' id=\"hdnReason" + l + "\" style='width:100%;height:30px;border:0px;'/></td><td style='display:none;'><input  type='text' id=\"txtReturnType" + l + "\" style='width:100%;height:30px;border:0px;'/></td><td style='display:none;'><input type='hidden' id=\"hdnItemModeId" + l + "\" value=\"" + Items[l].ITEMMODE_ID + "\"/></td><td style='display:none;'><input type='hidden' id=\"hdnItemModeType" + l + "\" value=\"" + Items[l].ITEMMODE_TYPE + "\"/></td><td style='display:none;'><input type='hidden' id=\"hdnItemTax" + l + "\" value=\"" + Items[l].ITEMTAX + "\"/></td></tr>"
                        }
                        else {
                            SummaryTable += "<tr id=\"" + l + "\" onmouseup=MouseOver(\"" + l + "\")><tr id=\"" + l + "\" onmouseup=MouseOver(\"" + l + "\") ><td style='text-align:center;'><input type='checkbox'  id=\"chkItem" + l + "\"  onclick='CheckBoxChecked(this,\"txtReturnQty" + l + "\"," + parseFloat(Items[l].InvItemQty).toFixed(2) + ",\"" + Items[l].Price + "\",\"" + Items[l].ItemDiscountPer + "\",\"" + Items[l].ItemId + "\",\"" + parseFloat(Items[l].Amount).toFixed(2) + "\",\"" + Items[l].ITEMMODE_ID + "\")'  /></td>";

                            SummaryTable += "<td><input type='hidden' id=\"hdnItem" + l + "\" value=\"" + Items[l].ItemId + "\"/><input type='text'  id=\"txtItem" + l + "\"  value=\"" + Items[l].ItemName + "\" style='width:100%;height:30px;border:0px;' disabled/></td><td><input type='hidden' id=\"hdnUnit" + l + "\" value=\"" + Items[l].UnitId + "\"/><input type='text'  id=\"txtUom" + l + "\" value=\"" + Items[l].UnitName + "\" style='width:100%;height:30px;border:0px;' disabled/></td><td><input type='text'  id=\"txtRefQty" + l + "\" value=\"" + parseFloat(Items[l].InvItemQty).toFixed(2) + "\" style='width:100%;height:30px;border:0px;text-align:right;' disabled/></td><td><input type='text'  id=\"txtPrice" + l + "\" value=\"" + parseFloat(Items[l].Price).toFixed(2) + "\" style='width:100%;height:30px;border:0px;text-align:right;' disabled/></td><td><input type='text'  id=\"txtDiscAmt" + l + "\" value=\"" + parseFloat(Items[l].ItemDiscountPer).toFixed(2) + "\" style='width:100%;height:30px;border:0px;text-align:right;' disabled/><td><input  type='text' id=\"txtReturnQty" + l + "\" style='width:100%;height:30px;border:0px;text-align:right;' value=\"" + parseFloat(0).toFixed(2) + "\"  onkeypress='OnlyNumbersAllow(event)' onchange='ReturnQtyChange(\"" + Items[l].Price + "\",\"" + Items[l].InvItemQty + "\",\"" + Items[l].ItemDiscountPer + "\",\"" + parseFloat(Items[l].Amount).toFixed(2) + "\");return false;'   disabled/></td><td><input  type='text' id=\"txtReturnAmt" + l + "\" style='width:100%;height:30px;border:0px;text-align:right;' value='" + parseFloat(0).toFixed(2) + "' disabled/></td><td><input  type='text' id=\"txtReason" + l + "\" style='width:100%;height:30px;border:0px;' disabled placeholder='' /><input  type='text' id=\"hdnReason" + l + "\" style='width:100%;height:30px;border:0px;display:none;' /></td><td style='display:none;'><input  type='text' id=\"txtReturnType" + l + "\" style='width:100%;height:30px;border:0px;'/></td><td style='display:none;'><input type='hidden' id=\"hdnItemModeId" + l + "\" value=\"" + Items[l].ITEMMODE_ID + "\"/></td><td style='display:none;'><input type='hidden' id=\"hdnItemModeType" + l + "\" value=\"" + Items[l].ITEMMODE_TYPE + "\"/></td><td style='display:none;'><input type='hidden' id=\"hdnItemTax" + l + "\" value=\"" + Items[l].ITEMTAX + "\"/></td></tr>"
                        }
                    };
                    if (Items.length < 6)
                        for (Items.length; Items.length < 6; Items.length++) {
                            AddNewRow(Items.length);
                        }
                    $('#tableSalesRetunItems tbody').remove();
                    SummaryTable += "<tbody>" + SummaryTable + NewRow + "</tbody>";
                    $('#tableSalesRetunItems').append(SummaryTable);
                    SRReasonsAutofill(0, Items.length);
                }
            }
            CalculateTotalAmt();
        },
        error: function (e) {
        }
    });
}


function CheckRedeemPpoints(InvoiceId) {
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.CheckRedeemPpoints,
        data: "InvoiceId=" + InvoiceId + "&VD=" + VD(),
        success: function (resp) {
            var Reason = resp;
            jsonObjReason = [];
            for (var i = 0; i < Reason.length; i++) {
                var ID = Reason[i].Id;
                var CODE = Reason[i].Code;
                var NAME = Reason[i].Name;
                Reas = {}
                Reas["ID"] = ID;
                Reas["CODE"] = CODE;
                Reas["NAME"] = NAME;
                jsonObjReason.push(Reas);
            };
            window.localStorage.setItem("FillSRReasons", jsonObjReason);
        },
        error: function (e) {
        }
    });
}
$('#ddlExchangeType').change(function (e) {
    EntryGridSettings();
    ClearGrid();
    $('#txtTotal').val('0.00');
    $('#chkheader').removeAttr('checked', false)
    $('#chkheader').prop("disabled", false);
});
function OnlyNumbersAllow(e) {
    if (((e.keyCode >= 48 && e.keyCode <= 57) || e.keyCode == 46) == false) {
        e.preventDefault();
    }
}

$('#chkheader').click(function () {

    if ($('#txtRefInvoiceNo').val() == "" || $('#txtRefInvoiceNo').val() == null || $('#txtRefInvoiceNo').val() == undefined) {
        return false;
    }
    if ($('#txtRefInvoiceNo').val() != "" && $('#txtRefInvoiceNo').val() != null && $('#txtRefInvoiceNo').val() != undefined) {

        var GrandTotal = 0.00; var m_;
        $('#tableSalesRetunItems tr').each(function () {
            var RID = $(this).attr('id');
            if (m_ != RID) {
                if (RID != undefined && RID != "" && RID != null) {
                    m_ = RID;
                    var RetQty = $('#txtRefQty' + RID).val();
                    var RetPrice = $('#txtPrice' + RID).val();
                    var RetDisc = $('#txtDiscAmt' + RID).val();
                    var Amt = $('#txtReturnAmt' + RID).val();
                    var SingleItemPrice = parseFloat(Amt) / parseFloat(RetQty);

                    $('#chkItem' + RID).attr("checked", 'checked')
                    if ($('#chkheader').prop("checked") == false) {
                        $('#chkItem' + RID).removeAttr('checked', false)
                        $('#txtReturnQty' + RID).attr("disabled", "disabled");
                        $('#txtReason' + RID).attr("disabled", "disabled");
                        $('#txtReturnQty' + RID).val("0.00");
                        $('#txtReturnAmt' + RID).val("0.00");
                    }
                    else
                        if ($('#chkheader').prop("checked") == true) {
                            $('#chkItem' + RID).prop("checked", true)
                            $('#txtReturnQty' + RID).attr("disabled", "disabled");
                            $('#txtReason' + RID).removeAttr("disabled");
                            $('#txtReturnQty' + RID).val(parseFloat(RetQty).toFixed(2));
                            if (parseFloat(RetDisc).toFixed(2) == '0.00')
                                $('#txtReturnAmt' + RID).val(parseFloat(parseFloat(RetQty) * parseFloat(RetPrice)).toFixed(2));
                            else {
                                var Amount = (parseFloat(RetQty) * parseFloat(RetPrice)) - ((parseFloat(RetQty) * parseFloat(RetPrice)) / 100) * parseFloat(RetDisc);
                                $('#txtReturnAmt' + RID).val(parseFloat(Amount).toFixed(2));
                            }
                            var GNet = parseFloat($('#txtReturnAmt' + RID).val());
                            if (parseFloat(RetQty) > 0)
                                GrandTotal = parseFloat(GrandTotal) + parseFloat(GNet);
                        }
                }

            }
        });
        $('#txtTotal').val(parseFloat(GrandTotal).toFixed(2));

    }
});
function CheckBoxChecked() {
    if ($('#txtRefInvoiceNo').val() == "" || $('#txtRefInvoiceNo').val() == null || $('#txtRefInvoiceNo').val() == undefined) {
        return false;
    }
}
function CheckBoxChecked(e, txtQtyId, RefQty, price, discount, ItemId, Amt, itemModeId) {
    if ($('#txtRefInvoiceNo').val() == "" || $('#txtRefInvoiceNo').val() == null || $('#txtRefInvoiceNo').val() == undefined) {
        return false;
    }
    var count = 0;
    $('#tableSalesRetunItems tr').each(function () {
        var TRId = $(this).attr('id');
        if (TRId != undefined) {
            if (itemModeId == $('#hdnItemModeId' + TRId).val()) {
                count = parseInt(count) + 1;
                if (e.checked == true) {
                    //$('#chkItem' + TRId).attr("checked", 'checked');
                    $('#txtReturnQty' + TRId).attr("disabled", "disabled");
                    $('#txtReturnQty' + TRId).val(parseFloat($('#txtRefQty' + TRId).val()).toFixed(2));
                    $('#chkItem' + TRId).prop('checked', true);
                    $('#txtReason' + TRId).removeAttr("disabled");

                } else {
                    // $('#chkItem' + TRId).attr("checked", 'checked');
                    $('#chkItem' + TRId).prop('checked', false);
                    $('#txtReturnQty' + TRId).attr("disabled", "disabled");
                    $('#txtReason' + TRId).attr("disabled", "disabled");

                    $('#txtReturnQty' + TRId).val("0.00");
                }
            }
        }
    });
    if (ItemId != "" && ItemId != null) {
        $('#' + txtQtyId).attr("disabled", "disabled");
        if (e.checked == false) {
            $('#' + txtQtyId).attr("disabled", "disabled");
            $('#' + txtQtyId).val("0.00");
            $('#chkheader').attr("checked", 'checked')
            $('#chkheader').removeAttr('checked', false)
        }
        else if (e.checked == true) {
            if (parseInt(count) == 1) {
                $('#' + txtQtyId).removeAttr("disabled");
            }
            $('#' + txtQtyId).val(parseFloat(RefQty).toFixed(2));
            $('#' + txtQtyId).focus();
        }
        ReturnQtyChange(price, RefQty, discount, Amt);
    }
}
function GetSRReasons(SubOrgId) {
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetSalesReturnReasons,
        data: "SubOrgId=" + SubOrgId + "&VD=" + VD(),
        success: function (resp) {
            var Reason = resp;
            jsonObjReason = [];
            for (var i = 0; i < Reason.length; i++) {
                var ID = Reason[i].Id;
                var CODE = Reason[i].Code;
                var NAME = Reason[i].Name;
                Reas = {}
                Reas["ID"] = ID;
                Reas["CODE"] = CODE;
                Reas["NAME"] = NAME;
                jsonObjReason.push(Reas);
            };
            window.localStorage.setItem("FillSRReasons", jsonObjReason);
        },
        error: function (e) {
        }
    });
}
var SResons = (window.localStorage.getItem("FillSRReasons"));
jsonObjReason = [];
if (SResons != "" && SResons != null) {
    var InvRea_ = SResons.split('$');
    for (var i = 0; i < InvRea_.length; i++) {
        var ReaChek = InvRea_[i].split(':');
        Inv = {}
        Inv["ID"] = ReaChek[0];
        Inv["CODE"] = ReaChek[1];
        Inv["NAME"] = ReaChek[2];
        jsonObjReason.push(Inv);
    }
}
function SRReasonsAutofill(min, max) {
    for (var p = min; p <= max; p++) {
        var PRid = p;
        $('#txtReason' + PRid).autocomplete({
            dataFilter: function (data) { return data },
            source: function (req, add) {
                add($.map(jsonObjReason, function (el) {
                    var re = $.ui.autocomplete.escapeRegex(req.term);
                    if (re.trim() != "") {

                        if (el.NAME.toLowerCase().indexOf(re.trim().toLowerCase()) != -1 || el.CODE.toLowerCase().indexOf(re.trim().toLowerCase()) != -1)
                            return {
                                label: el.NAME,
                                val: el.ID,
                                Code: el.CODE
                            };
                    }
                    else {
                        return {
                            label: el.NAME,
                            val: el.ID,
                            Code: el.CODE
                        };
                    }
                }));
            }
            ,
            change: function (e, i) {
                if (i.item) {
                }
                else {
                    var Rid_ = $('#hdnRId').val();
                    $('#txtReason' + Rid_).val('');
                    $('#hdnReason' + Rid_).val('');
                }
            },
            select: function (event, ui) {
                var Rid_ = $('#hdnRId').val();
                $('#txtReason' + Rid_).val('');
                $('#hdnReason' + Rid_).val('');
                $('#txtReason' + Rid_).val(ui.item.label);
                $('#hdnReason' + Rid_).val(ui.item.val);
            }
        });
    }
}

function MouseOver(R) {
    $('#hdnRId').val(R);
}
function ReturnQtyChange(Price, InvoiceQty, DiscountAmt, Amt) {
    var Rid_ = $('#hdnRId').val();
    var Qty = $('#txtReturnQty' + Rid_).val();
    var SingleItemPrice = parseFloat(Amt) / parseFloat(InvoiceQty);
    if (Qty == '' || Qty == null || Qty == 'NaN' || Qty == undefined || parseFloat(Qty) == parseFloat(0)) {
        Qty = '0.00'
        //$('#txtReturnAmt' + Rid_).val('0.00');
    }
    if (parseFloat(Qty) > parseFloat(InvoiceQty)) {
        var RetAmt = $('#txtReturnAmt' + Rid_).val();
        $('#txtReturnQty' + Rid_).val(parseFloat(InvoiceQty).toFixed(2));
        $('#txtReturnAmt' + Rid_).val(parseFloat(RetAmt).toFixed(2));
        navigator.notification.alert($('#hdnRtnQtyNGrRefQty').val(), "", "neo ecr", "Ok");
        CalculateTotalAmt();
        return false;
    }
    if (parseFloat(DiscountAmt).toFixed(2) == '0.00')
        $('#txtReturnAmt' + Rid_).val(parseFloat(parseFloat(Qty) * parseFloat(SingleItemPrice)).toFixed(2));
    else {
        var Amount = parseFloat(Qty) * parseFloat(SingleItemPrice);
        $('#txtReturnAmt' + Rid_).val(parseFloat(Amount).toFixed(2));
    }
    $('#txtReturnQty' + Rid_).val(parseFloat(Qty).toFixed(2));

    CalculateTotalAmt();
}
function CalculateTotalAmt() {
    var GrandTotal = 0.00; var m_;
    $('#tableSalesRetunItems tr').each(function () {
        var RID = $(this).attr('id');
        if (m_ != RID) {
            if (RID != undefined) {
                m_ = RID;
                if ($('#txtReturnAmt' + RID).val().trim() != '' && parseFloat($('#txtReturnQty' + RID).val()) > 0) {
                    var GNet = parseFloat($('#txtReturnAmt' + RID).val());
                    GrandTotal = parseFloat(GrandTotal) + parseFloat(GNet);
                }
            }
        }
    });
    //$('#txtTotal').val(parseFloat(GrandTotal).toFixed(2));
    $('#txtTotal').val(parseFloat(GrandTotal).toFixed(2));
}
function ClearGrid() {
    $("#hdnCustomerId").val('');
    $('#txtRefInvoiceNo').val('');
    $('#hdnRefInvoiceNo').val('');
    $('#calRefInvoiceDate').val('');
    $("#hdnCounterId").val('');
    $("#hdnShiftId").val('');
    EntryGridSettings(5);
}
function EnableFormfileds(Status) {
    $('#txtSubOrganization').prop("disabled", true);
    $('#txtCurrency').prop("disabled", true);
    $('#txtSalesReturnNo').prop("disabled", true);
    $('#txtRefInvoiceNo').prop("disabled", Status);
    $('#rdobtnReplace').prop("disabled", Status);
    $('#rdobtnCash').prop("disabled", Status);
    if ($("#hdnMode").val().trim() == 'View' || $("#hdnMode").val().trim() == 'Edit' || $("#hdnMode").val().trim() == 'Active') {
        $('#txtSalesReturnNo').prop("disabled", true);
        $('#txtRefInvoiceNo').prop("disabled", true);
        $('#rdobtnReplace').prop("disabled", true);
        $('#rdobtnCash').prop("disabled", true);
    }
    $('#calDocumentDate').prop("disabled", true);
    $('#calRefInvoiceDate').prop("disabled", true);
    $('#txtRemarks').prop("disabled", Status);
    if ($('#hdnAutoCode').val() == 'Y' && $('#txtSalesReturnNo').val().trim() == '') {
        $('#txtSalesReturnNo').prop("disabled", true);
    }
    $('#ddlExchangeType').prop("disabled", true);
    $('#chkheader').prop("disabled", Status);
}
function ShowMenuModes(Mode) {
    if (Mode == 'New') {
        $("#btnCreate").attr('style', 'display:block;');
        $("#btnCreate").attr('style', 'display:none;');
        $("#ShowMenuModes").attr('style', 'display:block;');
        $("#btnSave").attr('style', 'display:block;');
        $("#btnEdit").attr('style', 'display:none;');
        $("#btnDelete").attr('style', 'display:none;');
        $("#btnCancel").attr('style', 'display:block;');
        $("#SummaryGrid").attr('style', 'display:none;');
        $("#NewPage").attr('style', 'display:block;');
        $("#divStatusGroup").attr('style', 'display:none;');

    }
    if (Mode == 'Edit') {
        $("#btnCreate").attr('style', 'display:block;');
        $("#ShowMenuModes").attr('style', 'display:block;');
        $("#btnSave").attr('style', 'display:none;');
        $("#btnEdit").attr('style', 'display:none;');
        $("#btnDelete").attr('style', 'display:none;');
        $("#btnCancel").attr('style', 'display:block;border-radius:4px');
        $("#SummaryGrid").attr('style', 'display:none;');
        $("#NewPage").attr('style', 'display:block;');
        $("#divStatusGroup").attr('style', 'display:none;');

    }
    if (Mode == 'Cancel') {
        $("#btnCreate").attr('style', 'display:block;');
        $("#ShowMenuModes").attr('style', 'display:none;');
        $("#SummaryGrid").attr('style', 'display:block;');
        $("#NewPage").attr('style', 'display:none;');
        $("#divStatusGroup").attr('style', 'display:none;');

    }
}

//Client Side Validation
function ClientSideValidations(code) {
    switch (code) {
        case "Save":
            if ($('#txtSubOrganization').val() == "" || $('#hdnSubOrganization').val() == "") {
                navigator.notification.alert($('#hdnSubOrgAlrt').val(), "", "neo ecr", "Ok");

                $('#txtSubOrganization').focus();
                return false;
            }

            if ($('#calDocumentDate').val() == "") {
                navigator.notification.alert("Please select Document Date", "", "neo ecr", "Ok"); 
                $('#calDocumentDate').focus();
                return false;
            }
            if ($('#txtRefInvoiceNo').val() == "" || $('#hdnRefInvoiceNo').val() == "") {
                navigator.notification.alert($('#hdnFillRefNo').val(), "", "neo ecr", "Ok");

                $('#txtRefInvoiceNo').focus();
                return false;
            }
            if ($('#calRefInvoiceDate').val() == "") {
                navigator.notification.alert($('#hdnFillRefDt').val(), "", "neo ecr", "Ok");

                $('#txtCustType').focus();
                return false;
            }
            if ($('#txtCurrency').val() == "" || $('#hdnCurrency').val() == "") {
                navigator.notification.alert($('#hdnFillCurr').val(), "", "neo ecr", "Ok");

                $('#txtCurrency').focus();
                return false;
            }
            var Qty = 0;
            $('#tableSalesRetunItems tr').each(function () {
                var RID = $(this).attr('id');
                if (RID != undefined) {
                    if ($('#txtReturnQty' + RID).val().trim() != '' && $('#txtReturnQty' + RID).val().trim() != "0.00" && parseFloat($('#txtReturnQty' + RID).val()) != parseFloat(0))
                        Qty = 1;
                }
            });
            if (Qty == 0) {
                navigator.notification.alert($('#hdnRtnLeastOneItem').val(), "", "neo ecr", "Ok");
                return false;
            }
            return true;
            break;
        case "Delete":
            //if (confirm("Are you sure you want to delete this user?"))
            if (confirm($('#hdnDelConf').val() + "?"))
                return true;
            else
                return false;
            break;
    }

}
function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}
//Save and Edit
$("#btnSave").click(function () {
    if (ClientSideValidations('Save')) {
        GetNewData();
    }
});

//Get NewData
function GetNewData() {
    var HashVal = (window.localStorage.getItem("hashValues"));

    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');
    }

    var Mode = $("#hdnMode").val();
    if ($("#hdnTransId").val() == "" || $("#hdnTransId").val() == null || $("#hdnMode").val() == 'New') {
        Mode = "Create";
        $("#hdnTransId").val(guid());
    }
    else if ($("#hdnMode").val() == "Edit")
        Mode = "Update";
    else if ($("#hdnMode").val() == "Delete")
        Mode = "Delete";

    SRHeaderData = {}
    SRRefType = {}
    objSRHeaderData = [];
    objSRItemsData = [];
    objSRRefType = [];


    var CurDate = new Date();
    var DateWithFormate = CurDate.getFullYear() + '-' + (CurDate.getMonth() + 1) + '-' + CurDate.getDate() + ' ' + CurDate.getHours() + ':' + CurDate.getMinutes() + ':' + CurDate.getSeconds() + ':' + CurDate.getMilliseconds();

    SRHeaderData["SALES_RETURN_ID"] = $("#hdnTransId").val();
    var icode = 'POSSLSRT~USP';
    var strTable = 'QATABLE084';
    var strColumn = 'COLUMN002';
    var HashVal = (window.localStorage.getItem("hashValues"));
    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');
    }
    SRHeaderData["RETURN_NUMBER"] = $("#txtSalesReturnNo").val().trim();
    SRHeaderData["RETURN_BRANCH"] = $("#hdnSubOrganization").val();
    SRHeaderData["RETURN_DATE"] = GetDateWithYYYYMMDD($('#hdnDateFromate').val(), $('#calDocumentDate').val()) + ' ' + 00 + ':' + 00 + ':' + 00;
    SRHeaderData["CUSTOMER_ID"] = $("#hdnCustomerId").val();
    SRHeaderData["CURRENCY_ID"] = $("#hdnCurrency").val();
    SRHeaderData["REFERENCE_TYPE"] = "POS";
    SRHeaderData["COUNTERID"] = $("#hdnCounterId").val();
    SRHeaderData["SHIFTID"] = $("#hdnShiftId").val();
    if (document.getElementById('rdobtnCash').checked == true) {
        SRHeaderData["COLUMN019"] = parseInt("0");
    }
    else {
        SRHeaderData["COLUMN019"] = parseInt("1");
    }
    SRHeaderData["STATUS"] = "1";
    if ($('#txtTotal').val() == "" || $('#txtTotal').val() == null || $('#txtTotal').val() == 'NaN')
        SRHeaderData["TOTAL_AMOUNT"] = "0";
    else
        SRHeaderData["TOTAL_AMOUNT"] = $('#txtTotal').val();
    SRHeaderData["REMARKS"] = $('#txtRemarks').val();
    SRHeaderData["Cust_Return_No"] = $('#txtRefInvoiceNo').val();
    SRHeaderData["Cust_Return_Date"] = GetDateWithYYYYMMDD($('#hdnDateFromate').val(), $('#calRefInvoiceDate').val());

    SRHeaderData["Return_Mode"] = '';
    SRHeaderData["Return_Descr"] = '';
    SRHeaderData["Goods_at"] = '';
    SRHeaderData["Division"] = '';
    SRHeaderData["Received_At_Branch"] = '';
    SRHeaderData["Warehouse"] = '';
    SRHeaderData["Cus_Exp_Del_Date"] = '';
    SRHeaderData["OBS"] = '';
    SRRefType["SNO"] = '1';
    SRRefType["REFERENCE_TYPE"] = 'I';
    SRRefType["Reference_ID"] = '';
    SRRefType["POS_CashInv_ID"] = $("#hdnRefInvoiceNo").val();
    if (document.getElementById('rdobtnCash').checked == true) {
        SRRefType["STATUS"] = '1';
        SRRefType["SALESRETURNTYPE"] = 'Cash Refund';
    }
    else {
        SRRefType["SALESRETURNTYPE"] = 'Replacement';
        SRRefType["STATUS"] = "0";
    }

    var iSalesRCount = 0; var m_;
    $('#tableSalesRetunItems tr').each(function () {

        var TRId = $(this).attr('id');
        if (m_ != TRId) {
            if (TRId != undefined) {
                if ($('#hdnItem' + TRId).val().trim() != "" && $('#hdnItem' + TRId).val().trim() != null || $('#txtItem' + TRId).val().trim() == '') {
                    if (parseFloat($('#txtReturnQty' + TRId).val()) > parseFloat(0)) {
                        m_ = TRId;
                        SRItemsData = {}
                        SRItemsData["R_Type"] = '';
                        SRItemsData["SALES_RETURN_ID"] = $("#hdnTransId").val();
                        SRItemsData["SNO"] = parseInt(iSalesRCount);
                        SRItemsData["ITEM_ID"] = $('#hdnItem' + TRId).val();

                        SRItemsData["ORDERED_QTY"] = $('#txtRefQty' + TRId).val();
                        SRItemsData["UOM_ID"] = $('#hdnUnit' + TRId).val();
                        SRItemsData["RETURN_ITEMS_PRICE"] = $('#txtPrice' + TRId).val();
                        SRItemsData["RETURN_QTY"] = $('#txtReturnQty' + TRId).val();
                        SRItemsData["RETURN_REASON"] = $('#hdnReason' + TRId).val();
                        SRItemsData["BASE_AMOUNT"] = $('#txtReturnAmt' + TRId).val();
                        SRItemsData["BASE_QUANTITY"] = $('#txtReturnQty' + TRId).val();
                        SRItemsData["ITEM_MODETYPE"] = $('#hdnItemModeType' + TRId).val();
                        SRItemsData["FREE_QTY"] = 0;//RETURNFREEQTY
                        SRItemsData["APPOROVED_QTY"] = 0;//FreeQty
                        SRItemsData["EXPIRED_DATE"] = DateWithFormate;
                        SRItemsData["BASE_FREEQTY"] = "0"
                        SRItemsData["RETURN_STRATEGY"] = '';
                        SRItemsData["APPROVAL_REASON"] = '';
                        SRItemsData["STATUS"] = "6";
                        SRItemsData["SCHEME_ID"] = '';
                        SRItemsData["Total_Amount"] = $('#txtReturnAmt' + TRId).val();
                        if ($('#hdnItemTax' + TRId).val() != '') {

                            var tax_ = parseFloat(parseFloat($('#hdnItemTax' + TRId).val()) / parseFloat($('#txtRefQty' + TRId).val())).toFixed(4);
                            tax_ = parseFloat(tax_) * parseFloat($('#txtReturnQty' + TRId).val());

                            SRItemsData["Tax"] = parseFloat(tax_).toFixed(4);

                        }
                        SRItemsData["Amount"] = $('#txtReturnAmt' + TRId).val();
                        SRItemsData["Discount"] = $('#txtDiscAmt' + TRId).val();
                        //Replacement Related Data

                        SRItemsData["FK_SNO"] = "1";
                        SRItemsData["QUANTITY"] = $('#txtReturnQty' + TRId).val();
                        SRItemsData["REPLACED_QTY"] = $('#txtReturnQty' + TRId).val();
                        SRItemsData["REP_PRICE"] = $('#txtPrice' + TRId).val();
                        SRItemsData["REMARKS"] = $('#hdnReason' + TRId).val();
                        SRItemsData["STATUS"] = "6";
                        SRItemsData["RETURN_STRATEGY"] = '';

                        SRItemsData["CREATEDBY"] = HashValues[14];
                        SRItemsData["CREATEDDATE"] = DateWithFormate;
                        SRItemsData["ROWGUID"] = '';

                        SRItemsData["LASTUPDATEDBY"] = $("#hdnLastUpdatedBy").val();
                        SRItemsData["LASTUPDATEDDATE"] = $("#hdnLastUpdatedDate").val();
                        SRItemsData["BRANCHID"] = $("#hdnSubOrganization").val();
                        SRItemsData["COMPANYID"] = $("#hdnParentSubOrganization").val();
                        SRItemsData["UPDATEDSITE"] = "DBMS_REPUTIL.GLOBAL_NAME";
                        SRItemsData["LANGID"] = HashValues[7];
                        iSalesRCount++;
                        objSRItemsData.push(SRItemsData);
                    }
                }
            }
        }
    });
    if (iSalesRCount == 0) {//Select Atleast One Item
        $("#hdnTransId").val('');
        navigator.notification.alert($('#hdnRtnLeastOneItem').val(), "", "neo ecr", "Ok");

        return false;
    }

    SRRefType["CREDITAMOUNT"] = $('#txtTotal').val();
    SRRefType["Discount"] = '0.00';
    SRRefType["Tax"] = 0;
    SRRefType["RECORDCOUNT"] = iSalesRCount;


    SRHeaderData["CREATEDBY"] = HashValues[14];
    SRHeaderData["CREATEDDATE"] = DateWithFormate;
    if (Mode == 'Create') {
        SRHeaderData["ROWGUID"] = '';
        SRHeaderData["LASTUPDATEDBY"] = HashValues[14];
        SRHeaderData["LASTUPDATEDDATE"] = DateWithFormate;
    }
    if (Mode != 'Create') {

        SRHeaderData["LASTUPDATEDBY"] = HashValues[14];
        SRHeaderData["LASTUPDATEDDATE"] = DateWithFormate;
    }

    SRHeaderData["BRANCHID"] = $("#hdnSubOrganization").val();
    SRHeaderData["COMPANYID"] = $("#hdnParentSubOrganization").val();
    SRHeaderData["UPDATEDSITE"] = 'DBMS_REPUTIL.GLOBAL_NAME';
    SRHeaderData["LANGID"] = HashValues[7];
    objSRHeaderData.push(SRHeaderData);
    objSRRefType.push(SRRefType);
    var SalesReturnDML = new Object();
    SalesReturnDML.Mode = Mode;
    SalesReturnDML.SRHeaderData = JSON.stringify(objSRHeaderData);
    SalesReturnDML.SRRefType = JSON.stringify(objSRRefType);
    SalesReturnDML.SRItemsData = JSON.stringify(objSRItemsData);
    SalesReturnDML.VD = VD();
    var ComplexObject = new Object();

    ComplexObject.SalesReturnData = SalesReturnDML;
    var data = JSON.stringify(ComplexObject);

    $.ajax({
        type: 'POST',
        url: CashierLite.Settings.SaveSalesReturnData,
        data: data,
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function (resp) {
            if (resp != null) {

                var d_ = '';
                var orgd = '';
                if (resp.SaveSalesReturnDataResult.indexOf('~') != -1) {
                    d_ = resp.SaveSalesReturnDataResult.split('~')[0];
                    orgd = resp.SaveSalesReturnDataResult.split('~')[1];
                    resp.SaveSalesReturnDataResult = d_;
                }

                if (resp.SaveSalesReturnDataResult == '100000') {//Success Record   

                    $('#hdncreditnotId').val(orgd);
                    //if (document.getElementById('rdobtnCash').checked == true) { 
                    //navigator.notification.alert(orgd + '-' + $('#hdnSaveSucc').val(), "", "neo ecr", "Ok"); 
                    //}
                    //else { 
                    printSLDiv(orgd);
                    //}

                    ClearTotalScreen();
                    $('#PrimeInfo').addClass('open');
                    $('#ShippingInfo').removeClass('open');
                    $('#divPrimeInformation').attr('style', 'display:block');
                    $('#divShippingInformation').attr('style', 'display:none');
                    //$("#btnCancel").click();
                    $('#trSBInnerRow li span#lblMode').html($('#hdnNew').val());
                    return false;
                }
                if (resp.SaveSalesReturnDataResult.split('~')[0] == '100001') {//Duplicate SalesReturn Code
                    $("#hdnTransId").val('');
                    navigator.notification.alert($("#txtSalesReturnNo").val().trim() + $('#hdnDocNoExist').val(), "", "neo ecr", "Ok");

                    $('#PrimeInfo').addClass('open');
                    $('#ShippingInfo').removeClass('open');
                    $('#divPrimeInformation').attr('style', 'display:block');
                    $('#divShippingInformation').attr('style', 'display:none');
                    return false;

                }
                if (resp.SaveSalesReturnDataResult == 'POS10002') {//Deleted Record

                    navigator.notification.alert($("#txtSalesReturnNo").val().trim() + $('#hdnDelSucc').val(), "", "neo ecr", "Ok");

                    ClearTotalScreen();
                    $("#btnCancel").click();

                    GetSummaryGridData('Summary');
                    return false;
                }
                else {
                    navigator.notification.alert($('#hdnErrOccer').val(), "", "neo ecr", "Ok");

                    return false;
                }
            }
            return false;
        },
        error: function (e) {
            $("#hdnTransId").val('');

            navigator.notification.alert($('#hdnErrOccer').val(), "", "neo ecr", "Ok");

            return false;
        }
    });
}

function Print(orgd) {
    var HashVal = (window.localStorage.getItem("hashValues"));
    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');
        getResources(HashValues[7], "POSSalesReturn");
    }
    $('#lblSubOrgName').html($('#txtSubOrganization').val());
    $('#lblDoctDate').html($('#calDocumentDate').val());
    $('#lblCreditNoteNo').html(orgd);
    $('#lblInvoiceNo').html($('#txtRefInvoiceNo').val());
    var amt_ = Math.round($('#txtTotal').val(), 2);
    $('#lblTransAmt').html(parseFloat(amt_).toFixed(2));
    $('#myModalprint').show();
    $('#trSBInnerRow li span#lblMode').html($('#hdnNew').val());
}
function printCreditNote() {
    printUSBCreditNote();
    $('#myModalprint').css('display', "none");
}
function BufferToBase64(buf) {
    var binstr = Array.prototype.map.call(buf, function (ch) {
        return String.fromCharCode(ch);
    }).join('');
    return btoa(binstr);
}
 
function printUSBCreditNote() {
    var ReturnId = $('#hdncreditnotId').val();

    var PrinterName_ = "InnerPrinter";
    if (window.localStorage.getItem("PrinterName") != null) {
        PrinterName_ = window.localStorage.getItem("PrinterName");
    }
//T2s
    // BTPrinter.connect(function (data) {

    // }, function (err) {
    // }, PrinterName_); 
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.printUSBSalesReturn,
        data: "ReturnId=" + ReturnId + "&VD=" + VD1(),
        success: function (resp) {
            try {
                var dataMain = resp;
                //var printName_ = ''; 
                var qrData = dataMain.split('Š')[1];
                dataMain = dataMain.split('Š')[0]; 
                // BTPrinter.list(function (data) {
                //     if (PrinterName_ != "InnerPrinter") {
                //         BTPrinter.printText(function (data) {
                //         }, function (err) {
                //         }, dataMain);
                //         //printQrCodeRR(ReturnId);
                //         dataTT(qrData);
                //         BTPrinter.printPOSCommand(function (data) {
                //         }, function (err) {
                //         }, "0C");
                //         BTPrinter.printPOSCommand(function (data) {
                //         }, function (err) {
                //         }, "0A"); 
                //         BTPrinter.printPOSCommand(function () { }, function () { }, "1D");//FEED PAPER AND CUT
                //     } 
                //     if (PrinterName_ == "InnerPrinter") {
                        navigator.sunmiInnerPrinter.printerInit();
                        navigator.sunmiInnerPrinter.printerStatusStartListener();
                        navigator.sunmiInnerPrinter.printOriginalText(dataMain); 
                        navigator.sunmiInnerPrinter.setAlignment(1);
                        navigator.sunmiInnerPrinter.printQRCode(qrData,10,10); 
                        navigator.sunmiInnerPrinter.hasPrinter();
                        window.localStorage.setItem('PrintCut','1');
                        navigator.sunmiInnerPrinter.printerStatusStopListener();
                        // dataTT(qrData);
                        //printQrCodeRR(ReturnId);
                //     }
                //     BTPrinter.disconnect(function (data) {
                //         //alert('Printer dis-conneted')
                //     }, function (err) {
                //     }, PrinterName_);
                // }, function (err) {  
                // })
            } catch (a) {

            }
        }, error: function (e) {

        }
    });
}
PaperCutCheck();
function PaperCutCheck() {
        try
        {
            if(window.localStorage.getItem('PrintCut')=='1')
            {
                navigator.sunmiInnerPrinter.printOriginalText("                      "); 
                navigator.sunmiInnerPrinter.printOriginalText("                      "); 
                navigator.sunmiInnerPrinter.printOriginalText("                      "); 
                navigator.sunmiInnerPrinter.printOriginalText("                      "); 
                window.localStorage.setItem('PrintCut','0');
                navigator.sunmiInnerPrinter.sendRAWData("HVZCAA=="); 
            }
        }
        catch(a)
        {

        }
        setTimeout('PaperCutCheck()', 1000);
}
function dataTT(invData) { 
    BTPrinter.printBase64(function (data) {
    }, function (err) {
    }, invData, '1');//base64 string, align 
}
function printQrCodeRR(invoiceId) {
    const justify_center = '\x1B\x61\x01';
    const justify_left = '\x1B\x61\x00';
    const qr_model = '\x32';          // 31 or 32
    const qr_size = '\x08';          // size
    const qr_eclevel = '\x33';          // error correction level (30, 31, 32, 33 - higher)
    const qr_data = invoiceId;
    const qr_pL = String.fromCharCode((qr_data.length + 3) % 256);
    const qr_pH = String.fromCharCode((qr_data.length + 3) / 256);

    BTPrinter.printText(data => {
        this.msg = '';
    }, err => {
        this.msg = '';
    }, justify_center +
    '\x1D\x28\x6B\x04\x00\x31\x41' + qr_model + '\x00' +        // Select the model
    '\x1D\x28\x6B\x03\x00\x31\x43' + qr_size +                  // Size of the model
    '\x1D\x28\x6B\x03\x00\x31\x45' + qr_eclevel +               // Set n for error correction
    '\x1D\x28\x6B' + qr_pL + qr_pH + '\x31\x50\x30' + qr_data + // Store data 
    '\x1D\x28\x6B\x03\x00\x31\x51\x30' +                        // Print
    '\n\n\n' +
        justify_left);
}
function closeCreditNoteprintDialog() {
    $('#myModalprint').css('display', "none");
}

function PrintSLLoad() {
    var SubOrg_Id;
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        SubOrg_Id = hashSplit[1];
    }
    var param = {
        SubOrgId: SubOrg_Id, VD: VD()
    };
    $.ajax({
        url: CashierLite.Settings.GetPrintCustomizeStyles,
        data: param,
        type: "GET",
        success: function (data) {
            try {
                var v = [];
                v = data[0].HeaderInfo.split('  ').join('\n').split('\n');
                $('#tblheader_1').html("");
                $('#divfooter').html("");
                $('#tblheader_1').append('<div style="text-align:' + data[0].HeaderFontAlignment + ';font-family:' + data[0].Headerfont + '; font-style:' + data[0].HeaderFontStyle + ';font-size:' + data[0].HeaderFontSize + 'px">' + data[0].HeaderText + '</div>');
                for (var i = 0; i < v.length; i++) {
                    $('#tblheader_1').append('<div style="text-align:' + data[0].HeaderFontAlignment + ';font-family:' + data[0].Headerfont + '; font-style:' + data[0].HeaderFontStyle + ';font-size:' + data[0].HeaderFontSize + 'px">' + v[i] + '</div>');
                }
                $('#divfooter').append('<div style="text-align:' + data[0].FooterFontAlignment + ';font-family:' + data[0].FooterFont + '; font-style:' + data[0].FooterFontStyle + ';font-size:' + data[0].FooterFontSize + 'px">' + data[0].FooterText + '</div>');
                $('#tblheader_2').css("text-align", data[0].BodyAlignment);
                $('#tblheader_3').css("text-align", data[0].BodyAlignment);
                $('#tblheader_5').css("text-align", data[0].BodyAlignment);
                $('#hdnISSLPRINT').val(data[0].IsPrint);
            }
            catch (a) {

            }
        },
    });
    //printLocal(orgd);
}
function printSLLocal(orgd) {
    window.localStorage.setItem('Inv', '0');
    var HashVal = (window.localStorage.getItem("hashValues"));
    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');
        getResources(HashValues[7], "POSSalesReturn");
    }
    $('#lblSubOrgName').html($('#txtSubOrganization').val());
    $('#lblDoctDate').html($('#calDocumentDate').val());
    $('#lblCreditNoteNo').html(orgd);
    $('#lblInvoiceNo').html($('#txtRefInvoiceNo').val());
    //var amt_ = Math.round($('#txtTotal').val(), 2);
    var amt_ = $('#txtTotal').val();
    $('#lblTransAmt').html(parseFloat(amt_).toFixed(2));
    //$('#myModalprint').show();
    $('#trSBInnerRow li span#lblMode').html($('#hdnNew').val());

    printCreditNote();
    $('#myModalprint').css('display', "none");
}


function printSLDiv(orgd) {

    var hashSplit;
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        hashSplit = HashValues.split('~');
    }
    if ((hashSplit[25] == '1') || hashSplit[25] == '') {
        printSLLocal(orgd);
    }
}

function getResources(langCode, pageCode) {
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.getResource,
        data: "langCode=" + langCode + "&pageCode=" + pageCode,
        success: function (resp) {
            setLabels(resp);
        }
    });
}
function setLabels(labelData) {

    jQuery("label[for='lblPwrddbyCshrLt'").html(labelData["PwrddbyCshrLt"]);
    jQuery("label[for='lblUploadUserImage'").html(labelData["UploadUserImage"]);
    jQuery("label[for='lblClose'").html(labelData["closed"]);
    jQuery("label[for='lblSubcription'").html(labelData["Renewal"]);
    jQuery("label[for='lblLocationwiseProfitAnalysis'").html(labelData["LocwiseProfitAnlys"]);
    jQuery("label[for='lblItemCategoryProfitAnalysis'").html(labelData["ItemCatgPrftAnlys"]);
    jQuery("label[for='lblSCPopPwd'").html(labelData["pSettings"]);
    jQuery("label[for='lblLanguage'").html(labelData["lang"]);
    jQuery("label[for='lblDateFormat'").html(labelData["format"]);
    jQuery("label[for='lblPopOk'").html(labelData["ok"]);
    jQuery("label[for='lblPopClose'").html(labelData["cancel"]);
    $('#txtFindGo').attr('placeholder', labelData["searchHere"]);
    jQuery("label[for='lblSettings'").html(labelData["settings"]);
    jQuery("label[for='lblThemes'").html(labelData["themes"]);
    jQuery("label[for='lblTheme1'").html(labelData["theme1"]);
    jQuery("label[for='lblTheme2'").html(labelData["theme2"]);
    jQuery("label[for='lblTheme3'").html(labelData["theme3"]);
    jQuery("label[for='lblTheme4'").html(labelData["theme4"]);
    jQuery("label[for='lblChangePassword'").html(labelData["changePassword"]);
    jQuery("label[for='lblSalesReturn'").html(labelData["salesReturn"]);
    jQuery("label[for='lblCreate'").html(labelData["create"]);
    jQuery("label[for='lblSave'").html(labelData["save"]);
    jQuery("label[for='lblEdit'").html(labelData["edit"]);
    jQuery("label[for='lblDelete'").html(labelData["delete"]);
    jQuery("label[for='lblCancel'").html(labelData["cancel"]);
    jQuery("label[for='lblStatus'").html(labelData["status"]);
    jQuery("label[for='lblToggleDropdown'").html(labelData["toggleDropdown"]);
    jQuery("label[for='lblApprove'").html(labelData["approve"]);
    jQuery("label[for='lblReject'").html(labelData["reject"]);
    jQuery("label[for='lblSaveApprove'").html(labelData["save"] + ' & ' + labelData["approve"]);
    jQuery("label[for='lblFilter'").html(labelData["filter"]);
    jQuery("label[for='lblFilterToggleDropdown'").html(labelData["toggleDropdown"]);
    jQuery("label[for='lblAll'").html(labelData["all"]);
    jQuery("label[for='lblToDay'").html(labelData["toDay"]);
    jQuery("label[for='lblYesterday'").html(labelData["yesterDay"]);
    jQuery("label[for='lblLastSevenDays'").html(labelData["lastSevenDays"]);
    jQuery("label[for='lblCurrentMonth'").html(labelData["currentMonth"]);
    jQuery("label[for='lblConfigurators'").html(labelData["configurators"]);
jQuery("label[for='lblSubConfigurators'").html(labelData["POSEwalletConSts"]);
jQuery("label[for='lblInvoiceSummary'").html(labelData["category"]);
    jQuery("label[for='lblPrintCustomize'").html(labelData["printCustamize"]);
    jQuery("label[for='lblBarcodeGenerator'").html(labelData["barCodeGenerator"]);
    jQuery("label[for='lblMasters'").html(labelData["masters"]);
    jQuery("label[for='lblTillType'").html(labelData["tillType"]);
    jQuery("label[for='lblTill'").html(labelData["till"]);
    jQuery("label[for='lblTillPermissions'").html(labelData["tillPermissions"]);
    jQuery("label[for='lblCustomer'").html(labelData["customer"]);
    jQuery("label[for='lblTenderType'").html(labelData["tenderType"]);
    jQuery("label[for='lblTransactions'").html(labelData["transactions"]);
    jQuery("label[for='lblInvoce'").html(labelData["invoice"]);
    jQuery("label[for='lblSalesReturn'").html(labelData["salesReturn"]);
    jQuery("label[for='lblPosting'").html(labelData["posting"]);
    jQuery("label[for='lblTillOpening'").html(labelData["tillOpening"]);
    jQuery("label[for='lblTillClosing'").html(labelData["tillClosing"]);
    jQuery("label[for='lblAccountPosting'").html(labelData["accountPosting"]);
    jQuery("label[for='lblReports'").html(labelData["reports"]);
    jQuery("label[for='lblInvoiceListing'").html(labelData["invoiceListing"]);
    jQuery("label[for='lblTillInvoiceListing'").html(labelData["tillInvoiceListing"]);
    jQuery("label[for='lblTenderTypeWiseCollectionDailySummary'").html(labelData["tenderTypeWiseCollectionDailySummary"]);
    jQuery("label[for='lblSalesReturns'").html(labelData["salesReturns"]);
    jQuery("label[for='lblDocumentNo'").html(labelData["documentNo"]);
    jQuery("label[for='lblDocumentDate'").html(labelData["documentDate"]);
    jQuery("label[for='lblReferenceNo'").html(labelData["refNo"]);
    jQuery("label[for='lblPrimeInformation'").html(labelData["primeInformation"]);
    jQuery("label[for='lblSubOrganization'").html(labelData["subOrganization"]);
    jQuery("label[for='lblSummaryDocNo'").html(labelData["documentNo"]);
    jQuery("label[for='lblSummDocDt'").html(labelData["documentDate"]);
    jQuery("label[for='lblRefNo'").html(labelData["refNo"]);
    jQuery("label[for='lblRefDt'").html(labelData["refDt"]);
    jQuery("label[for='lblCurrency'").html(labelData["currency"]);
    jQuery("label[for='lblRemarks'").html(labelData["remarks"]);
    jQuery("label[for='lblLinesItem'").html(labelData["linesItem"]);
    jQuery("label[for='lblItem'").html(labelData["item"]);
    jQuery("label[for='lblUOM'").html(labelData["uom"]);
    jQuery("label[for='lblRefQty'").html(labelData["refQty"]);
    jQuery("label[for='lblPrice'").html(labelData["price"]);
    jQuery("label[for='lblDisc'").html(labelData["disc"]);
    jQuery("label[for='lblQty'").html(labelData["qty"]);
    jQuery("label[for='lblAmount'").html(labelData["amount"]);
    jQuery("label[for='lblReason'").html(labelData["alert13"]);
    jQuery("label[for='lblTotal'").html(labelData["total"]);
    jQuery("label[for='lblReturnType'").html(labelData["returnType"]);
    jQuery("label[for='lblReplacement'").html(labelData["replacement"]);
    jQuery("label[for='lblRefund'").html(labelData["refunds"]);
    jQuery("label[for='lblLoyaltyPoints'").html(labelData["LoyaltyPoints"]);
    jQuery("label[for='lblDispatch'").html(labelData["Dispatch"]);
    jQuery("label[for='lblItem'").html(labelData["item"]);
    jQuery("label[for='lblReceipt'").html(labelData["receipt"]);
    jQuery("label[for='lblVATReturns'").html(labelData["VATReturns"]);

    $('#txtRefInvoiceNo').attr('placeholder', labelData["plzFill"] + ' ' + labelData["refNo"]);
    $('#lblNavigationUrl').text(labelData["pos"] + "  >>  " + labelData["transactions"] + "  >>  " + labelData["salesReturn"]);
    $('#lblLastUpdate').text(labelData["lastUpdated"]);
    $('#lblLastUpdate').css("display", "none");
    jQuery("label[for='lblAnalyticalReports'").html(labelData["AnalyticalReports"]);
    jQuery("label[for='lblItemCategorySalesAnalysis'").html(labelData["ItemCategorySalesAnalysis"]);
    jQuery("label[for='lblWeekDaysRevenue'").html(labelData["WeekdaysRevenue"]);
    jQuery("label[for='lblBucketAnalysis'").html(labelData["BucketAnalysis"]);
    jQuery("label[for='lblLocationwiseAverageInvoiceValue'").html(labelData["LocationwiseAverageInvoiceValue"]);

    $('#hdnOpenTill').val(labelData["alert1"]);
    $('#hdnNPermitAccInvoice').val(labelData["alert2"]);
    $('#hdnNAssignCountrTrans').val(labelData["alert3"]);
    $('#hdnInvalidData').val(labelData["alert4"]);
    $('#hdnPlzContactAdmin').val(labelData["alert5"]);
    $('#hdnRtnQtyNGrRefQty').val(labelData["alert6"]);
    $('#hdnSubOrgAlrt').val(labelData["alert7"]);

    $('#hdnFillDocNo').val(labelData["POSMsgDocNo"]);
    $('#hdnFillRefNo').val(labelData["plzFill"] + ' ' + labelData["refNo"]);
    $('#hdnFillRefDt').val(labelData["alert8"]);
    $('#hdnFillCurr').val(labelData["alert9"]);
    $('#hdnDelConf').val(labelData["wantToDelete"]);
    $('#hdnRtnLeastOneItem').val(labelData["alert10"]);
    $('#hdnSaveSucc').val(labelData["saveSuccess"]);
    $('#hdnDocNoExist').val(labelData["alreadyExist"]);
    $('#hdnDelSucc').val(labelData["deleteSuccess"]);
    $('#hdnErrOccer').val(labelData["errorOccer"]);
    $('#hdnChPwdSaveSucc').val(labelData["saveSuccess"]);
    $('#hdnThameChange').val(labelData["alert12"]);
    $('#hdnNew').val(labelData["new1"]);
    $('#hdnView').val(labelData["view"]);
    $('#hdnEdit').val(labelData["edit"]);
    $('#hdnDefault').val(labelData["default1"]);
    $('#hdnNotAssignCountrToTrans').val(labelData["permissionsAssisted"]);
    $('#hdnInvwithRedeemcantreturn').val(labelData["InvwithRedeemcantreturn"]);

    $('#hdnExchangeTypeSelect').val(labelData["select"]);
    $('#hdnExchangeTypeReplacement').val(labelData["replacement"]);
    $('#hdnExchangeTypeRefund').val(labelData["refunds"]);

    jQuery("label[for='lblCreditNote'").html(labelData["alert18"]);
    jQuery("label[for='lblCreditNoteDate'").html(labelData["date"]);
    jQuery("label[for='lblCreditNoteCreditNoteNo'").html(labelData["alert15"]);
    jQuery("label[for='lblCreditNoteInvoiceNo'").html(labelData["alert14"]);
    jQuery("label[for='lblCreditNoteTotal'").html(labelData["total"]);
    jQuery("label[for='lblCreditNoteCustomerSignature'").html(labelData["alert16"]);
    jQuery("label[for='lblCreditNoteAuthorisedSignature'").html(labelData["alert17"]);
    jQuery("label[for='lblModelPrint'").html(labelData["alert19"]);
    jQuery("label[for='lblCurrentInventory'").html(labelData["CurrentInventory"]);
    $('#hdnImageSave').val(labelData["ImageSave"]);
    jQuery("label[for='lblUpload'").html(labelData["upload"]);
    $('#hdnSelImage').val(labelData["SelUserImage"]);

    var v = '';
    v = v + "<option value='2'>--" + $('#hdnExchangeTypeSelect').val() + "--</option>";
    v = v + "<option value='0'>" + $('#hdnExchangeTypeReplacement').val() + "</option>";
    v = v + "<option value='1'>" + $('#hdnExchangeTypeRefund').val() + "</option>";

    $('#ddlExchangeType').append(v);


    jQuery("label[for='lblExchangeType'").html(labelData["exchangetype"]);
    jQuery("label[for='lblCustLDGR'").html(labelData["PosCustLdger"]);

    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
}