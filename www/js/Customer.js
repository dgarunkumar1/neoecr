﻿$("#divNavBarRight").attr('style', 'display:block;');
function MainCounterData() {
    window.localStorage.setItem('ctCheck', 1);
    if (window.localStorage.getItem('network') == '1') {
        GetLoginUserName();
        defaultdata();
        $('#hdnPageIndex').val('1');
        $('#hdnFilterType').val('Summary');
        GetSummaryGridData('Summary');
        $('#lblLastUpdate').css("display", "none");
    }
    else {
        window.open('DashBoard.html', '_self', 'location=no');
    }
}
GetCityData();
function GetCityData() {
    var CityMain = "";
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetCityData,
        data: "VD=" + window.localStorage.getItem("UUID") + "",
        success: function (resp) {
            var len = resp.length;
            $("#txtCity").empty();
            $("#txtCity").append("<option value=0>Select City</option>");
            for (var i = 0; i < len; i++) {
                $("#txtCity").append("<option value='" + resp[i]["CityId"] + "'>" + resp[i]["CityName"] + "</option>");
                if (CityMain != '')
                    CityMain = CityMain + resp[i]["CityId"] + '~' + resp[i]["CityName"] + '$';
                else
                    CityMain = resp[i]["CityId"] + '~' + resp[i]["CityName"] + '$';
            };
            window.localStorage.setItem("FillCityMain", CityMain);
        }, error: function (e) {

        }
    });
}

function PageValidatebyPage(pagecode) {
    if (pagecode == 'POSIORCR') {
        GetShiftDataCS();
    }
    window.localStorage.setItem('ctCheck', 1);
    if (pagecode != '') {
        var vd_ = window.localStorage.getItem("UUID");;
        var LoginId = window.localStorage.getItem("Sno");
        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.PageValidatebyPage,
            data: "uid=" + LoginId + "&CounterId=" + window.localStorage.getItem('COUNTERTYPE') + "&PageCode=" + pagecode + "&VD=" + vd_,
            success: function (resp) {

                var s_ = resp;

                var s0_ = resp.split('~');
                var s_ = s0_[1];
                var s1_ = s0_[0];

                if (s1_ == '2') {
                    window.localStorage.setItem("RoleId", "Admin");

                }
                else {
                    window.localStorage.setItem("RoleId", "User");
                }
                if (s_ == '1') {
                    if (pagecode == 'POSCUSTLDGR') {
                        getPOSPage('POSCUSTLDGR');
                    }
                    if (pagecode == 'POSINVCFG') {
                        window.open('POSPrintCustomize.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSBCODG') {
                        window.open('POSBarcodeGenerator.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSCNTTYP') {
                        window.open('POSCounterType.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSCOUDF') {
                        window.open('PosCounterDef.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSCTRPRM') {
                        window.open('POSCounterPermissions.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSCUSTM') {
                        window.open('CustomerMaster.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSTENDERTYPE') {
                        window.open('TenderTypes.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSLOYPRG') {
                        window.open('LoyaltyPoints.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSSLSRT') {
                        window.open('SalesReturn.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSDISPATCH') {
                        window.open('POSDeliveryStatusDetails.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSCOUOP') {
                        dayCloserChecking();
                    }
                    if (pagecode == 'POSCOUCL') {
                        window.open('CounterClosing.html', '_self', 'location=no');
                    }

                    if (pagecode == 'POSACPO') {

                        getPOSPage('POSACPO');
                    }
                    if (pagecode == 'POSRPTINVLST') {

                        getPOSPage('POSTINL');
                    }
                    if (pagecode == 'POSRPTSLRT') {
                        getPOSPage('POSSDSEN');
                    }
                    if (pagecode == 'POSRPTINVL') {
                        getPOSPage('POSINLS');
                    }
                    if (pagecode == 'POSRPTDCTN') {
                        getPOSPage('POSTTDS');
                    }
                    if (pagecode == 'POSRPTCINV') {
                        getPOSPage('POSCINV');
                    }
                    if (pagecode == 'POSRPTDE') {
                        window.open('POSReport.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSRPTSGRW') {
                        getPOSPage('POSSGR');
                    }
                    if (pagecode == 'POSConfigT') {
                        window.open('Configurator.html', '_self', 'location=no')
                    }
                    if (pagecode == 'POSRPTICSA') {
                        getPOSPage('POSRPTICSA');
                    }
                    if (pagecode == 'POSRPTBAR') {
                        getPOSPage('POSRPTBAR');
                    }
                    if (pagecode == 'POSRPTWDRR') {
                        getPOSPage('POSRPTWDRR');
                    }
                    if (pagecode == 'POSRPTLWATV') {
                        getPOSPage('POSRPTLWATV');
                    }
                    if (pagecode == 'POSRPTICPA') {
                        getPOSPage('POSRPTICPA');
                    }
                    if (pagecode == 'POSRPTLWPAS') {
                        getPOSPage('POSRPTLWPAS');
                    }
                    if (pagecode == 'POSTVRR') {
                        getPOSPage('POSTVRR');
                    }
                    if (pagecode == 'POSINLSM') {
                        getPOSPage('POSINLSM');
                    }
                }
                else {

                }
            },
            error: function (e) {
            }
        });

    }

}
function GetShiftDataCS() {
    window.open('posmain.html?#', '_self', 'location=no');
}
try {
    $("#txtCity").autocomplete({

        dataFilter: function (data) {
            return data
        },
        source: function (req, add) {
            window.localStorage.setItem('ctCheck', 1);
            var v = (window.localStorage.getItem("FillCityMain"));
            jsonObj = [];
            if (v != "") {
                var cus_ = v.split('$');
                for (var i = 0; i < cus_.length; i++) {
                    var cusChek = cus_[i].split('~');
                    if (cusChek.length > 1) {
                        item = {
                        }
                        item["value"] = cusChek[1];
                        item["data"] = cusChek[0];
                        jsonObj.push(item);
                    }
                }
            }
            add($.map(jsonObj, function (el) {
                var re = $.ui.autocomplete.escapeRegex(req.term);
                if (re.trim() != "") {
                    if (el.value.toLowerCase().indexOf(re.toLowerCase()) != -1) {
                        return {
                            label: el.value,
                            val: el.data
                        };
                    }
                }
                else {
                    return {
                        label: el.value,
                        val: el.data
                    };
                }
            }));
        },
        change: function (e, i) {
            if (i.item) {
            }
            else {
                $('#txtCity').val('');
                $('#hdnCityId').val('');
            }
        },
        select: function (event, ui) {
            $('#txtCity').val(ui.item.label);
            $('#hdnCityId').val(ui.item.val);

        }
    });

}
catch (a) {

}

function defaultdata() {
    window.localStorage.setItem('ctCheck', 1);
    var Paging = ""; var CompanyId = ""; var SubOrgId = ''; var Orgcode = ''; var OBS = ''; var CashierID = window.localStorage.getItem("CashierID"); var Userid = '';
    var Lang = '';
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        getResources(hashSplit[7], "POSCustomerMaster");
        CompanyId = hashSplit[0];
        SubOrgId = hashSplit[1];
        OBS = hashSplit[9];
        Orgcode = hashSplit[17];
        Userid = hashSplit[14];
        Lang = hashSplit[7];
        Paging = hashSplit[19];
    }
    $('#CompanyId').val(CompanyId);
    $('#hdnSubOrganization').val(SubOrgId);
    $('#hdnOBS').val(OBS);
    $('#hdnUserId').val(Userid);
    //$('#hdnSubOrganization').val("004c6087-45a7-468b-bdd8-46b4a1dcf647");
    $('#txtSubOrganization').val(Orgcode);
    $('#hdnLanguage').val(Lang);
    $('#hdnPaging').val(Paging);
    $('#txtSubOrganization').prop("disabled", true);
    $('#Content_two').css("display", "none");
    $('#Create').css("display", "block");
    // $('#Create').css("display", "block");
    $('#Edit').css("display", "none");
    $('#Cancel').css("display", "none");
    $('#btnSave').css("display", "none");
    $('#Delete').css("display", "none");
    $('#Status').css("display", "none");
    $('#divActive').css("display", "none");
}


function IsNumeric_Item(e) {
    window.localStorage.setItem('ctCheck', 1);
    var specialKeys = new Array();
    specialKeys.push(8);
    var keyCode = e.which ? e.which : e.keyCode

    var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1 || keyCode == 46);
    if (ret == false) {
        return ret;
    }



    var input_ = $("#txtPrice").val();
    if (keyCode == 46) {
        if (input_.indexOf('.') != -1) {
            return false;
        }
        if (input_.split('.')[0].length == 0) {

            return false;
        }

    }
    var position_ = e.target.selectionStart;
    var inuputArray_ = input_.split('');


    input_ = getKeypressValues(inuputArray_, e.key, position_)

    if (input_.indexOf('.') == -1) {
        if (input_.length <= 8) {
            return true;
        }
        if (keyCode == 46)
            return true;

        else {
            return false;

        }

    }
    else {
        if (input_.split('.')[0].length <= 8 && input_.split('.')[1].length <= 2) {
            return true;
        } else return false;



    }

}
function IsNumericKey(e) {
    var specialKeys = new Array();
    specialKeys.push(8);
    var keyCode = e.which ? e.which : e.keyCode

    var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1 || keyCode == 46);
    if (ret == false) {
        return ret;
    }
    else {

    }

}
function VD1() {
    window.localStorage.setItem('ctCheck', 1);
    var vd_ = window.localStorage.getItem("UUID");;

    return vd_;
}
function GetLoginUserName() {
    window.localStorage.setItem('ctCheck', 1);
    var sess = CashierLite.Session.getInstance().get();
    if (sess != null) {
        if (sess.userName != "") {
            document.getElementById("lblName").innerHTML = sess.userName;
        }
        else {
            window.open('Login.html', '_self', 'location=no');
        }
    }
    else {
        window.open('Login.html', '_self', 'location=no');
    }
    theamLoad();
}
function theamLoad() {
    window.localStorage.setItem('ctCheck', 1);
    if (window.localStorage.getItem("Theam") != null)
        $('head').append('<link rel="stylesheet" href=' + window.localStorage.getItem("Theam") + ' type="text/css" />');
}

//For Invoice Link
function GetShiftData() {
    window.localStorage.setItem('ctCheck', 1);
    if (window.localStorage.getItem("CashierID") != null) {

        var SubOrgId = ''; var CashierID = window.localStorage.getItem("CashierID");
        var HashValues = (window.localStorage.getItem("hashValues"));
        if (HashValues != null && HashValues != '') {
            var hashSplit = HashValues.split('~');
            SubOrgId = hashSplit[1];
        }
        var vd_ = window.localStorage.getItem("UUID");;

        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.ShiftCheck,
            data: "CashierID=" + CashierID + "&OrgId=" + SubOrgId + "&VD=" + vd_,
            success: function (resp) {
                var result = resp;
                if (result.length > 0) {
                    var ACTIVE = result[0].ACTIVE;
                    var COUNTERID = result[0].COUNTERID;
                    var COUNTERNAME = result[0].COUNTERNAME;
                    var COUNTERNUMBER = result[0].COUNTERNUMBER;
                    var COUNTERTYPE = result[0].COUNTERTYPE;
                    var End_Time = result[0].End_Time;
                    var Is_Next_Day = result[0].Is_Next_Day;
                    var SHIFTID = result[0].SHIFTID;
                    var Shift_Code = result[0].Shift_Code;
                    var Shift_Name = result[0].Shift_Name;
                    var Start_Time = result[0].Start_Time;
                    var StageAlert = result[0].StageAlert;

                    window.localStorage.setItem('ACTIVE', result[0].ACTIVE);
                    window.localStorage.setItem('COUNTERID', result[0].COUNTERID);
                    window.localStorage.setItem('COUNTERNAME', result[0].COUNTERNAME);
                    window.localStorage.setItem('COUNTERNUMBER', result[0].COUNTERNUMBER);
                    window.localStorage.setItem('COUNTERTYPE', result[0].COUNTERTYPE);
                    window.localStorage.setItem('End_Time', result[0].End_Time);
                    window.localStorage.setItem('Is_Next_Day', result[0].Is_Next_Day);
                    window.localStorage.setItem('SHIFTID', result[0].SHIFTID);
                    window.localStorage.setItem('Shift_Code', result[0].Shift_Code);
                    window.localStorage.setItem('Shift_Name', result[0].Shift_Name);
                    window.localStorage.setItem('Start_Time', result[0].Start_Time);
                    var StageAlert = result[0].StageAlert;
                    if (StageAlert == '1') {
                        if (ACTIVE.toLowerCase() == 'y') {
                            window.open('posmain.html', '_self', 'location=no');
                        }
                        else {

                            navigator.notification.alert($('#hdnNPermInv').val(), "", "neo ecr", "Ok");
                        }
                    }
                    else if (StageAlert == '2') {

                        navigator.notification.alert($('#hdnNAsnCounterTran').val(), "", "neo ecr", "Ok");

                    }
                }
                else {

                    navigator.notification.alert($('#hdnNPermInv').val(), "", "neo ecr", "Ok");

                }
            },
            error: function (e) {


                navigator.notification.alert($('#hdnNPermInv').val(), "", "neo ecr", "Ok");


            }
        });
    }
    else {

        navigator.notification.alert($('#hdnNPermInv').val(), "", "neo ecr", "Ok");
    }
}
function Edit() {
    window.localStorage.setItem('ctCheck', 1);
    //$('#trSBInnerRow li span#lblMode').html("Edit");
    $('#trSBInnerRow li span#lblMode').html($('#hdnEdit').val());
    $("#btnCreate").attr('style', 'display:none;');
    $("#ShowMenuModes").attr('style', 'display:block;');
    $("#btnSave").attr('style', 'display:block;');
    $("#btnEdit").attr('style', 'display:none;');
    $("#btnDelete").attr('style', 'display:none;');
    $("#btnDelete").attr('style', 'border-left:1px solid #cac8c8;');
    $("#btnCancel").attr('style', 'display:block;');
    $("#divStatusGroup").attr('style', 'display:none;');
    $('#txtCounterName').removeAttr("disabled", "disabled");
    $("#txtDescription").removeAttr("disabled", "disabled");
    //$('#txtPhoneNo').removeAttr("disabled", "disabled");
    //$('#ddlCounterType').removeAttr("disabled", "disabled");
    $("#divNavBarRight").attr('style', 'display:none;');

    $('#PrimeInfo').addClass('open');
    $('#divPrimeInformation').attr('style', 'display:block');


    $("#txtCounterName").removeAttr("disabled", "disabled");

    $("#txtAliasName").removeAttr("disabled", "disabled");
    $("#txtPrice").removeAttr("disabled", "disabled");
    $("#txtBarcode").removeAttr("disabled", "disabled");
    $("#txtUOM").attr("disabled", "disabled");
    $('#ChckTaxOnMRPAp').prop('disabled', false);
    $('#ChckTaxAp').prop('disabled', false);
    if ($('#ChckTaxAp').prop('checked') == false) {
        $('#ChckTaxOnMRPAp').prop('disabled', true);
        $('#lblTaxValue').css("dispaly", "none");
        $('#txtTaxValue').css("dispaly", "none");

    }
    else {
        $('#txtTaxValue').removeAttr("disabled", "disabled");
        //$('#txtTaxValue').val(15);
    }

    $("#inp").removeAttr("disabled", "disabled");
}
//End
//Delete function
function Delete() {
    window.localStorage.setItem('ctCheck', 1);
    if ($('#hdnTransId').val() != "") {
        $('#SummaryGrid').css("display", "block");
        $('#NewPage').css("display", "none");
        $("#btnCreate").attr('style', 'display:block;');
        $("#ShowMenuModes").attr('style', 'display:none;');
        $("#divNavBarRight").attr('style', 'display:block;');
    }
    if (confirm($('#hdnDeleteConfirm').val())) {
        var vd_ = window.localStorage.getItem("UUID");;
        //var vdSplit_ = window.location.pathname.split('/');
        //if (vdSplit_.length > 2) {
        //    vd_ = vdSplit_[1];
        //}
        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.DeleteCounter,
            data: { Counter_Id: $('#hdnTransId').val(), LastUpdatedDate: $('#hdnLastUpdatedDate').val(), VD: vd_ },
            success: function (resp) {
                var result = resp.split(',');
                if (result[0] == "100000") {
                    GetSummaryGridData('Summary');
                    navigator.notification.alert($('#txtCounterCode').val() + ' ' + $('#hdnRecDelSucc').val(), "", "neo ecr", "Ok");
                    ClearScreen();

                }
            },
            error: function (e) {
                navigator.notification.alert($('#hdnInvalData').val(), "", "neo ecr", "Ok");
            }
        });
    } else {
        return false;
    }
}
//End
function ClearScreen() {
    window.localStorage.setItem('ctCheck', 1);
    $('#txtCounterCode').val("");
    $('#txtCounterName').val("");
    $("#txtDescription").val("");
    $('#txtBarcode').val("");
    //$('#ddlCounterType').val("0");
    $('#hdnTransId').val("");
    $('#hdnLastUpdatedDate').val("");
    $('#hdnScreenMode').val("");
    $("#txtUOM").val("");
    $("#txtAliasName").val("");
    $("#txtPrice").val("");
    $("#txtTaxValue").val(15);
    document.getElementById("b64").innerHTML = "";
    //document.getElementById("img").src = "images/IMGItem.png";
    //$('#inp')[0].value = ""
    $('#ChckTaxAp').prop('checked', true);
    $('#ChckTaxOnMRPAp').prop('checked', true);
    $('#ChckTaxOnMRPAp').prop('disabled', false);
    $('#lblTaxValue').css('display', 'block');
    $('#txtTaxValue').css('display', 'block');
    $('#txtUOM').val('EAC/Each');
    $('#hdnUOMId').val('1009');
}
function prevItem() {
    var pageIndexCreate = parseFloat($('#hdnPageIndex').val()) - 1;
    if (parseFloat(pageIndexCreate) == 0) {
        pageIndexCreate = '1';
    }
    $('#hdnPageIndex').val(pageIndexCreate);
    var FilterType = $("#hdnFilterType").val();
    GetSummaryGridData(FilterType);
    window.localStorage.setItem('ctCheck', 1);
}
function nextItem() {
    window.localStorage.setItem('ctCheck', 1);
    var pageIndexCreate = parseFloat($('#hdnPageIndex').val()) + 1;

    $('#hdnPageIndex').val(pageIndexCreate);
    var FilterType = $('#hdnFilterType').val();
    GetSummaryGridData(FilterType + '~Index');

}
function GetSummaryGridData(FilterType) {
    window.localStorage.setItem('ctCheck', 1);
    var Index = $('#hdnPageIndex').val();
    var MethodType = "";
    var FindGo = "";
    var QryType = "";
    if (FilterType == 'FindGo') {
        FindGo = $('#txtFindGo').val();
        Index = 1;

    }
    if (FilterType == 'Summary') {
        $('#txtFindGo').val('');
    }

    else {
        FindGo = $('#txtFindGo').val();
    }
    if (FilterType.indexOf('Index') != -1) {
        FilterType = FilterType.split('~')[0];
        MethodType = "next";
    }
    var HashVal = (window.localStorage.getItem("hashValues"));
    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');
    }
    var SummaryTable = '';
    Jsonobj = [];
    var SubOrg = {};
    SubOrg.SubOrgID = $('#hdnBranchId').val();
    var vd_ = window.localStorage.getItem("UUID");;
    SubOrg.VD = vd_;
    SubOrg.Index = Index;
    SubOrg.FilterType = FilterType;
    SubOrg.FindGoVal = FindGo;
    SubOrg.item_Id = "";
    Jsonobj.push(SubOrg);
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetAllCustomersWithPaging,
        // data: "OrgId=" + HashValues[15] + "&VD=" + VD(),
        data: "SubOrg=" + JSON.stringify(Jsonobj),
        success: function (resp) {
            var GridData = resp;
            if (GridData.length > 0) {
                window.localStorage.setItem("CustSummaryFilterGrid", JSON.stringify(GridData))
                var SummaryTable = '';
                for (var i = 0; i < GridData.length; i++) {
                    var CustId = GridData[i].CUSTOMER_ID;
                    var CustCode = GridData[i].PHONENO;
                    var CustName = GridData[i].CUSTOMER_NAME;
                    var Status = GridData[i].STATUS;
                    SummaryTable += "<tr onclick=GridClick(\'" + CustId + "\',\'" + Status + "\')><td style='display:none;'>" + CustId + "</td><td>" + CustCode + "</td><td>" + CustName + "</td><td>" + Status + "</td></tr>"

                };
                $('#tblSummaryGrid tbody').remove();
                SummaryTable += "<tbody>" + SummaryTable + "</tbody>";
                $('#tblSummaryGrid').append(SummaryTable);
                window.localStorage.setItem("CustSummaryGrid", JSON.stringify(SummaryTable));
                //GetPaging();
                $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());
            }
            else {

                $('#tblSummaryGrid tbody').remove();
                SummaryTable += "<tbody>" + SummaryTable + "</tbody>";
                $('#tblSummaryGrid').append(SummaryTable);
                if (MethodType == "next") {
                    var pageIndexCreate = parseFloat($('#hdnPageIndex').val()) - 1;
                    $('#hdnPageIndex').val(pageIndexCreate);
                }
            }
        },
        error: function (e) {
        }
    });
}
function GetPaging1() {
    let options = {
        numberPerPage: 10, //Cantidad de datos por pagina
        goBar: true, //Barra donde puedes digitar el numero de la pagina al que quiere ir
        pageCounter: true, //Contador de paginas, en cual estas, de cuantas paginas
    };
    let filterOptions = {
        el: '#searchBox' //Caja de texto para filtrar, puede ser una clase o un ID
    };
    paginate.init('#tblSummaryGrid', options, filterOptions);
    window.localStorage.setItem('ctCheck', 1);
}
function GetPaging() {
    $('table#tblSummaryGrid').each(function () {
        var currentPage = 0;
        var numPerPage = $('#hdnPaging').val();
        var $table = $(this);
        $table.bind('repaginate', function () {
            $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
        });
        $table.trigger('repaginate');
        var numRows = $table.find('tbody tr').length;
        var numPages = Math.ceil(numRows / numPerPage);
        $("#DivPager").remove();
        var $pager = $('<div id="DivPager" class="pager"></div>');
        for (var page = 0; page < numPages; page++) {
            $('<span class="page-number"></span>').text(page + 1).bind('click', {
                newPage: page
            }, function (event) {
                currentPage = event.data['newPage'];
                $table.trigger('repaginate');
                $(this).addClass('active').siblings().removeClass('active');
            }).appendTo($pager).addClass('clickable');
        }
        $pager.insertAfter($table).find('span.page-number:first').addClass('active');
    });
}

function GetPaging1() {
    let options = {
        numberPerPage: 10, //Cantidad de datos por pagina
        goBar: true, //Barra donde puedes digitar el numero de la pagina al que quiere ir
        pageCounter: true, //Contador de paginas, en cual estas, de cuantas paginas
    };

    let filterOptions = {
        el: '#searchBox' //Caja de texto para filtrar, puede ser una clase o un ID
    };

    paginate.init('#tblSummaryGrid', options, filterOptions);
    window.localStorage.setItem('ctCheck', 1);
}
$('#btnAll').click(function () {

    $("#btnCancel").click();

    GetSummaryGridData('Summary');
});
$('#btnToday').click(function () {
    $("#btnCancel").click();
    FilterSummaryGrid('Today', 'CREATEDDATE')
});
$('#btnYesterday').click(function () {
    $("#btnCancel").click();
    FilterSummaryGrid('Yesterday', 'CREATEDDATE')
});
$('#btnLast7days').click(function () {
    $("#btnCancel").click();
    FilterSummaryGrid('Last7', 'CREATEDDATE')

});
$('#btnCurrentMonth').click(function () {
    $("#btnCancel").click();
    FilterSummaryGrid('CMonth', 'CREATEDDATE')
});
$('#divActives').click(function () {
    $("#btnCancel").click();
    FilterSummaryGrid('Active', 'Status')
});
$('#divInActives').click(function () {
    $("#btnCancel").click();
    FilterSummaryGrid('Inactive', 'Status')
});

function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}

function Save(upCheck) {
    window.localStorage.setItem('ctCheck', 1);
    var Mode = $("#hdnMode").val();
    if ($("#hdnTransId").val() == "" || $("#hdnTransId").val() == null || $("#hdnMode").val() == 'New') {
        Mode = "Create";
        $("#hdnTransId").val(guid());
    }
    else if ($("#hdnMode").val() == "Edit")
        Mode = "Update";
    else if ($("#hdnMode").val() == "Delete")
        Mode = "Delete";
    else if ($("#hdnMode").val() == "InActive")
        Mode = "Active";
    else if ($("#hdnMode").val() == "Active")
        Mode = "InActive";
    var CustomerData = {};
    if ($('#hdnTransId').val() == "") {
        CustomerData["CUSTOMER_ID"] = "";
    }
    else {
        CustomerData["CUSTOMER_ID"] = $('#hdnTransId').val();
    }
    CustomerData["SUB_ORG_ID"] = $('#hdnSubOrganization').val();
    CustomerData["Mobile"] = $('#txtMobile').val();
    CustomerData["CUSTOMER_NAME"] = $('#txtName').val();
    CustomerData["ALIAS_NAME"] = $("#txtAlias").val();
    CustomerData["CUSTOMER_TYPE_ID"] = '';
    CustomerData["CUSTOMER_GRP_ID"] = '';
    CustomerData["COMPANY_NAME"] = '';
    CustomerData["CONTACT_PERSON"] = '';
    var CurDate = new Date();
    var DateWithFormate = CurDate.getFullYear() + '-' + (CurDate.getMonth() + 1) + '-' + CurDate.getDate() + ' ' + CurDate.getHours() + ':' + CurDate.getMinutes() + ':' + CurDate.getSeconds() + ':' + CurDate.getMilliseconds();
    CustomerData["EFFECTIVE_DATE"] = DateWithFormate;
    CustomerData["ORDER_PRIORITY"] = '';
    CustomerData["CUST_SERVICE_REP"] = '';
    CustomerData["REFERED_BY"] = '';
    CustomerData["INCOTERMS_PAYMENT"] = '';
    CustomerData["PAYMENT_MODE"] = '';
    CustomerData["SALES_TYPE"] = '';

    CustomerData["HEAD_OFFICE"] = '';
    CustomerData["SALES_PERSON_ID"] = '';//ID
    CustomerData["CURRENCY_ID"] = 1;
    CustomerData["LANGUAGE_ID"] = HashValues[7];
    CustomerData["NEAREST_DP_ID"] = '';
    CustomerData["PREF_SHIPPER"] = '';
    CustomerData["REMARKS"] = '';
    CustomerData["CPER_DESIG"] = '';

    CustomerData["BRANCH_ID"] = '1000';
    CustomerData["OBS"] = HashValues[9];


    CustomerData["TRANS_TYPE"] = 'PO';
    CustomerData["BIRTH_DATE"] = DateWithFormate;
    CustomerData["Marriage_Date"] = DateWithFormate;
    CustomerData["PARENTCUSTID"] = '';
    CustomerData["CATEGORY"] = 'SO';

    CustomerData["Email"] = $('#txtEmail').val();
    CustomerData["Address"] = $('#txtAddress').val();
    CustomerData["City_ID"] = $('#hdnCityId').val();
    CustomerData["OpeningBalnce"] = parseFloat($('#txtOpeningBalance').val());
    CustomerData["UserId"] = $('#hdnUserId').val();
    CustomerData["OBS"] = $('#hdnOBS').val();
    CustomerData["LANGID"] = $('#hdnLanguage').val();
    //CustomerData["LASTUPDATEDDATE"] = $('#hdnLastUpdatedDate').val();
    //CustomerData["LASTUPDATEDBY"] = HashValues[14];
    //CustomerData["LASTUPDATEDDATE"] = DateWithFormate;
    CustomerData["LASTUPDATEDBY"] = HashValues[14];
    CustomerData["CREATEDDATE"] = DateWithFormate;
    CustomerData["CREATEDBY"] = HashValues[14];
    CustomerData["COMPANYID"] = '1000';
    CustomerData["BRANCHID"] = '1000';
    CustomerData["STATUS"] = "Y";

    if (Mode == 'Create') {

        CustomerData["LASTUPDATEDBY"] = HashValues[14];
        CustomerData["LASTUPDATEDDATE"] = DateWithFormate;
    }
    else {
        CustomerData["LASTUPDATEDDATE"] = $('#hdnLastUpdatedDate').val();
    }
    //if (upCheck != '') {
    //    if ($('#hdnScreenMode').val() == "Active")
    //        CustomerData["Status"] = "Y";
    //    else
    //        CustomerData["Status"] = "N";
    //}
    if ($("#hdnStatus").val() == "Active" && $("#hdnMode").val() == "InActive")
        CustomerData["PROCESS_STATUS"] = '1';
    else
        CustomerData["PROCESS_STATUS"] = '2';
    var vd_ = window.localStorage.getItem("UUID");;
    CustomerData["insertType"] = '1';
    //item["img"] = document.getElementById("b64").innerHTML;
    CustomerData["VD"] = vd_;
    objCustomerData = []
    objCustomerData.push(CustomerData);

    var CustomerParams = new Object();
    CustomerParams.Mode = Mode;
    CustomerParams.CustomerData = JSON.stringify(objCustomerData);
    //CustomerParams.CustomerAddress = JSON.stringify(objCustomerAddress);
    CustomerParams.VD = VD();

    //Put the customer object into a container
    var ComplexObject = new Object();
    //The property name "WebMethodArgName" must match the web method argument "WebMethodArgName"!
    ComplexObject.CustData = CustomerParams;

    //Stringify and verify the object is foramtted as expected
    var data = JSON.stringify(ComplexObject);


    $.ajax({
        type: 'POST',
        url: CashierLite.Settings.SaveCustomerData,
        data: data,
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function (resp) {
            window.localStorage.setItem('ctCheck', 1);
            if (resp != null) {
                if (resp.SaveCustomerMstDataResult == '100000') {//Success Record
                    if ($("#hdnMode").val() == 'Delete') {

                        navigator.notification.alert($("#txtName").val().trim() + ' ' + $('#hdnDelSucc').val(), "", "neo ecr", "Ok");
                        $("#btnCancel").click();

                    }
                    else if ($("#hdnMode").val() == 'Edit') {

                        navigator.notification.alert($("#txtName").val().trim() + ' ' + $('#hdnSaveSucc').val(), "", "neo ecr", "Ok");
                        $("#btnCancel").click();
                    }
                    else {

                        navigator.notification.alert($("#txtName").val().trim() + ' ' + $('#hdnSaveSucc').val(), "", "neo ecr", "Ok");
                        $('#PrimeInfo').addClass('open');
                        $('#ShippingInfo').removeClass('open');
                        $('#divPrimeInformation').attr('style', 'display:block');
                        $('#divShippingInformation').attr('style', 'display:none');
                    }
                    ClearScreen();
                    // $("#btnCancel").click();
                    GetSummaryGridData();
                    return false;
                }
                else if (resp.SaveCustomerMstDataResult == '100001') {//Duplicate Mobile Number

                    navigator.notification.alert($("#txtMobile").val().trim() + ' ' + 'Mobile No Already exists.', "", "neo ecr", "Ok");
                    return false;
                }
                else if (resp.SaveCustomerMstDataResult == 'POS10002') {//Deleted Record

                    navigator.notification.alert($("#txtName").val().trim() + ' ' + $('#hdnDelSucc').val(), "", "neo ecr", "Ok");
                    //ClearTotalScreen();
                    $("#btnCancel").click();
                    GetSummaryGridData();
                    return false;
                }
                if (resp.SaveCustomerMstDataResult == 'POSActive') {//Active Record


                    navigator.notification.alert($("#txtName").val().trim() + ' ' + $('#hdnRecActSucc').val(), "", "neo ecr", "Ok");
                    $("#btnCancel").click();
                    GetSummaryGridData();
                    return false;
                }
                if (resp.SaveCustomerMstDataResult == 'POSInActive') {//Deactive Record

                    navigator.notification.alert($("#txtName").val().trim() + ' ' + $('#hdnRecInactSucc').val(), "", "neo ecr", "Ok");
                    // ClearTotalScreen();
                    //ClearScreen();
                    $("#btnCancel").click();
                    GetSummaryGridData();
                    return false;
                }
                //else {//Error Record
                //    $("#hdnTransId").val('');
                //    var $textAndPic = $('<div></div>');
                //    $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                //    //$textAndPic.append('<div class="alert-message">Error occured while Saving the Data</div>');
                //    $textAndPic.append('<div class="alert-message">' + $('#hdnErrCodeOccer').val() + '</div>');
                //    BootstrapDialog.alert($textAndPic);
                //    return false;
                //}
            }

        }
    });

    $('#PrimeInfo').addClass('open');
    $('#divPrimeInformation').attr('style', 'display:block');
}
function IsNumericUP(e) {
    window.localStorage.setItem('ctCheck', 1);
    var specialKeys = new Array();
    specialKeys.push(8);
    var keyCode = e.which ? e.which : e.keyCode
    var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1 || keyCode == 46);
    if (ret == false) {
        return ret;
    }
    else {

    }

} function IsNumericUPT(e) {
    window.localStorage.setItem('ctCheck', 1);
    var a = '#chek_tap_' + e.target.id.split('_')[0] + '_M';
    if ($(a)[0].disabled == true) {
        return false;
    }
    var specialKeys = new Array();
    specialKeys.push(8);
    var keyCode = e.which ? e.which : e.keyCode
    var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1 || keyCode == 46);
    if (ret == false) {
        return ret;
    }
    else {

    }

}

function taxMove(data) {
    window.localStorage.setItem('ctCheck', 1);
    if (data.children.length == 0) {
        var id_ = "<input type='text' value=" + data.innerHTML + ">";
        data.innerHTML = id_;
    }
    //else
    //{
    //    var dback_ = data.children[0].value;
    //    data.innerHTML = '';
    //    var id_ = "<input type='text' value=" + dback_ + ">";
    //    data.innerHTML = id_;
    //    
    //}
    //alert('daa');
}
function taxMoveout(data) {
    window.localStorage.setItem('ctCheck', 1);
    var data_ = data;
    if (data.children.length == 1) {
        var dback_ = data.children[0].value;
        data.innerHTML = '';
        var id_ = dback_;
        data.innerHTML = id_;
    }
}

function RefreshData() {
    window.localStorage.setItem('ctCheck', 1);
    location.reload();
}
$("#btnActive").click(function () {

    $('#hdnScreenMode').val("Active");
    if ($("#hdnTransId").val() != "" && $("#hdnMode").val() == 'Active')
        Save('Mode');
});
//InActive Record
$("#btnInActive").click(function () {
    $('#hdnScreenMode').val("InActive");
    if ($("#hdnTransId").val() != "" && $("#hdnMode").val() == 'InActive')
        Save('Mode');
});
$("#btnCreate").click(function () {
    window.localStorage.setItem('ctCheck', 1);
    $("#hdnTransId").val('');
    $("#CustAddress_GUID").val('');
    ClearScreen();
    $("#hdnMode").val('New');
    EnableFormfileds(false);
    ShowMenuModes($("#hdnMode").val());
    $('#lblLastUpdate').css("display", "none");
    //$('#trSBInnerRow li span#lblLastUpdate').html("");
    $('#trSBInnerRow li span#lblLastUpdatedUser').html("");
    $('#trSBInnerRow li span#lblLastUpdatedDate').html("");
    //$('#trSBInnerRow li span#lblMode').html("New");
    $('#trSBInnerRow li span#lblMode').html($('#hdnNew').val());
    var HashVal = (window.localStorage.getItem("hashValues"));

    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');
        $('#txtSubOrganization').val(HashValues[10]);
        orgName = HashValues[10];
        $('#hdnSubOrganization').val(HashValues[1]);
        $('#hdnParentSubOrganization').val(HashValues[0]);
        $('#hdnPaging').val(HashValues[19]);
        //GetBaseCurrnecyBySubOrg(HashValues[16]);
    }
    //$("#btnInActive").prop('disabled', true);
    //$("#btnActive").prop('disabled', false);
    $('#PrimeInfo').addClass('open');
    $('#ShippingInfo').removeClass('open');
    $('#divPrimeInformation').attr('style', 'display:block');
    $('#divShippingInformation').attr('style', 'display:none');
});
$("#btnCancel").click(function () {
    $("#btnCreate").attr('style', 'display:block;');
    $("#ShowMenuModes").attr('style', 'display:none;');
    $("#SummaryGrid").attr('style', 'display:block;');
    $("#NewPage").attr('style', 'display:none;');

    //$("#btnSave").attr('style', 'display:none;');
    //$("#btnEdit").attr('style', 'display:none;');
    //$("#btnCancel").attr('style', 'display:none;');
    //$("#PrimeInfo").addClass("active-link");
    //$("#ShippingInfo").addClass("deactive-link");
    $("#hdnTransId").val('');
    $("#CustAddress_GUID").val('');
    $("#hdnMode").val('Cancel');
    ShowMenuModes($("#hdnMode").val());
    $('#lblLastUpdate').css("display", "none");
    //$('#trSBInnerRow li span#lblLastUpdate').html("");
    $('#trSBInnerRow li span#lblLastUpdatedUser').html("");
    $('#trSBInnerRow li span#lblLastUpdatedDate').html("");
    //$('#trSBInnerRow li span#lblMode').html("Default");
    $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());
});
$("#btnEdit").click(function () {
    $("#hdnMode").val('Update');
    $("#hdnMode").val('Edit');
    EnableFormfileds(false);
    $('#txtEmail').prop("disabled", false);
    $('#txtOpeningBalance').prop("disabled", false);
    ShowMenuModes($("#hdnMode").val());
    //$('#trSBInnerRow li span#lblMode').html("Edit");
    $('#trSBInnerRow li span#lblMode').html($('#hdnEdit').val());
    $('#PrimeInfo').addClass('open');
    $('#ShippingInfo').removeClass('open');
    $('#divPrimeInformation').attr('style', 'display:block');
    $('#divShippingInformation').attr('style', 'display:none');
});
$('#btnSave').click(function () {
    window.localStorage.setItem('ctCheck', 1);
    if ($("#txtMobile").prop("disabled") == false && $('#txtMobile').val() == "") {

        navigator.notification.alert($("#hdnPleaseEnter").val().trim() + ' ' + $('#hdnMobile').val(), "", "neo ecr", "Ok");
        return false;
    }
    if ($('#txtName').val() == "" && $('#txtName').val().trim().length == 0) {

        navigator.notification.alert($('#hdnEntrName').val(), "", "neo ecr", "Ok");
        return false;
    }

    //if ($('#txtAlias').val() == "" && $('#txtEmail').val().trim().length == 0) {

    // navigator.notification.alert("Please enter Alias", "", "neo ecr", "Ok"); 

    //    return false;
    //}
    //if ($('#txtEmail').val() == "" && $('#txtAddress').val().trim().length == 0) {
    //    var $textAndPic = $('<div></div>');
    //    $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
    //    //$textAndPic.append('<div class="alert-message">' + $('#hdnphbarcode').val() + '</div>');
    //    $textAndPic.append('<div class="alert-message">' + "Please enter Email" + '</div>');
    //navigator.notification.alert("Please enter Alias", "", "neo ecr", "Ok"); 
    //    BootstrapDialog.alert($textAndPic);
    //    return false;
    //}
    if ($('#txtEmail').val() != "") {
        if (checkEmail($('#txtEmail').val()) == false) {

            navigator.notification.alert("Please Enter Valid E-mail Address", "", "neo ecr", "Ok");
            $('#txtEmail').focus();
            return false;
        }
    }
    //if ($('#txtAddress').val() == "" && $('#txtAddress').val().trim().length == 0) {
    //    var $textAndPic = $('<div></div>');
    //    $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
    //    //$textAndPic.append('<div class="alert-message">' + $('#hdnphbarcode').val() + '</div>');
    //    $textAndPic.append('<div class="alert-message">' + "Please enter Address" + '</div>');
    //    BootstrapDialog.alert($textAndPic);
    //    return false;
    //}
    //if ($('#txtCity').val() == "" && $('#txtCity').val().trim().length == 0) {
    //    var $textAndPic = $('<div></div>');
    //    $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
    //    //$textAndPic.append('<div class="alert-message">' + $('#hdnphbarcode').val() + '</div>');
    //    $textAndPic.append('<div class="alert-message">' + "Please fill City" + '</div>');
    //    BootstrapDialog.alert($textAndPic);
    //    return false;
    //}
    //if ($('#txtOpeningBalance').val() == "" && $('#txtOpeningBalance').val().trim().length == 0) {
    //    var $textAndPic = $('<div></div>');
    //    $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
    //    //$textAndPic.append('<div class="alert-message">' + $('#hdnphbarcode').val() + '</div>');
    //    $textAndPic.append('<div class="alert-message">' + "Please enter Opening Balance" + '</div>');
    //    BootstrapDialog.alert($textAndPic);
    //    return false;
    //}   
    Save('');
});
$('#btnEdit').click(function () {
    window.localStorage.setItem('ctCheck', 1);
    Edit();
});
$('#btnDelete').click(function () {
    window.localStorage.setItem('ctCheck', 1);
    Delete();
});

$('#hdnCounterNo').val();
function GetAutoCodeGen(str, iScreenCode) {
    var vd_ = window.localStorage.getItem("UUID");;

    window.localStorage.setItem('ctCheck', 1);
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetAutoGenHashTableCheck,
        data: "SubOrgId=" + str + "&iScreenCode=" + iScreenCode + "&VD=" + vd_,
        success: function (resp) {
            var CounterNo = resp;
            if (CounterNo.length == 1) {
                $('#hdnCounterNo').val(CounterNo[0].PageName);
            }
        },
        error: function (e) {
        }
    });
}
function GenerateCode(strOrgCode, strVarCode, strParamType, strTable, strColumn, USERID, BRANCHID, COMPANYID, LANGUAGE) {
    var vd_ = window.localStorage.getItem("UUID");;
    //var vdSplit_ = window.location.pathname.split('/');
    //if (vdSplit_.length > 2) {
    //    vd_ = vdSplit_[1];
    //}
    window.localStorage.setItem('ctCheck', 1);
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GenerateCode,
        data: "strOrgCode=" + strOrgCode + "&strVarCode=" + strVarCode + "&strParamType=" + strParamType + "&strTable=" + strTable + "&strColumn=" + strColumn + "&USERID=" + USERID + "&BRANCHID=" + BRANCHID + "&COMPANYID=" + COMPANYID + "&LANGUAGE=" + LANGUAGE + "&VD=" + vd_,

        success: function (resp) {
            var CounterNo = resp;
            if (CounterNo.length == 1) {
                $('#txtCounterOpeningNo').val(CounterNo[0].PageName);
            }
        },
        error: function (e) {
        }
    });
}
function getResources(langCode, pageCode) {
    window.localStorage.setItem('ctCheck', 1);
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.getResource,
        data: "langCode=" + langCode + "&pageCode=" + pageCode,
        success: function (resp) {
            setLabels(resp);
        }
    });
}

function setLabels(labelData) {
    $('#hdnPleaseEnter').val(labelData["PleaseEnter"]);
    $('#hdnMobile').val(labelData["mobile"]);
    $('#txtCity').attr('placeholder', labelData["plzFill"] + " " + labelData["city"]);
    $('#txtEmail').attr('placeholder', labelData["PleaseEnter"] + " " + labelData["email"]);
    $('#txtAddress').attr('placeholder', labelData["PleaseEnter"] + " " + labelData["addr1"]);
    $('#txtName').attr('placeholder', labelData["PleaseEnter"] + " " + labelData["name"]);
    $('#txtAlias').attr('placeholder', labelData["PleaseEnter"] + " " + labelData["Alias"]);
    $('#txtMobile').attr('placeholder', labelData["PleaseEnter"] + " " + labelData["mobile"]);
    jQuery("label[for='lblAnalyticalReports'").html(labelData["AnalyticalReports"]);
    jQuery("label[for='lblItemCategorySalesAnalysis'").html(labelData["ItemCategorySalesAnalysis"]);
    jQuery("label[for='lblWeekDaysRevenue'").html(labelData["WeekdaysRevenue"]);
    jQuery("label[for='lblBucketAnalysis'").html(labelData["BucketAnalysis"]);
    jQuery("label[for='lblLocationwiseAverageInvoiceValue'").html(labelData["LocationwiseAverageInvoiceValue"]);
    jQuery("label[for='lblAlias'").html(labelData["Alias"]);
    jQuery("label[for='lblLocationwiseProfitAnalysis'").html(labelData["LocwiseProfitAnlys"]);
    jQuery("label[for='lblItemCategoryProfitAnalysis'").html(labelData["ItemCatgPrftAnlys"]);
    jQuery("label[for='lblSCPopPwd'").html(labelData["pSettings"]);
    jQuery("label[for='lblLanguage'").html(labelData["lang"]);
    jQuery("label[for='lblDateFormat'").html(labelData["format"]);
    jQuery("label[for='lblPopOk'").html(labelData["ok"]);
    jQuery("label[for='lblPopClose'").html(labelData["cancel"]);
    $('#txtFindGo').attr('placeholder', labelData["searchHere"]);

    jQuery("label[for='lblCreate'").html(labelData["create"]);
    jQuery("label[for='lblSettings'").html(labelData["settings"]);
    jQuery("label[for='lblThemes'").html(labelData["themes"]);
    jQuery("label[for='lblTheme1'").html(labelData["theme1"]);
    jQuery("label[for='lblTheme2'").html(labelData["theme2"]);
    jQuery("label[for='lblTheme3'").html(labelData["theme3"]);
    jQuery("label[for='lblTheme4'").html(labelData["theme4"]);
    jQuery("label[for='lblChangePassword'").html(labelData["changePassword"]);
    jQuery("label[for='lblCustomer'").html(labelData["customer"]);
    jQuery("label[for='lblSave'").html(labelData["save"]);
    jQuery("label[for='lblEdit'").html(labelData["edit"]);
    jQuery("label[for='lblDelete'").html(labelData["delete"]);
    jQuery("label[for='lblCancel'").html(labelData["cancel"]);
    jQuery("label[for='lblStatus'").html(labelData["status"]);
    jQuery("label[for='lblToggleDropdown'").html(labelData["toggleDropdown"]);
    jQuery("label[for='lblActive'").html(labelData["active"]);
    jQuery("label[for='lblInActive'").html(labelData["inactive"]);
    jQuery("label[for='lblFilter'").html(labelData["filter"]);
    jQuery("label[for='lblFilterToggleDropdown'").html(labelData["toggleDropdown"]);
    jQuery("label[for='lblAll'").html(labelData["all"]);
    jQuery("label[for='lblToDay'").html(labelData["toDay"]);
    jQuery("label[for='lblYesterday'").html(labelData["yesterDay"]);
    jQuery("label[for='lblLastSevenDays'").html(labelData["lastSevenDays"]);
    jQuery("label[for='lblCurrentMonth'").html(labelData["currentMonth"]);
    jQuery("label[for='lblToggleActive'").html(labelData["active"]);
    jQuery("label[for='lblToggleInactive'").html(labelData["inactive"]);
    jQuery("label[for='lblConfigurators'").html(labelData["configurators"]);
    jQuery("label[for='lblPrintCustomize'").html(labelData["printCustamize"]);
    jQuery("label[for='lblBarcodeGenerator'").html(labelData["barCodeGenerator"]);
    jQuery("label[for='lblMasters'").html(labelData["masters"]);
    jQuery("label[for='lblTillType'").html(labelData["tillType"]);
    jQuery("label[for='lblTill'").html(labelData["till"]);
    jQuery("label[for='lblTillPermissions'").html(labelData["tillPermissions"]);
    jQuery("label[for='lblSubConfigurators'").html(labelData["POSEwalletConSts"]);
    jQuery("label[for='lblItem'").html(labelData["item"]);
    jQuery("label[for='lblReceipt'").html(labelData["receipt"]);
    jQuery("label[for='lblInvoiceSummary'").html(labelData["category"]);
    jQuery("label[for='lblVATReturns'").html(labelData["VATReturns"]);
    jQuery("label[for='lblCurrentInventory'").html(labelData["CurrentInventory"]);
    jQuery("label[for='lblTenderType'").html(labelData["tenderType"]);
    jQuery("label[for='lblTransactions'").html(labelData["transactions"]);
    jQuery("label[for='lblInvoce'").html(labelData["invoice"]);
    jQuery("label[for='lblSalesReturn'").html(labelData["salesReturn"]);
    jQuery("label[for='lblPosting'").html(labelData["posting"]);
    jQuery("label[for='lblTillOpening'").html(labelData["tillOpening"]);
    jQuery("label[for='lblTillClosing'").html(labelData["tillClosing"]);
    jQuery("label[for='lblAccountPosting'").html(labelData["accountPosting"]);
    jQuery("label[for='lblReports'").html(labelData["reports"]);
    jQuery("label[for='lblInvoiceListing'").html(labelData["invoiceListing"]);
    jQuery("label[for='lblTillInvoiceListing'").html(labelData["tillInvoiceListing"]);
    jQuery("label[for='lblTenderTypeWiseCollectionDailySummary'").html(labelData["tenderTypeWiseCollectionDailySummary"]);
    jQuery("label[for='lblSalesReturns'").html(labelData["salesReturns"]);
    jQuery("label[for='lblCode'").html(labelData["code"]);
    jQuery("label[for='lblName'").html(labelData["name"]);
    jQuery("label[for='lblProcessStatus'").html(labelData["processStatus"]);
    jQuery("label[for='lblPrimeInformation'").html(labelData["primeInformation"]);
    jQuery("label[for='lblOrganization'").html(labelData["subOrganization"]);
    jQuery("label[for='lblAddress'").html(labelData["addr1"]);
    jQuery("label[for='lblCity'").html(labelData["city"]);
    jQuery("label[for='lblMobile'").html(labelData["mobile"]);
    jQuery("label[for='lblEmail'").html(labelData["email"]);
    jQuery("label[for='lblOpeningBalance'").html(labelData["OpeningBalance"]);
    $('#hdnEntrName').val(labelData["alert8"]);

    $('#hdnNPermInv').val(labelData["alert1"]);
    $('#hdnNAsnCounterTran').val(labelData["alert2"]);
    $('#hdnInvalData').val(labelData["alert3"]);
    $('#hdnContactAdmin').val(labelData["alert4"]);
    //$('#hdnRecSaveSucc').val(labelData[""]);
    $('#hdnSubOrgAlert').val(labelData["alert5"]);
    $('#hdnMsgSubOrg').val(labelData["alert6"]);
    $('#hdnFillCode').val(labelData["alert7"]);
    $('#hdnFillCounterName').val(labelData["alert8"]);
    $('#hdnFillGroup').val(labelData["alert9"]);
    $('#hdnFillType').val(labelData["alert10"]);
    $('#hdnFillSalesReg').val(labelData["alert11"]);
    $('#hdnFillCurr').val(labelData["alert12"]);
    $('#hdnFillDOB').val(labelData["alert13"]);
    $('#hdnFutureDtAlert').val(labelData["alert14"]);
    $('#hdnMrgDtFuterDt').val(labelData["alert15"]);
    $('#hdnMrgDtNDOB').val(labelData["alert16"]);
    $('#hdnFillAddr1').val(labelData["alert17"]);
    $('#hdnFillCity').val(labelData["alert18"]);
    $('#hdnFillState').val(labelData["alert19"]);
    $('#hdnFillZipCode').val(labelData["alert20"]);
    $('#hdnFillPhNo').val(labelData["alert21"]);
    $('#hdnFillDelConf').val(labelData["alert22"]);
    $('#hdnDelSucc').val(labelData["alert23"]);
    $('#hdnSaveSucc').val(labelData["alert24"]);
    $('#hdnCodeAlreadyExist').val(labelData["alert25"]);
    $('#hdnRecActSucc').val(labelData["alert26"]);
    $('#hdnRecInactSucc').val(labelData["alert27"]);
    $('#hdnErrCodeOccer').val(labelData["alert28"]);
    $('#hdnChPwdSaveSucc').val(labelData["alert24"]);
    $('#hdnThameChange').val(labelData["alert29"]);
    $('#hdnNew').val(labelData["new1"]);
    $('#hdnView').val(labelData["view"]);
    $('#hdnEdit').val(labelData["edit"]);
    $('#hdnDefault').val(labelData["default1"]);
    $('#hdnNotAssignCountrToTrans').val(labelData["permissionsAssisted"]);
    $('#txtShippingToCity').attr('placeholder', labelData["plzFill"] + " " + labelData["city"]);
    $('#lblNavigationUrl').text(labelData["pos"] + "  >>  " + labelData["masters"] + "  >>  " + labelData["customer"]);
    $('#lblLastUpdate').text(labelData["lastUpdated"]);
    $('#lblLastUpdate').css("display", "none");
    jQuery("label[for='lblCustLDGR'").html(labelData["PosCustLdger"]);

}


function GridClick(CustomerId, status) {
    $("#btnCreate").attr('style', 'display:block;');
    $("#ShowMenuModes").attr('style', 'display:block;');
    $("#btnSave").attr('style', 'display:none;');
    $("#btnEdit").attr('style', 'display:block;');
    $("#btnDelete").attr('style', 'display:block;');
    $("#btnCancel").attr('style', 'display:block;');
    $("#SummaryGrid").attr('style', 'display:none;');
    $("#NewPage").attr('style', 'display:block;');
    $("#divStatusGroup").attr('style', 'display:block;');
    $("#hdnTransId").val(CustomerId);
    if (status == 'Inactive') {
        $("#btnInActive").addClass("disabledHyperlink");
        $("#btnActive").removeClass("disabledHyperlink");
    } else {
        $("#btnActive").addClass("disabledHyperlink");
        $("#btnInActive").removeClass("disabledHyperlink");
    }
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetCustomerMstDoubleClick,
        data: "CustId=" + CustomerId + "&VD=" + VD(),
        success: function (resp) {
            debugger;
            var GridData = JSON.parse(resp);
            //for (var i = 0; i < GridData.QATABLE038.length; i++) {
            if (GridData.QATABLE038.length > 0) {
                //Primary Info..
                //$('#txtSubOrganization').val(GridData.QATABLE038[0].ORGCODE + " / " + GridData.QATABLE038[0].ORGNAME);
                $('#txtSubOrganization').val(GridData.QATABLE038[0].ORGNAME);
                $('#hdnSubOrganization').val(GridData.QATABLE038[0].ORG_ID);

                $('#txtMobile').val(GridData.QATABLE038[0].PHONENO);
                $('#txtName').val(GridData.QATABLE038[0].CUSTOMER_NAME);
                $('#txtAlias').val(GridData.QATABLE038[0].ALIAS_NAME);
                $('#txtEmail').val(GridData.QATABLE038[0].EMAIL);
                $('#txtAddress').val(GridData.QATABLE038[0].ADDRESS);
                $('#txtCity').val(GridData.QATABLE038[0].CITY);
                if (GridData.QATABLE038[0].OPENINGBALANCE != 0)
                    $('#txtOpeningBalance').val(GridData.QATABLE038[0].OPENINGBALANCE);
                else
                    $('#txtOpeningBalance').val('0.00');

                $('#hdnLastUpdatedDate').val(GridData.QATABLE038[0].LASTUPDATEDDATE);

                $('#hdnStatus').val(status);
                $('#hdnTransId').val(GridData.QATABLE038[0].CUSTOMER_ID);
                if ($('#hdnStatus').val() == 'Active') {
                    $("#hdnMode").val('View');

                }
                else {
                    $("#hdnMode").val('InActive');

                }
                ShowMenuModes($('#hdnMode').val());
                //$("#hdnMode").val('Update');
                EnableFormfileds(true);
                //$('#lblLastUpdate').css("display", "none");
                //$('#trSBInnerRow li span#lblLastUpdate').html($('#trSBInnerRow li span#lblLastUpdate').html());
                //$('#trSBInnerRow li span#lblLastUpdatedUser').html($('#hdnLastUpdatedByName').val());
                //var Hours = convertHours(GridData.QATABLE038[0].LASTUPDATEDDATE.split('T')[1]);

                //var CurDate = new Date(Date.parse((GridData.QATABLE038[0].LASTUPDATEDDATE).replace('T', ' ')));
                //var DateWithFormate = CurDate.getFullYear() + '/' + (CurDate.getMonth() + 1) + '/' + CurDate.getDate();

                // ConvertDateWithFormat($('#hdnDateFormate').val(), DateWithFormate, "hdnFormatLastUpdatedDate")

                // $('#trSBInnerRow li span#lblLastUpdatedDate').html($('#hdnFormatLastUpdatedDate').val() + ' ' + Hours);
                //$('#trSBInnerRow li span#lblLastUpdatedDate').html(CurDate.getMonth()+1 +'/'+CurDate.getDate()+'/'+CurDate.getFullYear()+ ' ' + Hours);
                //$('#trSBInnerRow li span#lblMode').html("View");
                //$('#trSBInnerRow li span#lblMode').html($('#hdnView').val());
                //$('#lblLastUpdate').css("display", "block");
            }
        },
        error: function (e) {
        }
    });
    $('#PrimeInfo').addClass('open');
    $('#ShippingInfo').removeClass('open');
    $('#divPrimeInformation').attr('style', 'display:block');
    $('#divShippingInformation').attr('style', 'display:none');
}
function FilterSummaryGrid(ControlData, ColumnData) {
    $('#txtFindGo').val('');
    FindGo = [];
    FindGo = JSON.parse((window.localStorage.getItem("CustSummaryFilterGrid")));
    var SummaryTable;
    for (var i = 0; i < FindGo.length; i++) {
        var CustId = FindGo[i].CUSTOMER_ID;
        var CustCode = FindGo[i].CUSTOMER_CODE;
        var CustName = FindGo[i].CUSTOMER_NAME;
        var Status = FindGo[i].STATUS;

        if (ColumnData == 'Status') {
            var Status_ = FindGo[i].STATUS;
            if (Status_.toLowerCase() == ControlData.toLowerCase()) {
                SummaryTable = SummaryTable + "<tr onclick=GridClick(\'" + CustId + "\',\'" + Status + "\')><td style='display:none;'>" + CustId + "</td><td>" + CustCode + "</td><td>" + CustName + "</td><td>" + Status + "</td></tr>"
            }
        }
        else if (ColumnData == 'CREATEDDATE') {
            var CDate = FindGo[i].CREATEDDATE;
            var Date_ = CDate.split('-')
            //var CREATEDDATE = new Date(CDate);
            var CurDate = new Date();
            if (ControlData == 'Today') {
                if (Date_[0] == CurDate.getDate()) {
                    SummaryTable = SummaryTable + "<tr onclick=GridClick(\'" + CustId + "\',\'" + Status + "\')><td style='display:none;'>" + CustId + "</td><td>" + CustCode + "</td><td>" + CustName + "</td><td>" + Status + "</td></tr>"
                }
            }
            else if (ControlData == 'Yesterday') {
                if ((Date_[0]) == (CurDate.getDate() - 1)) {
                    SummaryTable = SummaryTable + "<tr onclick=GridClick(\'" + CustId + "\',\'" + Status + "\')><td style='display:none;'>" + CustId + "</td><td>" + CustCode + "</td><td>" + CustName + "</td><td>" + Status + "</td></tr>"
                }
            }
            else if (ControlData == 'Last7') {
                if (Date_[0] >= (CurDate.getDate() - 7)) {
                    SummaryTable = SummaryTable + "<tr onclick=GridClick(\'" + CustId + "\',\'" + Status + "\')><td style='display:none;'>" + CustId + "</td><td>" + CustCode + "</td><td>" + CustName + "</td><td>" + Status + "</td></tr>"
                }
            }
            else if (ControlData == 'CMonth') {
                if ((Date_[1]) == (CurDate.getMonth() + 1)) {
                    SummaryTable = SummaryTable + "<tr onclick=GridClick(\'" + CustId + "\',\'" + Status + "\')><td style='display:none;'>" + CustId + "</td><td>" + CustCode + "</td><td>" + CustName + "</td><td>" + Status + "</td></tr>"
                }
            }
        }
    }
    $('#tblSummaryGrid tbody').remove();
    SummaryTable += "<tbody>" + SummaryTable + "</tbody>";
    $('#tblSummaryGrid').append(SummaryTable);
    //Paging
    // GetPaging();
}
$(document).ready(function () {
    $('#txtFindGo').keyup(function () {
        window.localStorage.setItem('ctCheck', 1);
        result = [];
        result = JSON.parse((window.localStorage.getItem("CustSummaryFilterGrid")));
        if ($('#txtFindGo').val().trim() == "" || result.length <= 0) {

            GetSummaryGridData('Summary');
            return false;
        }
        else {

            GetSummaryGridData('FindGo');
        }
    })
    //$('#txtFindGo').keyup(function () {
    //    FindGo = [];
    //    FindGo = JSON.parse((window.localStorage.getItem("CustSummaryFilterGrid")));
    //    if ($('#txtFindGo').val().trim() == "" || FindGo.length <= 0) {
    //        GetSummaryGridData();
    //        return false;
    //    }
    //    else {
    //        var SummaryTable;
    //        for (var i = 0; i < FindGo.length; i++) {
    //            var CustId = FindGo[i].CUSTOMER_ID;
    //            //var CustCode = FindGo[i].CUSTOMER_CODE;
    //            var CustCode = FindGo[i].PHONENO;
    //            var CustName = FindGo[i].CUSTOMER_NAME;
    //            var Status = FindGo[i].STATUS;
    //            if (CustCode.toLowerCase().indexOf($('#txtFindGo').val().trim().toLowerCase()) != -1 || CustName.toLowerCase().indexOf($('#txtFindGo').val().trim().toLowerCase()) != -1 || Status.toLowerCase().indexOf($('#txtFindGo').val().trim().toLowerCase())!=-1) {

    //                SummaryTable = SummaryTable + "<tr onclick=GridClick(\'" + CustId + "\')><td style='display:none;'>" + CustId + "</td><td>" + CustCode + "</td><td>" + CustName + "</td><td>" + Status + "</td></tr>"
    //            }
    //        }
    //        $('#tblSummaryGrid tbody').remove();
    //        SummaryTable += "<tbody>" + SummaryTable + "</tbody>";
    //        $('#tblSummaryGrid').append(SummaryTable);
    //        //$('#txtFindGo').val('');
    //        //Paging
    //        GetPaging();
    //    }
    //})

});

$("#txtMobile").bind('cut copy paste', function (e) {
    e.preventDefault();
});

$("#txtMobile").keypress(function (event) {
    if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        //alert('hello');
        if ((event.which != 46 || $(this).val().indexOf('.') != -1)) {
            return false;
        }
        event.preventDefault();
    }
    if (this.value.indexOf(".") > -1 && (this.value.split('.')[1].length > 1)) {
        return false;
        event.preventDefault()
    }
});
$("#txtOpeningBalance").bind('cut copy paste', function (e) {
    e.preventDefault();
});

$("#txtOpeningBalance").keypress(function (event) {
    if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        //alert('hello');
        if ((event.which != 46 || $(this).val().indexOf('.') != -1)) {
            return false;
        }
        event.preventDefault();
    }
    if (this.value.indexOf(".") > -1 && (this.value.split('.')[1].length > 1)) {
        return false;
        event.preventDefault()
    }
});
function ShowMenuModes(Mode) {
    if (Mode == 'New') {
        $("#btnCreate").attr('style', 'display:block;');
        $("#btnCreate").attr('style', 'display:none;');
        $("#ShowMenuModes").attr('style', 'display:block;');
        $("#btnSave").attr('style', 'display:block;');
        $("#btnEdit").attr('style', 'display:none;');
        $("#btnDelete").attr('style', 'display:block;');
        $("#btnCancel").attr('style', 'display:block;');
        $("#SummaryGrid").attr('style', 'display:none;');
        $("#NewPage").attr('style', 'display:block;');
        $("#btnDelete").attr('style', 'border-left:1px solid #cac8c8;display:none;');
        $("#divStatusGroup").attr('style', 'display:none;');
        //$("#txtFindGo").attr('style', 'display:none;');
        //$("#divIntuitive").attr('style', 'display:none;');
        $("#divNavBarRight").attr('style', 'display:none;');
    }
    if (Mode == 'Edit') {
        $("#btnCreate").attr('style', 'display:none;');
        $("#ShowMenuModes").attr('style', 'display:block;');
        $("#btnSave").attr('style', 'display:block;');
        $("#btnEdit").attr('style', 'display:none;');
        $("#btnDelete").attr('style', 'display:block;');
        $("#btnCancel").attr('style', 'display:block;');
        $("#SummaryGrid").attr('style', 'display:none;');
        $("#NewPage").attr('style', 'display:block;');
        $("#divStatusGroup").attr('style', 'display:none;');
        $("#divNavBarRight").attr('style', 'display:none;');
        $("#btnDelete").attr('style', 'border-left:1px solid #cac8c8;');
    }
    if (Mode == 'Cancel') {
        $("#btnCreate").attr('style', 'display:block;');
        $("#ShowMenuModes").attr('style', 'display:none;');
        $("#SummaryGrid").attr('style', 'display:block;');
        $("#NewPage").attr('style', 'display:none;');
        $("#divStatusGroup").attr('style', 'display:none;');
        //$("#txtFindGo").attr('style', 'display:block;');
        //$("#divIntuitive").attr('style', 'display:block;');
        $("#divNavBarRight").attr('style', 'display:block;');

    }
    if (Mode == 'Active') {
        $("#btnCreate").attr('style', 'display:block;');
        $("#ShowMenuModes").attr('style', 'display:block;');
        $("#btnSave").attr('style', 'display:none;');
        $("#btnEdit").attr('style', 'display:block;');
        $("#btnDelete").attr('style', 'display:block;');
        $("#btnCancel").attr('style', 'display:block;');

        $("#SummaryGrid").attr('style', 'display:none;');
        $("#NewPage").attr('style', 'display:block;');
        $("#divStatusGroup").attr('style', 'display:block;');
        $("#divNavBarRight").attr('style', 'display:none;');
    }
    if (Mode == 'InActive') {
        $("#btnCreate").attr('style', 'display:block;');
        $("#ShowMenuModes").attr('style', 'display:block;');
        $("#btnSave").attr('style', 'display:none;');
        $("#btnEdit").attr('style', 'display:none;');
        $("#btnDelete").attr('style', 'display:none;');
        $("#btnCancel").attr('style', 'display:block;');
        $("#btnCancel").attr('style', 'display:block;border-radius:4px');

        $("#SummaryGrid").attr('style', 'display:none;');
        $("#NewPage").attr('style', 'display:block;');
        $("#divStatusGroup").attr('style', 'display:block;');
        $("#divNavBarRight").attr('style', 'display:none;');
    }
    if (Mode == 'View') {
        //$("#txtFindGo").attr('style', 'display:none;');
        //$("#divIntuitive").attr('style', 'display:none;');
        $("#divNavBarRight").attr('style', 'display:none;');

    }
}
//Active Record
$("#btnActive").click(function () {
    $("#hdnMode").val('InActive');
    if ($("#hdnTransId").val() != "" && $("#hdnMode").val() == 'InActive')
        //GetNewData();
        Save('Mode');
});
//InActive Record
$("#btnInActive").click(function () {
    $("#hdnMode").val('Active');
    if ($("#hdnTransId").val() != "" && $("#hdnMode").val() == 'Active')
        // GetNewData();
        Save('Mode');
});

function ClearScreen() {
    $('#txtMobile').val('');
    $('#txtName').val('');
    $('#txtAlias').val('');
    $('#txtEmail').val('');
    $('#txtAddress').val('');
    $('#txtCity').val('');
    $('#hdnCityId').val('');
    $('#txtOpeningBalance').val('0.00');
    $('#txtFindGo').val('');
}
function EnableFormfileds(Status) {
    //
    $('#txtName').prop("disabled", Status);
    $('#txtMobile').prop("disabled", Status);
    $('#txtName').prop("disabled", Status);
    $('#txtAlias').prop("disabled", Status);
    $('#txtEmail').prop("disabled", Status);
    $('#txtAddress').prop("disabled", Status);
    $('#txtCity').prop("disabled", Status);
    $('#txtOpeningBalance').prop("disabled", Status);
    if ($("#hdnMode").val().trim() == 'View' || $("#hdnMode").val().trim() == 'Edit' || $("#hdnMode").val().trim() == 'Active') {
        $('#txtCode').prop("disabled", true);
        $('#txtMobile').prop("disabled", true);
        $('#txtName').prop("disabled", true);
        $('#txtEmail').prop("disabled", true);
        $('#txtOpeningBalance').prop("disabled", true);
    }

    if ($('#hdnAutoCode').val() == 'Y' && $('#txtCode').val().trim() == '') {
        $('#txtCode').prop("disabled", true);
    }
}
function checkEmail(text) {
    var match = /[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-zA-Z0-9]/;
    return match.test(text);
}