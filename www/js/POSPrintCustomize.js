﻿function PageLoad() {
   
    window.localStorage.setItem('ctCheck', 1);

    GetLoginUserName();
    defaultData();

    if (window.localStorage.getItem("RoleId") == 'Admin') {
        $('#txtSubOrganization').removeAttr("readonly", "true");

    }
    else {
        $('#txtSubOrganization').attr("readonly", "true");
        $("#txtSubOrganization").attr("disabled", "disabled");

    }
    //window.localStorage.setItem("RoleId", "Admin");
}



function GetSubOrgData() {
   
    window.localStorage.setItem('ctCheck', 1);
    var OBSCode = (window.localStorage.getItem("hashValues"));
    if (OBSCode != null && OBSCode != '') {
        var OBS = OBSCode.split('~');
        var ss = OBS[0];
        var vd_ = window.localStorage.getItem("UUID");
        //var vdSplit_ = window.location.pathname.split('/');
        //if (vdSplit_.length > 2) {
        //    vd_ = vdSplit_[1];
        //}
        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.GetSubOrgData,
            data: { OBSCode: ss, VD: vd_ },
            success: function (resp) {
                var OrgData = resp;
                var MainData = '';
                jsonObjOrg = [];
                for (var i = 0; i < OrgData.length; i++) {
                    var OrgId = OrgData[i].ORG_ID;
                    var OrgName = OrgData[i].OrgCode;
                    var ParentOrgId = OrgData[i].Top_Parent;
                    var BasedCur_Id = OrgData[i].Base_Curr_Id;
                    Org = {}
                    Org["code"] = OrgId;
                    Org["name"] = OrgName;
                    Org["ParentSubId"] = ParentOrgId;
                    Org["BasedCur_Id"] = BasedCur_Id;
                    jsonObjOrg.push(Org);
                };
                window.localStorage.setItem("FillSubOrg", jsonObjOrg);
            },
            error: function (e) {
            }
        });
    }

}

$("#txtSubOrganization").autocomplete({

   
    dataFilter: function (data) { return data },
    source: function (req, add) {
        var OBSCode = (window.localStorage.getItem("hashValues"));
        if (OBSCode != null && OBSCode != '') {
            var OBS = OBSCode.split('~');
            var ss = OBS[0];
        }
        var jsonObjOrg = [];
        var vd_ = window.localStorage.getItem("UUID");
        //var vdSplit_ = window.location.pathname.split('/');
        //if (vdSplit_.length > 2) {
        //    vd_ = vdSplit_[1];
        //}
        $.ajax({
            url: CashierLite.Settings.GetSubOrgData,
            data: { OBSCode: ss, VD: vd_ },
            dataType: "json",
            type: 'GET',
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                add($.map(data, function (el) {
                    return {
                        label: el.OrgCode,
                        val: el.ORG_ID
                    };

                }));
            },
        });

    },
    change: function (e, i) {
        if (i.item) {
        }
        else {
            window.localStorage.setItem('ctCheck', 1);
            $("#txtSubOrganization").val('');
            $("#hdnBranchId").val('');


        }
    },
    select: function (event, ui) {
        window.localStorage.setItem('ctCheck', 1);
        $('#txtSubOrganization').val(ui.item.label);
        $('#hdnBranchId').val(ui.item.val);
        LoadPrintCustomize();
    }

});
function defaultData() {
    window.localStorage.setItem('ctCheck', 1);
    var SubOrgId = ''; var Orgcode = ''; var OBS = ''; var CashierID = window.localStorage.getItem("CashierID"); var Userid = '';
    var Lang = ''; var BaseCurr_Id = ''; var CURR_CODE = ''; var CompanyId = '';
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        getResources(hashSplit[7], "POSPrintCustomize");
        CompanyId = hashSplit[0];
        SubOrgId = hashSplit[1];
        OBS = hashSplit[9];
        Orgcode = hashSplit[17];
        Userid = hashSplit[14];
        Lang = hashSplit[7];
        BaseCurr_Id = hashSplit[16];
        CURR_CODE = hashSplit[18];
    }
    $('#CompanyId').val(CompanyId);
    $('#hdnBranchId').val(SubOrgId);
    $('#hdnOBS').val(OBS);
    $('#hdnUserId').val(Userid);
    $('#txtSubOrganization').val(Orgcode);
    $('#hdnLanguage').val(Lang);
    Curr_Id = BaseCurr_Id;
    Curr_Code = CURR_CODE;

    LoadPrintCustomize();
    // $("#txtCurrency").prop("disabled", true);

}
function GetLoginUserName() {
    window.localStorage.setItem('ctCheck', 1);
    var sess = CashierLite.Session.getInstance().get();
    if (sess != null) {
        if (sess.userName != "") {
            document.getElementById("lblName").innerHTML = sess.userName;
        }
        else {
            window.open('Login.html', '_self', 'location=no');
        }
    }
    else {
        window.open('Login.html', '_self', 'location=no');
    }
    theamLoad();
}
function theamLoad() {
    window.localStorage.setItem('ctCheck', 1);
    if (window.localStorage.getItem("Theam") != null)
        $('head').append('<link rel="stylesheet" href=' + window.localStorage.getItem("Theam") + ' type="text/css" />');
}
//For Invoice Link
function GetShiftData() {
    window.localStorage.setItem('ctCheck', 1);
    if (window.localStorage.getItem("CashierID") != null) {

        var SubOrgId = ''; var CashierID = window.localStorage.getItem("CashierID");
        var HashValues = (window.localStorage.getItem("hashValues"));
        if (HashValues != null && HashValues != '') {
            var hashSplit = HashValues.split('~');
            SubOrgId = hashSplit[1];
        }
        var vd_ = window.localStorage.getItem("UUID");
        //var vdSplit_ = window.location.pathname.split('/');
        //if (vdSplit_.length > 2) {
        //    vd_ = vdSplit_[1];
        //}
        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.ShiftCheck,
            data: "CashierID=" + CashierID + "&OrgId=" + SubOrgId + "&VD=" + vd_,
            success: function (resp) {
                var result = resp;
                if (result.length > 0) {
                    var ACTIVE = result[0].ACTIVE;
                    var COUNTERID = result[0].COUNTERID;
                    var COUNTERNAME = result[0].COUNTERNAME;
                    var COUNTERNUMBER = result[0].COUNTERNUMBER;
                    var COUNTERTYPE = result[0].COUNTERTYPE;
                    var End_Time = result[0].End_Time;
                    var Is_Next_Day = result[0].Is_Next_Day;
                    var SHIFTID = result[0].SHIFTID;
                    var Shift_Code = result[0].Shift_Code;
                    var Shift_Name = result[0].Shift_Name;
                    var Start_Time = result[0].Start_Time;
                    var StageAlert = result[0].StageAlert;

                    window.localStorage.setItem('ACTIVE', result[0].ACTIVE);
                    window.localStorage.setItem('COUNTERID', result[0].COUNTERID);
                    window.localStorage.setItem('COUNTERNAME', result[0].COUNTERNAME);
                    window.localStorage.setItem('COUNTERNUMBER', result[0].COUNTERNUMBER);
                    window.localStorage.setItem('COUNTERTYPE', result[0].COUNTERTYPE);
                    window.localStorage.setItem('End_Time', result[0].End_Time);
                    window.localStorage.setItem('Is_Next_Day', result[0].Is_Next_Day);
                    window.localStorage.setItem('SHIFTID', result[0].SHIFTID);
                    window.localStorage.setItem('Shift_Code', result[0].Shift_Code);
                    window.localStorage.setItem('Shift_Name', result[0].Shift_Name);
                    window.localStorage.setItem('Start_Time', result[0].Start_Time);
                    var StageAlert = result[0].StageAlert;
                    if (StageAlert == '1') {
                        if (ACTIVE.toLowerCase() == 'y') {
                            window.open('posmain.html?#', '_self', 'location=no');
                        }
                        else {
                            //var $textAndPic = $('<div></div>');
                            //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                            ////$textAndPic.append('<div class="alert-message">dont have permission to access invoice</div>');
                            //$textAndPic.append('<div class="alert-message">' + $('#hdnNPermInv').val() + '</div>');
                            //BootstrapDialog.alert($textAndPic);
                            navigator.notification.alert($('#hdnNPermInv').val(), "", "neo ecr", "Ok");
                        }
                    }
                    else if (StageAlert == '2') {
                        //var $textAndPic = $('<div></div>');
                        //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                        ////$textAndPic.append('<div class="alert-message">You are not assigned any Counter so you can\'t do the Transaction</div>');
                        //$textAndPic.append('<div class="alert-message">' + $('#hdnNAsnCounterTran').val() + '</div>');
                        //BootstrapDialog.alert($textAndPic);
                        navigator.notification.alert($('#hdnNAsnCounterTran').val(), "", "neo ecr", "Ok");

                    }
                }
                else {
                    //var $textAndPic = $('<div></div>');
                    //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                    ////$textAndPic.append('<div class="alert-message">dont have permission to access invoice</div>');
                    //$textAndPic.append('<div class="alert-message">' + $('#hdnNPermInv').val() + '</div>');
                    //BootstrapDialog.alert($textAndPic);
                    navigator.notification.alert($('#hdnNPermInv').val(), "", "neo ecr", "Ok");

                }
            },
            error: function (e) {
                //var $textAndPic = $('<div></div>');
                //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                ////$textAndPic.append('<div class="alert-message">Invalid Data</div>');
                //$textAndPic.append('<div class="alert-message">' + $('#hdnInvalData').val() + '</div>');
                //BootstrapDialog.alert($textAndPic);
                navigator.notification.alert($('#hdnInvalData').val(), "", "neo ecr", "Ok");

            }
        });
    }
    else {
        //var $textAndPic = $('<div></div>');
        //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        ////$textAndPic.append('<div class="alert-message">Please contact Admin.</div>');
        //$textAndPic.append('<div class="alert-message">' + $('#hdnContactAdmin').val() + '</div>');
        //BootstrapDialog.alert($textAndPic);
        navigator.notification.alert($('#hdnContactAdmin').val(), "", "neo ecr", "Ok");

    }


}

$('#btnPreview').click(function () {
    window.localStorage.setItem('ctCheck', 1);
    if ($('#rbnEnglish').is(":checked"))
    {
        $('#myModalprint').modal();
        var splitHeader = "";
        var v = [];
        v = $('#txtHeaderinfo').val().split('  ').join('\n').split('\n');

        var header = $('#divheader');
        var Footer = $('#divfooter');
        header.html("");
        Footer.html("");
        var HeaderAlignment = $("#headeralignment option:selected").val();
        var FooterAlignment = $("#footeralignment option:selected").val();
        header.append('<div style="text-align:' + HeaderAlignment + ';font-family:' + $('#hdrfont option:selected').val() + '; font-style:' + $('#fontstyle option:selected').val() + ';font-size:' + $('#fontsize option:selected').val() + 'px">' + $('#txtHeadertext').val() + '</div>');
        //header.append('<div style="text-align:' + HeaderAlignment + ';font-family:' + $('#hdrfont option:selected').val() + '; font-style:' + $('#fontstyle option:selected').val() + ';font-size:' + $('#fontsize option:selected').val() + 'px">' + $('#txtHeaderinfo').val() + '</div>');
        for (var i = 0; i < v.length; i++) {
            header.append('<div style="text-align:' + HeaderAlignment + ';font-family:' + $('#hdrfont option:selected').val() + '; font-style:' + $('#fontstyle option:selected').val() + ';font-size:' + $('#fontsize option:selected').val() + 'px">' + v[i] + '</div>');
        }
        Footer.append('<div style="text-align:' + FooterAlignment + ';font-family:' + $('#FooterFont option:selected').val() + '; font-style:' + $('#footerfontstyle option:selected').val() + ';font-size:' + $('#footerfontsize option:selected').val() + 'px">' + $('#txtFooter').val() + '</div>')
    } else {
        $('#myModalprintLocalLang').modal();
        var splitHeader = "";
        var v = [];
        v = $('#txtHeaderinfo').val().split('  ').join('\n').split('\n');

        var header = $('#divheader1');
        var Footer = $('#divfooter1');
        header.html("");
        Footer.html("");
        var HeaderAlignment = $("#headeralignment option:selected").val();
        var FooterAlignment = $("#footeralignment option:selected").val();
        header.append('<div style="text-align:' + HeaderAlignment + ';font-family:' + $('#hdrfont option:selected').val() + '; font-style:' + $('#fontstyle option:selected').val() + ';font-size:' + $('#fontsize option:selected').val() + 'px">' + $('#txtHeadertext').val() + '</div>');
        for (var i = 0; i < v.length; i++) {
            header.append('<div style="text-align:' + HeaderAlignment + ';font-family:' + $('#hdrfont option:selected').val() + '; font-style:' + $('#fontstyle option:selected').val() + ';font-size:' + $('#fontsize option:selected').val() + 'px">' + v[i] + '</div>');
        }
        Footer.append('<div style="text-align:' + FooterAlignment + ';font-family:' + $('#FooterFont option:selected').val() + '; font-style:' + $('#footerfontstyle option:selected').val() + ';font-size:' + $('#footerfontsize option:selected').val() + 'px">' + $('#txtFooter').val() + '</div>')
    }
});

 
function closeprintDialog() {
    window.localStorage.setItem('ctCheck', 1);
    $('#myModalprint').hide();
}
$('#btnSavePrintC').click(function () {
    window.localStorage.setItem('ctCheck', 1);
   
    objPrintCustomize = [];
    var PrintCustomizeParams = new Object();
    var MainData = {};
    MainData["TransId"] = $('#hdnTransId').val();
    MainData["SUBORGID"] = $('#hdnBranchId').val();
    MainData["HEADERTEXT"] = $('#txtHeadertext').val();
    MainData["TIN"] = $('#txtTIN').val();
    MainData["HEADERINFO"] = $('#txtHeaderinfo').val();
    MainData["HEADERFONT"] = $('#hdrfont option:selected').val();
    MainData["HEADERFONTSIZE"] = $('#fontsize option:selected').val();
    MainData["HEADERALIGNMENT"] = $('#headeralignment option:selected').val();
    MainData["HEADERFONTSTYLE"] = $('#fontstyle option:selected').val();
    MainData["BODYCAPTIONFONT"] = $('#bodyFont option:selected').val();
    MainData["BODYCAPTIONFONTSIZE"] = $('#bodyfontsize option:selected').val();
    MainData["BODYCAPTIONFONTSTYLE"] = $('#bodyfontstyle option:selected').val();
    MainData["BODYFONT"] = $('#bodyVlaueFont option:selected').val();
    MainData["BODYFONTSIZE"] = $('#bodyValuefontsize option:selected').val();
    MainData["BODYFONTSTYLE"] = $('#bodyValuefontstyle option:selected').val();
    MainData["BODYALIGNMENT"] = $('#bodyalignment option:selected').val();
    MainData["FOOTERTEXT"] = $('#txtFooter').val();
    MainData["FOOTERFONT"] = $('#FooterFont option:selected').val();
    MainData["FOOTERFONTSIZE"] = $('#footerfontsize option:selected').val();
    MainData["FOOTERALIGNMENT"] = $('#footeralignment option:selected').val();
    MainData["FOOTERFONTSTYLE"] = $('#footerfontstyle option:selected').val();
    MainData["CREATEDBY"] = $('#hdnUserId').val();
    MainData["RECEIPT"] = $('#txtReceipt').val();
    MainData["ISPRINTLANG"] = $("input[name='rbnLang']:checked").val();
    MainData["ISPRINTLANGB03"] = $("input[name='rbnPrintLang']:checked").val();
    if ($('#chkInvoiceBoth').is(":checked") == true) {
        MainData["ISPRINTLANGB04"] = 1;

    } else {
        MainData["ISPRINTLANGB04"] = 0;
    }
    if ($('#chkPrintBoth').is(":checked") == true) {
        MainData["ISPRINTLANGB05"] = 1;
    } else {
        MainData["ISPRINTLANGB05"] = 0;
    }
    var vd_ = window.localStorage.getItem("UUID");
    //var vdSplit_ = window.location.pathname.split('/');

    //if (vdSplit_.length > 2) {

    //    vd_ = vdSplit_[1];

    //}

    MainData["VD"] = vd_;
    if ($('#chkprntdialog').is(":checked") == true) {
        MainData["ISPRINT"] = "true";
    }
    else {
        MainData["ISPRINT"] = "false";
    }

    if ($('#chkphotoOrder').is(":checked") == true) {
        MainData["ISPHOTOORDER"] = "true";
    }
    else {
        MainData["ISPHOTOORDER"] = "false";
    }
    if ($('#chkRoundPrice').is(":checked") == true) {
        MainData["ISROUNDPRICE"] = "true";
    }
    else {
        MainData["ISROUNDPRICE"] = "false";
    }
    if ($('#chkCashDrawer').is(":checked") == true) {
        window.localStorage.setItem("openCashDrawer", "1");
        MainData["ISCASHDRAWER"] = "1";
    }
    else {
        window.localStorage.setItem("openCashDrawer", "0");
        MainData["ISCASHDRAWER"] = "0";
    }
    MainData["LASTUPDATEDDATE"] = $('#hdnLastUpdatedDate').val();
    MainData["PrinterName"] = $("#txtPrinterName").val();
    MainData["PaperSize"] = $("input[name='rbnPapermm']:checked").val();



    if ($('#chkPaymentTerminal').is(":checked") == true) 
    {
        window.localStorage.setItem('CardTest',"0");
        MainData["PayGateWay"] = "0";    
   }else{
        window.localStorage.setItem('CardTest',"1");
        MainData["PayGateWay"] = "1";
    }
    objPrintCustomize.push(MainData);
    PrintCustomizeParams.MainData = JSON.stringify(objPrintCustomize);
    var ComplexObject = new Object();
    ComplexObject.PrintCustomizeData = PrintCustomizeParams;
    var data = JSON.stringify(ComplexObject);
    $.ajax({
        type: 'POST',
        url: CashierLite.Settings.SavePrintCustomize,
        data: data,
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function (resp) {

            var result = resp.SavePrintCustomizeResult.split(',');
            if (result[0] == "100000") {
                //var $textAndPic = $('<div></div>');
                //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-info-circle info-alertSuc"></i>');
                ////$textAndPic.append('<div class="alert-message"> Record Saved Successfully</div>');
                //$textAndPic.append('<div class="alert-message">' + $('#hdnRecSaveSucc').val() + '</div>');
                //BootstrapDialog.alert($textAndPic);
                navigator.notification.alert($('#hdnRecSaveSucc').val(), "", "neo ecr", "Ok");

                $('#PrimeInfo').addClass('open');
                $('#Header').removeClass('open');
                $('#Body').removeClass('open');
                $('#Footer').removeClass('open');
                $('#divPrimeInformation').attr('style', 'display:block');
                $('#divHeader').attr('style', 'display:none');
                $('#divBody').attr('style', 'display:none');
                $('#divFooter').attr('style', 'display:none');
                //clearControls();
                LoadPrintCustomize();
                loadOfflineDataServer(window.localStorage.getItem("UUID"));
            }

        },
        error: function (e) {
            //var $textAndPic = $('<div></div>');
            //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
            ////$textAndPic.append('<div class="alert-message">Invalid Data</div>');
            //$textAndPic.append('<div class="alert-message">' + $('#hdnInvalData').val() + '</div>');
            //BootstrapDialog.alert($textAndPic);
            navigator.notification.alert($('#hdnInvalData').val(), "", "neo ecr", "Ok");

        }
    });
});
function LoadPrintCustomize() {
    window.localStorage.setItem('ctCheck', 1);
    var vd_ = window.localStorage.getItem("UUID");
    var param = { SubOrgId: $('#hdnBranchId').val(), VD: vd_ };
    $.ajax({
        url: CashierLite.Settings.GetPrintCustomizeStyles,
        data: param,
        type: "GET",
        success: function (data) {
            if (data.length > 0) {
                $('#txtHeadertext').val(data[0].HeaderText);
                $('#txtTIN').val(data[0].TIN);
                $('#txtHeaderinfo').val(data[0].HeaderInfo);
                $('#hdrfont').val(data[0].Headerfont);
                $('#fontsize').val(data[0].HeaderFontSize);
                $('#headeralignment').val(data[0].HeaderFontAlignment);
                $('#fontstyle').val(data[0].HeaderFontStyle);
                $('#bodyFont').val(data[0].BodyCaptionFont);
                $('#bodyfontsize').val(data[0].BodyCaptionFontSize);
                $('#bodyfontstyle').val(data[0].BodyCaptionFontStyle);
                $('#bodyalignment').val(data[0].BodyAlignment);
                $('#bodyVlaueFont').val(data[0].BodyFont);
                $('#bodyValuefontsize').val(data[0].BodyFontSize);
                $('#bodyValuefontstyle').val(data[0].BodyFontStyle);
                $('#txtFooter').val(data[0].FooterText);
                $('#FooterFont').val(data[0].FooterFont);
                $('#footerfontsize').val(data[0].FooterFontSize);
                $('#footeralignment').val(data[0].FooterFontAlignment);
                $('#footerfontstyle').val(data[0].FooterFontStyle);
                $('#hdnBranchId').val(data[0].SubOrgId);
                $('#hdnTransId').val(data[0].PrintCustomizeID);
                $('#chkphotoOrder').prop("checked", false);
                $('#txtPrinterName').val(data[0].PrinterName);
                if (data[0].PrinterName == null || data[0].PrinterName != "" || data[0].PaperSize == undefined) {
                    window.localStorage.setItem("PrinterName", "InnerPrinter");
                } else
                    window.localStorage.setItem("PrinterName", data[0].PrinterName);
                if (data[0].PaperSize == "0" || data[0].PaperSize == "" || data[0].PaperSize == null || data[0].PaperSize == undefined) {
                    $('#rbn58mm').prop("checked", true);
                    $('#rbn80mm').prop("checked", false);
                }
                if (data[0].PaperSize == "1") {
                    $('#rbn58mm').prop("checked", false);
                    $('#rbn80mm').prop("checked", true);
                }
               if (data[0].ISPRINTLANG == "0" || data[0].ISPRINTLANG == "" || data[0].ISPRINTLANG == null || data[0].ISPRINTLANG == undefined) {
                    $('#rbnEnglish').prop("checked", true);
                    $('#rbnLocal').prop("checked", false);
                    // $('#chkInvoiceBoth').prop("checked", false);
                }
                if (data[0].ISPRINTLANG == "1") {
                    $('#rbnEnglish').prop("checked", false);
                    $('#rbnLocal').prop("checked", true);
                    // $('#chkInvoiceBoth').prop("checked", false);
                }

                //if (data[0].ISPRINTLANG == "3") {

                //    $('#rbnEnglish').prop("checked", false);

                //    $('#rbnLocal').prop("checked", false);

                //  //  $('#chkInvoiceBoth').prop("checked", true);

                //}

                if (data[0].ISPRINTLANGB03 == "0" || data[0].ISPRINTLANGB03 == "" || data[0].ISPRINTLANGB03 == null || data[0].ISPRINTLANGB03 == undefined) {
                    $('#rbnPrintEnglish').prop("checked", true);
                    $('#rbnPrintLocal').prop("checked", false);
                    //   $('#chkPrintBoth').prop("checked", false);

                }
                if (data[0].ISPRINTLANGB03 == "1") {
                    $('#rbnPrintEnglish').prop("checked", false);
                    $('#rbnPrintLocal').prop("checked", true);
                    // $('#chkPrintBoth').prop("checked", false);
                }
                //if (data[0].ISPRINTLANGB03 == "3") {

                //    $('#rbnPrintEnglish').prop("checked", false);

                //    $('#rbnPrintLocal').prop("checked", false);

                //    $('#chkPrintBoth').prop("checked", true);

                //}

                if (data[0].ISPRINTLANGB04 == "1") {
                    $('#chkInvoiceBoth').prop("checked", true);

                } else {
                    $('#chkInvoiceBoth').prop("checked", false);
                }
                if (data[0].ISPRINTLANGB05 == "1") {
                    $('#chkPrintBoth').prop("checked", true);

                } else {
                    $('#chkPrintBoth').prop("checked", false);
                }
                //$("input[name='rbnLang']:checked").val(data[0].ISPRINTLANG)

                if (data[0].Receipt != "") {
                    $('#txtReceipt').val(data[0].Receipt);
                }
                if (data[0].IsPrint == true) {
                    $('#chkprntdialog').prop("checked", true);
                }
                else {
                    $('#chkprntdialog').prop("checked", false);
                }
                if (data[0].ISPHOTOORDER == true) {
                    $('#chkphotoOrder').prop("checked", true);

                }
                else {
                    $('#chkphotoOrder').prop("checked", false);
                }
                if (data[0].ISROUNDPRICE == "true") {
                    $('#chkRoundPrice').prop("checked", true);
                    window.localStorage.setItem("roundoff", "1");
                }
                else {
                    window.localStorage.setItem("roundoff", "0");
                    $('#chkRoundPrice').prop("checked", false);
                }
                if (data[0].ISCASHDRAWER == "1") {
                    $('#chkCashDrawer').prop("checked", true);
                    window.localStorage.setItem("openCashDrawer", "1");
                }
                else {
                    window.localStorage.setItem("openCashDrawer", "0");
                    $('#chkCashDrawer').prop("checked", false);
                }
               if (data[0].PayGateWay == "1") 
                {
                    $('#chkPaymentTerminal').prop("checked", false);
                    window.localStorage.setItem('CardTest',"1");
                }
                else 
                  {
                     $('#chkPaymentTerminal').prop("checked", true);
                     window.localStorage.setItem('CardTest',"0");
                  }
                $('#hdnLastUpdatedDate').val(data[0].LASTUPDATEDDATE);

            }

            else {
                $('#txtHeadertext').val("");
                $('#txtHeaderinfo').val("");
                $('#hdrfont').val("0");
                $('#fontsize').val("0");
                $('#headeralignment').val("0");
                $('#fontstyle').val("0");
                $('#bodyFont').val("0");
                $('#bodyfontsize').val("0");
                $('#bodyfontstyle').val("0");
                $('#bodyalignment').val("0");
                $('#bodyVlaueFont').val("0");
                $('#bodyValuefontsize').val("0");
                $('#bodyValuefontstyle').val("0");
                $('#txtFooter').val("");
                $('#FooterFont').val("0");
                $('#footerfontsize').val("0");
                $('#footeralignment').val("0");
                $('#footerfontstyle').val("0");
                //$('#hdnBranchId').val("");
                $('#hdnTransId').val("");
                $('#hdnLastUpdatedDate').val("");
                $('#chkprntdialog').prop("checked", false);
                $('#chkphotoOrder').prop("checked", false);
                $('#rbnEnglish').prop("checked", true);
                $('#rbnLocal').prop("checked", false);
                $('#rbn58mm').prop("checked", true);
                $('#rbn80mm').prop("checked", false);
                $('#chkPaymentTerminal').prop("checked", true);
            }
            $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());

        },
        error: function (e) {
            navigator.notification.alert($('#hdnInvalData').val(), "", "neo ecr", "Ok");
        }

    });
    //$.ajax({
    //    url: CashierLite.Settings.GetPrintCustomizeStyles,
    //    data: param,
    //    type: "GET",
    //    success: function (data) {
    //        if (data.length > 0) {
    //            $('#txtHeadertext').val(data[0].HeaderText);
    //            $('#txtHeaderinfo').val(data[0].HeaderInfo);
    //            $('#hdrfont').val(data[0].Headerfont);
    //            $('#fontsize').val(data[0].HeaderFontSize);
    //            $('#headeralignment').val(data[0].HeaderFontAlignment);
    //            $('#fontstyle').val(data[0].HeaderFontStyle);
    //            $('#bodyFont').val(data[0].BodyCaptionFont);
    //            $('#bodyfontsize').val(data[0].BodyCaptionFontSize);
    //            $('#bodyfontstyle').val(data[0].BodyCaptionFontStyle);
    //            $('#bodyalignment').val(data[0].BodyAlignment);
    //            $('#bodyVlaueFont').val(data[0].BodyFont);
    //            $('#bodyValuefontsize').val(data[0].BodyFontSize);
    //            $('#bodyValuefontstyle').val(data[0].BodyFontStyle);
    //            $('#txtFooter').val(data[0].FooterText);
    //            $('#FooterFont').val(data[0].FooterFont);
    //            $('#footerfontsize').val(data[0].FooterFontSize);
    //            $('#footeralignment').val(data[0].FooterFontAlignment);
    //            $('#footerfontstyle').val(data[0].FooterFontStyle);
    //            $('#hdnBranchId').val(data[0].SubOrgId);
    //            $('#hdnTransId').val(data[0].PrintCustomizeID);
    //            $('#chkphotoOrder').prop("checked", false);
    //            //if (data[0].ISPRINTLANG == "0" || data[0].ISPRINTLANG == "" || data[0].ISPRINTLANG == null || data[0].ISPRINTLANG == undefined) {
    //            //    $('#rbnEnglish').prop("checked", true);
    //            //    $('#rbnLocal').prop("checked", false);
    //            //}
    //            //if (data[0].ISPRINTLANG == "1") {
    //            //    $('#rbnEnglish').prop("checked", false);
    //            //    $('#rbnLocal').prop("checked", true);
    //            //}
    //            if (data[0].ISPRINTLANG == "0" || data[0].ISPRINTLANG == "" || data[0].ISPRINTLANG == null || data[0].ISPRINTLANG == undefined) {

    //                $('#rbnEnglish').prop("checked", true);

    //                $('#rbnLocal').prop("checked", false);

    //                // $('#chkInvoiceBoth').prop("checked", false);

    //            }

    //            if (data[0].ISPRINTLANG == "1") {

    //                $('#rbnEnglish').prop("checked", false);

    //                $('#rbnLocal').prop("checked", true);

    //                // $('#chkInvoiceBoth').prop("checked", false);

    //            }

    //            //if (data[0].ISPRINTLANG == "3") {

    //            //    $('#rbnEnglish').prop("checked", false);

    //            //    $('#rbnLocal').prop("checked", false);

    //            //  //  $('#chkInvoiceBoth').prop("checked", true);

    //            //}

    //            if (data[0].ISPRINTLANGB03 == "0" || data[0].ISPRINTLANGB03 == "" || data[0].ISPRINTLANGB03 == null || data[0].ISPRINTLANGB03 == undefined) {
    //                $('#rbnPrintEnglish').prop("checked", true);
    //                $('#rbnPrintLocal').prop("checked", false);
    //            }
    //            if (data[0].ISPRINTLANGB03 == "1") {
    //                $('#rbnPrintEnglish').prop("checked", false);
    //                $('#rbnPrintLocal').prop("checked", true);
    //            }

    //            if (data[0].ISPRINTLANGB04 == "1") {
    //                $('#chkInvoiceBoth').prop("checked", true);
    //            } else {
    //                $('#chkInvoiceBoth').prop("checked", false);
    //            }

    //            if (data[0].ISPRINTLANGB05 == "1") {
    //                $('#chkPrintBoth').prop("checked", true);
    //            } else {
    //                $('#chkPrintBoth').prop("checked", false);
    //            }

    //            if (data[0].Receipt != "") {
    //                $('#txtReceipt').val(data[0].Receipt);
    //            }
    //            if (data[0].IsPrint == true) {
    //                $('#chkprntdialog').prop("checked", true);
    //            }
    //            else {
    //                $('#chkprntdialog').prop("checked", false);
    //            }
    //            if (data[0].ISPHOTOORDER == true) {
    //                $('#chkphotoOrder').prop("checked", true);
    //            }
    //            else {
    //                $('#chkphotoOrder').prop("checked", false);
    //            }
    //            if (data[0].ISROUNDPRICE == "true") {
    //                $('#chkRoundPrice').prop("checked", true);
    //                window.localStorage.setItem("roundoff", "1");
    //            }
    //            else {
    //                window.localStorage.setItem("roundoff", "0");
    //                $('#chkRoundPrice').prop("checked", false);
    //            }
    //            if (data[0].ISCASHDRAWER == "1") {
    //                $('#chkCashDrawer').prop("checked", true);
    //                window.localStorage.setItem("openCashDrawer", "1");
    //            }
    //            else {
    //                window.localStorage.setItem("openCashDrawer", "0");
    //                $('#chkCashDrawer').prop("checked", false);
    //            }
    //            $('#hdnLastUpdatedDate').val(data[0].LASTUPDATEDDATE)
    //        }
    //        else {
    //            $('#txtHeadertext').val("");
    //            $('#txtHeaderinfo').val("");
    //            $('#hdrfont').val("0");
    //            $('#fontsize').val("0");
    //            $('#headeralignment').val("0");
    //            $('#fontstyle').val("0");
    //            $('#bodyFont').val("0");
    //            $('#bodyfontsize').val("0");
    //            $('#bodyfontstyle').val("0");
    //            $('#bodyalignment').val("0");
    //            $('#bodyVlaueFont').val("0");
    //            $('#bodyValuefontsize').val("0");
    //            $('#bodyValuefontstyle').val("0");
    //            $('#txtFooter').val("");
    //            $('#FooterFont').val("0");
    //            $('#footerfontsize').val("0");
    //            $('#footeralignment').val("0");
    //            $('#footerfontstyle').val("0");
    //            //$('#hdnBranchId').val("");
    //            $('#hdnTransId').val("");
    //            $('#hdnLastUpdatedDate').val("");
    //            $('#chkprntdialog').prop("checked", false);
    //            $('#chkphotoOrder').prop("checked", false);
    //            $('#rbnEnglish').prop("checked", true);
    //            $('#rbnLocal').prop("checked", false);
    //        }
    //        $('#trSBInnerRow li span#lblMode').html('');
    //    },
    //    error: function (e) {
    //        //var $textAndPic = $('<div></div>');
    //        //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
    //        //$textAndPic.append('<div class="alert-message">' + $('#hdnInvalData').val() + '</div>');
    //        //BootstrapDialog.alert($textAndPic);
    //        navigator.notification.alert($('#hdnInvalData').val(), "", "neo ecr", "Ok");

    //    }
    //});
}

function getResources(langCode, pageCode) {
    window.localStorage.setItem('ctCheck', 1);
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.getResource,
        data: "langCode=" + langCode + "&pageCode=" + pageCode,
        success: function (resp) {
            setLabels(resp);
            $('#fontsize').prepend($('<option></option').attr('value', '0').text('--' + $('#hdnSelect').val() + '--'));
            $('#headeralignment').prepend($('<option></option').attr('value', '0').text('--' + $('#hdnSelect').val() + '--'));
            $('#fontstyle').prepend($('<option></option').attr('value', '0').text('--' + $('#hdnSelect').val() + '--'));
            $('#bodyFont').prepend($('<option></option').attr('value', '0').text('--' + $('#hdnSelect').val() + '--'));
            $('#bodyfontsize').prepend($('<option></option').attr('value', '0').text('--' + $('#hdnSelect').val() + '--'));
            $('#bodyfontstyle').prepend($('<option></option').attr('value', '0').text('--' + $('#hdnSelect').val() + '--'));
            $('#bodyalignment').prepend($('<option></option').attr('value', '0').text('--' + $('#hdnSelect').val() + '--'));
            $('#bodyVlaueFont').prepend($('<option></option').attr('value', '0').text('--' + $('#hdnSelect').val() + '--'));
            $('#bodyValuefontsize').prepend($('<option></option').attr('value', '0').text('--' + $('#hdnSelect').val() + '--'));
            $('#bodyValuefontstyle').prepend($('<option></option').attr('value', '0').text('--' + $('#hdnSelect').val() + '--'));
            $('#FooterFont').prepend($('<option></option').attr('value', '0').text('--' + $('#hdnSelect').val() + '--'));
            $('#footerfontsize').prepend($('<option></option').attr('value', '0').text('--' + $('#hdnSelect').val() + '--'));
            $('#footeralignment').prepend($('<option></option').attr('value', '0').text('--' + $('#hdnSelect').val() + '--'));
            $('#footerfontstyle').prepend($('<option></option').attr('value', '0').text('--' + $('#hdnSelect').val() + '--'));
            $('#hdrfont').prepend($('<option></option').attr('value', '0').text('--' + $('#hdnSelect').val() + '--'));


        }
    });
}
function setLabels(labelData) {
    window.localStorage.setItem('ctCheck', 1);
    jQuery("label[for='lblUploadUserImage'").html(labelData["UploadUserImage"]);
    jQuery("label[for='lblClose'").html(labelData["closed"]);
    jQuery("label[for='lblSubcription'").html(labelData["Renewal"]);
    jQuery("label[for='lblLocationwiseProfitAnalysis'").html(labelData["LocwiseProfitAnlys"]);
    jQuery("label[for='lblItemCategoryProfitAnalysis'").html(labelData["ItemCatgPrftAnlys"]);
    jQuery("label[for='lblAnalyticalReports'").html(labelData["AnalyticalReports"]);
    jQuery("label[for='lblItemCategorySalesAnalysis'").html(labelData["ItemCategorySalesAnalysis"]);
    jQuery("label[for='lblWeekDaysRevenue'").html(labelData["WeekdaysRevenue"]);
    jQuery("label[for='lblBucketAnalysis'").html(labelData["BucketAnalysis"]);
    jQuery("label[for='lblLocationwiseAverageInvoiceValue'").html(labelData["LocationwiseAverageInvoiceValue"]);
    jQuery("label[for='lblHeaderFont'").html(labelData["headerFont"]);
    jQuery("label[for='lblSCPopPwd'").html(labelData["pSettings"]);
    jQuery("label[for='lblLanguage'").html(labelData["lang"]);
    jQuery("label[for='lblDateFormat'").html(labelData["format"]);
    jQuery("label[for='lblPopOk'").html(labelData["ok"]);
    jQuery("label[for='lblPopClose'").html(labelData["cancel"]);
    jQuery("label[for='lblSettings'").html(labelData["settings"]);
    jQuery("label[for='lblThemes'").html(labelData["themes"]);
    jQuery("label[for='lblTheme1'").html(labelData["theme1"]);
    jQuery("label[for='lblTheme2'").html(labelData["theme2"]);
    jQuery("label[for='lblTheme3'").html(labelData["theme3"]);
    jQuery("label[for='lblTheme4'").html(labelData["theme4"]);
    jQuery("label[for='lblChangePassword'").html(labelData["changePassword"]);
    jQuery("label[for='lblPreview'").html(labelData["preview"]);
    jQuery("label[for='lblSave'").html(labelData["save"]);
    jQuery("label[for='lblConfigurators'").html(labelData["configurators"]);
jQuery("label[for='lblSubConfigurators'").html(labelData["POSEwalletConSts"]);
jQuery("label[for='lblInvoiceSummary'").html(labelData["category"]);
    jQuery("label[for='lblPrintCustomize'").html(labelData["printCustamize"]);
    jQuery("label[for='lblBarcodeGenerator'").html(labelData["barCodeGenerator"]);
    jQuery("label[for='lblMasters'").html(labelData["masters"]);
    jQuery("label[for='lblTillType'").html(labelData["tillType"]);
    jQuery("label[for='lblTill'").html(labelData["till"]);
    jQuery("label[for='lblTillPermissions'").html(labelData["tillPermissions"]);
    jQuery("label[for='lblCustomer'").html(labelData["customer"]);
    jQuery("label[for='lblTenderType'").html(labelData["tenderType"]);
    jQuery("label[for='lblTransactions'").html(labelData["transactions"]);
    jQuery("label[for='lblInvoce'").html(labelData["invoice"]);
    jQuery("label[for='lblSalesReturn'").html(labelData["salesReturn"]);
    jQuery("label[for='lblPosting'").html(labelData["posting"]);
    jQuery("label[for='lblTillOpening'").html(labelData["tillOpening"]);
    jQuery("label[for='lblTillClosing'").html(labelData["tillClosing"]);
    jQuery("label[for='lblAccountPosting'").html(labelData["accountPosting"]);
    jQuery("label[for='lblReports'").html(labelData["reports"]);
    jQuery("label[for='lblInvoiceListing'").html(labelData["invoiceListing"]);
    jQuery("label[for='lblTillInvoiceListing'").html(labelData["tillInvoiceListing"]);
    jQuery("label[for='lblTenderTypeWiseCollectionDailySummary'").html(labelData["tenderTypeWiseCollectionDailySummary"]);
    jQuery("label[for='lblSalesReturns'").html(labelData["salesReturns"]);
    jQuery("label[for='lblPrimeInformation'").html(labelData["primeInformation"]);
    jQuery("label[for='lblOrgSubOrg'").html(labelData["orgSubOrg"]);
    jQuery("label[for='lblReceipt'").html(labelData["receipt"]);
    jQuery("label[for='lblposReceipt'").html(labelData["invoice"]);
    jQuery("label[for='lblPrintDialog'").html(labelData["printDialogue"]);
    jQuery("label[for='lblPhotoOrder'").html(labelData["photoOrder"]);
    jQuery("label[for='lblHeader'").html(labelData["header"]);
    jQuery("label[for='lblHeaderText'").html(labelData["headerText"]);
    jQuery("label[for='lblHeaderInfo'").html(labelData["headerInfo"]);
    jQuery("label[for='lblHeaderSelect'").html(labelData["select"]);
    jQuery("label[for='lblGeorgiaSerif'").html(labelData["georgiaSerif"]);
    jQuery("label[for='lblTimesNewRoman'").html(labelData["timesNewRoman"]);
    jQuery("label[for='lblVerdana'").html(labelData["verdana"]);
    jQuery("label[for='lblComicSansMS'").html(labelData["comicSansMS"]);
    jQuery("label[for='lblWildWest'").html(labelData["wildWest"]);
    jQuery("label[for='lblBedRock'").html(labelData["bedRock"]);
    jQuery("label[for='lblHeaderFontSize'").html(labelData["fontSize"]);
    jQuery("label[for='lblFontSelect'").html(labelData["select"]);
    jQuery("label[for='lblAlignment'").html(labelData["alignment"]);
    jQuery("label[for='lblAlignSelect'").html(labelData["select"]);
    jQuery("label[for='lblCenter'").html(labelData["center"]);
    jQuery("label[for='lblLeft'").html(labelData["left"]);
    jQuery("label[for='lblFontSelect'").html(labelData["select"]);
    jQuery("label[for='lblNormal'").html(labelData["normal"]);
    jQuery("label[for='lblItalic'").html(labelData["italic"]);
    jQuery("label[for='lblOblique'").html(labelData["oblique"]);
    jQuery("label[for='lblBody'").html(labelData["body"]);
    jQuery("label[for='lblDetailCaptionFont'").html(labelData["detailCaptionFont"]);
    jQuery("label[for='lblDentalCaptionFontSelect'").html(labelData["select"]);
    jQuery("label[for='lblDentalCaptionFontGeorgiaSerif'").html(labelData["georgiaSerif"]);
    jQuery("label[for='lblDentalCaptionFontTimesNewRoman'").html(labelData["timesNewRoman"]);
    jQuery("label[for='lblDentalCaptionFontVerdana'").html(labelData["verdana"]);
    jQuery("label[for='lblDentalCaptionFontComicSansMS'").html(labelData["comicSansMS"]);
    jQuery("label[for='lblDentalCaptionFontWildWest'").html(labelData["wildWest"]);
    jQuery("label[for='lblDentalCaptionFontBedRock'").html(labelData["bedRock"]);
    jQuery("label[for='lblFontSize'").html(labelData["fontSize"]);
    jQuery("label[for='lblDFontSizeSelect'").html(labelData["select"]);
    jQuery("label[for='lblBodyFontStyle'").html(labelData["fontStyle"]);
    jQuery("label[for='lblFontStyleSelect'").html(labelData["select"]);
    jQuery("label[for='lblFontStyleNormal'").html(labelData["normal"]);
    jQuery("label[for='lblFontStyleItalic'").html(labelData["italic"]);
    jQuery("label[for='lblFontStyleOblique'").html(labelData["oblique"]);
    jQuery("label[for='lblAlignmentSelect'").html(labelData["select"]);
    jQuery("label[for='lblAlignmentCenter'").html(labelData["center"]);
    jQuery("label[for='lblAlignmentLeft'").html(labelData["left"]);
    jQuery("label[for='lblDetailValueFont'").html(labelData["detailValueFont"]);
    jQuery("label[for='lblDetailValueFontSelect'").html(labelData["select"]);
    jQuery("label[for='lblDetailValueFontGeorgiaSerif'").html(labelData["georgiaSerif"]);
    jQuery("label[for='lblDetailValueFontTimesNewRoman'").html(labelData["timesNewRoman"]);
    jQuery("label[for='lblDetailValueFontVerdana'").html(labelData["verdana"]);
    jQuery("label[for='lblDetailValueFontComicSansMS'").html(labelData["comicSansMS"]);
    jQuery("label[for='lblDetailValueFontWildWest'").html(labelData["wildWest"]);
    jQuery("label[for='lblDetailValueFontBedRock'").html(labelData["bedRock"]);
    jQuery("label[for='lblDetailValueFontSize'").html(labelData["fontSize"]);
    jQuery("label[for='lblDetailValueFontSizeSelect'").html(labelData["select"]);
    jQuery("label[for='lblFontStyle'").html(labelData["fontStyle"]);
    jQuery("label[for='lblDetailValueFontStyle'").html(labelData["fontStyle"]);
    jQuery("label[for='lblDetailValueFontStyleSelect'").html(labelData["select"]);
    jQuery("label[for='lblDetailValueFontStyleNormal'").html(labelData["normal"]);
    jQuery("label[for='lblDetailValueFontStyleItalic'").html(labelData["italic"]);
    jQuery("label[for='lblDetailValueFontStyleOblique'").html(labelData["oblique"]);
    jQuery("label[for='lblFooter'").html(labelData["footer"]);
    jQuery("label[for='lblFooterText'").html(labelData["footerText"]);
    jQuery("label[for='lblFooterFont'").html(labelData["footerFont"]);
    jQuery("label[for='lblFooterSelect'").html(labelData["select"]);
    jQuery("label[for='lblFooterGeorgiaSerif'").html(labelData["georgiaSerif"]);
    jQuery("label[for='lblFooterTimesNewRoman'").html(labelData["timesNewRoman"]);
    jQuery("label[for='lblFooterVerdana'").html(labelData["verdana"]);
    jQuery("label[for='lblFooterComicSansMS'").html(labelData["comicSansMS"]);
    jQuery("label[for='lblFooterWildWest'").html(labelData["wildWest"]);
    jQuery("label[for='lblFooterBedRock'").html(labelData["bedRock"]);
    jQuery("label[for='lblFooterFontSize'").html(labelData["fontSize"]);
    jQuery("label[for='lblFooterFontSizeSelect'").html(labelData["select"]);
    jQuery("label[for='lblFooterAlignment'").html(labelData["alignment"]);
    jQuery("label[for='lblFooterAlignmentSelect'").html(labelData["select"]);
    jQuery("label[for='lblFooterAlignmentCenter'").html(labelData["center"]);
    jQuery("label[for='lblFooterAlignmentLeft'").html(labelData["left"]);
    jQuery("label[for='lblFooterFontStyle'").html(labelData["fontStyle"]);
    jQuery("label[for='lblFooterFontSelect'").html(labelData["select"]);
    jQuery("label[for='lblFooterFontNormal'").html(labelData["normal"]);
    jQuery("label[for='lblFooterFontItalic'").html(labelData["italic"]);
    jQuery("label[for='lblFooterFontOblique'").html(labelData["oblique"]);
    jQuery("label[for='lblPrint'").html(labelData["print"]);
    jQuery("label[for='lblPaySummary'").html(labelData["paySummary"]);
    jQuery("label[for='lblItem'").html(labelData["item"]);
    jQuery("label[for='lblQty'").html(labelData["qty"]);
    jQuery("label[for='lblRate'").html(labelData["rate"]);
    jQuery("label[for='lblAmt'").html(labelData["amt"]);
    jQuery("label[for='lblItem1'").html(labelData["item1"]);
    jQuery("label[for='lblItem2'").html(labelData["item2"]);
    jQuery("label[for='lblItem3'").html(labelData["item3"]);
    jQuery("label[for='lblItem4'").html(labelData["item4"]);
    jQuery("label[for='lblLoyaltyPoints'").html(labelData["LoyaltyPoints"]);
    jQuery("label[for='lblDispatch'").html(labelData["Dispatch"]);
    jQuery("label[for='lblVATReturns'").html(labelData["VATReturns"]);
    $('#lblNavigationUrl').text(labelData["pos"] + "  >>  " + labelData["configurators"] + "  >>  " + labelData["printCustamize"]);
    jQuery("label[for='lblCurrentInventory'").html(labelData["CurrentInventory"]);
    $('#lblLastUpdate').text(labelData["lastUpdated"]);
    $('#hdnNPermInv').val(labelData["alert1"]);
    $('#hdnNAsnCounterTran').val(labelData["alert2"]);
    $('#hdnInvalData').val(labelData["alert3"]);
    $('#hdnContactAdmin').val(labelData["alert4"]);
    $('#hdnRecSaveSucc').val(labelData["alert5"]);
    $('#hdnChPwdSaveSucc').val(labelData["alert5"]);
    $('#hdnThameChange').val(labelData["alert6"]);
    $('#hdnNotAssignCountrToTrans').val(labelData["permissionsAssisted"]);
    jQuery("label[for='lblEnglish'").html(labelData["alert7"]);
    jQuery("label[for='lblLocal'").html(labelData["alert8"]);
    //$('#hdnNew').val(labelData["new1"]);
    //$('#hdnView').val(labelData["view"]);
    //$('#hdnEdit').val(labelData["edit"]);
    $('#hdnDefault').val(labelData["default1"]);
    $('#lblMode').val(labelData["default1"])
    $('#hdnSelect').val(labelData["select"])
    $('#hdnImageSave').val(labelData["ImageSave"]);
    jQuery("label[for='lblUpload'").html(labelData["upload"]);
    jQuery("label[for='lblRoundPrice'").html(labelData["alert9"]);
    jQuery("label[for='lblPapperSettings'").html(labelData["PLegSettings"]);
    jQuery("label[for='lblPrinterName'").html(labelData["PrinterName"]);
    jQuery("label[for='lblTIN'").html(labelData["TIN"]);

    jQuery("label[for='lblPaymentDevice'").html(labelData["PayGateWay"]);
    //alert(labelData["alert10"]);
    // jQuery("label[for='lblInvoicePrint'").html(labelData["alert10"]);
    jQuery("label[for='lblInvoicePrint'").html(labelData["invoice"]);
    jQuery("label[for='lblPrintLang'").html(labelData["print"]);
    jQuery("label[for='rbnPrintEnglish'").html(labelData["alert7"]);
    jQuery("label[for='rbnPrintLocal'").html(labelData["alert8"]);
    jQuery("label[for='lblBoth'").html(labelData["alert10"]);
    jQuery("label[for='rbnPrintLocal'").html(labelData["alert8"]);
    jQuery("label[for='lblCashDrawer'").html(labelData["alert12"]);

    jQuery("label[for='lblPaper58'").html(labelData["pos58mm"]);
    jQuery("label[for='lblPaper55'").html(labelData["pos80mm"]);
    
    //$('#txtReceipt').attr('placeholder', labelData["alert11"]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    jQuery("label[for='lblCustLDGR'").html(labelData["PosCustLdger"]);

}