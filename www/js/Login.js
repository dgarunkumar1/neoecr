﻿

function onload() {

    var UUID = "";
    if (window.localStorage.getItem("UUID") == null || window.localStorage.getItem("UUID") == "" || window.localStorage.getItem("UUID") == undefined) {
        window.localStorage.setItem("UUID", guid());
        UUID = window.localStorage.getItem("UUID");
    }
    else {
        UUID = window.localStorage.getItem("UUID");
    }
    //UUID = '7100e205c2a73236';
    //UUID ='45ed2302af0c1468';
    //UUID = "753163b9d0373cad";
    //UUID = '55550ce2-a76d-aec4-af12-8495c045221c';
    //UUID = '89426f469b947ae6';
    //UUID = '12345_TEST';
    //UUID = '211d90636a31d434';
    // UUID="TESTQR"
    //UUID="211d90636a31d434";
    //UUID="cfc0d5029ef874d7";
    UUID="unknown";
    window.localStorage.setItem("showDisplay", "0");
    window.localStorage.setItem("navColor", "0");
    window.localStorage.setItem("UUID", UUID);
    selectNetworkLoginCheck(UUID);
    window.localStorage.setItem("Display", "3");
    window.localStorage.setItem("DisplayQR", "0");
    loadOfflineDataServer(UUID);
    speakWelcome(); 
}
function disagree() {
    $('#chkTerms').prop('checked', false);
    document.getElementById("btnPay").disabled = true;
}

function agree() {
    document.getElementById("btnPay").disabled = false;
}

function termsCheck(element) {
    if (element.checked) {
        $("#divTerms").modal({
            backdrop: 'static'
        });
    }
    else {
        document.getElementById("btnPay").disabled = true;
    }
}
function selectNetworkLoginCheck(UUID) {
    var options = { dimBackground: true };
    SpinnerPlugin.activityStart("Please wait...", options);
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetUUID,
        data: "UUID=" + window.localStorage.getItem("UUID") + "",
        success: function (resp) {

            SpinnerPlugin.activityStop();
            window.localStorage.setItem('network', '1');
            checkOrgData(UUID);
        },
        error: function (resp) {
            SpinnerPlugin.activityStop();
            window.localStorage.setItem('network', '0');
            window.localStorage.setItem("showDisplay", '1');
            window.localStorage.setItem("Advertisement", '0');
            var AtUserId = window.localStorage.getItem('AtUserId');
            var Atpwd = window.localStorage.getItem('Atpwd');

            if ((AtUserId == "") || (AtUserId == null) || (AtUserId == undefined) || (AtUserId == 'undefined')) {
                navigator.notification.alert("Please Check Network", "", "neo ecr", "Ok")
            }
            else {

                window.open('posmain.html?#', '_self', 'location=no');
                speakWelcome();
            }
        }
    });
}
function errorCallback(a) {

}
function successCallback(a) {

}
function CreateUserDirectV2() {
    jQuery("label[for='lblLoginRegistration'").html('Registration');
    var elemDraft = document.getElementById("div2");
    elemDraft.style.display = "none";
    var elem = document.getElementById("div1");
    elem.style.display = "block";
    var div4 = document.getElementById("div4");
    div4.style.display = "none";
    $('#div1')[0].className = 'section is-active'
    $('#li2')[0].className = ''
    $('#li1')[0].className = 'is-active'

}
GetCityData();
function GetCityData() {
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetCityData,
        data: "VD=" + window.localStorage.getItem("UUID") + "",
        success: function (resp) {
            var len = resp.length;
            $("#txtCity").empty();
            $("#txtCity").append("<option value=0>Select City</option>");
            for (var i = 0; i < len; i++) {
                $("#txtCity").append("<option value='" + resp[i]["CityId"] + "'>" + resp[i]["CityName"] + "</option>");
            }
        }, error: function (e) {

        }
    });
}
function checkPrint1() {
    var printer = '';
    CordovaPrint.usbList(function (data) {
        console.log("Success");
    }, function (err) {
        console.log("Error");
        console.log(err);
    })
}
function checkPrint() {

}
function stringToUint(string) {
    var string = btoa(unescape(encodeURIComponent(string))),
        charList = string.split(''),
        uintArray = [];
    for (var i = 0; i < charList.length; i++) {
        uintArray.push(charList[i].charCodeAt(0));
    }
    return new Uint8Array(uintArray);
}

function uintToString(uintArray) {
    var encodedString = String.fromCharCode.apply(null, uintArray),
        decodedString = decodeURIComponent(escape(atob(encodedString)));
    return decodedString;
}
function changeUUID() {
    // alert(window.localStorage.getItem("UUID"));
    // alert(device.serial);
}
function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    var uu_ = s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
    try {
        uu_ = device.uuid; 
    }
    catch (a) {
    }
    return uu_;
}
function checkOrgData(UUID) {
    var options = { dimBackground: true };
    SpinnerPlugin.activityStart("Please wait...", options);
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetUUID,
        data: "UUID=" + UUID + "",
        success: function (resp) {
            getSTCDetails();
            SpinnerPlugin.activityStop();
            if (resp.indexOf('~') != -1) {
                $("#hdnExpirydate").val(resp.split('~')[1]);
                resp = resp.split('~')[0];
                startTimer();
            }
            if (resp == '1') {
                var divSubscribe = document.getElementById("divSubscribe");
                divSubscribe.style.display = "none";
                $('#btnsignIn').removeAttr('disabled');
                if (window.localStorage.getItem("UUID") == null || window.localStorage.getItem("UUID") == "" || window.localStorage.getItem("UUID") == undefined) {

                }
                else {
                    if (window.localStorage.getItem("AtUserId") == null || window.localStorage.getItem("AtUserId") == ""
                        || window.localStorage.getItem("AtUserId") == undefined) {
                    }
                    else {
                        var AtUserId = window.localStorage.getItem('AtUserId');
                        var Atpwd = window.localStorage.getItem('Atpwd');
                        $('#txtusername').val(AtUserId);
                        $('#txtpassword').val(Atpwd);
                        login();
                    }
                }
            }
            if (resp == '2') {
                $('#hdnTypeSubscribe').val('Renewal_L');
                var divSubscribe = document.getElementById("divSubscribe");
                divSubscribe.style.display = "none";
                $("#myModal").modal({ backdrop: 'static' });
                var elemDraft = document.getElementById("div2");
                elemDraft.style.display = "block";
                $('#div2')[0].className = 'section is-active'
                $('#li1')[0].className = ''
                $('#li2')[0].className = 'is-active'
                $('#li3')[0].className = ''
                var elem = document.getElementById("div1");
                elem.style.display = "none";
                var div4 = document.getElementById("div4");
                div4.style.display = "none";
                var btnBack2 = document.getElementById("btnBack2");
                btnBack2.style.display = "none";
                var btnCan2 = document.getElementById("btnCan2");
                btnCan2.style.display = "none";
                $('#btnsignIn').attr('disabled', 'disabled');
                var div4 = document.getElementById("div2Ft");
                div4.style.display = "block";
                try {
                    $.ajax({
                        type: 'GET',
                        url: CashierLite.Settings.GetSubScriptionPackes,
                        data: "",
                        success: function (resp) {
                            var langData = resp;
                            var divString = '';
                            for (var i = 0; i < langData.length; i++) {
                                divString = divString + '<li id=li_' + langData[i].Id + ' onclick="changeSubscription(' + langData[i].Id + ',' + langData[i].Amount + ')" class="text-left"><h1><span id="dgListShow_item1_' + i + '">' + langData[i].CINID + '</span></h1> <span>' + langData[i].Currency + '<small><span id="dgListShow_item2_' + i + '">' + langData[i].Amount + '</small></span></li>';
                            };
                            divString = divString + '</tr>'
                            $('#dgListShow li').remove();
                            $('#dgListShow').append(divString);
                            jQuery("label[for='lblLoginRegistration'").html('Subscription-Renewal');
                        },
                        error: function (e) {

                        }
                    });
                }
                catch (r) {

                }
            }
            if (resp == '0') {
                $('#hdnTypeSubscribe').val('New');
                var divSubscribe = document.getElementById("divSubscribe");
                divSubscribe.style.display = "block";
                $('#btnsignIn').attr('disabled', 'disabled');
                $("#myModal").modal({ backdrop: 'static' });
            }
        },
        error: function (e) {
            navigator.notification.alert("Please Check Network", "", "neo ecr", "Ok")
            SpinnerPlugin.activityStop();
        }
    });
}
function getSTCDetails() {
    try {
        var Lang = 'en';
        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.GetSTCDetails,
            data: "UUID=" + window.localStorage.getItem("UUID") + "&lang=" + Lang,
            success: function (resp) {
                window.localStorage.setItem('OnlinePayment', resp.split('~')[0]);
                window.localStorage.setItem('STCMClientCode', resp.split('~')[1]);
                window.localStorage.setItem('STCMUserName', resp.split('~')[2]);
                window.localStorage.setItem('STCMPassword', resp.split('~')[3]);
                window.localStorage.setItem("showDisplay", '0');
                window.localStorage.setItem("Advertisement", '0');
            },
            error: function (e) {

            }
        });
    }
    catch (a) {
    }
}
function getSTCDetailsafterLogin() {
    try {
        var Lang = 'en';
        var HashValues = (window.localStorage.getItem("hashValues"));
        if (HashValues != null && HashValues != '') {
            var hashSplit = HashValues.split('~');
            Lang = hashSplit[7];
        }
        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.GetSTCDetails,
            data: "UUID=" + window.localStorage.getItem("UUID") + "&lang=" + Lang,
            success: function (resp) {
                window.localStorage.setItem('OnlinePayment', resp.split('~')[0]);
                window.localStorage.setItem('STCMClientCode', resp.split('~')[1]);
                window.localStorage.setItem('STCMUserName', resp.split('~')[2]);
                window.localStorage.setItem('STCMPassword', resp.split('~')[3]);
                if (resp.split('~')[4] != undefined) {
                    if (resp.split('~')[4] != '0') { 
                        window.localStorage.setItem('YOUTUBE', resp.split('~')[4]);
                        window.localStorage.setItem("showDisplay", resp.split('~')[5]);
                        window.localStorage.setItem("Advertisement", resp.split('~')[6]);
                        window.localStorage.setItem('DLTotal', resp.split('~')[7]);
                        window.localStorage.setItem("DLTotalItems", resp.split('~')[8]);
                        window.localStorage.setItem("DLPaidAmt", resp.split('~')[9]);
                        window.localStorage.setItem("DLPaybleAmt", resp.split('~')[10]);
                        window.localStorage.setItem("DLCounterReady", resp.split('~')[11]);
                        window.localStorage.setItem("DLCounterClose", resp.split('~')[12]);
                        window.localStorage.setItem("DLItem", resp.split('~')[13]);
                        window.localStorage.setItem("DLQty", resp.split('~')[14]);
                        window.localStorage.setItem("DLAmount", resp.split('~')[15]);
                        window.localStorage.setItem("DLCartEmpty", resp.split('~')[16]);
                        window.localStorage.setItem("DLName", resp.split('~')[17]);
                        if (resp.split('~')[4] == '') {
                            window.localStorage.setItem("Advertisement", '0');
                        }
                    }
                    else {
                        window.localStorage.setItem("showDisplay", resp.split('~')[5]);
                        window.localStorage.setItem("Advertisement", '0');
                    }
                }
                else {
                    window.localStorage.setItem('YOUTUBE', '0');
                    window.localStorage.setItem("showDisplay", '0');
                    window.localStorage.setItem("Advertisement", '0');
                }
                GetItemDetailsOffline();
            },
            error: function (e) {
                window.localStorage.setItem('YOUTUBE', '0');
                window.localStorage.setItem("showDisplay", '0');
                window.localStorage.setItem("Advertisement", '0');
            }
        });
    }
    catch (a) {
    }
}
var db;
var INdb;
var indexedDB;
var DBOP;
function GetItemDetailsOffline() {
    var SubOrgId = '';
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        SubOrgId = hashSplit[1] + ',' + hashSplit[16];
    }
    var options = { dimBackground: true };
    SpinnerPlugin.activityStart("Please wait...", options);
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetINVItemsOffline,
        data: "subOrgId=" + SubOrgId + "&VD=" + window.localStorage.getItem("UUID"),
        success: function (resp) {
            SpinnerPlugin.activityStop();
            DBOP = JSON.stringify(resp);
            var db = window.openDatabase("myItemMaster", '1.0', 'CashierLiteDB', 2 * 1024 * 1024);
            db.transaction(DBPush, errorCB, successCB);
        },
        error: function (e) {
            SpinnerPlugin.activityStop();
            window.localStorage.setItem("ctCheck", '0');

            window.open('DashBoard.html', '_self', 'location=no');
            speakWelcome();
        }
    });

}
function DBPush(tx) {
    try {
        tx.executeSql('DROP TABLE IF EXISTS Items');
        tx.executeSql('CREATE TABLE IF NOT EXISTS Items (ItemId, Name,Code,Price,Disc,UOMCode,UOMName,UOMID,StockData,BarCode,Item_Group_Code,Item_Group_Id,O1,img,O2,profileData,Alias)');
        var sqlDBObj_ = JSON.parse(DBOP);
        for (var i = 0; i < sqlDBObj_.length; i++) {
            if (sqlDBObj_[i].ItemId != '') {
                tx.executeSql('INSERT INTO Items (ItemId, Name,Code,Price,Disc,UOMCode,UOMName,UOMID,StockData,BarCode,Item_Group_Code,Item_Group_Id,O1,img,O2,profileData,Alias) VALUES ("' + sqlDBObj_[i].ItemId + '", "' + sqlDBObj_[i].O3.replace('"', '').replace('"', '').replace('"', '').replace('"', '') + '", "' + sqlDBObj_[i].Code.replace('"', '').replace('"', '').replace('"', '').replace('"', '') + '", "' + sqlDBObj_[i].Price + '", "' + sqlDBObj_[i].Disc + '", "' + sqlDBObj_[i].UOMCode
                    + '","' + sqlDBObj_[i].UOMName + '","' + sqlDBObj_[i].UOMID
                    + '","' + sqlDBObj_[i].StockData + '","'
                    + sqlDBObj_[i].BarCode + '","' + sqlDBObj_[i].Item_Group_Code
                    + '","' + sqlDBObj_[i].Item_Group_Id + '","' + sqlDBObj_[i].O1 + '","' + sqlDBObj_[i].img + '","' + sqlDBObj_[i].O2 + '","' + sqlDBObj_[i].profileData + '","' + sqlDBObj_[i].Alias.replace('"', '').replace('"', '').replace('"', '').replace('"', '') + '")');
            }
        }
    }
    catch (a) {

    }
    window.localStorage.setItem("ctCheck", '0');
    apkVersionCheck();
}
function errorCB(tx, err) {

}
function skipLogin() {
    window.open('DashBoard.html', '_self', 'location=no');
    speakWelcome();
}
function downloadApk() {
    window.open(window.localStorage.getItem('Downloadapk'), '_self', 'location=no');
}
function apkVersionCheck() {
    var UUID = window.localStorage.getItem("UUID");
    var apkVersion = CashierLite.Settings.apkVersion;
    if (UUID != null) {
        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.GetApkVersion,
            data: "UUID=" + UUID + "&version=" + apkVersion,
            success: function (resp) {
                if (resp.split('~')[0] == '2') {
                    //Alert Text
                    $('#divAlert')[0].innerHTML = resp.split('~')[2] + resp.split('~')[3];
                    //Old Version
                    var apkOldVersion = resp.split('~')[3];
                    var apkNewVersion = resp.split('~')[4];
                    var skipDisable = resp.split('~')[5];
                    if (skipDisable == '1') {
                        $('#DenominCancelbyAlert').attr("style", "display:block");
                    }
                    $("#myModalVersion").modal({ backdrop: 'static' });
                    window.localStorage.setItem('Downloadapk', resp.split('~')[1]);
                }
                else {

                    //$('#lnkdVersion')[0].style.display = 'none';

                    window.open('DashBoard.html', '_self', 'location=no');
                
                }
            },
            error: function (e) {

                //$("#myModalVersion").modal({ backdrop: 'static' }); 
                window.open('DashBoard.html', '_self', 'location=no');
            }
        });
    }
}
function speakWelcome() {
    //downloader.get("http://www.africau.edu/images/default/sample.pdf");
    window.localStorage.setItem("Play", "0");
}
function playAudio(src) {

}
function successCB() {

}
function startTimer() {
    var duration = 1;
    var timer = duration, minutes, seconds;
    setInterval(function () {
        var Expirydate_ = $("#hdnExpirydate").val();
        var date = new Date();
        var CuurentTime_ = date.toString("hh:mm tt").split(' ')[4];
        var Currentdate_ = date.getMonth() + 1 + '/' + date.getDate() + '/' + date.getFullYear();
        var days_ = 1000 * 60 * 60 * 24;
        var days_diff = (Date.parse(Expirydate_) - Date.parse(Currentdate_)) / days_;
        //days_diff = 0;
        var hours_diff = 0;
        var min_diff = 0;
        var divTimer_ = document.getElementById("divTimer");

        if (parseFloat(days_diff) < 7) {
            hours_diff = parseFloat(23 - parseFloat(CuurentTime_.split(':')[0]));
            min_diff = parseFloat(59 - parseFloat(CuurentTime_.split(':')[1]));
            divTimer_.style.display = "block";
            $("#licenseTitle").html('License will expire in');
            $("#countdown").html(
                '<div class="timer-wrapper"><div class="time">' + days_diff + '</div><span class="text">Days</span></div><div class="timer-wrapper"><div class="time">' + hours_diff + '</div><span class="text">Hours</span></div><div class="timer-wrapper"><div class="time">' + min_diff + '</div><span class="text">Minutes</span></div>'
            );
        }
        if (days_diff <= 0 && hours_diff == 0 && min_diff == 0) {
            $("#licenseTitle").html('License has expired.<br/>Please Renew');
            $("#countdown").html(
                '<div class="timer-wrapper"><div class="time">' + days_diff + '</div><span class="text">Days</span></div><div class="timer-wrapper"><div class="time">' + hours_diff + '</div><span class="text">Hours</span></div><div class="timer-wrapper"><div class="time">' + min_diff + '</div><span class="text">Minutes</span></div>'
            );
        }

        if (days_diff <= 0) {
            $("#licenseTitle").html('License has expired.<br/>Please Renew');
            days_diff = 0;
            hours_diff = 0;
            min_diff = 0;
            $("#countdown").html(
                '<div></div>'
            );
        }
        if (parseFloat(days_diff) > 7) {
            divTimer_.style.display = "none";
        }

        if (--timer < 0) {
            timer = duration;
        }
    }, 1000);
}

function login() {

    var userName = document.getElementById('txtusername').value;
    var password = document.getElementById('txtpassword').value;


    var vd_ = window.localStorage.getItem("UUID");

    var options = { dimBackground: true };
    SpinnerPlugin.activityStart("Please wait...", options);
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.login,
        data: "uid=" + userName + "&pwd=" + password + "&VD=" + vd_,
        success: function (resp) {
            SpinnerPlugin.activityStop();
            if (resp.status == '1') {

                CashierLite.Session.getInstance().set({
                    userId: resp.Sno,
                    userName: resp.Name,
                });
                var session = CashierLite.Session.getInstance().get();

                var username = userName;
                var password = password;
                window.localStorage.setItem("uname", resp.Name);
                window.localStorage.setItem("Loginsword", document.getElementById('txtpassword').value);
                window.localStorage.setItem("Sno", resp.userId);
                window.localStorage.setItem("RoleId", resp.RoleId);
                window.localStorage.setItem("Theam", resp.Theme);
                window.localStorage.setItem("CashierID", resp.CashierID);
                window.localStorage.setItem("showDisplay", '0');
                window.localStorage.setItem("Advertisement", '0');
                GetHashValues(username);

            }

        },
        error: function (e) {
            document.getElementById('txtusername').value = '';
            document.getElementById('txtpassword').value = '';
            SpinnerPlugin.activityStop();
            //alert('Please Check Network', null, 'Error');
            navigator.notification.alert("Please Check Network", "", "neo ecr", "Ok")
        }
    });
}
$('#txtEmail').blur(function () {
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.SubscribeEmailCheck,
        data: "emailId=" + $('#txtEmail').val() + "",
        success: function (resp) {
            var langData = resp;
            if (langData != '0') {
                $('#txtOrgName').css('display', 'none');
                $('#txtCurrency').css('display', 'none');
                $('#txtEmail').css('width', '94%');
                $('#txtEmail').css('margin', '10px 0px');
                $('#UserPlusIcon').css('display', 'block');
                $('#txtStoreGSTIN').css('display', 'none');
                $('#txtTills').css('display', 'none');
                $('#txtAddress').css('display', 'none');
                $('#txtmobile').css('display', 'none');
                $('#txtCity').css('display', 'none');

                $('#txtPUEmail').val($('#txtEmail').val());

                $(".form-wrapper #btn1").css('display', 'none');
            }
            else {
                $('#txtEmail').css('width', '100%');
                $('#UserPlusIcon').css('display', 'none');
                $('#txtOrgName').css('display', 'block');
                $('#txtCurrency').css('display', 'block');
                $('#txtStoreGSTIN').css('display', 'block');
                $('#txtTills').css('display', 'block');
                $('#txtAddress').css('display', 'block');
                $('#txtmobile').css('display', 'block');
                $('#txtCity').css('display', 'block');
                $(".form-wrapper #btn1").css('display', '');
                $(".form-wrapper #btn1").css('display', 'inline-block !important;');
            }
        }
    })
});


function btn1cl() {

    var apkVersion = CashierLite.Settings.apkVersion;
    if (apkVersion != null) {
        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.CheckApkVersion,
            data: "version=" + apkVersion + "&UUID=" + window.localStorage.getItem("UUID"),
            success: function (resp) {
                if (resp.split('~')[0] == '2') {
                    $('#lnkVersion')[0].href = resp.split('~')[1];
                    $('#lnkVersion')[0].style.display = 'block';
                    navigator.notification.alert("Please Update Application", "", "neo ecr", "Ok");
                    document.getElementById('txtOrgName').focus();
                    return false;
                }
                else {
                    $('#lnkVersion')[0].style.display = 'none';
                    var txtOrgName = document.getElementById('txtOrgName').value;
                    var txtCurrency = document.getElementById('txtCurrency').value;
                    var txtEmail = document.getElementById('txtEmail').value;
                    var txtNumber = document.getElementById('txtmobile').value;
                    var txtTills = document.getElementById('txtTills').value;
                    var txtAddrss = document.getElementById('txtAddress').value.trim();
                    var txtStoreGSTIN = document.getElementById('txtStoreGSTIN').value;
                    if (txtEmail == '') {
                        navigator.notification.alert("Please Enter E-mail", "", "neo ecr", "Ok");
                        document.getElementById('txtOrgName').focus();
                        return false;
                    }
                    if (checkEmail(txtEmail) == false) {
                        navigator.notification.alert("Please Enter Valid E-mail Address", "", "neo ecr", "Ok");
                        $('#txtEmail').focus();
                        return false;
                    }
                    if (txtOrgName == '') {
                        navigator.notification.alert("Please Enter Store", "", "neo ecr", "Ok");
                        document.getElementById('txtOrgName').focus();
                        return false;
                    } if (txtCurrency == '') {
                        navigator.notification.alert("Please Enter Store", "", "neo ecr", "Ok");
                        document.getElementById('txtOrgName').focus();
                        return false;
                    }
                    if (txtTills == '') {
                        navigator.notification.alert("Please Enter No. of Tills", "", "neo ecr", "Ok");
                        document.getElementById('txtTills').focus();
                        return false;
                    }
                    if (txtAddrss == '') {
                        navigator.notification.alert("Please Enter Address", "", "neo ecr", "Ok");
                        document.getElementById('txtAddress').focus();
                        return false;
                    }
                     if ($('#txtCity').val() == "0") {
                         navigator.notification.alert("Please select City", "", "neo ecr", "Ok");
                         document.getElementById('txtCity').focus();
                         return false;
                     }
                    if (txtNumber == '') {
                        navigator.notification.alert("Please Enter Phone No.", "", "neo ecr", "Ok");
                        document.getElementById('txtEmail').focus();
                        return false;
                     }
                    if (txtStoreGSTIN == '') {
                        navigator.notification.alert("Please Enter Tax Identification No.", "", "neo ecr", "Ok");
                        document.getElementById('txtStoreGSTIN').focus();
                        return false;
                    }
                    var options = { dimBackground: true };
                    SpinnerPlugin.activityStart("Please wait...", options);
                    try {
                        $.ajax({
                            type: 'GET',
                            url: CashierLite.Settings.SubscribeEmailCheck,
                            data: "emailId=" + txtEmail + "",
                            success: function (resp) {
                                SpinnerPlugin.activityStop();
                                var langData = resp;
                                if (langData == '0') {
                                    var elemDraft = document.getElementById("div1");
                                    elemDraft.style.display = "none";

                                    var elem = document.getElementById("div2");
                                    elem.style.display = "block";

                                    var div3 = document.getElementById("div3");
                                    div3.style.display = "none";


                                    var div4 = document.getElementById("div2Ft");
                                    div4.style.display = "block";


                                    $('#div1')[0].className = 'section'
                                    $('#div2')[0].className = 'section is-active'
                                    $('#div4')[0].className = 'section'

                                    $('#li2')[0].className = 'is-active'
                                    $('#li1')[0].className = ''
                                    try {
                                        var options = { dimBackground: true };
                                        SpinnerPlugin.activityStart("Please wait...", options);
                                        $.ajax({
                                            type: 'GET',
                                            url: CashierLite.Settings.GetSubScriptionPackes,
                                            data: "",
                                            success: function (resp) {
                                                SpinnerPlugin.activityStop();
                                                var langData = resp;
                                                var divString = '';
                                                for (var i = 0; i < langData.length; i++) {
                                                    divString = divString + '<li id=li_' + langData[i].Id + ' onclick="changeSubscription(' + langData[i].Id + ',' + langData[i].Amount + ')" class="text-left"><h1><span id="dgListShow_item1_' + i + '">' + langData[i].CINID + '</span></h1> <span>' + langData[i].Currency + '<small><span id="dgListShow_item2_' + i + '">' + langData[i].Amount + '</small></span></li>';

                                                };
                                                divString = divString + '</tr>'
                                                $('#dgListShow li').remove();
                                                $('#dgListShow').append(divString);
                                                jQuery("label[for='lblLoginRegistration'").html('Subscription');
                                            },
                                            error: function (e) {
                                                SpinnerPlugin.activityStop();
                                            }
                                        });

                                    }
                                    catch (r) {
                                        SpinnerPlugin.activityStop();
                                    }
                                }
                                else {

                                    $('#li1')[0].className = 'is-active';
                                    $('#li2')[0].className = '';
                                    navigator.notification.alert("E-mail already exists", "", "neo ecr", "Ok");
                                    document.getElementById('txtEmail').focus();
                                    return false;
                                }

                            },
                            error: function (e) {
                                SpinnerPlugin.activityStop();
                            }
                        });
                    }
                    catch (r) {

                    }
                }
            },
            error: function (e) {

            }

        });
    }
}
function checkEmail(text) {
    var match = /[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-zA-Z0-9]/;
    return match.test(text);
}

function SendForgotPassword() {
    var txtEmail = document.getElementById('txtusernameforgot').value;
    if (txtEmail == '') {
        //var $textAndPic = $('<div></div>');
        //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        //$textAndPic.append('<div class="alert-message">Please Enter User Name</div>');
        //BootstrapDialog.alert($textAndPic);
        navigator.notification.alert("Please Enter E-mail", "", "neo ecr", "Ok");

        document.getElementById('txtusernameforgot').focus();
        return false;
    }
    try {
        var options = { dimBackground: true };
        SpinnerPlugin.activityStart("Please wait...", options);
        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.ForgetPassword,
            //data: "emailId=" + txtEmail + "",
            data: "EmailId=" + txtEmail + "&VD=" + window.localStorage.getItem("UUID"),
            success: function (resp) {
                SpinnerPlugin.activityStop();
                var langData = resp;
                if (langData != '0') {
                    //var $textAndPic = $('<div></div>');
                    //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-info-circle info-success"></i>');
                    //$textAndPic.append('<div class="alert-message">mail sent successfully</div>');
                    //BootstrapDialog.alert($textAndPic);
                    navigator.notification.alert("Mail sent successfully", "", "neo ecr", "Ok");

                    document.getElementById('txtEmail').focus();
                    return false;

                }
                else {
                    //var $textAndPic = $('<div></div>');
                    //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                    //$textAndPic.append('<div class="alert-message">User Name doesnot exist</div>');
                    //BootstrapDialog.alert($textAndPic);
                    navigator.notification.alert("E-mail does not exist", "", "neo ecr", "Ok");

                    document.getElementById('txtEmail').focus();
                    return false;
                }
            },
            error: function (e) {
                SpinnerPlugin.activityStop();
            }
        });
    }
    catch (e) {
        SpinnerPlugin.activityStop();
    }
}
$('#btnSaveTillUser').click(function () {
    
    var elemDrafts = document.getElementById("divmsgs");
    var UUID = window.localStorage.getItem("UUID");
    jQuery("label[for='lblLoginRegistration'").html('Payment');

    var txtOrgName = document.getElementById('txtOrgName').value;
    var txtSUEmail = document.getElementById('txtSUEmail').value;
    var txtPUEmail = document.getElementById('txtPUEmail').value;
    var txtPUPassWord = document.getElementById('txtPUPassWord').value;
    if (txtPUPassWord == "") {
        //var $textAndPic = $('<div></div>');
        //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-info-circle info-success"></i>');
        //$textAndPic.append('<div class="alert-message">Please enter Primary User Password</div>');
        //BootstrapDialog.alert($textAndPic);
        navigator.notification.alert('Please enter primary user password', "", "neo ecr", "Ok");
        document.getElementById('txtPUPassWord').focus();
        elemDrafts.style.display = "none";
        $("#PopMultiUser").css('z-index', '');

        $("#PopMultiUser").modal({ backdrop: 'static' });
        $('#PopMultiUser').css('opacity', '1');
        // return false;

    }
    if (txtSUEmail == "") {
        //var $textAndPic = $('<div></div>');
        //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-info-circle info-success"></i>');
        //$textAndPic.append('<div class="alert-message">Please enter Till User Email</div>');
        //BootstrapDialog.alert($textAndPic);
        navigator.notification.alert('Please enter Till user E-mail', "", "neo ecr", "Ok");
        document.getElementById('txtSUEmail').focus();
        elemDrafts.style.display = "none";
        $("#PopMultiUser").css('z-index', '');

        $("#PopMultiUser").modal({ backdrop: 'static' });
        $('#PopMultiUser').css('opacity', '1');
        //return false;

    }
    else {
        var createUserDirectParams_ = new Object();
        createUserDirectParams_.orgName = txtOrgName;
        createUserDirectParams_.userName = txtPUEmail;
        createUserDirectParams_.pwd = txtPUPassWord;
        createUserDirectParams_.UUID = UUID;
        createUserDirectParams_.TillUserEmail = txtSUEmail;
        createUserDirectParams_.VD = "GLOBAL";
        var ComplexObject = new Object();
        ComplexObject.createUserDirectParams = createUserDirectParams_;
        var data = JSON.stringify(ComplexObject);

        elemDrafts.style.display = "none";
        elemDrafts.style.display = "block";
        $('#lblmsgDates')[0].innerText = "Please wait";
        $('#btnSaveTillUser').prop('disabled', true);

        $('#PopMultiUser').css('opacity', '.5');
        var mode = 0;
        $.ajax({
            type: 'POST',
            url: CashierLite.Settings.SubscribeTillUser,
            data: data,
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            success: function (resp) {
                
                $('#btnBack2').removeAttr('disabled');
                $('#btnPay').removeAttr('disabled');
                $('#btnCan2').removeAttr('disabled');
                var d_ = resp.SubscribeTillUserResult.split('~');

                if (d_[0] == '1') {

                    jQuery("label[for='lblLoginRegistration'").html('Till User');
                    elemDrafts.style.display = "none";

                    var div2 = document.getElementById("div2");
                    div2.style.display = "none";

                    $('#div1')[0].className = 'section'
                    $('#div4')[0].className = 'section'
                    $('#div6')[0].className = 'section'
                    $('#div2')[0].className = 'section is-active';

                    $('#div1').css('display', 'none');



                    $('#li1')[0].className = ''
                    $('#li2')[0].className = ''
                    $('#li3')[0].className = 'is-active'


                    if (d_[4] == 'New') {
                        var div7 = document.getElementById("div7");
                        div7.style.display = "block";



                        $('#lblSUserName')[0].innerText = d_[1];
                        $('#lblSPwd')[0].innerText = d_[2];
                        document.getElementById('txtusername').value = '';
                        document.getElementById('txtpassword').value = '';
                        $("#PopMultiUser").modal('hide');
                    }
                    else if (d_[4] == 'Renewal_L') {
                        var div2 = document.getElementById("div5");
                        div2.style.display = "none";
                    }
                    var div6 = document.getElementById("div6");
                    div6.style.display = "block";
                    $('#lblTranNo')[0].innerText = d_[3];
                    $('#div6')[0].className = 'section is-active';
                    checkOrgData();
                }
                else if (d_[0] == '5') {
                    navigator.notification.alert('Invalid Primary User Credentials', "", "neo ecr", "Ok");
                    elemDrafts.style.display = "none";
                    $("#PopMultiUser").css('z-index', '');

                    $("#PopMultiUser").modal({ backdrop: 'static' });
                    $('#PopMultiUser').css('opacity', '1');
                }
                else if (d_[0] == '6') {
                    navigator.notification.alert('Till User E-mail already exists', "", "neo ecr", "Ok");
                    elemDrafts.style.display = "none";
                    $("#PopMultiUser").css('z-index', '');
                    $("#PopMultiUser").modal({ backdrop: 'static' });
                    $('#PopMultiUser').css('opacity', '1');
                }
            },
            error: function (e) {
                elemDrafts.style.display = "none";
                $('#btnBack2').removeAttr('disabled');
                $('#btnPay').removeAttr('disabled');
                $('#btnCan2').removeAttr('disabled');
            }
        })
    }
});

function CreateUserDirect(mode) {
    var options = { dimBackground: true };
    SpinnerPlugin.activityStart("Please wait...", options);
    $('#btnBack2').attr('disabled', 'disabled');
    $('#btnPay').attr('disabled', 'disabled');
    $('#btnCan2').attr('disabled', 'disabled');
    var UUID = window.localStorage.getItem("UUID");
    jQuery("label[for='lblLoginRegistration'").html('Payment');
    var txtOrgName = document.getElementById('txtOrgName').value;
    var txtEmail = document.getElementById('txtEmail').value;
    var txtmobile = document.getElementById('txtmobile').value;
    var txtStoreGSTIN = document.getElementById('txtStoreGSTIN').value;
    var txtTills = document.getElementById('txtTills').value;
    var txtAddress = document.getElementById('txtAddress').value;
    var txtCity = document.getElementById('txtCity').value;
    var createUserDirectParams_ = new Object();
    createUserDirectParams_.orgName = txtOrgName;
    createUserDirectParams_.userName = txtEmail;
    createUserDirectParams_.pwd = "";
    createUserDirectParams_.ContactNumber = txtmobile;
    createUserDirectParams_.StoreGSTIN = txtStoreGSTIN;
    createUserDirectParams_.NoOfTills = txtTills;
    createUserDirectParams_.StoreAddres = txtAddress;
    createUserDirectParams_.CityId = $('#txtCity').val();
    createUserDirectParams_.UUID = UUID;
    createUserDirectParams_.txtEmail = txtEmail;
    createUserDirectParams_.curid = $('#hdnCurrId').val();
    createUserDirectParams_.VD = "GLOBAL";
    //createUserDirectParams_.SchemID = $("#ddlSub").val();
    createUserDirectParams_.SchemID = $('#hdnSubscribeId').val();
    //createUserDirectParams_.Amount = parseFloat($('#lblAmount')[0].innerText);
    createUserDirectParams_.Amount = parseFloat($('#hdnAmount').val());
    createUserDirectParams_.SubType = $('#hdnTypeSubscribe').val();

    //if (window.localStorage.getItem("Online") == "0") {
    //    createUserDirectParams_.PaymentType = "Offline";
    //    createUserDirectParams_.RefNum = "";
    //}
    //if (window.localStorage.getItem("Online") == "1") {
    //    createUserDirectParams_.PaymentType = "Online";
    //    createUserDirectParams_.RefNum = window.localStorage.getItem("STCRefNum");
    //}
    if (window.localStorage.getItem("Online") == "0") {
        createUserDirectParams_.PaymentType = "Offline";
        createUserDirectParams_.RefNum = "";
        createUserDirectParams_.TransactionType_Code = "2";
    }
    if (window.localStorage.getItem("Online") == "1") {
        createUserDirectParams_.PaymentType = "Online";
        createUserDirectParams_.RefNum = window.localStorage.getItem("STCRefNum");
        createUserDirectParams_.TransactionType_Code = "1";
    }
    var ComplexObject = new Object();
    ComplexObject.createUserDirectParams = createUserDirectParams_;
    var data = JSON.stringify(ComplexObject);
    var elemDraft = document.getElementById("divmsg");
    elemDraft.style.display = "none";
    //if (parseFloat($('#lblAmount')[0].innerText) == 0) {
    //if (parseFloat($('#hdnAmount').val()) == 0) {
    elemDraft.style.display = "block";
    $('#lblmsgDate')[0].innerText = "Please wait";
    var options = { dimBackground: true };
    SpinnerPlugin.activityStart("Please wait...", options);
    $.ajax({
        type: 'POST',
        url: CashierLite.Settings.Subscribe,
        data: data,
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function (resp) {
            SpinnerPlugin.activityStop();
            elemDraft.style.display = "none";
            $('#btnBack2').removeAttr('disabled');
            $('#btnPay').removeAttr('disabled');
            $('#btnCan2').removeAttr('disabled');
            var d_ = resp.SubscribeResult.split('~');
            if (mode == "0") {
                if (d_[0] == '1') {

                    elemDraft.style.display = "none";
                    var div2 = document.getElementById("div2");
                    div2.style.display = "none";
                    $('#div1')[0].className = 'section'
                    $('#div4')[0].className = 'section'
                    $('#div2')[0].className = 'section is-active';
                    $('#li1')[0].className = ''
                    $('#li2')[0].className = ''
                    $('#li3')[0].className = 'is-active'
                    if (d_[4] == 'New') {
                        var div2 = document.getElementById("div5");
                        div2.style.display = "block";
                        $('#lblSUserName')[0].innerText = d_[1];
                        $('#lblSPwd')[0].innerText = d_[2];
                        document.getElementById('txtusername').value = '';
                        document.getElementById('txtpassword').value = '';
                        
                    }
                    else if (d_[4] == 'Renewal_L') {
                        var div2 = document.getElementById("div5");
                        div2.style.display = "none";

                    }
                    var div4 = document.getElementById("div4");
                    div4.style.display = "block";
                    $('#lblTranNo')[0].innerText = d_[3];
                    $('#div4')[0].className = 'section is-active';
                    checkOrgData();
                }
                UpdatePaymentStatus('0', UUID, d_[3]);
                //SMSData();
            }
            else if (mode == "2") {
                if (d_[0] == '1') {

                    elemDraft.style.display = "none";
                    var div2 = document.getElementById("div2");
                    div2.style.display = "none";
                    $('#div1')[0].className = 'section'
                    $('#div4')[0].className = 'section'
                    $('#div2')[0].className = 'section is-active';
                    $('#li1')[0].className = ''
                    $('#li2')[0].className = ''
                    $('#li3')[0].className = 'is-active'
                    if (d_[4] == 'New') {
                        var div2 = document.getElementById("div5");
                        div2.style.display = "block";
                        $('#lblSUserName')[0].innerText = d_[1];
                        $('#lblSPwd')[0].innerText = d_[2];
                        document.getElementById('txtusername').value = '';
                        document.getElementById('txtpassword').value = '';
                    }
                    else if (d_[4] == 'Renewal_L') {
                        var div2 = document.getElementById("div5");
                        div2.style.display = "none";
                    }
                    var div4 = document.getElementById("div4");
                    div4.style.display = "block";
                    $('#lblTranNo')[0].innerText = d_[3];
                    $('#div4')[0].className = 'section is-active';
                    checkOrgData();
                }
                UpdatePaymentStatus('2', UUID, d_[3]);

            }
            else if (mode == "1") {
                $('#myModal').modal('hide');
                navigator.notification.alert('Payment is Pending Please contact neo ecr Admin', "", "neo ecr", "Ok");

                UpdatePaymentStatus('1', UUID, d_[3]);

            }
            else if (mode == '4') {
                SpinnerPlugin.activityStop();
                navigator.notification.alert('Payment was Cancelled', "", "neo ecr", "Ok");
                $('#myModal').modal('hide');
                UpdatePaymentStatus('4', UUID, d_[3]);
            }
        },
        error: function (e) {
            SpinnerPlugin.activityStop();
            elemDraft.style.display = "none";
            $('#btnBack2').removeAttr('disabled');
            $('#btnPay').removeAttr('disabled');
            $('#btnCan2').removeAttr('disabled');
            SpinnerPlugin.activityStop();
        }
    });

}

function SMSData() {

    JsNote = [];
    var Notedata = {};

    Notedata["RqID"] = "";
    Notedata["SvcID"] = "0080"
    Notedata["SubSvcID"] = "0500";
    Notedata["FuncID"] = "0002"
    Notedata["MsgTimestamp"] = "";
    Notedata["ChID"] = "ARB_FINTECH";
    Notedata["OSID"] = "00";
    Notedata["EventCode"] = "OPT_08";
    Notedata["AppType"] = "WALLET";
    Notedata["NotificationPriority"] = "1";
    Notedata["NotificationValidity"] = "600";
    Notedata["NotificationMethod"] = "SMS";
    Notedata["Contact"] = document.getElementById('txtmobile').value;
    JsNote.push(Notedata);
    var NENotificationSend = new Object();
    NENotificationSend.NotifiData = JSON.stringify(JsNote);;
    var ComplexObject = new Object();
    ComplexObject.NENotification = NENotificationSend;
    var dataFinal = JSON.stringify(ComplexObject);

    $.ajax({
        type: 'POST',
        url: CashierLite.Settings.NENotification,
        data: dataFinal,
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function (resp) {
            //alert(resp);
        },
        error: function (e) {

        }
    });
}
$('#UserPlusIcon').click(function () {
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.ChkTillsCount,
        data: "UserName=" + $('#txtEmail').val(),
        success: function (resp) {
            var d_ = resp;
            
            if (d_ == "2") {
                navigator.notification.alert('Please enter primary user E-mail', "", "neo ecr", "Ok");
            }
            if (d_ == "0") {
                navigator.notification.alert('No. of Tills exceeded. Please contact administrator', "", "neo ecr", "Ok");
            }
            else {
                $('#myModal').modal('hide');
                $("#PopMultiUser").modal({ backdrop: 'static' });
            }

        },
        error: function (e) {

        }
    });

})
function UpdatePaymentStatus(status, UUID, subRef) {
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.UpdatePayemntStatusById,
        data: "status=" + status + "&userId=" + UUID + "&Ref=" + subRef,
        success: function (resp) {

        },
        error: function (e) {

        }
    });
}
document.getElementById("btnsignIn").addEventListener("click", function () {

    var userName = document.getElementById('txtusername').value;
    var password = document.getElementById('txtpassword').value;
    var vd_ = window.localStorage.getItem("UUID");
    var options = { dimBackground: true };
    SpinnerPlugin.activityStart("Please wait...", options);
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.login,
        data: "uid=" + userName + "&pwd=" + password + "&VD=" + vd_,
        success: function (resp) {
            SpinnerPlugin.activityStop();
            if (resp.status == '1') {

                CashierLite.Session.getInstance().set({
                    userId: resp.Sno,
                    userName: resp.Name,
                });

                var session = CashierLite.Session.getInstance().get();
                var username = userName;
                var password = password;
                window.localStorage.setItem("uname", resp.Name);
                window.localStorage.setItem("Loginsword", password);
                window.localStorage.setItem("Sno", resp.userId);
                window.localStorage.setItem("RoleId", resp.RoleId);
                window.localStorage.setItem("Theam", resp.Theme);
                window.localStorage.setItem("CashierID", resp.CashierID);

                var today = new Date();
                window.localStorage.setItem('AtUserId', $('#txtusername').val());
                $('#txtpassword')[0].type = 'text';
                var Pwd_ = $('#txtpassword').val();
                window.localStorage.setItem('Atpwd', Pwd_);
                $('#txtpassword')[0].type = 'password';
                GetHashValues(username);
            }
            else if (resp.status == '2') {
                navigator.notification.alert("Account is InActive mode please contact admin", "", "neo ecr", "Ok")
            }
            else {
                //document.getElementById('txtusername').value = '';
                document.getElementById('txtpassword').value = '';
                navigator.notification.alert("Please Enter valid E-mail and password", "", "neo ecr", "Ok");
            }
        },
        error: function (e) {
            SpinnerPlugin.activityStop();
            document.getElementById('txtusername').value = '';
            document.getElementById('txtpassword').value = '';
            SpinnerPlugin.activityStop();
            navigator.notification.alert("Please Check Network", "", "neo ecr", "Ok");
        }
    });
});


function GetHashValues(UserName) {

    var vd_ = window.localStorage.getItem("UUID");
    var options = { dimBackground: true };
    SpinnerPlugin.activityStart("Please wait...", options);
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.HashValues,
        data: "UserName=" + UserName + "&ip=''&VD=" + vd_,
        success: function (resp) {

            var hashValuesData = resp;
            var hashValues = "";
            var MainData = '';
            
            for (var i = 0; i < hashValuesData.length; i++) {
                var CompanyID = hashValuesData[i].CompanyID;            //1
                var BranchID = hashValuesData[i].BranchID;              //2
                var DateFormat = hashValuesData[i].DateFormat;          //3
                var DecimalFormat = hashValuesData[i].DecimalFormat;    //4
                var EmpCode = hashValuesData[i].EmpCode;                //5
                var EmpID = hashValuesData[i].EmpID;                    //6
                var EmpName = hashValuesData[i].EmpName;                //7
                var Language = hashValuesData[i].Language;              //8
                var LoginName = hashValuesData[i].LoginName;            //9
                var OBS = hashValuesData[i].OBS;                        //10
                var Org_Name = hashValuesData[i].Org_Name;              //11
                var TimeZone = hashValuesData[i].TimeZone;              //12
                var Top_Parent = hashValuesData[i].Top_Parent;          //13
                var Top_ParentName = hashValuesData[i].Top_ParentName;  //14
                var UserId = hashValuesData[i].UserId;                  //15
                var Org_ID = hashValuesData[i].Org_ID;                  //16
                var Base_Cur_ID = hashValuesData[i].Base_Cur_ID;        //17
                var SubOrg_Name = hashValuesData[i].SubOrg_Name;        //18
                var CURR_CODE = hashValuesData[i].CURR_CODE;            //19
                var Paging = hashValuesData[i].Paging;                  //20
                var POINTS_OBTAIN_BASECURRENCY = hashValuesData[i].POINTS_OBTAIN_BASECURRENCY;  //21
                var MINPOINTS = hashValuesData[i].MINPOINTS;            //22
                var MAXPOINTS = hashValuesData[i].MAXPOINTS;            //23
                var printerName = hashValuesData[i].printerName;        //24
                var url = hashValuesData[i].url;                        //25
                var printType = hashValuesData[i].printType;            //26
                var Address = hashValuesData[i].Address;            //27
                var TaxGSTIN = hashValuesData[i].TaxGSTIN;  //28
                MainData = CompanyID + '~' + BranchID + '~' + DateFormat + '~' + DecimalFormat + '~' + EmpCode + '~' + EmpID + '~' + EmpName + '~' + Language + '~' + LoginName + '~' + OBS + '~' + Org_Name + '~' + TimeZone + '~' + Top_Parent + '~' + Top_ParentName + '~' + UserId + '~' + Org_ID + '~' + Base_Cur_ID + '~' + SubOrg_Name + '~' + CURR_CODE + '~' + Paging + '~' + POINTS_OBTAIN_BASECURRENCY + '~' + MINPOINTS + '~' + MAXPOINTS + '~' + printerName + '~' + url + '~' + printType + '~' + Address + '~' + TaxGSTIN;
                window.localStorage.setItem("roundoff", hashValuesData[i].ISROUNDPRICE);
                window.localStorage.setItem("openCashDrawer", hashValuesData[i].ISCASHDRAWER);
                window.localStorage.setItem("PrinterName", hashValuesData[i].printerName);
                window.localStorage.setItem('CardTest',MAXPOINTS); 

            }
            window.localStorage.setItem("hashValues", MainData);
            window.localStorage.setItem("FillCustomer", '');

            getSTCDetailsafterLogin();

            SpinnerPlugin.activityStop();
        },
        error: function (e) {
            SpinnerPlugin.activityStop();
        }
    });

}

function Subscribe() {
    if ($("#hdnSubscribeId").val() == '') {
        navigator.notification.alert("Please Select Subscription Package", "", "neo ecr", "Ok");
        return false;
    }
    if ($('#chkTerms').prop('checked') == false) {
        navigator.notification.alert("Please Accept End-User License Agreement", "", "neo ecr", "Ok");
        return false;
    }
    if (window.localStorage.getItem('OnlinePayment') == '0') {
        window.localStorage.setItem("Online", "0");
        CreateUserDirect('0');
    }
    else {
        SPCPAYSMSSentToCustomer();
    }
}
function SPCPAYSMSSentToCustomer() {
    var txtNumber = document.getElementById('txtmobile').value;
    var RefNum = GUID();
    var BillNumber = GUID();
    var bodyLevelData_ = {
        "BranchID": "CASHIERADMIN",
        "TellerID": "CASHIERADMIN-Subscription",
        "DeviceID": "CASHIERADMIN-New",
        "RefNum": RefNum,
        "BillNumber": BillNumber,
        "MobileNo": txtNumber,
        "Amount": parseFloat($('#hdnAmount').val()),
        "MerchantNote": "neo ecr Payment For Subscription / Renewal"
    };
    var headerData = {
        "X-ClientCode": window.localStorage.getItem("STCMClientCode"),
        "X-UserName": window.localStorage.getItem("STCMUserName"),
        "X-Password": window.localStorage.getItem("STCMPassword"),
    };
    var data = JSON.stringify({ DirectPaymentAuthorizeRequestMessage: (bodyLevelData_) });
    var options = { dimBackground: true };
    SpinnerPlugin.activityStart("Please wait...", options);

    $.ajax({
        type: "POST",
        headers: headerData,
        datatype: "JSON",
        url: "https://b2btest.stcpay.com.sa/B2B.DirectPayment.WebApi/DirectPayment/V3/DirectPaymentAuthorize",
        data: data,
        contentType: "application/json",
        success: function (data) {
            SpinnerPlugin.activityStop();
            var dOrignal_ = JSON.stringify(data);
            var dConverted_ = JSON.parse(dOrignal_);
            if (dConverted_.code != undefined) {
                window.localStorage.setItem("OtpReference", dConverted_.DirectPaymentAuthorizeResponseMessage[0].OtpReference);
                window.localStorage.setItem("STCPayPmtReference", dConverted_.DirectPaymentAuthorizeResponseMessage[0].STCPayPmtReference);
                window.localStorage.setItem("STCRefNum", RefNum);
                window.localStorage.setItem("STCBillNumber", BillNumber);
                $("#otp-modal").modal({ backdrop: 'static' });
            }
            else {
                navigator.notification.alert('error Please contact Admin ', "", "neo ecr", "Ok");
            }
        },
        error: function (e) {
            SpinnerPlugin.activityStop();
            navigator.notification.alert('STC Wallet Pay Offline - Please contact Admin  ', "", "neo ecr", "Ok");

        }
    });
}
function STCOTPVerify() {
    if ($('#txtCustomerOTP').val() == "") {
        navigator.notification.alert("Please Enter Customer OTP", "", "neo ecr", "Ok");
        return false;
    }
    if ($('#txtCustomerOTP').val() != window.localStorage.getItem("OtpReference")) {
        navigator.notification.alert("Invalid OTP", "", "neo ecr", "Ok");
        return false;
    }
    paymentOTPConfrm();
}
function paymentOTPConfrm() {
    var TokenId = GUID();
    var bodyLevelData_ = {
        "OtpReference": window.localStorage.getItem("OtpReference"),
        "OtpValue": $('#txtCustomerOTP').val(),
        "STCPayPmtReference": window.localStorage.getItem("STCPayPmtReference"),
        "TokenReference": TokenId,
        "TokenizeYn": false
    };
    var headerData = {
        "X-ClientCode": window.localStorage.getItem("STCMClientCode"),
        "X-UserName": window.localStorage.getItem("STCMUserName"),
        "X-Password": window.localStorage.getItem("STCMPassword"),
    };
    var data = JSON.stringify({ DirectPaymentConfirmRequestMessage: (bodyLevelData_) })
    var options = { dimBackground: true };
    SpinnerPlugin.activityStart("Please wait...", options);
    $.ajax({
        type: "POST",
        headers: headerData,
        datatype: "JSON",
        url: "https://b2btest.stcpay.com.sa/B2B.DirectPayment.WebApi/DirectPayment/V3/DirectPaymentConfirm",
        data: data,
        contentType: "application/json",
        success: function (data) {
            SpinnerPlugin.activityStop();
            $('##otp-modal').modal('hide');
            var dOrignal_ = JSON.stringify(data);
            var dConverted_ = JSON.parse(dOrignal_);
            if (dConverted_.code != undefined) {
                var payemtStatus = dConverted_.DirectPaymentConfirmResponseMessage[0].PaymentStatus;

                var payemtStatus = dConverted_.DirectPaymentConfirmResponseMessage[0].PaymentStatus;
                if (payemtStatus == 5) {

                    navigator.notification.alert('Transaction Expired', "", "neo ecr", "Ok");
                    $('#myModal').modal('hide');
                }
                else {
                    window.localStorage.setItem("Online", "1");
                    CreateUserDirect(payemtStatus);
                }
            }
            else {
                navigator.notification.alert('Payment is Pending(account was not created) Please contact Admin ', "", "neo ecr", "Ok");

                $('#myModal').modal('hide');
                //error...?
            }
        },
        error: function (e) {
            SpinnerPlugin.activityStop();
            navigator.notification.alert('Payment is Pending Please contact Admin ', "", "neo ecr", "Ok");
            //need to remove
            $('#otp-modal').modal('hide');
            $('#myModal').modal('hide');
        }
    });
}
function GUID() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}
function changeSubscription(id, amount) {


    $('#hdnSubscribeId').val(id);
    $('#hdnAmount').val(amount);
    var div3 = document.getElementById("div3");

    if (id != 0) {
        try {
            var options = { dimBackground: true };
            SpinnerPlugin.activityStart("Please wait...", options);
            $.ajax({
                type: 'GET',
                url: CashierLite.Settings.GetSubScriptionPackesByID,
                data: "Id=" + id + "&UUID=" + window.localStorage.getItem("UUID"),
                success: function (resp) {
                    SpinnerPlugin.activityStop();
                    var d_ = resp;
                    div3.style.display = "block";
                    $('#lblExpDate')[0].innerText = resp[0].ServiceExpiryDate;
                },
                error: function (e) {
                    SpinnerPlugin.activityStop();
                }
            });
        }
        catch (r) {
            SpinnerPlugin.activityStop();
        }
    }
    else {
        div3.style.display = "none";
    }
}

