﻿function MainData()
{ 

    setTimeout(selectBarcode(), 1000);  
    GetItemDetails();
    //GetCustomers_();
    GetCounterTypeTenderData();
    fillShiftValues();
    GetTypeTenderDataCashby();
    // $('#myModalprint').hide();
    PrintLoad();
    //document.getElementById("userinp").addEventListener("change", readFile);
    $('#txtItemScan').focus();

    var SubOrgId = '';
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        getResources1(hashSplit[7], "POSInvoice");
        SubOrgId = hashSplit[1] + ',' + hashSplit[16];
        CompanyId = hashSplit[0];
        SubIOrgId = hashSplit[1];
        OBS = hashSplit[9];
        Orgcode = hashSplit[17];
        Userid = hashSplit[14];
        Lang = hashSplit[7];
        Paging = hashSplit[19];
    }
    $('#CompanyId').val(CompanyId);
    $('#hdnBranchId').val(SubIOrgId);
    $('#hdnOBS').val(OBS);
    $('#hdnUserId').val(Userid);
    $('#txtPosCode').val(Orgcode);
    $('#hdnLanguage').val(Lang);
    $('#hdnPaging').val(Paging);
    GetCounterTypeTenderDataOffline();
 }

function VD1() {
    var vd_ = window.localStorage.getItem("UUID");;
    
    return vd_;
}
var txt = "";
function selectBarcode() {

    if (txt != $("#txtItemScan").val()) {
        setTimeout('selectBarcode()', 2000);
        txt = $("#txtItemScan").val();
        Itemdetect();
    }

    setTimeout('selectBarcode()', 2000);

}
var HashVal = (window.localStorage.getItem("hashValues"));
var HashValues;
if (HashVal != null && HashVal != '') {
    HashValues = HashVal.split('~');
    if (HashValues[7] == 'ar-SA') {
        $('html,body').append('<link rel="stylesheet" href="Styles/css/bootstrap-rtl.css" type="text/css" />');
        $('html,body').append('<link rel="stylesheet" href="Styles/css/bootstrap-flipped.css" type="text/css" />');
        $('head').append('<script src="js/bootstrap-dialog-SA.min.js"></script>')
        $('head').append('<script src="js/ardatepicker.js"></script>')
    }

}
function theamLoad() {
    if (window.localStorage.getItem("Theam") != null)
        $('head').append('<link rel="stylesheet" href=' + window.localStorage.getItem("Theam") + ' type="text/css" />');



}
function SearchItem() {
    if (window.localStorage.getItem('network') == '1') {
        GetItemDetails();
        $('#myItemSearch').modal('show');
    }
}

function ActiveRowget(itemId) {
    valueAppendtoPOPUp();
    $("#tbl_tenderDyms tr").remove();
    var myTab = document.getElementById('itemCarttable');
    var m_ = "";
    for (i = 0; i < myTab.rows.length; i++) {
        //if(myTab[i].value==)

        var objCells = myTab.rows.item(i).cells;
        try {
            var v_ = 'tr_' + objCells.item(5).innerHTML;
            document.getElementById(v_).style.cssText = 'font-weight:normal';
            if (itemId == objCells.item(5).innerHTML) {
                $('#txthdnItemId').val(itemId);
                $('#txtQuantity').val(objCells.item(2).innerHTML);
                $('#txtPrice').val(parseFloat($('#td_' + itemId).val()));
                $('#txtAmount').val(objCells.item(4).innerHTML);
                $('#txtDiscount').val(objCells.item(3).innerHTML);
                $('#txtUOM').val(objCells.item(8).innerHTML);
                $("#txtAmount").attr("disabled", "disabled");
                $("#txthdnItemId").attr("disabled", "disabled");
                $("#txtQuantity").attr("disabled", "disabled");
                $("#txtPrice").attr("disabled", "disabled");
                $("#txtUOM").attr("disabled", "disabled");
                $("#txtDiscount").attr("disabled", "disabled");
                $("#txtPrice").attr("disabled", "disabled");
                document.getElementById(v_).style.cssText = 'font-weight:bold';

            }
            $("#txthdnActiveColumn").val("");
            document.getElementById('liQtyM').style.cssText = 'color:white;background: #45bbe0;';
            document.getElementById('liDisM').style.cssText = 'color:white;background: #8892d6;';
            document.getElementById('liPriceM').style.cssText = 'color:white;background: #348cd4;';
        }
        catch (r) {

        }
    }
}
function MMSplitDecode(sText, sSymbol) {
    var Splitter = '';
    switch (sSymbol) {
        case 'Š':
            Splitter = '%u0160';
            break;
        case '%u02DC': //˜
            Splitter = '%u02DC';
            break;
        case '%B0': //°
            Splitter = '%B0';
            break;
        case '%F7': //÷
            Splitter = '%F7';
            break;
        case '%u0178':
            Splitter = '%u0178';
            break;
    }
    var str = escape(sText).split(Splitter);
    for (var iCount = 0; iCount < str.length; iCount++)
        str[iCount] = unescape(str[iCount]);
    return str;
}



function prev() {

    if ($('#hdnPageIndex').val() == 1) {
        $('#liPrev').addClass('inactive').removeClass('active');
        $('#liNext').addClass('active').removeClass('inactive');
    } else {
        $('#hdnPageIndex').val(parseInt($('#hdnPageIndex').val()) - 1);
        GetItemDetails();
        $('#liPrev').addClass('active').removeClass('inactive');
    }
}

function next() {
    $('#liPrev').addClass('active').removeClass('inactive');
    $('#hdnPageIndex').val(parseInt($('#hdnPageIndex').val()) + 1);
    GetItemDetails();
}

function GetItemDetails() {
    var SubOrgId = '';
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        SubOrgId = hashSplit[1] + ',' + hashSplit[16];
    }


     $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetINVItems,
         data: "subOrgId=" + SubOrgId + "&VD=" + VD1() + "&pageIndex=" + $('#hdnPageIndex').val(),
        success: function (resp) {
            if (resp.length > 0) {
                var ItemsData = resp;
                document.getElementById("lblStockCt").innerHTML = resp.length;
                var li_ = "";
                document.getElementById('tbl_Item').childNodes[0].nodeValue = null;
                var MainData = '';

                for (var i = 0; i < ItemsData.length; i++) {
                    var mapCode_ = "";
                    var profileData_ = "";
                    mapCode_ = ItemsData[i].O2.replace('$', '&');
                    if (mapCode_ != '')
                        for (var k = 0; k < ItemsData[i].profileData.length; k++) {
                            var cols = ItemsData[i].profileData[k].split('~');
                            profileData_ = profileData_ + "@" + cols[0] + "#" + cols[1] + "#" + cols[2] + "#" + cols[3] + "#" + cols[4] + "#" + cols[5] + "#" + cols[6] + "#" + cols[8] + "#" + cols[9] + "#" + cols[10] + "#" + cols[11] + "#" + cols[12] + "#" + cols[13] + "#" + cols[14] + "#" + cols[15] + "#" + cols[16] + "#" + cols[17];
                        }

                    var img_ = ""; var style_ = "";
                    if (ItemsData[i].img != '') {
                        img_ = ItemsData[i].img;
                        style_ = "style='background-image:url(" + img_ + ");background-size:100% 80%;background-repeat:no-repeat;display:block;width:100%;height:84px;background-color:transparent;margin-bottom:20px;text-align:center;'"
                        li_ = li_ + "<li><a href='#' onclick=onSubData(\'" + ItemsData[i].Code + "\',\'" + ItemsData[i].ItemId + "\',\'" + ItemsData[i].Name + "\',\'" + ItemsData[i].Price + "\',\'" + ItemsData[i].Disc + "\',\'" + ItemsData[i].UOMID + "\',\'" + ItemsData[i].UOMCode + "\',\'" + mapCode_.replace(" ", "") + "\',\'" + profileData_.replace(" ", "") + "\',\'" + img_ + "\');> <div class='content-body-item'>";
                        li_ = li_ + "<h5 style='font-size:12px'>" + ItemsData[i].Code + "</h5><div   " + style_ + " class='item-sec'>";
                        li_ = li_ + "<p style='margin-top:50px;color:#000000;font-size:80%;text-align:center;word-wrap:break-word;white-space:pre-line;line-height:20px;font-weight:bold'>" + ItemsData[i].Name + "</p>";
                        li_ = li_ + "</div><div style='display:none'>" + ItemsData[i].BarCode.toUpperCase() + "</div><div style='display:none'>" + mapCode_.replace(" ", "") + "</div><div style='display:none'>" + profileData_.replace(" ", "") + "</div>";
                        li_ = li_ + "<small style='float:left;clear:both'>" + ItemsData[i].Price + "</small><small style=' float:right;clear:right' >" + ItemsData[i].UOMCode + "</small>";
                        li_ = li_ + "<br/><small >" + ItemsData[i].Alias + "</small></div>";
                        li_ = li_ + "</a></li>";
                    }
                    else {
                        li_ = li_ + "<li><a href='#' onclick=onSubData(\'" + ItemsData[i].Code + "\',\'" + ItemsData[i].ItemId + "\',\'" + ItemsData[i].Name + "\',\'" + ItemsData[i].Price + "\',\'" + ItemsData[i].Disc + "\',\'" + ItemsData[i].UOMID + "\',\'" + ItemsData[i].UOMCode + "\',\'" + mapCode_.replace(" ", "") + "\',\'" + profileData_.replace(" ", "") + "\');> <div class='content-body-item'>";
                        li_ = li_ + "<h5 style='font-size:12px'>" + ItemsData[i].Code + "</h5><div class='item-sec'>";
                        li_ = li_ + "<samp>" + ItemsData[i].Price + "</samp>";
                        li_ = li_ + "<br/><small>" + ItemsData[i].UOMCode + "</small></div><div style='display:none'>" + ItemsData[i].BarCode.toUpperCase() + "</div><div style='display:none'>" + mapCode_.replace(" ", "") + "</div><div style='display:none'>" + profileData_.replace(" ", "") + "</div>";
                        li_ = li_ + "<small >" + ItemsData[i].Name + "</small>";
                        li_ = li_ + "<br/><small >" + ItemsData[i].Alias + "</small></div>";
                        li_ = li_ + "</a></li>";
                    }

                    var Code = ItemsData[i].Code.toUpperCase();
                    var ItemId = ItemsData[i].ItemId;
                    var Name = ItemsData[i].Name;
                    var Price = ItemsData[i].Price;
                    var Disc = ItemsData[i].Disc;
                    var UOMCode = ItemsData[i].UOMCode;
                    var UOMName = ItemsData[i].UOMName;
                    var UOMID = ItemsData[i].UOMID;
                    var BarCode = ItemsData[i].BarCode.toUpperCase();
                    var StockData = ItemsData[i].StockData;
                    var Item_Group_Code = ItemsData[i].Item_Group_Code;
                    var Item_Group_Id = ItemsData[i].Item_Group_Id;
                    var O1 = ItemsData[i].O1;
                    var img_ = ItemsData[i].img;
                    var alias = ItemsData[i].Alias;
                    if (MainData != '')
                        MainData = MainData + Code + '~' + ItemId + '~' + Name + '~' + Price + '~' + Disc + '~' + UOMCode + '~' + UOMName + '~' + UOMID + '~' + BarCode + '~' + StockData + '~' + mapCode_ + '~' + profileData_ + '~' + img_ + '~' + alias + '$';
                    else
                        MainData = Code + '~' + ItemId + '~' + Name + '~' + Price + '~' + Disc + '~' + UOMCode + '~' + UOMName + '~' + UOMID + '~' + BarCode + '~' + StockData + '~' + mapCode_ + '~' + profileData_ + '~' + img_ + '~' + alias + '$';
                }

                document.getElementById('tbl_Item').innerHTML = "<ul  id='myTable'>" + li_ +
                                           "</ul>";


                
                $('#txtItemScan').focus();
            } else {
                $('#hdnPageIndex').val(parseInt($('#hdnPageIndex').val()) - 1);
                $('#liNext').addClass('inactive').removeClass('active');
                $('#liPrev').addClass('active').removeClass('inactive');
            }
        },
        error: function (e) {

        }
    });

}


function btnTheam_Click1() {

    
    theamStroreDB('custom-style-1.css');
   
}
function btnTheam_Click2() {
   
    theamStroreDB('custom-style-2.css');
   
}
function btnTheam_Click3() {
  
    theamStroreDB('custom-style-3.css');
   

}
function btnTheam_Click4() {
  
    theamStroreDB('custom-style-4.css');
  
}
function fillpassword() {

    if (window.localStorage.getItem("uname") != 0) {
        TransportTrack.Session.getInstance().set({
            userId: window.localStorage.getItem("Sno"),
            userName: window.localStorage.getItem("uname"),

        });
        if (window.localStorage.getItem("RoleId") != 1) {

            document.getElementById('Username').value = window.localStorage.getItem("uname");
            document.getElementById('password').value = window.localStorage.getItem("pass");
            window.open('#', '_self', 'location=no');
        }
    }
    else { 
    }
}


function PageValidatebyPage(pagecode) {
    if (pagecode != '') {

        var LoginId = window.localStorage.getItem("Sno");
        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.PageValidatebyPage,
            data: "uid=" + LoginId + "&CounterId=" + window.localStorage.getItem('COUNTERTYPE') + "&PageCode=" + pagecode + "&VD=" + VD1(),
            success: function (resp) {

                var s_ = resp;

                var s0_ = resp.split('~');
                var s_ = s0_[1];
                var s1_ = s0_[0];

                if (s1_ == '2') {
                    window.localStorage.setItem("RoleId", "Admin");

                }
                else {
                    window.localStorage.setItem("RoleId", "User");
                }
                if (s_ == '1') {


                    if (pagecode == 'POSINVCFG') {
                        window.open('POSPrintCustomize.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSBCODG') {
                        window.open('POSBarcodeGenerator.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSCNTTYP') {
                        window.open('POSCounterType.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSCOUDF') {
                        window.open('PosCounterDef.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSCTRPRM') {
                        window.open('POSCounterPermissions.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSCUSTM') {
                        window.open('CustomerMaster.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSTENDERTYPE') {
                        window.open('TenderTypes.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSLOYPRG') {
                        window.open('LoyaltyPoints.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSIORCR') {
                        //window.open('posmain.html?#', '_self', 'location=no');
                        GetShiftData();
                    }
                    if (pagecode == 'POSSLSRT') {
                        window.open('SalesReturn.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSDISPATCH') {
                        window.open('POSDeliveryStatusDetails.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSCOUOP') {
                        dayCloserChecking();
                        //window.open('CounterOpening.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSCOUCL') {
                        window.open('CounterClosing.html', '_self', 'location=no');
                    }

                    //if (pagecode == 'POSACPO') {
                    //    window.open('AccountPosting.html', '_self', 'location=no');
                    //}
                    if (pagecode == 'POSACPO') {

                        getPOSPage('POSACPO');
                    }
                    if (pagecode == 'POSRPTINVLST') {

                        getPOSPage('POSTINL');
                    }
                    if (pagecode == 'POSRPTSLRT') {
                        getPOSPage('POSSDSEN');
                    }
                    if (pagecode == 'POSRPTINVL') {
                        getPOSPage('POSINLS');
                    }
                    if (pagecode == 'POSRPTDCTN') {
                        getPOSPage('POSTTDS');
                    }
                    if (pagecode == 'POSRPTCINV') {
                        getPOSPage('POSCINV');
                    }
                    if (pagecode == 'POSRPTDE') {
                        window.open('POSReport.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSRPTSGRW') {
                        getPOSPage('POSSGR');
                    }
                    if (pagecode == 'POSConfigT') {
                        window.open('Configurator.html', '_self', 'location=no')
                    }
                    if (pagecode == 'POSRPTICSA') {
                        getPOSPage('POSRPTICSA');
                        //window.open('ItemCategorySalesAnalysis.html', '_self', 'location=no')
                    }
                    if (pagecode == 'POSRPTBAR') {
                        getPOSPage('POSRPTBAR');
                        //window.open('BulkAnalysisReport.html', '_self', 'location=no')
                    }
                    if (pagecode == 'POSRPTWDRR') {
                        getPOSPage('POSRPTWDRR');
                        //window.open('POSWeekDaysRevenueRP.html', '_self', 'location=no')
                    }
                    if (pagecode == 'POSRPTLWATV') {
                        getPOSPage('POSRPTLWATV');
                        //window.open('POSLocationwiseAvgTransactionValueRPT.html', '_self', 'location=no')
                    }
                    if (pagecode == 'POSRPTICPA') {
                        getPOSPage('POSRPTICPA');
                    }
                    if (pagecode == 'POSRPTLWPAS') {
                        getPOSPage('POSRPTLWPAS');
                        //window.open('POSLocationwiseProfitAnalysis.html', '_self', 'location=no')
                    }


                    if (pagecode == 'POSTVRR') {
                        getPOSPage('POSTVRR');
                        //window.open('POSLocationwiseProfitAnalysis.html', '_self', 'location=no')
                    }
                }
                else {

                }
            },
            error: function (e) {
            }
        });

    }

}
function PageValidatebyPageDash(pagecode) {
    if (pagecode != '') {

        var LoginId = window.localStorage.getItem("Sno");

        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.PageValidatebyPage,
            data: "uid=" + LoginId + "&CounterId=" + window.localStorage.getItem('COUNTERTYPE') + "&PageCode=" + pagecode + "&VD=" + VD1(),
            success: function (resp) {

                var s0_ = resp.split('~');
                var s_ = s0_[1];
                var s1_ = s0_[0];
                if (s1_ == '2') {
                    window.localStorage.setItem("RoleId", "Admin");
                    //s_ = '1';
                }
                else {
                    window.localStorage.setItem("RoleId", "User");
                }
                if (s_ == '1') {
                    if (pagecode == 'POSINVCFG') {
                        window.open('POSPrintCustomize.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSBCODG') {
                        window.open('POSBarcodeGenerator.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSCNTTYP') {
                        window.open('POSCounterType.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSCOUDF') {
                        window.open('PosCounterDef.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSCTRPRM') {
                        window.open('POSCounterPermissions.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSCUSTM') {
                        window.open('CustomerMaster.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSTENDERTYPE') {
                        window.open('TenderTypes.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSLOYPRG') {
                        window.open('LoyaltyPoints.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSIORCR') {
                        window.open('posmain.html?#', '_self', 'location=no');
                    
                    }
                    if (pagecode == 'POSSLSRT') {
                        window.open('SalesReturn.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSDISPATCH') {
                        window.open('POSDeliveryStatusDetails.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSCOUOP') {
                        window.open('CounterOpening.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSCOUCL') {
                        window.open('CounterClosing.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSACPO') {
                        window.open('AccountPosting.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSRPTINVLST') {

                        getPOSPage('POSTINL');
                    }
                    if (pagecode == 'POSRPTSLRT') {
                        getPOSPage('POSSDSEN');
                    }
                    if (pagecode == 'POSRPTINVL') {
                        getPOSPage('POSINLS');
                    }
                    if (pagecode == 'POSRPTCINV') {
                        getPOSPage('POSCINV');
                    }
                    if (pagecode == 'POSRPTDCTN') {
                        getPOSPage('POSTTDS');
                    }
                    if (pagecode == 'POSRPTDE') {
                        window.open('POSReport.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSRPTSGRW') {
                        getPOSPage('POSSGR');
                    }
                    if (pagecode == 'POSRPTICSA') {
                        window.open('ItemCategorySalesAnalysis.html', '_self', 'location=no')
                    }
                    if (pagecode == 'POSRPTBAR') {
                        window.open('BulkAnalysisReport.html', '_self', 'location=no')
                    }
                    if (pagecode == 'POSRPTWDRR') {
                        window.open('POSWeekDaysRevenueRP.html', '_self', 'location=no')
                    }
                    if (pagecode == 'POSRPTLWATV') {
                        window.open('POSLocationwiseAvgTransactionValueRPT.html', '_self', 'location=no')
                    }
                    if (pagecode == 'POSRPTICPA') {
                        window.open('POSItemCategoryProfitAnalysis.html', '_self', 'location=no')
                    }
                    if (pagecode == 'POSRPTLWPAS') {
                        window.open('POSLocationwiseProfitAnalysis.html', '_self', 'location=no')
                    }
                }
                else {
          
                 
                    navigator.notification.alert( $('#hdnAccessDenied').val(), "", "neo ecr", "Ok");
                }
            },
            error: function (e) {
            }
        });

    }

}
function getPOSPage(pagecode) {

    $('#lblreportFromDate').show();
    $('#lblreportToDate').show();
    $('#lblreportSearchBy').show();
    $('#lblreportSubOrganization').show();


    //POSACPO
    if (pagecode == "POSACPO") {
        window.open('POSReport.html?PageId=POSACPO', '_self', 'location=no');
    }

    if (pagecode == "POSINLS") {
        window.open('POSReport.html?PageId=POSINLS', '_self', 'location=no');
    } if (pagecode == "POSTINL") {
        window.open('POSReport.html?PageId=POSTINL', '_self', 'location=no');
    } if (pagecode == "POSTTDS") {
        window.open('POSReport.html?PageId=POSTTDS', '_self', 'location=no');
    } if (pagecode == "POSSGR") {
        window.open('POSReport.html?PageId=POSSGR', '_self', 'location=no');
    } if (pagecode == "POSDEND") {
        window.open('POSReport.html?PageId=POSDEND', '_self', 'location=no');
    } if (pagecode == "POSSDSEN") {
        window.open('POSReport.html?PageId=POSSDSEN', '_self', 'location=no');
    } if (pagecode == "POSTVRR") {
        window.open('POSReport.html?PageId=POSTVRR', '_self', 'location=no');
    }
    if (pagecode == "POSCINV") {
        window.open('POSReport.html?PageId=POSCINV', '_self', 'location=no');
    }
    if (pagecode == "POSRPTICSA") {
        window.open('POSReport.html?PageId=POSRPTICSA', '_self', 'location=no');
        $('#lblreportFromDate').hide();
        $('#lblreportToDate').hide();
        $('#lblreportSearchBy').hide();
        $('#lblreportSubOrganization').show();
    }
    if (pagecode == "POSRPTBAR") {
        $('#lblreportFromDate').hide();
        $('#lblreportToDate').hide();
        $('#lblreportSubOrganization').show();
        $('#lblreportSearchBy').hide();
        window.open('POSReport.html?PageId=POSRPTBAR', '_self', 'location=no');
    } if (pagecode == "POSRPTWDRR") {
        $('#lblreportFromDate').hide();
        $('#lblreportToDate').hide();
        $('#lblreportSubOrganization').show();
        $('#lblreportSearchBy').hide();
        window.open('POSReport.html?PageId=POSRPTWDRR', '_self', 'location=no');
    }
    if (pagecode == "POSRPTLWATV") {
        $('#lblreportFromDate').hide();
        $('#lblreportToDate').hide();
        $('#lblreportSubOrganization').hide();
        $('#lblreportSearchBy').hide();
        window.open('POSReport.html?PageId=POSRPTLWATV', '_self', 'location=no');
    }
    if (pagecode == "POSRPTICPA") {
        $('#lblreportFromDate').hide();
        $('#lblreportToDate').hide();
        $('#lblreportSubOrganization').hide();
        $('#lblreportSearchBy').hide();
        window.open('POSReport.html?PageId=POSRPTICPA', '_self', 'location=no');
    }
    if (pagecode == "POSRPTLWPAS") {
        $('#lblreportFromDate').hide();
        $('#lblreportToDate').hide();
        $('#lblreportSubOrganization').hide();
        $('#lblreportSearchBy').hide();
        window.open('POSReport.html?PageId=POSRPTLWPAS', '_self', 'location=no');
    }

}
function theamStroreDB(theamName) {
    if (window.localStorage.getItem('network') == '1') {
        var TheamVal = theamName;
        var LoginId = window.localStorage.getItem("Sno");
        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.LoginSetTheam,
            data: "uid=" + LoginId + "&theamval=" + TheamVal + "&VD=" + VD1(),
            success: function (resp) { 
                navigator.notification.alert( $('#hdnThameChange').val(), "", "neo ecr", "Ok");
            },
            error: function (e) {
            }
        });
    }
    else {
        navigator.notification.alert("can't Change Theme in offline network mode", "", "Mcashier", "Ok");
    }
}
var re;
function checkNetConnection() {

    $.ajax({
        type: 'POST',
        url: CashierLite.Settings.CheckConnectionExist,
        data: document.body,
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function (resp) {
            alert('S')
            //return true;
        },
        error: function (resp) {
            alert('error')
            //return false;
        }
    });
}



function GetCounterTypeTenderLocalData() {
    $('#hdnCreditCardRefStatus').val('0');
    $('#divParent').show();
    $('#divRefNumber').hide();
    $('#txtRefNumber').val('');
    $('#hdnTemBasePricePlace').val('');
    $('#hdnTransModeforPay').val('');
    $('#lblTendertype').text('');
    TenderRead('false', '');
    $('#lblTenderConvertAmt').text('');
    $('#hdn_TenderTypeId').val('');
    if (window.localStorage.getItem('TenderDetails') != null) {

        document.getElementById('divTenderType').innerHTML = window.localStorage.getItem('TenderDetails');
    }
}
function GetCounterTypeTenderData() {
    var SubOrgId = '';
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        SubOrgId = hashSplit[1];
    }

    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetCounterTypeTenderData,
        data: "CounterType=" + window.localStorage.getItem('COUNTERTYPE') + "&OrgId=" + SubOrgId + "&VD=" + VD1(),
        success: function (resp) {
            var ItemsData = resp;
            var li_ = "";


            var MainData = '';
            for (var i = 0; i < ItemsData.length; i++) {
                if (ItemsData[i].TenderID.replace(' ', '') != 'CreditNote') {
                    li_ = li_ + "<li  id=\'li_" + ItemsData[i].TenderID.replace(' ', '') + "\' onclick=ontender(\'" + ItemsData[i].TenderID.replace(' ', '') + "\',\'" + ItemsData[i].TenderType.replace(' ', '') + "\',\'" + ItemsData[i].REFERENCE.replace(' ', '') + "\',\'" + ItemsData[i].SNO + "\');>";
                    li_ = li_ + ItemsData[i].TenderID + "</li>";
                }
            } for (var i = 0; i < ItemsData.length; i++) {
                if (ItemsData[i].TenderID.replace(' ', '') == 'CreditNote') {
                    li_ = li_ + "<li  id=\'li_" + ItemsData[i].TenderID.replace(' ', '') + "\' onclick=ontender(\'" + ItemsData[i].TenderID.replace(' ', '') + "\',\'" + ItemsData[i].TenderType.replace(' ', '') + "\',\'" + ItemsData[i].REFERENCE.replace(' ', '') + "\',\'" + ItemsData[i].SNO + "\');>";
                    li_ = li_ + ItemsData[i].TenderID + "</li>";
                }
            }
            document.getElementById('divTenderType').innerHTML = li_;
            //var itemDataLocal = "<ul id='myTable'>" + li_ + "</ul>";
            window.localStorage.setItem("TenderDetails", li_);
            $('#txtItemScan').focus();
        },
        error: function (e) {

        }
    });
}
function oneChangePPOne() {
    //alert($('#hdnExchangerate').val());

    if ($('#hdnCreditCardRefStatus').val() == '1') {
        var strng = document.getElementById("txtRefNumber").value;
        document.getElementById("txtRefNumber").value = strng + "1";
        txtRefNumberKeyPress();
        return false;
    }

    if ($('#hdnTransModeforPay').val() != '') {
        if ($('#hdnTransModeforPay').val() == 'Cash') {
            if ($('#hdn_TenderTypeId').val() != '') {
                var strng = document.getElementById("txtTendertrans").value;
                document.getElementById("txtTendertrans").value = strng + "1";
                changeCals();
            }
            else {
               
                navigator.notification.alert( $('#hdnSelCurType').val(), "", "neo ecr", "Ok");
            }
        }
        else {
            var strng = document.getElementById("txtTendertrans").value;
            document.getElementById("txtTendertrans").value = strng + "1";
            changeCals();
        }

        //$('#hdnPayMode').val();
        //$('#hdnExchangerate').val();
        if ($('#hdnExchangerate').val() == '') {
            //$('#hdnTenderSubType')
            if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
                if ($('#hdnTenderSubType').val() != '')
                    tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), '1', '');
            }
            else {
                tenderAddpartial($('#hdnPayMode').val(), '1', '');
            }
        }
        else {
            if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
                if ($('#hdnTenderSubType').val() != '')
                    tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), $('#hdnExchangerate').val(), '');
            }
            else {

                tenderAddpartial($('#hdnPayMode').val(), $('#hdnExchangerate').val(), '');

            }
        }
    }
    else {
        
        navigator.notification.alert( $('#hdnSelTranType').val(), "", "neo ecr", "Ok");
    }
    partialValBalnceCal();
    amtTenderCalc();
}
function oneChangePPTwo() {

    if ($('#hdnCreditCardRefStatus').val() == '1') {
        var strng = document.getElementById("txtRefNumber").value;
        document.getElementById("txtRefNumber").value = strng + "2";
        txtRefNumberKeyPress();
        return false;
    }
    if ($('#hdnTransModeforPay').val() != '') {
        if ($('#hdnTransModeforPay').val() == 'Cash') {
            if ($('#hdn_TenderTypeId').val() != '') {
                var strng = document.getElementById("txtTendertrans").value;
                document.getElementById("txtTendertrans").value = strng + "2";
                changeCals();
            }
            else {
             
                navigator.notification.alert( $('#hdnSelCurType').val(), "", "neo ecr", "Ok");
            }
        }
        else {
            var strng = document.getElementById("txtTendertrans").value;
            document.getElementById("txtTendertrans").value = strng + "2"; changeCals();
        }

        if ($('#hdnExchangerate').val() == '') {
            //$('#hdnTenderSubType')
            if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
                if ($('#hdnTenderSubType').val() != '')
                    tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), '1', '');
            }
            else {
                tenderAddpartial($('#hdnPayMode').val(), '1', '');
            }
        }
        else {
            if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
                if ($('#hdnTenderSubType').val() != '')
                    tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), $('#hdnExchangerate').val(), '');
            }
            else {

                tenderAddpartial($('#hdnPayMode').val(), $('#hdnExchangerate').val(), '');

            }
        }
    }
    else {
        
        navigator.notification.alert( $('#hdnSelTranType').val(), "", "neo ecr", "Ok");
    }
    partialValBalnceCal();
    amtTenderCalc();
}
function oneChangePPThree() {

    if ($('#hdnCreditCardRefStatus').val() == '1') {
        var strng = document.getElementById("txtRefNumber").value;
        document.getElementById("txtRefNumber").value = strng + "3";
        txtRefNumberKeyPress();
        return false;
    }
    if ($('#hdnTransModeforPay').val() != '') {
        if ($('#hdnTransModeforPay').val() == 'Cash') {
            if ($('#hdn_TenderTypeId').val() != '') {
                var strng = document.getElementById("txtTendertrans").value;
                document.getElementById("txtTendertrans").value = strng + "3";
                changeCals();
            }
            else { 
                navigator.notification.alert( $('#hdnSelCurType').val(), "", "neo ecr", "Ok");
            }
        }
        else {
            var strng = document.getElementById("txtTendertrans").value;
            document.getElementById("txtTendertrans").value = strng + "3"; changeCals();
        }

        if ($('#hdnExchangerate').val() == '') {
            //$('#hdnTenderSubType')
            if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
                if ($('#hdnTenderSubType').val() != '')
                    tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), '1', '');
            }
            else {
                tenderAddpartial($('#hdnPayMode').val(), '1', '');
            }
        }
        else {
            if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
                if ($('#hdnTenderSubType').val() != '')
                    tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), $('#hdnExchangerate').val(), '');
            }
            else {

                tenderAddpartial($('#hdnPayMode').val(), $('#hdnExchangerate').val(), '');

            }
        }
    }
    else {
      
        navigator.notification.alert( $('#hdnSelTranType').val(), "", "neo ecr", "Ok");
    }
    partialValBalnceCal();
    amtTenderCalc();
}
function oneChangePPFour() {
    if ($('#hdnCreditCardRefStatus').val() == '1') {
        var strng = document.getElementById("txtRefNumber").value;
        document.getElementById("txtRefNumber").value = strng + "4";
        txtRefNumberKeyPress();
        return false;
    }

    if ($('#hdnTransModeforPay').val() != '') {
        if ($('#hdnTransModeforPay').val() == 'Cash') {
            if ($('#hdn_TenderTypeId').val() != '') {
                var strng = document.getElementById("txtTendertrans").value;
                document.getElementById("txtTendertrans").value = strng + "4";
                changeCals();
            }
            else {
           
                navigator.notification.alert( $('#hdnSelCurType').val(), "", "neo ecr", "Ok");
            }
        }
        else {
            var strng = document.getElementById("txtTendertrans").value;
            document.getElementById("txtTendertrans").value = strng + "4"; changeCals();
        }

        if ($('#hdnExchangerate').val() == '') {
            //$('#hdnTenderSubType')
            if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
                if ($('#hdnTenderSubType').val() != '')
                    tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), '1', '');
            }
            else {
                tenderAddpartial($('#hdnPayMode').val(), '1', '');
            }
        }
        else {
            if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
                if ($('#hdnTenderSubType').val() != '')
                    tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), $('#hdnExchangerate').val(), '');
            }
            else {

                tenderAddpartial($('#hdnPayMode').val(), $('#hdnExchangerate').val(), '');

            }
        }
    }
    else {
      
        navigator.notification.alert( $('#hdnSelTranType').val(), "", "neo ecr", "Ok");
    }
    partialValBalnceCal();
    amtTenderCalc();
}
function oneChangePPFive() {

    if ($('#hdnCreditCardRefStatus').val() == '1') {
        var strng = document.getElementById("txtRefNumber").value;
        document.getElementById("txtRefNumber").value = strng + "5";
        txtRefNumberKeyPress();
        return false;
    }

    if ($('#hdnTransModeforPay').val() != '') {
        if ($('#hdnTransModeforPay').val() == 'Cash') {
            if ($('#hdn_TenderTypeId').val() != '') {
                var strng = document.getElementById("txtTendertrans").value;
                document.getElementById("txtTendertrans").value = strng + "5";
                changeCals();
            }
            else {
                var $textAndPic = $('<div></div>');
                $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                //$textAndPic.append('<div class="alert-message">Please Select Currency Type</div>');
                $textAndPic.append('<div class="alert-message">' + $('#hdnSelCurType').val() + '</div>');
                BootstrapDialog.alert($textAndPic);
            }
        }
        else {
            var strng = document.getElementById("txtTendertrans").value;
            document.getElementById("txtTendertrans").value = strng + "5"; changeCals();
        }

        if ($('#hdnExchangerate').val() == '') {
            //$('#hdnTenderSubType')
            if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
                if ($('#hdnTenderSubType').val() != '')
                    tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), '1', '');
            }
            else {
                tenderAddpartial($('#hdnPayMode').val(), '1', '');
            }
        }
        else {
            if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
                if ($('#hdnTenderSubType').val() != '')
                    tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), $('#hdnExchangerate').val(), '');
            }
            else {

                tenderAddpartial($('#hdnPayMode').val(), $('#hdnExchangerate').val(), '');

            }
        }
    }
    else {
        var $textAndPic = $('<div></div>');
        $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        //$textAndPic.append('<div class="alert-message">Please Select Transaction Type</div>');
        $textAndPic.append('<div class="alert-message">' + $('#hdnSelTranType').val() + '</div>');
        BootstrapDialog.alert($textAndPic);
    }
    partialValBalnceCal();
    amtTenderCalc();
}
function oneChangePPSix() {

    if ($('#hdnCreditCardRefStatus').val() == '1') {
        var strng = document.getElementById("txtRefNumber").value;
        document.getElementById("txtRefNumber").value = strng + "6";
        txtRefNumberKeyPress();
        return false;
    }

    if ($('#hdnTransModeforPay').val() != '') {
        if ($('#hdnTransModeforPay').val() == 'Cash') {
            if ($('#hdn_TenderTypeId').val() != '') {
                var strng = document.getElementById("txtTendertrans").value;
                document.getElementById("txtTendertrans").value = strng + "6";
                changeCals();
            }
            else {
                var $textAndPic = $('<div></div>');
                $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                //$textAndPic.append('<div class="alert-message">Please Select Currency Type</div>');
                $textAndPic.append('<div class="alert-message">' + $('#hdnSelCurType').val() + '</div>');
                BootstrapDialog.alert($textAndPic);
            }
        }
        else {
            var strng = document.getElementById("txtTendertrans").value;
            document.getElementById("txtTendertrans").value = strng + "6"; changeCals();
        }

        if ($('#hdnExchangerate').val() == '') {
            //$('#hdnTenderSubType')
            if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
                if ($('#hdnTenderSubType').val() != '')
                    tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), '1', '');
            }
            else {
                tenderAddpartial($('#hdnPayMode').val(), '1', '');
            }
        }
        else {
            if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
                if ($('#hdnTenderSubType').val() != '')
                    tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), $('#hdnExchangerate').val(), '');
            }
            else {

                tenderAddpartial($('#hdnPayMode').val(), $('#hdnExchangerate').val(), '');

            }
        }
    }
    else {
        var $textAndPic = $('<div></div>');
        $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        //$textAndPic.append('<div class="alert-message">Please Select Transaction Type</div>');
        $textAndPic.append('<div class="alert-message">' + $('#hdnSelTranType').val() + '</div>');
        BootstrapDialog.alert($textAndPic);
    }
    partialValBalnceCal();
    amtTenderCalc();
}
function oneChangePPSeven() {


    if ($('#hdnCreditCardRefStatus').val() == '1') {
        var strng = document.getElementById("txtRefNumber").value;
        document.getElementById("txtRefNumber").value = strng + "7";
        txtRefNumberKeyPress();
        return false;
    }

    if ($('#hdnTransModeforPay').val() != '') {
        if ($('#hdnTransModeforPay').val() == 'Cash') {
            if ($('#hdn_TenderTypeId').val() != '') {
                var strng = document.getElementById("txtTendertrans").value;
                document.getElementById("txtTendertrans").value = strng + "7";
                changeCals();
            }
            else {
                var $textAndPic = $('<div></div>');
                $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                //$textAndPic.append('<div class="alert-message">Please Select Currency Type</div>');
                $textAndPic.append('<div class="alert-message">' + $('#hdnSelCurType').val() + '</div>');
                BootstrapDialog.alert($textAndPic);
            }
        }
        else {
            var strng = document.getElementById("txtTendertrans").value;
            document.getElementById("txtTendertrans").value = strng + "7"; changeCals();
        }

        if ($('#hdnExchangerate').val() == '') {
            //$('#hdnTenderSubType')
            if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
                if ($('#hdnTenderSubType').val() != '')
                    tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), '1', '');
            }
            else {
                tenderAddpartial($('#hdnPayMode').val(), '1', '');
            }
        }
        else {
            if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
                if ($('#hdnTenderSubType').val() != '')
                    tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), $('#hdnExchangerate').val(), '');
            }
            else {

                tenderAddpartial($('#hdnPayMode').val(), $('#hdnExchangerate').val(), '');

            }
        }
    }
    else {
        var $textAndPic = $('<div></div>');
        $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        //$textAndPic.append('<div class="alert-message">Please Select Transaction Type</div>');
        $textAndPic.append('<div class="alert-message">' + $('#hdnSelTranType').val() + '</div>');
        BootstrapDialog.alert($textAndPic);
    }
    partialValBalnceCal();
    amtTenderCalc();
}
function oneChangePPEight() {

    if ($('#hdnCreditCardRefStatus').val() == '1') {
        var strng = document.getElementById("txtRefNumber").value;
        document.getElementById("txtRefNumber").value = strng + "8";
        txtRefNumberKeyPress();
        return false;
    }

    if ($('#hdnTransModeforPay').val() != '') {
        if ($('#hdnTransModeforPay').val() == 'Cash') {
            if ($('#hdn_TenderTypeId').val() != '') {
                var strng = document.getElementById("txtTendertrans").value;
                document.getElementById("txtTendertrans").value = strng + "8";
                changeCals();
            }
            else {
                var $textAndPic = $('<div></div>');
                $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                //$textAndPic.append('<div class="alert-message">Please Select Currency Type</div>');
                $textAndPic.append('<div class="alert-message">' + $('#hdnSelCurType').val() + '</div>');
                BootstrapDialog.alert($textAndPic);
            }
        }
        else {
            var strng = document.getElementById("txtTendertrans").value;
            document.getElementById("txtTendertrans").value = strng + "8"; changeCals();
        }

        if ($('#hdnExchangerate').val() == '') {
            //$('#hdnTenderSubType')
            if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
                if ($('#hdnTenderSubType').val() != '')
                    tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), '1', '');
            }
            else {
                tenderAddpartial($('#hdnPayMode').val(), '1', '');
            }
        }
        else {
            if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
                if ($('#hdnTenderSubType').val() != '')
                    tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), $('#hdnExchangerate').val(), '');
            }
            else {

                tenderAddpartial($('#hdnPayMode').val(), $('#hdnExchangerate').val(), '');

            }
        }
    }
    else {
        var $textAndPic = $('<div></div>');
        $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        //$textAndPic.append('<div class="alert-message">Please Select Transaction Type</div>');
        $textAndPic.append('<div class="alert-message">' + $('#hdnSelTranType').val() + '</div>');
        BootstrapDialog.alert($textAndPic);
    }
    partialValBalnceCal();
    amtTenderCalc();
}
function oneChangePPNine() {

    if ($('#hdnCreditCardRefStatus').val() == '1') {
        var strng = document.getElementById("txtRefNumber").value;
        document.getElementById("txtRefNumber").value = strng + "9";
        txtRefNumberKeyPress();
        return false;
    }

    if ($('#hdnTransModeforPay').val() != '') {
        if ($('#hdnTransModeforPay').val() == 'Cash') {
            if ($('#hdn_TenderTypeId').val() != '') {
                var strng = document.getElementById("txtTendertrans").value;
                document.getElementById("txtTendertrans").value = strng + "9";
                changeCals();
            }
            else {
                var $textAndPic = $('<div></div>');
                $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                //$textAndPic.append('<div class="alert-message">Please Select Currency Type</div>');
                $textAndPic.append('<div class="alert-message">' + $('#hdnSelCurType').val() + '</div>');
                BootstrapDialog.alert($textAndPic);
            }
        }
        else {
            var strng = document.getElementById("txtTendertrans").value;
            document.getElementById("txtTendertrans").value = strng + "9"; changeCals();
        }

        if ($('#hdnExchangerate').val() == '') {
            //$('#hdnTenderSubType')
            if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
                if ($('#hdnTenderSubType').val() != '')
                    tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), '1', '');
            }
            else {
                tenderAddpartial($('#hdnPayMode').val(), '1', '');
            }
        }
        else {
            if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
                if ($('#hdnTenderSubType').val() != '')
                    tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), $('#hdnExchangerate').val(), '');
            }
            else {

                tenderAddpartial($('#hdnPayMode').val(), $('#hdnExchangerate').val(), '');

            }
        }
    }
    else {
        var $textAndPic = $('<div></div>');
        $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        //$textAndPic.append('<div class="alert-message">Please Select Transaction Type</div>');
        $textAndPic.append('<div class="alert-message">' + $('#hdnSelTranType').val() + '</div>');
        BootstrapDialog.alert($textAndPic);
    }
    partialValBalnceCal();
    amtTenderCalc();
}
function oneChangePPZero() {

    if ($('#hdnCreditCardRefStatus').val() == '1') {
        var strng = document.getElementById("txtRefNumber").value;
        document.getElementById("txtRefNumber").value = strng + "0";
        txtRefNumberKeyPress();
        return false;
    }

    if ($('#hdnTransModeforPay').val() != '') {
        if ($('#hdnTransModeforPay').val() == 'Cash') {
            if ($('#hdn_TenderTypeId').val() != '') {
                var strng = document.getElementById("txtTendertrans").value;
                document.getElementById("txtTendertrans").value = strng + "0";
                changeCals();
            }
            else {
                var $textAndPic = $('<div></div>');
                $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                //$textAndPic.append('<div class="alert-message">Please Select Currency Type</div>');
                $textAndPic.append('<div class="alert-message">' + $('#hdnSelCurType').val() + '</div>');
                BootstrapDialog.alert($textAndPic);
            }
        }
        else {
            var strng = document.getElementById("txtTendertrans").value;
            document.getElementById("txtTendertrans").value = strng + "0"; changeCals();
        }

        if ($('#hdnExchangerate').val() == '') {
            //$('#hdnTenderSubType')
            if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
                if ($('#hdnTenderSubType').val() != '')
                    tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), '1', '');
            }
            else {
                tenderAddpartial($('#hdnPayMode').val(), '1', '');
            }
        }
        else {
            if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
                if ($('#hdnTenderSubType').val() != '')
                    tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), $('#hdnExchangerate').val(), '');
            }
            else {

                tenderAddpartial($('#hdnPayMode').val(), $('#hdnExchangerate').val(), '');

            }
        }
    }
    else {
        var $textAndPic = $('<div></div>');
        $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        //$textAndPic.append('<div class="alert-message">Please Select Transaction Type</div>');
        $textAndPic.append('<div class="alert-message">' + $('#hdnSelTranType').val() + '</div>');
        BootstrapDialog.alert($textAndPic);
    }
    partialValBalnceCal();
    amtTenderCalc();
}
function oneChangePPDot() {

    if ($('#hdnCreditCardRefStatus').val() == '1') {
        var strng = document.getElementById("txtRefNumber").value;
        document.getElementById("txtRefNumber").value = strng + ".";
        txtRefNumberKeyPress();
        return false;
    }

    var strng = document.getElementById("txtTendertrans").value;
    document.getElementById("txtTendertrans").value = strng + ".";
    changeCals();
    $('#txtItemSearch').focus();
    if ($('#hdnExchangerate').val() == '') {
        //$('#hdnTenderSubType')
        if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
            if ($('#hdnTenderSubType').val() != '')
                tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), '1', '');
        }
        else {
            tenderAddpartial($('#hdnPayMode').val(), '1', '');
        }
    }
    else {
        if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
            if ($('#hdnTenderSubType').val() != '')
                tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), $('#hdnExchangerate').val(), '');
        }
        else {

            tenderAddpartial($('#hdnPayMode').val(), $('#hdnExchangerate').val(), '');

        }
    }
    partialValBalnceCal();
    amtTenderCalc();
}
function oneChangePPDel() {


    if ($('#hdnCreditCardRefStatus').val() == '1') {

        var strng = document.getElementById("txtRefNumber").value;
        document.getElementById("txtRefNumber").value = strng.substring(0, strng.length - 1)
        txtRefNumberKeyPress();
        return false;
    }


    var strng = document.getElementById("txtTendertrans").value;
    document.getElementById("txtTendertrans").value = strng.substring(0, strng.length - 1)
    changeCals();
    $('#txtItemSearch').focus();
    if ($('#hdnExchangerate').val() == '') {
        //$('#hdnTenderSubType')
        if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
            if ($('#hdnTenderSubType').val() != '')
                tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), '1', '');
        }
        else {
            tenderAddpartial($('#hdnPayMode').val(), '1', '');
        }
    }
    else {
        if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
            if ($('#hdnTenderSubType').val() != '')
                tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), $('#hdnExchangerate').val(), '');
        }
        else {

            tenderAddpartial($('#hdnPayMode').val(), $('#hdnExchangerate').val(), '');

        }
    }
    partialValBalnceCal();
    amtTenderCalc();
}


function ontender(TenderID, TenderType, REFERENCE, SNO) {
    var a = valueAppendtoPOPUp();
    //var myTab = document.getElementById('itemCarttable');
    if (a == false) {
        return false;
    }

    $('#hdnCreditCardRefStatus').val('0');
    $('#divParent').show();
    $('#divRefNumber').hide();
    $('#hdnExchangerate').val('');
    $('#hdn_TenderTypeId').val('');
    $('#lblTenderConvertAmt').text(''); $('#lblTendertype').text('');
    document.getElementById('divTenderType').innerHTML = window.localStorage.getItem("TenderDetails");
    window.localStorage.setItem('PayModeInvoice', TenderID);
    TenderRead('true', '');
    $('#hdnTransModeforPay').val('');
    $('#txtTendertrans').val('');
    $('#txtTendertransref').val('');
    if (TenderID == 'Cash') {
        var elem = document.getElementById("lblTransamodetypeRef");
        var elemDraft = document.getElementById("txtTendertransref");
        elem.style.display = "block";
        elemDraft.style.display = "block";

        //$('#lblTransamodetype').text('Amount');
        $('#lblTransamodetype').text($('#hdnAmount').val());
        //$('#lblTransamodetypeRef').text('Change');
        $('#lblTransamodetypeRef').text($('#hdnChange').val());
        $('#hdnPayMode').val(TenderID);
        $('#hdnPayModeType').val(TenderType);
        $('#hdnTransModeforPay').val('Cash');
        var id_ = 'li_' + TenderID;
        TenderRead('false', '');

        GetTypeTenderDataCashbyLocal();
    }
    else if (TenderID.toUpperCase() == 'CREDITNOTE') {

        $('#ModalCreditNote').modal('show');
        tenderAddpartial(TenderID, 1, '');
        $('#txtCreditNoteRefnoAmt').val('');
        $('#txtCreditNoteRefno').val('');
        $('#txtCreditNoteRefno').focus();
        $('#txtCreditNoteRefnoAmt').attr("readonly", "true");

    }
    else if (TenderID.toUpperCase() == 'REDEMPOINTS') {
        var convertPoints = parseFloat(parseFloat(parseFloat(jQuery("label[for='lblRedeem_points'").html())) / parseFloat(jQuery("label[for='lblPointsObtainBasecurrency'").html())).toFixed(4);
        // $('#').val(convertPoints);
        jQuery("label[for='lbl_Amount'").html(convertPoints);
        if (parseFloat(jQuery("label[for='lblPointsObtainBasecurrency'").html()) > 0) {
            $('#ModalRedeem').modal('show');
            //
            //alert('REDEMPOINTS');
            tenderAddpartial("REDEEMPOINTS", 0, '');

        }
        else {
            var $textAndPic = $('<div></div>');
            $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
            //$textAndPic.append('<div class="alert-message">Please Select Transaction Type</div>');
            // $textAndPic.append('<div class="alert-message">Reward Points Not Found</div>');
            $textAndPic.append('<div class="alert-message">' + $('#hdnRewardPoints404').val() + '</div>');
            BootstrapDialog.alert($textAndPic);
        }
    }
    else if (TenderID.toUpperCase() == 'REDEEMPOINTS') {
        var convertPoints = parseFloat(parseFloat(parseFloat(jQuery("label[for='lblRedeem_points'").html())) / parseFloat(jQuery("label[for='lblPointsObtainBasecurrency'").html())).toFixed(4);
        // $('#').val(convertPoints);
        jQuery("label[for='lbl_Amount'").html(convertPoints);
        if (parseFloat(jQuery("label[for='lblPointsObtainBasecurrency'").html()) > 0) {
            $('#ModalRedeem').modal('show');
            //
            //alert('REDEMPOINTS');
            tenderAddpartial("REDEEMPOINTS", 0, '');
        }
        else {
            var $textAndPic = $('<div></div>');
            $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
            //$textAndPic.append('<div class="alert-message">Please Select Transaction Type</div>');
            //  $textAndPic.append('<div class="alert-message">Reward Points Not Found</div>');
            $textAndPic.append('<div class="alert-message">' + $('#hdnRewardPoints404').val() + '</div>');
            BootstrapDialog.alert($textAndPic);
        }
    }
    else if (TenderID.toUpperCase() == 'GIFTVOUCHE') {
        $('#ModalGiftVouche').modal('show');
        tenderAddpartial('GIFTVOUCHER', 1, '');
        $('#txtGiftVoucheRefnoAmt').val('');
        $('#txtGiftVoucheRefno').val('');
        $('#txtGiftVoucheRefno').focus();
    }
    else if (TenderID.toUpperCase() == 'GIFTVOUCHER') {
        $('#ModalGiftVouche').modal('show');
        tenderAddpartial('GIFTVOUCHER', 1, '');
        $('#txtGiftVoucheRefnoAmt').val('');
        $('#txtGiftVoucheRefno').val('');
        $('#txtGiftVoucheRefno').focus();
    }
    else if (TenderType.toUpperCase().indexOf('CREDITCARD') != -1) {


        var valTy = '(' + $('#txtCurrency').val() + ')' + ' : ';
        $('#lblTendertype').text(valTy);
        var convertedVal = parseFloat($('#txtAmountpp').val());

        if ($('#hdnTemBasePricePlace').val() != '') {
            $('#lblTenderConvertAmt').text(parseFloat(convertedVal).toFixed(4));

        }
        else
            $('#lblTenderConvertAmt').text((parseFloat(convertedVal)).toFixed(4));


        //POLEDisplay("Total :" + (Math.round(convertedVal)).toFixed(2));
        //var id_ = 'li_' + TenderID;
        //document.getElementById(id_).style.cssText = 'color: #fff;background-color: #2e6da4;font-weight:bold; border-color: #2e6da4;';

        $('#hdnTransModeforPay').val('Other');

        $('#hdnPayMode').val(TenderID);
        $('#hdnPayModeType').val(TenderType);
        $('#txtTendertransref').val('');
        //Arunkumar
        //$("#txtTendertrans").focus();
        tenderAddpartial(TenderID, 1, '');
        //GetTypeTenderDataCreditbyLocal();
        FullPaymentCredit();
    }
    else if (TenderType.toUpperCase().indexOf('DEBITCARD') != -1) {


        var valTy = '(' + $('#txtCurrency').val() + ')' + ' : ';
        $('#lblTendertype').text(valTy);
        var convertedVal = parseFloat($('#txtAmountpp').val());

        if ($('#hdnTemBasePricePlace').val() != '') {
            $('#lblTenderConvertAmt').text(parseFloat(convertedVal).toFixed(4));

        }
        else
            $('#lblTenderConvertAmt').text((parseFloat(convertedVal)).toFixed(4));


        //POLEDisplay("Total :" + (Math.round(convertedVal)).toFixed(2));
        //var id_ = 'li_' + TenderID;
        //document.getElementById(id_).style.cssText = 'color: #fff;background-color: #2e6da4;font-weight:bold; border-color: #2e6da4;';

        $('#hdnTransModeforPay').val('Other');

        $('#hdnPayMode').val(TenderID);
        $('#hdnPayModeType').val(TenderType);
        $('#txtTendertransref').val('');
        //$("#txtTendertrans").focus();
        tenderAddpartial(TenderID, 1, '');
        //GetTypeTenderDataCreditbyLocal();
        FullPaymentCredit();
    }
    else if (TenderID == 'E-Pay') {
        $('#lbl_qrcodeName')[0].innerText = '';
        $("#img_QRCodePP").attr("src", "images/sampleQR.jpeg");
        $('#divWalletPayParams').attr("style", "display:none");

        $('#txtWalletNamePPSub').val('');
        $('#txtWalletBalancePPSub').val('');
        $('#txtWalletAmtPPSub').val('');
        $('#txtWalletRefPPSub').val('');




        $('#txt_amount_Ref').val('');
        $('#txt_amount_UPI').val('');
        $('#txt_balance_UPI').val('');
        $('#TXT_upiid').val('');



        $('#hdnWalletCheckVerify').val('0');

        $('#hdn_TYPEWallet').val('0');

        $('#home').attr('style', 'display:block');
        $('#wallet-pay').attr('style', 'display:none');
        $('#footer-pay').attr('style', 'display:none');
        $('#UPI-pay').attr('style', 'display:none');

        $('#div_form-section-UPI_UPI').attr('style', 'display:none');
        $('#myModal-payment').modal();



        var SubOrgId = '';
        var HashValues = (window.localStorage.getItem("hashValues"));
        if (HashValues != null && HashValues != '') {
            var hashSplit = HashValues.split('~');

            SubOrgId = hashSplit[1];

        }

        GetConfigDataINaPPfUN(SubOrgId);

    }
    else {
        var valTy = '(' + $('#txtCurrency').val() + ')' + ' : ';
        $('#lblTendertype').text(valTy);
        var convertedVal = parseFloat($('#txtAmountpp').val());

        if ($('#hdnTemBasePricePlace').val() != '') {
            $('#lblTenderConvertAmt').text(parseFloat(convertedVal).toFixed(4));

        }
        else
            $('#lblTenderConvertAmt').text((parseFloat(convertedVal)).toFixed(4));


        POLEDisplay("Total :" + (parseFloat(convertedVal)).toFixed(4));
        var id_ = 'li_' + TenderID;
        document.getElementById(id_).style.cssText = 'color: #fff;background-color: #2e6da4;font-weight:bold; border-color: #2e6da4;';
        //$('#lblTransamodetype').text('ReferenceNo.');
        //$('#lblTransamodetypeRef').text('ExpDate');

        //$('#lblTransamodetype').text($('#hdnRefNo').val());
        //$('#lblTransamodetypeRef').text($('#hdnExpDt').val());

        //var elem = document.getElementById("lblTransamodetypeRef");
        //var elemDraft = document.getElementById("txtTendertransref");
        //elem.style.display = "none";
        //elemDraft.style.display = "none";

        $('#hdnTransModeforPay').val('Other');

        $('#hdnPayMode').val(TenderID);
        $('#hdnPayModeType').val(TenderType);
        $('#txtTendertransref').val('');
        $("#txtTendertrans").focus();
        tenderAddpartial(TenderID, 1, '');
    }
    partialValBalnceCal();
    amtTenderCalc();
}


function GetConfigDataINaPPfUN(SubOrgId) {

    $.ajax({
        url: CashierLite.Settings.GetConfigWalletData,
        data: { SubOrgId: SubOrgId, VD: VD() },
        dataType: "json",
        type: 'GET',
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.length != 0) {


                var li_ = ''; var liUPI_ = '';
                for (var i = 0; i < data.length; i++) {
                    var id_ = 'li_' + data[i].ID;
                    var logoimg = '';
                    if (data[i].LogoPath != "") {
                        logoimg = "<img src='" + data[i].LogoPath + "' style='width:30px;height:30px;' />";
                    }
                    if (data[i].Status.split(',')[0].toUpperCase() == 'TRUE')
                        if (data[i].Status.split(',')[1].toUpperCase() == 'TRUE') {
                            li_ = li_ + "<li onclick=ConvertWalletAmount_n1(\'" + data[i].QRPath + "\',\'" + data[i].Name.replace(' ', '') + "\',\'" + data[i].ID + "\',\'" + data[i].LogoPath + "\',\'" + data[i].Desc + "\') ><a href='#' id=" + data[i].ID + "> " + logoimg + "</a><small class ='desc'>" + data[i].Name + "</small></li>";

                        }
                        else {
                            liUPI_ = liUPI_ + "<li onclick=ConvertWalletAmount_n2(\'" + data[i].QRPath + "\',\'" + data[i].Name.replace(' ', '') + "\',\'" + data[i].ID + "\',\'" + data[i].LogoPath + "\',\'" + data[i].Desc + "\') ><a href='#' id=" + data[i].ID + "> " + logoimg + "</a><small class ='desc'>" + data[i].Name + "</small></li>";

                        }

                    //li_ = li_ + "<li id=" + id_ + "  onclick=ConvertWalletAmount(\'" + data[i].QRPath + "\',\'" + data[i].Name.replace(' ', '') + "\',\'" + data[i].ID + "\') style='color: #fff;background-color: #8A97AD;border-color: #8A97AD;width:100%;margin-bottom: 2px'>" + logoimg + "&nbsp;&nbsp;&nbsp" + data[i].Name + "</li>";
                }

                window.localStorage.setItem("walletData", li_);
                window.localStorage.setItem("walletDataUPI", liUPI_);



                document.getElementById('div_ul_walletData_n1').innerHTML = li_;

                document.getElementById('div_ul_walletData_n_upi').innerHTML = liUPI_;

                document.getElementById('divWallet').innerHTML = li_;


            }
            else {

            }

        },
        error: function (e) {

        }
    });
}
function ConvertWalletAmount_n2(QRParth, Name, ID, imgPath, desc) {

    var balance_ = $('#lbl_TpartTotal')[0].innerText.replace('-', '');
    if (parseFloat(balance_) > 0) {
        $('#hdn_TYPEWallet').val('2');
        $('#hdnWalletCheckVerify').val('1');
        $('#wallet-pay,#form-section-wallet,#footer-pay').show();
        $('#home,#form-section').hide();

        $("#img_QRCodePP_n2").attr("src", imgPath);
        $("#img_QRCodePP_New_n1").attr("src", QRParth);

        //$('#lbl_qrcodeName_n1')[0].innerText = "UPI";
        $('#lbl_QRCOde_Mode_UPI')[0].innerText = desc;
        $('#lbl_qrcodeName_n2')[0].innerText = (Name);
        //$('#divWalletPayParams').attr("style", "display:block");
        var li_ = '';
        //li_ = li_ + "<li onclick=gotoWallet() style='color: #fff;background-color: #49a3f1;border-color: #49a3f1;width:100%;margin-bottom: 2px'> <i class='fa fa-angle-up'></i>&nbsp;&nbsp;&nbsp Back to Wallet</li>";
        $('#hdnWalletCheckVerify').val('1');
        //document.getElementById('divWallet').innerHTML = li_;
        $('#txtWalletNamePPSub').val('UPI');
        $('#txt_balance_UPI').val(balance_);
        $('#txt_amount_UPI').val(balance_);
        $('#txt_amount_Ref').focus();
        $('#bhim-pay,#footer-pay,#QR-code-pay_2').show();
        $('#div_form-section-UPI_UPI,#div_qrimg_2').hide();

        $('#txtWalletNamePPSub').val(Name);

        QRShow2();
    }
    else {
        var $textAndPic = $('<div></div>');
        $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        //$textAndPic.append('<div class="alert-message">Please Select Transaction Type</div>');
        // $textAndPic.append('<div class="alert-message">Please Enter Amount</div>');
        $textAndPic.append('<div class="alert-message">Please Check Balance Amount</div>');
        BootstrapDialog.alert($textAndPic);
    }
}
function QRShow() {

    $('#div_qrimg').show();
    $('#QR-code-pay_1').hide();
    //alert('sa');
}
function QRShow2() {

    $('#div_qrimg_2').show();
    $('#QR-code-pay_2').hide();
    //alert('sa');
}
function ConvertWalletAmount_n1(QRParth, Name, ID, imgPath, desc) {
    $('#hdn_TYPEWallet').val('1');
    $('#QR-code-pay_1').show();
    var balance_ = $('#lbl_TpartTotal')[0].innerText.replace('-', '');
    if (parseFloat(balance_) > 0) {
        $('#lbl_qrcodeName_n1')[0].innerText = Name;

        $("#img_QRCodePP_n1").attr("src", imgPath);
        $("#img_QRCodePP_New").attr("src", QRParth);
        $('#lbl_QRCOde_Mode_wallet')[0].innerText = desc;

        //$('#divWalletPayParams').attr("style", "display:block");
        var li_ = '';

        //li_ = li_ + "<li onclick=gotoWallet() style='color: #fff;background-color: #49a3f1;border-color: #49a3f1;width:100%;margin-bottom: 2px'> <i class='fa fa-angle-up'></i>&nbsp;&nbsp;&nbsp Back to Wallet</li>";
        $('#hdnWalletCheckVerify').val('1');
        //document.getElementById('divWallet').innerHTML = li_;
        $('#txtWalletNamePPSub').val(Name);
        $('#txtWalletBalancePPSub').val(balance_);
        $('#txtWalletAmtPPSub').val(balance_);

        $('#txtWalletRefPPSub').focus();
        $('#bhim-pay,#footer-pay').show();
        $('#form-section-UPI,#QR-code-pay').hide();
        QRShow();
    }
    else {
        var $textAndPic = $('<div></div>');
        $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        //$textAndPic.append('<div class="alert-message">Please Select Transaction Type</div>');
        // $textAndPic.append('<div class="alert-message">Please Enter Amount</div>');
        $textAndPic.append('<div class="alert-message">Please Check Balance Amount</div>');
        BootstrapDialog.alert($textAndPic);
    }
}
function ConvertWalletAmount(QRParth, Name, ID) {


    var balance_ = $('#lbl_TpartTotal')[0].innerText.replace('-', '');
    if (parseFloat(balance_) > 0) {
        $('#lbl_qrcodeName')[0].innerText = Name;

        $("#img_QRCodePP").attr("src", QRParth);
        $('#divWalletPayParams').attr("style", "display:block");
        var li_ = '';

        li_ = li_ + "<li onclick=gotoWallet() style='color: #fff;background-color: #49a3f1;border-color: #49a3f1;width:100%;margin-bottom: 2px'> <i class='fa fa-angle-up'></i>&nbsp;&nbsp;&nbsp Back to Wallet</li>";
        $('#hdnWalletCheckVerify').val('1');
        document.getElementById('divWallet').innerHTML = li_;
        $('#txtWalletNamePPSub').val(Name);
        $('#txtWalletBalancePPSub').val(balance_);
        $('#txtWalletAmtPPSub').val(balance_);

        $('#txtWalletRefPPSub').focus();
    }
    else {
        var $textAndPic = $('<div></div>');
        $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        //$textAndPic.append('<div class="alert-message">Please Select Transaction Type</div>');
        // $textAndPic.append('<div class="alert-message">Please Enter Amount</div>');
        $textAndPic.append('<div class="alert-message">Please Check Balance Amount</div>');
        BootstrapDialog.alert($textAndPic);
    }
}

function gotoWallet() {
    $('#hdnWalletCheckVerify').val('0');
    $('#divWalletPayParams').attr("style", "display:none");
    $('#lbl_qrcodeName')[0].innerText = '';
    $("#img_QRCodePP").attr("src", "images/sampleQR.jpeg");

    document.getElementById('divWallet').innerHTML = window.localStorage.getItem("walletData");
}
function payviaWallet() {

    var mode_ = $('#hdn_TYPEWallet').val();
    if (mode_ == '1') {


        var Name = $('#txtWalletNamePPSub').val();
        if ($('#hdnWalletCheckVerify').val() == '1') {

            //if ($('#txtWalletRefPPSub').val() != '') {
            if (parseFloat($('#txtWalletAmtPPSub').val()) > 0) {
                Name = 'E-Pay_Wallet-' + Name
                var type = Name.replace(' ', '');
                tenderAddpartial(Name, 1, '');
                var myTab = document.getElementById('tbl_tenderDyms');
                var m_ = ""; var lCheck_ = '0';
                $('#hdnGiftVoucheRefId').val($('#txtWalletRefPPSub').val());
                // LOOP THROUGH EACH ROW OF THE TABLE AFTER HEADER.
                for (i = 0; i < myTab.rows.length; i++) {
                    // GET THE CELLS COLLECTION OF THE CURRENT ROW.
                    var objCells = myTab.rows.item(i).cells;
                    if (objCells.length > 0) {
                        // LOOP THROUGH EACH CELL OF THE CURENT ROW TO READ CELL VALUES.
                        if (type.toUpperCase() == objCells.item(1).innerHTML.toUpperCase()) {
                            //var markup = "<td  style='width:5% !important' ><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right' >" + type.toUpperCase() + "</td><td>" + parseFloat(amount).toFixed(2) + "</td>";
                            //$('#tr_' + type).html(markup);
                            $('#tr_' + type.toUpperCase())[0].childNodes[2].innerText = parseFloat(1).toFixed(4);
                            $('#tr_' + type.toUpperCase())[0].childNodes[3].innerText = parseFloat(parseFloat($('#txtWalletAmtPPSub').val())).toFixed(4);
                            $('#tr_' + type.toUpperCase())[0].childNodes[4].innerText = parseFloat(parseFloat($('#txtWalletAmtPPSub').val()) * parseFloat(1)).toFixed(4);
                            $('#tr_' + type.toUpperCase())[0].childNodes[5].innerText = $('#hdnGiftVoucheRefId').val();
                            lCheck_ = '1';

                            break;
                        }
                    }
                }

                partialValBalnceCal();
                $('#ModalWallet').modal('hide');
                $('#myModal-payment').modal('hide');
            }
            else {
                var $textAndPic = $('<div></div>');
                $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                //$textAndPic.append('<div class="alert-message">Please Select Transaction Type</div>');
                // $textAndPic.append('<div class="alert-message">Please Enter Amount</div>');
                $textAndPic.append('<div class="alert-message">' + $('#hdnPlsEnterAmount').val() + '</div>');
                BootstrapDialog.alert($textAndPic);
            }
            //}
            //else {
            //    var $textAndPic = $('<div></div>');
            //    $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
            //    //$textAndPic.append('<div class="alert-message">Please Select Transaction Type</div>');
            //    // $textAndPic.append('<div class="alert-message">Please Enter Amount</div>');
            //    $textAndPic.append('<div class="alert-message">Please Enter Reference No.</div>');
            //    BootstrapDialog.alert($textAndPic);
            //}
        }
        else {
            var $textAndPic = $('<div></div>');
            $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
            //$textAndPic.append('<div class="alert-message">Please Select Transaction Type</div>');
            // $textAndPic.append('<div class="alert-message">Please Enter Amount</div>');
            $textAndPic.append('<div class="alert-message">Please Select at least One Wallet</div>');
            BootstrapDialog.alert($textAndPic);
        }
    }
    else if (mode_ == '2') {
        var Name = $('#txtWalletNamePPSub').val();
        if ($('#hdnWalletCheckVerify').val() == '1') {

            //if ($('#TXT_upiid').val() == '') {

            //    var $textAndPic = $('<div></div>');
            //    $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
            //    //$textAndPic.append('<div class="alert-message">Please Select Transaction Type</div>');
            //    // $textAndPic.append('<div class="alert-message">Please Enter Amount</div>');
            //    $textAndPic.append('<div class="alert-message">Please Enter UPI ID</div>');
            //    BootstrapDialog.alert($textAndPic);
            //    return false;
            //}

            if ($('#txt_amount_UPI').val() != '') {
                //Name = 'UPI'
                if (parseFloat($('#txt_amount_UPI').val()) > 0) {
                    Name = 'E-Pay_UPI-' + Name
                    var type = Name.replace(' ', '');
                    tenderAddpartial(Name, 1, '');
                    var myTab = document.getElementById('tbl_tenderDyms');
                    var m_ = ""; var lCheck_ = '0';
                    $('#hdnGiftVoucheRefId').val($('#txt_amount_Ref').val() + '~' + $('#TXT_upiid').val());
                    // LOOP THROUGH EACH ROW OF THE TABLE AFTER HEADER.
                    for (i = 0; i < myTab.rows.length; i++) {
                        // GET THE CELLS COLLECTION OF THE CURRENT ROW.
                        var objCells = myTab.rows.item(i).cells;
                        if (objCells.length > 0) {
                            // LOOP THROUGH EACH CELL OF THE CURENT ROW TO READ CELL VALUES.
                            if (type.toUpperCase() == objCells.item(1).innerHTML.toUpperCase()) {
                                //var markup = "<td  style='width:5% !important' ><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right' >" + type.toUpperCase() + "</td><td>" + parseFloat(amount).toFixed(2) + "</td>";
                                //$('#tr_' + type).html(markup);
                                $('#tr_' + type.toUpperCase())[0].childNodes[2].innerText = parseFloat(1).toFixed(4);
                                $('#tr_' + type.toUpperCase())[0].childNodes[3].innerText = parseFloat(parseFloat($('#txt_amount_UPI').val())).toFixed(4);
                                $('#tr_' + type.toUpperCase())[0].childNodes[4].innerText = parseFloat(parseFloat($('#txt_amount_UPI').val()) * parseFloat(1)).toFixed(4);
                                $('#tr_' + type.toUpperCase())[0].childNodes[5].innerText = $('#hdnGiftVoucheRefId').val();
                                lCheck_ = '1';

                                break;
                            }
                        }
                    }

                    partialValBalnceCal();
                    $('#ModalWallet').modal('hide');
                    $('#myModal-payment').modal('hide');
                }
                else {
                    var $textAndPic = $('<div></div>');
                    $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                    //$textAndPic.append('<div class="alert-message">Please Select Transaction Type</div>');
                    // $textAndPic.append('<div class="alert-message">Please Enter Amount</div>');
                    $textAndPic.append('<div class="alert-message">' + $('#hdnPlsEnterAmount').val() + '</div>');
                    BootstrapDialog.alert($textAndPic);
                }
            }
            else {
                var $textAndPic = $('<div></div>');
                $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                //$textAndPic.append('<div class="alert-message">Please Select Transaction Type</div>');
                // $textAndPic.append('<div class="alert-message">Please Enter Amount</div>');
                $textAndPic.append('<div class="alert-message">Please Enter Reference No.</div>');
                BootstrapDialog.alert($textAndPic);
            }
        }
        else {
            var $textAndPic = $('<div></div>');
            $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
            //$textAndPic.append('<div class="alert-message">Please Select Transaction Type</div>');
            // $textAndPic.append('<div class="alert-message">Please Enter Amount</div>');
            $textAndPic.append('<div class="alert-message">Please Select at least One Wallet</div>');
            BootstrapDialog.alert($textAndPic);
        }
    }
}
function GiftVouche() {
    if (parseFloat($('#txtGiftVoucheRefnoAmt').val()) > 0) {
        var type = 'GIFTVOUCHER';
        var myTab = document.getElementById('tbl_tenderDyms');
        var m_ = ""; var lCheck_ = '0';
        $('#hdnGiftVoucheRefId').val($('#txtGiftVoucheRefno').val());
        // LOOP THROUGH EACH ROW OF THE TABLE AFTER HEADER.
        for (i = 0; i < myTab.rows.length; i++) {
            // GET THE CELLS COLLECTION OF THE CURRENT ROW.
            var objCells = myTab.rows.item(i).cells;
            if (objCells.length > 0) {
                // LOOP THROUGH EACH CELL OF THE CURENT ROW TO READ CELL VALUES.
                if (type.toUpperCase() == objCells.item(1).innerHTML.toUpperCase()) {
                    //var markup = "<td  style='width:5% !important' ><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right' >" + type.toUpperCase() + "</td><td>" + parseFloat(amount).toFixed(2) + "</td>";
                    //$('#tr_' + type).html(markup);
                    $('#tr_' + type.toUpperCase())[0].childNodes[2].innerText = parseFloat(1).toFixed(4);
                    $('#tr_' + type.toUpperCase())[0].childNodes[3].innerText = parseFloat(parseFloat($('#txtGiftVoucheRefnoAmt').val())).toFixed(4);
                    $('#tr_' + type.toUpperCase())[0].childNodes[4].innerText = parseFloat(parseFloat($('#txtGiftVoucheRefnoAmt').val()) * parseFloat(1)).toFixed(4);
                    $('#tr_' + type.toUpperCase())[0].childNodes[5].innerText = $('#hdnGiftVoucheRefId').val();
                    lCheck_ = '1';

                    break;
                }
            }
        }
        $('#txtGiftVoucheRefno').val('');
        $('#txtGiftVoucheRefnoAmt').val('');
        $('#hdnGiftVoucheRefId').val('');
        partialValBalnceCal();
        $('#ModalGiftVouche').modal('hide');
    }
    else {
        var $textAndPic = $('<div></div>');
        $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        //$textAndPic.append('<div class="alert-message">Please Select Transaction Type</div>');
        // $textAndPic.append('<div class="alert-message">Please Enter Amount</div>');
        $textAndPic.append('<div class="alert-message">' + $('#hdnPlsEnterAmount').val() + '</div>');
        BootstrapDialog.alert($textAndPic);
    }
}
function txtRedeemPointskeyUP() {

    var points = (parseFloat(jQuery("label[for='lblPointsObtainBasecurrency'").html()));
    //txt_RedeemPointsAmt
    if ($('#txt_RedeemPoints').val() != '') {

        var convertPoints = parseFloat($('#txt_RedeemPoints').val()) / parseFloat(jQuery("label[for='lblPointsObtainBasecurrency'").html());
        $('#txt_RedeemPointsAmt').val(convertPoints);
    }
    else {
        $('#txt_RedeemPointsAmt').val('0');
    }
    //var txt_RedeemPointsAmt


}
function RedeemPointsSet() {


    if (parseFloat(jQuery("label[for='lblRewardPoints'").html()) >= parseFloat($('#txt_RedeemPoints').val())) {

        if (parseFloat(jQuery("label[for='lblMinPoints'").html()) <= parseFloat($('#txt_RedeemPoints').val())) {
            if (parseFloat(jQuery("label[for='lblMaxPoints'").html()) >= parseFloat($('#txt_RedeemPoints').val())) {
                //
                var type = 'REDEEMPOINTS';
                var myTab = document.getElementById('tbl_tenderDyms');
                var m_ = ""; var lCheck_ = '0';
                // LOOP THROUGH EACH ROW OF THE TABLE AFTER HEADER.
                for (i = 0; i < myTab.rows.length; i++) {
                    // GET THE CELLS COLLECTION OF THE CURRENT ROW.
                    var objCells = myTab.rows.item(i).cells;
                    if (objCells.length > 0) {
                        // LOOP THROUGH EACH CELL OF THE CURENT ROW TO READ CELL VALUES.
                        if (type.toUpperCase() == objCells.item(1).innerHTML.toUpperCase()) {
                            //var markup = "<td  style='width:5% !important' ><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right' >" + type.toUpperCase() + "</td><td>" + parseFloat(amount).toFixed(2) + "</td>";
                            //$('#tr_' + type).html(markup);
                            $('#tr_' + type.toUpperCase())[0].childNodes[2].innerText = "1.000";
                            $('#tr_' + type.toUpperCase())[0].childNodes[3].innerText = parseFloat(parseFloat($('#txt_RedeemPointsAmt').val())).toFixed(4);
                            $('#tr_' + type.toUpperCase())[0].childNodes[4].innerText = parseFloat(parseFloat($('#txt_RedeemPointsAmt').val()) * parseFloat(1)).toFixed(4);
                            $('#tr_' + type.toUpperCase())[0].childNodes[5].innerText = $('#txt_RedeemPoints').val();
                            lCheck_ = '1';
                            break;
                        }
                    }
                }

                $('#txt_RedeemPointsAmt').val('');
                $('#txt_RedeemPoints').val('');
                partialValBalnceCal();
                $('#ModalRedeem').modal('hide');

            }
            else {
                var $textAndPic = $('<div></div>');
                $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                //$textAndPic.append('<div class="alert-message">Please Select Transaction Type</div>');
                $textAndPic.append('<div class="alert-message">' + $('#hdnrewardptsLstrMinpts').val() + '</div>');
                // $textAndPic.append('<div class="alert-message">Reward Points Must be less than or Equals to Maximum Points( ' + jQuery("label[for='lblMaxPoints'").html() + ' )</div>');
                BootstrapDialog.alert($textAndPic);
            }
        }
        else {
            var $textAndPic = $('<div></div>');
            $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
            //$textAndPic.append('<div class="alert-message">Please Select Transaction Type</div>');
            $textAndPic.append('<div class="alert-message">' + $('#hdnrewardptsgrtrMinpts').val() + '</div>');
            // $textAndPic.append('<div class="alert-message">Reward Points Must be grater than or Equals to Minimum Points ( ' + jQuery("label[for='lblMinPoints'").html() + ' )</div>');
            BootstrapDialog.alert($textAndPic);
        }
    }
    else {
        var $textAndPic = $('<div></div>');
        $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        //$textAndPic.append('<div class="alert-message">Please Select Transaction Type</div>');
        $textAndPic.append('<div class="alert-message">' + $('#hdnrewardptsLstrMinpts').val() + '</div>');
        //$textAndPic.append('<div class="alert-message">Reward Points Must be Less than or Equals to Existing Points</div>');
        BootstrapDialog.alert($textAndPic);

    }
}
function CrediNoteSet() {
    if ($('#txtCreditNoteRefno').val().trim() != "") {
        var type = 'CREDITNOTE';
        var myTab = document.getElementById('tbl_tenderDyms');
        var m_ = ""; var lCheck_ = '0';
        // LOOP THROUGH EACH ROW OF THE TABLE AFTER HEADER.
        for (i = 0; i < myTab.rows.length; i++) {
            // GET THE CELLS COLLECTION OF THE CURRENT ROW.
            var objCells = myTab.rows.item(i).cells;
            if (objCells.length > 0) {
                if ($('#hdnCreditCodeRefId').val() != '') {
                    // LOOP THROUGH EACH CELL OF THE CURENT ROW TO READ CELL VALUES.
                    if (type.toUpperCase() == objCells.item(1).innerHTML.toUpperCase()) {
                        //var markup = "<td  style='width:5% !important' ><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right' >" + type.toUpperCase() + "</td><td>" + parseFloat(amount).toFixed(2) + "</td>";
                        //$('#tr_' + type).html(markup);
                        $('#tr_' + type.toUpperCase())[0].childNodes[2].innerText = parseFloat(1).toFixed(4);
                        $('#tr_' + type.toUpperCase())[0].childNodes[3].innerText = parseFloat(parseFloat($('#txtCreditNoteRefnoAmt').val()));
                        $('#tr_' + type.toUpperCase())[0].childNodes[4].innerText = parseFloat(parseFloat($('#txtCreditNoteRefnoAmt').val()) * parseFloat(1)).toFixed(4);
                        $('#tr_' + type.toUpperCase())[0].childNodes[5].innerText = $('#hdnCreditCodeRefId').val();
                        lCheck_ = '1';

                        break;
                    }
                }
                else
                {
                    alert('Please select CreditNote')
                    return false;
                }

            }
        }
        $('#txtCreditNoteRefno').val('');
        $('#txtCreditNoteRefnoAmt').val('');
        $('#hdnCreditCodeRefId').val('');
        //partialValBalnceCal();
        partialValBalnceCalForCreditNote();
    }
    $('#ModalCreditNote').modal('hide');
}
try {
    $("[id$='txtCreditNoteRefno']").autocomplete({

        source: function (request, response) {
            var HashValues = (window.localStorage.getItem("hashValues"));
            if (HashValues != null && HashValues != '') {
                var hashSplit = HashValues.split('~');
                SubOrgId = hashSplit[1];
            }
            var param = "passParms=" + $('#txtCreditNoteRefno').val() + "&SubOrgId=" + SubOrgId + "&VD=" + VD1();
            $.ajax({
                url: CashierLite.Settings.CreditNoteData,
                data: param,
                dataType: "json",
                type: "Get",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            label: MMSplitDecode(item, 'Š')[1],
                            val: MMSplitDecode(item, 'Š')[0],
                            Amt: MMSplitDecode(item, 'Š')[2]
                        }
                    }))
                },
                error: function (response) {
                    //alert(response.responseText);
                },
                failure: function (response) {
                    //alert(response.responseText);
                }
            });

        },
        change: function (e, i) {
            if (i.item) {
                // do whatever you want to when the item is found
            }
            else {
                $('#txtCreditNoteRefno').val('');
                $('#txtCreditNoteRefnoAmt').val('');
                $('#hdnCreditCodeRefId').val('');
            }
        },
        select: function (e, i) {

            //alert(i.item.val);
            //$('#txtCreditNoteRefno').val('');
            $('#txtCreditNoteRefnoAmt').val(i.item.Amt);
            $('#hdnCreditCodeRefId').val(i.item.val);
            //rowActive.getCellFromKey('Profile').setValue(i.item.label);


        },
        minLength: 1
    });
}
catch (a) {

}

function changeCallKeyUp() {

    if ($('#hdnTransModeforPay').val() != '') {

        if ($('#hdnExchangerate').val() == '') {
            //$('#hdnTenderSubType')
            if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
                tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), '1', '');
            }
            else {
                tenderAddpartial($('#hdnPayMode').val(), '1', '');
            }
        }
        else {
            if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
                tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), $('#hdnExchangerate').val(), '');
            }
            else {

                tenderAddpartial($('#hdnPayMode').val(), $('#hdnExchangerate').val(), '');

            }
        }
    }
    else {
        var $textAndPic = $('<div></div>');
        $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        //$textAndPic.append('<div class="alert-message">Please Select Transaction Type</div>');
        $textAndPic.append('<div class="alert-message">' + $('#hdnSelTranType').val() + '</div>');
        BootstrapDialog.alert($textAndPic);
        $('#txtTendertrans').val('');
        return false;
    }
    //alert('a');
    partialValBalnceCal($('#hdnExchangerate').val());
    amtTenderCalc();
}

function changeCals() {
    if ($('#hdnTransModeforPay').val() == 'Cash') {

        var val_ = parseFloat($('#txtTendertrans').val()) - parseFloat($('#lblTenderConvertAmt').text());
        if (isNaN(val_)) {
            $('#txtTendertransref').val('');
        }
        else {
            $('#txtTendertransref').val(parseFloat(val_).toFixed(4));
        }
    }
    else {
        var val_ = parseFloat($('#txtTendertrans').val()) - parseFloat($('#lblTenderConvertAmt').text());
        if (isNaN(val_)) {
            $('#txtTendertransref').val('');
        }
        else
            $('#txtTendertransref').val(parseFloat(val_).toFixed(4));
    }

}
function TenderRead(bool, mode) {

    $('#txtTendertrans').val('');
    $('#txtTendertransref').val('');
    if (bool == 'true') {
        $('#txtTendertrans').removeAttr("readonly", "true");
        $("#txtTendertrans").removeAttr("disabled", "disabled");
        //$('#txtTendertransref').removeAttr("readonly", "true");
        //$("#txtTendertransref").removeAttr("disabled", "disabled");
    }
    else {
        $('#txtTendertrans').attr("readonly", "true");
        $("#txtTendertrans").attr("disabled", "disabled");
        //$('#txtTendertransref').attr("readonly", "true");
        //$("#txtTendertransref").attr("disabled", "disabled");
    }
    if (mode == 'cash') {

        //$('#txtTendertransref').attr("readonly", "true");
        //$("#txtTendertransref").attr("disabled", "disabled");
    }
    //txtTendertrans
}
function GetTypeTenderDataCashbyLocal() {

    document.getElementById('divTenderType').childNodes[0].nodeValue = null;
    //alert('1');
    document.getElementById('divTenderType').innerHTML = window.localStorage.getItem('liTenderType');

    $('#hdnTemBasePrice').val(window.localStorage.getItem('BaseCurrencyCode'));
    if ($('#hdnTemBasePrice').val() != '') {
        //alert('a');
        var id_ = 'li_' + $('#hdnTemBasePrice').val();
        document.getElementById(id_).style.cssText = 'color: #fff;background-color: navy;font-weight:bold; border-color: navy;';
        for (var m = 1; m < $('#divTenderType li').length; m++) {
            var liId_ = "#" + $('#divTenderType li')[m].id;
            $(liId_)[0].onclick = null;
        }

        $('#hdnTransModeforPay').val('Cash');
        var tenderType_ = $('#hdnTemBasePrice').val();
        var exchangerate_ = $('#hdnTemBasePriceEX').val();
        var tendeId_ = $('#hdnTemBasePriceTend').val();
        var CurrencyId_ = $('#hdnTemBasePriceCurId').val();

        ConverTenderAmountSub(tenderType_, exchangerate_, tendeId_, CurrencyId_);

        var myTab = document.getElementById('tbl_tenderDyms');
        var TenSubid_ = 'CASH_' + $('#hdnTemBasePrice').val().toUpperCase();
        tenderAddpartial(TenSubid_, 1, '');

        for (i = 0; i < myTab.rows.length; i++) {
            var objCells = myTab.rows.item(i).cells;
            if (objCells.length > 0) {
                if (objCells.item(1).innerHTML.toUpperCase() == TenSubid_) {
                    $('#txtTendertrans').val(objCells.item(3).innerHTML);
                }
            }
        }
        //$('#txtTendertrans').removeAttr("readonly", "true");
        //$("#txtTendertrans").removeAttr("disabled", "disabled");
        //$('#txtTendertransref').removeAttr("readonly", "true");
        //$("#txtTendertransref").removeAttr("disabled", "disabled");
        //$('#txtTendertrans').focus();
        $('#txtTendertrans').val('');
    }
}
function GetTypeTenderDataCreditbyLocal() {

    document.getElementById('divTenderType').childNodes[0].nodeValue = null;

    var li_ = "<li style='color: #fff;background-color: #49a3f1;border-color: #49a3f1;' onClick=GetCounterTypeTenderLocalData();><i class='fa fa-angle-up'></i> Back</li>";

    li_ = li_ + "<li    onclick=FullPaymentCredit(); style='color: #fff;background-color: #2e6da4;border-color: #2e6da4;'>Full Payment</li>";

    li_ = li_ + "<li  onclick=GetTypeTenderDataCreditbyLocal(); style='color: #fff;background-color: navy;font-weight:bold; border-color: navy;'>Partial Payment</li>";
    li_ = li_ + "<li  onclick=FullPaymentRefNum(); style='color: #fff;background-color: #2e6da4;border-color: #2e6da4;'>Reference Number</li>";


    document.getElementById('divTenderType').innerHTML = li_;

    $('#txtTendertrans').focus();
    $('#hdnCreditCardRefStatus').val('0');
    //$('#txtTendertrans').val('');
    $('#divParent').show();
    $('#divRefNumber').hide();
}
function FullPaymentRefNum() {
    document.getElementById('divTenderType').childNodes[0].nodeValue = null;

    var li_ = "<li style='color: #fff;background-color: #49a3f1;border-color: #49a3f1;' onClick=GetCounterTypeTenderLocalData();><i class='fa fa-angle-up'></i> Back</li>";

    li_ = li_ + "<li    onclick=FullPaymentCredit(); style='color: #fff;background-color: #2e6da4;border-color: #2e6da4;'>Full Payment</li>";

    li_ = li_ + "<li  onclick=GetTypeTenderDataCreditbyLocal(); style='color: #fff;background-color: #2e6da4;border-color: #2e6da4;'>Partial Payment</li>";
    li_ = li_ + "<li  onclick=FullPaymentRefNum(); style='color: #fff;background-color: navy;font-weight:bold; border-color: navy;'>Reference Number</li>";


    document.getElementById('divTenderType').innerHTML = li_;
    $('#divRefNumber').show();
    $('#divParent').hide();
    $('#txtRefNumber').focus();

    $('#hdnCreditCardRefStatus').val('1');
}

function txtRefNumberKeyPress() {

    var myTab = document.getElementById('tbl_tenderDyms');
    var m_ = ""; var lCheck_ = '0';
    for (i = 0; i < myTab.rows.length; i++) {
        var objCells = myTab.rows.item(i).cells;
        if (objCells.length > 0) {
            if ($('#hdnPayMode').val().toUpperCase() == objCells.item(1).innerHTML.toUpperCase()) {

                //$('#tr_' + $('#hdnPayMode').val().toUpperCase())[0].childNodes[2].innerText = parseFloat(ExchangeVal).toFixed(4);
                //if ($('#txtTendertrans').val() != "") {
                //    $('#tr_' + $('#hdnPayMode').val().toUpperCase())[0].childNodes[3].innerText = parseFloat($('#txtTendertrans').val()).toFixed(4);
                //    $('#tr_' + $('#hdnPayMode').val().toUpperCase())[0].childNodes[4].innerText = parseFloat(parseFloat($('#txtTendertrans').val()) * parseFloat(ExchangeVal)).toFixed(4);
                //    lCheck_ = '1';
                //}
                //else {
                $('#tr_' + $('#hdnPayMode').val().toUpperCase())[0].childNodes[5].innerText = $('#txtRefNumber').val();
                //    $('#tr_' + $('#hdnPayMode').val().toUpperCase())[0].childNodes[4].innerText = parseFloat(0).toFixed(4);
                //    lCheck_ = '1';
                //}
                break;
            }
        }

    }
    //alert($('#hdnPayModeType').val());
}

function FullPaymentCredit() {


    document.getElementById('divTenderType').childNodes[0].nodeValue = null;

    var li_ = "<li style='color: #fff;background-color: #49a3f1;border-color: #49a3f1;' onClick=GetCounterTypeTenderLocalData();><i class='fa fa-angle-up'></i> Back</li>";

    li_ = li_ + "<li    onclick=FullPaymentCredit(); style='color: #fff;background-color: navy;font-weight:bold; border-color: navy;'>Full Payment</li>";

    li_ = li_ + "<li  onclick=GetTypeTenderDataCreditbyLocal(); style='color: #fff;background-color: #2e6da4;border-color: #2e6da4;'>Partial Payment</li>";
    li_ = li_ + "<li  onclick=FullPaymentRefNum(); style='color: #fff;background-color: #2e6da4;border-color: #2e6da4;'>Reference Number</li>";


    document.getElementById('divTenderType').innerHTML = li_;


    if (parseFloat(($('#lbl_TpartTotal')[0].innerText)) < 0) {
        var ChangeAMT_ = parseFloat(parseFloat(($('#lbl_TTpartTotal')[0].innerText)) - parseFloat(($('#txtAmountpp').val()))).toFixed(4);

        $('#txtTendertrans').val(ChangeAMT_.replace('-', ''));
        changeCals();
        if ($('#hdnExchangerate').val() == '') {
            tenderAddpartial($('#hdnPayMode').val(), '1', '');
        }
        else {
            tenderAddpartial($('#hdnPayMode').val(), $('#hdnExchangerate').val(), '');
        }
        partialValBalnceCal();
        amtTenderCalc();
    }
    $('#divParent').show();
    $('#divRefNumber').hide();
}
function ConverTenderAmountSub(tenderType, exchangerate, tendeId, CurrencyId) {


    $('#hdnTemBasePricePlace').val(CurrencyId);
    $('#hdnExchangerate').val(exchangerate);
    document.getElementById('divTenderType').innerHTML = window.localStorage.getItem('liTenderType');
    var id_ = 'li_' + tenderType;
    document.getElementById(id_).style.cssText = 'color: #fff;background-color: navy;font-weight:bold; border-color: navy;';
    //'background-color: red; color: white; font-size: 44px';
    $('#hdnTransModeforPay').val('Cash');
    TenderRead('true', 'cash');
    var valTy = '(' + tenderType + ')' + ' : ';
    $('#lblTendertype').text(valTy);
    var convertedVal = parseFloat($('#txtAmountpp').val()) / parseFloat(exchangerate);

    if ($('#hdnTemBasePricePlace').val() != '') {
        $('#lblTenderConvertAmt').text(parseFloat(convertedVal).toFixed(4));

    }
    else
        $('#lblTenderConvertAmt').text((parseFloat(convertedVal)).toFixed(4));

    //$('#lblTenderConvertAmt').text((Math.round(convertedVal)).toFixed(2));
    //alert(tendeId);
    $('#hdn_TenderTypeId').val(tendeId);
    $("#txtTendertrans").focus();
    $('#hdnTenderSubType').val(tenderType);
    //tenderAddpartial('cash_' + tenderType, exchangerate, tendeId);
    //alert(exchangerate);

    POLEDisplay("Total :" + (Math.round(convertedVal)).toFixed(2));

    //$('#divTenderType li').each(function () {    
    for (var m = 1; m < $('#divTenderType li').length; m++) {

        var liId_ = "#" + $('#divTenderType li')[m].id;
        $(liId_)[0].onclick = null;
        //$(liId_).prop("disabled", "disabled");
    }
    $('#txtTendertrans').focus();
    //});

}

function ConverTenderAmount(tenderType, exchangerate, tendeId, CurrencyId) {
    $('#hdnTemBasePrice').val(tenderType);
    $('#hdnTemBasePriceEX').val(exchangerate);
    $('#hdnTemBasePriceTend').val(tendeId);
    $('#hdnTemBasePriceCurId').val(CurrencyId);


    $('#hdnTemBasePricePlace').val(CurrencyId);
    $('#hdnExchangerate').val(exchangerate);
    document.getElementById('divTenderType').innerHTML = window.localStorage.getItem('liTenderType');
    var id_ = 'li_' + tenderType;
    document.getElementById(id_).style.cssText = 'color: #fff;background-color: navy;font-weight:bold; border-color: navy;';
    //'background-color: red; color: white; font-size: 44px';
    $('#hdnTransModeforPay').val('Cash');
    TenderRead('true', 'cash');
    var valTy = '(' + tenderType + ')' + ' : ';
    $('#lblTendertype').text(valTy);
    var convertedVal = parseFloat($('#txtAmountpp').val()) / parseFloat(exchangerate);
    if ($('#hdnTemBasePricePlace').val() != '') {
        $('#lblTenderConvertAmt').text(parseFloat(parseFloat($('#txtAmountpp').val()).toFixed(4) / parseFloat(exchangerate)).toFixed(4));

    }
    else
        $('#lblTenderConvertAmt').text((parseFloat(convertedVal)).toFixed(4));
    //$('#lblTenderConvertAmt').text((Math.round(convertedVal)).toFixed(2));
    //alert(tendeId);
    $('#hdn_TenderTypeId').val(tendeId);
    $("#txtTendertrans").focus();
    $('#hdnTenderSubType').val(tenderType);
    tenderAddpartial('cash_' + tenderType, exchangerate, tendeId);
    //alert(exchangerate);
    partialValBalnceCal();
    amtTenderCalc();
    //POLEDisplay("Total :" + (Math.round(convertedVal)).toFixed(2));

    //$('#divTenderType li').each(function () {    
    for (var m = 1; m < $('#divTenderType li').length; m++) {

        var liId_ = "#" + $('#divTenderType li')[m].id;
        $(liId_)[0].onclick = null;
        //$(liId_).prop("disabled", "disabled");
    }
    $('#txtTendertrans').focus();
    //});

}
function GetTypeTenderDataCashby() {
    var SubOrgId = '';
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        SubOrgId = hashSplit[1];
    }
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetTypeTenderDataCashby,
        data: "CounterType=" + window.localStorage.getItem('COUNTERTYPE') + "&OrgId=" + SubOrgId + "&VD=" + VD1(),
        success: function (resp) {
            var ItemsData = resp;
            var li_ = "";
            if (ItemsData.length > 0) {
                //document.getElementById('divTenderType').childNodes[0].nodeValue = null;
                var MainData = '';
                li_ = li_ + "<li style='color: #fff;background-color: #49a3f1;border-color: #49a3f1;' onClick=GetCounterTypeTenderLocalData();><i class='fa fa-angle-up'></i> Back</li>";
                for (var i = 0; i < ItemsData.length; i++) {
                    var id_ = 'li_' + ItemsData[i].TENDERNAME;
                    li_ = li_ + "<li id=" + id_ + "  onclick=ConverTenderAmount(\'" + ItemsData[i].TENDERNAME.replace(' ', '') + "\',\'" + ItemsData[i].ExchangeRate + "\',\'" + ItemsData[i].BankID + "\',\'" + ItemsData[i].CurrencyID + "\') style='color: #fff;background-color: #2e6da4;border-color: #2e6da4;'>" + ItemsData[i].TENDERNAME + '(' + ItemsData[i].ExchangeRate + ')' + "</li>";
                }
                window.localStorage.setItem('BaseCurrencyCode', ItemsData[0].baseCurrency);
                window.localStorage.setItem('liTenderType', li_);
                //  document.getElementById('divTenderType').innerHTML = li_;
            }
            $('#txtItemScan').focus();
        },
        error: function (e) {

        }

    });

}
function partialValBalnceCal(ExchangeVal) {

    var myTab = document.getElementById('tbl_tenderDyms');
    var mVal_ = "0";
    // LOOP THROUGH EACH ROW OF THE TABLE AFTER HEADER.
    for (i = 0; i < myTab.rows.length; i++) {
        // GET THE CELLS COLLECTION OF THE CURRENT ROW.
        var objCells = myTab.rows.item(i).cells;
        if (objCells.length > 0) {
            mVal_ = parseFloat(mVal_) + parseFloat(objCells.item(4).innerHTML);
        }
    }
    var valGet_ = parseFloat(mVal_) - (Math.round($('#txtAmountpp').val()) / parseFloat(ExchangeVal).toFixed(4));
    // var valGet_ = parseFloat(mVal_) - $('#lbl_TpartTotal').innerText;
    //$('#lbl_TpartTotal')[0].innerText = parseFloat(mVal_).toFixed(4);

    $('#lbl_TpartTotal')[0].innerText = Math.round(valGet_).toFixed(2);
    $('#txtTendertransref').val(parseFloat(valGet_).toFixed(4));
    if ($('#hdnTemBasePricePlace').val() != '') {
        $('#lblTenderConvertAmt').text(parseFloat(valGet_).toFixed(4));

    }
    else
        $('#lblTenderConvertAmt').text((parseFloat(valGet_)).toFixed(4));

    //$('#lblTenderConvertAmt').text((Math.round(valGet_)).toFixed(2));
    //$('#hdnExchangerate').val('');
}
function amtTenderCalc() {



    if (($('#hdnExchangerate').val() == '') || $('#hdnExchangerate').val() == '1') {
        var exc_ = 1;
        var balance_ = parseFloat($('#lbl_TpartTotal')[0].innerText);
        //var amtTenderBalance_ = parseFloat($('#lblTenderConvertAmt')[0].innerText);
        //$('#lblTenderConvertAmt')[0].innerText = balance_ / exc_;

        if ($('#hdnTemBasePricePlace').val() != '') {
            $('#lblTenderConvertAmt').text(parseFloat(balance_ / exc_).toFixed(4));

        }
        else
            $('#lblTenderConvertAmt').text((parseFloat(balance_ / exc_)).toFixed(4));

        //$('#lblTenderConvertAmt')[0].innerText = Math.round(balance_ / exc_).toFixed(2);
    }
    else {
        var exc_ = parseFloat($('#hdnExchangerate').val());
        var balance_ = parseFloat($('#lbl_TTpartTotal')[0].innerText) - parseFloat(Math.round($('#txtAmountpp').val()).toFixed(2));
        //var amtTenderBalance_ = parseFloat($('#lblTenderConvertAmt')[0].innerText);


        if ($('#hdnTemBasePricePlace').val() != '') {
            $('#lblTenderConvertAmt').text(parseFloat(balance_ / exc_).toFixed(4));

        }
        else
            $('#lblTenderConvertAmt').text((parseFloat(balance_ / exc_)).toFixed(4));

        //$('#lblTenderConvertAmt')[0].innerText = Math.round(balance_ / exc_).toFixed(2);

    }
}
function partialValBalnceCalForCreditNote() {

    var myTab = document.getElementById('tbl_tenderDyms');
    var mVal_ = "0";
    // LOOP THROUGH EACH ROW OF THE TABLE AFTER HEADER.
    for (i = 0; i < myTab.rows.length; i++) {
        // GET THE CELLS COLLECTION OF THE CURRENT ROW.
        var objCells = myTab.rows.item(i).cells;
        if (objCells.length > 0) {
            mVal_ = parseFloat(mVal_) + parseFloat(objCells.item(4).innerHTML);

        }
    }
    var valGet_ = parseFloat(mVal_) - parseFloat($('#txtAmountpp').val());
    $('#lbl_TTpartTotal')[0].innerText = parseFloat(mVal_).toFixed(4);

    $('#lbl_TpartTotal')[0].innerText = parseFloat(valGet_).toFixed(4);
    $('#txtTendertransref').val(parseFloat(valGet_).toFixed(4));



    if ($('#hdnTemBasePricePlace').val() != '') {
        $('#lblTenderConvertAmt').text(parseFloat(valGet_).toFixed(4));

    }
    else
        $('#lblTenderConvertAmt').text((parseFloat(valGet_)).toFixed(4));

    //$('#lblTenderConvertAmt').text((Math.round(valGet_)).toFixed(2));

    var changeConverttoAmt_ = $('#lbl_TTpartTotal')[0].innerText;
    var txtAmountpp_ = $('#txtAmountpp').val();

    var changeConverttoEx_ = $('#hdnTemBasePriceEX').val();
    if (changeConverttoEx_ == "") {
        changeConverttoEx_ = '1';
    }

    if (changeConverttoEx_ == 1) {
        txtAmountpp_ = parseFloat(txtAmountpp_).toFixed(4);
        txtAmountpp_ = parseFloat(changeConverttoAmt_) - parseFloat(txtAmountpp_);
        //$('#txtTendertransref').val(Math.round(txtAmountpp_).toFixed(2));

        //if (parseFloat(changeConverttoAmt_) == 0) {
        $('#lblTenderConvertAmt')[0].innerText = parseFloat($('#lbl_TpartTotal')[0].innerText).toFixed(4);
        //}
    }
    else {
        txtAmountpp_ = parseFloat(txtAmountpp_).toFixed(4);
        txtAmountpp_ = parseFloat(changeConverttoAmt_) - parseFloat(txtAmountpp_);
        //$('#txtTendertransref').val(parseFloat(Math.round(txtAmountpp_).toFixed(2) / parseFloat(changeConverttoEx_)).toFixed(4));
        $('#txtTendertransref').val(parseFloat((parseFloat($('#lbl_TTpartTotal')[0].innerText) / parseFloat(changeConverttoEx_)) - (parseFloat($('#txtAmountpp').val()).toFixed(4) / parseFloat(changeConverttoEx_))).toFixed(4));

        var amtCheck_ = txtAmountpp_;
        $('#lblTenderConvertAmt')[0].innerText = parseFloat(parseFloat(amtCheck_).toFixed(4) / parseFloat(changeConverttoEx_)).toFixed(4);

    }



}
function partialValBalnceCal() {
    var myTab = document.getElementById('tbl_tenderDyms');
    var mVal_ = "0";
    // LOOP THROUGH EACH ROW OF THE TABLE AFTER HEADER.
    for (i = 0; i < myTab.rows.length; i++) {
        // GET THE CELLS COLLECTION OF THE CURRENT ROW.
        var objCells = myTab.rows.item(i).cells;
        if (objCells.length > 0) {
            mVal_ = parseFloat(mVal_) + parseFloat(objCells.item(4).innerHTML);
            mVal_ = parseFloat(mVal_).toFixed(2);
        }
    }
    var valGet_ = parseFloat(mVal_).toFixed(4) - parseFloat($('#txtAmountpp').val());
    $('#lbl_TTpartTotal')[0].innerText = parseFloat(mVal_).toFixed(4);

    $('#lbl_TpartTotal')[0].innerText = parseFloat(valGet_).toFixed(4);
    $('#txtTendertransref').val(parseFloat(valGet_).toFixed(4));



    if ($('#hdnTemBasePricePlace').val() != '') {
        $('#lblTenderConvertAmt').text(parseFloat(valGet_).toFixed(4));

    }
    else
        $('#lblTenderConvertAmt').text((parseFloat(valGet_)).toFixed(4));

    //$('#lblTenderConvertAmt').text((Math.round(valGet_)).toFixed(2));

    var changeConverttoAmt_ = $('#lbl_TTpartTotal')[0].innerText;
    var txtAmountpp_ = $('#txtAmountpp').val();

    var changeConverttoEx_ = $('#hdnTemBasePriceEX').val();
    if (changeConverttoEx_ == "") {
        changeConverttoEx_ = '1';
    }

    if (changeConverttoEx_ == 1) {
        txtAmountpp_ = parseFloat(txtAmountpp_).toFixed(4);
        txtAmountpp_ = parseFloat(changeConverttoAmt_) - parseFloat(txtAmountpp_);
        //$('#txtTendertransref').val(Math.round(txtAmountpp_).toFixed(2));

        //if (parseFloat(changeConverttoAmt_) == 0) {

        $('#lblTenderConvertAmt')[0].innerText = parseFloat($('#lbl_TpartTotal')[0].innerText).toFixed(4);
        //}
    }
    else {
        txtAmountpp_ = parseFloat(txtAmountpp_).toFixed(4);
        txtAmountpp_ = parseFloat(changeConverttoAmt_) - parseFloat(txtAmountpp_);
        //$('#txtTendertransref').val(parseFloat(Math.round(txtAmountpp_).toFixed(2) / parseFloat(changeConverttoEx_)).toFixed(4));
        $('#txtTendertransref').val(parseFloat((parseFloat($('#lbl_TTpartTotal')[0].innerText) / parseFloat(changeConverttoEx_)) - (parseFloat($('#txtAmountpp').val()).toFixed(4) / parseFloat(changeConverttoEx_))).toFixed(4));

        var amtCheck_ = txtAmountpp_;
        $('#lblTenderConvertAmt')[0].innerText = parseFloat(Math.round(amtCheck_).toFixed(2) / parseFloat(changeConverttoEx_)).toFixed(4);

    }
}
function tenderAddpartial(type, ExchangeVal, tenderId) {

    DuplicateTR(type, ExchangeVal, tenderId);
}
function DuplicateTR(type, ExchangeVal, tenderId) {

    var myTab = document.getElementById('tbl_tenderDyms');
    var m_ = ""; var lCheck_ = '0';
    // LOOP THROUGH EACH ROW OF THE TABLE AFTER HEADER.
    for (i = 0; i < myTab.rows.length; i++) {
        // GET THE CELLS COLLECTION OF THE CURRENT ROW.
        var objCells = myTab.rows.item(i).cells;
        if (objCells.length > 0) {
            // LOOP THROUGH EACH CELL OF THE CURENT ROW TO READ CELL VALUES.
            if (type.toUpperCase() == objCells.item(1).innerHTML.toUpperCase()) {
                //var markup = "<td  style='width:5% !important' ><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right' >" + type.toUpperCase() + "</td><td>" + parseFloat(amount).toFixed(2) + "</td>";
                //$('#tr_' + type).html(markup);
                $('#tr_' + type.toUpperCase())[0].childNodes[2].innerText = parseFloat(ExchangeVal).toFixed(4);
                if ($('#txtTendertrans').val() != "") {
                    $('#tr_' + type.toUpperCase())[0].childNodes[3].innerText = parseFloat($('#txtTendertrans').val()).toFixed(4);
                    $('#tr_' + type.toUpperCase())[0].childNodes[4].innerText = parseFloat(parseFloat($('#txtTendertrans').val()) * parseFloat(ExchangeVal)).toFixed(4);
                    lCheck_ = '1';
                }
                else {
                    //$('#tr_' + type.toUpperCase())[0].childNodes[3].innerText = parseFloat(0).toFixed(4);
                    //$('#tr_' + type.toUpperCase())[0].childNodes[4].innerText = parseFloat(0).toFixed(4);
                    lCheck_ = '1';
                }
                break;
            }
        }

    }
    //alert($('#hdnTemBasePrice').val());
    if (lCheck_ == '0') {
        var CURRENCYID = '';
        var HashValues = (window.localStorage.getItem("hashValues"));
        if (HashValues != null && HashValues != '') {
            var hashSplit = HashValues.split('~');
            CURRENCYID = hashSplit[16];
        }

        if ($('#hdnTemBasePricePlace').val() != '') {
            CURRENCYID = $('#hdnTemBasePricePlace').val();
        }
        var markup = "<tr id=" + 'tr_' + type.toUpperCase() + "><td style='width:5% !important'><img src='images/delete.png' onclick='deleteRowTr(this)'/></td><td align='left'>" + type.toUpperCase() + "</td><td style='display:none'  align='right'>" + parseFloat(ExchangeVal).toFixed(4) + "</td><td  align='right'  style='display:none'>0.0000</td><td  align='right'>0.0000</td><td style='display:none'>" + tenderId + "</td><td style='display:none'>" + CURRENCYID + "</td></tr>";
        $("#tbl_tenderDyms tbody").append(markup);

    }
}
function deleteRowTr(r) {

    var i = r.parentNode.parentNode.rowIndex;

    var id_ = r.parentNode.parentNode.id;

    var tempId = 'tr_CASH_' + $('#hdnTemBasePrice').val().toUpperCase();
    if (id_ == tempId) {
        $('#hdnTemBasePrice').val('');
        $('#hdnTemBasePriceEX').val('1');

        //GetTypeTenderDataCashbyLocal();

    }
    document.getElementById("tbl_tenderDyms").deleteRow(i);
    partialValBalnceCal();

    GetCounterTypeTenderLocalData();

}

function tenderRemovepartial(type) {

}

function fillShiftValues() {
    try {
        if (window.localStorage.getItem('Shift_Name') != null) {
            document.getElementById("lblShiftName").innerHTML = window.localStorage.getItem('Shift_Name');
            document.getElementById("lblCounterName").innerHTML = window.localStorage.getItem('COUNTERNAME');
            document.getElementById("lblStartTime").innerHTML = window.localStorage.getItem('Start_Time');
            document.getElementById("lblEndTime").innerHTML = window.localStorage.getItem('End_Time');
            $('#txtItemScan').focus();
        }
    }
    catch (e) {

    }
}
function deleteRow(r) {

    var i = r.parentNode.parentNode.rowIndex;
    //$("#itemCarttable").deleteRow(i);
    //document.getElementById("itemCarttable").deleteRow(i);
    var mId = r.parentNode.parentNode.childNodes[5].innerText;
    var mIdSubLength = r.parentNode.parentNode.childNodes[12].innerText;
    if (parseFloat(mIdSubLength) > 0) {
        for (var m1_ = 1; m1_ < mIdSubLength + 1; m1_++) {
            var idC_ = '#tr_' + mId + "_" + (m1_ - 1);
            $(idC_).remove();
        }
    }
    var idC_ = '#tr_' + mId;
    $(idC_).remove();
    $('#txthdnItemId').val('');
    $('#txtUOM').val('');
    $('#txtQuantity').val('');
    $('#txtPrice').val('');
    $('#txtAmount').val('');
    $('#txtQuantity').val('');
    $("#txtDiscount").val('');
    //$('#txtQuantity').val('0.000');
    //$('#txtQuantity').val('0.000');
    $("#txtAmount").attr("disabled", "disabled");
    $("#txtQuantity").attr("disabled", "disabled");
    $("#txtPrice").attr("disabled", "disabled");
    $("#txtUOM").attr("disabled", "disabled");
    $("#txtDiscount").attr("disabled", "disabled");
    GradCaluclation();
}

function ClearFileds() {
    $("#txtUOM").val("");
    $("#txthdnActiveColumn").val("0");
    $('#txthdnItemId').val('');
    $('#txtQuantity').val('');
    $('#txtPrice').val('');
    $('#txtAmount').val('');
    $('#txtDiscount').val('');
    //$('#txtQuantity').val('');

    $("#txtAmount").attr("disabled", "disabled");
    $("#txtQuantity").attr("disabled", "disabled");
    $("#txtPrice").attr("disabled", "disabled");
    $("#txtUOM").attr("disabled", "disabled");
    $("#txtDiscount").attr("disabled", "disabled");

    document.getElementById('liQtyM').style.cssText = 'color:white;background: #45bbe0;';
    document.getElementById('liDisM').style.cssText = 'color:white;background: #8892d6;';
    document.getElementById('liPriceM').style.cssText = 'color:white;background:#348cd4;';
    var myTab = document.getElementById('itemCarttable');
    for (i = 0; i < myTab.rows.length; i++) {
        var objCells = myTab.rows.item(i).cells;
        var v_ = 'tr_' + objCells.item(5).innerHTML;
        try {
            document.getElementById(v_).style.cssText = 'font-weight:normal';
        }
        catch (r) {
        }
    }
}

function tets() {
    var Data = "<!doctype html> <html> <head> <meta charset='utf-8'> <title>A simple, clean, and responsive HTML invoice template</title> <style> .invoice-box { max-width: 800px; margin: auto; padding: 30px; border: 1px solid #eee; box-shadow: 0 0 10px rgba(0, 0, 0, .15); font-size: 16px; line-height: 24px; font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; color: #555; } .invoice-box table { width: 100%; line-height: inherit; text-align: left; } .invoice-box table td { padding: 5px; vertical-align: top; } .invoice-box table tr td:nth-child(2) { text-align: right; } .invoice-box table tr.top table td { padding-bottom: 20px; } .invoice-box table tr.top table td.title { font-size: 45px; line-height: 45px; color: #333; } .invoice-box table tr.information table td { padding-bottom: 40px; } .invoice-box table tr.heading td { background: #eee; border-bottom: 1px solid #ddd; font-weight: bold; } .invoice-box table tr.details td { padding-bottom: 20px; } .invoice-box table tr.item td{ border-bottom: 1px solid #eee; } .invoice-box table tr.item.last td { border-bottom: none; } .invoice-box table tr.total td:nth-child(2) { border-top: 2px solid #eee; font-weight: bold; } @media only screen and (max-width: 600px) { .invoice-box table tr.top table td { width: 100%; display: block; text-align: center; } .invoice-box table tr.information table td { width: 100%; display: block; text-align: center; } } /** RTL **/ .rtl { direction: rtl; font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; } .rtl table { text-align: right; } .rtl table tr td:nth-child(2) { text-align: left; } </style> </head> <body> <div class='invoice-box'> <table cellpadding='0' cellspacing='0'> <tr class='top'> <td colspan='2'> <table> <tr> <td class='title'> <img src='images/mmlogo-inner1.png' style='width:100%; max-width:300px;'> </td> <td> Invoice #: 123<br> Created: January 1, 2015<br> Due: February 1, 2015 </td> </tr> </table> </td> </tr> <tr class='information'> <td colspan='2'> <table> <tr> <td> Sparksuite, Inc.<br> 12345 Sunny Road<br> Sunnyville, CA 12345 </td> <td> Acme Corp.<br> John Doe<br> john@example.com </td> </tr> </table> </td> </tr> <tr class='heading'> <td> Payment Method </td> <td> Check # </td> </tr> <tr class='details'> <td> Check </td> <td> 1000 </td> </tr> <tr class='heading'> <td> Item </td> <td> Price </td> </tr> <tr class='item'> <td> Website design </td> <td> $300.00 </td> </tr> <tr class='item'> <td> Hosting (3 months) </td> <td> $75.00 </td> </tr> <tr class='item last'> <td> Domain name (1 year) </td> <td> $10.00 </td> </tr> <tr class='total'> <td></td> <td> Total: $385.00 </td> </tr> </table> </div> </body> </html>"
    var PrintParm = new Object();
    PrintParm.Data = JSON.stringify(Data);

    var ComplexObject = new Object();
    ComplexObject.Data = PrintParm;
    var FinalData = JSON.stringify(ComplexObject);
    $.ajax({
        type: 'POST',
        //url: CashierLite.Settings.DirectPrint,
        url: HashValues[24] + '/DirectPrint',

        dataType: 'jsonp',
        contentType: "application/json; charset=utf-8",
        success: function () {
            return false;
        },
        error: function (e) {
        }
    });
}

function POLEDisplay(content) {


}
function onSubDataQTY(code, ItemId, Name, Price, Disc, UomID, UomCode, mapCode, Profile, qty_, img) {

    var passQTY = 1; var passPrice = 0; var baseCur_;
    var myTabChkQty = document.getElementById('itemCarttable');
    for (i = 0; i < myTabChkQty.rows.length; i++) {
        var objCells = myTabChkQty.rows.item(i).cells;
        if (ItemId == objCells.item(5).innerHTML) {
            passQTY = passQTY + parseFloat(objCells.item(2).innerHTML);
            passPrice = parseFloat(passPrice) + parseFloat(objCells.item(9).innerHTML);
        }
    }
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        SubOrg_Id = hashSplit[1];
        baseCur_ = hashSplit[16];
    }

    var SubOrg = {
    };
    SubOrg.SubOrgID = $('#hdnBranchId').val();
    //  SubOrg.VD = vd_;
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetItemSchema,
        data: "ItemId=" + ItemId + "&SubOrg=" + SubOrg_Id + "&QTY=" + qty_ + "&PRICE=" + passPrice + "&BaseCur=" + baseCur_ + "&VD=" + VD1(),
        success: function (resp) {

            var mainData_ = resp;
            resp = resp[0].split('~');
            if (resp[0] != 'NODATA') {

                if (resp[0] == 'ADDEXTRAITEM') {
                    var strOptions1 = $('#txtCusName').val();
                    var strOptions = $('#hdn_CusId').val();
                    if ($('#txthdnRemoveStatus').val() == '1') {
                        removeDraft(strOptions);
                        $('#txthdnRemoveStatus').val('0');
                    }
                    if (strOptions == '') {
                        strOptions = GenarateGustId() + '-01';
                        strOptions1 = GenarateGustName();
                    }

                    var valItemCheck_ = $('#itemIdhiddenCheck').val();
                    DuplicateQTYMODE(ItemId, Price, Disc, mapCode, Profile, mainData_, qty_);
                    valItemCheck_ = $('#itemIdhiddenCheck').val();
                    if (valItemCheck_ == '0') {
                        var markup = "<tr id=" + 'tr_' + ItemId + " onclick=ActiveRowget(\'" + ItemId + "\',\'" + Price + "\','1');><td style='width:5% !important'><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right'>" + Name + "</td><td>1.00</td><td>" + parseFloat(Disc).toFixed(2) + "</td><td style='display:none' >" + parseFloat(Price).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td><input type='text' class='form-control'  value='" + Price + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + UomID + "</td><td style='display:none'>" + UomCode + "</td><td>" + floorFigure(parseFloat(Price), 2) + "</td><td style='display:none'>" + mapCode + "</td><td style='display:none'>" + Profile + "</td><td  style='display:none'>" + mainData_.length + "</td></tr>";
                        $("#itemCarttable tbody").append(markup);
                        if (mainData_.length > 0) {
                            for (var k_ = 0; k_ < mainData_.length; k_++) {
                                var resp_ = mainData_[k_].split('~');
                                var id_ = 'tr_' + ItemId + "_" + k_;
                                markup = "<tr  id=" + id_ + " onclick=ActiveRowget(\'" + ItemId + "\',\'" + Price + "\','1');><td  style='width:5% !important;background:#ddd;color:black;'></td><td style='background:#ddd;color:black;' align='right' > " + resp_[2] + "</td><td style='background:#ddd;color:black;' >" + parseFloat(resp_[3]).toFixed(2) + "</td><td style='background:#ddd;color:black;' >" + parseFloat(resp_[10]).toFixed(2) + "</td><td style='display:none;background:#ddd;color:black;'>" + resp_[11] + "</td><td style='display:none;background:#ddd;color:black;'>" + resp_[1] + "~" + "</td><td style='background:#ddd;color:black;' ><input type='text' class='form-control'  value='" + resp_[4] + "' id='" + 'td_' + resp_[1] + "' style='display:none;background:#ddd;color:black;'></td><td  style='display:none;background:#ddd;color:black;'>" + resp_[7] + "</td><td  style='display:none;background:#ddd;color:black;'>" + resp_[5] + "</td><td style='background:#ddd;color:black;' >" + parseFloat(resp_[4]).toFixed(2) + "</td><td style='background:#ddd;color:black;display:none;' >" + mapCode + "</td><td  style='background:#ddd;color:black;'></td><td  style='display:none'>" + resp_[3] + "</td></tr>"

                                //<td style='display:none'></td><td  style='display:none'>" + resp_[3] + "</td></tr>";
                                //markup = "<tr  id=" + id_ + "><td  style='width:5% !important;background:#ddd;color:black;'></td><td align='right' style='background:#ddd;color:black'> " + resp_[2] + "</td><td style='background:#ddd;color:black' >" + parseFloat(resp_[3]).toFixed(2) + "</td><td style='background:#ddd;color:black' >" + parseFloat(resp_[10]).toFixed(2) + "</td><td style='display:none;background:#ddd;color:black'>" + resp_[4] + "</td><td style='display:none;background:#ddd;color:black'>" + resp_[1] + "</td><td style='background:#ddd;color:black' ><input type='text' class='form-control'  value='" + resp_[4] + "' id='" + 'td_' + resp_[1] + "' style='display:none'></td><td  style='display:none;background:#ddd;color:black'>" + resp_[7] + "</td><td  style='display:none;background:#ddd;color:black'>" + resp_[5] + "</td><td style='background:#ddd;color:black' >" + parseFloat(resp_[4]).toFixed(2) + "</td><td style='display:none'></td><td style='display:none'></td><td  style='display:none'>" + resp_[3] + "</td></tr>";
                                //var markup_ = "<tr  id=" + id_ + "><td style='width:5% !important'></td> <td align='right'>" + resp_[2] + "</td><td>" + resp_[3] + "</td><td>" + parseFloat(resp_[10]).toFixed(2) + "</td><td style='display:none'>" + parseFloat(resp_[4]).toFixed(2) + "</td><td style='display:none'>" + resp_[1] + "</td><td ><input type='text' class='form-control'  value='" + resp_[4] + "' id='" + 'td_' + resp_[1] + "' style='display:none;'></td><td style='display:none'>" + resp_[7] + "</td><td style='display:none'>" + resp_[5] + "</td><td >" + floorFigure(parseFloat(resp_[4]), 2) + "</td><td style='display:none'> </td><td style='display:none'> </td></tr>";
                                //$("#itemCarttable tbody").append(markup_);
                                $("#itemCarttable tbody").append(markup);
                            }
                        }

                        POLEDisplay(Name + ':' + Price);
                    }


                    GradCaluclation();
                    ActiveRowget(ItemId)
                    $('#txtItemScan').val('');
                    SubQtySubChange();
                    $('#txtQuantity').val(qty_);
                }
            }
            else {

                var strOptions1 = $('#txtCusName').val();
                var strOptions = $('#hdn_CusId').val();
                if ($('#txthdnRemoveStatus').val() == '1') {
                    removeDraft(strOptions);
                    $('#txthdnRemoveStatus').val('0');
                }
                if (strOptions == '') {
                    strOptions = GenarateGustId() + '-01';
                    strOptions1 = GenarateGustName();
                }

                var valItemCheck_ = $('#itemIdhiddenCheck').val();
                var _mainDiscChek = '';
                //Duplicate(ItemId, Price, Disc, mapCode, Profile, mainData_, qty_);
                if (mainData_.length > 0) {
                    var resp_ = mainData_[0].split('~');
                    if (resp_.length >= 2) {
                        var Mresp_ = resp_[1].split('$');
                        if (Mresp_.length > 1) {
                            Disc = Mresp_[0];
                            _mainDiscChek = 'DISC';
                        }
                    }
                }
                DuplicateQTYMODE(ItemId, Price, Disc, mapCode, Profile, _mainDiscChek, qty_, img);
                valItemCheck_ = $('#itemIdhiddenCheck').val();
                if (valItemCheck_ == '0') {
                    var imgHost_ = img;
                    if ((img == "") || img == undefined) {
                        imgHost_ = "";
                    }
                    var markup = "<tr id=" + 'tr_' + ItemId + " onclick=ActiveRowget(\'" + ItemId + "\',\'" + Price + "\','1');><td style='width:5% !important'><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right'>" + Name + "</td><td>1.00</td><td>" + parseFloat(Disc).toFixed(2) + "</td><td style='display:none' >" + parseFloat(Price).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td><input type='text' class='form-control'  value='" + Price + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + UomID + "</td><td style='display:none'>" + UomCode + "</td><td>" + floorFigure(parseFloat(Price), 2) + "</td><td style='display:none'>" + mapCode + "</td><td style='display:none'>" + Profile + "</td><td style='display:none'>0</td><td>" + imgHost_ + "</td></tr>";
                    $("#itemCarttable tbody").append(markup);
                    POLEDisplay(Name + ':' + Price);
                }


                GradCaluclation();

                ActiveRowget(ItemId)
                $('#txtItemScan').val('');
                SubQtySubChange();
                $('#txtQuantity').val(qty_);
            }

        },
        error: function (e) {

        }
    });



}
function onSubData(code, ItemId, Name, Price, Disc, UomID, UomCode, mapCode, Profile, img) {
    $("#tbl_tenderDyms tr").remove();
    $('#lbl_TTpartTotal')[0].innerText = "0.00";
    window.localStorage.setItem("Display", "2");
    var passQTY = 1; var passPrice = Price; var baseCur_;
    var myTabChkQty = document.getElementById('itemCarttable');
    for (i = 0; i < myTabChkQty.rows.length; i++) {
        var objCells = myTabChkQty.rows.item(i).cells;
        if (ItemId == objCells.item(5).innerHTML) {
            passQTY = passQTY + parseFloat(objCells.item(2).innerHTML);
            passPrice = parseFloat(passPrice) + parseFloat(objCells.item(9).innerHTML);
        }
    }

    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        SubOrg_Id = hashSplit[1];
        baseCur_ = hashSplit[16];
    }

    var SubOrg = {
    };

    SubOrg.SubOrgID = $('#hdnBranchId').val();
    //  SubOrg.VD = vd_;
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetItemSchema,
        data: "ItemId=" + ItemId + "&SubOrg=" + SubOrg_Id + "&QTY=" + passQTY + "&PRICE=" + passPrice + "&BaseCur=" + baseCur_ + "&VD=" + VD1(),
        success: function (resp) {

            var mainData_ = resp;
            resp = resp[0].split('~');

            if (resp[0] != 'NODATA') {

                if (resp[0] == 'ADDEXTRAITEM') {
                    var strOptions1 = $('#txtCusName').val();
                    var strOptions = $('#hdn_CusId').val();
                    if ($('#txthdnRemoveStatus').val() == '1') {
                        removeDraft(strOptions);
                        $('#txthdnRemoveStatus').val('0');
                    }
                    if (strOptions == '') {
                        strOptions = GenarateGustId() + '-01';
                        strOptions1 = GenarateGustName();
                    }

                    var valItemCheck_ = $('#itemIdhiddenCheck').val();

                    Duplicate(ItemId, Price, Disc, mapCode, Profile, mainData_, '1', img);
                    valItemCheck_ = $('#itemIdhiddenCheck').val();
                    if (valItemCheck_ == '0') {
                        var discPrice_ = Price;
                        if (mainData_.length > 0) {
                            var resp_ = mainData_[0].split('~');
                            if (resp_.length > 10) {
                                var Mresp_ = resp_[12].split('$');
                                if (Mresp_.length > 1) {
                                    Disc = Mresp_[0];
                                    discPrice_ = Mresp_[3];
                                }
                            }
                        }
                        var markup = "<tr id=" + 'tr_' + ItemId + " onclick=ActiveRowget(\'" + ItemId + "\',\'" + Price + "\','1');><td style='width:5% !important'><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right'>" + Name + "</td><td>1.00</td><td>" + parseFloat(Disc).toFixed(2) + "</td><td style='display:none' >" + parseFloat(discPrice_).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td><input type='text' class='form-control'  value='" + Price + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + UomID + "</td><td style='display:none'>" + UomCode + "</td><td>" + parseFloat(Price).toFixed(2) + "</td><td style='display:none'>" + mapCode + "</td><td style='display:none'>" + Profile + "</td><td  style='display:none'>" + mainData_.length + "</td></tr>";
                        $("#itemCarttable tbody").append(markup);
                        if (mainData_.length > 0) {
                            for (var k_ = 0; k_ < mainData_.length; k_++) {
                                var resp_ = mainData_[k_].split('~');
                                var id_ = 'tr_' + ItemId + "_" + k_;
                                markup = "<tr  id=" + id_ + " onclick=ActiveRowget(\'" + ItemId + "\',\'" + Price + "\','1');><td  style='width:5% !important;background:#ddd;color:black;'></td><td style='background:#ddd;color:black;' align='right' > " + resp_[2] + "</td><td style='background:#ddd;color:black;' >" + parseFloat(resp_[3]).toFixed(2) + "</td><td style='background:#ddd;color:black;' >" + parseFloat(resp_[10]).toFixed(2) + "</td><td style='display:none;background:#ddd;color:black;'>" + resp_[11] + "</td><td style='display:none;background:#ddd;color:black;'>" + resp_[1] + "~" + "</td><td style='background:#ddd;color:black;' ><input type='text' class='form-control'  value='" + resp_[4] + "' id='" + 'td_' + resp_[1] + "' style='display:none;background:#ddd;color:black;'></td><td  style='display:none;background:#ddd;color:black;'>" + resp_[7] + "</td><td  style='display:none;background:#ddd;color:black;'>" + resp_[5] + "</td><td style='background:#ddd;color:black;' >" + parseFloat(resp_[4]).toFixed(2) + "</td><td style='display:none;background:#ddd;color:black;' >" + resp_[8] + "</td><td  style='background:#ddd;color:black;display:none'>" + resp[9] + "</td><td  style='display:none'>" + resp_[3] + "</td></tr>"

                                //<td style='display:none'></td><td  style='display:none'>" + resp_[3] + "</td></tr>";
                                //markup = "<tr  id=" + id_ + "><td  style='width:5% !important;background:#ddd;color:black;'></td><td align='right' style='background:#ddd;color:black'> " + resp_[2] + "</td><td style='background:#ddd;color:black' >" + parseFloat(resp_[3]).toFixed(2) + "</td><td style='background:#ddd;color:black' >" + parseFloat(resp_[10]).toFixed(2) + "</td><td style='display:none;background:#ddd;color:black'>" + resp_[4] + "</td><td style='display:none;background:#ddd;color:black'>" + resp_[1] + "</td><td style='background:#ddd;color:black' ><input type='text' class='form-control'  value='" + resp_[4] + "' id='" + 'td_' + resp_[1] + "' style='display:none'></td><td  style='display:none;background:#ddd;color:black'>" + resp_[7] + "</td><td  style='display:none;background:#ddd;color:black'>" + resp_[5] + "</td><td style='background:#ddd;color:black' >" + parseFloat(resp_[4]).toFixed(2) + "</td><td style='display:none'></td><td style='display:none'></td><td  style='display:none'>" + resp_[3] + "</td></tr>";
                                //var markup_ = "<tr  id=" + id_ + "><td style='width:5% !important'></td> <td align='right'>" + resp_[2] + "</td><td>" + resp_[3] + "</td><td>" + parseFloat(resp_[10]).toFixed(2) + "</td><td style='display:none'>" + parseFloat(resp_[4]).toFixed(2) + "</td><td style='display:none'>" + resp_[1] + "</td><td ><input type='text' class='form-control'  value='" + resp_[4] + "' id='" + 'td_' + resp_[1] + "' style='display:none;'></td><td style='display:none'>" + resp_[7] + "</td><td style='display:none'>" + resp_[5] + "</td><td >" + floorFigure(parseFloat(resp_[4]), 2) + "</td><td style='display:none'> </td><td style='display:none'> </td></tr>";
                                //$("#itemCarttable tbody").append(markup_);
                                $("#itemCarttable tbody").append(markup);



                            }
                        }

                        POLEDisplay(Name + ':' + Price);
                    }

                    $('#itemIdhiddenCheck').val('0');
                    GradCaluclation();
                    ClearFileds();
                    ActiveRowget(ItemId)
                    $('#txtItemScan').val('');
                }
            }
            else {

                var strOptions1 = $('#txtCusName').val();
                var strOptions = $('#hdn_CusId').val();
                if ($('#txthdnRemoveStatus').val() == '1') {
                    removeDraft(strOptions);
                    $('#txthdnRemoveStatus').val('0');
                }
                if (strOptions == '') {
                    strOptions = GenarateGustId() + '-01';
                    strOptions1 = GenarateGustName();
                }

                var valItemCheck_ = $('#itemIdhiddenCheck').val();
                var dicPrice_ = Price;
                var _mainDataCheck = '';

                if (mainData_.length > 0) {
                    var resp_ = mainData_[0].split('~');
                    if (resp_.length >= 2) {
                        var Mresp_ = resp_[1].split('$');
                        if (Mresp_.length > 1) {
                            Disc = Mresp_[0];
                            dicPrice_ = Mresp_[3];
                            _mainDataCheck = 'DISC';
                        }
                    }
                }

                Duplicate(ItemId, Price, Disc, mapCode, Profile, _mainDataCheck, '1', img);
                valItemCheck_ = $('#itemIdhiddenCheck').val();
                if (valItemCheck_ == '0') {
                    var imgHost_ = '<img src=' + img + ' style="width:25px;height:25px">';
                    if ((img == "") || img == undefined) {
                        imgHost_ = "";
                    }
                    var markup = "<tr id=" + 'tr_' + ItemId + " onclick=ActiveRowget(\'" + ItemId + "\',\'" + Price + "\','1');><td style='width:5% !important'><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right'>" + Name + "</td><td>1.00</td><td>" + parseFloat(Disc).toFixed(2) + "</td><td style='display:none' >" + parseFloat(dicPrice_).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td><input type='text' class='form-control'  value='" + Price + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + UomID + "</td><td style='display:none'>" + UomCode + "</td><td>" + toNumberDecimalTwoPR(Price) + "</td><td style='display:none'>" + mapCode + "</td><td style='display:none'>" + Profile + "</td><td style='display:none'>0</td><td>" + imgHost_ + "</td></tr>";
                    $("#itemCarttable tbody").append(markup);
                    POLEDisplay(Name + ':' + Price);


                }

                $('#itemIdhiddenCheck').val('0');
                GradCaluclation();
                ClearFileds();
                ActiveRowget(ItemId)
                $('#txtItemScan').val('');
            }

        },
        error: function (e) {

        }
    });






}
function floorFigure(figure, decimals) {
    if (!decimals) decimals = 2;
    var d = Math.pow(10, decimals);
    return (parseInt(figure * d) / d).toFixed(decimals);
};

function f11() {
    $('#myModalpay').modal('show');
}
function valueAppendtoPOPUp() {


    $('#hdnTemBasePrice').val('');

    $('#hdnTemBasePriceEX').val('');
    $('#hdnTemBasePriceTend').val('');
    $('#hdnTemBasePriceCurId').val('');

    //$('#lbl_TTpartTotal')[0].innerText = '0';
    TenderRead('false', '');
    $('#txtTendertrans').val('');
    $('#txtTendertransref').val('');

    //$('#lblTransamodetype').text('Amount');
    //$('#lblTransamodetypeRef').text('Change');
    $('#lblTransamodetype').text($('#hdnAmount').val());
    $('#lblTransamodetypeRef').text($('#hdnChange').val());
    $('#hdnTransModeforPay').val('');
    GetCounterTypeTenderLocalData();
    var myTab = document.getElementById('itemCarttable');
    //var m_ = "0.000"; var qty_ = "0.000"; 
    var Price = "0.000"; var Items = "0.000"; var Quantity = "0.000"; var GrossAmt = "0.000"; var Quantity_ = "0.000"; var TaxTotal = "0.000"; var DiscAmount = "0.000";
    // LOOP THROUGH EACH ROW OF THE TABLE AFTER HEADER.
    $("#tabletrpopbind thead").remove();

    $("#tabletrpopbind tbody").remove();
    //var markupHeader = " <thead class='thead-inverse'><tr><th>#</th><th>Item</th><th>Qty</th><th>Disc(%)</th><th>Price</th><th>Amount</th><th style='display:none' >Tax</th><th  style='display:none' >Tax Amount</th><th>Total</th></tr><thead>  <tbody></tbody>";
    var markupHeader = " <thead class='thead-inverse'><tr><th>#</th><th>" + $('#hdnItem').val() + "</th><th>" + $('#hdnQty').val() + "</th><th>" + $('#hdnDisc').val() + "</th><th>" + $('#hdnPrice').val() + "</th><th>" + $('#hdnAmount').val() + "</th><th style='display:none' >Tax</th><th  style='display:none' >Tax Amount</th><th>" + $('#hdnTotal').val() + "</th></tr><thead><tbody></tbody>";
    $("#tabletrpopbind").append(markupHeader);
    var itemGuidbyM = '';
    var myTabPP = document.getElementById('itemCarttable');
    for (i = 0; i < myTab.rows.length; i++) {
        // GET THE CELLS COLLECTION OF THE CURRENT ROW.
        var objCells = myTab.rows.item(i).cells;
        // LOOP THROUGH EACH CELL OF THE CURENT ROW TO READ CELL VALUES. 
        Quantity = parseFloat(objCells.item(2).innerHTML);
        Quantity_ = parseFloat(Quantity_) + parseFloat(objCells.item(2).innerHTML);
        //Price = parseFloat(objCells.item(4).innerHTML) + parseFloat(Price) - parseFloat(objCells.item(3).innerHTML);
        //Price = parseFloat(Price).toFixed(3);

        var Baseprice = objCells.item(9).innerHTML;

        //var Baseprice = $('#td_' + objCells.item(5).innerHTML).val();
        //GrossAmt = parseFloat(GrossAmt) + parseFloat(Baseprice) * parseFloat(Quantity);

        var Dis1 = ((parseFloat(parseFloat(Baseprice) * parseFloat(Quantity)) * (parseFloat(objCells.item(3).innerHTML))) / 100);
        var Amt_ = parseFloat(Baseprice) * parseFloat(Quantity);
        if (parseFloat(Dis1) != 0) {

            Amt_ = parseFloat(parseFloat(Baseprice) * parseFloat(Quantity)) - parseFloat(Dis1);
        }


        //Price = parseFloat(objCells.item(4).innerHTML) + parseFloat(Price);
        //Price = parseFloat(Price).toFixed(3);
        var sno = parseFloat(i) + parseFloat(1);
        var taxSplit_ = objCells.item(10).innerText.split('&');
        var taxType_ = 'F';
        if (taxSplit_.length >= 1) {
            //Tax Caluclation    
            var profileData = objCells.item(11).innerText.split('@');
            if (profileData != '') {
                var CAMT_ = 0;
                for (var K = 0; K < profileData.length; K++) {
                    var cols = profileData[K].split('#');
                    if (cols != '') {
                        taxType_ = cols[16];
                        if (cols[4] == "+") {
                            CAMT_ = parseFloat((parseFloat(CAMT_) + parseFloat(cols[5])));
                        }
                        else
                            CAMT_ = parseFloat(parseFloat(CAMT_) - parseFloat(cols[5]));
                    }
                }
            }

        }

        var Total_ = 0; var AmountSub_ = 0; var taxTypeP = '';
        if (profileData != '') {
            if (taxType_ == "R") {

                var taxAmt = parseFloat(Amt_) * parseFloat(CAMT_);
                Total_ = toNumberDecimalTwo(Amt_) - toNumberDecimalTwo(taxAmt);
                //Total_ = parseFloat(parseFloat(Amt_) - parseFloat(taxAmt));
                CAMT_ = toNumberDecimalTwoPR(taxAmt);
                //CAMT_ = parseFloat(taxAmt);
                AmountSub_ = toNumberDecimalTwo(Total_);
                //AmountSub_ = parseFloat(Total_).toFixed(2);
                Total_ = toNumberDecimalTwo(Amt_);
                //Total_ = parseFloat(Amt_).toFixed(2);
                GrossAmt = parseFloat(toNumberDecimalTwo(GrossAmt)) + parseFloat(toNumberDecimalTwo(AmountSub_));
                //GrossAmt = parseFloat(GrossAmt) + parseFloat(AmountSub_);
                //Price = parseFloat(Price) + parseFloat(Total_);
                Price = toNumberDecimalTwo(Price) + toNumberDecimalTwo(Total_);
                taxTypeP = "R";
            }
            else {

                var taxAmt = parseFloat(Amt_) * (parseFloat(CAMT_));
                //Price = parseFloat(Amt_) + parseFloat(taxAmt);
                Total_ = toNumberDecimalTwo(Amt_) + toNumberDecimalTwo(taxAmt);
                CAMT_ = toNumberDecimalTwo(taxAmt);
                AmountSub_ = toNumberDecimalTwo(Amt_);
                GrossAmt = toNumberDecimalTwo(GrossAmt) + toNumberDecimalTwo(AmountSub_);
                //GrossAmt = parseFloat(GrossAmt) + parseFloat(AmountSub_);
                Price = toNumberDecimalTwo(Price) + toNumberDecimalTwo(Total_);
                taxTypeP = "F";


            }
        }
        else {
            CAMT_ = 0;
            Total_ = toNumberDecimalTwo(Amt_);
            AmountSub_ = toNumberDecimalTwo(Amt_);
            //GrossAmt = parseFloat(GrossAmt) + parseFloat(AmountSub_);
            GrossAmt = toNumberDecimalTwo(GrossAmt) + toNumberDecimalTwo(AmountSub_);
            Price = toNumberDecimalTwo(Price) + toNumberDecimalTwo(Total_);
            //Price = parseFloat(Price) + parseFloat(Total_);
        }
        //TaxTotal = parseFloat(TaxTotal) + parseFloat(CAMT_);
        TaxTotal = toNumberDecimalTwo(TaxTotal) + toNumberDecimalTwo(CAMT_);
        //DiscAmount = Number(floorFigure(DiscAmount, 4)) + Number(floorFigure(parseFloat(parseFloat(objCells.item(2).innerHTML) * parseFloat(objCells.item(9).innerHTML)) / parseFloat(objCells.item(3).innerHTML)), 4);


        DiscAmount = parseFloat(DiscAmount) + parseFloat(Dis1);
        //guid();
        var type = 'M';
        if (objCells[1].attributes["0"].value == 'right') {
            itemGuidbyM = guid();
            type = 'M';
        }
        else {
            type = 'S';
        }
        var ItemIdP_ = objCells.item(5).innerHTML;
        var QtyP_ = objCells.item(2).innerHTML;
        var DiscP_ = objCells.item(3).innerHTML;
        var AmountP_ = Total_;
        var UOMP_ = objCells.item(7).innerHTML;

        var markup = "<tr><td style='width:5% !important'>" + sno + "</td><td align='left'>" + objCells.item(1).innerHTML + "</td><td align='right'>" + objCells.item(2).innerHTML + "</td><td align='right'>" + objCells.item(3).innerHTML + "</td><td align='right'>" + Baseprice + "</td><td align='right'>" + AmountSub_ + "</td><td style='display:none' >" + taxSplit_[0] + "</td><td  style='display:none'>" + (CAMT_) + "</td><td>" + toNumberDecimalTwo(Total_) + "</td><td style='display:none'>" + objCells.item(10).innerText + "</td><td style='display:none'>" + objCells.item(11).innerText + "</td><td style='display:none'>" + ItemIdP_ + "</td><td style='display:none'>" + QtyP_ + "</td><td style='display:none'>" + DiscP_ + "</td><td style='display:none'>" + AmountP_ + "</td><td style='display:none'>" + UOMP_ + "</td><td style='display:none'>" + taxTypeP + "</td><td style='display:none'>" + itemGuidbyM + "</td><td style='display:none'>" + type + "</td><tr>";
        $("#tabletrpopbind tbody").append(markup);


    }
    //$('#txtQuantity').val(qty_);
    if (myTab.rows.length > 0) {
        //$('#myModalpay').modal('show');
    }
    else {
        var $textAndPic = $('<div></div>');
        $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        //$textAndPic.append('<div class="alert-message">Please Fill Cart</div>');
        $textAndPic.append('<div class="alert-message">' + $('#hdnMsgCart').val() + '</div>');
        BootstrapDialog.alert($textAndPic);
        return false;
    }
    // BootstrapDialog.alert('Please Fill Cart');
    $('#txtCurrency').val(window.localStorage.getItem('BaseCurrencyCode'));
    $('#txtItemspp').val(parseFloat(myTab.rows.length).toFixed(2));
    $('#hdnItemspp').val(parseFloat(myTab.rows.length).toFixed(4));
    $('#txtPricepp').val(parseFloat(GrossAmt).toFixed(2));
    $('#hdnPricepp').val(GrossAmt);
    $('#txtAmountpp').val(Math.round(Price).toFixed(2));
    $('#lbl_TpartTotal')[0].innerText = '-' + Math.round(Price).toFixed(2);
    //$("#tbl_tenderDyms tbody").remove();
    $('#txtItemSearchCart').val('');
    //new
    //$("#tbl_tenderDyms tr").remove();
    $('#hdnAmountpp').val((Price));
    $('#txtQuantityPP').val(parseFloat(Quantity_).toFixed(2));
    $('#hdnQuantityPP').val(parseFloat(Quantity_).toFixed(4));
    if (parseFloat(DiscAmount) > 0) {
        if (DiscAmount != 'Infinity') {
            $('#txtDiscountpp').val(parseFloat(DiscAmount).toFixed(2));
            $('#hdnDiscountpp').val(parseFloat(DiscAmount).toFixed(4));
        }
        else {
            $('#txtDiscountpp').val('0.00');
            $('#hdnDiscountpp').val('0.00');
        }
    } else {
        $('#txtDiscountpp').val('0.00');
        $('#hdnDiscountpp').val('0.00');
    }

    $('#txtTaxPP').val(parseFloat(TaxTotal).toFixed(2));
    $('#hdnTaxPP').val(Number(TaxTotal));

    var HashVal = (window.localStorage.getItem("hashValues"));
    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');
        var HeadeName_ = HashValues[17].split('/')
        $('#lblheader').text(HeadeName_[0]);
    }

    var CurDate = new Date();
    //var DateWithFormate = CurDate.getFullYear() + '-' + (CurDate.getMonth() + 1) + '-' + CurDate.getDate() + ' ' + CurDate.getHours() + ':' + CurDate.getSeconds();
    var min = CurDate.getMinutes().toString().length == 1 ? '0' + CurDate.getMinutes() : CurDate.getMinutes();
    var DateWithFormate = CurDate.getDate() + '-' + (CurDate.getMonth() + 1) + '-' + CurDate.getFullYear() + ' ' + CurDate.getHours() + ':' + min;
    $('#lbl_date').text(DateWithFormate);
    //$('#lbl_shift_Print').text('Shift : ' + window.localStorage.getItem('Shift_Name'));
    //$('#lbl_counter_Print').text('Counter : ' + window.localStorage.getItem('COUNTERNAME'));
    //$('#lbl_InvoiceNoPrint').text('invoice No. : ' + window.localStorage.getItem('COUNTERNAME'));
    //$('#lbl_ItemsPrint').text('Total Item : ' + parseFloat(myTab.rows.length).toFixed(2));
    //$('#lbl_Qty_Print').text('Total Qty : ' + Quantity_);
    //$('#lbl_Pay_Print').text('Pay in : Cash - INR');

    $('#lbl_shift_Print').text($('#hdnShift').val() + ' : ' + window.localStorage.getItem('Shift_Name'));
    $('#lbl_counter_Print').text($('#hdnCounter').val() + ' : ' + window.localStorage.getItem('COUNTERNAME'));
    $('#lbl_InvoiceNoPrint').text($('#hdnInvoice').val() + ' : ' + window.localStorage.getItem('COUNTERNAME'));
    $('#lbl_ItemsPrint').text($('#hdnTotItems').val() + ' : ' + parseFloat(myTab.rows.length).toFixed(2));
    $('#lbl_Qty_Print').text($('#hdnTotQty').val() + ' : ' + Quantity_);
    $('#lbl_Pay_Print').text($('#hdnPayIn').val() + ' : Cash - INR');




    //$('#lbl_totalGross').text('Base Total : ' + $('#txtPricepp').val());
    //$('#lbl_totalGross').text('Base Total : ' + (parseFloat($('#txtPricepp').val()) / parseFloat(v)).toFixed(2));
    //$('#lbl_Discount_Print').text('Discount : ' + (parseFloat($('#txtDiscountpp').val()) / parseFloat(v)).toFixed(2));
    //////$('#lbl_Round_Print').text('Net Total : ' + parseFloat($('#txtAmountpp').val()).toFixed(2));
    //$('#lbl_Round_Print').text('Net Total : ' + parseFloat(parseFloat($('#txtAmountpp').val()) / parseFloat(v)).toFixed(2));
    $('#txtItemScan').val('');
    POLEDisplay("Total : " + parseFloat($('#txtAmountpp').val()).toFixed(2));
    $('#chkHomeDelivery').prop('checked', false);
    return true;
}

function closeprintDialog() {
    //
    GetItemDetails();
    $('#myModalprint').css('display', "none");
}
function SearchItem() {
    GetItemDetails();
    $('#myItemSearch').modal('show');
}
function GradCaluclation() {

    var myTab_ = document.getElementById('itemCarttable');
    //var m_ = "0.000"; var qty_ = "0.000"; 
    var Price = "0.000"; var Items = "0.000"; var Quantity = "0.000"; var GrossAmt = "0.000";
    // LOOP THROUGH EACH ROW OF THE TABLE AFTER HEADER.
    for (i = 0; i < myTab_.rows.length; i++) {
        // GET THE CELLS COLLECTION OF THE CURRENT ROW.
        var objCells = myTab_.rows.item(i).cells;
        // LOOP THROUGH EACH CELL OF THE CURENT ROW TO READ CELL VALUES. 
        Quantity = parseFloat(objCells.item(2).innerHTML);
        //Price = parseFloat(objCells.item(4).innerHTML) + parseFloat(Price) - parseFloat(objCells.item(3).innerHTML);
        //Price = parseFloat(Price).toFixed(3);
        var Baseprice = $('#td_' + objCells.item(5).innerHTML).val();
        GrossAmt = parseFloat(GrossAmt) + parseFloat(Baseprice) * parseFloat(Quantity);
        Price = parseFloat(objCells.item(4).innerHTML) + parseFloat(Price);
        Price = parseFloat(Price).toFixed(3);
    }
    //$('#txtQuantity').val(qty_);
    $('#grdandTotal').text(parseFloat(Price).toFixed(2));
    $('#discGrdandTotal').text((parseFloat(GrossAmt) - parseFloat(Price)).toFixed(2));


    //$('#txtAmount').val(Price);
}

function Caluclation() {
    var myTab = document.getElementById('itemCarttable');
    var m_ = "0.000"; var qty_ = "0.000"; var Price = "0.000";
    // LOOP THROUGH EACH ROW OF THE TABLE AFTER HEADER.
    for (i = 0; i < myTab.rows.length; i++) {
        // GET THE CELLS COLLECTION OF THE CURRENT ROW.
        var objCells = myTab.rows.item(i).cells;
        // LOOP THROUGH EACH CELL OF THE CURENT ROW TO READ CELL VALUES. 
        //qty_ = parseFloat($('#td_' + objCells.item(4).innerHTML).val()) + parseFloat(qty_);
        qty_ = parseFloat(objCells.item(2).innerHTML) + parseFloat(qty_);
        Price = parseFloat(objCells.item(3).innerHTML) + parseFloat(Price);
    }

    $('#txtQuantity').val(parseFloat(qty_).toFixed(2));
    $('#txtPrice').val(parseFloat(Price).toFixed(2));
    $('#txtAmount').val(parseFloat(Price).toFixed(2));
}

function DuplicateQTYMODE(ItemId, Price_, Disc_, MapCode_, Profile_, mainData_, QTY_, img) {
    var myTab = document.getElementById('itemCarttable');
    var m_ = "";
    $('#itemIdhiddenCheck').val('0');
    // LOOP THROUGH EACH ROW OF THE TABLE AFTER HEADER.
    for (i = 0; i < myTab.rows.length; i++) {
        // GET THE CELLS COLLECTION OF THE CURRENT ROW.
        var objCells = myTab.rows.item(i).cells;
        // LOOP THROUGH EACH CELL OF THE CURENT ROW TO READ CELL VALUES.
        if (ItemId == objCells.item(5).innerHTML) {
            var qty_ = parseFloat(QTY_);
            var Price = '0';
            var Baseprice = floorFigure(parseFloat(objCells.item(9).innerHTML), 2);
            if (mainData_ == "DISC") {
                objCells.item(3).innerHTML = Disc_;
            }
            else {
                objCells.item(3).innerHTML = '0';

            }
            Price = parseFloat(Baseprice) * parseFloat(qty_);
            if (objCells.item(3).innerHTML != "") {
                if (parseFloat(objCells.item(3).innerHTML) > 0) {
                    Price = parseFloat(Price) - ((parseFloat(Price) * (parseFloat(objCells.item(3).innerHTML))) / 100);
                }
                else if (parseFloat(Disc_) > 0) {
                    Price = parseFloat(Price) - ((parseFloat(Price) * (parseFloat(Disc_))) / 100);
                }
            }

            var imgHost_ = img;
            if ((img == "") || img == undefined) {
                imgHost_ = "";
            }
            try {
                var markup = "<tr id=" + 'tr_' + ItemId + " onclick=ActiveRowget(\'" + ItemId + "\',\'" + Price_ + "\','1');><td  style='width:5% !important' ><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right' >" + objCells.item(1).innerHTML + "</td><td>" + parseFloat(qty_).toFixed(2) + "</td><td>" + parseFloat(objCells.item(3).innerHTML).toFixed(2) + "</td><td style='display:none' >" + parseFloat(Price).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td><input type='text' class='form-control'  value='" + Baseprice + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + objCells.item(7).innerHTML + "</td><td style='display:none'>" + objCells.item(8).innerHTML + "</td><td>" + Baseprice + "</td><td style='display:none'>" + MapCode_ + "</td><td style='display:none'>" + Profile_ + "</td><td style='display:none'>" + mainData_.length + "</td><td>" + imgHost_ + "</td></tr>";
            }
            catch (r) {
                var markup = "<tr id=" + 'tr_' + ItemId + " onclick=ActiveRowget(\'" + ItemId + "\',\'" + Price_ + "\','1');><td  style='width:5% !important' ><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right' >" + objCells.item(1).innerHTML + "</td><td>" + parseFloat(qty_).toFixed(2) + "</td><td>" + parseFloat(objCells.item(3).innerHTML).toFixed(2) + "</td><td style='display:none' >" + parseFloat(Price).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td><input type='text' class='form-control'  value='" + Baseprice + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + objCells.item(7).innerHTML + "</td><td style='display:none'>" + objCells.item(8).innerHTML + "</td><td>" + Baseprice + "</td><td style='display:none'>" + MapCode_ + "</td><td style='display:none'>" + Profile_ + "</td><td style='display:none'>" + objCells.item(12).innerHTML + "</td><td>" + imgHost_ + "</td></tr>";
            }
            //var markup = "<td style='width:5% !important'><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right'>" + Name + "</td>                          <td>1.00</td>                               <td>" + parseFloat(Disc).toFixed(2) + "</td>                     <td style='display:none' >" + parseFloat(Price).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td><input type='text' class='form-control'  value='" + Price + "' id='" + 'td_' + ItemId + "' style='display:none'></td>                      <td style='display:none'>" + UomID + "</td>                     <td style='display:none'>" + UomCode + "</td>                   <td>" + Price + "</td>";
            var mIdSubLength = objCells.item(12).innerHTML;
            if (parseFloat(mIdSubLength) >= 0) {
                for (var m1_ = 1; m1_ < mIdSubLength + 1; m1_++) {
                    var idC_ = '#tr_' + ItemId + "_" + (m1_ - 1);
                    $(idC_).remove();
                }
            }
            var idC_ = '#tr_' + ItemId;
            $(idC_).remove();


            $("#itemCarttable tbody").append(markup);
            try {
                if (mainData_ != "DISC") {
                    //if (parseFloat(objCells.item(12).innerHTML) > 0) {
                    if (mainData_.length > 0) {
                        for (var k_ = 0; k_ < mainData_.length; k_++) {
                            var resp_ = mainData_[k_].split('~');
                            var id_ = 'tr_' + ItemId + "_" + k_;
                            var imgHost_ = img;
                            if ((img == "") || img == undefined) {
                                imgHost_ = "";
                            }
                            markup = "<tr  id=" + id_ + " onclick=ActiveRowget(\'" + ItemId + "\',\'" + Price + "\','1');><td  style='width:5% !important;background:#ddd;color:black;'></td><td style='background:#ddd;color:black;' align='right' > " + resp_[2] + "</td><td style='background:#ddd;color:black;' >" + parseFloat(resp_[3]).toFixed(2) + "</td><td style='background:#ddd;color:black;' >" + parseFloat(resp_[10]).toFixed(2) + "</td><td style='display:none;background:#ddd;color:black;'>" + resp_[4] + "</td><td style='display:none;background:#ddd;color:black;'>" + resp_[1] + "~" + "</td><td style='background:#ddd;color:black;' ><input type='text' class='form-control'  value='" + resp_[4] + "' id='" + 'td_' + resp_[1] + "' style='display:none;background:#ddd;color:black;'></td><td  style='display:none;background:#ddd;color:black;'>" + resp_[7] + "</td><td  style='display:none;background:#ddd;color:black;'>" + resp_[5] + "</td><td style='background:#ddd;color:black;' >" + parseFloat(resp_[4]).toFixed(2) + "</td><td style='background:#ddd;color:black;' ></td><td  style='background:#ddd;color:black;'></td><td  style='display:none'>" + resp_[3] + "</td><td>" + imgHost_ + "</td></tr>"
                            $("#itemCarttable tbody").append(markup);
                        }
                    }
                }
                //}
            }
            catch (r) {

            }

            //duplicateValForScheemItem(ItemId, qty_, 0, objCells.item(12).innerHTML);
            $('#itemIdhiddenCheck').val('1');
            ClearFileds();
            POLEDisplay(objCells.item(1).innerHTML + ':' + Price);

            $('#txtQuantity').val(QTY_);
        }
    }
}

function Duplicate(ItemId, Price_, Disc_, MapCode_, Profile_, mainData_, QTY_, img) {

    var myTab = document.getElementById('itemCarttable');
    var m_ = "";
    $('#itemIdhiddenCheck').val('0');
    // LOOP THROUGH EACH ROW OF THE TABLE AFTER HEADER.
    for (i = 0; i < myTab.rows.length; i++) {
        // GET THE CELLS COLLECTION OF THE CURRENT ROW.
        var objCells = myTab.rows.item(i).cells;
        // LOOP THROUGH EACH CELL OF THE CURENT ROW TO READ CELL VALUES.
        if (ItemId == objCells.item(5).innerHTML) {
            var qty_ = parseFloat(objCells.item(2).innerHTML) + parseFloat(QTY_);
            var Price = '0';
            var Baseprice = floorFigure(parseFloat(objCells.item(9).innerHTML), 2);
            Price = parseFloat(Baseprice) * parseFloat(qty_);
            if (mainData_ == 'DISC') {
                objCells.item(3).innerHTML = Disc_;
            }
            else {
                objCells.item(3).innerHTML = '0';
            }
            if (objCells.item(3).innerHTML != "") {
                if (parseFloat(objCells.item(3).innerHTML) > 0) {
                    Price = parseFloat(Price) - ((parseFloat(Price) * (parseFloat(objCells.item(3).innerHTML))) / 100);
                }
                else if (parseFloat(Disc_) > 0) {
                    Price = parseFloat(Price) - ((parseFloat(Price) * (parseFloat(Disc_))) / 100);
                }

            }
            var imgHost_ = '<img src=' + img + ' style="width:25px;height:25px">';
            if ((img == "") || img == undefined) {
                imgHost_ = "";
            }
            try {
                if (mainData_ == 'DISC') {
                    var markup = "<tr id=" + 'tr_' + ItemId + " onclick=ActiveRowget(\'" + ItemId + "\',\'" + Price_ + "\','1');><td  style='width:5% !important' ><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right' >" + objCells.item(1).innerHTML + "</td><td>" + parseFloat(qty_).toFixed(2) + "</td><td>" + parseFloat(objCells.item(3).innerHTML).toFixed(2) + "</td><td style='display:none' >" + parseFloat(Price).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td><input type='text' class='form-control'  value='" + Baseprice + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + objCells.item(7).innerHTML + "</td><td style='display:none'>" + objCells.item(8).innerHTML + "</td><td>" + Baseprice + "</td><td style='display:none'>" + MapCode_ + "</td><td style='display:none'>" + Profile_ + "</td><td style='display:none'>" + objCells.item(12).innerHTML + "</td><td>" + imgHost_ + "</td></tr>";

                }
                else {
                    var markup = "<tr id=" + 'tr_' + ItemId + " onclick=ActiveRowget(\'" + ItemId + "\',\'" + Price_ + "\','1');><td  style='width:5% !important' ><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right' >" + objCells.item(1).innerHTML + "</td><td>" + parseFloat(qty_).toFixed(2) + "</td><td>" + parseFloat(objCells.item(3).innerHTML).toFixed(2) + "</td><td style='display:none' >" + parseFloat(Price).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td><input type='text' class='form-control'  value='" + Baseprice + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + objCells.item(7).innerHTML + "</td><td style='display:none'>" + objCells.item(8).innerHTML + "</td><td>" + Baseprice + "</td><td style='display:none'>" + MapCode_ + "</td><td style='display:none'>" + Profile_ + "</td><td style='display:none'>" + mainData_.length + "</td><td>" + imgHost_ + "</td></tr>";
                }
            }
            catch (r) {
                var markup = "<tr id=" + 'tr_' + ItemId + " onclick=ActiveRowget(\'" + ItemId + "\',\'" + Price_ + "\','1');><td  style='width:5% !important' ><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right' >" + objCells.item(1).innerHTML + "</td><td>" + parseFloat(qty_).toFixed(2) + "</td><td>" + parseFloat(objCells.item(3).innerHTML).toFixed(2) + "</td><td style='display:none' >" + parseFloat(Price).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td><input type='text' class='form-control'  value='" + Baseprice + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + objCells.item(7).innerHTML + "</td><td style='display:none'>" + objCells.item(8).innerHTML + "</td><td>" + Baseprice + "</td><td style='display:none'>" + MapCode_ + "</td><td style='display:none'>" + Profile_ + "</td><td style='display:none'>" + objCells.item(12).innerHTML + "</td><td>" + imgHost_ + "</td></tr>";
            }
            //var markup = "<td style='width:5% !important'><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right'>" + Name + "</td>                          <td>1.00</td>                               <td>" + parseFloat(Disc).toFixed(2) + "</td>                     <td style='display:none' >" + parseFloat(Price).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td><input type='text' class='form-control'  value='" + Price + "' id='" + 'td_' + ItemId + "' style='display:none'></td>                      <td style='display:none'>" + UomID + "</td>                     <td style='display:none'>" + UomCode + "</td>                   <td>" + Price + "</td>";
            var mIdSubLength = objCells.item(12).innerHTML;
            if (parseFloat(mIdSubLength) >= 0) {
                for (var m1_ = 1; m1_ < mIdSubLength + 1; m1_++) {
                    var idC_ = '#tr_' + ItemId + "_" + (m1_ - 1);
                    $(idC_).remove();
                }
            }
            var idC_ = '#tr_' + ItemId;
            $(idC_).remove();


            $("#itemCarttable tbody").append(markup);
            try {
                if (mainData_ != 'DISC') {
                    //if (parseFloat(objCells.item(12).innerHTML) > 0) {
                    if (mainData_.length > 0) {
                        for (var k_ = 0; k_ < mainData_.length; k_++) {
                            var resp_ = mainData_[k_].split('~');
                            var id_ = 'tr_' + ItemId + "_" + k_;
                            var imgHost_ = '<img src=' + img + ' style="width:25px;height:25px">';
                            if ((img == "") || img == undefined) {
                                imgHost_ = "";
                            }
                            markup = "<tr  id=" + id_ + " onclick=ActiveRowget(\'" + ItemId + "\',\'" + Price + "\','1');><td  style='width:5% !important;background:#ddd;color:black;'></td><td style='background:#ddd;color:black;' align='right' > " + resp_[2] + "</td><td style='background:#ddd;color:black;' >" + parseFloat(resp_[3]).toFixed(2) + "</td><td style='background:#ddd;color:black;' >" + parseFloat(resp_[10]).toFixed(2) + "</td><td style='display:none;background:#ddd;color:black;'>" + resp_[11] + "</td><td style='display:none;background:#ddd;color:black;'>" + resp_[1] + "~" + "</td><td style='background:#ddd;color:black;' ><input type='text' class='form-control'  value='" + resp_[4] + "' id='" + 'td_' + resp_[1] + "' style='display:none;background:#ddd;color:black;'></td><td  style='display:none;background:#ddd;color:black;'>" + resp_[7] + "</td><td  style='display:none;background:#ddd;color:black;'>" + resp_[5] + "</td><td style='background:#ddd;color:black;' >" + parseFloat(resp_[4]).toFixed(2) + "</td><td style='display:none;background:#ddd;color:black;' >" + resp_[8] + "</td><td  style='background:#ddd;color:black;display:none'>" + resp_[9] + "</td><td  style='display:none'>" + resp_[3] + "</td><td>" + imgHost_ + "</td></tr>"
                            $("#itemCarttable tbody").append(markup);
                        }
                    }
                }
                //}
            }
            catch (r) {

            }
            //duplicateValForScheemItem(ItemId, qty_, 0, objCells.item(12).innerHTML);
            $('#itemIdhiddenCheck').val('1');
            ClearFileds();
            POLEDisplay(objCells.item(1).innerHTML + ':' + Price);

        }
    }
}
function duplicateValForScheemItem(id, qty, price, scheemItemCount) {

    var myTab = document.getElementById('itemCarttable');
    var m_ = "";
    if (scheemItemCount > 0) {
        // LOOP THROUGH EACH ROW OF THE TABLE AFTER HEADER.
        for (var m = 1; m < parseFloat(scheemItemCount) + 1; m++) {
            for (i = 0; i < myTab.rows.length; i++) {
                // GET THE CELLS COLLECTION OF THE CURRENT ROW.
                var objCells = myTab.rows.item(i).cells;
                var ItemId = 'tr_' + id + "_" + (m - 1);
                var checkId = myTab.rows[i].attributes["0"].value;
                // LOOP THROUGH EACH CELL OF THE CURENT ROW TO READ CELL VALUES.
                if (ItemId == checkId) {
                    var qtyS_ = parseFloat(qty) * parseFloat(objCells.item(12).innerHTML);
                    var PriceS = price;
                    //var markup = "<td  style='width:5% !important' ></td><td align='right' >" + objCells.item(1).innerHTML + "</td><td>" + parseFloat(qty_).toFixed(2) + "</td><td>" + parseFloat(objCells.item(3).innerHTML).toFixed(2) + "</td><td style='display:none' >" + parseFloat(0).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td><input type='text' class='form-control'  value='0' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + objCells.item(7).innerHTML + "</td><td style='display:none'>" + objCells.item(8).innerHTML + "</td><td>0</td><td style='display:none'></td><td style='display:none'></td><td style='display:none'>" + objCells.item(12).innerHTML + "</td>";
                    //var markup = "<td  style='width:5% !important;background:#ddd;color:black;'></td><td align='right' style='background:#ddd;color:black'> " + objCells.item(1).innerHTML + "</td><td style='background:#ddd;color:black' >" + parseFloat(qty_).toFixed(2) + "</td><td style='background:#ddd;color:black' >" + objCells.item(3).innerHTML + "</td><td style='display:none;background:#ddd;color:black'>" + price + "</td><td style='display:none;background:#ddd;color:black'>" + objCells.item(5).innerHTML + "</td><td style='background:#ddd;color:black' ><input type='text' class='form-control'  value='" + objCells.item(9).innerHTML + "' id='" + 'td_' + objCells.item(5).innerHTML + "' style='display:none'></td><td  style='display:none;background:#ddd;color:black'>0</td><td  style='display:none;background:#ddd;color:black'>0</td><td style='background:#ddd;color:black' >0</td><td style='display:none'></td><td style='display:none'></td><td  style='display:none'>" + objCells.item(12).innerHTML + "</td>";
                    var markup = "<td  style='width:5% !important;background:#ddd;color:black;'></td><td align='right'  style='background:#ddd;color:black;'> " + objCells.item(1).innerHTML + "</td><td style='background:#ddd;color:black;' >" + parseFloat(qtyS_).toFixed(2) + "</td><td style='background:#ddd;color:black;' >" + objCells.item(3).innerHTML + "</td><td style='display:none;background:#ddd;color:black;' >" + objCells.item(4).innerHTML + "</td><td style='display:none;background:#ddd;color:black;'>" + objCells.item(5).innerHTML + "</td><td style='background:#ddd;color:black;' ></td><td style='display:none;background:#ddd;color:black;'>" + objCells.item(7).innerHTML + "</td><td style='display:none;background:#ddd;color:black;'>" + objCells.item(8).innerHTML + "</td><td style='background:#ddd;color:black;'>0.00</td><td style='background:#ddd;color:black;'></td><td style='background:#ddd;color:black;'></td><td style='display:none'>" + objCells.item(12).innerHTML + "</td>";
                    //markup = "<tr  id=" + id_ + "><td  style='width:5% !important;'></td><td align='right' style=''> " + resp_[2] + "</td><td style='' >" + parseFloat(resp_[3]).toFixed(2) + "</td><td style='' >" + parseFloat(resp_[10]).toFixed(2) + "</td><td style='display:none;'>" + resp_[4] + "</td><td style='display:none;'>" + resp_[1] + "</td><td style='' ></td><td  style='display:none;'>" + resp_[5] + "</td><td>" + parseFloat(resp_[4]).toFixed(2) + "</td></td><td  style='display:none'>" + resp_[3] + "</td></tr>"
                    //var markup = "<td  style='width:5% !important;'></td><td align='right' style=''> " + objCells.item(1).innerHTML + "</td><td style='' >" + parseFloat(qty_).toFixed(2) + "</td><td style='' >" + parseFloat(resp_[10]).toFixed(2) + "</td><td style='display:none;'>" + resp_[4] + "</td><td style='display:none;'>" + resp_[1] + "</td><td style='' ><input type='text' class='form-control'  value='" + resp_[4] + "' id='" + 'td_' + resp_[1] + "' style='display:none'></td><td  style='display:none;'>" + resp_[7] + "</td><td  style='display:none;'>" + resp_[5] + "</td><td>" + parseFloat(resp_[4]).toFixed(2) + "</td></td><td  style='display:none'>" + resp_[3] + "</td>"
                    $('#' + ItemId).html(markup);

                }
            }
        }
    }
}




function SubQtySubChange() {
    var myTab = document.getElementById('itemCarttable');
    var m_ = "";

    if ($('#txthdnItemId').val() != "") {
        document.getElementById('liQtyM').style.cssText = 'color: black;font-weight: bold;background:  #45bbe0;';
        document.getElementById('liDisM').style.cssText = 'color:white;background: #8892d6;';
        document.getElementById('liPriceM').style.cssText = 'color:white;background: #348cd4;';
        for (i = 0; i < myTab.rows.length; i++) {
            var objCells = myTab.rows.item(i).cells;
            if ($('#txthdnItemId').val() == objCells.item(5).innerHTML) {
                $("#txtAmount").attr("disabled", "disabled");
                $("#txthdnItemId").attr("disabled", "disabled");
                $("#txtQuantity").attr("disabled", "disabled");
                $("#txtPrice").attr("disabled", "disabled");
                $("#txtUOM").attr("disabled", "disabled");
                $("#txtDiscount").attr("disabled", "disabled");
                $("#txtQuantity").removeAttr("disabled");

                $("#txthdnActiveColumn").val("1");
                break;
            }
        }
    }
    else {
        var $textAndPic = $('<div></div>');
        $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        //$textAndPic.append('<div class="alert-message">Please Select Item</div>');
        $textAndPic.append('<div class="alert-message">' + $('#hdnPlzSelectItem').val() + '</div>');
        BootstrapDialog.alert($textAndPic);
    }
    $("#txtQuantity").focus();

}

function QtySubChange() {
    var myTab = document.getElementById('itemCarttable');
    var m_ = "";

    if ($('#txthdnItemId').val() != "") {
        document.getElementById('liQtyM').style.cssText = 'color: black;font-weight: bold;background:  #45bbe0;';
        document.getElementById('liDisM').style.cssText = 'color:white;background: #8892d6;';
        document.getElementById('liPriceM').style.cssText = 'color:white;background: #348cd4;';
        for (i = 0; i < myTab.rows.length; i++) {
            var objCells = myTab.rows.item(i).cells;
            if ($('#txthdnItemId').val() == objCells.item(5).innerHTML) {
                $("#txtAmount").attr("disabled", "disabled");
                $("#txthdnItemId").attr("disabled", "disabled");
                $("#txtQuantity").attr("disabled", "disabled");
                $("#txtPrice").attr("disabled", "disabled");
                $("#txtUOM").attr("disabled", "disabled");
                $("#txtDiscount").attr("disabled", "disabled");
                $("#txtQuantity").removeAttr("disabled");
                $("#txtQuantity").val('');

                $("#txthdnActiveColumn").val("1");
                break;
            }
        }
    }
    else {
        var $textAndPic = $('<div></div>');
        $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        //$textAndPic.append('<div class="alert-message">Please Select Item</div>');
        $textAndPic.append('<div class="alert-message">' + $('#hdnPlzSelectItem').val() + '</div>');
        BootstrapDialog.alert($textAndPic);
    }
    $("#txtQuantity").focus();

}
function PriceSubChange() {

    if ($("#hdnPwdCheckStatus").val() == '1') {
        var myTab = document.getElementById('itemCarttable');

        var m_ = "";
        if ($('#txthdnItemId').val() != "") {
            document.getElementById('liQtyM').style.cssText = 'color:white;background:  #45bbe0;';
            document.getElementById('liDisM').style.cssText = 'color:white;background: #8892d6;';
            document.getElementById('liPriceM').style.cssText = 'color:black;font-weight:bold;background: #348cd4;';
            for (i = 0; i < myTab.rows.length; i++) {
                var objCells = myTab.rows.item(i).cells;
                if ($('#txthdnItemId').val() == objCells.item(5).innerHTML) {
                    $("#txtAmount").attr("disabled", "disabled");
                    $("#txthdnItemId").attr("disabled", "disabled");
                    $("#txtQuantity").attr("disabled", "disabled");
                    $("#txtPrice").attr("disabled", "disabled");
                    $("#txtUOM").attr("disabled", "disabled");
                    $("#txtDiscount").attr("disabled", "disabled");
                    $("#txtPrice").val('');
                    $("#txtPrice").removeAttr("disabled");

                    $("#txthdnActiveColumn").val("3");
                    break;
                }
            }
        }
        else {
            var $textAndPic = $('<div></div>');
            $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
            //$textAndPic.append('<div class="alert-message">Please Select Item</div>');
            $textAndPic.append('<div class="alert-message">' + $('#hdnPlzSelectItem').val() + '</div>');
            BootstrapDialog.alert($textAndPic);
        }
        $("#txtPrice").focus();
    }
    else {
        $("#txtAUTPwd").focus();
        //$('#ModalPasswordPopup').modal('show');
        $('#ModalPasswordPopup').on('shown.bs.modal', function () {
            $('#txtAUTPwd').focus();
        });

        //$("#txtAUTPwd").focus();
        $('#ModalPasswordPopup').modal();
    }
}
function DiscSubChange() {


    if ($("#hdnPwdCheckStatus").val() == '1') {
        //Code For Discount Change Mode  * Important
        var myTab = document.getElementById('itemCarttable');
        var m_ = "";

        if ($('#txthdnItemId').val() != "") {
            document.getElementById('liQtyM').style.cssText = 'color:white;background: #45bbe0;';
            document.getElementById('liDisM').style.cssText = 'color:black;background: #8892d6;;font-weight:bold';
            document.getElementById('liPriceM').style.cssText = 'color:white;background: #348cd4;';
            for (i = 0; i < myTab.rows.length; i++) {
                var objCells = myTab.rows.item(i).cells;
                if ($('#txthdnItemId').val() == objCells.item(5).innerHTML) {
                    $("#txtAmount").attr("disabled", "disabled");
                    $("#txthdnItemId").attr("disabled", "disabled");
                    $("#txtQuantity").attr("disabled", "disabled");
                    $("#txtPrice").attr("disabled", "disabled");
                    $("#txtUOM").attr("disabled", "disabled");
                    $("#txtDiscount").attr("disabled", "disabled");
                    $("#txtDiscount").val('');
                    $("#txtDiscount").removeAttr("disabled");
                    $("#txthdnActiveColumn").val("2");
                    $("#txtDiscount").focus();
                    break;
                }
            }

        }
        else {
            var $textAndPic = $('<div></div>');
            $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
            //$textAndPic.append('<div class="alert-message">Please Select Item</div>');
            $textAndPic.append('<div class="alert-message">' + $('#hdnPlzSelectItem').val() + '</div>');
            BootstrapDialog.alert($textAndPic);
        }
    }
    else {
        $("#txtAUTPwd").focus();
        $('#ModalPasswordPopup').modal('show');
    }
}
function btnCheckOKClick() {
    var password = document.getElementById('txtAUTPwd').value;
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        SubOrg_Id = hashSplit[1];
    }

    Jsonobj = [];
    var SubOrg = {
    };
    SubOrg.SubOrgID = $('#hdnBranchId').val();
    //  SubOrg.VD = vd_;
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.VerifyPasswordAUTCheck,
        data: "password=" + password + "&VD=" + VD1(),
        success: function (resp) {
            if (resp == '100000') {
                $("#hdnPwdCheckStatus").val("1");
                $('#ModalPasswordPopup').modal('hide');
            }
            else {

                var $textAndPic = $('<div></div>');
                $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                //$textAndPic.append('<div class="alert-message">Invalid Authentication</div>');
                $textAndPic.append('<div class="alert-message">' + $('#hdnInvalAuth').val() + '</div>');
                BootstrapDialog.alert($textAndPic);

                $('#ModalPasswordPopup').modal('show');
            }

        },
        error: function (e) {

        }
    });

}
function oneSubChange() {
    if ($('#txthdnItemId').val() != "") {
        if ($("#txthdnActiveColumn").val() == "1") {
            var strng = document.getElementById("txtQuantity").value;
            document.getElementById("txtQuantity").value = strng + "1";
            UpdateQTYSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "2") {
            var strng = document.getElementById("txtDiscount").value;
            document.getElementById("txtDiscount").value = strng + "1";
            UpdateDiscSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "3") {
            var strng = document.getElementById("txtPrice").value;
            document.getElementById("txtPrice").value = strng + "1";
            UpdatePriceSubChange();
        }
    }
    else {
        var $textAndPic = $('<div></div>');
        $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        //$textAndPic.append('<div class="alert-message">Please Select Item</div>');
        $textAndPic.append('<div class="alert-message">' + $('#hdnPlzSelectItem').val() + '</div>');
        BootstrapDialog.alert($textAndPic);
    }
}
function TwoSubChange() {
    if ($('#txthdnItemId').val() != "") {
        if ($("#txthdnActiveColumn").val() == "1") {
            var strng = document.getElementById("txtQuantity").value;
            document.getElementById("txtQuantity").value = strng + "2";
            UpdateQTYSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "2") {
            var strng = document.getElementById("txtDiscount").value;
            document.getElementById("txtDiscount").value = strng + "2";
            UpdateDiscSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "3") {
            var strng = document.getElementById("txtPrice").value;
            document.getElementById("txtPrice").value = strng + "2";
            UpdatePriceSubChange();
        }
    }
    else {
        var $textAndPic = $('<div></div>');
        $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        //$textAndPic.append('<div class="alert-message">Please Select Item</div>');
        $textAndPic.append('<div class="alert-message">' + $('#hdnPlzSelectItem').val() + '</div>');
        BootstrapDialog.alert($textAndPic);
    }
}
function ThreeSubChange() {
    if ($('#txthdnItemId').val() != "") {
        if ($("#txthdnActiveColumn").val() == "1") {
            var strng = document.getElementById("txtQuantity").value;
            document.getElementById("txtQuantity").value = strng + "3";
            UpdateQTYSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "2") {
            var strng = document.getElementById("txtDiscount").value;
            document.getElementById("txtDiscount").value = strng + "3";
            UpdateDiscSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "3") {
            var strng = document.getElementById("txtPrice").value;
            document.getElementById("txtPrice").value = strng + "3";
            UpdatePriceSubChange();
        }
    }
    else {
        var $textAndPic = $('<div></div>');
        $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        //$textAndPic.append('<div class="alert-message">Please Select Item</div>');
        $textAndPic.append('<div class="alert-message">' + $('#hdnPlzSelectItem').val() + '</div>');
        BootstrapDialog.alert($textAndPic);
    }
}
function FourSubChange() {
    if ($('#txthdnItemId').val() != "") {
        if ($("#txthdnActiveColumn").val() == "1") {
            var strng = document.getElementById("txtQuantity").value;
            document.getElementById("txtQuantity").value = strng + "4";
            UpdateQTYSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "2") {
            var strng = document.getElementById("txtDiscount").value;
            document.getElementById("txtDiscount").value = strng + "4";
            UpdateDiscSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "3") {
            var strng = document.getElementById("txtPrice").value;
            document.getElementById("txtPrice").value = strng + "4";
            UpdatePriceSubChange();
        }
    }
    else {
        var $textAndPic = $('<div></div>');
        $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        //$textAndPic.append('<div class="alert-message">Please Select Item</div>');
        $textAndPic.append('<div class="alert-message">' + $('#hdnPlzSelectItem').val() + '</div>');
        BootstrapDialog.alert($textAndPic);
    }
}
function FiveSubChange() {
    if ($('#txthdnItemId').val() != "") {
        if ($("#txthdnActiveColumn").val() == "1") {
            var strng = document.getElementById("txtQuantity").value;
            document.getElementById("txtQuantity").value = strng + "5";
            UpdateQTYSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "2") {
            var strng = document.getElementById("txtDiscount").value;
            document.getElementById("txtDiscount").value = strng + "5";
            UpdateDiscSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "3") {
            var strng = document.getElementById("txtPrice").value;
            document.getElementById("txtPrice").value = strng + "5";
            UpdatePriceSubChange();
        }
    }
    else {
        var $textAndPic = $('<div></div>');
        $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        //$textAndPic.append('<div class="alert-message">Please Select Item</div>');
        $textAndPic.append('<div class="alert-message">' + $('#hdnPlzSelectItem').val() + '</div>');
        BootstrapDialog.alert($textAndPic);
    }
}

function SixSubChange() {
    if ($('#txthdnItemId').val() != "") {
        if ($("#txthdnActiveColumn").val() == "1") {
            var strng = document.getElementById("txtQuantity").value;
            document.getElementById("txtQuantity").value = strng + "6";
            UpdateQTYSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "2") {
            var strng = document.getElementById("txtDiscount").value;
            document.getElementById("txtDiscount").value = strng + "6";
            UpdateDiscSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "3") {
            var strng = document.getElementById("txtPrice").value;
            document.getElementById("txtPrice").value = strng + "6";
            UpdatePriceSubChange();
        }
    }
    else {
        var $textAndPic = $('<div></div>');
        $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        //$textAndPic.append('<div class="alert-message">Please Select Item</div>');
        $textAndPic.append('<div class="alert-message">' + $('#hdnPlzSelectItem').val() + '</div>');
        BootstrapDialog.alert($textAndPic);
    }
}
function SevenSubChange() {
    if ($('#txthdnItemId').val() != "") {
        if ($("#txthdnActiveColumn").val() == "1") {
            var strng = document.getElementById("txtQuantity").value;
            document.getElementById("txtQuantity").value = strng + "7";
            UpdateQTYSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "2") {
            var strng = document.getElementById("txtDiscount").value;
            document.getElementById("txtDiscount").value = strng + "7";
            UpdateDiscSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "3") {
            var strng = document.getElementById("txtPrice").value;
            document.getElementById("txtPrice").value = strng + "7";
            UpdatePriceSubChange();
        }
    }
    else {
        var $textAndPic = $('<div></div>');
        $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        //$textAndPic.append('<div class="alert-message">Please Select Item</div>');
        $textAndPic.append('<div class="alert-message">' + $('#hdnPlzSelectItem').val() + '</div>');
        BootstrapDialog.alert($textAndPic);
    }
}
function EightSubChange() {
    if ($('#txthdnItemId').val() != "") {
        if ($("#txthdnActiveColumn").val() == "1") {
            var strng = document.getElementById("txtQuantity").value;
            document.getElementById("txtQuantity").value = strng + "8";
            UpdateQTYSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "2") {
            var strng = document.getElementById("txtDiscount").value;
            document.getElementById("txtDiscount").value = strng + "8";
            UpdateDiscSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "3") {
            var strng = document.getElementById("txtPrice").value;
            document.getElementById("txtPrice").value = strng + "8";
            UpdatePriceSubChange();
        }
    }
    else {
        var $textAndPic = $('<div></div>');
        $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        //$textAndPic.append('<div class="alert-message">Please Select Item</div>');
        $textAndPic.append('<div class="alert-message">' + $('#hdnPlzSelectItem').val() + '</div>');
        BootstrapDialog.alert($textAndPic);
    }
}
function NineSubChange() {
    if ($('#txthdnItemId').val() != "") {
        if ($("#txthdnActiveColumn").val() == "1") {
            var strng = document.getElementById("txtQuantity").value;
            document.getElementById("txtQuantity").value = strng + "9";
            UpdateQTYSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "2") {
            var strng = document.getElementById("txtDiscount").value;
            document.getElementById("txtDiscount").value = strng + "9";
            UpdateDiscSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "3") {
            var strng = document.getElementById("txtPrice").value;
            document.getElementById("txtPrice").value = strng + "9";
            UpdatePriceSubChange();
        }
    }
    else {
        var $textAndPic = $('<div></div>');
        $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        //$textAndPic.append('<div class="alert-message">Please Select Item</div>');
        $textAndPic.append('<div class="alert-message">' + $('#hdnPlzSelectItem').val() + '</div>');
        BootstrapDialog.alert($textAndPic);
    }
}
function ZeroSubChange() {
    if ($('#txthdnItemId').val() != "") {
        if ($("#txthdnActiveColumn").val() == "1") {
            var strng = document.getElementById("txtQuantity").value;
            document.getElementById("txtQuantity").value = strng + "0";
            UpdateQTYSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "2") {
            var strng = document.getElementById("txtDiscount").value;
            document.getElementById("txtDiscount").value = strng + "0";
            UpdateDiscSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "3") {
            var strng = document.getElementById("txtPrice").value;
            if (Number(document.getElementById("txtPrice").value) != 0) {
                document.getElementById("txtPrice").value = strng + "0";
                UpdatePriceSubChange();
            } else {
                document.getElementById("txtAmount").value = '0.00';
                document.getElementById("txtPrice").value = '0'
                UpdatePriceSubChange();
            }
        }
    }
    else {
        var $textAndPic = $('<div></div>');
        $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        //$textAndPic.append('<div class="alert-message">Please Select Item</div>');
        $textAndPic.append('<div class="alert-message">' + $('#hdnPlzSelectItem').val() + '</div>');
        BootstrapDialog.alert($textAndPic);
    }
}
function dotSubChange() {
    if ($('#txthdnItemId').val() != "") {
        if ($("#txthdnActiveColumn").val() == "1") {
            var strng = document.getElementById("txtQuantity").value;
            document.getElementById("txtQuantity").value = strng + ".";
            UpdateQTYSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "2") {
            var strng = document.getElementById("txtDiscount").value;
            document.getElementById("txtDiscount").value = strng + ".";
            UpdateDiscSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "3") {
            var strng = document.getElementById("txtPrice").value;
            document.getElementById("txtPrice").value = strng + ".";
            UpdatePriceSubChange();
        }
    }
    else {
        var $textAndPic = $('<div></div>');
        $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        //$textAndPic.append('<div class="alert-message">Please Select Item</div>');
        $textAndPic.append('<div class="alert-message">' + $('#hdnPlzSelectItem').val() + '</div>');
        BootstrapDialog.alert($textAndPic);
    }
}


function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
      s4() + '-' + s4() + s4() + s4();
}
function GenarateGustName() {
    var d = new Date();
    var n = d.valueOf();
    return 'G#' + n;
}
function GenarateGustId() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
      s4() + '-' + s4() + s4() + s4();
}

var popupSts;
function toNumberDecimalTwoPR(number) {

    var numberSplitd_ = number.toString().split('.');
    if (numberSplitd_.length > 1) {
        var subUpdatedNumber = '';
        var submNumberSplit = numberSplitd_[1];
        if (submNumberSplit.length >= 2) {
            if (submNumberSplit.length == 2) {

                submNumberSplit = submNumberSplit + '00';
                subUpdatedNumber = submNumberSplit.substring(0, 4);
                //   return subUpdatedNumber == "0000" ? parseFloat(numberSplitd_[0] + '.' + subUpdatedNumber).toFixed(2) : parseFloat(numberSplitd_[0] + '.' + subUpdatedNumber);
                return (numberSplitd_[0] + '.' + subUpdatedNumber);
                //if (subUpdatedNumber == "0000") {
                //    return parseFloat(numberSplitd_[0] + '.' + subUpdatedNumber).toFixed(2);
                //} else {
                //    return parseFloat(numberSplitd_[0] + '.' + subUpdatedNumber);
                //}
            }
            if (submNumberSplit.length == 3) {

                submNumberSplit = submNumberSplit + '0';
                subUpdatedNumber = submNumberSplit.substring(0, 4);
                //  return subUpdatedNumber == "0000" ? parseFloat(numberSplitd_[0] + '.' + subUpdatedNumber).toFixed(2) : parseFloat(numberSplitd_[0] + '.' + subUpdatedNumber);
                return (numberSplitd_[0] + '.' + subUpdatedNumber);
            }
            if (submNumberSplit.length >= 4) {

                submNumberSplit = submNumberSplit;
                subUpdatedNumber = submNumberSplit.substring(0, 4);
                //   return subUpdatedNumber == "0000" ? parseFloat(numberSplitd_[0] + '.' + subUpdatedNumber).toFixed(2) : parseFloat(numberSplitd_[0] + '.' + subUpdatedNumber);
                return (numberSplitd_[0] + '.' + subUpdatedNumber);
            }
        }
        else {
            return (number);
        }
    }
    else {
        return (number + ".00");
    }
}
function toNumberDecimalTwo(number_) {

    var numberSplitd_ = number_.toString().split('.');
    if (numberSplitd_.length > 1) {
        var subUpdatedNumber = '';
        var submNumberSplit = numberSplitd_[1];
        if (submNumberSplit.length >= 2) {
            if (submNumberSplit.length == 2) {

                submNumberSplit = submNumberSplit + '00';
                subUpdatedNumber = submNumberSplit.substring(0, 4);
                //   return subUpdatedNumber == "0000" ? parseFloat(numberSplitd_[0] + '.' + subUpdatedNumber).toFixed(2) : parseFloat(numberSplitd_[0] + '.' + subUpdatedNumber);
                return Number(numberSplitd_[0] + '.' + subUpdatedNumber);
                //if (subUpdatedNumber == "0000") {
                //    return parseFloat(numberSplitd_[0] + '.' + subUpdatedNumber).toFixed(2);
                //} else {
                //    return parseFloat(numberSplitd_[0] + '.' + subUpdatedNumber);
                //}
            }
            if (submNumberSplit.length == 3) {

                submNumberSplit = submNumberSplit + '0';
                subUpdatedNumber = submNumberSplit.substring(0, 4);
                //  return subUpdatedNumber == "0000" ? parseFloat(numberSplitd_[0] + '.' + subUpdatedNumber).toFixed(2) : parseFloat(numberSplitd_[0] + '.' + subUpdatedNumber);
                return Number(numberSplitd_[0] + '.' + subUpdatedNumber);
            }
            if (submNumberSplit.length >= 4) {

                submNumberSplit = submNumberSplit;
                subUpdatedNumber = submNumberSplit.substring(0, 4);
                //   return subUpdatedNumber == "0000" ? parseFloat(numberSplitd_[0] + '.' + subUpdatedNumber).toFixed(2) : parseFloat(numberSplitd_[0] + '.' + subUpdatedNumber);
                return Number(numberSplitd_[0] + '.' + subUpdatedNumber);
            }
        }
        else {
            return Number(number_);
        }
    }
    else {
        return Number(number_ + ".00");
    }
}
function OnSave() {
    var a = valueAppendtoPOPUp();
    if (a == false) {
        return false;
    }
    if ($('#chkHomeDelivery').prop('checked') == true && $('#txtHomeDelPhNo').val() == '' && $('#txtHomeDelName').val() == '' && $('#txtHomeDelAddr').val() == '') {
        var $textAndPic = $('<div></div>');
        $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        $textAndPic.append('<div class="alert-message">Please uncheck Home delivery checkbox or enter Contact Details</div>');
        BootstrapDialog.alert($textAndPic);
        return false;
    }
    //$('#hdnTransModeforPay').val('Other');

    var myTabPart = document.getElementById('tbl_tenderDyms');
    if (myTabPart.rows.length > 0) {
      

        var val_ = parseFloat($('#lbl_TpartTotal')[0].innerText);
        val_ = 0;
        if (parseFloat(val_) >= 0) {

        }
        else {
            var $textAndPic = $('<div></div>');
            $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
            //$textAndPic.append('<div class="alert-message">Amount should be Greater than or Equal to Amount Tendered</div>');
            $textAndPic.append('<div class="alert-message">' + $('#hdnAmtGrtrEqlAmtTend').val() + '</div>');
            BootstrapDialog.alert($textAndPic);
            return false;
        }
      
        if ($('#hdnTransModeforPay').val() == 'Other') {
            if ($('#txtTendertrans').val() == '') {
                var $textAndPic = $('<div></div>');
                $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                //$textAndPic.append('<div class="alert-message">Please Enter Transaction Reference</div>');
                $textAndPic.append('<div class="alert-message">' + $('#hdnMsgTranRef').val() + '</div>');
                BootstrapDialog.alert($textAndPic);
                return false;
            }
        }
        var BRANCHID = ''; var COMPANYID = '';
        var CURRENCYID = '';
        var HashValues = (window.localStorage.getItem("hashValues"));
        if (HashValues != null && HashValues != '') {
            var hashSplit = HashValues.split('~');
            CURRENCYID = hashSplit[16];
            COMPANYID = hashSplit[0];
            BRANCHID = hashSplit[1];
        }
        var strOptions = $('#hdn_CusId').val();
        //if (strOptions != "") {
        var ItemData = ""; var CREATEDBY = window.localStorage.getItem('Sno');
        var CASHINVID = guid();
        var myTab = document.getElementById('tabletrpopbind');
        var ItemId = ""; var Qty_ = "0"; var BasePrice = "0"; var Amount = "0"; var Disc_ = "0"; var taxTypeP_ = ''; var BaseAmount = 0;
      
        var P20 = {
        }
        var P21 = {
        }
        var P24 = {
        }
        var I44 = {
        }
        P20["CUSTOMERID"] = strOptions;
        P20["CASHINVID"] = CASHINVID;
        P20["CASHINVOICETYPE"] = '0';
        var cusSplit = $('#hdn_CusId').val().split('-$');
        if (cusSplit.length == 2) {
            P20["CUSTID"] = '';
        }
        else {
            P20["CUSTID"] = $('#hdn_CusId').val();
        }

        P20["CURRENCYID"] = CURRENCYID;
        P20["COUNTERID"] = window.localStorage.getItem('COUNTERID');
        P20["INVOICECATEGORY"] = '0';
        //  P20["TOTALBILLAMOUNT"] = $('#txtPricepp').val();
        P20["TOTALBILLAMOUNT"] = $('#hdnPricepp').val();
         
        P20["DISCOUNT"] = $('#hdnDiscountpp').val();
        P20["NETBILLAMOUNT"] = Math.round($('#txtAmountpp').val(), 2);
        P20["ROUNDOFAMT"] = Math.round($('#txtAmountpp').val(), 2);
        //if ($('#hdnTransModeforPay').val() == 'Cash') {
        //    P20["AMOUNTRECEIVED"] = $('#txtTendertrans').val();

        //}
        //else {
        //    P20["AMOUNTRECEIVED"] = $('#hdnAmountpp').val();

        //}

        P20["AMOUNTRECEIVED"] = $('#lbl_TTpartTotal')[0].innerText;
        P20["PAYMENTSTATUS"] = '6';
        P20["SHIFTID"] = window.localStorage.getItem('SHIFTID');
        P20["DESCRIPTON"] = '';
        P20["PAYMENTTERMSID"] = '';
        P20["LOYALTYCARDID"] = '';
        P20["DLIVERY_FROM"] = '';
        P20["DELIVERY_STATUS"] = 'CLOSED';
        P20["RECEIPT_STATUS"] = 'CLOSED';
        P20["INVOICE_STATUS"] = '';
        P20["TAXFORMULA"] = '';
        P20["BRANCHID"] = BRANCHID;
        P20["COMPANYID"] = COMPANYID;
        P20["CREATEDBY"] = CREATEDBY;
        P20["UPDATEDSITE"] = 'DBMS_REPUTIL.GLOBAL_NAME';
        P20["LANGID"] = 'en';
        P20["SALESPERSONID"] = window.localStorage.getItem('CashierID');

        P20["DELIVERYADDRESS"] = homeDeliveryAddress;
        P20["HOMEDELIVERY"] = $('#chkHomeDelivery').prop('checked') == true ? 'H' : 'O';
        //P20["COUNTERID"] = window.localStorage.getItem('COUNTERID');
        var OflineInvoice = GetofInvoice();
        P20["OFINVOICE"] = OflineInvoice;

        objP20 = [];
        //P20 - Data End
        //P21 - Data Prepare START
        objP21 = [];

        var Tax_ = 0; var Tax_1 = 0;
        for (i = 1; i < myTab.rows.length; i++) {
            Tax_ = 0;
            P21 = {
            }
            var objCells = myTab.rows.item(i).cells;

            if (objCells.length > 0) {
                taxTypeP_ = objCells.item(16).innerHTML;
                if (objCells.item(6).innerHTML != '') {

                    if (taxTypeP_ != 'R') {
                        Tax_ = Number(objCells.item(7).innerHTML);
                        Tax_1 = Tax_1 + Number(objCells.item(7).innerHTML);
                    }
                    else {

                        Tax_ = toNumberDecimalTwo(parseFloat(objCells.item(7).innerHTML).toFixed(8));
                        Tax_1 = Tax_1 + parseFloat(toNumberDecimalTwo(parseFloat(objCells.item(7).innerHTML).toFixed(8)));

                    }
                }
                ItemId = objCells.item(11).innerHTML.split('~')[0];
                Qty_ = objCells.item(12).innerHTML;
                Disc_ = objCells.item(13).innerHTML;
                Amount = objCells.item(14).innerHTML;


                BaseAmount = objCells.item(5).innerHTML;

                var mapCode = objCells.item(6).innerHTML;
                if (mapCode == 'undefined') {
                    mapCode = '';
                }
                BasePrice = objCells.item(4).innerHTML;
                //BasePrice = $('#td_' + ItemId).val();
                P21["RGUID"] = guid();
                P21["CASHINVID"] = CASHINVID
                P21["ITEMID"] = ItemId;
                P21["ITEMCATEGORYID"] = Qty_;
                P21["UOMID"] = objCells.item(15).innerHTML;
                P21["DISCOUNT"] = Disc_; //Line Item Discount
                P21["ITEMTAX"] = Tax_; //Line Item TaxPercent
                P21["QUANTITY"] = Qty_;
                P21["FREE_QUANTITY"] = '';
                P21["PRICE"] = BasePrice;
                //P21["PRICE"] = BasePrice;
                P21["Total"] = Amount;
                P21["mapCode"] = mapCode;
                P21["SALESPERSONID"] = window.localStorage.getItem('CashierID');
                P21["RET_QTY"] = "0";
                P21["RET_FREE_QTY"] = "0";
                P21["ITEMDISC"] = '';
                P21["TAXTYPE"] = taxTypeP_;
                P21["AMOUNT"] = BaseAmount;
                P21["BRANCHID"] = BRANCHID;
                P21["COMPANYID"] = COMPANYID;
                P21["CREATEDBY"] = CREATEDBY;
                P24["UPDATEDSITE"] = 'DBMS_REPUTIL.GLOBAL_NAME';
                P24["LANGID"] = 'en';
                P21["ITEMTYPE_ID"] = objCells.item(17).innerHTML;
                P21["ITEMTYPE_MODE"] = objCells.item(18).innerHTML;
                objP21.push(P21);
            }
        }
        P20["TOTALTAXAMOUNT"] = Tax_1;
        objP20.push(P20);
        //P21 - Data End


     
        var currencyExdCheck_ = 0;
        //P24 - Data Prepare START
        objP24 = [];

        var myTabPart = document.getElementById('tbl_tenderDyms');
        for (i = 0; i < myTabPart.rows.length; i++) {
            var objCells = myTabPart.rows.item(i).cells;
            try {
                if (parseFloat(objCells.item(4).innerHTML) > 0) {
                    P24 = {
                    }

                    var payMode_ = objCells.item(1).innerHTML.split('_');

                    P24["CASHINVID"] = CASHINVID
                    //P24["TENDERTYPE"] = $('#hdnPayMode').val();
                    P24["TENDERTYPE"] = payMode_[0];
                    $('#hdnPayMode').val(payMode_[0]);
                    if ($('#hdnPayMode').val().toUpperCase() == 'CREDITCARD') {
                        P24["TENDERTYPE"] = 'Credit Card';
                    }
                    if ($('#hdnPayMode').val().toUpperCase() == 'CREDITNOTE') {
                        P24["TENDERTYPE"] = 'Credit Note';
                    }
                    if ($('#hdnPayMode').val().toUpperCase() == 'DEBITCARD') {
                        P24["TENDERTYPE"] = 'Debit Card';
                    }
                    P24["BANKNAME"] = $('#hdnPayModeType').val();
                    P24["CARDNAME"] = '';
                    P24["CARDTYPE"] = '';
                    //if ($('#lblTendertype').text() != '') {

                    //    var trndertype = $('#lblTendertype').text().split('(')
                    //    var split1_ = trndertype[1].split(')');
                    //    P24["INSTRUMENTNUMBER"] = split1_[0];
                    //}
                    //else
                    //    P24["INSTRUMENTNUMBER"] = '';

                    if (payMode_.length >= 1) {

                        //var trndertype = $('#lblTendertype').text().split('(')
                        //var split1_ = trndertype[1].split(')');
                        P24["INSTRUMENTNUMBER"] = payMode_[1];
                    }
                    else
                        P24["INSTRUMENTNUMBER"] = '';

                    P24["TOTAL_AMOUNT"] = objCells.item(3).innerHTML;
                    //P24["TOTAL_AMOUNT"] = BaseAmount;

                    P24["TENDERID"] = objCells.item(5).innerHTML;
                    P24["COUNTERTYPE"] = window.localStorage.getItem('COUNTERTYPE');
                    //P24["CURRENCYID"] = CURRENCYID;
                    P24["CURRENCYID"] = objCells.item(6).innerHTML;
                    //P24["AMOUNT"] = $('#hdnAmountpp').val();;
                    P24["AMOUNT"] = objCells.item(4).innerHTML;
                    P24["CLEARINGCHARGES"] = '';
                    P24["EXCHANGERATE"] = objCells.item(2).innerHTML;
                    P24["TRANSTYPE"] = 'INVOICE';
                    P24["COUNTERID"] = window.localStorage.getItem('COUNTERID');
                    P24["SHIFTID"] = window.localStorage.getItem('SHIFTID');
                    if (parseFloat(parseFloat($('#lbl_TTpartTotal')[0].innerText) - Math.round($('#txtAmountpp').val()).toFixed(2)) > 0)
                        if (payMode_[0].toUpperCase() == 'CASH') {

                            if ($('#hdnTemBasePriceEX').val() == '1') {
                                P24["CHANGE"] = parseFloat(parseFloat($('#lbl_TpartTotal')[0].innerText) / parseFloat(objCells.item(2).innerHTML)).toFixed(4);

                            }
                            else {

                                P24["CHANGE"] = parseFloat(parseFloat(parseFloat($('#lbl_TTpartTotal')[0].innerText) - Math.round($('#txtAmountpp').val()).toFixed(2)) / parseFloat(objCells.item(2).innerHTML)).toFixed(4);

                            }
                        }
                        else {
                            P24["CHANGE"] = '0';
                            currencyExdCheck_ = parseFloat(currencyExdCheck_) + parseFloat(objCells.item(4).innerHTML);
                        }
                    else
                        P24["CHANGE"] = '0';
                    P24["UPDATEDSITE"] = 'DBMS_REPUTIL.GLOBAL_NAME';
                    P24["LANGID"] = 'en';
                    P24["BRANCHID"] = BRANCHID;
                    P24["COMPANYID"] = COMPANYID;
                    P24["CREATEDBY"] = CREATEDBY;
                    objP24.push(P24);
                }
            }
            catch (e) {

            }
        }

        //P24 - Data End

        var myTab = document.getElementById('tbl_tenderDyms');
        var mVal_ = "0";
        // LOOP THROUGH EACH ROW OF THE TABLE AFTER HEADER.
        for (i = 0; i < myTab.rows.length; i++) {
            // GET THE CELLS COLLECTION OF THE CURRENT ROW.
            var objCells = myTab.rows.item(i).cells;
            if (objCells.length > 0) {
                mVal_ = parseFloat(mVal_) + parseFloat(objCells.item(4).innerHTML);
                mVal_ = parseFloat(mVal_).toFixed(2);
            }
        }
        var valGet_ = Math.round(mVal_) - Math.round($('#txtAmountpp').val());
        if (valGet_ < 0) {
            //if (currencyExdCheck_ >= Math.round($('#txtAmountpp').val()).toFixed(2)) {
            var $textAndPic = $('<div></div>');
            $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
            //$textAndPic.append('<div class="alert-message">Please Enter Transaction Reference</div>');
            $textAndPic.append('<div class="alert-message">Please enter Valid Amount </div>');
            BootstrapDialog.alert($textAndPic);
            return false;
            //}
        }

         

        var InvoiceParams = new Object();
        InvoiceParams.P20 = JSON.stringify(objP20);
        InvoiceParams.P21 = JSON.stringify(objP21);
        InvoiceParams.P24 = JSON.stringify(objP24);
        InvoiceParams.VD = VD1();

        //Put the customer object into a container
        var ComplexObject = new Object();
        //The property name "WebMethodArgName" must match the web method argument "WebMethodArgName"!
        ComplexObject.Invoice = InvoiceParams;

        //Stringify and verify the object is foramtted as expected
        var data = JSON.stringify(ComplexObject);
        var img = document.getElementById('loader');
        img.style.display = "block";
        //$('#loader').show();

        $.ajax({
            type: 'POST',
            url: CashierLite.Settings.InvoiceSave,
            data: data,
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            success: function (resp) {

                img.style.display = "none";

                var result = resp.InvoiceSaveResult.split(',');
                if (result[0] == "100000") {
                    if ($('#hdnISPRINT').val() == 'true') {
                        window.localStorage.setItem('Inv', '1');
                        //$('#lbl_InvoiceNoPrint').text('Invoice No. : ' + result[1]);
                        window.localStorage.setItem("Tax", result[3]);
                        $('#lbl_InvoiceNoPrint').text($('#hdnInvoice').val() + ' : ' + result[1]);
                        window.localStorage.setItem('InvoiceId', result[1]);
                        window.localStorage.setItem('Connection', result[4]);
                        print('t', result[1], result[5]);

                    }
                    else {

                        window.localStorage.setItem('Inv', '1');
                        window.localStorage.setItem('InvoiceId', '');
                        window.localStorage.setItem('Connection', '');
                        window.localStorage.setItem('InvoiceId', result[1]);
                        window.localStorage.setItem('Connection', result[4]);
                        //$('#lbl_InvoiceNoPrint').text('Invoice No. : ' + result[1]);
                        $('#lbl_InvoiceNoPrint').text($('#hdnInvoice').val() + ' : ' + result[1]);
                        window.localStorage.setItem("Tax", result[3]);
                        print('f', result[1], result[5]);

                    }
                

                }

            },
            error: function (e) {
                
                OflineInvoice = GetofInvoice();
                var tx = db.transaction("MyObjectStore", "readwrite");
                var store = tx.objectStore("MyObjectStore");
                store.put({
                    id: CASHINVID, InvoiceData: data
                });
                window.localStorage.setItem('Inv', '1');
                $('#lbl_InvoiceNoPrint').text($('#hdnInvoice').val() + ' : ' + OflineInvoice);
                window.localStorage.setItem("Tax", false);
                print('t');
                window.localStorage.setItem("Tax", "");
                removeDraft(strOptions);
                $('#hdn_CusId').val('');
                $('#txtCusName').val('');
                ClearFileds();
                $("#itemCarttable tr").remove();
                $('#grdandTotal').text('0.00');
                $('#myModalpay').modal('hide');
                $('#lbl_TTpartTotal')[0].innerText = '0';
                $("#hdnPwdCheckStatus").val('0');
                $('#hdn_CusId').val('');
                $('#chkHomeDelivery').prop('checked', false);
                $('#myTable li a').removeAttr('onclick');
            }
        });
      

    }
    else {
        var $textAndPic = $('<div></div>');
        $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        //$textAndPic.append('<div class="alert-message">Please Select Transaction Type</div>');
        $textAndPic.append('<div class="alert-message">' + $('#hdnPlzSelectTranType').val() + '</div>');
        BootstrapDialog.alert($textAndPic);
    }
    
}

function print(val_, invId, Roundval) {
    $.ajax({
        url: CashierLite.Settings.getItemAliasByInvoiceId,
        data: "VD=" + VD1() + "&InvoiceId=" + invId,
        success: function (resp) {

            $('#tblNBaseCurrPayment').hide();
            v = $('#hdnExchangerate').val();
            $('#myModalpay').hide();
            var rows = $('#tabletrpopbind tbody >tr');
            $("#tblheader_4 tbody").remove();
            $("#tblheader_4 tr").remove();
            for (var i = 0; i < rows.length; i++) {
                if (rows[i].innerHTML !== '') {
                    var Columns = $(rows[i]).find('td');
                     
                    //$('#tblheader_4').append("<tr><td style='font-size: 12px;' colspan='4'>" + resp.find(x=>x.CusID == $(Columns[11]).html())["Code"] + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>");
                    if ($(Columns[1]).html().length <= 17) {
                        $('#tblheader_4').append("<tr><td style='font-size: 12px;'>" + $(Columns[1]).html() + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td style='font-size: 12px;'>" + $(Columns[2]).html() + " &nbsp;&nbsp;&nbsp;&nbsp;</td><td style='font-size: 12px;'>" + (parseFloat($(Columns[4]).html())).toFixed(2) + "&nbsp;&nbsp;&nbsp;&nbsp;</td><td style='font-size: 12px;'>" + (parseFloat($(Columns[8]).html())).toFixed(2) + "</td></tr>");
                    }
                    else {

                        $('#tblheader_4').append("<tr><td style='font-size: 12px;'  colspan='4'>" + $(Columns[1]).html() + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>");
                        $('#tblheader_4').append("<tr><td style='font-size: 12px;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td style='font-size: 12px;'>" + $(Columns[2]).html() + " &nbsp;&nbsp;&nbsp;&nbsp;</td><td style='font-size: 12px;'>" + (parseFloat($(Columns[4]).html())).toFixed(2) + "&nbsp;&nbsp;&nbsp;&nbsp;</td><td style='font-size: 12px;'>" + (parseFloat($(Columns[8]).html())).toFixed(2) + "</td></tr>");

                    }//}
                }
            }
            var tblheader5rows = $('#tbhearder_5 tbody tr');
            var tblColumns = $(tblheader5rows[0]).find('td');
            $(tblColumns[4]).html($('#txtPricepp').val());
            var tblColumns_1 = $(tblheader5rows[1]).find('td');
            $(tblColumns_1[4]).html($('#txtDiscountpp').val());
            var tblColumns_2 = $(tblheader5rows[2]).find('td');
            $(tblColumns_2[4]).html($('#txtAmountpp').val());
            

            $('#lbl_totalGross').text($('#hdnBaseTotal').val() + ' : ' + (parseFloat($('#txtPricepp').val())));
            $('#lbl_Discount_Print').text($('#hdnDisc').val().replace('(%)', '') + ' : ' + (parseFloat($('#txtDiscountpp').val())).toFixed(2));
            ////$('#lbl_Round_Print').text('Net Total : ' + parseFloat($('#txtAmountpp').val()).toFixed(2));
            $('#lblRound').text($('#hdnRound').val() + ' : ' + parseFloat(Roundval).toFixed(2));
            $('#lbl_Round_Print').text($('#hdnNetTotal').val() + ' : ' + parseFloat(parseFloat($('#txtAmountpp').val())).toFixed(2));
            //}
            //$('#hdnTransModeforPay').val('Cash');
            //$('#lbl_total_2').text(parseFloat($('#txtTendertrans').val()).toFixed(2));
            var HashCURRENCY = '';
            var hashSplit = '';
            var HashValues = (window.localStorage.getItem("hashValues"));
            if (HashValues != null && HashValues != '') {
                hashSplit = HashValues.split('~');
                HashCURRENCY = hashSplit[18].split('/')[0];
                $('#lbl_Currency_2').text((currency_symbols[HashCURRENCY]) + ' ' + HashCURRENCY);
            }
            $('#lblTenderConvertAmt').text($('#lbl_TTpartTotal')[0].innerText);

            $('#lbl_total_2').text((currency_symbols[HashCURRENCY]) + ' ' + parseFloat($('#txtAmountpp').val()).toFixed(0));
            $('#lbl_cash_1').text((currency_symbols[HashCURRENCY]) + ' ' + parseFloat($('#lbl_TTpartTotal')[0].innerText).toFixed(0));

            $('#lbl_change_1').text((currency_symbols[HashCURRENCY]) + ' ' + (parseFloat($('#lbl_TTpartTotal')[0].innerText).toFixed(0) - parseFloat($('#txtAmountpp').val()).toFixed(0)));


            //$('#lbl_Pay_Print').text('Pay in : ' + window.localStorage.getItem('PayModeInvoice'));
            $('#lbl_Pay_Print').text($('#hdnPayIn').val() + ' : ' + window.localStorage.getItem('PayModeInvoice'));



            if ($('#hdnTemBasePriceCurId').val() != '' && $('#hdnTemBasePriceCurId').val() != 1) {
                $('#lbl_Currency_3').text($('#hdnTemBasePrice').val());
                $('#tblNBaseCurrPayment').show();
                $('#lbl_total_3').text(parseFloat(parseFloat(Math.round($('#txtAmountpp').val()).toFixed(2)) / parseFloat($('#hdnTemBasePriceEX').val())).toFixed(4));

                //$('#lbl_cash_2').text(parseFloat(parseFloat($('#lbl_TTpartTotal')[0].innerText) / parseFloat($('#hdnExchangerate').val())).toFixed(0));

                //$('#lbl_cash_2').text(parseFloat($('#lbl_TTpartTotal')[0].innerText).toFixed(4));
                $('#lbl_cash_2').text(parseFloat(parseFloat($('#lbl_TTpartTotal')[0].innerText) / parseFloat($('#hdnTemBasePriceEX').val())).toFixed(4));

                $('#lbl_change_2').text((currency_symbols[$('#hdnTemBasePrice').val()]) + ' ' + (parseFloat(parseFloat($('#lbl_cash_2')[0].innerText) - parseFloat($('#lbl_total_3')[0].innerText)).toFixed(4)));


                $('#lbl_total_3').text((currency_symbols[$('#hdnTemBasePrice').val()]) + ' ' + $('#lbl_total_3').text());

                $('#lbl_cash_2').text((currency_symbols[$('#hdnTemBasePrice').val()]) + ' ' + $('#lbl_cash_2').text());
            }
            var Tax_ = ""; var Sub_ = ""; var TaxCheck_ = "0";

            document.getElementById('tblheader_5_').innerHTML = "";

            if (window.localStorage.getItem("Tax")) {

                var TaxSplit_ = window.localStorage.getItem("Tax").split('~');

                for (var v_ = 0; v_ < TaxSplit_.length; v_++) {
                    var tax1_ = TaxSplit_[v_].split('@');
                    if (tax1_.length == 2) {
                        if (tax1_[1] != '') {
                            TaxCheck_ = "1"; Sub_ = "";
                            Sub_ = tax1_[0] + " : " + parseFloat(tax1_[1]).toFixed(2);
                            Tax_ = Tax_ + " <tr><td style='font-size: 12px;'>&nbsp;&nbsp;" + Sub_ + "</td></tr>";
                        }
                    }
                }
            }
            if (TaxCheck_ == "1")
                document.getElementById('tblheader_5_').innerHTML = Tax_;

            
            if (val_ == 't') {

                if ($('#hdnISPRINT').val() == "true") {
                    $('#myModalprint').modal();
                    //$('#myModalprint').css('display', "block");

                } else {
                    $('#myModalprint').css('display', "none");
                };
                //$('#myModalprint').css('display', "block");

            }
            else {

                if ($('#hdnISPRINT').val() == "true") {
                    $('#myModalprint').modal();
                    //$('#myModalprint').css('display', "block");

                } else {
                    $('#myModalprint').css('display', "none");
                };
               
            }
            

            if (val_ == 't') {

                window.localStorage.setItem('InvoiceId', '');
                window.localStorage.setItem('Connection', '');
                window.localStorage.setItem("Tax", "");
                removeDraft($('#hdn_CusId').val());
                $('#hdn_CusId').val('');
                $('#txtCusName').val('');
                ClearFileds();
                $("#itemCarttable tr").remove();
                $('#grdandTotal').text('0.00');
                $('#myModalpay').modal('hide');
                $("#tbl_tenderDyms tr").remove();
                $('#lblTenderConvertAmt').text('');
            }
            else {
                window.localStorage.setItem("Tax", "");
                removeDraft($('#hdn_CusId').val());
                $('#hdn_CusId').val('');
                $('#txtCusName').val('');
                ClearFileds();
                $("#itemCarttable tr").remove()
                $('#grdandTotal').text('0.00');;
                $('#myModalpay').modal('hide');
                //printDiv();
                $("#tbl_tenderDyms tr").remove();
                $('#lblTenderConvertAmt').text('');
            }
            $('#lbl_TTpartTotal')[0].innerText = '0';
            $("#hdnPwdCheckStatus").val('0');
            $('#hdn_CusId').val('');
            $('#chkHomeDelivery').prop('checked', false);
            printDiv();
            $('#txtItemScan').val('');
            $('#myTable li a').removeAttr('onclick');
            GetCustomers_();
        }
    });


    
}
function GetofInvoice() {
    
    return "OF";
}
function RemoveubChange() {
    if ($('#txthdnItemId').val() != "") {
        if ($("#txthdnActiveColumn").val() == "1") {
            var strng = document.getElementById("txtQuantity").value;
            document.getElementById("txtQuantity").value = strng.substring(0, strng.length - 1)
            UpdateQTYSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "2") {
            var strng = document.getElementById("txtDiscount").value;
            document.getElementById("txtDiscount").value = strng.substring(0, strng.length - 1)
            UpdateDiscSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "3") {
            var strng = document.getElementById("txtPrice").value;
            document.getElementById("txtPrice").value = strng.substring(0, strng.length - 1)
            UpdatePriceSubChange();
        }
    }
    else {
        var $textAndPic = $('<div></div>');
        $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        //$textAndPic.append('<div class="alert-message">Please Select Item</div>');
        $textAndPic.append('<div class="alert-message">' + $('#hdnPlzSelectItem').val() + '</div>');
        BootstrapDialog.alert($textAndPic);
    }
}


function IsNumericTenderRef(e) {

    var specialKeys = new Array();
    specialKeys.push(8);
    var keyCode = e.which ? e.which : e.keyCode
    var key = e.which ? e.which : e.key
    if (keyCode == 59) {
        $("#txtRefNumber").val('');

    }
    if ($('#hdnPayMode').val() == 'Cash') {
        var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1 || keyCode == 46);
        if (ret == false) {
            return ret;
        }
        else {

        }
    }
    else {
        if ($("#txtRefNumber").val().length <= 15) {
            var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1 || keyCode == 46);
            if (ret == false) {
                return ret;
            }
            else {

            }
        }
        else {
            return false;
        }
    }

}



function IsNumericTender(e) {

    var specialKeys = new Array();
    specialKeys.push(8);
    var keyCode = e.which ? e.which : e.keyCode
    var key = e.which ? e.which : e.key
    if (keyCode == 59) {
        $("#txtTendertrans").val('');

    }
    if ($('#hdnPayMode').val() == 'Cash') {
        var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1 || keyCode == 46);
        if (ret == false) {
            return ret;
        }
        else {

        }
    }
    else {
        if ($("#txtTendertrans").val().length <= 15) {
            var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1 || keyCode == 46);
            if (ret == false) {
                return ret;
            }
            else {

            }
        }
        else {
            return false;
        }
    }
}
function IsNumericQTY(e) {

    var myTab = document.getElementById('itemCarttable');
    if (myTab.rows.length == 0) {
        return false;
    }
    var specialKeys = new Array();
    specialKeys.push(8);
    var keyCode = e.which ? e.which : e.keyCode

    var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1 || keyCode == 46);
    if (ret == false) {
        return ret;
    }
    else {

    }

}
function UpdateQTYSubChange() {
    //
    var ItemId = $('#txthdnItemId').val();
    var Qty = $('#txtQuantity').val();
    var myTab = document.getElementById('itemCarttable');
    var m_ = ""; var Price = "0";
    for (i = 0; i < myTab.rows.length; i++) {
        var objCells = myTab.rows.item(i).cells;
        if (Qty != "") {
            if (parseFloat(Qty) != 0) {
                if (ItemId == objCells.item(5).innerHTML) {
                    var qty_ = parseFloat(Qty).toFixed(3);
                    var Baseprice = Number(floorFigure($('#td_' + ItemId).val(), 4));
                    //var Baseprice = $('#td_' + ItemId).val();
                    //Baseprice = (parseFloat(objCells.item(4).innerHTML) / parseFloat(objCells.item(2).innerHTML));
                    Price = parseFloat(Baseprice) * parseFloat(Qty);
                    if (objCells.item(3).innerHTML != "") {
                        if (parseFloat(objCells.item(3).innerHTML) > 0) {
                            Price = parseFloat(Price) - ((parseFloat(Price) * (parseFloat(objCells.item(3).innerHTML))) / 100);
                        }
                    }

                    onSubDataQTY(objCells.item(1).innerHTML, objCells.item(5).innerHTML, objCells.item(1).innerHTML, objCells.item(9).innerHTML, objCells.item(3).innerHTML, objCells.item(7).innerHTML, objCells.item(8).innerHTML, objCells.item(10).innerHTML, objCells.item(11).innerHTML, document.getElementById("txtQuantity").value, objCells.item(13).innerHTML);
                    var markup = "<td  style='width:5% !important' ><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right' >" + objCells.item(1).innerHTML + "</td><td>" + parseFloat(qty_).toFixed(2) + "</td><td>" + parseFloat(objCells.item(3).innerHTML).toFixed(2) + "</td><td style='display:none' >" + parseFloat(Price).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td><input type='text' class='form-control'  value='" + Baseprice + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + objCells.item(7).innerHTML + "</td><td style='display:none'>" + objCells.item(8).innerHTML + "</td><td>" + Baseprice + "</td><td style='display:none'>" + objCells.item(10).innerText + "</td><td style='display:none'>" + objCells.item(11).innerText + "</td><td style='display:none'>" + objCells.item(12).innerText + "</td>";
                    ////No Use--var markup = "<td  style='width:5% !important' ><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right' >" + objCells.item(1).innerHTML + "</td><td>" + parseFloat(qty_).toFixed(2) + "</td><td>" + parseFloat(objCells.item(3).innerHTML).toFixed(2) + "</td><td style='display:none' >" + parseFloat(Price).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td><input type='text' class='form-control'  value='" + Baseprice + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + objCells.item(7).innerHTML + "</td><td style='display:none'>" + objCells.item(8).innerHTML + "</td><td>" + Baseprice + "</td>";
                    //$('#tr_' + ItemId).html(markup);

                    //duplicateValForScheemItem(ItemId, qty_, 0, objCells.item(12).innerHTML);
                    $('#itemIdhiddenCheck').val('1');
                    GradCaluclation();
                    ReassignSubVals(ItemId);
                }
            }
            else {
                //alert("Please Enter Quantity");
            }
        }
        else {
            //alert("Please Enter Quantity");
        }
    }
}
function UpdateDiscSubChange() {

    var ItemId = $('#txthdnItemId').val();
    var Disc = $('#txtDiscount').val();
    var myTab = document.getElementById('itemCarttable');
    var m_ = ""; var Price = "0";
    for (i = 0; i < myTab.rows.length; i++) {
        var objCells = myTab.rows.item(i).cells;
        if (ItemId == objCells.item(5).innerHTML) {
            var qty_ = parseFloat(objCells.item(2).innerHTML).toFixed(3);
            //var Baseprice = $('#td_' + ItemId).val();
            var Baseprice = floorFigure($('#td_' + ItemId).val(), 4);
            Price = parseFloat(Baseprice) * parseFloat(qty_);
            //Price = parseFloat(objCells.item(4).innerHTML);
            if ((Disc) != "") {
                if (parseFloat(Disc) <= 100) {
                    //var Baseprice = $('#td_' + ItemId).val();
                    var Baseprice = floorFigure($('#td_' + ItemId).val(), 2);
                    var Dis1 = parseFloat(Price) - ((parseFloat(Price) * (parseFloat(Disc))) / 100);
                    var markup = "<td  style='width:5% !important' ><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right' >" + objCells.item(1).innerHTML + "</td><td>" + parseFloat(qty_).toFixed(2) + "</td><td>" + parseFloat(Disc).toFixed(2) + "</td>                      <td style='display:none'>" + parseFloat(Dis1).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td><input type='text' class='form-control'  value='" + Baseprice + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + objCells.item(7).innerHTML + "</td><td style='display:none'>" + objCells.item(8).innerHTML + "</td><td>" + Baseprice + "</td><td style='display:none'>" + objCells.item(10).innerText + "</td><td style='display:none'>" + objCells.item(11).innerText + "</td><td style='display:none'>" + objCells.item(12).innerText + "</td><td>" + objCells.item(13).innerHTML + "</td>";
                    //var markup = "<td  style='width:5% !important' ><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right' >" + objCells.item(1).innerHTML + "</td><td>" + parseFloat(qty_).toFixed(2) + "</td><td>" + parseFloat(objCells.item(3).innerHTML).toFixed(2) + "</td><td style='display:none'>" + parseFloat(Price).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td><input type='text' class='form-control'  value='" + Baseprice + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + objCells.item(7).innerHTML + "</td><td style='display:none'>" + objCells.item(8).innerHTML + "</td><td>" + Baseprice + "</td>";
                    $('#tr_' + ItemId).html(markup);
                    $('#itemIdhiddenCheck').val('1');
                    GradCaluclation();
                }
                else {
                    var $textAndPic = $('<div></div>');
                    $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                    //$textAndPic.append('<div class="alert-message">Discount Must be in 1 t0 100 (%)</div>');
                    $textAndPic.append('<div class="alert-message">' + $('#hdnDiscVal').val() + '</div>');
                    BootstrapDialog.alert($textAndPic);
                    $('#txtDiscount').val('');
                    //var Baseprice = $('#td_' + ItemId).val();
                    var Baseprice = floorFigure(parseFloat($('#td_' + ItemId).val()), 2);
                    Disc = 0;
                    var markup = "<td  style='width:5% !important' ><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right' >" + objCells.item(1).innerHTML + "</td><td>" + parseFloat(qty_).toFixed(2) + "</td><td>" + parseFloat(Disc).toFixed(2) + "</td><td style='display:none'>" + parseFloat(parseFloat(Baseprice) * parseFloat(qty_)).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td><input type='text' class='form-control'  value='" + Baseprice + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + objCells.item(7).innerHTML + "</td><td style='display:none'>" + objCells.item(8).innerHTML + "</td><td>" + Baseprice + "</td><td style='display:none'>" + objCells.item(10).innerText + "</td><td style='display:none'>" + objCells.item(11).innerText + "</td><td style='display:none'>" + objCells.item(12).innerText + "</td><td>" + objCells.item(13).innerHTML + "</td>";
                    //var markup = "<td  style='width:5% !important' ><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right' >" + objCells.item(1).innerHTML + "</td><td>" + parseFloat(qty_).toFixed(2) + "</td><td>" + parseFloat(Disc).toFixed(2) + "</td><td style='display:none'>" + parseFloat(Dis1).toFixed(2) + "</td>                                    <td style='display:none'>" + ItemId + "</td><td><input type='text' class='form-control'  value='" + Baseprice + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + objCells.item(7).innerHTML + "</td><td style='display:none'>" + objCells.item(8).innerHTML + "</td><td>" + Baseprice + "</td>";
                    $('#tr_' + ItemId).html(markup);
                    $('#itemIdhiddenCheck').val('1');
                    GradCaluclation();

                }
                ReassignSubVals(ItemId);

            }
            //else {
            //    $('#txtDiscount').val('');
            //    var Baseprice = $('#td_' + ItemId).val();
            //    Disc = 0;
            //    var markup = "<td  style='width:5% !important' ><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right' >" + objCells.item(1).innerHTML + "</td><td>" + parseFloat(qty_).toFixed(2) + "</td><td>" + parseFloat(Disc).toFixed(2) + "</td><td>" + parseFloat(parseFloat(Baseprice) * parseFloat(qty_)).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td><input type='text' class='form-control'  value='" + Baseprice + "' id='" + 'td_' + ItemId + "' style='display:none'></td>";
            //    $('#tr_' + ItemId).html(markup);
            //    $('#itemIdhiddenCheck').val('1');
            //    GradCaluclation();
            //}
            break;
        }
    }
}

function UpdatePriceSubChange() {
    var ItemId = $('#txthdnItemId').val();
    var Baseprice = $('#txtPrice').val();
    var myTab = document.getElementById('itemCarttable');
    var m_ = ""; var Price = "0";
    for (i = 0; i < myTab.rows.length; i++) {
        var objCells = myTab.rows.item(i).cells;
        if (ItemId == objCells.item(5).innerHTML) {
            var qty_ = parseFloat(objCells.item(2).innerHTML).toFixed(3);
            Price = parseFloat(Baseprice) * parseFloat(qty_);
            //
            if (Baseprice == "") {
                Baseprice = "0";
                Price = "0";
            }
            if ((Baseprice) != "") {
                if (Baseprice == "0") {
                    $('#txtPrice').val("0.00")
                }
                var Dis1 = parseFloat(Price) - ((parseFloat(Price) * (parseFloat(objCells.item(3).innerHTML))) / 100);
                //var markup = "<td  style='width:5% !important' ><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right' >" + objCells.item(1).innerHTML + "</td>  <td>" + parseFloat(qty_).toFixed(2) + "</td><td>" + parseFloat(objCells.item(3).innerHTML).toFixed(2) + "</td><td style='display:none'>" + parseFloat(Dis1).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td><input type='text' class='form-control'  value='" + Baseprice + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + objCells.item(7).innerHTML + "</td><td style='display:none'>" + objCells.item(8).innerHTML + "</td><td>" + Baseprice + "</td><td style='display:none'>" + objCells.item(10).innerText + "</td><td style='display:none'>" + objCells.item(11).innerText + "</td><td style='display:none'>" + objCells.item(12).innerText + "</td>";
                //$('#tr_' + ItemId).html(markup);
                onSubDataPrice(objCells.item(1).innerHTML, objCells.item(5).innerHTML, objCells.item(1).innerHTML, $('#txtPrice').val(), objCells.item(3).innerHTML, objCells.item(7).innerHTML, objCells.item(8).innerHTML, objCells.item(10).innerHTML, objCells.item(11).innerHTML, document.getElementById("txtQuantity").value, objCells.item(13).innerHTML);
                //var markup = "<td  style='width:5% !important' ><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right' >" + objCells.item(1).innerHTML + "</td><td>" + parseFloat(qty_).toFixed(2) + "</td><td>" + parseFloat(objCells.item(3).innerHTML).toFixed(2) + "</td><td style='display:none' >" + parseFloat(Price).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td><input type='text' class='form-control'  value='" + Baseprice + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + objCells.item(7).innerHTML + "</td><td style='display:none'>" + objCells.item(8).innerHTML + "</td><td>" + Baseprice + "</td><td style='display:none'>" + objCells.item(10).innerText + "</td><td style='display:none'>" + objCells.item(11).innerText + "</td><td style='display:none'>" + objCells.item(12).innerText + "</td>";
                ////No Use--var markup = "<td  style='width:5% !important' ><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right' >" + objCells.item(1).innerHTML + "</td><td>" + parseFloat(qty_).toFixed(2) + "</td><td>" + parseFloat(objCells.item(3).innerHTML).toFixed(2) + "</td><td style='display:none' >" + parseFloat(Price).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td><input type='text' class='form-control'  value='" + Baseprice + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + objCells.item(7).innerHTML + "</td><td style='display:none'>" + objCells.item(8).innerHTML + "</td><td>" + Baseprice + "</td>";
                //$('#tr_' + ItemId).html(markup);

                //duplicateValForScheemItem(ItemId, qty_, 0, objCells.item(12).innerHTML);
                $('#itemIdhiddenCheck').val('1');
                GradCaluclation();
                ReassignSubVals(ItemId);
                //    $('#txtPrice').val("");
                $("#txtPrice").attr("disabled", "disabled");
            }
            else {

            }
            break;
        }
    }
}
function onSubDataPrice(code, ItemId, Name, Price, Disc, UomID, UomCode, mapCode, Profile, qty_, img) {

    var passQTY = 1; var passPrice = 0; var baseCur_;
    var myTabChkQty = document.getElementById('itemCarttable');
    for (i = 0; i < myTabChkQty.rows.length; i++) {
        var objCells = myTabChkQty.rows.item(i).cells;
        if (ItemId == objCells.item(5).innerHTML) {
            passQTY = passQTY + parseFloat(objCells.item(2).innerHTML);
            passPrice = passPrice + parseFloat(objCells.item(9).innerHTML);
        }
    }
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        SubOrg_Id = hashSplit[1];
        baseCur_ = hashSplit[16];
    }

    var SubOrg = {
    };
    SubOrg.SubOrgID = $('#hdnBranchId').val();
    //  SubOrg.VD = vd_;
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetItemSchema,
        data: "ItemId=" + ItemId + "&SubOrg=" + SubOrg_Id + "&QTY=" + qty_ + "&PRICE=" + Price + "&BaseCur=" + baseCur_ + "&VD=" + VD1(),
        success: function (resp) {

            var mainData_ = resp;
            resp = resp[0].split('~');
            if (resp[0] != 'NODATA') {

                if (resp[0] == 'ADDEXTRAITEM') {
                    var strOptions1 = $('#txtCusName').val();
                    var strOptions = $('#hdn_CusId').val();
                    if ($('#txthdnRemoveStatus').val() == '1') {
                        removeDraft(strOptions);
                        $('#txthdnRemoveStatus').val('0');
                    }
                    if (strOptions == '') {
                        strOptions = GenarateGustId() + '-01';
                        strOptions1 = GenarateGustName();
                    }

                    var valItemCheck_ = $('#itemIdhiddenCheck').val();
                    DuplicatePRICEMODE(ItemId, Price, Disc, mapCode, Profile, mainData_, qty_);
                    valItemCheck_ = $('#itemIdhiddenCheck').val();
                    if (valItemCheck_ == '0') {
                        var markup = "<tr id=" + 'tr_' + ItemId + " onclick=ActiveRowget(\'" + ItemId + "\',\'" + Price + "\','1');><td style='width:5% !important'><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right'>" + Name + "</td><td>1.00</td><td>" + parseFloat(Disc).toFixed(2) + "</td><td style='display:none' >" + parseFloat(Price).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td><input type='text' class='form-control'  value='" + Price + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + UomID + "</td><td style='display:none'>" + UomCode + "</td><td>" + parseFloat(Price).toFixed(2) + "</td><td style='display:none'>" + mapCode + "</td><td style='display:none'>" + Profile + "</td><td  style='display:none'>" + mainData_.length + "</td></tr>";
                        $("#itemCarttable tbody").append(markup);
                        if (mainData_.length > 0) {
                            for (var k_ = 0; k_ < mainData_.length; k_++) {
                                var resp_ = mainData_[k_].split('~');
                                var id_ = 'tr_' + ItemId + "_" + k_;
                                markup = "<tr  id=" + id_ + " onclick=ActiveRowget(\'" + ItemId + "\',\'" + Price + "\','1');><td  style='width:5% !important;background:#ddd;color:black;'></td><td style='background:#ddd;color:black;' align='right' > " + resp_[2] + "</td><td style='background:#ddd;color:black;' >" + parseFloat(resp_[3]).toFixed(2) + "</td><td style='background:#ddd;color:black;' >" + parseFloat(resp_[10]).toFixed(2) + "</td><td style='display:none;background:#ddd;color:black;'>" + resp_[11] + "</td><td style='display:none;background:#ddd;color:black;'>" + resp_[1] + "~" + "</td><td style='background:#ddd;color:black;' ><input type='text' class='form-control'  value='" + resp_[4] + "' id='" + 'td_' + resp_[1] + "' style='display:none;background:#ddd;color:black;'></td><td  style='display:none;background:#ddd;color:black;'>" + resp_[7] + "</td><td  style='display:none;background:#ddd;color:black;'>" + resp_[5] + "</td><td style='background:#ddd;color:black;' >" + parseFloat(resp_[4]).toFixed(2) + "</td><td style='background:#ddd;color:black;' ></td><td  style='background:#ddd;color:black;'></td><td  style='display:none'>" + resp_[3] + "</td></tr>"

                                $("#itemCarttable tbody").append(markup);
                            }
                        }

                        POLEDisplay(Name + ':' + Price);
                    }


                    GradCaluclation();
                    ActiveRowget(ItemId)
                    $('#txtItemScan').val('');
                    PriceSubChange();
                    $('#txtPrice').val(Price);
                }
            }
            else {

                var strOptions1 = $('#txtCusName').val();
                var strOptions = $('#hdn_CusId').val();
                if ($('#txthdnRemoveStatus').val() == '1') {
                    removeDraft(strOptions);
                    $('#txthdnRemoveStatus').val('0');
                }
                if (strOptions == '') {
                    strOptions = GenarateGustId() + '-01';
                    strOptions1 = GenarateGustName();
                }

                var valItemCheck_ = $('#itemIdhiddenCheck').val();
                if (mainData_.length > 0) {
                    var resp_ = mainData_[0].split('~');
                    if (resp_.length >= 2) {
                        var Mresp_ = resp_[1].split('$');
                        if (Mresp_.length > 1) {
                            Disc = Mresp_[0];

                        }
                    }
                }
                //Duplicate(ItemId, Price, Disc, mapCode, Profile, mainData_, qty_);
                DuplicatePRICEMODE(ItemId, Price, Disc, mapCode, Profile, '', qty_, img);
                valItemCheck_ = $('#itemIdhiddenCheck').val();
                if (valItemCheck_ == '0') {
                    var imgHost_ = img;
                    if ((img == "") || img == undefined) {
                        imgHost_ = "";
                    }
                    var markup = "<tr id=" + 'tr_' + ItemId + " onclick=ActiveRowget(\'" + ItemId + "\',\'" + Price + "\','1');><td style='width:5% !important'><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right'>" + Name + "</td><td>1.00</td><td>" + parseFloat(Disc).toFixed(2) + "</td><td style='display:none' >" + parseFloat(Price).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td><input type='text' class='form-control'  value='" + Price + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + UomID + "</td><td style='display:none'>" + UomCode + "</td><td>" + floorFigure(parseFloat(Price), 2) + "</td><td style='display:none'>" + mapCode + "</td><td style='display:none'>" + Profile + "</td><td style='display:none'>0</td><td>" + imgHost_ + "</td></tr>";
                    $("#itemCarttable tbody").append(markup);
                    POLEDisplay(Name + ':' + Price);
                }


                GradCaluclation();
                ActiveRowget(ItemId)
                $('#txtItemScan').val('');
                PriceSubChange();
                $('#txtPrice').val(Price);
            }

        },
        error: function (e) {

        }
    });



}

function DuplicatePRICEMODE(ItemId, Price_, Disc_, MapCode_, Profile_, mainData_, QTY_, img) {

    var myTab = document.getElementById('itemCarttable');
    var m_ = "";
    $('#itemIdhiddenCheck').val('0');
    // LOOP THROUGH EACH ROW OF THE TABLE AFTER HEADER.
    for (i = 0; i < myTab.rows.length; i++) {
        // GET THE CELLS COLLECTION OF THE CURRENT ROW.
        var objCells = myTab.rows.item(i).cells;
        // LOOP THROUGH EACH CELL OF THE CURENT ROW TO READ CELL VALUES.
        if (ItemId == objCells.item(5).innerHTML) {
            var qty_ = parseFloat(QTY_);
            var Price = Price_;
            var Baseprice = floorFigure(parseFloat(Price), 2);
            Price = parseFloat(Baseprice) * parseFloat(qty_);
            if (objCells.item(3).innerHTML != "") {
                if (parseFloat(objCells.item(3).innerHTML) > 0) {
                    Price = parseFloat(Price) - ((parseFloat(Price) * (parseFloat(objCells.item(3).innerHTML))) / 100);
                }
                else if (parseFloat(Disc_) > 0) {
                    Price = parseFloat(Price) - ((parseFloat(Price) * (parseFloat(Disc_))) / 100);
                }
            }
            var imgHost_ = img;
            if ((img == "") || img == undefined) {
                imgHost_ = "";
            }
            try {

                var markup = "<tr id=" + 'tr_' + ItemId + " onclick=ActiveRowget(\'" + ItemId + "\',\'" + Price_ + "\','1');><td  style='width:5% !important' ><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right' >" + objCells.item(1).innerHTML + "</td><td>" + parseFloat(qty_).toFixed(2) + "</td><td>" + parseFloat(objCells.item(3).innerHTML).toFixed(2) + "</td><td style='display:none' >" + parseFloat(Price).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td><input type='text' class='form-control'  value='" + Baseprice + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + objCells.item(7).innerHTML + "</td><td style='display:none'>" + objCells.item(8).innerHTML + "</td><td>" + Baseprice + "</td><td style='display:none'>" + MapCode_ + "</td><td style='display:none'>" + Profile_ + "</td><td style='display:none'>" + mainData_.length + "</td><td>" + imgHost_ + "</td></tr>";
            }
            catch (r) {
                var markup = "<tr id=" + 'tr_' + ItemId + " onclick=ActiveRowget(\'" + ItemId + "\',\'" + Price_ + "\','1');><td  style='width:5% !important' ><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right' >" + objCells.item(1).innerHTML + "</td><td>" + parseFloat(qty_).toFixed(2) + "</td><td>" + parseFloat(objCells.item(3).innerHTML).toFixed(2) + "</td><td style='display:none' >" + parseFloat(Price).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td><input type='text' class='form-control'  value='" + Baseprice + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + objCells.item(7).innerHTML + "</td><td style='display:none'>" + objCells.item(8).innerHTML + "</td><td>" + Baseprice + "</td><td style='display:none'>" + MapCode_ + "</td><td style='display:none'>" + Profile_ + "</td><td style='display:none'>" + objCells.item(12).innerHTML + "</td><td>" + imgHost_ + "</td></tr>";
            }
            //var markup = "<td style='width:5% !important'><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right'>" + Name + "</td>                          <td>1.00</td>                               <td>" + parseFloat(Disc).toFixed(2) + "</td>                     <td style='display:none' >" + parseFloat(Price).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td><input type='text' class='form-control'  value='" + Price + "' id='" + 'td_' + ItemId + "' style='display:none'></td>                      <td style='display:none'>" + UomID + "</td>                     <td style='display:none'>" + UomCode + "</td>                   <td>" + Price + "</td>";
            var mIdSubLength = objCells.item(12).innerHTML;
            if (parseFloat(mIdSubLength) >= 0) {
                for (var m1_ = 1; m1_ < mIdSubLength + 1; m1_++) {
                    var idC_ = '#tr_' + ItemId + "_" + (m1_ - 1);
                    $(idC_).remove();
                }
            }
            var idC_ = '#tr_' + ItemId;
            $(idC_).remove();


            $("#itemCarttable tbody").append(markup);
            try {
                //if (parseFloat(objCells.item(12).innerHTML) > 0) {
                if (mainData_.length > 0) {
                    for (var k_ = 0; k_ < mainData_.length; k_++) {
                        var resp_ = mainData_[k_].split('~');
                        var id_ = 'tr_' + ItemId + "_" + k_;
                        var imgHost_ = img;
                        if ((img == "") || img == undefined) {
                            imgHost_ = "";
                        }
                        markup = "<tr  id=" + id_ + " onclick=ActiveRowget(\'" + ItemId + "\',\'" + Price + "\','1');><td  style='width:5% !important;background:#ddd;color:black;'></td><td style='background:#ddd;color:black;' align='right' > " + resp_[2] + "</td><td style='background:#ddd;color:black;' >" + parseFloat(resp_[3]).toFixed(2) + "</td><td style='background:#ddd;color:black;' >" + parseFloat(resp_[10]).toFixed(2) + "</td><td style='display:none;background:#ddd;color:black;'>" + resp_[4] + "</td><td style='display:none;background:#ddd;color:black;'>" + resp_[1] + "~" + "</td><td style='background:#ddd;color:black;' ><input type='text' class='form-control'  value='" + resp_[4] + "' id='" + 'td_' + resp_[1] + "' style='display:none;background:#ddd;color:black;'></td><td  style='display:none;background:#ddd;color:black;'>" + resp_[7] + "</td><td  style='display:none;background:#ddd;color:black;'>" + resp_[5] + "</td><td style='background:#ddd;color:black;' >" + parseFloat(resp_[4]).toFixed(2) + "</td><td style='background:#ddd;color:black;display:none;' >" + MapCode_ + "</td><td  style='background:#ddd;color:black;'></td><td  style='display:none'>" + resp_[3] + "</td><td>" + imgHost_ + "</td></tr>"
                        $("#itemCarttable tbody").append(markup);
                    }
                }
                //}
            }
            catch (r) {

            }

            //duplicateValForScheemItem(ItemId, qty_, 0, objCells.item(12).innerHTML);
            $('#itemIdhiddenCheck').val('1');
            ClearFileds();
            POLEDisplay(objCells.item(1).innerHTML + ':' + Price);

            $('#txtPrice').val(Price_);
        }
    }
}
function UpdatePriceSubChange1() {
    var ItemId = $('#txthdnItemId').val();
    var Baseprice = $('#txtPrice').val();
    var myTab = document.getElementById('itemCarttable');
    var m_ = ""; var Price = "0";
    for (i = 0; i < myTab.rows.length; i++) {
        var objCells = myTab.rows.item(i).cells;
        if (ItemId == objCells.item(5).innerHTML) {
            var qty_ = parseFloat(objCells.item(2).innerHTML).toFixed(3);
            Price = parseFloat(Baseprice) * parseFloat(qty_);
            //Price = parseFloat(objCells.item(4).innerHTML);
            if ((Baseprice) != "") {
                //  if (parseFloat(Baseprice)) {
                var Dis1 = parseFloat(Price) - ((parseFloat(Price) * (parseFloat(objCells.item(3).innerHTML))) / 100);
                var markup = "<td  style='width:5% !important' ><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right' >" + objCells.item(1).innerHTML + "</td>  <td>" + parseFloat(qty_).toFixed(2) + "</td><td>" + parseFloat(objCells.item(3).innerHTML).toFixed(2) + "</td><td style='display:none'>" + parseFloat(Dis1).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td><input type='text' class='form-control'  value='" + Baseprice + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + objCells.item(7).innerHTML + "</td><td style='display:none'>" + objCells.item(8).innerHTML + "</td><td>" + Baseprice + "</td><td style='display:none'>" + objCells.item(10).innerText + "</td><td style='display:none'>" + objCells.item(11).innerText + "</td><td style='display:none'>" + objCells.item(12).innerText + "</td>";
                //var markup = "<td  style='width:5% !important' ><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right' >" + objCells.item(1).innerHTML + "</td><td>" + parseFloat(qty_).toFixed(2) + "</td><td>" + parseFloat(objCells.item(3).innerHTML).toFixed(2) + "</td><td style='display:none'>" + parseFloat(Price).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td><input type='text' class='form-control'  value='" + Baseprice + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + objCells.item(7).innerHTML + "</td><td style='display:none'>" + objCells.item(8).innerHTML + "</td><td>" + Baseprice + "</td>";
                $('#tr_' + ItemId).html(markup);
                $('#itemIdhiddenCheck').val('1');
                GradCaluclation();

                ReassignSubVals(ItemId);
                //}
                //else {

                //    //alert("Please Enter Price");
                //}
            }
            else {
                //alert("Please Enter Price");
            }
            break;
        }
    }
}

function newCart() {
    $("#tbl_tenderDyms tr").remove();
    $('#txtUOM').val('');
    $('#txtQuantity').val('');
    $('#txtDiscount').val('');
    $('#txtPrice').val('');
    $('#txtAmount').val('');
    //var e = document.getElementById("dpCusData");
    //var strOptions = e.options[e.selectedIndex].value;
    //var strOptions1 = e.options[e.selectedIndex].text;
    var strOptions = $('#hdn_CusId').val();
    var strOptions1 = $('#txtCusName').val();
    //if (strOptions == "") {

    strOptions = GenarateGustId();
    strOptions1 = GenarateGustName();
    //}
    removeDraft(strOptions);
    if (strOptions != '') {
        var myTab = document.getElementById('itemCarttable');
        var MainVal_ = ""; var cusId_ = ""; var cusName_ = ""; var ItemId_ = ""; var ItemName_ = ""; var BasePrice_ = ""; var Disc_ = ""; var Amount_ = ""; var Quantity_ = "";
        var totalAmout_ = "0.0000";
        var itemCheck = "";
        if (myTab.rows.length > 0) {
            for (i = 0; i < myTab.rows.length; i++) {

                var objCells = myTab.rows.item(i).cells;
                cusId_ = strOptions;
                cusName_ = strOptions1;
                ItemId_ = objCells.item(5).innerHTML;
                //ItemName_ = objCells.item(1).innerHTML;
                //BasePrice_ = objCells.item(9).innerHTML;
                //Disc_ = objCells.item(3).innerHTML;
                Amount_ = objCells.item(4).innerHTML;
                //Quantity_ = objCells.item(2).innerHTML;

                totalAmout_ = parseFloat(totalAmout_) + parseFloat(Amount_);
                itemCheck = ItemId_;
                //if (MainVal_ == "") {
                //    MainVal_ = cusId_ + '~' + cusName_ + '~' + objCells.item(1).innerHTML + '~' + objCells.item(2).innerHTML + '~' + objCells.item(3).innerHTML + '~' + objCells.item(4).innerHTML + '~' + objCells.item(5).innerHTML + '~' + objCells.item(6).childNodes["0"].value + '~' + objCells.item(7).innerHTML + '~' + objCells.item(8).innerHTML + '~' + objCells.item(9).innerHTML + '~' + objCells.item(10).innerHTML + '~' + objCells.item(11).innerHTML + '~' + objCells.item(12).innerHTML + '~' + myTab.rows[i].id + '$';
                //}
                //else {
                //    MainVal_ = MainVal_ + cusId_ + '~' + cusName_ + '~' + objCells.item(1).innerHTML + '~' + objCells.item(2).innerHTML + '~' + objCells.item(3).innerHTML + '~' + objCells.item(4).innerHTML + '~' + objCells.item(5).innerHTML + '~' + objCells.item(6).childNodes["0"].value + '~' + objCells.item(7).innerHTML + '~' + objCells.item(8).innerHTML + '~' + objCells.item(9).innerHTML + '~' + objCells.item(10).innerHTML + '~' + objCells.item(11).innerHTML + '~' + objCells.item(12).innerHTML + '~' + myTab.rows[i].id + '$';
                //}
            }
            if (itemCheck != "") {
                window.localStorage.setItem(cusId_, $('#itemCarttable')[0].innerHTML);
                window.localStorage.setItem(cusId_ + '#0', cusId_ + '~' + cusName_);
                var HeadeSub_ = cusId_ + '~' + cusName_ + '~' + totalAmout_ + '$';
                if (window.localStorage.getItem("DraftItemsHeader") == null) {
                    window.localStorage.setItem("DraftItemsHeader", HeadeSub_);
                }
                else {
                    var HeaderMain = window.localStorage.getItem("DraftItemsHeader") + HeadeSub_;
                    window.localStorage.setItem("DraftItemsHeader", HeaderMain);
                }
                //window.localStorage.removeItem("DraftItems");
                //var mm = MainVal_.split('$')
                //alert(window.localStorage.getItem("DraftItemsHeader"));
                //$("#dpCusData")[0].selectedIndex = 0;
                $('#hdn_CusId').val('');
                $('#txtCusName').val('');
                $("#itemCarttable tr").remove();
                $('#grdandTotal').text('0.00');

            }

        }
        else {

        }
    }
    CartItemsPULL();
    var elem = document.getElementById("toggle-field");
    var elemDraft = document.getElementById("toggle-fieldSub");
    //elem.style.display = "block";
    //elemDraft.style.display = "none";
    $('.content-panel').css('position', 'absolute');
    $('.content-panel').css('width', '50%');
    $('.content-panel').css('right', '0px');
    $("#hdnPwdCheckStatus").val('0');
    POLEDisplay("Counter Ready");
    ClearFileds();
    GetCounterTypeTenderLocalData();
    $('#lbl_TpartTotal')[0].innerText = "0.00";
    $("#tbl_tenderDyms tr").remove();
    $("#tabletrpopbind tbody").remove();
    $('#txtItemspp').val('0');
    $('#txtQuantityPP').val('0');
    $('#txtPricepp').val('0');
    $('#txtDiscountpp').val('0');
    $('#txtTaxPP').val('0');
    $('#txtAmountpp').val('0');
    $('#txtTendertrans').val('0');
    $('#lblTenderConvertAmt').text('0');
    $('#lbl_TTpartTotal')[0].innerText = "0.00";


}


function ToggelCartChange() {
    //var e = document.getElementById("dpCusData");
    //var strOptions = e.options[e.selectedIndex].value;
    //var strOptions1 = e.options[e.selectedIndex].text;
    var strOptions = $('#hdn_CusId').val();
    var strOptions1 = $('#txtCusName').val();
    //$('#txtCusName').val('');
    if (strOptions != '0') {
        var myTab = document.getElementById('itemCarttable');
        var MainVal_ = ""; var cusId_ = ""; var cusName_ = ""; var ItemId_ = ""; var ItemName_ = ""; var BasePrice_ = ""; var Disc_ = ""; var Amount_ = ""; var Quantity_ = "";
        var totalAmout_ = "0.0000";
        var itemCheck = "";
        for (i = 0; i < myTab.rows.length; i++) {
            var objCells = myTab.rows.item(i).cells;
            cusId_ = strOptions;
            cusName_ = strOptions1;
            ItemId_ = objCells.item(5).innerHTML;
            ItemName_ = objCells.item(1).innerHTML;
            BasePrice_ = $('#td_' + objCells.item(5).innerHTML).val();;
            Disc_ = objCells.item(3).innerHTML;
            Amount_ = objCells.item(4).innerHTML;
            Quantity_ = objCells.item(2).innerHTML;
            totalAmout_ = parseFloat(totalAmout_) + parseFloat(Amount_);
            itemCheck = ItemId_;
            if (MainVal_ == "") {
                MainVal_ = cusId_ + '~' + cusName_ + '~' + ItemId_ + '~' + ItemName_ + '~' + BasePrice_ + '~' + Disc_ + '~' + Amount_ + '~' + Quantity_ + '$';
            }
            else {
                MainVal_ = MainVal_ + cusId_ + '~' + cusName_ + '~' + ItemId_ + '~' + ItemName_ + '~' + BasePrice_ + '~' + Disc_ + '~' + Amount_ + '~' + Quantity_ + '$';
            }
        }
        if (itemCheck != "") {
            window.localStorage.setItem(cusId_, MainVal_);
            var HeadeSub_ = cusId_ + '~' + cusName_ + '~' + totalAmout_ + '$';
            if (window.localStorage.getItem("DraftItemsHeader") == null) {
                window.localStorage.setItem("DraftItemsHeader", HeadeSub_);
            }
            else {
                var HeaderMain = window.localStorage.getItem("DraftItemsHeader") + HeadeSub_;
                window.localStorage.setItem("DraftItemsHeader", HeaderMain);
            }
            //window.localStorage.removeItem("DraftItems");
            //var mm = MainVal_.split('$')
            //alert(window.localStorage.getItem("DraftItemsHeader"));
            //$("#dpCusData")[0].selectedIndex = 0;

            $('#hdn_CusId').val('');
            $('#txtCusName').val('');
            $("#itemCarttable tr").remove();

        }
    }


}

function toggleDiv() {


    //var elem = document.getElementById("toggle-field");
    //var elemDraft = document.getElementById("toggle-fieldSub");
    //var hideDraft = elemDraft.style.display == "none";
    //var hide = elem.style.display == "none";
    //if (hide) {
    //elem.style.display = "block";
    //elemDraft.style.display = "none";
    $("#itemCarttable tr").remove();
    //$("#dpCusData")[0].selectedIndex = 0;
    $('#hdn_CusId').val('');
    $('#txtCusName').val('');
    //}
    //else {
    //ToggelCartChange();
    //$("#dpCusData")[0].selectedIndex = 0;
    $('#hdn_CusId').val('');
    $('#txtCusName').val('');
    //elem.style.display = "none";
    //elemDraft.style.display = "block";
    CartItemsPULL();
    //}
    $('.content-panel').css('position', 'absolute');
    $('.content-panel').css('width', '50%');
    $('.content-panel').css('right', '0px');
    $('#myModalDrafts').modal('show');
}
function tenderPOP() {
    $('#myModalTenders').modal('show');
}
function CartItemsPULL() {
    if (window.localStorage.getItem("DraftItemsHeader") != null) {
        var cusHeader = window.localStorage.getItem("DraftItemsHeader").split('$');
        $("#itemCustable tbody tr").remove();
        if (cusHeader.length > 0) {
            for (var i = 0; i < cusHeader.length; i++) {
                var cusHeader_ = cusHeader[i].split('~');

                if (cusHeader_[0] != "") {
                    var markup = "<tr id=" + 'tr_' + cusHeader_[0] + " onclick=ActiveRowgetCus(\'" + cusHeader_[0] + "\');><td style='width:5% !important'><img src='images/delete.png' onclick='deleteRowCus(this)'/></td><td class='text-align:right'>" + cusHeader_[1] + "</td><td>" + parseFloat(cusHeader_[2]).toFixed(2) + "</td><td style='display:none' >" + cusHeader_[0] + "</td></tr>";
                    $("#itemCustable tbody").append(markup);
                }
            }
        }
    }
}


function ddpActiveRowgetCus(CustId) {
    valueAppendtoPOPUp();
    $("#tbl_tenderDyms tr").remove();
    if (window.localStorage.getItem(CustId) != null) {
        var CusDraftData = window.localStorage.getItem(CustId).split('$');
        if (CusDraftData[0] != "") {
            if (CusDraftData.length > 0) {
                for (var m = 0; m < CusDraftData.length; m++) {

                    var cusItems_ = CusDraftData[m].split('~');
                    if (cusItems_[0] != "") {
                        var markup = "<tr id=" + 'tr_' + cusItems_[2] + " onclick=ActiveRowget(\'" + cusItems_[2] + "\',\'" + cusItems_[4] + "\','1');><td style='width:5% !important'><img src='images/delete.png' onclick='deleteRow(this)'/></td><td class='text-align:right'>" + cusItems_[3] + "</td><td>" + parseFloat(cusItems_[7]).toFixed(2) + "</td><td>" + parseFloat(cusItems_[5]).toFixed(2) + "</td><td>" + parseFloat(cusItems_[6]).toFixed(2) + "</td><td style='display:none'>" + cusItems_[2] + "</td><td><input type='text' class='form-control'  value='" + cusItems_[4] + "' id='" + 'td_' + cusItems_[2] + "' style='display:none'></td></tr>";

                        $("#itemCarttable tbody").append(markup);
                    }
                    else {
                        //$("#dpCusData")[0].selectedIndex = 0;
                    }
                }
                $('#txthdnRemoveStatus').val('1');
                //removeDraft(CustId);
                GradCaluclation();
                ClearFileds();
            }
        }
        else {
            $("#itemCarttable tr").remove();
            GradCaluclation();
            ClearFileds();
        }
    }


}
function ActiveRowgetCus(CustId) {
    $("#tbl_tenderDyms tr").remove();
    $('#myModalDrafts').modal('hide');
    //$('#myModalDrafts').hide();
    if ($('#txthdnCusRemoveStatus').val() == '0') {

        $('#itemCarttable')[0].innerHTML = '';
        var CusDraftData = window.localStorage.getItem(CustId);
        //$("#dpCusData option[value=" + CustId + "]").attr("selected", "selected");
        //$("#itemCarttable tr").remove();
        //$("select#dpCusData option")
        //.each(function () { this.selected = (this.value == CustId); });
        //$('#hdn_CusId').val(CustId);
        //$('#txtCusName').val('');
        if (CusDraftData[0] != "") {
            if (CusDraftData.length > 0) {
                //for (var m = 0; m < CusDraftData.length; m++) {
                //    var cusItems_ = CusDraftData[m].split('~');
                //    if (cusItems_[0] != "") {
                //        var markup = "<tr id=" + 'tr_' + cusItems_[14] + " onclick=ActiveRowget(\'" + cusItems_[6] + "\',\'" + cusItems_[7] + "\','1');><td style='width:5% !important'><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right'>" + cusItems_[2] + "</td><td>" + cusItems_[3] + "</td><td>" + parseFloat(cusItems_[4]).toFixed(2) + "</td><td style='display:none' >" + parseFloat(cusItems_[5]).toFixed(2) + "</td><td style='display:none'>" + cusItems_[6] + "</td><td><input type='text' class='form-control'  value='" + cusItems_[7] + "' id='" + 'td_' + cusItems_[6] + "' style='display:none'></td><td style='display:none'>" + cusItems_[8] + "</td><td style='display:none'>" + cusItems_[9] + "</td><td>" + parseFloat(cusItems_[10]).toFixed(2) + "</td><td style='display:none'>" + cusItems_[11] + "</td><td style='display:none'>" + cusItems_[12] + "</td><td  style='display:none'>" + cusItems_[13] + "</td></tr>";
                //        //var markup = "<tr id=" + 'tr_' + cusItems_[14] + " onclick=ActiveRowget(\'" + cusItems_[6] + "\',\'" + cusItems_[7] + "\','1');><td style='width:5% !important'><img src='images/delete.png' onclick='deleteRow(this)'/></td>   <td align='right'>" + cusItems_[2] + "</td>              <td>" + parseFloat(cusItems_[3]).toFixed(2) + "</td><td>" + parseFloat(cusItems_[4]).toFixed(2) + "</td>              <td style='display:none'>" + parseFloat(cusItems_[5]).toFixed(2) + "</td>              <td style='display:none'>" + cusItems_[6] + "</td><td><input type='text' class='form-control'  value='" + cusItems_[7] + "' id='" + 'td_' + cusItems_[8] + "' style='display:none'></td><td style='display:none'>" + cusItems_[9] + "</td><td style='display:none'>" + cusItems_[10] + "</td><td>" + cusItems_[11] + "</td><td style='display:none'>" + cusItems_[12] + "</td><td style='display:none'>" + cusItems_[13] + "</td></tr>";
                //        //var markup = "                                                                                                                  <td  style='width:5% !important' ><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right' >" + objCells.item(1).innerHTML + "</td><td>" + parseFloat(qty_).toFixed(2) + "</td>       <td>" + parseFloat(objCells.item(3).innerHTML).toFixed(2) + "</td><td style='display:none'>" + parseFloat(Price).toFixed(2) + "</td>                     <td style='display:none'>" + ItemId + "</td>      <td><input type='text' class='form-control'  value='" + Baseprice + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + objCells.item(7).innerHTML + "</td><td style='display:none'>" + objCells.item(8).innerHTML + "</td><td>" + Baseprice + "</td>";
                //        //var markup = "<tr id=" + 'tr_' + ItemId + " onclick=ActiveRowget(\'" + ItemId + "\',\'" + Price + "\','1');><td style='width:5% !important'><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right'>"                       + Name + "</td><td>                          1.00</td><td>                                   " + parseFloat(Disc).toFixed(2) + "</td><td style='display:none' >                     " + parseFloat(Price).toFixed(2) + "</td><td style='display:none'>                      " + ItemId + "</td><td><input type='text' class='form-control'  value='     " + Price + "' id='" + 'td_'        + ItemId + "' style='display:none'></td><td style='display:none'>" + UomID + "</td><td style='display:none'>" + UomCode + "</td><td>" + parseFloat(Price).toFixed(2) + "</td><td style='display:none'>" + mapCode + "</td><td style='display:none'>" + Profile + "</td></tr>";
                //        $("#itemCarttable tbody").append(markup);
                //        $('#txtCusName').val(cusItems_[1]);
                //    }
                //}
                var CusDData = window.localStorage.getItem(CustId + '#0');
                if (CusDData != '') {

                    var spliCus = CusDData.split('~');
                    if (spliCus[1].indexOf('G#') == -1) {
                        $('#hdn_CusId').val(spliCus[0]);
                        $('#txtCusName').val(spliCus[1]);
                        GetRewardPoints(spliCus[0])
                    }
                    else {
                        $('#hdn_CusId').val('');
                        $('#txtCusName').val('');
                        jQuery("label[for='lblRedeem_points'").html('0.00');
                        jQuery("label[for='lblRewardPoints'").html('0.00');
                    }

                }
                $('#itemCarttable')[0].innerHTML = CusDraftData;
                $('#txthdnRemoveStatus').val('1');
                GradCaluclation();
                ClearFileds();
                //window.localStorage.setItem(CustId, '');
                //window.localStorage.setItem(CustId + "#0", '');
                //removeDraft(CustId);
            }
        }
        else {
            $('#hdn_CusId').val('');
            $('#txtCusName').val('');
        }
        valueAppendtoPOPUp();
    }
    removeDraft(CustId)
    $('#txthdnCusRemoveStatus').val('0');

}
function deleteRowCus(r) {
    $('#txthdnCusRemoveStatus').val('1');
    var i = r.parentNode.parentNode.rowIndex;
    //$("#itemCarttable").deleteRow(i);
    var CusId = r.parentNode.parentNode.children[3].innerHTML;
    removeDraft(CusId);

    var elem = document.getElementById("toggle-field");
    var elemDraft = document.getElementById("toggle-fieldSub");
    $('#hdn_CusId').val('');
    $('#txtCusName').val('');
    //elem.style.display = "none";
    //elemDraft.style.display = "block";

}
function removeDraft(CustId) {

    if (window.localStorage.getItem("DraftItemsHeader") != null) {
        window.localStorage.setItem(CustId, "");
        window.localStorage.setItem(CustId + "#0", "");
        var cusHeader = window.localStorage.getItem("DraftItemsHeader").split('$');
        if (cusHeader.length > 0) {
            window.localStorage.removeItem("DraftItemsHeader");
            for (var i = 0; i < cusHeader.length; i++) {
                var cusHeader_ = cusHeader[i].split('~');
                if (cusHeader_[0] != CustId) {
                    var HeadeSub_ = cusHeader_[0] + '~' + cusHeader_[1] + '~' + cusHeader_[2] + '$';
                    if (window.localStorage.getItem("DraftItemsHeader") == null) {
                        window.localStorage.setItem("DraftItemsHeader", HeadeSub_);
                    }
                    else {
                        var HeaderMain = window.localStorage.getItem("DraftItemsHeader") + HeadeSub_;
                        window.localStorage.setItem("DraftItemsHeader", HeaderMain);
                    }
                }
            }
        }
        CartItemsPULL();
    }
}

function dpCusChange() {
    //$("#itemCarttable tr").remove();
    //var ex1 = document.getElementById("dpCusData")
    //var strr_option = ex1.options[ex1.selectedIndex].value;
    //newCart();
    if ($('#hdn_CusId').val() != "") {
        ddpActiveRowgetCus($('#hdn_CusId').val());
        var elem = document.getElementById("toggle-field");
        var elemDraft = document.getElementById("toggle-fieldSub");
        elem.style.display = "block";
        elemDraft.style.display = "none";
        $('.content-panel').css('position', 'absolute');
        $('.content-panel').css('width', '50%');
        $('.content-panel').css('right', '0px');
    }
}


//
//var strng = document.getElementById("text_tag_input").value;
//document.getElementById("text_tag_input").value = strng.substring(0, strng.length - 1)

function ReassignSubVals(itemId) {
    var myTab1_ = document.getElementById('itemCarttable');
    var m_ = "";
    for (i = 0; i < myTab1_.rows.length; i++) {
        var objCells = myTab1_.rows.item(i).cells;

        if (itemId == objCells.item(5).innerHTML) {
            $('#txtAmount').val(objCells.item(4).innerHTML);
            break;
        }
    }
}
function ActiveRowget(itemId) {
    valueAppendtoPOPUp();
    $("#tbl_tenderDyms tr").remove();
    var myTab = document.getElementById('itemCarttable');
    var m_ = "";
    for (i = 0; i < myTab.rows.length; i++) {
        //if(myTab[i].value==)

        var objCells = myTab.rows.item(i).cells;
        try {
            var v_ = 'tr_' + objCells.item(5).innerHTML;
            document.getElementById(v_).style.cssText = 'font-weight:normal';
            if (itemId == objCells.item(5).innerHTML) {
                $('#txthdnItemId').val(itemId);
                $('#txtQuantity').val(objCells.item(2).innerHTML);
                $('#txtPrice').val(parseFloat($('#td_' + itemId).val()));
                $('#txtAmount').val(objCells.item(4).innerHTML);
                $('#txtDiscount').val(objCells.item(3).innerHTML);
                $('#txtUOM').val(objCells.item(8).innerHTML);
                $("#txtAmount").attr("disabled", "disabled");
                $("#txthdnItemId").attr("disabled", "disabled");
                $("#txtQuantity").attr("disabled", "disabled");
                $("#txtPrice").attr("disabled", "disabled");
                $("#txtUOM").attr("disabled", "disabled");
                $("#txtDiscount").attr("disabled", "disabled");
                $("#txtPrice").attr("disabled", "disabled");
                document.getElementById(v_).style.cssText = 'font-weight:bold';

            }
            $("#txthdnActiveColumn").val("");
            document.getElementById('liQtyM').style.cssText = 'color:white;background: #45bbe0;';
            document.getElementById('liDisM').style.cssText = 'color:white;background: #8892d6;';
            document.getElementById('liPriceM').style.cssText = 'color:white;background: #348cd4;';
        }
        catch (r) {

        }
    }
}
function MMSplitDecode(sText, sSymbol) {
    var Splitter = '';
    switch (sSymbol) {
        case 'Š':
            Splitter = '%u0160';
            break;
        case '%u02DC': //˜
            Splitter = '%u02DC';
            break;
        case '%B0': //°
            Splitter = '%B0';
            break;
        case '%F7': //÷
            Splitter = '%F7';
            break;
        case '%u0178':
            Splitter = '%u0178';
            break;
    }
    var str = escape(sText).split(Splitter);
    for (var iCount = 0; iCount < str.length; iCount++)
        str[iCount] = unescape(str[iCount]);
    return str;
}



function prev() {

    if ($('#hdnPageIndex').val() == 1) {
        $('#liPrev').addClass('inactive').removeClass('active');
        $('#liNext').addClass('active').removeClass('inactive');
    } else {
        $('#hdnPageIndex').val(parseInt($('#hdnPageIndex').val()) - 1);
        GetItemDetails();
        $('#liPrev').addClass('active').removeClass('inactive');
    }
}

function next() {
    $('#liPrev').addClass('active').removeClass('inactive');
    $('#hdnPageIndex').val(parseInt($('#hdnPageIndex').val()) + 1);
    GetItemDetails();
}

function GetItemDetails() {
    var SubOrgId = '';
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        SubOrgId = hashSplit[1] + ',' + hashSplit[16];
    }


    //data: "subOrgId=" + strOptions + "&ItemsData=" + JSON.stringify(jsonObj),
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetINVItems,
        //data: "subOrgId=" + SubOrgId + "," + $('#hdnCurrId').val() + "&VD=" + VD1(),
        data: "subOrgId=" + SubOrgId + "&VD=" + VD1() + "&pageIndex=" + $('#hdnPageIndex').val(),
        success: function (resp) {
            if (resp.length > 0) {
                var ItemsData = resp;
                document.getElementById("lblStockCt").innerHTML = resp.length;
                var li_ = "";
                document.getElementById('tbl_Item').childNodes[0].nodeValue = null;
                var MainData = '';

                for (var i = 0; i < ItemsData.length; i++) {
                    var mapCode_ = "";
                    var profileData_ = "";
                    mapCode_ = ItemsData[i].O2.replace('$', '&');
                    if (mapCode_ != '')
                        for (var k = 0; k < ItemsData[i].profileData.length; k++) {
                            var cols = ItemsData[i].profileData[k].split('~');
                            profileData_ = profileData_ + "@" + cols[0] + "#" + cols[1] + "#" + cols[2] + "#" + cols[3] + "#" + cols[4] + "#" + cols[5] + "#" + cols[6] + "#" + cols[8] + "#" + cols[9] + "#" + cols[10] + "#" + cols[11] + "#" + cols[12] + "#" + cols[13] + "#" + cols[14] + "#" + cols[15] + "#" + cols[16] + "#" + cols[17];
                        }

                    var img_ = ""; var style_ = "";
                    if (ItemsData[i].img != '') {
                        img_ = ItemsData[i].img;
                        style_ = "style='background-image:url(" + img_ + ");background-size:100% 80%;background-repeat:no-repeat;display:block;width:100%;height:84px;background-color:transparent;margin-bottom:20px;text-align:center;'"
                        li_ = li_ + "<li><a href='#' onclick=onSubData(\'" + ItemsData[i].Code + "\',\'" + ItemsData[i].ItemId + "\',\'" + ItemsData[i].Name + "\',\'" + ItemsData[i].Price + "\',\'" + ItemsData[i].Disc + "\',\'" + ItemsData[i].UOMID + "\',\'" + ItemsData[i].UOMCode + "\',\'" + mapCode_.replace(" ", "") + "\',\'" + profileData_.replace(" ", "") + "\',\'" + img_ + "\');> <div class='content-body-item'>";
                        li_ = li_ + "<h5 style='font-size:12px'>" + ItemsData[i].Code + "</h5><div   " + style_ + " class='item-sec'>";
                        li_ = li_ + "<p style='margin-top:50px;color:#000000;font-size:80%;text-align:center;word-wrap:break-word;white-space:pre-line;line-height:20px;font-weight:bold'>" + ItemsData[i].Name + "</p>";
                        li_ = li_ + "</div><div style='display:none'>" + ItemsData[i].BarCode.toUpperCase() + "</div><div style='display:none'>" + mapCode_.replace(" ", "") + "</div><div style='display:none'>" + profileData_.replace(" ", "") + "</div>";
                        li_ = li_ + "<small style='float:left;clear:both'>" + ItemsData[i].Price + "</small><small style=' float:right;clear:right' >" + ItemsData[i].UOMCode + "</small>";
                        li_ = li_ + "<br/><small >" + ItemsData[i].Alias + "</small></div>";
                        li_ = li_ + "</a></li>";
                    }
                    else {
                        li_ = li_ + "<li><a href='#' onclick=onSubData(\'" + ItemsData[i].Code + "\',\'" + ItemsData[i].ItemId + "\',\'" + ItemsData[i].Name + "\',\'" + ItemsData[i].Price + "\',\'" + ItemsData[i].Disc + "\',\'" + ItemsData[i].UOMID + "\',\'" + ItemsData[i].UOMCode + "\',\'" + mapCode_.replace(" ", "") + "\',\'" + profileData_.replace(" ", "") + "\');> <div class='content-body-item'>";
                        li_ = li_ + "<h5 style='font-size:12px'>" + ItemsData[i].Code + "</h5><div class='item-sec'>";
                        li_ = li_ + "<samp>" + ItemsData[i].Price + "</samp>";
                        li_ = li_ + "<br/><small>" + ItemsData[i].UOMCode + "</small></div><div style='display:none'>" + ItemsData[i].BarCode.toUpperCase() + "</div><div style='display:none'>" + mapCode_.replace(" ", "") + "</div><div style='display:none'>" + profileData_.replace(" ", "") + "</div>";
                        li_ = li_ + "<small >" + ItemsData[i].Name + "</small>";
                        li_ = li_ + "<br/><small >" + ItemsData[i].Alias + "</small></div>";
                        li_ = li_ + "</a></li>";
                    }

                    var Code = ItemsData[i].Code.toUpperCase();
                    var ItemId = ItemsData[i].ItemId;
                    var Name = ItemsData[i].Name;
                    var Price = ItemsData[i].Price;
                    var Disc = ItemsData[i].Disc;
                    var UOMCode = ItemsData[i].UOMCode;
                    var UOMName = ItemsData[i].UOMName;
                    var UOMID = ItemsData[i].UOMID;
                    var BarCode = ItemsData[i].BarCode.toUpperCase();
                    var StockData = ItemsData[i].StockData;
                    var Item_Group_Code = ItemsData[i].Item_Group_Code;
                    var Item_Group_Id = ItemsData[i].Item_Group_Id;
                    var O1 = ItemsData[i].O1;
                    var img_ = ItemsData[i].img;
                    var alias = ItemsData[i].Alias;
                    if (MainData != '')
                        MainData = MainData + Code + '~' + ItemId + '~' + Name + '~' + Price + '~' + Disc + '~' + UOMCode + '~' + UOMName + '~' + UOMID + '~' + BarCode + '~' + StockData + '~' + mapCode_ + '~' + profileData_ + '~' + img_ + '~' + alias + '$';
                    else
                        MainData = Code + '~' + ItemId + '~' + Name + '~' + Price + '~' + Disc + '~' + UOMCode + '~' + UOMName + '~' + UOMID + '~' + BarCode + '~' + StockData + '~' + mapCode_ + '~' + profileData_ + '~' + img_ + '~' + alias + '$';
                }

                document.getElementById('tbl_Item').innerHTML = "<ul  id='myTable'>" + li_ +
                                           "</ul>";


                var ct_ = parseFloat(parseFloat(parseFloat(ItemsData.length / 25).toFixed(0)) + parseFloat(1));

                if (ct_ >= 5) {
                    ct_ = 5;
                }
                if (ItemsData.length <= 25) {
                    ct_ = 1;
                }

                //$('#tbl_Item tfoot li').remove();
                //$('#paging-footer')[0].innerHTML = "";

                //$('#tbl_Item ul').paginathing({
                //    perPage: 25,
                //    limitPagination: ct_,
                //    containerClass: 'panel-footer',
                //    pageNumbers: false
                //})
                //data_ = $('#tbl_Item tfoot').find('ul li');
                //$('#tbl_Item tfoot').find('ul li').appendTo($('#paging-footer'));
                //var itemDataLocal = "<ul id='myTable'>" + li_ + "</ul>";
                // window.localStorage.setItem("itemDataLocal", MainData);
                $('#txtItemScan').focus();
                GetCounterTypeTenderData();
            } else {
                $('#hdnPageIndex').val(parseInt($('#hdnPageIndex').val()) - 1);
                $('#liNext').addClass('inactive').removeClass('active');
                $('#liPrev').addClass('active').removeClass('inactive');
            }
        },
        error: function (e) {

        }
    });

}
var data_ = '';
$('#div_OnlyInvoice').click(function (e) {

    //var da_ = $('#tblSummaryGrid tfoot');
    //$('#paging-footer')[0].innerHTML = "";
    //$('#tblSummaryGrid tfoot').find('ul li').appendTo($('#paging-footer'));
    //e.stopPropagation();
    //var l_ = $('#tbl_Item tfoot').find('ul li').length;
    //$('#paging-footer')[0].innerHTML = "";
    //$('#tbl_Item tfoot').find('ul li').appendTo($('#paging-footer'));
    //e.stopPropagation();
    //if (l_ == 0) {
    //    data_.appendTo($('#paging-footer'));
    //}
});
function GetShiftDataRTDRCHK() {

    if (window.localStorage.getItem("CashierID") != null) {
        var SubOrgId = ''; var CashierID = window.localStorage.getItem("CashierID");
        var HashValues = (window.localStorage.getItem("hashValues"));
        if (HashValues != null && HashValues != '') {
            var hashSplit = HashValues.split('~');
            SubOrgId = hashSplit[1];
        }


        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.ShiftCheck,
            data: "CashierID=" + CashierID + "&OrgId=" + SubOrgId + "&VD=" + VD1(),
            success: function (resp) {

                var result = resp;
                if (result.length > 0) {
                    var ACTIVE = result[0].ACTIVE;
                    var COUNTERID = result[0].COUNTERID;
                    var COUNTERNAME = result[0].COUNTERNAME;
                    var COUNTERNUMBER = result[0].COUNTERNUMBER;
                    var COUNTERTYPE = result[0].COUNTERTYPE;
                    var End_Time = result[0].End_Time;
                    var Is_Next_Day = result[0].Is_Next_Day;
                    var SHIFTID = result[0].SHIFTID;
                    var Shift_Code = result[0].Shift_Code;
                    var Shift_Name = result[0].Shift_Name;
                    var Start_Time = result[0].Start_Time;
                    var StageAlert = result[0].StageAlert;

                    window.localStorage.setItem('ACTIVE', result[0].ACTIVE);
                    window.localStorage.setItem('COUNTERID', result[0].COUNTERID);
                    window.localStorage.setItem('COUNTERNAME', result[0].COUNTERNAME);
                    window.localStorage.setItem('COUNTERNUMBER', result[0].COUNTERNUMBER);
                    window.localStorage.setItem('COUNTERTYPE', result[0].COUNTERTYPE);
                    window.localStorage.setItem('End_Time', result[0].End_Time);
                    window.localStorage.setItem('Is_Next_Day', result[0].Is_Next_Day);
                    window.localStorage.setItem('SHIFTID', result[0].SHIFTID);
                    window.localStorage.setItem('Shift_Code', result[0].Shift_Code);
                    window.localStorage.setItem('Shift_Name', result[0].Shift_Name);
                    window.localStorage.setItem('Start_Time', result[0].Start_Time);
                    var StageAlert = result[0].StageAlert;
                    DashBoardDataFill();
                }
                else {

                }
            },
            error: function (e) {

            }
        });
    }
    else {

    }


}

function GetClosedShiftData() {

    if (window.localStorage.getItem("CashierID") != null) {

        var SubOrgId = ''; var CashierID = window.localStorage.getItem("CashierID");
        var HashValues = (window.localStorage.getItem("hashValues"));
        if (HashValues != null && HashValues != '') {
            var hashSplit = HashValues.split('~');
            SubOrgId = hashSplit[1];
        }


        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.ClosedShiftCheck,
            data: "CashierID=" + CashierID + "&OrgId=" + SubOrgId + "&VD=" + VD1(),
            success: function (resp) {
                var result = resp;
                if (result.length > 0) {
                    var ACTIVE = result[0].ACTIVE;
                    var COUNTERID = result[0].COUNTERID;
                    var COUNTERNAME = result[0].COUNTERNAME;
                    var COUNTERNUMBER = result[0].COUNTERNUMBER;
                    var COUNTERTYPE = result[0].COUNTERTYPE;
                    var End_Time = result[0].End_Time;
                    var Is_Next_Day = result[0].Is_Next_Day;
                    var SHIFTID = result[0].SHIFTID;
                    var Shift_Code = result[0].Shift_Code;
                    var Shift_Name = result[0].Shift_Name;
                    var Start_Time = result[0].Start_Time;
                    var StageAlert = result[0].StageAlert;

                    window.localStorage.setItem('ACTIVE', result[0].ACTIVE);
                    window.localStorage.setItem('COUNTERID', result[0].COUNTERID);
                    window.localStorage.setItem('COUNTERNAME', result[0].COUNTERNAME);
                    window.localStorage.setItem('COUNTERNUMBER', result[0].COUNTERNUMBER);
                    window.localStorage.setItem('COUNTERTYPE', result[0].COUNTERTYPE);
                    window.localStorage.setItem('End_Time', result[0].End_Time);
                    window.localStorage.setItem('Is_Next_Day', result[0].Is_Next_Day);
                    window.localStorage.setItem('SHIFTID', result[0].SHIFTID);
                    window.localStorage.setItem('Shift_Code', result[0].Shift_Code);
                    window.localStorage.setItem('Shift_Name', result[0].Shift_Name);
                    window.localStorage.setItem('Start_Time', result[0].Start_Time);
                    var StageAlert = result[0].StageAlert;
                    //if (StageAlert == '1') {
                    //if (ACTIVE.toLowerCase() == 'y') {
                    window.open('TodaysCloseoutRP.html', '_self', 'location=no');
                    //}
                    //else {
                    //    BootstrapDialog.alert('dont have permission to access')
                    //}
                    //}
                    //else if (StageAlert == '2') {
                    //    BootstrapDialog.alert('Please Check Till');
                    //}
                }
                else {
                    var $textAndPic = $('<div></div>');
                    $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                    //$textAndPic.append('<div class="alert-message">dont have permission to access invoice</div>');
                    $textAndPic.append('<div class="alert-message">' + $('#hdnDontPermisAccessInvoice').val() + '</div>');
                    BootstrapDialog.alert($textAndPic);
                }
            },
            error: function (e) {
                var $textAndPic = $('<div></div>');
                $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                //$textAndPic.append('<div class="alert-message">Invalid Data</div>');
                $textAndPic.append('<div class="alert-message">' + $('#hdnInvalidData').val() + '</div>');
                BootstrapDialog.alert($textAndPic);
            }
        });
    }
    else {
        var $textAndPic = $('<div></div>');
        $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        //$textAndPic.append('<div class="alert-message">Please contact Admin.</div>');
        $textAndPic.append('<div class="alert-message">' + $('#hdnPleaseContactAdmin').val() + '</div>');
        BootstrapDialog.alert($textAndPic);
    }


}


function GetShiftDataRTDR() {

    if (window.localStorage.getItem("CashierID") != null) {

        var SubOrgId = ''; var CashierID = window.localStorage.getItem("CashierID");
        var HashValues = (window.localStorage.getItem("hashValues"));
        if (HashValues != null && HashValues != '') {
            var hashSplit = HashValues.split('~');
            SubOrgId = hashSplit[1];
        }

        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.ShiftCheck,
            data: "CashierID=" + CashierID + "&OrgId=" + SubOrgId + "&VD=" + VD1(),
            success: function (resp) {
                var result = resp;
                if (result.length > 0) {
                    var ACTIVE = result[0].ACTIVE;
                    var COUNTERID = result[0].COUNTERID;
                    var COUNTERNAME = result[0].COUNTERNAME;
                    var COUNTERNUMBER = result[0].COUNTERNUMBER;
                    var COUNTERTYPE = result[0].COUNTERTYPE;
                    var End_Time = result[0].End_Time;
                    var Is_Next_Day = result[0].Is_Next_Day;
                    var SHIFTID = result[0].SHIFTID;
                    var Shift_Code = result[0].Shift_Code;
                    var Shift_Name = result[0].Shift_Name;
                    var Start_Time = result[0].Start_Time;
                    var StageAlert = result[0].StageAlert;

                    window.localStorage.setItem('ACTIVE', result[0].ACTIVE);
                    window.localStorage.setItem('COUNTERID', result[0].COUNTERID);
                    window.localStorage.setItem('COUNTERNAME', result[0].COUNTERNAME);
                    window.localStorage.setItem('COUNTERNUMBER', result[0].COUNTERNUMBER);
                    window.localStorage.setItem('COUNTERTYPE', result[0].COUNTERTYPE);
                    window.localStorage.setItem('End_Time', result[0].End_Time);
                    window.localStorage.setItem('Is_Next_Day', result[0].Is_Next_Day);
                    window.localStorage.setItem('SHIFTID', result[0].SHIFTID);
                    window.localStorage.setItem('Shift_Code', result[0].Shift_Code);
                    window.localStorage.setItem('Shift_Name', result[0].Shift_Name);
                    window.localStorage.setItem('Start_Time', result[0].Start_Time);
                    var StageAlert = result[0].StageAlert;
                    if (StageAlert == '1') {
                        if (ACTIVE.toLowerCase() == 'y') {
                            window.open('TodaySalesDetailsRP.html', '_self', 'location=no');
                        }
                        else {
                            var $textAndPic = $('<div></div>');
                            $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                            //$textAndPic.append('<div class="alert-message">dont have permission to access invoice</div>');
                            $textAndPic.append('<div class="alert-message">' + $('#hdnDontPermisAccessInvoice').val() + '</div>');
                            BootstrapDialog.alert($textAndPic);
                        }
                    }
                    else if (StageAlert == '2') {
                        var $textAndPic = $('<div></div>');
                        $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                        //$textAndPic.append('<div class="alert-message">You are not assigned any Counter so you can\'t do the Transaction</div>');
                        $textAndPic.append('<div class="alert-message">' + $('#hdnNotAssignCountrToTrans').val() + '</div>');
                        //$textAndPic.append('<div class="alert-message">' + $('#hdnNAssignCountrTrans').val() + '</div>');
                        BootstrapDialog.alert($textAndPic);
                    }
                }
                else {
                    var $textAndPic = $('<div></div>');
                    $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                    //$textAndPic.append('<div class="alert-message">dont have permission to access invoice</div>');
                    $textAndPic.append('<div class="alert-message">' + $('#hdnDontPermisAccessInvoice').val() + '</div>');
                    BootstrapDialog.alert($textAndPic);
                }
            },
            error: function (e) {
                var $textAndPic = $('<div></div>');
                $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                //$textAndPic.append('<div class="alert-message">Invalid Data</div>');
                $textAndPic.append('<div class="alert-message">' + $('#hdnInvalidData').val() + '</div>');
                BootstrapDialog.alert($textAndPic);
            }
        });
    }
    else {
        var $textAndPic = $('<div></div>');
        $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        //$textAndPic.append('<div class="alert-message">Please contact Admin.</div>');
        $textAndPic.append('<div class="alert-message">' + $('#hdnPleaseContactAdmin').val() + '</div>');
        BootstrapDialog.alert($textAndPic);
    }


}
function GetShiftData() {

    if (window.localStorage.getItem("CashierID") != null) {

        //window.open('posmain.html?#', '_self', 'location=no');

        //return true;

        var SubOrgId = ''; var CashierID = window.localStorage.getItem("CashierID");
        var HashValues = (window.localStorage.getItem("hashValues"));
        if (HashValues != null && HashValues != '') {
            var hashSplit = HashValues.split('~');
            SubOrgId = hashSplit[1];
        }


        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.ShiftCheck,
            data: "CashierID=" + CashierID + "&OrgId=" + SubOrgId + "&VD=" + VD1(),
            success: function (resp) {
                var result = resp;
                if (result.length > 0) {
                    var ACTIVE = result[0].ACTIVE;
                    var COUNTERID = result[0].COUNTERID;
                    var COUNTERNAME = result[0].COUNTERNAME;
                    var COUNTERNUMBER = result[0].COUNTERNUMBER;
                    var COUNTERTYPE = result[0].COUNTERTYPE;
                    var End_Time = result[0].End_Time;
                    var Is_Next_Day = result[0].Is_Next_Day;
                    var SHIFTID = result[0].SHIFTID;
                    var Shift_Code = result[0].Shift_Code;
                    var Shift_Name = result[0].Shift_Name;
                    var Start_Time = result[0].Start_Time;
                    var StageAlert = result[0].StageAlert;

                    window.localStorage.setItem('ACTIVE', result[0].ACTIVE);
                    window.localStorage.setItem('COUNTERID', result[0].COUNTERID);
                    window.localStorage.setItem('COUNTERNAME', result[0].COUNTERNAME);
                    window.localStorage.setItem('COUNTERNUMBER', result[0].COUNTERNUMBER);
                    window.localStorage.setItem('COUNTERTYPE', result[0].COUNTERTYPE);
                    window.localStorage.setItem('End_Time', result[0].End_Time);
                    window.localStorage.setItem('Is_Next_Day', result[0].Is_Next_Day);
                    window.localStorage.setItem('SHIFTID', result[0].SHIFTID);
                    window.localStorage.setItem('Shift_Code', result[0].Shift_Code);
                    window.localStorage.setItem('Shift_Name', result[0].Shift_Name);
                    window.localStorage.setItem('Start_Time', result[0].Start_Time);
                    var StageAlert = result[0].StageAlert;
                    if (StageAlert == '1') {
                        //if (ACTIVE.toLowerCase() == 'y') {
                        window.open('posmain.html?#', '_self', 'location=no');
                        //}
                        //else {
                        //    var $textAndPic = $('<div></div>');
                        //    $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                        //    //$textAndPic.append('<div class="alert-message">dont have permission to access invoice</div>');
                        //    $textAndPic.append('<div class="alert-message">' + $('#hdnNPermissionInvoice').val() + '</div>');
                        //    BootstrapDialog.alert($textAndPic);
                        //}
                    }
                    else if (StageAlert == '2') {
                        var $textAndPic = $('<div></div>');
                        $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                        //$textAndPic.append('<div class="alert-message">You are not assigned any Counter so you can\'t do the Transaction</div>');
                        //$textAndPic.append('<div class="alert-message"> ' + $('#hdnNAssignCountrTrans').val() + '</div>');
                        $textAndPic.append('<div class="alert-message">' + $('#hdnNotAssignCountrToTrans').val() + '</div>');

                        BootstrapDialog.alert($textAndPic);
                    } else if (StageAlert == '3') {
                        var $textAndPic = $('<div></div>');
                        $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                        //$textAndPic.append('<div class="alert-message">You are not assigned any Counter so you can\'t do the Transaction</div>');
                        $textAndPic.append('<div class="alert-message">Day closer under processing – can’t do any operations </div>');
                        //$textAndPic.append('<div class="alert-message">' + $('#hdnNotAssignCountrToTrans').val() + '</div>');

                        BootstrapDialog.alert($textAndPic);
                    }
                }
                else {
                    var $textAndPic = $('<div></div>');
                    $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                    //$textAndPic.append('<div class="alert-message">dont have permission to access invoice</div>');
                    $textAndPic.append('<div class="alert-message">' + $('#hdnNPermissionInvoice').val() + '</div>');
                    BootstrapDialog.alert($textAndPic);
                }
            },
            error: function (e) {
                var $textAndPic = $('<div></div>');
                $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                //$textAndPic.append('<div class="alert-message">Invalid Data</div>');
                $textAndPic.append('<div class="alert-message">' + $('#hdnInvalData').val() + '</div>');
                BootstrapDialog.alert($textAndPic);
            }
        });
    }
    else {
        var $textAndPic = $('<div></div>');
        $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        //$textAndPic.append('<div class="alert-message">Please contact Admin.</div>');
        $textAndPic.append('<div class="alert-message">' + $('#hdnContactAdmin').val() + '</div>');
        BootstrapDialog.alert($textAndPic);
    }


}

function createJSON() {
    jsonObj = [];
    $("input[class=email]").each(function () {

        var id = $(this).attr("title");
        var email = $(this).val();

        item = {
        }
        item["title"] = id;
        item["email"] = email;

        jsonObj.push(item);
    });

    console.log(jsonObj);
}
function GetCustomers1() {
    var SubOrgId = ''; var CashierID = window.localStorage.getItem("CashierID");
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        SubOrgId = hashSplit[1];
    }

    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetCustomers,
        data: "VD=" + VD1(),
        success: function (resp) {
            var CusData = resp;
            //document.getElementById("dpCusData").options.length = 0;
            //$("#dpCusData").append($("<option value='' disabled selected ></option>").val(0).html('Select'));//this is for adding select after clearing
            var MainData = '';
            jsonObj = [];
            for (var i = 0; i < CusData.length; i++) {
                var CusId = CusData[i].CusID;
                var CusName = CusData[i].Code + "/" + CusData[i].Name;
                if (MainData != '')
                    MainData = MainData + CusId + '~' + CusName + '$';
                else
                    MainData = CusId + '~' + CusName + '$';
                //item = {}
                //item["code"] = CusId;
                //item["name"] = CusName;
                //jsonObj.push(item);
                //$("#dpCusData").append($("<option></option>").val(CusId).html(CusName));
            };
            window.localStorage.setItem("FillCustomer", MainData);
        },
        error: function (e) {
            //alert('Invalid Data', null, 'Error');
        }
    });
}

function GetCustomers_() {


    var HashVal = (window.localStorage.getItem("hashValues"));
    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');
    }

    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetCustomerSummaryGrid,
        data: "OrgId=" + HashValues[15] + "&VD=" + VD1(),

        success: function (resp) {
            var GridData = resp;

            var MainData = '';
            var SummaryTable = '';
            for (var i = 0; i < GridData.length; i++) {
                var CustId = GridData[i].CUSTOMER_ID;
                var CustCode = GridData[i].CUSTOMER_CODE;
                var CustName = GridData[i].CUSTOMER_NAME;
                var Status = GridData[i].STATUS;
                var RewardPoints = GridData[i].REWARDPOINTS;
                var phoneNo = GridData[i].PHONENO;
                var CusId = CustId;
                var CusName = phoneNo + " / " + CustName;
                if (Status.toUpperCase() == "ACTIVE")
                    if (MainData != '')
                        //MainData = MainData + CusId + '~' + CusName  + '$';
                        MainData = MainData + CusId + '~' + CusName + '~' + RewardPoints + '$';
                    else
                        MainData = CusId + '~' + CusName + '~' + RewardPoints + '$';

            };
            window.localStorage.setItem("FillCustomer1", MainData);
            $('#txtItemScan').focus();
        },
        error: function (e) {
        }
    });
}
//$("#txtCusNamea").autocomplete({
//    source: function (request, response) {
//        var aa = '';
//        var v = (window.localStorage.getItem("FillCustomer"));
//        jsonObj = [];
//        if (v != "") {
//            var cus_ = v.split('$');
//            for (var i = 0; i < cus_.length; i++) {
//                var cusChek = cus_[i].split('~');
//                item = {}
//                item["value"] = cusChek[1];
//                item["data"] = cusChek[0];
//                jsonObj.push(item);
//            }
//        }
//        response(jsonObj);
//        //$.ajax({
//        //    type: "POST",
//        //    contentType: "application/json; charset=utf-8",
//        //    url: "Default.aspx/GetEmployeeName",
//        //    data: "{'empName':'" + document.getElementById('txtCusName').value + "'}",
//        //    dataType: "json",
//        //    success: function (data) {
//        //        response(data.d);
//        //    },
//        //    error: function (result) {
//        //        alert("ss");
//        //    }
//        //});
//    }
//});



//var v = (window.localStorage.getItem("FillCustomer"));
//jsonObj = [];
//if (v != "") {
//    var cus_ = v.split('$');
//    for (var i = 0; i < cus_.length; i++) {
//        var cusChek = cus_[i].split('~');
//        item = {}
//        item["value"] = cusChek[1];
//        item["data"] = cusChek[0];
//        jsonObj.push(item);
//    }
//}
try {
    $("#txtCusNamea").autocomplete({
        source: function (req, response) {
            $.ajax({

                success: function (data) {

                    var re = $.ui.autocomplete.escapeRegex(req.term);
                    var matcher = new RegExp("^" + re, "i");
                    response($.grep(data, function (item) {
                        return matcher.test(item.value);
                    }));
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            //custom select function if needed
        }
    });
}
catch (a) {

}
$('#txtCusName').dblclick(function () {

    BootstrapDialog.show({
        message: $('<div></div>').load('/' + window.location.href.split('/')[3] + '/Common/SimpleSearch.aspx?keypair=' + 'SDOnlyOrgs' + '&ModuleName=' + 'Sales' + '&Params=' + strParams + ''),
        title: "<h1> " + strOrganization + " </h1><small>" + strPickerDesc + " </small>",
        buttons: [{
            id: 'btnOk',
            label: strARCok,
            cssClass: 'btn-primary',
            action: function (dialogRef) {


            }
        },
        {
            id: 'btnCancel',
            label: strARCCancel,
            cssClass: 'btn-primary',
            action: function (dialogRefClose) {
                dialogRefClose.close();
            }
        }]
    });
});

try {
    $("#txtCusName").autocomplete({

        dataFilter: function (data) {
            return data
        },
        source: function (req, add) {

            var v = (window.localStorage.getItem("FillCustomer1"));
            jsonObj = [];
            if (v != "") {
                var cus_ = v.split('$');
                for (var i = 0; i < cus_.length; i++) {
                    var cusChek = cus_[i].split('~');
                    if (cusChek.length > 1) {
                        item = {
                        }
                        item["value"] = cusChek[1];
                        item["data"] = cusChek[0];
                        item["rewardPoints"] = cusChek[2];
                        jsonObj.push(item);
                    }
                }
            }
            add($.map(jsonObj, function (el) {
                var re = $.ui.autocomplete.escapeRegex(req.term);
                if (re.trim() != "") {
                    if (el.value.toLowerCase().indexOf(re.toLowerCase()) != -1) {
                        return {
                            label: el.value,
                            val: el.data,
                            rewardPoints: el.rewardPoints
                        };
                    }
                }
                else {
                    return {
                        label: el.value,
                        val: el.data,
                        rewardPoints: el.rewardPoints
                    };
                }
            }));
        },
        change: function (e, i) {
            if (i.item) {
            }
            else {
                $('#txtCusName').val('');
                $('#hdn_CusId').val('');
            }
        },
        select: function (event, ui) {
            ClearFileds();
            var HashValues = (window.localStorage.getItem("hashValues"));
            if (HashValues != null && HashValues != '') {
                var hashSplit = HashValues.split('~');
                SubOrgId = hashSplit[1];
            }
            $("#itemCarttable tr").remove();
            $('#hdn_CusId').val(ui.item.val);
            GetRewardPoints(ui.item.val);//  jQuery("label[for='lblRewardPoints'").html(ui.item.rewardPoints);
            jQuery("label[for='lblPointsObtainBasecurrency'").html(hashSplit[20]);
            jQuery("label[for='lblMinPoints'").html(hashSplit[21]);
            jQuery("label[for='lblMaxPoints'").html(hashSplit[22]);
            $('#grdandTotal').text('0.000');
            dpCusChange();
            popupSts = 0;
        }
    });

}
catch (a) {

}

try {
    //$("#txtItemSearchCart_0").autocomplete({

    //    dataFilter: function (data) {
    //        return data
    //    },
    //    source: function (req, add) {

    //        var v = (window.localStorage.getItem("itemDataLocal"));
    //        jsonObj = [];

    //        if (v != "") {
    //            var cus_ = v.split('$');
    //            for (var i = 0; i < cus_.length; i++) {
    //                var itemChek = cus_[i].split('~');

    //                if (itemChek.length > 1) {
    //                    item = {
    //                    }

    //                    //item["value"] = cusChek[1];
    //                    //item["data"] = cusChek[0];
    //                    //item["rewardPoints"] = cusChek[2];
    //                    item["itemChek0"] = itemChek[0].replace('"', '');
    //                    item["itemChek1"] = itemChek[1];
    //                    item["itemChek2"] = itemChek[2];
    //                    item["itemChek3"] = itemChek[3];
    //                    item["itemChek4"] = itemChek[4];
    //                    item["itemChek7"] = itemChek[7];
    //                    item["itemChek5"] = itemChek[5];
    //                    item["itemChek10"] = itemChek[10];
    //                    item["itemChek11"] = itemChek[11];
    //                    item["itemName"] = itemChek[2];
    //                    item["img"] = itemChek[12];

    //                    jsonObj.push(item);
    //                }
    //            }
    //        }

    //        add($.map(jsonObj, function (el) {
    //            var re = $.ui.autocomplete.escapeRegex(req.term);
    //            if (re.trim() != "") {
    //                if (((el.itemChek0.toLowerCase().indexOf(re.toLowerCase())) != -1) || ((el.itemName.toLowerCase().indexOf(re.toLowerCase())) != -1)) {
    //                    if (el.img != "") {
    //                        return {
    //                            label: '<img style="width:20px;height:20px" src=' + el.img + '>&nbsp;&nbsp;' + el.itemChek0 + '/' + el.itemName,
    //                            val: el.itemChek1,
    //                            itemChek0: el.itemChek0,
    //                            itemChek1: el.itemChek1,
    //                            itemChek2: el.itemChek2,
    //                            itemChek3: el.itemChek3,
    //                            itemChek4: el.itemChek4,
    //                            itemChek7: el.itemChek7,
    //                            itemChek5: el.itemChek5,
    //                            itemChek10: el.itemChek10,
    //                            itemChek11: el.itemChek11,
    //                            itemName: el.itemChek2,
    //                            img_: el.img,
    //                        };
    //                    }
    //                    else {
    //                        return {
    //                            label: el.itemChek0 + '/' + el.itemName,
    //                            val: el.itemChek1,
    //                            itemChek0: el.itemChek0,
    //                            itemChek1: el.itemChek1,
    //                            itemChek2: el.itemChek2,
    //                            itemChek3: el.itemChek3,
    //                            itemChek4: el.itemChek4,
    //                            itemChek7: el.itemChek7,
    //                            itemChek5: el.itemChek5,
    //                            itemChek10: el.itemChek10,
    //                            itemChek11: el.itemChek11,
    //                            itemName: el.itemChek2,
    //                            img_: el.img,
    //                        };
    //                    }
    //                }
    //            }
    //            else {
    //                if (el.img != "") {
    //                    return {

    //                        label: '<img style="width:20px;height:20px" src=' + el.img + '>&nbsp;&nbsp;' + el.itemChek0 + '/' + el.itemName,
    //                        val: el.itemChek1,
    //                        itemChek0: el.itemChek0,
    //                        itemChek1: el.itemChek1,
    //                        itemChek2: el.itemChek2,
    //                        itemChek3: el.itemChek3,
    //                        itemChek4: el.itemChek4,
    //                        itemChek7: el.itemChek7,
    //                        itemChek5: el.itemChek5,
    //                        itemChek10: el.itemChek10,
    //                        itemChek11: el.itemChek11,
    //                        itemName: el.itemChek2,
    //                        img_: el.img,
    //                    };
    //                }
    //                else {
    //                    return {
    //                        label: el.itemChek0 + '/' + el.itemName,
    //                        val: el.itemChek1,
    //                        itemChek0: el.itemChek0,
    //                        itemChek1: el.itemChek1,
    //                        itemChek2: el.itemChek2,
    //                        itemChek3: el.itemChek3,
    //                        itemChek4: el.itemChek4,
    //                        itemChek7: el.itemChek7,
    //                        itemChek5: el.itemChek5,
    //                        itemChek10: el.itemChek10,
    //                        itemChek11: el.itemChek11,
    //                        itemName: el.itemChek2,
    //                        img_: el.img,
    //                    };
    //                }
    //            }
    //        }));
    //    },
    //    change: function (e, i) {
    //        if (i.item) {
    //        }
    //        else {
    //            $('#txtCusName').val('');
    //            $('#hdn_CusId').val('');
    //        }
    //    },
    //    focus: function (event, ui) {
    //        $('#txtItemSearchCart').val(ui.item.itemChek2);

    //        //return false;
    //    },
    //    select: function (event, ui) {
    //        onSubData(ui.item.itemChek0, ui.item.itemChek1, ui.item.itemChek2, ui.item.itemChek3, ui.item.itemChek4, ui.item.itemChek7, ui.item.itemChek5, ui.item.itemChek10, ui.item.itemChek11, ui.item.img_);
    //        $('#txtItemSearchCart').val('');

    //    }
    //});


    $("#txtItemSearchCart").autocomplete({
        minLength: 1,
        source: function (req, res) {
            var SubOrgId = '';
            var HashValues = (window.localStorage.getItem("hashValues"));
            if (HashValues != null && HashValues != '') {
                var hashSplit = HashValues.split('~');
                //getResources1(hashSplit[7], "POSInvoice");
                SubOrgId = hashSplit[1] + ',' + hashSplit[16];
            }
            $.ajax({
                url: CashierLite.Settings.getItemDataAutocomplte,
                data: "subOrgId=" + SubOrgId + "&VD=" + VD1() + "&passingText=" + $('#txtItemSearchCart').val().trim(),
                success: function (resp) {
                    res($.map(resp, function (el) {
                        var li_ = "";
                        var mapCode_ = "";
                        var profileData_ = "";
                        mapCode_ = el.O2.replace('$', '&');

                        if (mapCode_ != '')
                            for (var k = 0; k < el.profileData.length; k++) {
                                var cols = el.profileData[k].split('~');
                                profileData_ = profileData_ + "@" + cols[0] + "#" + cols[1] + "#" + cols[2] + "#" + cols[3] + "#" + cols[4] + "#" + cols[5] + "#" + cols[6] + "#" + cols[8] + "#" + cols[9] + "#" + cols[10] + "#" + cols[11] + "#" + cols[12] + "#" + cols[13] + "#" + cols[14] + "#" + cols[15] + "#" + cols[16] + "#" + cols[17];
                            }

                        var img_ = "";
                        img_ = el.img;
                        //var style_ = "";
                        //if (el.img != '') {
                        //    img_ = el.img;
                        //    style_ = "style='background-image:url(" + img_ + ");background-size:100% 80%;background-repeat:no-repeat;display:block;width:84px;height:84px;background-color:transparent;margin-bottom:20px;text-align:center;'"
                        //    li_ = li_ + "<li><a href='#' onclick=onSubData(\'" + el.Code + "\',\'" + el.ItemId + "\',\'" + el.Name + "\',\'" + el.Price + "\',\'" + el.Disc + "\',\'" + el.UOMID + "\',\'" + el.UOMCode + "\',\'" + mapCode_.replace(" ", "") + "\',\'" + profileData_.replace(" ", "") + "\',\'" + img_ + "\');> <div class='content-body-item'>";
                        //    li_ = li_ + "<h5 style='font-size:12px'>" + el.Code + "</h5><div   " + style_ + " class='item-sec'>";
                        //    li_ = li_ + "<p style='margin-top:50px;color:#000000;font-size:80%;text-align:center;word-wrap:break-word;white-space:pre-line;line-height:20px;font-weight:bold'>" + el.Name + "</p>";
                        //    li_ = li_ + "</div><div style='display:none'>" + el.BarCode.toUpperCase() + "</div><div style='display:none'>" + mapCode_.replace(" ", "") + "</div><div style='display:none'>" + profileData_.replace(" ", "") + "</div>";
                        //    li_ = li_ + "<small style='float:left;clear:both'>" + el.Price + "</small><small style=' float:right;clear:right' >" + el.UOMCode + "</small></div>";
                        //    li_ = li_ + "</a></li>";
                        //}
                        //else {
                        //    li_ = li_ + "<li><a href='#' onclick=onSubData(\'" + el.Code + "\',\'" + el.ItemId + "\',\'" + el.Name + "\',\'" + el.Price + "\',\'" + el.Disc + "\',\'" + el.UOMID + "\',\'" + el.UOMCode + "\',\'" + mapCode_.replace(" ", "") + "\',\'" + profileData_.replace(" ", "") + "\');> <div class='content-body-item'>";
                        //    li_ = li_ + "<h5 style='font-size:12px'>" + el.Code + "</h5><div class='item-sec'>";
                        //    li_ = li_ + "<samp>" + el.Price + "</samp>";
                        //    li_ = li_ + "<br/><small>" + el.UOMCode + "</small></div><div style='display:none'>" + el.BarCode.toUpperCase() + "</div><div style='display:none'>" + mapCode_.replace(" ", "") + "</div><div style='display:none'>" + profileData_.replace(" ", "") + "</div>";
                        //    li_ = li_ + "<small>" + el.Name + "</small></div>";
                        //    li_ = li_ + "</a></li>";
                        //}
                        // document.getElementById('tbl_Item').innerHTML = "<ul  id='myTable'>" + li_ +
                        //               "</ul>";

                        if (el.img != "") {
                            return {
                                label: '<img style="width:20px;height:20px" src=' + el.img + '>&nbsp;&nbsp;' + el.Code + '/' + el.Name + '  ' + el.Alias,
                                val: el.Name,
                                itemChek0: el.Code,
                                itemChek1: el.ItemId,
                                itemChek2: el.Name,
                                itemChek3: el.Price,
                                itemChek4: el.Disc,
                                itemChek7: el.UOMID,
                                itemChek5: el.UOMCode,
                                itemChek10: mapCode_,
                                itemChek11: profileData_,
                                itemName: el.Name,
                                img_: img_
                            }
                        } else {
                            return {
                                label: el.Code + '/' + el.Name + '  ' + el.Alias,
                                val: el.Name,
                                itemChek0: el.Code,
                                itemChek1: el.ItemId,
                                itemChek2: el.Name,
                                itemChek3: el.Price,
                                itemChek4: el.Disc,
                                itemChek7: el.UOMID,
                                itemChek5: el.UOMCode,
                                itemChek10: mapCode_,
                                itemChek11: profileData_,
                                itemName: el.Name,
                                img_: img_,
                            }
                        }

                    }))
                }
            });
        },
        focus: updateTextbox,
        select: updateTextboxSelect
    }).autocomplete("instance")._renderItem = function (ul, item) {
        return $("<li>")
            .append("<img class='imageClass' src=" + item.img + " alt=" + item.img + "/>")
            .append('<a>' + item.itemChek2 + '</a>')
            .appendTo(ul);
    };


    //$("#txtItemSearchCart").autocomplete({
    //    minLength: 1,
    //    source: function (req, add) {

    //        var v = (window.localStorage.getItem("itemDataLocal"));
    //        jsonObj = [];

    //        if (v != "") {
    //            var cus_ = v.split('$');
    //            for (var i = 0; i < cus_.length; i++) {
    //                var itemChek = cus_[i].split('~');

    //                if (itemChek.length > 1) {
    //                    item = {
    //                    }

    //                    //item["value"] = cusChek[1];
    //                    //item["data"] = cusChek[0];
    //                    //item["rewardPoints"] = cusChek[2];
    //                    item["itemChek0"] = itemChek[0].replace('"', '');
    //                    item["itemChek1"] = itemChek[1];
    //                    item["itemChek2"] = itemChek[2];
    //                    item["itemChek3"] = itemChek[3];
    //                    item["itemChek4"] = itemChek[4];
    //                    item["itemChek7"] = itemChek[7];
    //                    item["itemChek5"] = itemChek[5];
    //                    item["itemChek10"] = itemChek[10];
    //                    item["itemChek11"] = itemChek[11];
    //                    item["itemName"] = itemChek[2];
    //                    item["img"] = itemChek[12];

    //                    jsonObj.push(item);
    //                }
    //            }
    //        }

    //        add($.map(jsonObj, function (el) {
    //            var re = $.ui.autocomplete.escapeRegex(req.term);
    //            if (re.trim() != "") {
    //                if (((el.itemChek0.toLowerCase().indexOf(re.toLowerCase())) != -1) || ((el.itemName.toLowerCase().indexOf(re.toLowerCase())) != -1)) {
    //                    if (el.img != "") {
    //                        return {
    //                            label: '<img style="width:20px;height:20px" src=' + el.img + '>&nbsp;&nbsp;' + el.itemChek0 + '/' + el.itemName,
    //                            val: el.itemChek1,
    //                            itemChek0: el.itemChek0,
    //                            itemChek1: el.itemChek1,
    //                            itemChek2: el.itemChek2,
    //                            itemChek3: el.itemChek3,
    //                            itemChek4: el.itemChek4,
    //                            itemChek7: el.itemChek7,
    //                            itemChek5: el.itemChek5,
    //                            itemChek10: el.itemChek10,
    //                            itemChek11: el.itemChek11,
    //                            itemName: el.itemChek2,
    //                            img_: el.img,
    //                        };
    //                    }
    //                    else {
    //                        return {
    //                            label: el.itemChek0 + '/' + el.itemName,
    //                            val: el.itemChek1,
    //                            itemChek0: el.itemChek0,
    //                            itemChek1: el.itemChek1,
    //                            itemChek2: el.itemChek2,
    //                            itemChek3: el.itemChek3,
    //                            itemChek4: el.itemChek4,
    //                            itemChek7: el.itemChek7,
    //                            itemChek5: el.itemChek5,
    //                            itemChek10: el.itemChek10,
    //                            itemChek11: el.itemChek11,
    //                            itemName: el.itemChek2,
    //                            img_: el.img,
    //                        };
    //                    }
    //                }
    //            }
    //            else {
    //                if (el.img != "") {
    //                    return {

    //                        label: '<img style="width:20px;height:20px" src=' + el.img + '>&nbsp;&nbsp;' + el.itemChek0 + '/' + el.itemName,
    //                        val: el.itemChek1,
    //                        itemChek0: el.itemChek0,
    //                        itemChek1: el.itemChek1,
    //                        itemChek2: el.itemChek2,
    //                        itemChek3: el.itemChek3,
    //                        itemChek4: el.itemChek4,
    //                        itemChek7: el.itemChek7,
    //                        itemChek5: el.itemChek5,
    //                        itemChek10: el.itemChek10,
    //                        itemChek11: el.itemChek11,
    //                        itemName: el.itemChek2,
    //                        img_: el.img,
    //                    };
    //                }
    //                else {
    //                    return {
    //                        label: el.itemChek0 + '/' + el.itemName,
    //                        val: el.itemChek1,
    //                        itemChek0: el.itemChek0,
    //                        itemChek1: el.itemChek1,
    //                        itemChek2: el.itemChek2,
    //                        itemChek3: el.itemChek3,
    //                        itemChek4: el.itemChek4,
    //                        itemChek7: el.itemChek7,
    //                        itemChek5: el.itemChek5,
    //                        itemChek10: el.itemChek10,
    //                        itemChek11: el.itemChek11,
    //                        itemName: el.itemChek2,
    //                        img_: el.img,
    //                    };
    //                }
    //            }
    //        }));
    //    },
    //    focus: updateTextbox,
    //    select: updateTextboxSelect
    //}).autocomplete("instance")._renderItem = function (ul, item) {
    //    return $("<li>")
    //        .append("<img class='imageClass' src=" + item.img + " alt=" + item.img + "/>")
    //        .append('<a>' + item.itemChek2 + '</a>')
    //        .appendTo(ul);
    //};
    function updateTextbox(event, ui) {
        $(this).val(ui.item.itemChek2);
        return false;
    }
    function updateTextboxSelect(event, ui) {
        $(this).val(ui.item.itemChek2);
        onSubData(ui.item.itemChek0, ui.item.itemChek1, ui.item.itemChek2, ui.item.itemChek3, ui.item.itemChek4, ui.item.itemChek7, ui.item.itemChek5, ui.item.itemChek10, ui.item.itemChek11, ui.item.img_);
        $('#txtItemSearchCart').val('');
        return false;
    }
}
catch (a) {

}



function GetRewardPoints(Cust_ID) {
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetRewardPoints,
        data: "Custid=" + Cust_ID + "&VD=" + VD1(),

        success: function (resp) {

            if (resp != null)
                jQuery("label[for='lblRedeem_points'").html(resp[0].Redeem_Points);
            jQuery("label[for='lblRewardPoints'").html(resp[0].Redeem_Points);
        }
    });
}
function myFunction_() {
    $('#txtItemScan').focus();
}
function myFunction() {
    // Declare variables 
    var input, filter, table, tr, td, i;
    input = document.getElementById("txtItemSearch");
    filter = input.value.toUpperCase();
    table = document.getElementById("myTable");
    tr = table.childNodes;
    var ctVal_ = "0";
    // Loop through all table rows, and hide those who don't match the search query
    //for (i = 0; i < tr.length; i++) {
    //    td = tr[i].innerText;
    //    if (td) {
    //        if (td.toUpperCase().indexOf(filter) > -1) {
    //            tr[i].style.display = "";
    //            ctVal_ = parseFloat(ctVal_) + parseFloat(1);
    //        } else {
    //            tr[i].style.display = "none";
    //        }
    //    }
    //}
    document.getElementById("lblStockCt").innerHTML = ctVal_;
    fillItemsdataFilter_(filter);
}



function fillItemsdataFilter_(Name) {
    var SubOrgId = '';
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        getResources1(hashSplit[7], "POSInvoice");
        SubOrgId = hashSplit[1] + ',' + hashSplit[16];
    }


    //data: "subOrgId=" + strOptions + "&ItemsData=" + JSON.stringify(jsonObj),
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.getItemDataByFilter,
        //data: "subOrgId=" + SubOrgId + "," + $('#hdnCurrId').val() + "&VD=" + VD1(),
        data: "subOrgId=" + SubOrgId + "&itemCode=" + Name + "&VD=" + VD1(),
        success: function (resp) {
            var ItemsData = resp;
            document.getElementById("lblStockCt").innerHTML = resp.length;
            var li_ = "";
            document.getElementById('tbl_Item').childNodes[0].nodeValue = null;
            var MainData = '';

            for (var i = 0; i < ItemsData.length; i++) {
                var mapCode_ = "";
                var profileData_ = "";
                mapCode_ = ItemsData[i].O2.replace('$', '&');
                if (mapCode_ != '')
                    for (var k = 0; k < ItemsData[i].profileData.length; k++) {
                        var cols = ItemsData[i].profileData[k].split('~');
                        profileData_ = profileData_ + "@" + cols[0] + "#" + cols[1] + "#" + cols[2] + "#" + cols[3] + "#" + cols[4] + "#" + cols[5] + "#" + cols[6] + "#" + cols[8] + "#" + cols[9] + "#" + cols[10] + "#" + cols[11] + "#" + cols[12] + "#" + cols[13] + "#" + cols[14] + "#" + cols[15] + "#" + cols[16] + "#" + cols[17];
                    }

                var img_ = ""; var style_ = "";
                if (ItemsData[i].img != '') {
                    img_ = ItemsData[i].img;
                    style_ = "style='background-image:url(" + img_ + ");background-size:100% 80%;background-repeat:no-repeat;display:block;width:100%;height:84px;background-color:transparent;margin-bottom:20px;text-align:center;'"
                    li_ = li_ + "<li><a href='#' onclick=onSubData(\'" + ItemsData[i].Code + "\',\'" + ItemsData[i].ItemId + "\',\'" + ItemsData[i].Name + "\',\'" + ItemsData[i].Price + "\',\'" + ItemsData[i].Disc + "\',\'" + ItemsData[i].UOMID + "\',\'" + ItemsData[i].UOMCode + "\',\'" + mapCode_.replace(" ", "") + "\',\'" + profileData_.replace(" ", "") + "\',\'" + img_ + "\');> <div class='content-body-item'>";
                    li_ = li_ + "<h5 style='font-size:12px'>" + ItemsData[i].Code + "</h5><div   " + style_ + " class='item-sec'>";
                    li_ = li_ + "<p style='margin-top:50px;color:#000000;font-size:80%;text-align:center;word-wrap:break-word;white-space:pre-line;line-height:20px;font-weight:bold'>" + ItemsData[i].Name + "</p>";
                    li_ = li_ + "</div><div style='display:none'>" + ItemsData[i].BarCode.toUpperCase() + "</div><div style='display:none'>" + mapCode_.replace(" ", "") + "</div><div style='display:none'>" + profileData_.replace(" ", "") + "</div>";
                    li_ = li_ + "<small style='float:left;clear:both'>" + ItemsData[i].Price + "</small><small style=' float:right;clear:right' >" + ItemsData[i].UOMCode + "</small>";
                    li_ = li_ + "<br/><small >" + ItemsData[i].Alias + "</small></div>";
                    li_ = li_ + "</a></li>";
                }
                else {
                    li_ = li_ + "<li><a href='#' onclick=onSubData(\'" + ItemsData[i].Code + "\',\'" + ItemsData[i].ItemId + "\',\'" + ItemsData[i].Name + "\',\'" + ItemsData[i].Price + "\',\'" + ItemsData[i].Disc + "\',\'" + ItemsData[i].UOMID + "\',\'" + ItemsData[i].UOMCode + "\',\'" + mapCode_.replace(" ", "") + "\',\'" + profileData_.replace(" ", "") + "\');> <div class='content-body-item'>";
                    li_ = li_ + "<h5 style='font-size:12px'>" + ItemsData[i].Code + "</h5><div class='item-sec'>";
                    li_ = li_ + "<samp>" + ItemsData[i].Price + "</samp>";
                    li_ = li_ + "<br/><small>" + ItemsData[i].UOMCode + "</small></div><div style='display:none'>" + ItemsData[i].BarCode.toUpperCase() + "</div><div style='display:none'>" + mapCode_.replace(" ", "") + "</div><div style='display:none'>" + profileData_.replace(" ", "") + "</div>";
                    li_ = li_ + "<small >" + ItemsData[i].Name + "</small>";
                    li_ = li_ + "<br/><small >" + ItemsData[i].Alias + "</small></div>";
                    li_ = li_ + "</a></li>";
                }
                //if (ItemsData[i].img != '') {
                //    img_ = ItemsData[i].img;
                //    style_ = "style='background-image:url(" + img_ + ");background-size:100% 80%;background-repeat:no-repeat;display:block;width:84px;height:84px;background-color:transparent;margin-bottom:20px;text-align:center;'"
                //    li_ = li_ + "<li><a href='#' onclick=onSubData(\'" + ItemsData[i].Code + "\',\'" + ItemsData[i].ItemId + "\',\'" + ItemsData[i].Name + "\',\'" + ItemsData[i].Price + "\',\'" + ItemsData[i].Disc + "\',\'" + ItemsData[i].UOMID + "\',\'" + ItemsData[i].UOMCode + "\',\'" + mapCode_.replace(" ", "") + "\',\'" + profileData_.replace(" ", "") + "\',\'" + img_ + "\');> <div class='content-body-item'>";
                //    li_ = li_ + "<h5 style='font-size:12px'>" + ItemsData[i].Code + "</h5><div   " + style_ + " class='item-sec'>";
                //    li_ = li_ + "<p style='margin-top:50px;color:#000000;font-size:80%;text-align:center;word-wrap:break-word;white-space:pre-line;line-height:20px;font-weight:bold'>" + ItemsData[i].Name + "</p>";
                //    li_ = li_ + "</div><div style='display:none'>" + ItemsData[i].BarCode.toUpperCase() + "</div><div style='display:none'>" + mapCode_.replace(" ", "") + "</div><div style='display:none'>" + profileData_.replace(" ", "") + "</div>";
                //    li_ = li_ + "<small style='float:left;clear:both'>" + ItemsData[i].Price + "</small><small style=' float:right;clear:right' >" + ItemsData[i].UOMCode + "</small></div>";
                //    li_ = li_ + "</a></li>";
                //}
                //else {
                //    li_ = li_ + "<li><a href='#' onclick=onSubData(\'" + ItemsData[i].Code + "\',\'" + ItemsData[i].ItemId + "\',\'" + ItemsData[i].Name + "\',\'" + ItemsData[i].Price + "\',\'" + ItemsData[i].Disc + "\',\'" + ItemsData[i].UOMID + "\',\'" + ItemsData[i].UOMCode + "\',\'" + mapCode_.replace(" ", "") + "\',\'" + profileData_.replace(" ", "") + "\');> <div class='content-body-item'>";
                //    li_ = li_ + "<h5 style='font-size:12px'>" + ItemsData[i].Code + "</h5><div class='item-sec'>";
                //    li_ = li_ + "<samp>" + ItemsData[i].Price + "</samp>";
                //    li_ = li_ + "<br/><small>" + ItemsData[i].UOMCode + "</small></div><div style='display:none'>" + ItemsData[i].BarCode.toUpperCase() + "</div><div style='display:none'>" + mapCode_.replace(" ", "") + "</div><div style='display:none'>" + profileData_.replace(" ", "") + "</div>";
                //    li_ = li_ + "<small>" + ItemsData[i].Name + "</small></div>";
                //    li_ = li_ + "</a></li>";
                //}

                var Code = ItemsData[i].Code.toUpperCase();
                var ItemId = ItemsData[i].ItemId;
                var Name = ItemsData[i].Name;
                var Price = ItemsData[i].Price;
                var Disc = ItemsData[i].Disc;
                var UOMCode = ItemsData[i].UOMCode;
                var UOMName = ItemsData[i].UOMName;
                var UOMID = ItemsData[i].UOMID;
                var BarCode = ItemsData[i].BarCode.toUpperCase();
                var StockData = ItemsData[i].StockData;
                var Item_Group_Code = ItemsData[i].Item_Group_Code;
                var Item_Group_Id = ItemsData[i].Item_Group_Id;
                var O1 = ItemsData[i].O1;
                var img_ = ItemsData[i].img;
                if (MainData != '')
                    MainData = MainData + Code + '~' + ItemId + '~' + Name + '~' + Price + '~' + Disc + '~' + UOMCode + '~' + UOMName + '~' + UOMID + '~' + BarCode + '~' + StockData + '~' + mapCode_ + '~' + profileData_ + '~' + img_ + '$';
                else
                    MainData = Code + '~' + ItemId + '~' + Name + '~' + Price + '~' + Disc + '~' + UOMCode + '~' + UOMName + '~' + UOMID + '~' + BarCode + '~' + StockData + '~' + mapCode_ + '~' + profileData_ + '~' + img_ + '$';
            }

            document.getElementById('tbl_Item').innerHTML = "<ul  id='myTable'>" + li_ +
                                       "</ul>";

            $('#tbl_Item tfoot li').remove();
            //$('#paging-footer')[0].innerHTML = "";
            var ct_ = parseFloat(parseFloat(parseFloat(ItemsData.length / 25).toFixed(0)) + parseFloat(1));

            if (ct_ >= 5) {
                ct_ = 5;
            }
            if (ItemsData.length <= 25) {
                ct_ = 1;
            }
            //$('#tbl_Item ul').paginathing({
            //    perPage: 25,
            //    limitPagination: ct_,
            //    containerClass: 'panel-footer',
            //    pageNumbers: false
            //})
            //data_ = $('#tbl_Item tfoot').find('ul li');
            //$('#tbl_Item tfoot').find('ul li').appendTo($('#paging-footer'));
            //var itemDataLocal = "<ul id='myTable'>" + li_ + "</ul>";


        },
        error: function (e) {

        }
    });

}

//#itempopup

function openItem() {

    $('#txtUOMitem').val('EAC/Each');
    $('#hdnUOMitemId').val('1009');
    var ItemScantxt = document.getElementById("txtItemScan").value;
    $('#txtCounterCode').val(ItemScantxt);
    $('#txtCounterName').val(ItemScantxt);
    $('#txtBarcode').val(ItemScantxt);
    $('#ModalUploadItemsPopup').modal();
}
function taxApplicabilty() {

    if ($('#ChckTaxAp').prop('checked') == true) {
        $('#ChckTaxOnMRPAp').prop('checked', true);
        $('#ChckTaxOnMRPAp').prop('disabled', false);
        $('#lblTaxValue').css('display', 'block');
        $('#txtTaxValue').css('display', 'block');
    }
    else {
        $('#ChckTaxOnMRPAp').prop('checked', false);
        //$("#ChckTaxOnMRPAp").attr("disabled", "disabled");
        $('#ChckTaxOnMRPAp').prop('disabled', true);
        $('#lblTaxValue').css('display', 'none');
        $('#txtTaxValue').css('display', 'none');

    }
}
function IsNumericKey(e) {
    var specialKeys = new Array();
    specialKeys.push(8);
    var keyCode = e.which ? e.which : e.keyCode

    var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1 || keyCode == 46);
    if (ret == false) {
        return ret;
    }
    else {

    }

}
function IsNumeric(e) {

    var d_ = $('#txtPriceItem').val().split('.');

    if (d_.length == 2) {
        if (e.keyCode == 46) {
            return false;
        }
        if (d_[1].length == 4) {
            return false;
        }
    }
    if (d_.length == 3) {
        return false;
    }
    var specialKeys = new Array();
    specialKeys.push(8);
    var keyCode = e.which ? e.which : e.keyCode

    var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1 || keyCode == 46);
    if (ret == false) {
        return ret;
    }
    else {

    }
}

$("#txtUOMitem").autocomplete({
    source: function (request, response) {
        var HashValues = (window.localStorage.getItem("hashValues"));
        if (HashValues != null && HashValues != '') {
            var hashSplit = HashValues.split('~');
            SubOrgId = hashSplit[1];
        }
        var param = "VD=" + VD1() + "&searchkey=" + $('#txtUOMitem').val() + "";
        $.ajax({
            url: CashierLite.Settings.GetUOM,
            data: param,
            dataType: "json",
            type: "Get",
            //contentType: "application/json; charset=utf-8",
            success: function (data) {

                response($.map(data, function (item) {
                    return {
                        label: item.UOMCode + '/' + item.UOMName,
                        val: item.UOMID
                    }
                }))
            },
            error: function (response) {
            },
            failure: function (response) {
            }
        });

    },
    change: function (e, i) {
        if (i.item) {

        }
        else {
            $('#txtUOMitem').val('');
            $('#hdnUOMitemId').val('');

        }
    },
    select: function (e, i) {
        $('#hdnUOMitemId').val(i.item.val);


    },
    minLength: 1
});

function saveitem() {

    if ($("#txtCounterCode").prop("disabled") == false && $('#txtCounterCode').val() == "") {
        var $textAndPic = $('<div></div>');
        $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        //$textAndPic.append('<div class="alert-message">Please enter Code</div>');
        $textAndPic.append('<div class="alert-message">' + $('#hdnEntrCode').val() + '</div>');
        BootstrapDialog.alert($textAndPic);
        return false;
    }
    if ($('#txtCounterName').val() == "" && $('#txtCounterName').val().trim().length == 0) {
        var $textAndPic = $('<div></div>');
        $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        //$textAndPic.append('<div class="alert-message">Please enter Name</div>');
        $textAndPic.append('<div class="alert-message">' + $('#hdnEntrName').val() + '</div>');
        BootstrapDialog.alert($textAndPic);
        return false;
    }

    if ($('#txtUOMitem').val() == "" && $('#txtUOMitem').val().trim().length == 0) {
        var $textAndPic = $('<div></div>');
        $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        $textAndPic.append('<div class="alert-message">Please Select UOM</div>');
        BootstrapDialog.alert($textAndPic);
        return false;
    }
    if ($(ChckTaxOnMRPAp).prop('checked') == true) {
        if ($('#txtTaxValue').val() == "" && $('#txtTaxValue').val().trim().length == 0) {
            var $textAndPic = $('<div></div>');
            $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
            $textAndPic.append('<div class="alert-message">Please enter Tax(VAT%)</div>');
            BootstrapDialog.alert($textAndPic);
            return false;
        }
    }
    if ($('#txtPriceItem').val() == "" && $('#txtPriceItem').val().trim().length == 0) {
        var $textAndPic = $('<div></div>');
        $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        //$textAndPic.append('<div class="alert-message">Please enter Name</div>');
        $textAndPic.append('<div class="alert-message">Please Enter Price</div>');
        BootstrapDialog.alert($textAndPic);
        return false;
    }

    ItemSavePP('');
    $('#txtItemScan').val('');

}

function ItemSavePP(upCheck) {

    Jsonobj = [];
    var item = {};
    if ($('#hdnTransId').val() == "") {
        item["TransId"] = "";
    }
    else {
        item["TransId"] = $('#hdnTransId').val();
    }
    item["SubOrgID"] = $('#hdnBranchId').val();
    item["CompanyId"] = $('#CompanyId').val();
    item["CounterNumber"] = $('#txtCounterCode').val();
    item["CounterName"] = $('#txtCounterName').val();
    item["UserId"] = $('#hdnUserId').val();
    item["OBS"] = $('#hdnOBS').val();
    item["LANGID"] = $('#hdnLanguage').val();
    item["LASTUPDATEDDATE"] = $('#hdnLastUpdatedDate').val();
    item["Status"] = "Y";

    if (upCheck != '') {
        if ($('#hdnScreenMode').val() == "Active")
            item["Status"] = "Y";
        else
            item["Status"] = "N";
    }
    item["ItemCode"] = $('#txtCounterCode').val();
    item["ItemName"] = $('#txtCounterName').val();
    item["UOMID"] = $('#hdnUOMitemId').val();
    item["BarCode"] = $('#txtBarcode').val();
    item["Desc"] = "";
    item["Alias"] = $('#txtAliasName').val();
    item["Price"] = "0"
    item["TAXAP"] = 'Y';
    item["TAXMRP"] = 'R';

    if ($('#ChckTaxOnMRPAp').prop('checked') == false) {
        item["TAXMRP"] = 'F';
    }
    if ($('#ChckTaxAp').prop('checked') == false) {
        item["TAXAP"] = 'N';
        item["TAXMRP"] = '';
        item["TaxAmt"] = "0";
    }
    else {
        item["TaxAmt"] = $('#txtTaxValue').val();
    }
    if ($('#txtPriceItem').val() != "") {
        item["Price"] = $('#txtPriceItem').val();
    }
    var vd_ = window.localStorage.getItem("UUID");
    item["img"] = "";
    item["VD"] = vd_;
    Jsonobj.push(item);

    var ItemsData = new Object();
    ItemsData.Data = JSON.stringify(Jsonobj);
    var ComplexObject = new Object();
    ComplexObject.ItemsData = ItemsData;
    var data = JSON.stringify(ComplexObject);

    $.ajax({
        type: 'POST',
        url: CashierLite.Settings.ItemSave,
        data: data,
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function (resp) {
            resp = resp.ItemSaveResult;
            var result = resp.split(',');
            if (result[0] == "100000") {
                var $textAndPic = $('<div></div>');
                $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-info-circle info-alertSuc"></i>');
                $textAndPic.append('<div class="alert-message">' + $('#txtCounterCode').val() + ' ' + $('#hdnRecSaveSucc').val() + '</div>');
                BootstrapDialog.alert($textAndPic);
                clearControls();
                $('#ModalUploadItemsPopup').modal('hide');

            }
            if (result[0] == "100001") {
                var $textAndPic = $('<div></div>');
                $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                $textAndPic.append('<div class="alert-message">' + $('#txtCounterCode').val() + ' ' + $('#hdnICodeAlreadyExist').val() + '</div>');
                BootstrapDialog.alert($textAndPic);
                return false;
            }
            if (result[0] == "1000020") {
                var $textAndPic = $('<div></div>');
                $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                $textAndPic.append('<div class="alert-message">' + $('#hdnInvalData').val() + '</div>');
                BootstrapDialog.alert($textAndPic);
                return false;
            }
        },
        error: function (e) {
            var $textAndPic = $('<div></div>');
            $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
            $textAndPic.append('<div class="alert-message">' + $('#hdnInvalData').val() + '</div>');
            BootstrapDialog.alert($textAndPic);
            return false;
        }
    });
}

function clearControls() {
    $('#txtCounterCode').val("");
    $('#txtCounterName').val("");
    $("#txtDescription").val("");
    $('#txtBarcode').val("");
    $('#hdnTransId').val("");
    $('#hdnLastUpdatedDate').val("");
    $("#txtUOMitem").val("");
    $("#txtAliasName").val("");
    $("#txtPriceItem").val("");
    $('#ChckTaxAp').prop('checked', true);
    $('#ChckTaxOnMRPAp').prop('checked', true);
    $('#ChckTaxOnMRPAp').prop('disabled', false);
    $('#lblTaxValue').css('display', 'block');
    $('#txtTaxValue').css('display', 'block');
    closeitem();
}

function closeitem() {
    $('#ModalUploadimagePopup').modal('hide');
}

//#enditempopup


function showCuspop() {
    jQuery("label[for='lblCustomerEnroll'").html("Customer Enroll");
    popupSts = 1;
    clearPOPUP();
    $('#ModalHomeDelivery').modal('show');

}


function Itemdetect() {

    var ItemCode = $('#txtItemScan').val().toUpperCase();
    var dup_ = "0";
    var strOptions = $('#hdn_CusId').val();
    if (strOptions == '') {
        strOptions = GenarateGustId() + '-01';
        strOptions1 = GenarateGustName();
        $('#hdn_CusId').val(strOptions);
    }
    if (strOptions != '') {
        if ($("#txtItemScan").val().trim() != '') {
            var SubOrgId = '';
            var HashValues = (window.localStorage.getItem("hashValues"));
            if (HashValues != null && HashValues != '') {
                var hashSplit = HashValues.split('~');
                getResources1(hashSplit[7], "POSInvoice");
                SubOrgId = hashSplit[1] + ',' + hashSplit[16];
            }
            $.ajax({
                url: CashierLite.Settings.getItemDataFromBarcode,
                data: "VD=" + VD1() + "&SubOrgId=" + SubOrgId + "&Barcode=" + $("#txtItemScan").val().trim(),
                success: function (resp) {

                    if (resp.length > 0) {
                        var mapCode_ = "";
                        var profileData_ = "";
                        mapCode_ = resp[0].O2.replace('$', '&');
                        if (mapCode_ != '')
                            for (var k = 0; k < resp[0].profileData.length; k++) {
                                var cols = resp[0].profileData[k].split('~');
                                profileData_ = profileData_ + "@" + cols[0] + "#" + cols[1] + "#" + cols[2] + "#" + cols[3] + "#" + cols[4] + "#" + cols[5] + "#" + cols[6] + "#" + cols[8] + "#" + cols[9] + "#" + cols[10] + "#" + cols[11] + "#" + cols[12] + "#" + cols[13] + "#" + cols[14] + "#" + cols[15] + "#" + cols[16] + "#" + cols[17];
                            }
                        onSubData(resp[0].Code, resp[0].ItemId, resp[0].Name, resp[0].Price, resp[0].Disc, resp[0].UOMID, resp[0].UOMCode, mapCode_, profileData_, resp[0].img);
                        $('#txtItemScan')[0].style = "";
                    }
                    else {
                        $('#txtItemScan')[0].style = "background-color:red;color:white";
                    }
                }
            });





            //var v = JSON.stringify(window.localStorage.getItem("itemDataLocal"));
            //if (v != "") {
            //    var cus_ = v.split('$');

            //    //MainData = MainData + Code + ':' + ItemId + ':' + Name + ':' + Price + ':' + Disc + '$';
            //    for (var i = 0; i < cus_.length; i++) {
            //        var itemChek = cus_[i].split('~');
            //        if (itemChek.length > 1)
            //            if (ItemCode == itemChek[8].replace('"', '')) {
            //                onSubData(itemChek[0].replace('"', ''), itemChek[1], itemChek[2], itemChek[3], itemChek[4], itemChek[7], itemChek[5], itemChek[10], itemChek[11], itemChek[12]);
            //                $('#txtItemScan').val('');
            //                //Duplicate(itemChek[1].replace('"', ''), itemChek[3], itemChek[4]);
            //                dup_ = "1";

            //                break;
            //            }
            //            else if (ItemCode == itemChek[0].replace('"', '')) {
            //                onSubData(itemChek[0].replace('"', ''), itemChek[1], itemChek[2], itemChek[3], itemChek[4], itemChek[7], itemChek[5], itemChek[10], itemChek[11], itemChek[12]);
            //                $('#txtItemScan').val('');
            //                //Duplicate(itemChek[1].replace('"', ''), itemChek[3], itemChek[4]);
            //                dup_ = "1";

            //                break;
            //            }

            //    }

            //}
            if (dup_ == "0") {
                //$('#txtItemScan').val('');

                //var $textAndPic = $('<div></div>');
                //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                ////$textAndPic.append('<div class="alert-message">Item Not Available</div>');
                //$textAndPic.append('<div class="alert-message">' + $('#hdnItmNotAvail').val() + '</div>');
                //BootstrapDialog.alert($textAndPic);

            }
            else {
                $('#txtItemScan').val('');
            }
        }
        else {
            $('#txtItemScan')[0].style = "";
        }

    }
    else {
        var $textAndPic = $('<div></div>');
        $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        //$textAndPic.append('<div class="alert-message">Please select Customer</div>');
        $textAndPic.append('<div class="alert-message">' + $('#hdnSelCust').val() + '</div>');
        BootstrapDialog.alert($textAndPic);

    }

}

function printLocal() {
    //
    window.localStorage.setItem('Inv', '0');
    if ($('#hdnISPRINT').val() == "true") {
        $('#myModalprint').modal();
        //$('#myModalprint').css('display', "block");

    } else {
        //var divContents = document.getElementById("div_invoice_print").innerHTML;
        //var printWindow = window.open('', '', 'height=200,width=400');
        //printWindow.document.write('<html><head><title>DIV Contents</title>');
        //printWindow.document.write('</head><body >');
        //printWindow.document.write(divContents);
        //printWindow.document.write('</body></html>');
        //printWindow.document.close();
        //printWindow.print();
       
        $('#myModalprint').css('display', "none");
    }

    GetItemDetails();
}

function printServer() {
    var hashSplit;
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        hashSplit = HashValues.split('~');
    }
    window.localStorage.setItem('Inv', '0');
    $('#myModalprint').css('display', "none");
    var divContents = document.getElementById("div_invoice_print").innerHTML;
    //var printWindow = window.open('', '', 'height=200,width=400');
    //printWindow.document.write('<html><head><title>DIV Contents</title>');
    //printWindow.document.write('</head><body >');
    //printWindow.document.write(divContents);
    //printWindow.document.write('</body></html>');
    //printWindow.document.close();
    //printWindow.print();
    GetItemDetails();
    //$('#myModalprint').css('display', "none");
    if ($('#hdnISPRINT').val() == "true") {
        $('#myModalprint').modal();
        //$('#myModalprint').css('display', "block");

    } else {
        $('#myModalprint').css('display', "none");
    }
    var param = {
        invoiceId: window.localStorage.getItem('InvoiceId'), connectionString: window.localStorage.getItem('Connection'), printerName: hashSplit[23]
    };
    $.ajax({
        type: 'GET',
        //url: CashierLite.Settings.DirectPrint,
        url: hashSplit[24] + "/DirectPrint",
        data: param,
        dataType: 'JSONP',
        crossDomain: true,
        contentType: "application/json; charset=utf-8",
        success: function () {
            if ($('#hdnISPRINT').val() == "true") {
                $('#myModalprint').modal();
                //$('#myModalprint').css('display', "block");

            } else {
                $('#myModalprint').css('display', "none");
            }
            return false;
        },
        error: function (e) {
        }
    });
}

function printDiv() {

    var hashSplit;
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        hashSplit = HashValues.split('~');
    }
    if ((hashSplit[25] == '1') || hashSplit[25] == '') {
        printLocal();
    } else {
        printServer()
    }
}
function printDivA1() {

    var divContents = document.getElementById("div_invoice_print").innerHTML;
    var printWindow = window.open('', '', 'height=200,width=400');
    printWindow.document.write('<html><head><title>DIV Contents</title>');
    printWindow.document.write('</head><body >');
    printWindow.document.write(divContents);
    printWindow.document.write('</body></html>');
    printWindow.document.close();
    printWindow.print();
    $('#myModalprint').css('display', "none");

}
function PrintLoad() {
    var SubOrg_Id;
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        SubOrg_Id = hashSplit[1];
    }


    var param = {
        SubOrgId: SubOrg_Id, VD: VD1()
    };
    $.ajax({
        url: CashierLite.Settings.GetPrintCustomizeStyles,
        data: param,
        type: "GET",
        success: function (data) {
            try {
                $('#tblheader_1').html("");
                $('#divfooter').html("");
                $('#tblheader_1').append('<div style="text-align:' + data[0].HeaderFontAlignment + ';font-family:' + data[0].Headerfont + '; font-style:' + data[0].HeaderFontStyle + ';font-size:' + data[0].HeaderFontSize + 'px">' + data[0].HeaderText + '</div>');
                $('#tblheader_1').append('<div style="text-align:' + data[0].HeaderFontAlignment + ';font-family:' + data[0].Headerfont + '; font-style:' + data[0].HeaderFontStyle + ';font-size:' + data[0].HeaderFontSize + 'px">' + data[0].HeaderInfo + '</div>');
                $('#divfooter').append('<div style="text-align:' + data[0].FooterFontAlignment + ';font-family:' + data[0].FooterFont + '; font-style:' + data[0].FooterFontStyle + ';font-size:' + data[0].FooterFontSize + 'px">' + data[0].FooterText + '</div>');
                $('#tblheader_2').css("text-align", data[0].BodyAlignment);
                $('#tblheader_3').css("text-align", data[0].BodyAlignment);
                $('#tblheader_5').css("text-align", data[0].BodyAlignment);
                $('#hdnISPRINT').val(data[0].IsPrint);
            }
            catch (a) {

            }
            $('#txtItemScan').focus();
        },
    });
}
function Sampleprint() {
    tets();
    $('#myModalprint').show();
}


function DashBoardDataFill() {
    var SubOrg_Id;
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        SubOrg_Id = hashSplit[1];
    }

    var param = {
        SubOrgId: SubOrg_Id, CounterId: window.localStorage.getItem('COUNTERID'), ShiftId: window.localStorage.getItem('SHIFTID'), SalespersonId: window.localStorage.getItem('CashierID'),
        roleId: window.localStorage.getItem('RoleId'), VD: VD1()
    };
    $.ajax({
        url: CashierLite.Settings.GetDashboardCounts,
        data: param,
        type: "GET",
        success: function (data) {

            var d_ = data.split(',');
            $('#lblsalesCTDash').text(d_[0]);
            $('#lblsalesCUSDash').text(d_[1]);
            $('#lblsalesITEMDash').text(d_[2]);
            $('#lblTodayCustomers').text(d_[3]);
        },
    });
}


function loadWeekGraphData() {

    var subOrgId = '';
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        subOrgId = hashSplit[1];
    }


    Chart.plugins.register({
        afterDatasetsDraw: function (chartInstance, easing) {
            // To only draw at the end of animation, check for easing === 1
            var ctx = chartInstance.chart.ctx;

            chartInstance.data.datasets.forEach(function (dataset, i) {
                var meta = chartInstance.getDatasetMeta(i);
                if (!meta.hidden) {
                    meta.data.forEach(function (element, index) {
                        // Draw the text in black, with the specified font
                        ctx.fillStyle = 'rgb(0, 0, 0)';

                        var fontSize = 16;
                        var fontStyle = 'normal';
                        var fontFamily = 'Helvetica Neue';
                        ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

                        // Just naively convert to string for now
                        var dataString = dataset.data[index].toString();

                        // Make sure alignment settings are correct
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'middle';

                        var padding = 5;
                        var position = element.tooltipPosition();
                        ctx.fillText(dataString, position.x, position.y - (fontSize / 2) - padding);
                    });
                }
            });
        }
    });
    //var MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var color = Chart.helpers.color;
    var barChartData = '';

    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetDashBoardWeekSalesByCounter,
        data: "CounterId=" + window.localStorage.getItem('CashierID') + "&VD=" + VD1(),
        success: function (resp) {

            var x = []; var y = [];


            for (var i = 0; i < resp.length; i++) {

                var s_ = resp[i].Day.split(' ');
                x.push(s_[0]);


                y.push(parseFloat(resp[i].SalesCount).toFixed(2));
            }

            barChartData = {
                labels: x,
                datasets: [{
                    label: 'Sale',
                    backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
                    borderColor: window.chartColors.blue,
                    borderWidth: 1,
                    data: y
                }, {
                    type: 'line',
                    label: 'Line View',
                    borderColor: window.chartColors.red,
                    borderWidth: 2,
                    fill: false,
                    data: y,

                }
                ]
            }



            var ctx = document.getElementById("canvasWeek").getContext("2d");
            window.myBar = new Chart(ctx, {
                type: 'bar',
                data: barChartData,
                options: {
                    responsive: true,
                    title: {
                        display: true,
                        text: ''
                    },
                }
            });
        }
    });


    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetSp_POSMargin,
        data: "SubOrgId=" + subOrgId + "&VD=" + VD1(),
        success: function (resp) {
            var x = resp.split(',');

            $('#lblSales_M').text(parseFloat(x[0]).toFixed(4));
            $('#lblRefunds_M').text(parseFloat(x[2]).toFixed(4));
            $('#lblRevenue_M').text(parseFloat(x[3]).toFixed(4));
            $('#lblCost_M').text(parseFloat(x[1]).toFixed(4));
            $('#lblProfit_M').text(parseFloat(x[4]).toFixed(4));


            //  if (parseFloat(x[4]).toFixed(4) > 0) {
            //if (parseFloat(x[1]).toFixed(4) > 0) {

            var ProfitPe_ = parseFloat(x[4]).toFixed(4) / parseFloat(x[1]).toFixed(4);
            //ProfitPe_ != 'NaN' ? $('#lblProfitPercent_M').text('0.00'): $('#lblProfitPercent_M').text(parseFloat(parseFloat(ProfitPe_) * 100).toFixed(4)) ;
            if (String(ProfitPe_) == "NaN" || String(ProfitPe_) == "Infinity") {
                $('#lblProfitPercent_M').text('0.00')
            } else {
                $('#lblProfitPercent_M').text(parseFloat(parseFloat(ProfitPe_) * 100).toFixed(4))
            }
            //$('#lblProfitPercent_M').text(parseFloat(parseFloat(ProfitPe_) * 100).toFixed(4));
            //}
            //   }
        }
    });

    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetDashBoardTenderWiseSales,
        data: "subOrgId=" + subOrgId + "&VD=" + VD1(),
        success: function (resp) {
            var data = [];
            var subData = { TenderType: "", TenderTypeValue: "" };
            var P21 = {
            }
            for (var i = 0; i < resp.length; i++) {
                P21 = {
                }
                P21["TenderType"] = resp[i].ITEMNAME;
                P21["TenderTypeValue"] = resp[i].AMOUNT;
                data.push(P21);
            }
            AmCharts.makeChart("TodayTenderSale", {
                "type": "pie",
                "dataProvider": data,
                "pullOutDuration": 3,
                "gradientRatio": [-0.4, -0.4, -0.4, -0.4, -0.4, -0.4, 0, 0.1, 0.2, 0.1, 0, -0.2, -0.5],
                "innerRadius": "60%",
                "pullOutRadius": "5",
                "depth3D": 0,
                "titleField": "TenderType",
                "valueField": "TenderTypeValue",
                "labelText": "[[title]]:[[TenderTypeValue]]",
                //   "balloonText": "[[TenderType]]<br><span style='font-size:14px'><b>[[TenderTypeValue]]</b> </span>"

            });


        }
    });

}

function loadMonthGraphData() {


    Chart.plugins.register({
        afterDatasetsDraw: function (chartInstance, easing) {
            // To only draw at the end of animation, check for easing === 1
            var ctx = chartInstance.chart.ctx;

            chartInstance.data.datasets.forEach(function (dataset, i) {
                var meta = chartInstance.getDatasetMeta(i);
                if (!meta.hidden) {
                    meta.data.forEach(function (element, index) {
                        // Draw the text in black, with the specified font
                        ctx.fillStyle = 'rgb(0, 0, 0)';

                        var fontSize = 16;
                        var fontStyle = 'normal';
                        var fontFamily = 'Helvetica Neue';
                        ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

                        // Just naively convert to string for now
                        var dataString = dataset.data[index].toString();

                        // Make sure alignment settings are correct
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'middle';

                        var padding = 5;
                        var position = element.tooltipPosition();
                        ctx.fillText(dataString, position.x, position.y - (fontSize / 2) - padding);
                    });
                }
            });
        }
    });
    //var MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var color = Chart.helpers.color;
    var barChartData = '';
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetDashBoardMonthSalesByCounter,
        data: "CounterId=" + window.localStorage.getItem('CashierID') + "&VD=" + VD1(),
        success: function (resp) {

            var x = []; var y = [];
            for (var i = 0; i < resp.length; i++) {
                var s_ = resp[i].Day.split(' ');
                x.push(s_[0]);
                y.push(parseFloat(resp[i].SalesCount).toFixed(2));
            }

            barChartData = {
                type: 'bar',
                labels: x,
                datasets: [{
                    label: 'Sale',
                    backgroundColor: color(window.chartColors.blue).alpha(0.6).rgbString(),
                    borderColor: window.chartColors.blue,
                    borderWidth: 1,
                    data: y
                }
                ,
                {
                    type: 'line',
                    label: 'Line View',
                    borderColor: window.chartColors.red,
                    borderWidth: 2,
                    fill: false,
                    data: y,

                }
                ]
            }



            var ctx = document.getElementById("canvasMonth").getContext("2d");
            window.myBar = new Chart(ctx, {
                type: 'bar',
                data: barChartData,
                options: {
                    responsive: true,
                    title: {
                        display: true,
                        text: ''
                    },
                }
            });
        }
    });



}

function loadYearGraphData() {


    Chart.plugins.register({
        afterDatasetsDraw: function (chartInstance, easing) {
            // To only draw at the end of animation, check for easing === 1
            var ctx = chartInstance.chart.ctx;

            chartInstance.data.datasets.forEach(function (dataset, i) {
                var meta = chartInstance.getDatasetMeta(i);
                if (!meta.hidden) {
                    meta.data.forEach(function (element, index) {
                        // Draw the text in black, with the specified font
                        ctx.fillStyle = 'rgb(0, 0, 0)';

                        var fontSize = 16;
                        var fontStyle = 'normal';
                        var fontFamily = 'Helvetica Neue';
                        ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

                        // Just naively convert to string for now
                        var dataString = dataset.data[index].toString();

                        // Make sure alignment settings are correct
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'middle';

                        var padding = 5;
                        var position = element.tooltipPosition();
                        ctx.fillText(dataString, position.x, position.y - (fontSize / 2) - padding);
                    });
                }
            });
        }
    });
    //var MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var color = Chart.helpers.color;
    var barChartData = '';
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetDashBoardYearSalesByCounter,
        data: "CounterId=" + window.localStorage.getItem('CashierID') + "&VD=" + VD1(),
        success: function (resp) {

            var x = []; var y = [];


            for (var i = 0; i < resp.length; i++) {

                var s_ = resp[i].Day;
                x.push(s_);


                y.push(parseFloat(resp[i].SalesCount).toFixed(2));
            }

            barChartData = {
                labels: x,
                datasets: [{
                    label: 'Sale',
                    backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
                    borderColor: window.chartColors.blue,
                    borderWidth: 1,
                    data: y
                }, {
                    type: 'line',
                    label: 'Line View',
                    borderColor: window.chartColors.red,
                    borderWidth: 2,
                    fill: false,
                    data: y,

                }
                ]
            }



            var ctx = document.getElementById("canvasYear").getContext("2d");
            window.myBar = new Chart(ctx, {
                type: 'bar',
                data: barChartData,
                options: {
                    responsive: true,
                    title: {
                        display: true,
                        text: ''
                    },
                }
            });
        }
    });



}

$("#txtHomeDelPhNo").keypress(function (event) {
    if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        //alert('hello');
        if ((event.which != 46 || $(this).val().indexOf('.') != -1)) {
            return false;
        }
        event.preventDefault();
    }
    if (this.value.indexOf(".") > -1 && (this.value.split('.')[1].length > 1)) {
        return false;
        event.preventDefault()
    }
});

$("#txtDiscount").keypress(function (event) {
    if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        //alert('hello');
        if ((event.which != 46 || $(this).val().indexOf('.') != -1)) {
            return false;
        }
        event.preventDefault();
    }
    if (this.value.indexOf(".") > -1 && (this.value.split('.')[1].length > 1)) {
        return false;
        event.preventDefault()
    }
});

function getResources1(langCode, pageCode) {
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.getResource,
        data: "langCode=" + langCode + "&pageCode=" + pageCode,
        success: function (resp) {
            setLabels1(resp);
        }
    });
}

var homeDeliveryAddress = '';


$('#txtHomeDelName').val('');
$('#txtHomeDelAddr1').val('');
$('#txtHomeDelAddr2').val('');
$('#txtHomeDelCity').val('');
$('#txtHomeDelState').val('');
$('#txtHomeDelCountry').val('');
$('#txtHomeDelZipCode').val('');
$('#txtHomeDelPhNo').val('');
$('#txtHomeDelMobileNo').val('');
$('#txtHomeDelEmail').val('');




function saveHomeAddressPOP(sts) {
    if ($('#txtHomeDelPhNo').val() == '') {
        var $textAndPic = $('<div></div>');
        $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        //$textAndPic.append('<div class="alert-message">Please Enter Transaction Reference</div>');
        // $textAndPic.append('<div class="alert-message">Please enter Contact No.</div>');
        $textAndPic.append('<div class="alert-message">' + $('#hdnPlsContactNo').val() + '</div>');
        BootstrapDialog.alert($textAndPic);
        return false;
    }
    if ($('#txtHomeDelName').val() == '') {
        var $textAndPic = $('<div></div>');
        $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        //$textAndPic.append('<div class="alert-message">Please Enter Transaction Reference</div>');
        //$textAndPic.append('<div class="alert-message">Please enter Name</div>');
        $textAndPic.append('<div class="alert-message">' + $('#hdnMsgCounterName').val() + '</div>');
        BootstrapDialog.alert($textAndPic);
        return false;
    }
    if ($('#txtHomeDelAddr').val() == '') {
        var $textAndPic = $('<div></div>');
        $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        //$textAndPic.append('<div class="alert-message">Please Enter Transaction Reference</div>');
        //$textAndPic.append('<div class="alert-message">Please enter Address</div>');
        $textAndPic.append('<div class="alert-message">' + $('#hdnPlsEnterAddrss').val() + '</div>');
        hdnPlsEnterAddrss
        BootstrapDialog.alert($textAndPic);
        return false;
    }
    homeDeliveryAddress = $('#txtHomeDelName').val() + '~' + $('#txtHomeDelAddr').val() + '~' + $('#txtHomeDelZipCode').val() + '~' + $('#txtHomeDelPhNo').val() + '~' + $('#txtHomeDelEmail').val();
    if (sts == '2') {
        $('#ModalHomeDelivery').modal('hide');
    } else {
        $('#ModalHomeDelivery').modal('show');
    }
    if (popupSts == 1) {
        insertCustomer();
    }


}

function cancelHomeDelivery() {
    $('#chkHomeDelivery').prop('checked', false);
    $('#txtHomeDelName').val('');
    $('#txtHomeDelAddr').val('');
    $('#txtHomeDelZipCode').val('');
    $('#txtHomeDelPhNo').val('');
    $('#txtHomeDelEmail').val('');
}

var address;
$('#btnCheckOK').prop('disabled', false);
function GetStateCountryByCity1(CityId) {
    CityData = [];
    CityData = JSON.parse((window.localStorage.getItem("FillCountryByCity")));
    for (var i = 0; i < CityData.length; i++) {
        if (CityData[i].CityId == CityId) {
            address = address + "," + CityData[i].CityName + "," + CityData[i].RegionName + "," + CityData[i].CountryName + ",";
        }
    }
    $('#txtHomeDelAddr').val(address.slice(0, -1));
    $('#ModalHomeDelivery').modal('show');
}
function HomeDelivery(type) {
    var checkedStatus = '0';

    if (type == '1') {

        if ($('#chkHomeDelivery').prop('checked') == true) {
            $('#chkHomeDelivery').prop('checked', false);
        }
        else {
            $('#chkHomeDelivery').prop('checked', true);
            checkedStatus = '1';
        }
    }
    else {

        if ($('#chkHomeDelivery').prop('checked') == true) {
            checkedStatus = '1';
        }
    }

    if (checkedStatus == '1') {
        //s jQuery("label[for='lblHomeDelHeader'").html("Home Delivery");
        if ($('#hdn_CusId').val() != "" && $('#txtCusName').val().substring(0, 2) != 'G#') {
            $.ajax({
                type: 'GET',
                url: CashierLite.Settings.GetCustomerMstDoubleClick,
                data: "CustId=" + $('#hdn_CusId').val() + "&VD=" + VD1(),
                success: function (resp) {
                    var GridData = JSON.parse(resp);
                    $('#txtHomeDelName').val(GridData.QATABLE038[0].CUSTOMER_NAME);
                    address = GridData.QATABLE040[0].ADDRESS1 + ',' + GridData.QATABLE040[0].ADDRESS2;

                    $('#txtHomeDelZipCode').val(GridData.QATABLE040[0].PINCODE);
                    $('#txtHomeDelPhNo').val(GridData.QATABLE040[0].MOBILE_NO);
                    $('#txtHomeDelEmail').val(GridData.QATABLE040[0].EMAIL);
                    GatCityData();
                    GetStateCountryByCity1(GridData.QATABLE040[0].CITY);
                    $('#ModalHomeDelivery').modal('show');
                    saveHomeAddressPOP(1);
                }
            });
        }
        else {
            $('#txtHomeDelName').val('');
            $('#txtHomeDelAddr').val('');
            $('#txtHomeDelZipCode').val('');
            $('#txtHomeDelPhNo').val('');
            $('#txtHomeDelEmail').val('');
            $('#ModalHomeDelivery').modal('show');
            //saveHomeAddressPOP();
        }
        //   $("#btnCheckHomeDelOK").prop('disabled', false);
    }
    else {
        $('#ModalHomeDelivery').modal('hide');

        cancelHomeDelivery();
    }

}

function HomeDelivery1() {

    alert(1);
    if ($('#chkHomeDelivery').prop('checked') == true) {
        //s jQuery("label[for='lblHomeDelHeader'").html("Home Delivery");
        if ($('#hdn_CusId').val() != "" && $('#txtCusName').val().substring(0, 2) != 'G#') {
            $.ajax({
                type: 'GET',
                url: CashierLite.Settings.GetCustomerMstDoubleClick,
                data: "CustId=" + $('#hdn_CusId').val() + "&VD=" + VD1(),
                success: function (resp) {
                    var GridData = JSON.parse(resp);
                    $('#txtHomeDelName').val(GridData.QATABLE038[0].CUSTOMER_NAME);
                    address = GridData.QATABLE040[0].ADDRESS1 + ',' + GridData.QATABLE040[0].ADDRESS2;

                    $('#txtHomeDelZipCode').val(GridData.QATABLE040[0].PINCODE);
                    $('#txtHomeDelPhNo').val(GridData.QATABLE040[0].MOBILE_NO);
                    $('#txtHomeDelEmail').val(GridData.QATABLE040[0].EMAIL);
                    GatCityData();
                    GetStateCountryByCity1(GridData.QATABLE040[0].CITY);
                }
            });
        }
        else {
            $('#txtHomeDelName').val('');
            $('#txtHomeDelAddr').val('');
            $('#txtHomeDelZipCode').val('');
            $('#txtHomeDelPhNo').val('');
            $('#txtHomeDelEmail').val('');
            $('#ModalHomeDelivery').modal('show');
        }
        //   $("#btnCheckHomeDelOK").prop('disabled', false);
    };
}

function GetSubOrgNameWithCur1(SubOrgId) {
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetSubOrgCurrency,
        data: "SubOrgId=" + SubOrgId + "&VD=" + VD1(),
        success: function (resp) {
            var CurrData = resp;
            window.localStorage.setItem("CurData", JSON.stringify(CurrData))
            for (var i = 0; i < CurrData.length; i++) {
                //     $('#txtCurrency').val(CurrData[0].CurrencyCode + " / " + CurrData[0].CurrencyName);
                $('#hdnCurrency1').val(CurrData[0].CurrencyId);
                //         $("#txtCurrency").prop("disabled", true);
            }

        },
        error: function (e) {
        }
    });
}

function insertCustomer() {


    var HashVal = (window.localStorage.getItem("hashValues"));
    var MDate;
    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');
    }
    GetSubOrgNameWithCur1(HashValues[1])
    var Mode = "Create";

    CustomerAddress = {
    }
    CustomerData = {
    }
    var custId = guid();
    $('#hdn_CusId').val(custId);
    CustomerData["CUSTOMER_ID"] = custId;
    CustomerData["CUSTOMER_CODE"] = 'GNY-ST-CUST-007';
    CustomerData["CUSTOMER_NAME"] = $("#txtHomeDelName").val().trim();
    CustomerData["ALIAS_NAME"] = '';


    CustomerData["CUSTOMER_TYPE_ID"] = '1';


    CustomerData["CUSTOMER_GRP_ID"] = '37';
    CustomerData["COMPANY_NAME"] = '';
    CustomerData["CONTACT_PERSON"] = '';
    CustomerData["CPER_DESIG"] = '';


    var CurDate = new Date();
    var DateWithFormate = CurDate.getFullYear() + '-' + (CurDate.getMonth() + 1) + '-' + CurDate.getDate() + ' ' + CurDate.getHours() + ':' + CurDate.getMinutes() + ':' + CurDate.getSeconds() + ':' + CurDate.getMilliseconds();
    CustomerData["EFFECTIVE_DATE"] = DateWithFormate;
    CustomerData["ORDER_PRIORITY"] = 'Y';
    CustomerData["CUST_SERVICE_REP"] = '';
    CustomerData["REFERED_BY"] = '';
    CustomerData["INCOTERMS_PAYMENT"] = '';
    CustomerData["PAYMENT_MODE"] = '';
    //CustomerData["SALES_TYPE"] = $("#ddlSalesType").setItem.value();
    CustomerData["SALES_TYPE"] = 'Domestic';

    CustomerData["HEAD_OFFICE"] = '';
    CustomerData["SALES_PERSON_ID"] = '';//ID


    CustomerData["CURRENCY_ID"] = $("#hdnCurrency1").val();


    CustomerData["LANGUAGE_ID"] = HashValues[7];
    CustomerData["NEAREST_DP_ID"] = '';
    CustomerData["PREF_SHIPPER"] = '';
    CustomerData["REMARKS"] = '';
    CustomerData["STATUS"] = '';
    CustomerData["BRANCH_ID"] = HashValues[0];
    CustomerData["SUB_ORG_ID"] = HashValues[0];
    CustomerData["OBS"] = HashValues[9];

    //var DateFormate=$("#hdnDateFormate").val();
    //var HashVal = (window.localStorage.getItem("hashValues"));
    //HashValues = HashVal.split('~');

    //$("#hdnDateFormate").val(HashValues[2]);
    //var Date_ = ($("#calBirthDate").val()).split('-');
    //var BDate = Date_[2] + '-' + Date_[1] + '-' + Date_[0];
    //new Date(Date.parse(
    //var BDate = $("#calBirthDate").val().getFullYear() + '/' + ($("#calBirthDate").val().getMonth() + 1) + '/' + $("#calBirthDate").val().getDate();
    if ($("#calMarraigeDate").val() != "") {
        //var Date_ = ($("#calMarraigeDate").val()).split('-');
        //MDate = Date_[2] + '-' + Date_[1] + '-' + Date_[0];
        var MDate = '';
    }
    else {
        MDate = "";
    }
    CustomerData["TRANS_TYPE"] = 'POS';
    CustomerData["BIRTH_DATE"] = '';
    CustomerData["Marriage_Date"] = '';
    CustomerData["PARENTCUSTID"] = '';
    CustomerData["CATEGORY"] = 'SO';

    CustomerData["BRANCHID"] = HashValues[0];
    CustomerData["COMPANYID"] = HashValues[0];
    CustomerData["CREATEDBY"] = HashValues[14];
    CustomerData["CREATEDDATE"] = DateWithFormate;

    CustomerAddress["CREATEDBY"] = HashValues[14];
    CustomerAddress["CREATEDDATE"] = DateWithFormate;
    if (Mode == 'Create') {
        CustomerData["ROWGUID"] = '';
        CustomerAddress["ROWGUID"] = '';
        CustomerAddress["CustAddress_GUID"] = '';

        CustomerData["LASTUPDATEDBY"] = HashValues[14];
        CustomerData["LASTUPDATEDDATE"] = DateWithFormate;

        CustomerAddress["LASTUPDATEDBY"] = HashValues[14];
        CustomerAddress["LASTUPDATEDDATE"] = DateWithFormate;
    }
    if (Mode != 'Create') {
        CustomerData["LASTUPDATEDBY"] = $("#hdnLastUpdatedBy").val();
        CustomerData["LASTUPDATEDDATE"] = $("#hdnLastUpdatedDate").val();

        CustomerAddress["LASTUPDATEDBY"] = $("#hdnLastUpdatedBy").val();
        CustomerAddress["LASTUPDATEDDATE"] = $("#hdnLastUpdatedDate").val();
        $('#trSBInnerRow li span#lblLastUpdate').html("");
        $('#trSBInnerRow li span#lblLastUpdatedUser').html("");
        $('#trSBInnerRow li span#lblLastUpdatedDate').html("");
        //$('#trSBInnerRow li span#lblMode').html("Default");
        $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());
        //CustomerData["LASTUPDATEDBY"] = HashValues[14];
        //CustomerData["LASTUPDATEDDATE"] = DateWithFormate;

        //CustomerAddress["LASTUPDATEDBY"] = HashValues[14];
        //CustomerAddress["LASTUPDATEDDATE"] = DateWithFormate;
    }

    CustomerData["UPDATEDSITE"] = 'DBMS_REPUTIL.GLOBAL_NAME';
    CustomerData["LANGID"] = HashValues[7];

    CustomerData["PROCESS_STATUS"] = '2';
    CustomerData["insertType"] = '1';
    //Address
    CustomerAddress["CustAddress_GUID"] = '';
    CustomerAddress["CUSTOMER_ID"] = custId;
    CustomerAddress["ADD_TYPE_ID"] = 'Shipping Address';
    CustomerAddress["ADDRESS1"] = $("#txtHomeDelAddr").val().trim();
    CustomerAddress["ADDRESS2"] = '';
    CustomerAddress["CITY"] = '';
    CustomerAddress["PROVINCE"] = '';
    CustomerAddress["COUNTRY_ID"] = '';
    CustomerAddress["PINCODE"] = $("#txtHomeDelZipCode").val().trim();
    CustomerAddress["PHONE_NO"] = '';
    CustomerAddress["IP_PHONE_NO"] = '';
    CustomerAddress["MOBILE_NO"] = $("#txtHomeDelPhNo").val().trim();
    CustomerAddress["EMAIL"] = $("#txtHomeDelEmail").val().trim();
    CustomerAddress["WEB_SITE"] = '';
    CustomerAddress["CONTACT_ADDRESS"] = 'Y';
    CustomerAddress["Remarks"] = '';
    CustomerAddress["status"] = '';
    CustomerAddress["FAX_NO"] = '';
    CustomerAddress["BRANCHID"] = HashValues[0];
    CustomerAddress["COMPANYID"] = HashValues[0];
    CustomerAddress["UPDATEDSITE"] = "DBMS_REPUTIL.GLOBAL_NAME";
    CustomerAddress["LANGID"] = HashValues[7];
    objCustomerData = []; objCustomerAddress = [];
    objCustomerData.push(CustomerData);
    objCustomerAddress.push(CustomerAddress);




    //Create the customer object
    var CustomerParams = new Object();
    CustomerParams.Mode = Mode;
    CustomerParams.CustomerData = JSON.stringify(objCustomerData);
    CustomerParams.CustomerAddress = JSON.stringify(objCustomerAddress);
    CustomerParams.VD = VD1();

    //Put the customer object into a container
    var ComplexObject = new Object();
    //The property name "WebMethodArgName" must match the web method argument "WebMethodArgName"!
    ComplexObject.CustData = CustomerParams;

    //Stringify and verify the object is foramtted as expected
    var data = JSON.stringify(ComplexObject);
    $.ajax({
        type: 'POST',
        url: CashierLite.Settings.SaveCustomerData,
        data: data,
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        //data: "Mode=" + Mode + "&CustomerData=" + JSON.stringify(objCustomerData) + "&CustomerAddress=" + JSON.stringify(objCustomerAddress),
        success: function (resp) {
            if (resp != null) {
                if (resp.SaveCustomerMstDataResult == '100000') {//Success Record
                    if ($("#hdnMode").val() == 'Delete') {
                        var $textAndPic = $('<div></div>');
                        $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-info-circle info-alertSuc"></i>');
                        //$textAndPic.append('<div class="alert-message">' + $("#txtCode").val().trim() + ' Record Deleted Successfully.' + '</div>');
                        $textAndPic.append('<div class="alert-message">' + $("#txtCode").val().trim() + $('#hdnDelSucc').val() + '</div>');
                        BootstrapDialog.alert($textAndPic);
                        // $("#btnCancel").click();

                    }
                    //   $('#hdn_CusId').val('');
                    popupSts = "";
                    GetCustomers_();
                    clearPOPUP();
                    return false;
                }
                else if (resp.SaveCustomerMstDataResult == '100001') {//Duplicate Customer Code
                    $("#hdnTransId").val('');
                    var $textAndPic = $('<div></div>');
                    $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                    //$textAndPic.append('<div class="alert-message">' + $("#txtCode").val().trim() + ' Code Already exists.' + '</div>');
                    $textAndPic.append('<div class="alert-message">' + $("#txtCode").val().trim() + $('#hdnCodeAlreadyExist').val() + '</div>');
                    BootstrapDialog.alert($textAndPic);
                    return false;
                }
                else if (resp.SaveCustomerMstDataResult == 'POS10002') {//Deleted Record
                    var $textAndPic = $('<div></div>');
                    $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-info-circle info-alertSuc"></i>');
                    //$textAndPic.append('<div class="alert-message">' + $("#txtCode").val().trim() + ' Record Deleted Successfully.' + '</div>');
                    $textAndPic.append('<div class="alert-message">' + $("#txtCode").val().trim() + $('#hdnDelSucc').val() + '</div>');
                    BootstrapDialog.alert($textAndPic);

                    return false;
                }
                else if (resp.SaveCustomerMstDataResult == 'POSActive') {//Active Record
                    var $textAndPic = $('<div></div>');
                    $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-info-circle info-alertSuc"></i>');
                    //$textAndPic.append('<div class="alert-message">' + $("#txtCode").val().trim() + ' Record Activated Successfully.' + '</div>');
                    $textAndPic.append('<div class="alert-message">' + $("#txtCode").val().trim() + $('#hdnRecActSucc').val() + '</div>');
                    BootstrapDialog.alert($textAndPic);

                    return false;
                }
                else if (resp.SaveCustomerMstDataResult == 'POSInActive') {//Deactive Record
                    var $textAndPic = $('<div></div>');
                    $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-info-circle info-alertSuc"></i>');
                    //$textAndPic.append('<div class="alert-message">' + $("#txtCode").val().trim() + ' Record InActived Successfully.' + '</div>');
                    $textAndPic.append('<div class="alert-message">' + $("#txtCode").val().trim() + $('#hdnRecInActSucc').val() + '</div>');
                    BootstrapDialog.alert($textAndPic);

                    return false;
                }
                else {//Error Record
                    $("#hdnTransId").val('');
                    var $textAndPic = $('<div></div>');
                    $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                    //$textAndPic.append('<div class="alert-message">Error occured while Saving the Data</div>');
                    $textAndPic.append('<div class="alert-message">' + $('#hdnErrCodeOccer').val() + '</div>');
                    BootstrapDialog.alert($textAndPic);
                    return false;
                }
            }
            return false;
        },
        error: function (e) {
            //   $("#hdnTransId").val('');
            var $textAndPic = $('<div></div>');
            $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
            //$textAndPic.append('<div class="alert-message">Error occured while Saving the Data</div>');
            $textAndPic.append('<div class="alert-message">' + $('#hdnErrCodeOccer').val() + '</div>');
            BootstrapDialog.alert($textAndPic);
            return false;
        }
    });

}

function clearPOPUP() {
    $('#txtHomeDelName').val('');
    $('#txtHomeDelAddr').val('');
    $('#txtHomeDelZipCode').val('');
    $('#txtHomeDelPhNo').val('');
    $('#txtHomeDelEmail').val('');
};

function contactNoExist() {
    //  $("#btnCheckHomeDelOK").prop('disabled', false);
    $('#txtHomeDelName').val('');
    $('#txtHomeDelAddr').val('');
    $('#txtHomeDelZipCode').val('');

    $('#txtHomeDelEmail').val('');
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.verifyContactNoExist,
        data: "contNo=" + $('#txtHomeDelPhNo').val() + "&VD=" + VD1(),
        success: function (resp) {
            GatCityData();

            var result = JSON.parse(resp);
            if (result["customerResult"].length > 0) {
                popupSts = 0;
                $('#txtHomeDelName').val(result["customerResult"][0]["NAME"]);
                var mm = result["customerResult"][0]["ADDRESS1"] + result["customerResult"][0]["ADDRESS2"];
                if (mm != '')
                    address = result["customerResult"][0]["ADDRESS1"] + "," + result["customerResult"][0]["ADDRESS2"]
                else
                    address = '';
                //  $('#txtHomeDelAddr').val(result["customerResult"][0]["ADDRESS1"] + "," + result["customerResult"][0]["ADDRESS2"]);
                $('#txtHomeDelZipCode').val(result["customerResult"][0]["PINCODE"]);
                $('#txtHomeDelPhNo').val(result["customerResult"][0]["MOBILENO"]);
                $('#txtHomeDelEmail').val(result["customerResult"][0]["EMAIL"]);
                if (result["customerResult"][0]["CITY"] != "") {
                    GetStateCountryByCity1(result["customerResult"][0]["CITY"]);
                } else {
                    $('#txtHomeDelAddr').val(address);
                }
                $('#hdn_CusId').val(result["customerResult"][0]["id"]);
                $('#txtHomeDelName').prop('disabled', true);
                $('#txtHomeDelAddr').prop('disabled', false);
                $('#txtHomeDelZipCode').prop('disabled', true);
                $('#txtHomeDelPhNo').prop('disabled', false);
                $('#txtHomeDelEmail').prop('disabled', true);
                //$("#btnCheckHomeDelOK").prop('disabled', true);
                //var $textAndPic = $('<div></div>');
                //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                ////$textAndPic.append('<div class="alert-message">Please Enter Transaction Reference</div>');
                //$textAndPic.append('<div class="alert-message">Contact No. Already Exist.</div>');
                //BootstrapDialog.alert($textAndPic);
                //return false;
            } else {
                //  $("#btnCheckHomeDelOK").prop('disabled', false);
                $('#txtHomeDelName').prop('disabled', false);
                $('#txtHomeDelAddr').prop('disabled', false);
                $('#txtHomeDelZipCode').prop('disabled', false);
                //$('#txtHomeDelPhNo').prop('disabled', true);
                $('#txtHomeDelEmail').prop('disabled', false);
                popupSts = 1;
                return true;
            }
            var res = resp.d;
        }, error: function (e) {
        }
    });
}

function setLabels1(labelData) {
    jQuery("label[for='lblSubcription'").html(labelData["Renewal"]);
    jQuery("label[for='lblTaxValue'").html(labelData["TaxVAT"]);
    jQuery("label[for='lblPrice'").html(labelData["price"]);
    $('#lblTaxValue')[0].innerText = labelData["TaxVAT"];
    $('#hdnEntrCode').val(labelData["alert24"]);
    $('#hdnEntrName').val(labelData["MsgCounterName"]);
    jQuery("label[for='lblItemDraft'").html(labelData["itemdraft"]);
    jQuery("label[for='lblSCPopPwd'").html(labelData["pSettings"]);
    jQuery("label[for='lblLanguage'").html(labelData["lang"]);
    jQuery("label[for='lblDateFormat'").html(labelData["format"]);
    jQuery("label[for='lblPopOk'").html(labelData["ok"]);
    $('#btnCheck_OK').html(labelData["ok"]);
    jQuery("label[for='lblPopClose'").html(labelData["cancel"]);
    jQuery("label[for='lblSettings'").html(labelData["settings"]);
    jQuery("label[for='lblShift'").html(labelData["shift"]);
    jQuery("label[for='lblShiftStart'").html(labelData["shiftStart"]);
    jQuery("label[for='lblShiftEnd'").html(labelData["shiftEnd"]);
    jQuery("label[for='lblThemes'").html(labelData["themes"]);
    jQuery("label[for='lblTheme1'").html(labelData["theme1"]);
    jQuery("label[for='lblTheme2'").html(labelData["theme2"]);
    jQuery("label[for='lblTheme3'").html(labelData["theme3"]);
    jQuery("label[for='lblTheme4'").html(labelData["theme4"]);
    jQuery("label[for='lblChangePassword'").html(labelData["changePassword"]);
    jQuery("label[for='lblShortCut'").html(labelData["shortcut"]);
    jQuery("label[for='lblTenderType'").html(labelData["tenderType"]);
    jQuery("label[for='lblCreate'").html(labelData["create"]);
    jQuery("label[for='lblSave'").html(labelData["save"]);
    jQuery("label[for='lblEdit'").html(labelData["edit"]);
    jQuery("label[for='lblDelete'").html(labelData["delete"]);
    jQuery("label[for='lblCancel'").html(labelData["cancel"]);
    jQuery("label[for='lblStatus'").html(labelData["status"]);
    jQuery("label[for='lblToggleDropdown'").html(labelData["toggleDropdown"]);
    jQuery("label[for='lblActive'").html(labelData["active"]);
    jQuery("label[for='lblInActive'").html(labelData["inactive"]);
    jQuery("label[for='lblFilter'").html(labelData["filter"]);
    jQuery("label[for='lblFilterToggleDropdown'").html(labelData["toggleDropdown"]);
    jQuery("label[for='lblAll'").html(labelData["all"]);
    jQuery("label[for='lblToday'").html(labelData["toDay"]);
    jQuery("label[for='lblYesterDay'").html(labelData["yesterDay"]);
    jQuery("label[for='lblLastSevenDays'").html(labelData["lastSevenDays"]);
    jQuery("label[for='lblCurrentMonth'").html(labelData["currentMonth"]);
    jQuery("label[for='lblToggleActive'").html(labelData["active"]);
    jQuery("label[for='lblToggleInactive'").html(labelData["inactive"]);
    jQuery("label[for='lblConfigurators'").html(labelData["configurators"]);
    jQuery("label[for='lblPrintCustomize'").html(labelData["printCustamize"]);
    jQuery("label[for='lblBarcodeGenerator'").html(labelData["barCodeGenerator"]);
    jQuery("label[for='lblMasters'").html(labelData["masters"]);
    jQuery("label[for='lblTillType'").html(labelData["tillType"]);
    jQuery("label[for='lblTill'").html(labelData["till"]);
    jQuery("label[for='lblTillPermissions'").html(labelData["tillPermissions"]);
    jQuery("label[for='lblUserPermissions'").html(labelData["user"]);
    jQuery("label[for='lblCustomer'").html(labelData["customer"]);
    jQuery("label[for='lblTenderType'").html(labelData["tenderType"]);
    jQuery("label[for='lblTransactions'").html(labelData["transactions"]);
    jQuery("label[for='lblInvoce'").html(labelData["invoice"]);
    jQuery("label[for='lblSalesReturn'").html(labelData["salesReturn"]);
    jQuery("label[for='lblPosting'").html(labelData["posting"]);
    jQuery("label[for='lblTillOpening'").html(labelData["tillOpening"]);
    jQuery("label[for='lblTillClosing'").html(labelData["tillClosing"]);
    jQuery("label[for='lblAccountPosting'").html(labelData["accountPosting"]);
    jQuery("label[for='lblReports'").html(labelData["reports"]);
    jQuery("label[for='lblInvoiceListing'").html(labelData["invoiceListing"]);
    jQuery("label[for='lblTillInvoiceListing'").html(labelData["tillInvoiceListing"]);
    jQuery("label[for='lblTenderTypeWiseCollectionDailySummary'").html(labelData["tenderTypeWiseCollectionDailySummary"]);
    jQuery("label[for='lblSalesReturns'").html(labelData["salesReturns"]);
    jQuery("label[for='lblCustomerEnroll'").html(labelData["CustomerEnroll"]);
    jQuery("label[for='lblHomeDelHeader'").html(labelData["HomeDelivery"]);
    jQuery("label[for='lblHomeDelivery'").html(labelData["HomeDelivery"]);
    jQuery("label[for='lblGiftVouche'").html(labelData["giftVoucher"]);
    jQuery("label[for='lblCreditNote'").html(labelData["creditNote"]);
    jQuery("label[for='lblLoyaltyPoints'").html(labelData["LoyaltyPoints"]);
    jQuery("label[for='lblUserPermissions'").html(labelData["user"]);
    jQuery("label[for='lblDrft'").html(labelData["draft"]);
    jQuery("label[for='lblNewCart'").html(labelData["newCart"]);
    jQuery("label[for='lblItemCart'").html(labelData["itemCart"]);
    jQuery("label[for='lblItem'").html(labelData["item"]);
    jQuery("label[for='lblQty'").html(labelData["qty"]);
    jQuery("label[for='lblDisc'").html(labelData["disc"]);
    jQuery("label[for='lblDiscount'").html(labelData["disc"]);
    jQuery("label[for='lblNetAmount'").html(labelData["netAmount"]);
    jQuery("label[for='lblCustomer'").html(labelData["customer"]);
    jQuery("label[for='lblAmount'").html(labelData["amount"]);
    jQuery("label[for='lblStockItem'").html(labelData["stockItems"]);
    jQuery("label[for='lblQuickSearch'").html(labelData["quickSearch"]);
    jQuery("label[for='lblCalUOM'").html(labelData["uom"]);
    jQuery("label[for='lblcalQty'").html(labelData["qty"]);
    jQuery("label[for='lblCalDisc'").html(labelData["disc"]);
    jQuery("label[for='lblPrice'").html(labelData["price"]);
    jQuery("label[for='lblAmount'").html(labelData["amount"]);
    jQuery("label[for='lblCurrentInventory'").html(labelData["CurrentInventory"]);
    jQuery("label[for='lblReceipt'").html(labelData["receipt"]);
    jQuery("label[for='lblVATReturns'").html(labelData["VATReturns"]);
    jQuery("label[for='lblItem'").html(labelData["item"]);
    //jQuery("label[for='lblSubQty'").html(labelData["qty"]);
    $('#liDisM').html(labelData["disc"]);
    $('#liQtyM').html(labelData["qty"]);
    $('#liPriceM').html(labelData["price"]);
    $('#liPaym').html(labelData["pay"]);
    //jQuery("label[for='lblSubPrice'").html(labelData["price"]);
    //jQuery("label[for='lblSubDel'").html(labelData["delete"]);
    //jQuery("label[for='lblSubPay'").html(labelData["pay"]);
    jQuery("label[for='lblPaySummary'").html(labelData["paySummary"]);
    jQuery("label[for='lblTax'").html(labelData["tax"]);
    jQuery("label[for='lblTotalItems'").html(labelData["totalItems"]);
    jQuery("label[for='lblPopQty'").html(labelData["qty"]);
    jQuery("label[for='lblPopGrossTot'").html(labelData["grossTot"]);
    jQuery("label[for='lblPopDiscount'").html(labelData["disc"].replace('(%)', ''));
    jQuery("label[for='lblPopTax'").html(labelData["tax"]);
    //jQuery("label[for='lblPopTotal'").html(labelData["total"]);
    jQuery("label[for='lblPopCurrency'").html(labelData["currency"]);
    jQuery("label[for='lblAmountTendered'").html(labelData["amountTendered"]);
    jQuery("label[for='lblTransamodetype'").html(labelData["amount"]);
    jQuery("label[for='lblTransamodetypeRef'").html(labelData["change"]);
    jQuery("label[for='lblAdjust'").html(labelData["adjust"]);
    jQuery("label[for='lblCalculator'").html(labelData["calc"]);
    jQuery("label[for='lblProceed'").html(labelData["proceed"]);
    jQuery("label[for='lblClose'").html(labelData["closed"]);
    jQuery("label[for='lblPrint'").html(labelData["print"]);
    jQuery("label[for='lblModelPrint'").html(labelData["print"]);
    jQuery("label[for='lblModelPaySummary'").html(labelData["paySummary"]);
    jQuery("label[for='lblModelInvoice'").html(labelData["invoice"]);
    jQuery("label[for='lblModelItem'").html(labelData["item"]);
    jQuery("label[for='lblModelQty'").html(labelData["qty"]);
    jQuery("label[for='lblModelRate'").html(labelData["rate"]);
    jQuery("label[for='lblModelAmt'").html(labelData["amount"]);
    jQuery("label[for='lblPopTotal'").html(labelData["total"]);
    jQuery("label[for='lblModelPaid'").html(labelData["paid"]);
    jQuery("label[for='lblModelChange'").html(labelData["change"]);
    jQuery("label[for='lblSCPopShortcutKeys'").html(labelData["shortcuts"]);
    jQuery("label[for='lblSCPopShortcutKey'").html(labelData["shortcut"]);
    jQuery("label[for='lblSCPopOpenShortcuts'").html(labelData["openShotcutKeys"]);
    jQuery("label[for='lblSCPopScanItemFocus'").html(labelData["scanItemFocus"]);
    jQuery("label[for='lblSCPopChangetheQuantity'").html(labelData["changeQty"]);
    jQuery("label[for='lblSCPopChangetheDisc'").html(labelData["changeDesc"]);
    jQuery("label[for='lblSCPopChangethePrice'").html(labelData["changePrice"]);
    jQuery("label[for='lblSCPopProceedPaySummary'").html(labelData["proceedPaySummary"]);
    jQuery("label[for='lblSCPopOpenDraft'").html(labelData["openDraft"]);
    jQuery("label[for='lblSCPopNewCartStoreDraft'").html(labelData["newCartstrDrft"]);
    jQuery("label[for='lblSCPopPaymentProceed'").html(labelData["payProceed"]);
    jQuery("label[for='lblSCPopClosingPaySummary'").html(labelData["closingPaySummary"]);
    jQuery("label[for='lblSCPopClose'").html(labelData["closed"]);

    jQuery("label[for='lblDispatch'").html(labelData["Dispatch"]);
    jQuery("label[for='lblPopBalance'").html(labelData["PopBalance"]);
    jQuery("label[for='lblPointsObtainBasecurrency'").html(labelData["PointsObtainBasecurrency"]);
    $('#lblRewardPoints').html(labelData["RedeemPoints"]);
    jQuery("label[for='lblMinPoints'").html(labelData["MinPoints"]);
    jQuery("label[for='lblMaxPoints'").html(labelData["MaxPoints"]);
    jQuery("label[for='lblHomeDelPhNo'").html(labelData["ContactNo"]);
    jQuery("label[for='lblHomeDelName'").html(labelData["CommonName"]);
    jQuery("label[for='lblHomeDelAddr'").html(labelData["Address"]);
    jQuery("label[for='lblHomeDelZipCode'").html(labelData["ZipCode"]);
    jQuery("label[for='lblHomeDelEmail'").html(labelData["Email"]);
    jQuery("label[for='lblGiftVoucheRefno'").html(labelData["GiftVoucherRefNo"]);
    jQuery("label[for='lblGiftVoucheRefnoAmt'").html(labelData["amount"]);
    jQuery("label[for='lblAvailableAmount'").html(labelData["AvailAmount"]);
    jQuery("label[for='lblRedeemPoints'").html(labelData["RedeemPoint"]);
    jQuery("label[for='lblAvailPoints'").html(labelData["AvailPoints"]);
    jQuery("label[for='lblCreditNoteRefnoAmt'").html(labelData["amount"]);
    jQuery("label[for='lblCreditNoteRefno'").html(labelData["CreditNoteRefNo"]);
    jQuery("label[for='lblUpload'").html(labelData["upload"]);
    $('#hdnImageSave').val(labelData["ImageSave"]);
    $('#txtCusName').attr('placeholder', labelData["fillCust"]);
    $('#txtItemScan').attr('placeholder', labelData["scanItem"]);
    $('#txtItemSearch').attr('placeholder', labelData["searchItems"]);
    $('#txtUOM').attr('placeholder', labelData["uom"]);
    $('#txtUOMitem').attr('placeholder', labelData["uom"]);
    $('#txtQuantity').attr('placeholder', labelData["qty"]);
    $('#txtDiscount').attr('placeholder', labelData["disc"]);
    $('#txtPrice').attr('placeholder', labelData["price"]);
    $('#txtAmount').attr('placeholder', labelData["amount"]);
    $('#txtItemspp').attr('placeholder', labelData["disc"]);
    $('#txtQuantityPP').attr('placeholder', labelData["disc"]);
    $('#txtPricepp').attr('placeholder', labelData["grossTot"]);
    $('#txtDiscountpp').attr('placeholder', labelData["disc"]);
    $('#txtTaxPP').attr('placeholder', labelData["disc"]);
    $('#txtAmountpp').attr('placeholder', labelData["disc"]);
    $('#txtCurrency').attr('placeholder', labelData["disc"]);
    //$('#txtPrice').attr('placeholder', labelData["price"]);
    //$('#txtPrice').attr('placeholder', labelData["price"]);
    $('#hdnRecSaveSucc').val(labelData["saveSuccess"]);
    $('#hdnICodeAlreadyExist').val(labelData["alert23"]);
    $('#hdnAccessDenied').val(labelData["alert1"]);
    $('#hdnThameChange').val(labelData["alert2"]);
    $('#hdnSelCurType').val(labelData["alert3"]);
    $('#hdnSelTranType').val(labelData["alert4"]);
    $('#hdnSelCurrType').val(labelData["alert5"]);
    $('#hdnMsgCart').val(labelData["alert6"]);
    $('#hdnCounter').val(labelData["alert7"]);
    $('#hdnSearchTitleInv').val(labelData["alert8"]);
    $('#hdnPayIn').val(labelData["alert9"]);
    $('#hdnPlzSelectItem').val(labelData["alert10"] + ' ' + labelData["item"]);
    $('#hdnPlzSelectTranType').val(labelData["alert10"] + labelData["tenderType"]);
    $('#hdnInvalAuth').val(labelData["alert11"]);
    $('#hdnAmtGrtrEqlAmtTend').val(labelData["alert12"]);
    $('#hdnMsgTranRef').val(labelData["alert13"]);
    $('#hdnDiscVal').val(labelData["alert14"]);
    $('#hdnDontPermisAccessInvoice').val(labelData["alert15"]);
    $('#hdnPleaseContactAdmin').val(labelData["alert16"]);
    $('#hdnNotAssignCountrToTrans').val(labelData["alert17"]);
    $('#hdnItmNotAvail').val(labelData["alert18"]);
    $('#hdnSelCust').val(labelData["alert19"]);
    $('#hdnItem').val(labelData["item"]);
    $('#hdnQty').val(labelData["qty"]);
    $('#hdnDisc').val(labelData["disc"]);
    $('#hdnPrice').val(labelData["price"]);
    $('#hdnAmount').val(labelData["amount"]);
    $('#hdnTotal').val(labelData["total"]);
    $('#hdnInvalidData').val(labelData["InvalidData"]);
    $('#hdnChange').val(labelData["change"]);
    $('#hdnRefNo').val(labelData["refNo"]);
    $('#hdnExpDt').val(labelData["alert11"]);
    $('#hdnChPwdSaveSucc').val(labelData["saveSuccess"]);
    $('#hdnNetTotal').val(labelData["netTotal"]);
    $('#hdnRound').val(labelData["Round"]);
    $('#hdnBaseTotal').val(labelData["alert20"]);
    $('#hdnTaxTotal').val(labelData["alert21"]);
    $('#hdnNotAssignCountrToTrans').val(labelData["permissionsAssisted"]);
    $('#hdnShift').val(labelData['shift']);
    $('#hdnInvoice').val(labelData['invoice']);
    $('#hdnTotItems').val(labelData['totalItems']);
    $('#hdnTotQty').val(labelData['total'] + labelData['qty']);

    $('#hdnPlsContactNo').val(labelData["PlsContactNo"]);
    $('#hdnRewardPoints404').val(labelData["RewardPoints404"]);
    $('#hdnPlsEnterAddrss').val(labelData["PlsEnterAddrss"]);
    $('#hdnPlsEnterAmount').val(labelData["PlsEnterAmount"]);
    $('#hdnrewardptsgrtrMinpts').val(labelData["rewardptsgrtrMinpts"]);
    $('#hdnrewardptsLstrMinpts').val(labelData["rewardptsLstrMinpts"]);
    $('#hdnMsgCounterName').val(labelData["MsgCounterName"]);
    $('#btnCreditOK').html(labelData["ok"]);
    $('#btnCreditClose').html(labelData["closed"]);
    $('#btnCreditRedeemOK').html(labelData["ok"]);
    $('#btnCreditRedeemClose').html(labelData["closed"]);
    $('#btnGiftVoucheOk ').html(labelData["ok"]);
    $('#btnGiftVoucheClose').html(labelData["closed"]);
    jQuery("label[for='lblLocationwiseProfitAnalysis'").html(labelData["LocwiseProfitAnlys"]);
    jQuery("label[for='lblItemCategoryProfitAnalysis'").html(labelData["ItemCatgPrftAnlys"]);
    jQuery("label[for='lblAnalyticalReports'").html(labelData["AnalyticalReports"]);
    jQuery("label[for='lblItemCategorySalesAnalysis'").html(labelData["ItemCategorySalesAnalysis"]);
    jQuery("label[for='lblWeekDaysRevenue'").html(labelData["WeekdaysRevenue"]);
    jQuery("label[for='lblBucketAnalysis'").html(labelData["BucketAnalysis"]);
    jQuery("label[for='lblLocationwiseAverageInvoiceValue'").html(labelData["LocationwiseAverageInvoiceValue"]);
    jQuery("label[for='lblRewardPointsLabel'").html(labelData["alert22"]);
    //$('#hdn').val(labelData["alert1"]);
    //$('#hdn').val(labelData["alert1"]);


    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);

    $('#hdnImageSave').val(labelData["ImageSave"]);
    jQuery("label[for='lblUpload'").html(labelData["upload"]);
    jQuery("label[for='lblCustLDGR'").html(labelData["PosCustLdger"]);

}

function getResources3(langCode, pageCode) {
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.getResource,
        data: "langCode=" + langCode + "&pageCode=" + pageCode,
        success: function (resp) {
            setLabels3(resp);
            //  jQuery("label[for='lblLoyaltyPoints'").html(labelData["LoyaltyPoints"]);
        }
    });
}

$('#card-payment').click(function () {
    $('#home,#form-section').show();
    $('#card-pay,#footer-pay').hide();
});

$('#card-pay-icon').click(function () {
    $('#card-pay,#form-section-2,#footer-pay').show();
    $('#home,#form-section').hide();
});
$('#netbanking-pay-icon').click(function () {
    $('#netbanking-pay,#footer-pay').show();
    $('#home,#form-section').hide();
});

$('#UPI-card-payment_DIV').click(function () {
    $('#div_form-section-UPI_UPI').hide();
    $('#home,#form-section').show();
});

$('#wallet-pay-icon').click(function () {
    //ConvertWalletAmount_n2();
    $('#QR-code-pay_2').hide();
    $('#home,#form-section').hide();
    $('#div_form-section-UPI_UPI').show();

});

$('#wallet-card-payment').click(function () {
    $('#home,#form-section').show();
    $('#footer-pay,#wallet-pay').hide();
});

$('#UPI-pay-icon').click(function () {

    $('#form-section-UPI').show();
    $('#UPI-pay').show();
    $('#home,#form-section,#footer-pay,#bhim-pay').hide();

    $('#form-section-UPI,#QR-code-pay').show();
    $('#bhim-pay,#footer-pay,#div_qrimg').hide();
});

$('#UPI-card-payment').click(function () {
    $('#home,#form-section').show();
    $('#footer-pay,#UPI-pay').hide();
});

$('#bhim-icon').click(function () {
    $('#bhim-pay,#footer-pay').show();
    $('#form-section-UPI,#QR-code-pay').hide();
});

$('#change-app_2').click(function () {
    $('#div_form-section-UPI_UPI').show();
    $('#QR-code-pay_2,#wallet-pay,#div_qrimg,#footer-pay').hide();

    $('#txt_amount_Ref').val('');
});
$('#change-app').click(function () {
    $('#form-section-UPI,#QR-code-pay').show();
    $('#bhim-pay,#footer-pay,#div_qrimg').hide();


});
$('#googlepay-icon').click(function () {
    $('#google-pay,#footer-pay').show();
    $('#form-section-UPI,#QR-code-pay').hide();
});

$('#change-app-google').click(function () {
    $('#form-section-UPI,#QR-code-pay').show();
    $('#google-pay,#footer-pay').hide();
});

$('#whatsapp-icon').click(function () {
    $('#whatsapp-pay,#footer-pay').show();
    $('#form-section-UPI,#QR-code-pay').hide();
});

$('#change-app-whatsapp').click(function () {
    $('#form-section-UPI,#QR-code-pay').show();
    $('#whatsapp-pay,#footer-pay').hide();
});

$('#paytm-icon').click(function () {
    $('#paytm-pay,#footer-pay').show();
    $('#form-section-UPI,#QR-code-pay').hide();
});

$('#change-app-paytm').click(function () {
    $('#form-section-UPI,#QR-code-pay').show();
    $('#paytm-pay,#footer-pay').hide();
});

$('#phonepe-icon').click(function () {
    $('#phonepe-pay,#footer-pay').show();
    $('#form-section-UPI,#QR-code-pay').hide();
});

$('#change-app-phonepay').click(function () {
    $('#form-section-UPI,#QR-code-pay').show();
    $('#phonepe-pay,#footer-pay').hide();
});

$('#other-icon').click(function () {
    $('#other-pay,#footer-pay').show();
    $('#form-section-UPI,#QR-code-pay').hide();
});

$('#change-app-other').click(function () {
    $('#form-section-UPI,#QR-code-pay').show();
    $('#other-pay,#footer-pay').hide();
});

$('#QR-code-pay').click(function () {
    $('#QRCode-pay-section').show();
    $('#form-section-UPI,#QR-code-pay,#UPI-pay,#footer-pay').hide();
});

$('#QRCode-pay-section').click(function () {
    $('#QRCode-pay-section').hide();
    $('#form-section-UPI,#QR-code-pay,#UPI-pay').show();
});


var currency_symbols = {
    'AED': 'د.إ', // ?
    'AFN': '؋',
    'ALL': '',
    'AMD': '',
    'ANG': 'ƒ',
    'AOA': '', // ?
    'ARS': '$',
    'AUD': '$',
    'AWG': 'ƒ',
    'AZN': '',
    'BAM': 'KM',
    'BBD': '$',
    'BDT': '৳', // ?
    'BGN': 'Лв',
    'BHD': 'د.ب', // ?
    'BIF': 'FBu', // ?
    'BMD': '$',
    'BND': '$',
    'BOB': '',
    'BRL': '',
    'BSD': '',
    'BTN': '', // ?
    'BWP': '',
    'BYR': '',
    'BZD': '',
    'CAD': '',
    'CDF': '',
    'CHF': '',
    'CLF': '', // ?
    'CLP': '',
    'CNY': '',
    'COP': '',
    'CRC': '',
    'CUP': '',
    'CVE': '', // ?
    'CZK': '',
    'DJF': '', // ?
    'DKK': '',
    'DOP': '',
    'DZD': '', // ?
    'EGP': '',
    'ETB': '',
    'EUR': '€',
    'FJD': '',
    'FKP': '',
    'GBP': '',
    'GEL': '', // ?
    'GHS': '',
    'GIP': '',
    'GMD': '', // ?
    'GNF': '', // ?
    'GTQ': '',
    'GYD': '',
    'HKD': '',
    'HNL': '',
    'HRK': '',
    'HTG': '', // ?
    'HUF': '',
    'IDR': 'Rp',
    'ILS': '',
    'INR': '₹',
    'IQD': '', // ?
    'IRR': '',
    'ISK': '',
    'JEP': '',
    'JMD': '',
    'JOD': '', // ?
    'JPY': '',
    'KES': '', // ?
    'KGS': '',
    'KHR': '',
    'KMF': '', // ?
    'KPW': '',
    'KRW': '',
    'KWD': '', // ?
    'KYD': '',
    'KZT': '',
    'LAK': '',
    'LBP': '',
    'LKR': '',
    'LRD': '',
    'LSL': '', // ?
    'LTL': '',
    'LVL': '',
    'LYD': '', // ?
    'MAD': '', //?
    'MDL': '',
    'MGA': '', // ?
    'MKD': '',
    'MMK': '',
    'MNT': '',
    'MOP': '', // ?
    'MRO': '', // ?
    'MUR': '', // ?
    'MVR': '', // ?
    'MWK': '',
    'MXN': '',
    'MYR': '',
    'MZN': '',
    'NAD': '',
    'NGN': '',
    'NIO': '',
    'NOK': '',
    'NPR': '',
    'NZD': '',
    'OMR': '',
    'PAB': '',
    'PEN': '',
    'PGK': '', // ?
    'PHP': '',
    'PKR': '',
    'PLN': '',
    'PYG': '',
    'QAR': '',
    'RON': '',
    'RSD': '',
    'RUB': '',
    'RWF': '',
    'SAR': '﷼',
    'SBD': '',
    'SCR': '',
    'SDG': '', // ?
    'SEK': '',
    'SGD': '',
    'SHP': '',
    'SLL': '', // ?
    'SOS': '',
    'SRD': '',
    'STD': '', // ?
    'SVC': '',
    'SYP': '',
    'SZL': '', // ?
    'THB': '',
    'TJS': '', // ? TJS (guess)
    'TMT': '',
    'TND': '',
    'TOP': '',
    'TRY': '', // New Turkey Lira (old symbol used)
    'TTD': '',
    'TWD': '',
    'TZS': '',
    'UAH': '',
    'UGX': '',
    'USD': '$',
    'UYU': '',
    'UZS': '',
    'VEF': '',
    'VND': '',
    'VUV': '',
    'WST': '',
    'XAF': '',
    'XCD': '',
    'XDR': '',
    'XOF': '',
    'XPF': '',
    'YER': '',
    'ZAR': '',
    'ZMK': '', // ?
    'ZWL': '',
}


function setLabels3(labelData) {

    jQuery("label[for='lblItemDraft'").html(labelData["itemdraft"]);
    jQuery("label[for='lblLocationwiseProfitAnalysis'").html(labelData["LocwiseProfitAnlys"]);
    jQuery("label[for='lblItemCategoryProfitAnalysis'").html(labelData["ItemCatgPrftAnlys"]);
    jQuery("label[for='lblAnalyticalReports'").html(labelData["AnalyticalReports"]);
    jQuery("label[for='lblItemCategorySalesAnalysis'").html(labelData["ItemCategorySalesAnalysis"]);
    jQuery("label[for='lblWeekDaysRevenue'").html(labelData["WeekdaysRevenue"]);
    jQuery("label[for='lblBucketAnalysis'").html(labelData["BucketAnalysis"]);
    jQuery("label[for='lblLocationwiseAverageInvoiceValue'").html(labelData["LocationwiseAverageInvoiceValue"]);

    jQuery("label[for='lblYear'").html(labelData["year"]);
    jQuery("label[for='lblMonth'").html(labelData["month"]);
    jQuery("label[for='lblSalesInfo'").html(labelData["salesInfo"]);
    jQuery("label[for='lblYear'").html(labelData["year"]);
    jQuery("label[for='lblSalesAnalysis'").html(labelData["salesAnalysis"]);
    jQuery("label[for='lblWeek'").html(labelData["alert4"]);

    jQuery("label[for='lblSCPopPwd'").html(labelData["pSettings"]);
    jQuery("label[for='lblLanguage'").html(labelData["lang"]);
    jQuery("label[for='lblDateFormat'").html(labelData["format"]);
    jQuery("label[for='lblPopOk'").html(labelData["ok"]);
    jQuery("label[for='lblPopClose'").html(labelData["cancel"]);
    jQuery("label[for='lblSettings'").html(labelData["settings"]);
    jQuery("label[for='lblThemes'").html(labelData["themes"]);
    jQuery("label[for='lblTheme1'").html(labelData["theme1"]);
    jQuery("label[for='lblTheme2'").html(labelData["theme2"]);
    jQuery("label[for='lblTheme3'").html(labelData["theme3"]);
    jQuery("label[for='lblTheme4'").html(labelData["theme4"]);
    jQuery("label[for='lblChangePassword'").html(labelData["changePassword"]);
    jQuery("label[for='lblConfigurators'").html(labelData["configurators"]);
    jQuery("label[for='lblPrintCustomize'").html(labelData["printCustamize"]);
    jQuery("label[for='lblBarcodeGenerator'").html(labelData["barCodeGenerator"]);
    jQuery("label[for='lblMasters'").html(labelData["masters"]);
    jQuery("label[for='lblTillType'").html(labelData["tillType"]);
    jQuery("label[for='lblTill'").html(labelData["till"]);
    jQuery("label[for='lblTillPermissions'").html(labelData["tillPermissions"]);
    jQuery("label[for='lblCustomer'").html(labelData["customer"]);
    jQuery("label[for='lblTenderType'").html(labelData["tenderType"]);
    jQuery("label[for='lblTransactions'").html(labelData["transactions"]);
    jQuery("label[for='lblInvoce'").html(labelData["invoice"]);
    jQuery("label[for='lblSalesReturn'").html(labelData["salesReturn"]);
    jQuery("label[for='lblPosting'").html(labelData["posting"]);
    jQuery("label[for='lblTillOpening'").html(labelData["tillOpening"]);
    jQuery("label[for='lblAccountPosting'").html(labelData["accountPosting"]);
    jQuery("label[for='lblReports'").html(labelData["reports"]);
    jQuery("label[for='lblInvoiceListing'").html(labelData["invoiceListing"]);
    jQuery("label[for='lblTillInvoiceListing'").html(labelData["tillInvoiceListing"]);
    jQuery("label[for='lblTenderTypeWiseCollectionDailySummary'").html(labelData["tenderTypeWiseCollectionDailySummary"]);
    jQuery("label[for='lblSalesReturns'").html(labelData["salesReturns"]);
    jQuery("label[for='lblTotalSales'").html(labelData["totSales"]);
    jQuery("label[for='lblTotCust'").html(labelData["totCust"]);
    jQuery("label[for='lblTotItems'").html(labelData["totalItems"]);
    jQuery("label[for='lblNewCust'").html(labelData["newCust"]);
    jQuery("label[for='lblWelToPOS'").html(labelData["welPOS"]);
    jQuery("label[for='lblStartANewSale'").html(labelData["startNewSale"]);
    jQuery("label[for='lblTillOpening'").html(labelData["tillOpening"]);
    jQuery("label[for='lblTodayCloseoutReport'").html(labelData["todatCloseRep"]);
    jQuery("label[for='lblTodaysSalesDetailsReport'").html(labelData["toDaySalDetRep"]);
    jQuery("label[for='lblTillClosing'").html(labelData["tillClosing"]);
    jQuery("label[for='lblRepTillClosing'").html(labelData["tillClosing"]);
    jQuery("label[for='lblLoyaltyPoints'").html(labelData["LoyaltyPoints"]);
    jQuery("label[for='lblTodayItemsSummaryReport'").html(labelData["toDayItemSumRep"]);
    jQuery("label[for='lblAccountPosting'").html(labelData["accountPosting"]);
    jQuery("label[for='lblSalesInfo'").html(labelData["salesInfo"]);
    jQuery("label[for='lblMonth'").html(labelData["month"]);
    jQuery("label[for='lblYear'").html(labelData["year"]);
    jQuery("label[for='lblSalesAnalysis'").html(labelData["salesAnalysis"]);
    jQuery("label[for='lblSales'").html(labelData["sales"]);
    jQuery("label[for='lblRefunds'").html(labelData["refunds"]);
    jQuery("label[for='lblRevenue'").html(labelData["revenue"]);
    jQuery("label[for='lblCost'").html(labelData["cost"]);
    jQuery("label[for='lblProfit'").html(labelData["profit"]);
    jQuery("label[for='lblProfitPercent'").html(labelData["profitPersentage"]);
    jQuery("label[for='lblTenderWiseSales'").html(labelData["tenderSales"]);
    jQuery("label[for='lblReceipt'").html(labelData["receipt"]);
    jQuery("label[for='lblVATReturns'").html(labelData["VATReturns"]);
    jQuery("label[for='lblItem'").html(labelData["item"]);
    jQuery("label[for='lblUserPermissions'").html(labelData["user"]);
    jQuery("label[for='lblPrintPole'").html(labelData["PrintPoleConfig"]);
    jQuery("label[for='lblCurrentInventory'").html(labelData["CurrentInventory"]);
    jQuery("label[for='lblLocationwiseAverageInvoiceValue'").html(labelData["LocationwiseAverageInvoiceValue"]);
    $('#hdnNAssignCountrTrans').val(labelData["alert1"]);
    $('#hdnNPermissionInvoice').val(labelData["alert2"]);
    $('#hdnInvalData').val(labelData["InvalidData"]);
    $('#hdnContactAdmin').val(labelData["contactAdmin"]);
    $('#hdnChPwdSaveSucc').val(labelData["saveSuccess"]);
    $('#hdnThameChange').val(labelData["alert3"]);
    $('#hdnNotAssignCountrToTrans').val(labelData["alert17"]);
    $('#hdnAccessDenied').val(labelData["alert1"]);
    jQuery("label[for='lblDispatch'").html(labelData["Dispatch"]);
    $('#hdnImageSave').val(labelData["ImageSave"]);
    jQuery("label[for='lblUpload'").html(labelData["upload"]);
    jQuery("label[for='lblSubcription'").html(labelData["Renewal"]);
    $('#hdnEntrCode').val(labelData["alert11"]);
    $('#hdnEntrName').val(labelData["alert24"]);

    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
}
function dayCloserChecking() {
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetDayCloserChecking,
        data: { VD: VD1(), subOrgId: HashValues[1] },
        success: function (resp) {
            if (resp == 0) {
                window.open('CounterOpening.html', '_self', 'location=no');
            } else {
                var $textAndPic = $('<div></div>');
                $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                //$textAndPic.append('<div class="alert-message">You are not assigned any Counter so you can\'t do the Transaction</div>');
                $textAndPic.append('<div class="alert-message">Day closer under processing – can’t do any operations </div>');
                //$textAndPic.append('<div class="alert-message">' + $('#hdnNotAssignCountrToTrans').val() + '</div>');

                BootstrapDialog.alert($textAndPic);
            }
        }
    });
}



function printInternalUSB(invoiceId) {
    $.ajax({
        type: 'GET',
        url: mbsPOS.Settings.printUSB,
        data: "InvoiceId=" + invoiceId + "&VD=" + VD1(),
        success: function (resp) {
            try {
                window.cordova.plugins.generic.printer.usb.scan(
                    function (result) {
                        var printerName = '';
                        //printerName = printer;
                        var data = resp;
                        //var printerName = 'GP-80';
                        var printerName = result[0]["productName"];
                        window.cordova.plugins.generic.printer.usb.print(printerName, data);
                    }, function (error) {
                        alert('Printer: connect fail: ' + error);
                    }
                );
            } catch (a) {
               
            }
        }, error: function (e) {
           
        }
    });
}

