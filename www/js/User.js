﻿function GetUserData() {
    window.localStorage.setItem('ctCheck', 1);
    if (window.localStorage.getItem('network') == '1') {
        GetLoginUserName();
        defaultdata();
        $('#hdnPageIndex').val('1');
        $('#hdnFilterType').val('Summary');
        GetSummaryGridData('Summary');
        $('#lblLastUpdate').css("display", "none");       
    }
    else {
        window.open('DashBoard.html', '_self', 'location=no');
    }
}
function GetLoginUserName() {
    window.localStorage.setItem('ctCheck', 1);
    var sess = CashierLite.Session.getInstance().get();
    if (sess != null) {
        if (sess.userName != "") {
            document.getElementById("lblName").innerHTML = sess.userName;
        }
        else {
            window.open('Login.html', '_self', 'location=no');
        }
    }
    else {
        window.open('Login.html', '_self', 'location=no');
    }
    theamLoad();
}
function theamLoad() {
    window.localStorage.setItem('ctCheck', 1);
    if (window.localStorage.getItem("Theam") != null)
        $('head').append('<link rel="stylesheet" href=' + window.localStorage.getItem("Theam") + ' type="text/css" />');
}

function defaultdata() {
    window.localStorage.setItem('ctCheck', 1);
    var Paging = ""; var CompanyId = ""; var SubOrgId = ''; var Orgcode = ''; var OBS = ''; var CashierID = window.localStorage.getItem("CashierID"); var Userid = '';
    var Lang = '';
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        getResources(hashSplit[7], "POSUsers");
        CompanyId = hashSplit[0];
        SubOrgId = hashSplit[1];
        
        $('#hdnOrgId').val(CompanyId);
        $('#hdnSubOrgId').val(SubOrgId);
        OBS = hashSplit[9];
        Orgcode = hashSplit[17];
        Userid = hashSplit[14];
        Lang = hashSplit[7];
        Paging = hashSplit[19];
    }
    document.getElementById("lblMTillCode").innerHTML = hashSplit[28];
    $('#hdnOBS').val(OBS);
    $('#hdnUserId').val(Userid);
    $('#hdnLanguage').val(Lang);
    $('#hdnPaging').val(Paging);
    $('#SummaryGrid').css("display", "block");
    $("#ContentDet").css('display', 'none');
    $('#Create').css("display", "block");
    $('#Edit').css("display", "none");
    $('#Cancel').css("display", "none");
    $('#btnSave').css("display", "none");
    $('#Delete').css("display", "none");
    $('#Status').css("display", "none");
    $('#divActive').css("display", "none");
}
$("#btnActive").click(function () {

    $('#hdnScreenMode').val("Active");
    Save('Mode');
});
$("#btnInActive").click(function () {
    $('#hdnScreenMode').val("InActive");
    Save('Mode');
});
$("#btnCreate").click(function () {
    window.localStorage.setItem('ctCheck', 1);
    clearControls_User();
    defaultdata();
    EnableFormfileds(false);
    document.getElementById('rdobtnStoreAdmin').checked = true;
    document.getElementById('rdobtnCashier').checked = false;
    $('#lblLastUpdate').css("display", "none");
    $('#trSBInnerRow li span#lblLastUpdatedUser').html("");
    $('#trSBInnerRow li span#lblLastUpdatedDate').html("");
    $('#trSBInnerRow li span#lblMode').html($('#hdnNew').val());
    $('#hdnMode').val('New');
    $('#txtStore').removeAttr("disabled", "disabled");
    $('#txtSubName').removeAttr("disabled", "disabled");
    $("#txtAlias").removeAttr("disabled", "disabled");
    $("#btnCreate").attr('style', 'display:none;');
    $("#ShowMenuModes").attr('style', 'display:block;');
    $("#btnSave").attr('style', 'display:block;');
    $("#btnEdit").attr('style', 'display:none;');
    $("#btnDelete").attr('style', 'display:none;');
    $("#btnCancel").attr('style', 'display:block;');
    $('#SummaryGrid').css("display", "none");
    $("#ContentDet").css('display', 'block');
    $("#divStatusGroup").attr('style', 'display:none;');
    $("#divNavBarRight").attr('style', 'display:none;');
    if ($("#hdnMode").val() == 'New' && counterN == 'Y') {
        $('#txtStore').prop("disabled", true);  // Document No. Text Box
    }
    else {
        $('#txtStore').prop("disabled", false);
    }
    $('#PrimeInfo').addClass('open');
    $('#divPrimeInformation').attr('style', 'display:block');
});
GetCityData();
function GetCityData() {
    var CityMain = "";
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetCityData,
        data: "VD=" + window.localStorage.getItem("UUID") + "",
        success: function (resp) {
            var len = resp.length;
            $("#txtCity").empty();
            $("#txtCity").append("<option value=0>Select City</option>");
            for (var i = 0; i < len; i++) {
                $("#txtCity").append("<option value='" + resp[i]["CityId"] + "'>" + resp[i]["CityName"] + "</option>");
                if (CityMain != '')
                    CityMain = CityMain + resp[i]["CityId"] + '~' + resp[i]["CityName"] + '$';
                else
                    CityMain = resp[i]["CityId"] + '~' + resp[i]["CityName"] + '$';
            };
            window.localStorage.setItem("FillCityMain", CityMain);
        }, error: function (e) {

        }
    });
}
try {
    var ss = null;
    $("[id$='txtStore']").autocomplete({

        source: function (request, response) {
            var HashValues = (window.localStorage.getItem("hashValues"));
            if (HashValues != null && HashValues != '') {
                var hashSplit = HashValues.split('~');
                SubOrgId = hashSplit[1];
                ss = hashSplit[0];
            }
            var param = "OBSCode=" + $('#txtStore').val() + "&VD=" + window.localStorage.getItem("UUID");
            $.ajax({
                url: CashierLite.Settings.getSubOrgAutoData,
                data: param,
                dataType: "json",
                type: "Get",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    window.localStorage.setItem('ctCheck', 1);
                    response($.map(data, function (item) {
                        return {
                            label: item.OrgCode,
                            val: item.ORG_ID
                        }
                    }))
                },
                error: function (response) {
                    //alert(response.responseText);
                },
                failure: function (response) {
                    //alert(response.responseText);
                }
            });

        },
        change: function (e, i) {
            if (i.item) {
                // do whatever you want to when the item is found
            }
            else {
                $('#txtStore').val('');
                $('#hdnSubOrgId').val('');
            }
        },
        select: function (e, i) {
            window.localStorage.setItem('ctCheck', 1);
            $('#txtStore').val(parseFloat(i.item.label));
            $('#hdnSubOrgId').val(i.item.val);
        },
        minLength: 1
    });
}
catch (a) {

}
$('#btnSave').click(function () {
    window.localStorage.setItem('ctCheck', 1);    
    if ($('#txtStore').val() == "") { 
        navigator.notification.alert($("#hdnPleaseEnterStore").val(), "", "neo ecr", "Ok");

        return false;
    }
    if ($('#txtEmail').val() == "") { 
        navigator.notification.alert($("#hdnPleaseEnterEmail").val(), "", "neo ecr", "Ok");

        return false;
    }
    if ($('#txtEmail').val() != "") {
        if (checkEmail($('#txtEmail').val()) == false) {
          
        navigator.notification.alert($("#hdnValidEmail").val(), "", "neo ecr", "Ok");

            $('#txtEmail').focus();
            return false;
        }
    }
    if ($("#txtMobile").prop("disabled") == false && $('#txtMobile').val() == "") {
  
        navigator.notification.alert($("#hdnPleaseEnter").val().trim() + ' ' + $('#hdnMobile').val(), "", "neo ecr", "Ok");

        return false;
    }
    Save('');
});
function Save(upCheck)
{
    window.localStorage.setItem('ctCheck', 1);
    Jsonobj = [];
    var userData = {};
    if ($('#hdnTransId').val() == "") {
        userData["TransId"] = "";
    }
    else {
        userData["TransId"] = $('#hdnTransId').val();
    }    
    userData["OrgId"] = $('#hdnOrgId').val();
    userData["SubOrgId"] = $('#hdnSubOrgId').val();
    userData["Email"] = $('#txtEmail').val();
    userData["Mobile"] = $('#txtMobile').val();
    if (document.getElementById('rdobtnCashier').checked == true)
        userData["UserType"] = 'N';
    else 
        userData["UserType"] = 'A';
    userData["Status"] = "1";
    if (upCheck != '') {
        if ($('#hdnScreenMode').val() == "Active")
            userData["Status"] = "1";
        else
            userData["Status"] = "2";
    }
    var vd_ = window.localStorage.getItem("UUID");;
    userData["VD"] = vd_;
    Jsonobj.push(userData);
    var ItemsData = new Object();
    ItemsData.Data = JSON.stringify(Jsonobj);
    var ComplexObject = new Object();
    ComplexObject.ItemsData = ItemsData;
    var data = JSON.stringify(ComplexObject);
    $.ajax({
        type: 'POST',
        url: CashierLite.Settings.UserSave,
        data: data,
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function (resp) {

            window.localStorage.setItem('ctCheck', 1);

            resp = resp.UserSaveResult;
            var result = resp.split('~');
            if (result[0] == "100000") {
                if ($('#hdnScreenMode').val() == "Active") {

                     
                    navigator.notification.alert( $('#hdnRecActSucc').val(), "", "neo ecr", "Ok");

                    $("#btnCancel").click();
                }
                else if ($('#hdnScreenMode').val() == "InActive") {
 
                    navigator.notification.alert( $('#hdnRecInactSucc').val(), "", "neo ecr", "Ok");

                    $("#btnCancel").click();
                }
                else {
            
                    navigator.notification.alert( $('#hdnRecSaveSucc').val(), "", "neo ecr", "Ok");

                    if ($('#hdnTransId').val() == "") {
                        $("#btnCreate").click();
                    }
                    else {
                        $("#btnCancel").click();
                    }
                }

                clearControls_User();
                GetSummaryGridData('Summary');
            }
            if (result[0] == "100001") {
              
                navigator.notification.alert( $('#hdnCodeAlreadyExist').val(), "", "neo ecr", "Ok");

            }            
            if (result[0] == "1000020") {
                
                navigator.notification.alert( $('#hdnInvalData').val(), "", "neo ecr", "Ok");

            }
        },
        error: function (e) {
        
            navigator.notification.alert( $('#hdnInvalData').val(), "", "neo ecr", "Ok");
        }
    });

    $('#PrimeInfo').addClass('open');
    $('#divPrimeInformation').attr('style', 'display:block');
};

$("#txtMobile").bind('cut copy paste', function (e) {
    e.preventDefault();
});
$("#txtMobile").keypress(function (event) {
    if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {       
        if ((event.which != 46 || $(this).val().indexOf('.') != -1)) {
            return false;
        }
        event.preventDefault();
    }
    if (this.value.indexOf(".") > -1 && (this.value.split('.')[1].length > 1)) {
        return false;
        event.preventDefault()
    }
});
function checkEmail(text) {
    var match = /[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-zA-Z0-9]/;
    return match.test(text);
}

function GetSummaryGridData(FilterType) {
    window.localStorage.setItem('ctCheck', 1);
    var Index = $('#hdnPageIndex').val();
    var MethodType = "";
    var FindGo = "";
    var QryType = "";
    if (FilterType == 'FindGo') {
        FindGo = $('#txtFindGo').val();
        Index = 1;

    }
    if (FilterType == 'Summary') {
        $('#txtFindGo').val('');
    }

    else {
        FindGo = $('#txtFindGo').val();
    }
    if (FilterType.indexOf('Index') != -1) {
        FilterType = FilterType.split('~')[0];
        MethodType = "next";
    }
   
    var HashVal = (window.localStorage.getItem("hashValues"));
    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');
    }
    var SummaryTable = '';
    Jsonobj = [];
    var SubOrg = {};
    SubOrg.SubOrgID = $('#hdnSubOrgId').val();
    var vd_ = window.localStorage.getItem("UUID");;
   
    SubOrg.VD = vd_;
    SubOrg.Index = Index;
    SubOrg.FilterType = FilterType;
    SubOrg.FindGoVal = FindGo;
    SubOrg.item_Id = "";
   
    Jsonobj.push(SubOrg);
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetAllUsersWithPaging,
        data: "SubOrg=" + JSON.stringify(Jsonobj),
        success: function (resp) {
            var result = resp;
            if (result.length > 0) {
                window.localStorage.setItem("CounterDefFilterGrid", JSON.stringify(result))
                $('#tblSummaryGrid >tbody').children().remove();

                for (var i = 0; i < result.length; i++) {
                    SummaryTable += "<tr onclick='DoubleClick(this)'><td style='display:none'>" + result[i].StoreId + "</td><td>" + result[i].Store + "</td><td>" + result[i].Email + "</td><td>" + result[i].Mobile + "</td><td>" + result[i].status + "</td></tr>";
                }

                $('#tblSummaryGrid tbody').remove();
                SummaryTable += "<tbody>" + SummaryTable + "</tbody>";
                $('#tblSummaryGrid').append(SummaryTable);
                window.localStorage.setItem("CounterDefGrid", JSON.stringify(SummaryTable));
                //GetPaging1();
                $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());
            }
            else {

                $('#tblSummaryGrid tbody').remove();
                SummaryTable += "<tbody>" + SummaryTable + "</tbody>";
                $('#tblSummaryGrid').append(SummaryTable);
                if (MethodType == "next") {
                    var pageIndexCreate = parseFloat($('#hdnPageIndex').val()) - 1;
                    $('#hdnPageIndex').val(pageIndexCreate);
                }

            }
        },
        error: function (e) {
           
            navigator.notification.alert( $('#hdnInvalData').val(), "", "neo ecr", "Ok");
        }

    });
}
function DoubleClick(tr) {
    window.localStorage.setItem('ctCheck', 1);
    var id = $(tr).find('td:first').html();
    var vd_ = window.localStorage.getItem("UUID");;

    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetAllUsers,
        data: "ItemId=" + id + "&VD=" + vd_,
        success: function (resp) {

            if (resp.length != 0) {

                $('#txtStore').val(resp[0].Store);
                $('#txtEmail').val(resp[0].Email);
                $('#txtMobile').val(resp[0].Mobile);
                $('#hdnTransId').val(resp[0].StoreId);
                if (resp[0].UserType == "N") {
                    document.getElementById('rdobtnCashier').checked = true;
                    document.getElementById('rdobtnStoreAdmin').checked = false;
                }
                else {
                    document.getElementById('rdobtnCashier').checked = false;
                    document.getElementById('rdobtnStoreAdmin').checked = true;
                }

                $('#lblLastUpdate').css("display", "block");
                $('#trSBInnerRow li span#lblLastUpdate').html($('#trSBInnerRow li span#lblLastUpdate').html());
                $('#trSBInnerRow li span#lblLastUpdatedUser').html(resp[0].LastUpdateduser);
                $('#trSBInnerRow li span#lblLastUpdatedDate').html(resp[0].LastUpdateduserDate);
                $('#trSBInnerRow li span#lblMode').html($('#hdnView').val());
                if (resp[0].status != "Active") {
                    $("#btnCreate").attr('style', 'display:block;');
                    $("#ShowMenuModes").attr('style', 'display:block;');
                    $("#btnSave").attr('style', 'display:none;');
                    $("#btnEdit").attr('style', 'display:none;');
                    $("#btnDelete").attr('style', 'display:none;');
                    $("#btnCancel").attr('style', 'display:block;');
                    $("#SummaryGrid").attr('style', 'display:none;');
                    $("#NewPage").attr('style', 'display:block;');
                    $("#divStatusGroup").attr('style', 'display:block;');
                    $("#divNavBarRight").attr('style', 'display:none;');
                    $("#btnInActive").addClass("disabledHyperlink");
                    $("#btnActive").removeClass("disabledHyperlink");
                }
                else {
                    $("#btnInActive").removeClass("disabledHyperlink");
                    $("#btnActive").addClass("disabledHyperlink");
                    $("#btnCreate").attr('style', 'display:block;');
                    $("#ShowMenuModes").attr('style', 'display:block;');
                    $("#btnSave").attr('style', 'display:none;');
                    $("#btnEdit").attr('style', 'display:block;');
                    $("#btnDelete").attr('style', 'display:none;');
                    $("#btnCancel").attr('style', 'display:block;');
                    $("#SummaryGrid").attr('style', 'display:none;');
                    $("#NewPage").attr('style', 'display:block;');
                    $("#divStatusGroup").attr('style', 'display:block;');
                    $("#divNavBarRight").attr('style', 'display:none;');
                }
               
                $('#inp').prop('disabled', true);
            }
        },
        error: function (e) {
           
            navigator.notification.alert( $('#hdnInvalData').val(), "", "neo ecr", "Ok");

        }
    });
    $('#SummaryGrid').css("display", "none");
    $("#ContentDet").css('display', 'block');

    $('#PrimeInfo').addClass('open');
    $('#divPrimeInformation').attr('style', 'display:block');
    EnableFormfileds(true);
}
$('#txtFindGo').keyup(function () {
    window.localStorage.setItem('ctCheck', 1);
    result = [];
    result = JSON.parse((window.localStorage.getItem("CounterDefFilterGrid")));
    if ($('#txtFindGo').val().trim() == "" || result.length <= 0) {

        GetSummaryGridData('Summary');
        return false;
    }
    else {

        GetSummaryGridData('FindGo');
    }
})
$('#btnAll').click(function () {

    $("#btnCancel").click();

    GetSummaryGridData('Summary');
});
function getResources(langCode, pageCode) {
    window.localStorage.setItem('ctCheck', 1);
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.getResource,
        data: "langCode=" + langCode + "&pageCode=" + pageCode,
        success: function (resp) {
            setLabels(resp);
        }
    });
}

function setLabels(labelData) {
    window.localStorage.setItem('ctCheck', 1);
    
    jQuery("label[for='lblUser'").html(labelData["user"]);
    jQuery("label[for='lblPrimeInformation'").html(labelData["primeInformation"]);
    jQuery("label[for='lblStore'").html(labelData["subOrganization"]);
    jQuery("label[for='lblEmail'").html(labelData["email"]);
    jQuery("label[for='lblMobile'").html(labelData["mobile"]);

    jQuery("label[for='lblEmail'").html(labelData["email"]);
    jQuery("label[for='lblMobile'").html(labelData["mobile"]);
    jQuery("label[for='lblProcessStatus'").html(labelData["status"]);
   
    $('#txtStore').attr('placeholder', labelData["PleaseEnter"] + ' ' + labelData["subOrganization"]);
    $('#txtEmail').attr('placeholder', labelData["PleaseEnter"] + ' ' + labelData["email"]);
    $('#txtMobile').attr('placeholder', labelData["PleaseEnter"] + ' ' + labelData["mobile"]);
   
    $('#hdnPleaseEnterStore').val(labelData["PosLEntStore"]);
    $('#hdnValidEmail').val(labelData["ValidEmail"]);
    $('#hdnPleaseEnter').val(labelData["PleaseEnter"]);
    $('#hdnMobile').val(labelData["mobile"]);
    $('#hdnPleaseEnterEmail').val(labelData["enterMail"]);
    $('#hdnRecSaveSucc').val(labelData["RecordsavedSucc"]);
    $('#hdnRecActSucc').val(labelData["alert6"]);
    $('#hdnRecInactSucc').val(labelData["alert7"]);
    $('#hdnCodeAlreadyExist').val(labelData["TillEmailExists"]);
    $('#hdnEntrCode').val(labelData["alert10"]);
    $('#hdnEntrName').val(labelData["alert11"]);

    jQuery("label[for='lblSCPopPwd'").html(labelData["pSettings"]);
    jQuery("label[for='lblLanguage'").html(labelData["lang"]);
    jQuery("label[for='lblDateFormat'").html(labelData["format"]);
    jQuery("label[for='lblPopOk'").html(labelData["ok"]);
    jQuery("label[for='lblPopClose'").html(labelData["cancel"]);
    $('#txtFindGo').attr('placeholder', labelData["searchHere"]);
    jQuery("label[for='lblSettings'").html(labelData["settings"]);
    jQuery("label[for='lblThemes'").html(labelData["themes"]);
    jQuery("label[for='lblTheme1'").html(labelData["theme1"]);
    jQuery("label[for='lblTheme2'").html(labelData["theme2"]);
    jQuery("label[for='lblTheme3'").html(labelData["theme3"]);
    jQuery("label[for='lblTheme4'").html(labelData["theme4"]);
    jQuery("label[for='lblChangePassword'").html(labelData["changePassword"]);
    jQuery("label[for='lblCreate'").html(labelData["create"]);
    jQuery("label[for='lblSave'").html(labelData["save"]);
    jQuery("label[for='lblEdit'").html(labelData["edit"]);
    jQuery("label[for='lblDelete'").html(labelData["delete"]);
    jQuery("label[for='lblCancel'").html(labelData["cancel"]);
    jQuery("label[for='lblStatus'").html(labelData["status"]);
    jQuery("label[for='lblActive'").html(labelData["active"]);
    jQuery("label[for='lblInactive'").html(labelData["inactive"]);
    jQuery("label[for='lblOk'").html(labelData["ok"]);
    $('#hdnChPwdSaveSucc').val(labelData["alert8"]);
    jQuery("label[for='lblUserType'").html(labelData["UserType"]);
    jQuery("label[for='lblCashier'").html(labelData["CashierName"]);
    jQuery("label[for='lblStoreAdmin'").html(labelData["StoreAdmin"]);
}
$("#btnCancel").click(function () {
    window.localStorage.setItem('ctCheck', 1);   
    clearControls_User();
    $('#lblLastUpdate').css("display", "none");  
    $('#trSBInnerRow li span#lblLastUpdatedUser').html("");
    $('#trSBInnerRow li span#lblLastUpdatedDate').html("");
    $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());
    $("#btnCreate").attr('style', 'display:block;');
    $("#ShowMenuModes").attr('style', 'display:none;');
    $("#NewPage").attr('style', 'display:none;');
    $("#divNavBarRight").attr('style', 'display:block;');
    $('#SummaryGrid').css("display", "block");
    $("#ContentDet").css('display', 'none');
    GetSummaryGridData('Summary');
});
$('#btnEdit').click(function () {
    window.localStorage.setItem('ctCheck', 1);
    Edit();
    EnableFormfileds(false);
    $('#txtStore').prop("disabled", true);
});
function Edit() {
    window.localStorage.setItem('ctCheck', 1);
    $('#trSBInnerRow li span#lblMode').html($('#hdnEdit').val());
    $("#btnCreate").attr('style', 'display:none;');
    $("#ShowMenuModes").attr('style', 'display:block;');
    $("#btnSave").attr('style', 'display:block;');
    $("#btnEdit").attr('style', 'display:none;');
    $("#btnDelete").attr('style', 'display:none;');
    $("#btnDelete").attr('style', 'border-left:1px solid #cac8c8;');
    $("#btnCancel").attr('style', 'display:block;');
    $("#divStatusGroup").attr('style', 'display:none;');   
    $("#divNavBarRight").attr('style', 'display:none;');
    $('#PrimeInfo').addClass('open');
    $('#divPrimeInformation').attr('style', 'display:block');
}
function clearControls_User() {
    window.localStorage.setItem('ctCheck', 1);
    $('#hdnTransId').val("");
    $('#hdnSubOrgId').val("");
    $('#txtStore').val("");
    $('#txtEmail').val("");
    $("#txtMobile").val("");
    $("#hdnScreenMode").val("");
}
function EnableFormfileds(Status) {
    $('#txtStore').prop("disabled", Status);
    $('#txtEmail').prop("disabled", Status);
    $('#txtMobile').prop("disabled", Status);
    $('#rdobtnCashier').prop("disabled", Status);
    $('#rdobtnStoreAdmin').prop("disabled", Status);
}
function prevItem() {
    var pageIndexCreate = parseFloat($('#hdnPageIndex').val()) - 1;
    if (parseFloat(pageIndexCreate) == 0) {
        pageIndexCreate = '1';
    }
    $('#hdnPageIndex').val(pageIndexCreate);
    var FilterType = $("#hdnFilterType").val();
    GetSummaryGridData(FilterType);
    window.localStorage.setItem('ctCheck', 1);
}
function nextItem() {
    window.localStorage.setItem('ctCheck', 1);
    var pageIndexCreate = parseFloat($('#hdnPageIndex').val()) + 1;

    $('#hdnPageIndex').val(pageIndexCreate);
    var FilterType = $('#hdnFilterType').val();
    GetSummaryGridData(FilterType + '~Index');
}
$('#rdobtnCashier').change(function () {
    window.localStorage.setItem('ctCheck', 1);
    document.getElementById('rdobtnStoreAdmin').checked = false;
    document.getElementById('rdobtnCashier').checked = true;
});
$('#rdobtnStoreAdmin').change(function () {
    window.localStorage.setItem('ctCheck', 1);
    document.getElementById('rdobtnCashier').checked = false;
    document.getElementById('rdobtnStoreAdmin').checked = true;
});