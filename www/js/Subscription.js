﻿function getSubscriptionData() {
    window.localStorage.setItem('ctCheck', 1);
    var HashVal = (window.localStorage.getItem("hashValues"));
    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');
        getResources(HashValues[7], "POSConfiguratorST");
        $('#hdnDateFromate').val(HashValues[2]);
    }
    theamLoad();
    var sess = CashierLite.Session.getInstance().get();
    if (sess != null) {
        if (sess.userName != "") {
            document.getElementById("lblName").innerHTML = sess.userName;
            $('#txtUser').val(sess.userName);
            $('#txtOldpwd').val(window.localStorage.getItem("Loginsword"));
        }
        else {
            window.open('Login.html', '_self', 'location=no');
        }
    }
    else {
        window.open('Login.html', '_self', 'location=no');
    }
    subscriptionBindData();
    subscriptionuserData();
}
function getResources(langCode, pageCode) {
    window.localStorage.setItem('ctCheck', 1);
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.getResource,
        data: "langCode=" + langCode + "&pageCode=" + pageCode,
        success: function (resp) {
            setLabels(resp);
            $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());
        }
    });
}

function subscriptionBindData() {
    window.localStorage.setItem('ctCheck', 1);
    var SummaryTable = "";
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetSubscriptionHistory,
        data: "UUID=" + window.localStorage.getItem("UUID") + "&VD=GLOBAL",
        success: function (resp) {

            jQuery("label[for='lblBindServiceType'").html(resp[0]["Id"]);
            // jQuery("label[for='lblBindCreatedDate'").html(ConvertDateAndReturnDate($('#hdnDateFromate').val(), resp[0]["CINID"]));
            jQuery("label[for='lblBindCreatedDate'").html(resp[0]["CINID"]);
            jQuery("label[for='lblBindAmount'").html(parseFloat(resp[0]["Amount"]).toFixed(2));
            jQuery("label[for='lblBindFromDate'").html(resp[0]["FromDate"]);
            jQuery("label[for='lblBindExpireDate'").html(resp[0]["Currency"]);
            // jQuery("label[for='lblBindExpireDate'").html(ConvertDateAndReturnDate($('#hdnDateFromate').val(), resp[0]["Currency"]));
            $('#tblSubscribeSummaryGrid >tbody').children().remove();
            for (var i = 0; i < resp.length; i++) {
                SummaryTable = SummaryTable + "<tr><td style='display:none'>" + (i + 1) + "</td><td>" + resp[i]["Id"] + "</td><td>" + resp[i]["CINID"] + "</td><td>" + resp[i]["FromDate"] + "</td><td>" + resp[i]["Currency"] + "</td><td align=right>" + parseFloat(resp[i]["Amount"]).toFixed(2) + "</td></tr>";
            }
            // $('#tblSummaryGrid tbody').remove();
            SummaryTable += "<tbody>" + SummaryTable + "</tbody>";
            $('#tblSubscribeSummaryGrid').append(SummaryTable);
        }
    });

}

$('#btnRenewal').click(function () {
    window.localStorage.setItem('ctCheck', 1);
    $("#hdnSubscribeId").val('');
    jQuery("label[for='lblBindServiceType'").html();
    // jQuery("label[for='lblBindCreatedDate'").html(ConvertDateAndReturnDate($('#hdnDateFromate').val(), resp[0]["CINID"]));
    jQuery("label[for='lblBindCreatedDate'").html();
    jQuery("label[for='lblBindAmount'").html();
    jQuery("label[for='lblBindExpireDate'").html();
    var elemDraft = document.getElementById("divmsg");
    elemDraft.style.display = "none";
    $('#div3').hide();
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetSubScriptionPackes,
        data: "",
        success: function (resp) {
            var langData = resp;
            var divString = '';

            for (var i = 0; i < langData.length; i++) {
                divString = divString + '<li id=li_' + langData[i].Id + ' onclick="changeSubscription(' + langData[i].Id + ',' + langData[i].Amount + ')" class="text-left"><h1><span id="dgListShow_item1_' + i + '">' + langData[i].CINID + '</span></h1> <span style="font-weight:bold">' + langData[i].Currency + '<small><span style="font-weight:bold" id="dgListShow_item2_' + i + '">' + langData[i].Amount + '</small></span></li>';

                //divString = divString + '<li id=li_' + langData[i].Id + ' onclick="changeSubscription(' + langData[i].Id + ',' + langData[i].Amount + ')" class="text-left"><h1><span id="dgListShow_item1_' + i + '">' + langData[i].CINID + '</span></h1> <span >' + langData[i].Currency + '<small><span id="dgListShow_item2_' + i + '">' + langData[i].Amount + '</small></span></li>';
            };

            divString = divString + '</tr>'
            $('#dgListShow li').remove();
            $('#dgListShow').append(divString);
            //   $('#divSubscribePaymentOptions').append(divString);
            jQuery("label[for='lblLoginRegistration'").html('Renewal');
        },
        error: function (e) {

        }
    });
    $('#myModalSubscription').modal('show');
});

function Subscribe() {
    window.localStorage.setItem('ctCheck', 1);
    $('#dgListShow').addClass('disabledC');
    $('#btnCan2').addClass('disabledC');
    if ($("#hdnSubscribeId").val() == '') {
        //var $textAndPic = $('<div></div>');
        //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        //$textAndPic.append('<div class="alert-message">'+$('#hdnPlSelSubscribePackage').val()+'</div>');
        //BootstrapDialog.alert($textAndPic);
        navigator.notification.alert($("#hdnPlSelSubscribePackage").val(), "", "neo ecr", "Ok");

        $('#dgListShow').removeClass('disabledC');
        $('#btnCan2').removeClass('disabledC');
        return false;
    }
    // CreateUserDirect();
    if (window.localStorage.getItem('OnlinePayment') == '0') {
        window.localStorage.setItem("Online", "0");
        CreateUserDirect('0');
    }
    else {
        SPCPAYSMSSentToCustomer();
    }

    //CashierLiteAppSPCPAYSMSSentToCustomer();
}

function changeSubscription(id, amount) {
    window.localStorage.setItem('ctCheck', 1);
    var id_ = '#li_' + id;
    try {
        $('#li_1')[0].style = '';
        $('#li_1')[0].childNodes[0].style = 'background-color:#348cd4;color:white';
    }
    catch (a)
    { }
    for (let i = 1; i <= $(id_)[0].parentElement.childNodes.length ; i++) {
        try{

            $('#li_' +i)[0].style = '';

            $('#li_' +i)[0].childNodes[0].style = '';
            $('#li_' +i)[0].childNodes[0].style = 'background-color:#348cd4;color:white';
        }
        catch(a)
        {

        }
    }


    $('#li_' + id)[0].style = 'background: #1bbf2b;color: #FFF;border-color:#1bbf2b';
    $('#li_' + id)[0].childNodes[0].style = 'background-color:white;color:black'
    $('#hdnSubscribeId').val(id);
    $('#hdnAmount').val(amount);
    var div3 = document.getElementById("div3");
    $('#div3').hide();

    if (id != 0) {
        try {
            $.ajax({
                type: 'GET',
                url: CashierLite.Settings.GetSubScriptionPackesByID,
                data: "Id=" + id + "&UUID=" + window.localStorage.getItem("UUID"),
                success: function (resp) {
                        $('#hdnAmount').val(resp[0].Amount);
                        $('#hdnCurrency').val(resp[0].Currency);
                        jQuery("label[for='lblExpDate'").html(resp[0].ServiceExpiryDate);
                        $('#hdnExpDate').val(resp[0].ServiceExpiryDate);
                        $('#div3').show();
                },
                error: function (e) {

                }
            });
        }
        catch (r) {

        }
    }
    else {
    }
}
function subscriptionuserData() {
    window.localStorage.setItem('ctCheck', 1);
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetSubscriptionUserData,
       // data: "UUID=" + UUID + "&VD=" + pageCode,
        data: "UUID=" + window.localStorage.getItem("UUID") + "&VD=GLOBAL",
        success: function (resp) {
           

            if (resp.split('~')[0] == "1") {
                $("#hdnMobile").val(resp.split('~')[1]);
            }
       
        }
    });
}
function CreateUserDirect(mode) {
    window.localStorage.setItem('ctCheck', 1);
    $('#myModalSubscription').children().attr("disabled", "disabled");
    var UUID = window.localStorage.getItem("UUID");

    var createUserDirectParams_ = new Object();
    createUserDirectParams_.orgName = HashValues[13].trim();
    createUserDirectParams_.userName = HashValues[6].trim();
    createUserDirectParams_.pwd = "";
    createUserDirectParams_.ContactNumber = '';
    createUserDirectParams_.UUID = UUID;
    createUserDirectParams_.txtEmail = HashValues[6].trim();
    createUserDirectParams_.curid = $('#hdnCurrency').val();
    createUserDirectParams_.VD = "GLOBAL";
    createUserDirectParams_.SchemID = $('#hdnSubscribeId').val();
    createUserDirectParams_.Amount = parseFloat($('#hdnAmount').val());
    createUserDirectParams_.SubType = 'Renewal_L';

    var ComplexObject = new Object();
    ComplexObject.createUserDirectParams = createUserDirectParams_;
    var data = JSON.stringify(ComplexObject);
    var elemDraft = document.getElementById("divmsg");
    elemDraft.style.display = "none";
    elemDraft.style.display = "block";
    $('#lblmsgDate')[0].innerText = $('#hdnPlSelSubscribePackage').val();
    $.ajax({
        type: 'POST',
        url: CashierLite.Settings.Subscribe,
        data: data,
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function (resp) {
            $('#myModalSubscription').modal('hide');
            //var d_ = resp.SubscribeResult.split('~');
            subscriptionBindData();
            jQuery("label[for='lblBindServiceType'").html();
            // jQuery("label[for='lblBindCreatedDate'").html(ConvertDateAndReturnDate($('#hdnDateFromate').val(), resp[0]["CINID"]));
            jQuery("label[for='lblBindCreatedDate'").html();
            jQuery("label[for='lblBindAmount'").html();
            jQuery("label[for='lblBindExpireDate'").html();
            $('#div3').hide();         
            $('#myModalSubscription').children().removeAttr("disabled");
            var d_ = resp.SubscribeResult.split('~');
            if (mode == "0") {
                //var $textAndPic = $('<div></div>');
                //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-info-circle info-alertSuc"></i>');

                ////$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                //$textAndPic.append('<div class="alert-message">' + $('#hdnThnkSubscribe').val() + '</div>');
                //BootstrapDialog.alert($textAndPic);
                navigator.notification.alert($("#hdnThnkSubscribe").val(), "", "neo ecr", "Ok");

                UpdatePaymentStatus('0', UUID, d_[3]);
            }
            else if (mode == "2") {
                //var $textAndPic = $('<div></div>');
                //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-info-circle info-alertSuc"></i>');

                ////$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                //$textAndPic.append('<div class="alert-message">' + $('#hdnThnkSubscribe').val() + '</div>');
                navigator.notification.alert($("#hdnThnkSubscribe").val(), "", "neo ecr", "Ok");

               
                UpdatePaymentStatus('2', UUID, d_[3]);
            }
            else if (mode == "1") {
                //var $textAndPic = $('<div></div>');
                //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                //$textAndPic.append('<div class="alert-message">Payment is Pending Please contact CashierLite Admin</div>');
                //BootstrapDialog.alert($textAndPic);

                navigator.notification.alert('Payment is Pending Please contact neo ecr Admin', "", "neo ecr", "Ok");

                UpdatePaymentStatus('1', UUID, d_[3]);
            }
            else if (mode == '4') {
                //var $textAndPic = $('<div></div>');
                //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                //$textAndPic.append('<div class="alert-message">Payment was Cancelled Please contact admin</div>');
                //BootstrapDialog.alert($textAndPic);
              
                navigator.notification.alert('Payment was Cancelled Please contact admin', "", "neo ecr", "Ok");

                UpdatePaymentStatus('4', UUID, d_[3]);
            }


        },
        error: function (e) {
        }
    });
}
function setLabels(labelData) {
    window.localStorage.setItem('ctCheck', 1);
    //alert(labelData["PlSelSubscribePackage"]);
    $('#hdnThnkSubscribe').val(labelData["alert10"]);
    $('#hdnPlSelSubscribePackage').val(labelData["PlSelSubscribePackage"]);
    jQuery("label[for='lblPay'").html(labelData["pay"]);
    jQuery("label[for='lblPleasewait'").html(labelData["Pleasewait"]);
    jQuery("label[for='lblRenewalValidTill'").html(labelData["RenewalValidTill"]);
    jQuery("label[for='lblFromDate'").html(labelData["fromDate"]);
    jQuery("label[for='lblExpireDate'").html(labelData["alert11"]);
    jQuery("label[for='lblAmount'").html(labelData["amount"]);
    jQuery("label[for='lblCreatedDate'").html(labelData["CreatedDate"]);
    jQuery("label[for='lblBindTblFromDate'").html(labelData["fromDate"]);
    jQuery("label[for='lblServiceType'").html(labelData["ServiceType"]);
    jQuery("label[for='lblUploadUserImage'").html(labelData["UploadUserImage"]);
    jQuery("label[for='lblClose'").html(labelData["closed"]);
    jQuery("label[for='lblSave'").html(labelData["save"]);
    jQuery("label[for='lblok'").html(labelData["ok"]);
    jQuery("label[for='lblRegionalLang'").html(labelData["RegionalLanguage"]);
    jQuery("label[for='lblRenewal'").html(labelData["Renewal"]);
    jQuery("label[for='lblSubcription'").html(labelData["Renewal"]);
    jQuery("label[for='lblSCPopPwd'").html(labelData["pSettings"]);
    jQuery("label[for='lblLanguage'").html(labelData["lang"]);
    jQuery("label[for='lblDateFormat'").html(labelData["format"]);
    jQuery("label[for='lblPopOk'").html(labelData["ok"]);
    jQuery("label[for='lblPopClose'").html(labelData["cancel"]);
    jQuery("label[for='lblSettings'").html(labelData["settings"]);
    jQuery("label[for='lblThemes'").html(labelData["themes"]);
    jQuery("label[for='lblTheme1'").html(labelData["theme1"]);
    jQuery("label[for='lblTheme2'").html(labelData["theme2"]);
    jQuery("label[for='lblTheme3'").html(labelData["theme3"]);
    jQuery("label[for='lblTheme4'").html(labelData["theme4"]);
    jQuery("label[for='lblChangePassword'").html(labelData["changePassword"]);
    jQuery("label[for='lblTxtChangePassword'").html(labelData["changePassword"]);
    jQuery("label[for='lblSave'").html(labelData["save"]);
    jQuery("label[for='lblEdit'").html(labelData["edit"]);
    jQuery("label[for='lblCancel'").html(labelData["cancel"]);
    jQuery("label[for='lblStatus'").html(labelData["status"]);
    jQuery("label[for='lblToggleDropdown'").html(labelData["toggleDropdown"]);
    jQuery("label[for='lblApprove'").html(labelData[""]);
    jQuery("label[for='lblConfigurators'").html(labelData["configurators"]);
jQuery("label[for='lblSubConfigurators'").html(labelData["POSEwalletConSts"]);
    jQuery("label[for='lblPrintCustomize'").html(labelData["printCustamize"]);
jQuery("label[for='lblInvoiceSummary'").html(labelData["category"]);
    jQuery("label[for='lblBarcodeGenerator'").html(labelData["barCodeGenerator"]);
    jQuery("label[for='lblMasters'").html(labelData["masters"]);
    jQuery("label[for='lblTillType'").html(labelData["tillType"]);
    jQuery("label[for='lblTill'").html(labelData["till"]);
    jQuery("label[for='lblMastersTill'").html(labelData["till"]);
    jQuery("label[for='lblTillPermissions'").html(labelData["tillPermissions"]);
    jQuery("label[for='lblCustomer'").html(labelData["customer"]);
    jQuery("label[for='lblTenderType'").html(labelData["tenderType"]);
    jQuery("label[for='lblTransactions'").html(labelData["transactions"]);
    jQuery("label[for='lblInvoice'").html(labelData["invoice"]);
    jQuery("label[for='lblSalesReturn'").html(labelData["salesReturns"]);
    jQuery("label[for='lblPosting'").html(labelData["posting"]);
    jQuery("label[for='lblPostingTillOpening'").html(labelData["tillOpening"]);
    jQuery("label[for='lblPostingTillClosing'").html(labelData["tillClosing"]);
    jQuery("label[for='lblAccountPosting'").html(labelData["accountPosting"]);
    jQuery("label[for='lblReports'").html(labelData["reports"]);
    jQuery("label[for='lblInvoiceListing'").html(labelData["invoiceListing"]);
    jQuery("label[for='lblTillInvoiceListing'").html(labelData["tillInvoiceListing"]);
    jQuery("label[for='lblTenderTypeWiseCollectionDailySummary'").html(labelData["tenderTypeWiseCollectionDailySummary"]);
    jQuery("label[for='lblSalesReturns'").html(labelData["salesReturns"]);
    jQuery("label[for='lblPrimeInformation'").html(labelData["primeInformation"]);
    jQuery("label[for='lblUser'").html(labelData["user"]);
    jQuery("label[for='lblOldPwd'").html(labelData["oldPwd"]);
    jQuery("label[for='lblNewPwd'").html(labelData["newPwd"]);
    jQuery("label[for='lblConfPwd'").html(labelData["confPwd"]);
    jQuery("label[for='lblItem'").html(labelData["item"]);
    jQuery("label[for='lblReceipt'").html(labelData["receipt"]);
    jQuery("label[for='lblVATReturns'").html(labelData["VATReturns"]);
    jQuery("label[for='lblOrgSubOrg'").html(labelData["OrgSubOrganization"]);
    jQuery("label[for='lblEPay'").html(labelData["EPay"]);
    jQuery("label[for='lblLoyaltyPoints'").html(labelData["LoyaltyPoints"]);
    jQuery("label[for='lblInvoce'").html(labelData["invoice"]);
    jQuery("label[for='lblDispatch'").html(labelData["Dispatch"]);
    jQuery("label[for='lblTillOpening'").html(labelData["tillOpening"]);
    jQuery("label[for='lblTillClosing'").html(labelData["tillClosing"]);
    jQuery("label[for='lblAnalyticalReports'").html(labelData["AnalyticalReports"]);
    jQuery("label[for='lblItemCategorySalesAnalysis'").html(labelData["ItemCategorySalesAnalysis"]);
    jQuery("label[for='lblWeekDaysRevenue'").html(labelData["WeekdaysRevenue"]);
    jQuery("label[for='lblBucketAnalysis'").html(labelData["BucketAnalysis"]);
    jQuery("label[for='lblLocationwiseAverageInvoiceValue'").html(labelData["LocationwiseAverageInvoiceValue"]);
    jQuery("label[for='lblName'").html(labelData["name"]);
    jQuery("label[for='lblWalletLogo'").html(labelData["WalletLogo"]);
    jQuery("label[for='lblWalletQRCode'").html(labelData["WalletQRCode"]);
    jQuery("label[for='lblMobile'").html(labelData["mobile"]);
    jQuery("label[for='lblWallet'").html(labelData["Wallet"]);
    jQuery("label[for='lblCurrentInventory'").html(labelData["CurrentInventory"]);
    jQuery("label[for='lblVATReturns'").html(labelData["VATReturns"]);
    $('#lblNavigationUrl').text(labelData["pos"] + "  >>  " + labelData["Renewal"]);
    $('#lblLastUpdate').text(labelData["lastUpdated"]);
    $('#hdnNotAssignCountrToTrans').val(labelData["permissionsAssisted"]);
    $('#hdnAlrtNewPwd').val(labelData["alert1"]);
    $('#hdnEnterConfPwd').val(labelData["alert2"]);
    $('#hdnPwdMatch').val(labelData["alert3"]);
    $('#hdnPwdChangeSucc').val(labelData["alert4"]);
    $('#hdnInvalidData').val(labelData["alert5"]);
    $('#hdnChPwdSaveSucc').val(labelData["alert5"]);
    $('#hdnThameChange').val(labelData["alert6"]);
    $('#hdnNew').val(labelData["new1"]);
    $('#hdnView').val(labelData["view"]);
    $('#hdnEdit').val(labelData["edit"]);
    $('#hdnDefault').val(labelData["default1"]);
    $('#btnAdd').attr('value', labelData["alert18"]);
    $('#hdnImageSave').val(labelData["ImageSave"]);
    $('#hdnSelImage').val(labelData["SelUserImage"]);
    jQuery("label[for='lblUpload'").html(labelData["upload"]);
    $('#hdnPlzEnterEWalletName').val(labelData["PlzEnterEWalletName"]);
    $('#hdnRecordsavedSucc').val(labelData["RecordsavedSucc"]);
    $('#hdnInvalidData').val(labelData["alert3"]);
    $('#hndPleaseUploadQRCodefor').val(labelData["PleaseUploadQRCodefor"]);
    $('#hdnCodeGennotconfiforthisproc').val(labelData["CodeGennotconfiforthisproc"]);
    $('#hdnInvrungtodaywecantchgmomnt').val(labelData["Invrungtodaywecantchgmomnt"]);
    //jQuery("label[for='lblPrimeInformation'").html(labelData["primeInformation"]);
    //jQuery("label[for='lblPrimeInformation'").html(labelData["primeInformation"]);

}
function SPCPAYSMSSentToCustomer() {
    window.localStorage.setItem('ctCheck', 1);
    var txtNumber = $('#hdnMobile').val();
    
    var RefNum = GUID();
    var BillNumber = GUID();
    var bodyLevelData_ = {
        "BranchID": "CASHIERADMIN",
        "TellerID": "CASHIERADMIN-Subscription",
        "DeviceID": "CASHIERADMIN-New",
        "RefNum": RefNum,
        "BillNumber": BillNumber,
        "MobileNo": txtNumber,
        "Amount": parseFloat($('#hdnAmount').val()),
        "MerchantNote": " neo ecr Payment For Subscription / Renewal"
    };
    var headerData = {
        "X-ClientCode": window.localStorage.getItem("STCMClientCode"),
        "X-UserName": window.localStorage.getItem("STCMUserName"),
        "X-Password": window.localStorage.getItem("STCMPassword"),
    };
    var data = JSON.stringify({ DirectPaymentAuthorizeRequestMessage: (bodyLevelData_) })
    var elemDraft = document.getElementById("divmsg");
    elemDraft.style.display = "none";
    //if (parseFloat($('#lblAmount')[0].innerText) == 0) {
    //if (parseFloat($('#hdnAmount').val()) == 0) {
    elemDraft.style.display = "block";
    $('#lblmsgDate')[0].innerText = "Please wait";
    $.ajax({
        type: "POST",
        headers: headerData,
        datatype: "JSON",
        url: "https://b2btest.stcpay.com.sa/B2B.DirectPayment.WebApi/DirectPayment/V3/DirectPaymentAuthorize",
        data: data,
        contentType: "application/json",
        success: function (data) {
            elemDraft.style.display = "none";
            var dOrignal_ = JSON.stringify(data);
            var dConverted_ = JSON.parse(dOrignal_);
            if (dConverted_.code != undefined) {
                window.localStorage.setItem("OtpReference", dConverted_.DirectPaymentAuthorizeResponseMessage[0].OtpReference);
                window.localStorage.setItem("STCPayPmtReference", dConverted_.DirectPaymentAuthorizeResponseMessage[0].STCPayPmtReference);
                window.localStorage.setItem("STCRefNum", RefNum);
                window.localStorage.setItem("STCBillNumber", BillNumber);
                $("#otp-modal").modal({ backdrop: 'static' });
            }
            else {
                //var $textAndPic = $('<div></div>');
                //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                //$textAndPic.append('<div class="alert-message">error Please contact Admin </div>');

                navigator.notification.alert('error Please contact Admin', "", "neo ecr", "Ok");

              

            }
            $('#dgListShow').removeClass('disabledC');
            $('#btnCan2').removeClass('disabledC');
        },
        error: function (e) {
            elemDraft.style.display = "none";
            //var $textAndPic = $('<div></div>');
            //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
            //$textAndPic.append('<div class="alert-message">STC Wallet Pay Offline - Please contact Admin </div>');
            //BootstrapDialog.alert($textAndPic);

            navigator.notification.alert('STC Wallet Pay Offline - Please contact Admin', "", "neo ecr", "Ok");

            $('#dgListShow').removeClass('disabledC');
            $('#btnCan2').removeClass('disabledC');
            
            //window.localStorage.setItem("OtpReference", "1234");
            //$("#otp-modal").modal({ backdrop: 'static' });


        }
    });
}
function GUID() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
      s4() + '-' + s4() + s4() + s4();
}
function STCOTPVerify() {
    window.localStorage.setItem('ctCheck', 1);
    if ($('#txtCustomerOTP').val() == "") {
        //var $textAndPic = $('<div></div>');
        //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        //$textAndPic.append('<div class="alert-message">Please Enter Customer OTP</div>');
        //BootstrapDialog.alert($textAndPic);


        navigator.notification.alert('Please Enter Customer OTP', "", "neo ecr", "Ok");

        return false;
    }
    if ($('#txtCustomerOTP').val() != window.localStorage.getItem("OtpReference")) {
        //var $textAndPic = $('<div></div>');
        //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        //$textAndPic.append('<div class="alert-message">Invalid OTP</div>');
        //BootstrapDialog.alert($textAndPic);


        navigator.notification.alert('Invalid OTP', "", "neo ecr", "Ok");

        return false;
    }
    paymentOTPConfrm();
}

function paymentOTPConfrm() {
    window.localStorage.setItem('ctCheck', 1);
    var TokenId = GUID();
    var bodyLevelData_ = {
        "OtpReference": window.localStorage.getItem("OtpReference"),
        "OtpValue": $('#txtCustomerOTP').val(),
        "STCPayPmtReference": window.localStorage.getItem("STCPayPmtReference"),
        "TokenReference": TokenId,
        "TokenizeYn": false
    };
    var headerData = {
        "X-ClientCode": window.localStorage.getItem("STCMClientCode"),
        "X-UserName": window.localStorage.getItem("STCMUserName"),
        "X-Password": window.localStorage.getItem("STCMPassword"),
    };
    var elemDraft = document.getElementById("divmsg");
    elemDraft.style.display = "none";
    //if (parseFloat($('#lblAmount')[0].innerText) == 0) {
    //if (parseFloat($('#hdnAmount').val()) == 0) {
    elemDraft.style.display = "block";
    $('#lblmsgDate')[0].innerText = "Please wait";
    var data = JSON.stringify({ DirectPaymentConfirmRequestMessage: (bodyLevelData_) })
    $.ajax({
        type: "POST",
        headers: headerData,
        datatype: "JSON",
        url: "https://b2btest.stcpay.com.sa/B2B.DirectPayment.WebApi/DirectPayment/V3/DirectPaymentConfirm",
        data: data,
        contentType: "application/json",
        success: function (data) {
            elemDraft.style.display = "none";
            $('#otp-modal').modal('hide');
            var dOrignal_ = JSON.stringify(data);
            var dConverted_ = JSON.parse(dOrignal_);
            if (dConverted_.code != undefined) {
                var payemtStatus = dConverted_.DirectPaymentConfirmResponseMessage[0].PaymentStatus;
                if (payemtStatus == 5) {
                    //var $textAndPic = $('<div></div>');
                    //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                    //$textAndPic.append('<div class="alert-message">Transaction Expired</div>');
                    //BootstrapDialog.alert($textAndPic);


                    navigator.notification.alert('Transaction Expired', "", "neo ecr", "Ok");

                    $('#myModal').modal('hide');
                }
                else {
                    window.localStorage.setItem("Online", "1");
                    CreateUserDirect(payemtStatus);
                }

            }
            else {
                //var $textAndPic = $('<div></div>');
                //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                //$textAndPic.append('<div class="alert-message">Payment is Pending(account was not created) Please contact Admin ' + window.localStorage.getItem("STCRefNum") + '</div>');
                //BootstrapDialog.alert($textAndPic);

                navigator.notification.alert('Payment is Pending(account was not created) Please contact Admin', "", "neo ecr", "Ok");

                $('#myModal').modal('hide');
                //error...?
            }
        },
        error: function (e) {
            elemDraft.style.display = "none";
            $('#otp-modal').modal('hide');
            $('#myModal').modal('hide');
            //var $textAndPic = $('<div></div>');
            //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
            //$textAndPic.append('<div class="alert-message">STC Wallet Pay Offline - Please contact Admin</div>');
            //BootstrapDialog.alert($textAndPic);

            navigator.notification.alert('STC Wallet Pay Offline - Please contact Admin', "", "neo ecr", "Ok");


        }
    });
}
function UpdatePaymentStatus(status, UUID, subRef) {
    window.localStorage.setItem('ctCheck', 1);
   
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.UpdatePayemntExtStatusById,
        data: "status=" + status + "&userId=" + UUID + "&Ref=" + subRef,
        success: function (resp) {
            $('#dgListShow').removeClass('disabledC');
            $('#btnCan2').removeClass('disabledC');

        },
        error: function (e) {

        }
    });
}