﻿$("#divNavBarRight").attr('style', 'display:block;');

function VD() {
    window.localStorage.setItem('ctCheck', 1);
    var vd_ = window.localStorage.getItem("UUID");
    return vd_;
}

function getResources(langCode, pageCode) {
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.getResource,
        data: "langCode=" + langCode + "&pageCode=" + pageCode,
        success: function (resp) {
            setLabels(resp);
        }
    });
}

function setLabels(labelData) {
    //jQuery("label[for='lblLoyaltyPoints'").html(labelData["LoyaltyPoints"]);
    //jQuery("label[for='lblDispatch'").html(labelData["Dispatch"]);
    //jQuery("label[for='lblLocationwiseProfitAnalysis'").html(labelData["LocwiseProfitAnlys"]);
    //jQuery("label[for='lblItemCategoryProfitAnalysis'").html(labelData["ItemCatgPrftAnlys"]);
    //jQuery("label[for='lblSCPopPwd'").html(labelData["pSettings"]);
    //alert(labelData);
    jQuery("label[for='lblLanguage'").html(labelData["lang"]);
    jQuery("label[for='lblDateFormat'").html(labelData["format"]);
    jQuery("label[for='lblPopOk'").html(labelData["ok"]);
    jQuery("label[for='lblPopClose'").html(labelData["cancel"]);
    jQuery("label[for='lblSettings'").html(labelData["settings"]);
    jQuery("label[for='lblDocumentNo']").html(labelData["documentNo"]);
    jQuery("label[for='lblDocumentDate'").html(labelData["documentDate"]);
    jQuery("label[for='lblTill'").html(labelData["till"]);
    jQuery("label[for='lblCashier'").html(labelData["cashier"]);
    //jQuery("label[for='lblShift'").html(labelData["shift"]);
    //jQuery("label[for='lblProcessStatus'").html(labelData["processStatus"]);
    jQuery("label[for='lblPrimeInformation'").html(labelData["primeInformation"]);
    jQuery("label[for='lblSubOrganization'").html(labelData["subOrganization"]);
    //jQuery("label[for='lblDocumentNumber']").html(labelData["documentNo"]);
    //jQuery("label[for='lblDocDate'").html(labelData["documentDate"]);
    jQuery("label[for='lblCounterTill'").html(labelData["till"]);
    jQuery("label[for='lblTillCashier'").html(labelData["cashier"]);
    //jQuery("label[for='lblTillShift'").html(labelData["shift"]);
    jQuery("label[for='lblThemes'").html(labelData["themes"]);
    jQuery("label[for='lblTheme1'").html(labelData["theme1"]);
    jQuery("label[for='lblTheme2'").html(labelData["theme2"]);
    jQuery("label[for='lblTheme3'").html(labelData["theme3"]);
    jQuery("label[for='lblTheme4'").html(labelData["theme4"]);
    jQuery("label[for='lblChangePassword'").html(labelData["changePassword"]);
    jQuery("label[for='lblTillOpening'").html(labelData["tillopenClose"]);
    jQuery("label[for='lblCreate'").html(labelData["create"]);
    jQuery("label[for='lblSave'").html(labelData["save"]);
    jQuery("label[for='lblDelete'").html(labelData["delete"]);
    jQuery("label[for='lblCancel'").html(labelData["cancel"]);
    jQuery("label[for='lblStatus'").html(labelData["status"]);
    jQuery("label[for='lblToggleDropdown'").html(labelData["toggleDropdown"]);
    jQuery("label[for='lblActive'").html(labelData["active"]);
    jQuery("label[for='lblInActive'").html(labelData["inactive"]);
    jQuery("label[for='lblEdit'").html(labelData["edit"]);
    jQuery("label[for='lblFilter'").html(labelData["filter"]);
    //jQuery("label[for='lblCounterToggleDropdown'").html(labelData["toggleDropdown"]);
    jQuery("label[for='lblAll'").html(labelData["all"]);
    jQuery("label[for='lblToDay'").html(labelData["toDay"]);
    jQuery("label[for='lblYesterday'").html(labelData["yesterDay"]);
    jQuery("label[for='lblLastSevenDays'").html(labelData["lastSevenDays"]);
    jQuery("label[for='lblCurrentMonth'").html(labelData["currentMonth"]);
    $('#hdnErrOccer').val(labelData["errorOccer"]);
    //jQuery("label[for='lblSearchOpen'").html(labelData["open"]);
    //jQuery("label[for='lblSearchClosed'").html(labelData["closed"]);
    jQuery("label[for='lblConfigurators'").html(labelData["configurators"]);
    jQuery("label[for='lblPrintCustomize'").html(labelData["printCustamize"]);
    jQuery("label[for='lblBarcodeGenerator'").html(labelData["barCodeGenerator"]);
    jQuery("label[for='lblMasters'").html(labelData["masters"]);
    jQuery("label[for='lblTillType'").html(labelData["tillType"]);
    // jQuery("label[for='lblMastersTill'").html(labelData["till"]);
    jQuery("label[for='lblTillPermissions'").html(labelData["tillPermissions"]);
    jQuery("label[for='lblCustomer'").html(labelData["customer"]);
    jQuery("label[for='lblTenderType'").html(labelData["tenderType"]);
    jQuery("label[for='lblTransactions'").html(labelData["transactions"]);
    //jQuery("label[for='lblInvoice'").html(labelData["invoice"]);
    // jQuery("label[for='lblSalesReturn'").html(labelData["salesReturn"]);
    jQuery("label[for='lblPosting'").html(labelData["posting"]);
    //jQuery("label[for='lblPostingTillOpening'").html(labelData["tillOpening"]);

    //jQuery("label[for='lblPostingTillClosing'").html(labelData["tillClosing"]);
    jQuery("label[for='lblAccountPosting'").html(labelData["accountPosting"]);
    jQuery("label[for='lblReports'").html(labelData["reports"]);
    jQuery("label[for='lblInvoiceListing'").html(labelData["invoiceListing"]);
    jQuery("label[for='lblTillInvoiceListing'").html(labelData["tillInvoiceListing"]);
    jQuery("label[for='lblTenderTypeWiseCollectionDailySummary'").html(labelData["tenderTypeWiseCollectionDailySummary"]);
    jQuery("label[for='lblSalesReturns'").html(labelData["salesReturns"]);
    //jQuery("label[for='lblCurrency'").html(labelData["currency"]);
    // jQuery("label[for='lblFloat'").html(labelData["flot"]);
    //jQuery("label[for='lblDepositAmount'").html(labelData["depositAmount"]);
    //jQuery("label[for='lblNetTotal'").html(labelData["netTotal"]);
    jQuery("label[for='lblRemarks'").html(labelData["remarks"]);
    jQuery("label[for='lblLinesTender'").html(labelData["linesTender"]);
    //jQuery("label[for='lblTableTenderType'").html(labelData["tenderType"]);
    //jQuery("label[for='lblDeposit'").html(labelData["deposit"]);
    //jQuery("label[for='lblTotal'").html(labelData["total"]);
    //jQuery("label[for='lblDenomination'").html(labelData["denomination"]);
    //jQuery("label[for='lblPopDenomination'").html(labelData["denomination"]);
    //jQuery("label[for='lblPopFloat'").html(labelData["flot"]);
    //jQuery("label[for='lblPopDeposit'").html(labelData["deposit"]);
    //jQuery("label[for='lblPopNetQty'").html(labelData["netQty"]);
    //jQuery("label[for='lblPopNetValue'").html(labelData["netValue"]);
    jQuery("label[for='lblPopOk'").html(labelData["ok"]);
    //jQuery("label[for='lblPopCancel'").html(labelData["cancel"]);
    //jQuery("label[for='lblPopDenominations'").html(labelData["denominations"]);

    $('#txtFindGo').attr("placeholder", labelData["searchHere"]);
    $('#txtCounterName').attr("placeholder", labelData["fillTill"]);
    $('#txtCashier').attr("placeholder", labelData["plzFill"] + ' ' + labelData["cashier"]);
    $('#txtSubOrganization').attr("placeholder", labelData["alert11"]);
    $('#hdnFloat').attr("placeholder", labelData["flot"]);

    //jQuery("label[for='lblAlertDelete'").html(labelData["deleteSuccess"]);
    //jQuery("label[for='lblAlertSaved'").html(labelData["saveSuccess"]);
    //jQuery("label[for='lblAlertSavedSuccessfully'").html(labelData["saveSuccess"]);
    //jQuery("label[for='lblAlertDocExist'").html(labelData["alreadyExist"]);
    //jQuery("label[for='lblAlertDeleteSucc'").html(labelData["deleteSuccess"]);
    //jQuery("label[for='lblAlertRecAct'").html(labelData["recActSuccess"]);
    //jQuery("label[for='lblAlertInactSucc'").html(labelData["recInactiveSucc"]);
    //jQuery("label[for='lblAlertErrOcc'").html(labelData["errorOccer"]);
    //jQuery("label[for='lblAlertErrorOcc'").html(labelData["errorOccer"]);
    $('#lblNavigationUrl').text(labelData["pos"] + "  >>  " + labelData["POSMasters"] + "  >>  " + labelData["tillopenClose"]);
    $('#lblLastUpdate').text(labelData["lastUpdated"]);
    $('#lblLastUpdate').css("display", "none");
    //jQuery("label[for='lblAnalyticalReports'").html(labelData["AnalyticalReports"]);
    //jQuery("label[for='lblItemCategorySalesAnalysis'").html(labelData["ItemCategorySalesAnalysis"]);
    //jQuery("label[for='lblWeekDaysRevenue'").html(labelData["WeekdaysRevenue"]);
    //jQuery("label[for='lblBucketAnalysis'").html(labelData["BucketAnalysis"]);
    //jQuery("label[for='lblLocationwiseAverageInvoiceValue'").html(labelData["LocationwiseAverageInvoiceValue"]);

    //$('#hdnNPermInv').val(labelData["alert1"]);
    //$('#hdnNAsnCounterTran').val(labelData["alert2"]);
    $('#hdnInvalData').val(labelData["alert3"]);
    //$('#hdnContactAdmin').val(labelData["alert4"]);


    // $('#hdnAlrtDocNo').val(labelData["alert15"]);
    $('#hdnFillTill').val(labelData["fillTill"]);
    $('#hdnFillCasher').val(labelData["plzFill"] + ' ' + labelData["cashier"]);
    //$('#hdnFillShift').val(labelData["fillShift"]);


    //$('#hdnDeleteSuccess').val(labelData["deleteSuccess"]);
    //$('#hdnSaveSuccess').val(labelData["saveSuccess"]);
    //$('#hdnActSuccess').val(labelData["recActSuccess"]);
    //$('#hdnAlreadyExist').val(labelData["alert5"]);
    //$('#hdnRecInactiveSucc').val(labelData["recInactiveSucc"]);
    //$('#hdnErrorOccer').val(labelData["errorOccer"]);
    //$('#hdnTenderTypeExist').val(labelData["alert6"]);
    //$('#hdnTillOpenRClosed').val(labelData["alert7"]);
    //$('#hdnCasherAlreadyExist').val(labelData["recInactiveSucc"]);
    // $('#hdnAlreadyOpen').val(labelData["alert9"]);
    //$('#hdnAlreadyOpenIn').val(labelData["alert10"]);
    //$('#fillSubOrg').val(labelData["alert11"]);
    //$('#hdnEnterDocNo').val(labelData["alert12"] + " " + labelData["documentNo"]);
    //$('#hdnFillCurrency').val(labelData["alert13"]);
    //$('#hdnDelConf').val(labelData["alert14"]);
    //$('#hdnChPwdSaveSucc').val(labelData["saveSuccess"]);

    $('#hdnThameChange').val(labelData["alert17"]);
    $('#hdnNew').val(labelData["new1"]);
    $('#hdnView').val(labelData["view"]);
    $('#hdnEdit').val(labelData["edit"]);
    $('#hdnDefault').val(labelData["default1"]);
    // $('#btnAdd').attr('value', labelData["alert18"]);
    $('#hdnNotAssignCountrToTrans').val(labelData["permissionsAssisted"]);
    //$('#btnDelete1').attr('value', labelData["delete"]);
    //$('#btnAddPop').attr('value', labelData["alert18"]);
    //$('#btnDeletePop').attr('value', labelData["delete"]); 
    $('#hdnSubOrgAlrt').val(labelData["alert11"]);
    $('#hdnDocNo').val(labelData["POSMsgDocNo"]);
}

//Get Login UserName
function GetLoginUserName() {
    window.localStorage.setItem('ctCheck', 1);
    var sess = CashierLite.Session.getInstance().get();
    if (sess != null) {
        if (sess.userName != "") {
            document.getElementById("lblName").innerHTML = sess.userName;
        }
        else {
            window.open('Login.html', '_self', 'location=no');
        }
    }
    else {
        window.open('Login.html', '_self', 'location=no');
    }
    theamLoad();
}
function theamLoad() {
    window.localStorage.setItem('ctCheck', 1);
    if (window.localStorage.getItem("Theam") != null)
        $('head').append('<link rel="stylesheet" href=' + window.localStorage.getItem("Theam") + ' type="text/css" />');
}

function defaultdata() {
    window.localStorage.setItem('ctCheck', 1);
    var Paging = ""; var CompanyId = ""; var SubOrgId = ''; var Orgcode = ''; var OBS = ''; var CashierID = window.localStorage.getItem("CashierID"); var Userid = '';
    var Lang = '';
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        // getResources(hashSplit[7], "POSCounterDefination");
        getResources(hashSplit[7], "POSCOUOP");
        // GetAutoCodeGen(hashSplit[1], '3');
        CompanyId = hashSplit[0];
        SubOrgId = hashSplit[1];
        OBS = hashSplit[9];
        Orgcode = hashSplit[17];
        Userid = hashSplit[14];
        Lang = hashSplit[7];
        Paging = hashSplit[19];
    }
    document.getElementById("lblMTillCode").innerHTML = hashSplit[28];
    $('#CompanyId').val(CompanyId);
    $('#hdnBranchId').val(SubOrgId);
    $('#hdnOBS').val(OBS);
    $('#hdnUserId').val(Userid);
    //$('#txtPosCode').val(Orgcode);
    $('#hdnLanguage').val(Lang);
    $('#hdnPaging').val(Paging);
    $('#Content_two').css("display", "none");
    $('#Create').css("display", "block");
    $('#Edit').css("display", "none");
    $('#Cancel').css("display", "none");
    $('#btnSave').css("display", "none");
    $('#Delete').css("display", "none");
    $('#Status').css("display", "none");
    $('#divActive').css("display", "none");
}

function TillOpeningMainData() {
    window.localStorage.setItem('ctCheck', 1);
    GetLoginUserName();
    //  ClearScreen();
    ClearTotalScreen();
    defaultdata();
    //PrintSLLoad();
    var HashVal = (window.localStorage.getItem("hashValues"));
    var HashValues;
    // alert('hi');
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');

        //getResources(HashValues[7], "POSCOUOP");
        $('#hdnDateFromate').val(HashValues[2]);
        $('#txtSubOrganization').val(HashValues[17]);
        $('#hdnSubOrganization').val(HashValues[1]);
        $('#hdnParentSubOrganization').val(HashValues[0]);
        // GetSummaryGridData();
        //GetSubOrgNameWithCur(HashValues[1]);
        GetCashiersData(HashValues[1]);
        GetCountersData(HashValues[1]);
        // GetShiftsData(HashValues[1]);
        GetTenderTypes(HashValues[1]);
        //GetAutoCodeGen(HashValues[1]);
        RoundAndDecimalPrice(HashValues[16]);
        $('#hdnPaging').val(HashValues[19]);

        // GetAutoCodeGen(HashValues[1], '3');
        // alert(HashValues[1]);
        $('#calDocumentDate').DatePicker({
            format: $('#hdnDateFromate').val(),
            changeMonth: 'true',
            changeYear: 'true',
        });

    }
    // alert('hi1');

    GetSummaryGridData('Summary');
    // EntryGridSettings(5);

    $('#myModalprint').hide();
    $('#lblLastUpdate').css("display", "none");
    var HashVal = (window.localStorage.getItem("hashValues"));
    window.localStorage.setItem('ctCheck', 1);

    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');
        $('#hdnDateFromate').val(HashValues[2])
        if (HashValues[7] == 'ar-SA') {
            $('html,body').append('<link rel="stylesheet" href="Styles/css/bootstrap-rtl.css" type="text/css" />');
            $('html,body').append('<link rel="stylesheet" href="Styles/css/bootstrap-flipped.css" type="text/css" />');
            $('head').append('<script src="scripts/bootstrap-dialog-SA.min.js"></script>');
        }
    }
}

$("#btnCreate").click(function () {
    window.localStorage.setItem('ctCheck', 1);
    ClearTotalScreen();
    defaultdata();
    //GetAutoCodeGen($('#hdnBranchId').val(), '3');
    $('#lblLastUpdate').css("display", "none");
    //$('#trSBInnerRow li span#lblLastUpdate').html("");
    $('#trSBInnerRow li span#lblLastUpdatedUser').html("");
    $('#trSBInnerRow li span#lblLastUpdatedDate').html("");
    //$('#trSBInnerRow li span#lblMode').html("New");
    $('#trSBInnerRow li span#lblMode').html($('#hdnNew').val());
    $('#hdnMode').val('New');

    $("#inp").removeAttr("disabled", "disabled");
    $("#btnCreate").attr('style', 'display:none;');
    $("#ShowMenuModes").attr('style', 'display:block;');
    $("#btnSave").attr('style', 'display:block;');
    $("#btnEdit").attr('style', 'display:none;');
    $("#btnDelete").attr('style', 'display:none;');
    $("#btnCancel").attr('style', 'display:block;');
    $("#SummaryGrid").attr('style', 'display:none;');
    $("#NewPage").attr('style', 'display:block;');
    $("#divStatusGroup").attr('style', 'display:none;');
    $("#divNavBarRight").attr('style', 'display:none;');

    var CurDate = new Date();
    var DateWithFormate = CurDate.getFullYear() + '-' + ((CurDate.getMonth() + 1) < 10 ? '0' : '') + (CurDate.getMonth() + 1) + '-' + (CurDate.getDate() < 10 ? '0' : '') + CurDate.getDate();
    ConvertDateWithFormat($('#hdnDateFromate').val(), DateWithFormate, "calDocumentDate");



    //$('#lblLastUpdate').css("display", "none");
    //$('#trSBInnerRow li span#lblLastUpdatedUser').html("");
    //$('#trSBInnerRow li span#lblLastUpdatedDate').html("");
    //$('#trSBInnerRow li span#lblMode').html($('#hdnNew').val());

    //$("#btnCreate").attr('style', 'display:none;');
    //$("#ShowMenuModes").attr('style', 'display:block;');
    //$("#btnSave").attr('style', 'display:block;');
    // alert('save');
    //$("#btnEdit").attr('style', 'display:none;');
    //$("#btnDelete").attr('style', 'display:none;');
    //$("#btnCancel").attr('style', 'display:block;');
    // $("#SummaryGrid").attr('style', 'display:none;');
    // $("#NewPage").attr('style', 'display:block;');
    // $("#divStatusGroup").attr('style', 'display:none;');
    // $("#divNavBarRight").attr('style', 'display:none;');

    EnableFormfileds(false)

    $('#PrimeInfo').addClass('open');
    $('#divPrimeInformation').attr('style', 'display:block');
});

$("#btnCancel").click(function () {
    window.localStorage.setItem('ctCheck', 1);
    //GetSummaryGridData();
    ClearTotalScreen();
    $('#lblLastUpdate').css("display", "none");    //$('#trSBInnerRow li span#lblLastUpdate').html("");
    $('#trSBInnerRow li span#lblLastUpdatedUser').html("");
    $('#trSBInnerRow li span#lblLastUpdatedDate').html("");
    //$('#trSBInnerRow li span#lblMode').html("Default");
    $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());
    $("#btnCreate").attr('style', 'display:block;');
    $("#ShowMenuModes").attr('style', 'display:none;');
    $("#SummaryGrid").attr('style', 'display:block;');
    $("#NewPage").attr('style', 'display:none;');
    $("#divNavBarRight").attr('style', 'display:block;');
    GetSummaryGridData('Summary');
});


function Edit() {
    window.localStorage.setItem('ctCheck', 1);
    //$('#trSBInnerRow li span#lblMode').html("Edit");
    $('#trSBInnerRow li span#lblMode').html($('#hdnEdit').val());
    $("#btnCreate").attr('style', 'display:none;');
    $("#ShowMenuModes").attr('style', 'display:block;');
    $("#btnSave").attr('style', 'display:block;');
    $("#btnEdit").attr('style', 'display:none;');
    $("#btnDelete").attr('style', 'display:none;');
    //$("#btnDelete").attr('style', 'border-left:1px solid #cac8c8;');
    $("#btnCancel").attr('style', 'display:block;');
    $("#divStatusGroup").attr('style', 'display:none;');
    $("#divNavBarRight").attr('style', 'display:none;');
    $('#PrimeInfo').addClass('open');
    $('#divPrimeInformation').attr('style', 'display:block');
    $("#inp").removeAttr("disabled", "disabled");
    $('#hdnMode').val('Edit');
    EnableFormfileds(true);
}

//function ClearScreen() {
//   // window.localStorage.setItem('ctCheck', 1);
//    //$('#txtSubOrganization').val('');
//    //$('#hdnSubOrganization').val('');
//    //$('#hdnParentSubOrganization').val('');
//    //$('#txtCurrency').val('');
//    //$('#hdnCurrency').val('');
//}
function ClearTotalScreen() {
    window.localStorage.setItem('ctCheck', 1);
    $('#hdnSubOrganization').val('');
    $('#txtSubOrganization').val('');
    $('#txtCounterName').val('');
    $('#txtCashier').val('');
    $('#txtCounterOpeningNo').val('');
    //$('#calDate').val('');
    $('#txtFloat').val('0.00');
    // $('#txtDepositAmt').val('0.00');
    // $('#txtNetTotal').val('0.00');
    $('#txtRemarks').val('');

    $("#hdnTransId").val('');
    $("#hdnLastUpdatedBy").val('');
    $("#hdnLastUpdatedDate").val('');
    $("#hdnMode").val('New');
    $("#hdnLastUpdatedByName").val('');
    //$("#CustAddress_GUID").val('');
    $('#txtFindGo').val('');
    // $('#hdnRPopId').val('');
    $('#hdnRId').val('');
    $('#ChckTillClosure').prop('checked', false);
    $('#ChckTillClosure').prop('disabled', false);

    // EntryGridSettings(4);

    //$('#rdobtnReplace').prop('checked', false)
    //$('#rdobtnCash').prop('checked', true)
    //ClearGrid();

    //$('#txtTotal').val('0.00');
    //$('#txtRemarks').val('');
    //$('#calRefInvoiceDate').val('');
    //$('#txtRefInvoiceNo').val('');
    //$('#hdnRefInvoiceNo').val('');
    //$('#txtSalesReturnNo').val('');
    //$('#hdnTransId').val('');
    //$('#hdnLastUpdatedBy').val('');
    //$('#hdnLastUpdatedDate').val('');
    //$('#hdnLastUpdatedByName').val('');
    //var CurDate = new Date();
    //var DateWithFormate = CurDate.getFullYear() + '-' + (CurDate.getMonth() + 1) + '-' + CurDate.getDate();
    //ConvertDateWithFormat($('#hdnDateFromate').val(), DateWithFormate, "calDocumentDate")
    //EntryGridSettings(5);
    //$('#chkheader').removeAttr('checked', false)

}
$("#txtSubOrganization").autocomplete({
    dataFilter: function (data) { return data },
    source: function (req, add) {
        var OBSCode = (window.localStorage.getItem("hashValues"));
        if (OBSCode != null && OBSCode != '') {
            var OBS = OBSCode.split('~');
            var ss = OBS[0];
        }
        var jsonObjOrg = [];
        $.ajax({
            url: CashierLite.Settings.GetSubOrgData,
            data: { OBSCode: ss, Code: $('#txtSubOrganization').val(), VD: VD() },
            dataType: "json",
            type: 'GET',
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                add($.map(data, function (el) {
                    return {
                        label: el.OrgCode,
                        val: el.ORG_ID
                    };
                }));
            },
        });
    },
    change: function (e, i) {
        if (i.item) {
        }
        else {
            ClearTotalScreen();
        }
    },
    select: function (event, ui) {
        $('#txtSubOrganization').val(ui.item.label);
        $('#hdnSubOrganization').val(ui.item.val);
        $('#txtCounterName').val('');
        $('#hdnCounterName').val('');
        $('#txtCashier').val('');
        $('#hdnCashier').val('');
        //   GetConfigData(ui.item.val);
    }
});

$("#txtCounterName").autocomplete({
    source: function (request, response) {
        //var HashValues = (window.localStorage.getItem("hashValues"));
        //if (HashValues != null && HashValues != '') {
        //    var hashSplit = HashValues.split('~');
        //    SubOrgId = hashSplit[1];
        //}
        if ($('#txtSubOrganization').val() == "" || $('#hdnSubOrganization').val() == "") {
            
            navigator.notification.alert( $("#hdnSubOrgAlrt").val() , "", "neo ecr", "Ok");

            $('#txtSubOrganization').focus();
            return false;
        }
        $.ajax({
            url: CashierLite.Settings.GetCounterNames,
            data: { SubOrgId: $('#hdnSubOrganization').val(), Code: $('#txtCounterName').val(), VD: VD() },
            dataType: "json",
            type: "Get",
            //contentType: "application/json; charset=utf-8",
            success: function (data) {
                window.localStorage.setItem('ctCheck', 1);
                response($.map(data, function (item) {
                    return {
                        label: item.COUNTERCODE + '/' + item.COUNTERNAME,
                        val: item.COUNTERID
                    }
                }))
            },
            error: function (response) {
            },
            failure: function (response) {
            }
        });

    },
    change: function (e, i) {
        if (i.item) {

        }
        else {
            $('#txtCounterName').val('');
            $('#hdnCounterName').val('');
            $('#txtCashier').val('');
            $('#hdnCashier').val('');
        }
    },
    select: function (e, i) {
        $('#hdnCounterName').val(i.item.val);
        $('#txtCashier').val('');
        $('#hdnCashier').val('');
    },
    minLength: 1
});

$("#txtCashier").autocomplete({
    source: function (request, response) {
        //var HashValues = (window.localStorage.getItem("hashValues"));
        //if (HashValues != null && HashValues != '') {
        //    var hashSplit = HashValues.split('~');
        //    SubOrgId = hashSplit[1];
        //}
        if ($('#txtSubOrganization').val() == "" || $('#hdnSubOrganization').val() == "") {
         
            navigator.notification.alert($('#hdnSubOrgAlrt').val() , "", "neo ecr", "Ok");

            $('#txtSubOrganization').focus();
            return false;
        }
        if ($('#txtCounterName').val() == "" || $('#txtCounterName').val() == "") {
         
            navigator.notification.alert($('#hdnFillTill').val() , "", "neo ecr", "Ok");

            $('#txtCounterName').focus();
            return false;
        }
        $.ajax({
            url: CashierLite.Settings.GetCashiers,
            data: { SubOrgId: $('#hdnSubOrganization').val(), Code: $('#txtCashier').val(), Counter: $('#hdnCounterName').val(), VD: VD() },
            dataType: "json",
            type: "Get",
            //contentType: "application/json; charset=utf-8",
            success: function (data) {
                window.localStorage.setItem('ctCheck', 1);
                response($.map(data, function (item) {
                    return {
                        label: item.EMP_CODE + '/' + item.EMP_NAME,
                        val: item.EMP_ID
                    }
                }))
            },
            error: function (response) {
            },
            failure: function (response) {
            }
        });
    },
    change: function (e, i) {
        if (i.item) {

        }
        else {
            $('#txtCashier').val('');
            $('#hdnCashier').val('');
        }
    },
    select: function (e, i) {
        $('#hdnCashier').val(i.item.val);
    },
    minLength: 1
});

//Client Side Validation
function ClientSideValidations(code) {
    switch (code) {
        case "Save":
            if ($('#txtSubOrganization').val() == "" || $('#hdnSubOrganization').val() == "") {
              
                navigator.notification.alert($('#hdnSubOrgAlrt').val() , "", "neo ecr", "Ok");

                $('#txtSubOrganization').focus();
                return false;
            }
            //if ($('#txtCounterOpeningNo').val() == "") {
            //    var $textAndPic = $('<div></div>');
            //    $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
            //    $textAndPic.append('<div class="alert-message">' + $('#hdnDocNo').val() + '</div>');
            //    BootstrapDialog.alert($textAndPic);
            //    $('#txtCounterOpeningNo').focus();
            //    return false;
            //}
            if ($('#calDocumentDate').val() == "") { 

                navigator.notification.alert('Please select Document Date' , "", "neo ecr", "Ok");

                $('#calDocumentDate').focus();
                return false;
            }
            if ($('#txtCounterName').val() == "" || $('#txtCounterName').val() == "") {
            

                navigator.notification.alert($('#hdnFillTill').val() , "", "neo ecr", "Ok");

                $('#txtCounterName').focus();
                return false;
            }
            if ($('#txtCashier').val() == "") { 

                navigator.notification.alert($('#hdnFillCasher').val() , "", "neo ecr", "Ok");

                $('#txtCashier').focus();
                return false;
            }
            //if ($('#txtFloat').val() == "0.00") {
            //    var $textAndPic = $('<div></div>');
            //    $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
            //    $textAndPic.append('<div class="alert-message">' + $('#hdnFloat').val() + '</div>');
            //    BootstrapDialog.alert($textAndPic);
            //    $('#txtFloat').focus();
            //    return false;
            //}

            return true;
            break;
        case "Delete":
            //if (confirm("Are you sure you want to delete this user?"))
            if (confirm($('#hdnDelConf').val() + "?"))
                return true;
            else
                return false;
            break;
    }

}
//Save and Edit
$("#btnSave").click(function () {
    if (ClientSideValidations('Save')) {
        GetNewData();
    }
});

//Get NewData
function GetNewData() {
    var HashVal = (window.localStorage.getItem("hashValues"));

    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');
    }
    // alert('hi1');
    var Mode = $("#hdnMode").val();
    if ($("#hdnTransId").val() == "" || $("#hdnTransId").val() == null || $("#hdnMode").val() == 'New') {
        Mode = "Create";
        $("#hdnTransId").val(guid());
    }
    else if ($("#hdnMode").val() == "Edit")
        Mode = "Update";
    else if ($("#hdnMode").val() == "Delete")
        Mode = "Delete";

    CounterHeaderData = {}
    objCounterHeaderData = [];
    //objCounterTenderData = [];

    var icode = 'POSCOUOP';
    var strTable = 'RATABLE031';
    var strColumn = 'COLUMN013';

    //if ($("#txtCounterOpeningNo").val().trim() == '' && $("#hdnMode").val() == 'New') {
    //    GenerateCode(HashValues[1], icode, 'Y', strTable, strColumn, HashValues[0], HashValues[1], HashValues[5], HashValues[7]);
    //    // GenerateCode(strOrgCode, strVarCode, strParamType, strTable, strColumn, USERID, BRANCHID, COMPANYID, LANGUAGE)
    //}

    var CurDate = new Date();
    var DateWithFormate = CurDate.getFullYear() + '-' + (CurDate.getMonth() + 1) + '-' + CurDate.getDate() + ' ' + CurDate.getHours() + ':' + CurDate.getSeconds() + ':' + CurDate.getSeconds();
    //alert('crea');
    //CounterOpening HeaderData
    //CounterHeaderData["BALANCEID"] = $("#hdnTransId").val();

    if ($('#hdnTransId').val() == "") {
        CounterHeaderData["BALANCEID"] = "";
    }
    else {
        CounterHeaderData["BALANCEID"] = $("#hdnTransId").val();
    }

    CounterHeaderData["BALANCECODE"] = $("#txtCounterOpeningNo").val().trim(); //$('#hdnCounterNo').val();
    CounterHeaderData["BALANCEDATE"] = GetDateWithYYYYMMDD($('#hdnDateFromate').val(), $('#calDocumentDate').val()) + ' ' + 00 + ':' + 00 + ':' + 00; // $('#calDocumentDate').val() + ' ' + 00 + ':' + 00 + ':' + 00;
    CounterHeaderData["COUNTERID"] = $("#hdnCounterName").val();
    CounterHeaderData["CASHIERID"] = $("#hdnCashier").val();
    CounterHeaderData["SHIFTID"] = "";//$("#hdnShift").val();
    CounterHeaderData["SHIFTSEQUENCE"] = '1';
    CounterHeaderData["CURRENCYID"] = '0';//$("#hdnCurrency").val();
    var PrevOpeningBal = parseFloat('0.00');
    //if (txtPreviousBal.Text.Length.Equals(0))
    //    txtPreviousBal.Text = "0.00";
    //var DecPlc = 2;
    //if ($("#hdnDecPrice").val() != '')
    //    DecPlc = $("#hdnDecPrice").val();

    //CounterHeaderData["OPENINGBALANCE"] = parseFloat(PrevOpeningBal + parseFloat($("#txtFloat").val())).toFixed(DecPlc);
    //CounterHeaderData["CASHCOLLECTION"] = parseFloat(0).toFixed(DecPlc);
    CounterHeaderData["OPENINGBALANCE"] = parseFloat($("#txtFloat").val()).toFixed(2); //parseFloat(PrevOpeningBal + parseFloat($("#txtFloat").val())).toFixed(2);
    CounterHeaderData["CASHCOLLECTION"] = parseFloat(0).toFixed(2);
    CounterHeaderData["CASHRETURNS"] = parseFloat(0);
    CounterHeaderData["CLOSINGBALANCE"] = parseFloat(0);
    //CounterHeaderData["DEPOSITAMOUNT"] = parseFloat($("#txtFloat").val()).toFixed(DecPlc);
    //CounterHeaderData["PREVIOUSDAYBAL"] = parseFloat(PrevOpeningBal).toFixed(DecPlc);
    CounterHeaderData["DEPOSITAMOUNT"] = parseFloat($("#txtFloat").val()).toFixed(2);  //parseFloat($("#txtFloat").val()).toFixed(2);
    CounterHeaderData["PREVIOUSDAYBAL"] = parseFloat(PrevOpeningBal).toFixed(2);
    CounterHeaderData["WITHDRAWAL"] = parseFloat(0);
    CounterHeaderData["POSTING_AMOUNT"] = parseFloat(0);
    CounterHeaderData["LoyalityCardFees"] = parseFloat(0);
    CounterHeaderData["STATUS"] = "O";
    CounterHeaderData["Narration"] = $("#txtRemarks").val();

    CounterHeaderData["CREATEDBY"] = HashValues[14];
    CounterHeaderData["CREATEDDATE"] = DateWithFormate;
    // alert('save');
    if (Mode == 'Create') {
        CounterHeaderData["ROWGUID"] = '';
        CounterHeaderData["LASTUPDATEDBY"] = HashValues[14];
        CounterHeaderData["LASTUPDATEDDATE"] = DateWithFormate;
    }
    if (Mode != 'Create') {
        CounterHeaderData["LASTUPDATEDBY"] = HashValues[14];
        CounterHeaderData["LASTUPDATEDDATE"] = DateWithFormate;
    }
    //alert('hi');
    CounterHeaderData["BRANCHID"] = $("#hdnSubOrganization").val();
    CounterHeaderData["COMPANYID"] = $("#hdnParentSubOrganization").val();
    CounterHeaderData["UPDATEDSITE"] = 'DBMS_REPUTIL.GLOBAL_NAME';
    CounterHeaderData["LANGID"] = HashValues[7];
    CounterHeaderData["COLUMND10"] = '6';

    if ($('#ChckTillClosure').prop('checked') == true) {
        CounterHeaderData["COLUMND10"] = '3';
        CounterHeaderData["STATUS"] = "C";
    }


    objCounterHeaderData.push(CounterHeaderData);

    //Create the CounterOpening object
    var CounterDML = new Object();
    CounterDML.Mode = Mode;
    //alert(Mode);
    CounterDML.CounterHeaderData = JSON.stringify(objCounterHeaderData);
    //CounterDML.CounterTenderData = JSON.stringify(objCounterTenderData);
    CounterDML.VD = VD();
    //Put the customer object into a container
    var ComplexObject = new Object();
    //The property name "CounterOpeningData" must match the web method argument "CounterOpeningData"!
    ComplexObject.CounterOpeningData = CounterDML;

    //Stringify and verify the object is foramtted as expected
    var data = JSON.stringify(ComplexObject);

    // tillOpeningDML.SRRefType = JSON.stringify(objSRRefType);
    //// tillOpeningDML.SRItemsData = JSON.stringify(objSRItemsData);
    // tillOpeningDML.VD = VD();
    // var ComplexObject = new Object();

    $.ajax({
        type: 'POST',
        url: CashierLite.Settings.SaveCounterOpeningData,
        data: data,
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function (resp) {
            if (resp != null) {
                // alert('save1');
                //  alert(d_);
                var d_ = '';
                var orgd = '';
                if (resp.SaveCounterOpeningDataResult.indexOf('~') != -1) {
                    d_ = resp.SaveCounterOpeningDataResult.split('~')[0];
                    orgd = resp.SaveCounterOpeningDataResult.split('~')[1];
                    resp.SaveCounterOpeningDataResult = d_;                   
                }

                if (resp.SaveCounterOpeningDataResult == '100000') {//Success Record   
                   
                    navigator.notification.alert($('#txtCounterOpeningNo').val() + ' ' + 'Record Saved Successfully', "", "neo ecr", "Ok");

                    if (Mode == "Update") {
                        $("#btnCancel").click();
                        GetSummaryGridData('Summary');
                    }
                    else
                        $("#btnCreate").click();

                    ClearTotalScreen();
                    return false;

                    // $('#SummaryGrid').css("display", "block");
                    //// $('#PrimeInfo').addClass('open');
                    // $('#divPrimeInformation').attr('style', 'display:none');
                    //return false;                   
                }
                    //if (resp.SaveSalesReturnDataResult.split('~')[0] == '100001') {//Duplicate SalesReturn Code
                    //    $("#hdnTransId").val('');
                    //    var $textAndPic = $('<div></div>');
                    //    $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                    //    $textAndPic.append('<div class="alert-message">' + $("#txtSalesReturnNo").val().trim() + $('#hdnDocNoExist').val() + '</div>');
                    //    BootstrapDialog.alert($textAndPic);

                    //    $('#PrimeInfo').addClass('open');
                    //    $('#ShippingInfo').removeClass('open');
                    //    $('#divPrimeInformation').attr('style', 'display:block');
                    //    $('#divShippingInformation').attr('style', 'display:none');
                    //    return false;

                    //}
                    //if (resp.SaveSalesReturnDataResult == 'POS10002') {//Deleted Record
                    //    var $textAndPic = $('<div></div>');
                    //    $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-info-circle info-alertSuc"></i>');
                    //    $textAndPic.append('<div class="alert-message">' + $("#txtSalesReturnNo").val().trim() + $('#hdnDelSucc').val() + '</div>');
                    //    BootstrapDialog.alert($textAndPic);

                    //    ClearTotalScreen();
                    //    $("#btnCancel").click();

                    //    GetSummaryGridData('Summary');
                    //    return false;
                    //}
                else {
                    
            navigator.notification.alert( $('#hdnErrOccer').val(), "", "neo ecr", "Ok");

                    return false;
                }
            }
            return false;
        },
        error: function (e) {
            $("#hdnTransId").val(''); 
            navigator.notification.alert( $('#hdnErrOccer').val(), "", "neo ecr", "Ok");

            return false;
        }
    });


}

function GenerateCode(strOrgCode, strVarCode, strParamType, strTable, strColumn, USERID, BRANCHID, COMPANYID, LANGUAGE) {
    window.localStorage.setItem('ctCheck', 1);
    var CounterNo_ = "0";
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GenerateCode,
        data: "strOrgCode=" + strOrgCode + "&strVarCode=" + strVarCode + "&strParamType=" + strParamType + "&strTable=" + strTable + "&strColumn=" + strColumn + "&USERID=" + USERID + "&BRANCHID=" + BRANCHID + "&COMPANYID=" + COMPANYID + "&LANGUAGE=" + LANGUAGE + "&VD=" + VD(),
        success: function (resp) {
            var CounterNo = resp;
            if (CounterNo.length > 1) {
                $('#txtCounterOpeningNo').val(CounterNo);
                CounterNo_ = CounterNo;
                return CounterNo_;
            }
        },
        error: function (e) {
            CounterNo_ = "0";
        }
    });
    return CounterNo_;
}

function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}

function EnableFormfileds(Status) {
    $('#txtSubOrganization').prop("disabled", Status);
    // $('#txtCounterOpeningNo').prop("disabled", false);
    $('#ChckTillClosure').prop("disabled", true);

    if ($("#hdnMode").val() == "New") {
        $('#ChckTillClosure').hide();
        $('#lblTillCloser').hide();
    }
    else {
        $('#ChckTillClosure').show();
        $('#lblTillCloser').show();
    }

    //if ($("#hdnMode").val() == 'New' && $("#hdnCounterNo").val() == 'Y') {
    //    $('#txtCounterOpeningNo').prop("disabled", false);
    //}
    //else {
    //    $('#txtCounterOpeningNo').prop("disabled", Status);
    //}

    ////if ($("#hdnMode").val().trim() == 'View' || $("#hdnMode").val().trim() == 'Edit' || $("#hdnMode").val().trim() == 'Active') {
    ////    $('#txtSubOrganization').prop("disabled", true);
    ////    $('#txtCurrency').prop("disabled", true);
    ////    $('#txtCounterOpeningNo').prop("disabled", true);
    ////    $('#txtParentCustomer').prop("disabled", true);
    ////}

    $('#calDocumentDate').prop("disabled", true);
    $('#txtCounterName').prop("disabled", Status);
    $('#txtCashier').prop("disabled", Status);

    if ($("#hdnMode").val() == "Edit") {
        $('#txtRemarks').prop("disabled", false);
        $('#ChckTillClosure').prop("disabled", false);
        $('#txtFloat').prop("disabled", false);
    }
    else {
        $('#txtFloat').prop("disabled", Status);
        $('#txtRemarks').prop("disabled", Status);
    }


}

//arrangeDailyCollectionData(data);

//Summary Grid Click data Binding
function DoubleClick(DoctNo) {
    ClearTotalScreen();
    // alert(DoctNo);
    //  $("#btnEdit").click();

    $('#hdnTransId').val("");
    var id = $(DoctNo).find('td:first').html();

    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetCounterOpeningDoubleClick,
        data: "Code=" + id + "&VD=" + VD(),
        success: function (resp) {
            //alert(resp);
            // var GridData = resp;
            if (resp.length > 0) {
                //Primary Info..
                $("#hdnTransId").val(resp[0].BALANCEID)
                $('#txtSubOrganization').val(resp[0].ORGCODE + " / " + resp[0].ORGNAME);
                $('#hdnSubOrganization').val(resp[0].BRANCHID);
                $('#hdnParentSubOrganization').val(resp[0].COMPANYID);

                $('#txtCounterName').val(resp[0].COUNTERCODE + " / " + resp[0].COUNTERNAME);
                $('#hdnCounterName').val(resp[0].COUNTERID);
                $('#txtCashier').val(resp[0].CASHIERCODE);
                $('#hdnCashier').val(resp[0].CASHIERID);

                $('#txtCounterOpeningNo').val(resp[0].BALANCECODE);
                var BALANCEDATE = new Date((resp[0].BALANCEDATE));
                //var BALANCEDATEFormate = BALANCEDATE.getFullYear() + '-' + (BALANCEDATE.getMonth() + 1) + '-' + BALANCEDATE.getDate();
                $('#txtCounterOpeningNo').val(resp[0].BALANCECODE);

                var DateWithFormate = BALANCEDATE.getFullYear() + '-' + ((BALANCEDATE.getMonth() + 1) < 10 ? '0' : '') + (BALANCEDATE.getMonth() + 1) + '-' + (BALANCEDATE.getDate() < 10 ? '0' : '') + BALANCEDATE.getDate();
                ConvertDateWithFormat($('#hdnDateFromate').val(), DateWithFormate, "calDocumentDate");

                $('#calDate').val(ConvertDateAndReturnDate($('#hdnDateFromate').val(), resp[0].BALANCEDATE));
                $('#txtFloat').val(parseFloat(resp[0].OPENINGBALANCE).toFixed(2));
                $('#txtRemarks').val(resp[0].Narration);
                $('#hdnLastUpdatedDate').val(resp[0].LASTUPDATEDDATE);
                $('#hdnLastUpdatedBy').val(resp[0].LASTUPDATEDBY);
                $('#hdnLastUpdatedByName').val(resp[0].LASTUPDATEDBYName);

                //var ListD = resp[0].LISTD;
                //var Tender_ = "";
                //var tender = "";

                //if (ListD.length > 0) {                   
                //    Tender_ = ListD;                   
                //    if (Tender_.length > 0) {
                //        for (var i = 0; i < Tender_.length; i++) {                           
                //            tender = "<tr><td>" + Tender_[i].TenderTypeCode + "</td><td align='right' >" + Tender_[i].Net_Qty + "</td></tr>";
                //            // $("#tableTenderTypeWiseBalance tbody").append(tender);
                //        }
                //    }
                //    alert(tender);
                //    $('#tableTenderTypeWiseBalance tbody tr').remove();
                //    $('#tableTenderTypeWiseBalance tbody').append(tender);
                //    $('#tableTenderTypeWiseBalance').find("input,button,textarea,select").attr("disabled", "disabled")

                //    // }                     
                //}

                var dataLen = resp[0].LISTD.length;
                if (dataLen != 0) {
                    var grdTable = '';
                    for (var i = 0; i < dataLen; i++) {
                        grdTable += "<tr><td>" + resp[0].LISTD[i].TenderTypeCode + "</td><td align='right'>" + resp[0].LISTD[i].Net_Qty + "</td></tr>";
                        //  alert(grdTable);                        
                    }
                }

                $("#tableTenderTypeWiseBalance tbody").append(grdTable);

                $('#lblLastUpdate').css("display", "block");
                $('#trSBInnerRow li span#lblLastUpdate').html($('#trSBInnerRow li span#lblLastUpdate').html());
                $('#trSBInnerRow li span#lblLastUpdatedUser').html(resp[0].LastUpdateduser);
                $('#trSBInnerRow li span#lblLastUpdatedDate').html(resp[0].LastUpdateduserDate);
                // alert(resp[0].STATUS);
                //$('#trSBInnerRow li span#lblMode').html("View");
                $('#trSBInnerRow li span#lblMode').html($('#hdnView').val());
                if (resp[0].STATUS == "6") {
                    $("#btnInActive").removeClass("disabledHyperlink");
                    $("#btnActive").addClass("disabledHyperlink");
                    $("#btnCreate").attr('style', 'display:block;');
                    $("#ShowMenuModes").attr('style', 'display:block;');
                    $("#btnSave").attr('style', 'display:none;');
                    $("#btnEdit").attr('style', 'display:block;');
                    $("#btnDelete").attr('style', 'display:none;');
                    $("#btnCancel").attr('style', 'display:block;');
                    $("#SummaryGrid").attr('style', 'display:none;');
                    $("#NewPage").attr('style', 'display:block;');
                    $("#divStatusGroup").attr('style', 'display:none;');
                    $("#divNavBarRight").attr('style', 'display:none;');
                }
                else {
                    $("#btnCreate").attr('style', 'display:block;');
                    $("#ShowMenuModes").attr('style', 'display:block;');
                    $("#btnSave").attr('style', 'display:none;');
                    $("#btnEdit").attr('style', 'display:none;');
                    $("#btnDelete").attr('style', 'display:none;');
                    $("#btnCancel").attr('style', 'display:block;');
                    $("#SummaryGrid").attr('style', 'display:none;');
                    $("#NewPage").attr('style', 'display:block;');
                    $("#divStatusGroup").attr('style', 'display:none;');
                    $("#divNavBarRight").attr('style', 'display:none;');
                    $("#btnInActive").addClass("disabledHyperlink");
                    $("#btnActive").removeClass("disabledHyperlink");
                }
                $('#lblLastUpdate').css("display", "block");
            }
        },
        error: function (e) {
          
            navigator.notification.alert( $('#hdnInvalData').val(), "", "neo ecr", "Ok");

        }
    });
    $('#hdnMode').val('View');
    EnableFormfileds(true);
    $('#PrimeInfo').addClass('open');
    $('#divPrimeInformation').attr('style', 'display:block');
}

function GetSummaryGridData(FilterType) {
    window.localStorage.setItem('ctCheck', 1);
    var Index = $('#hdnPageIndex').val();
    var MethodType = "";
    var FindGo = "";
    var QryType = "";
    if (FilterType == 'FindGo') {
        FindGo = $('#txtFindGo').val();
        Index = 1;
    }
    if (FilterType == 'Summary') {
        $('#txtFindGo').val('');
    }
    else {
        FindGo = $('#txtFindGo').val();
    }
    if (FilterType.indexOf('Index') != -1) {
        FilterType = FilterType.split('~')[0];
        MethodType = "next";
    }

    var HashVal = (window.localStorage.getItem("hashValues"));
    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');
    }
    var SummaryTable = '';
    Jsonobj = [];
    var SubOrg = {};
    SubOrg.SubOrgID = HashValues[1];
    var vd_ = window.localStorage.getItem("UUID");
    SubOrg.VD = vd_;
    SubOrg.Index = Index;
    SubOrg.FilterType = FilterType;
    SubOrg.FindGoVal = FindGo;
    Jsonobj.push(SubOrg);
    $("#hdnFilterType").val(FilterType);

    // alert(SubOrg.SubOrgID);
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.CounterOpeningSummaryGrid,
        data: "SubOrgId=" + JSON.stringify(Jsonobj) + "&VD=" + VD(),
        data: "Params=" + JSON.stringify(Jsonobj),
        success: function (resp) {
            var GridData = resp;
            // alert(Jsonobj);
            if (GridData.length > 0) {
                // alert('1');
                window.localStorage.setItem("CounterOpenandCloseSummaryGrid", JSON.stringify(GridData))
                $('#tblSummaryGrid >tbody').children().remove();
                for (var i = 0; i < GridData.length; i++) {
                    //var BALANCEID = GridData[i].BALANCEID;
                    //var BALANCECODE = GridData[i].BALANCECODE;
                    ////  var BALANCEDATE = GridData[i].BALANCEDATE;
                    //var BALANCEDATE = ConvertDateAndReturnDate($('#hdnDateFromate').val(), GridData[i].BALANCEDATE);
                    //var COUNTERCODE = GridData[i].COUNTERCODE;
                    //var CASHIER = GridData[i].CASHIER;
                    //var SHIFTCODE = GridData[i].SHIFTCODE;


                    //Counter = {}
                    //Counter["BALANCEID"] = GridData[i].BALANCEID;
                    //Counter["COUNTERID"] = GridData[i].COUNTERID;
                    //Counter["COUNTERNAME"] = GridData[i].COUNTERCODE;
                    //Counter["SHIFTID"] = GridData[i].SHIFTID;
                    //Counter["CASHIERID"] = GridData[i].CASHIERID;
                    //Counter["BALANCEDATE"] = GridData[i].BALANCEDATE;

                    //ConvertDateWithFormatLocalRet($('#hdnDateFromate').val(), DateWithFormate)
                    //SummaryTable += "<tr onclick=GridSingleClick(\'" + BALANCEID + "\')><td style='display:none;'>" + BALANCEID + "</td><td>" + COUNTERCODE + "</td><td>" + BALANCECODE + "</td><td>" + BALANCEDATE + "</td><td>" + STATUS + "</td></tr>"

                    var CurDate = new Date(GridData[i].BALANCEDATE);
                    var DateWithFormate = ((CurDate.getMonth() + 1) < 10 ? '0' : '') + (CurDate.getMonth() + 1) + '-' + (CurDate.getDate() < 10 ? '0' : '') + CurDate.getDate() + '-' + CurDate.getFullYear();

                    var STATUS;
                    if (GridData[i].STATUS == "6")
                        STATUS = "Open";
                    else
                        STATUS = "Closed";

                    //  SummaryTable += "<tr onclick=DoubleClick(\'" + BALANCEID + "\')><td style='display:none;'>" + BALANCEID + "</td><td>" + COUNTERCODE + "</td><td>" + BALANCECODE + "</td><td>" + BALANCEDATE + "</td><td>" + STATUS + "</td></tr>"

                    SummaryTable += "<tr onclick='DoubleClick(this)'><td style='display:none'>" + GridData[i].BALANCEID + "</td><td>" + GridData[i].COUNTERCODE + "</td><td>" + GridData[i].BALANCECODE + "</td><td>" + ConvertDateWithFormatLocalRet($('#hdnDateFromate').val(), DateWithFormate) + "</td><td>" + STATUS + "</td></tr>"; //258
                };
                $('#tblSummaryGrid tbody').remove();
                SummaryTable += "<tbody>" + SummaryTable + "</tbody>";
                $('#tblSummaryGrid').append(SummaryTable);
                //Paging
                //window.localStorage.setItem("POSSalesReturnSumGrd", JSON.stringify(SummaryTable));
                //GetPaging();
                // $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());

            }
            else {

                if (MethodType == "next") {
                    var pageIndexCreate = parseFloat($('#hdnPageIndex').val()) - 1;
                    $('#hdnPageIndex').val(pageIndexCreate);
                }
                else
                    $('#tblSummaryGrid tbody').remove();
            }
        },
        error: function (e) {
            
            navigator.notification.alert( $('#hdnInvalData').val(), "", "neo ecr", "Ok");

        }

    });

}

function FilterSummaryGrid(ControlData, ColumnData) {
    window.localStorage.setItem('ctCheck', 1);
    $('#txtFindGo').val('');
    FindGo = [];
    FindGo = JSON.parse((window.localStorage.getItem("CounterOpenandCloseSummaryGrid")));
    var SummaryTable;

    if (ColumnData == 'Status') {
        $("#hdnFilterType").val(ControlData.toLowerCase());
        GetSummaryGridData(ControlData.toLowerCase())

    }
    else
        if (ColumnData == 'CREATEDDATE') {
            $("#hdnFilterType").val(ControlData);
            GetSummaryGridData(ControlData)
        }
}


//SummaryGrid Paging
function GetPaging() {
    $('table#tblOuterSummaryGrid').each(function () {
        var currentPage = 0;
        var numPerPage = $('#hdnPaging').val();
        var $table = $(this);
        $table.bind('repaginate', function () {
            $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
        });
        $table.trigger('repaginate');
        var numRows = $table.find('tbody tr').length;
        var numPages = Math.ceil(numRows / numPerPage);
        $("#DivPager").remove();
        var $pager = $('<div id="DivPager" class="pager"></div>');
        for (var page = 0; page < numPages; page++) {
            $('<span class="page-number"></span>').text(page + 1).bind('click', {
                newPage: page
            }, function (event) {
                currentPage = event.data['newPage'];
                $table.trigger('repaginate');
                $(this).addClass('active').siblings().removeClass('active');
            }).appendTo($pager).addClass('clickable');
        }
        $pager.insertAfter($table).find('span.page-number:first').addClass('active');
    });
}
//Get RoundAndDecimalPrice
function RoundAndDecimalPrice(CurId) {
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetDefaultCurrency,
        data: "CurId=" + CurId + "&VD=" + VD(),
        success: function (resp) {
            var CurrData = resp;
            $('#hdnDecPrice').val(CurrData[0].Decimal_Place);
            //var Float = parseFloat(0).toFixed($('#hdnDecPrice').val());
            var Float = parseFloat(0).toFixed(2);
            $('#txtFloat').val(Float);
            $('#hdnRoundValue').val(CurrData[0].Rounding_Value);
        },
        error: function (e) {
        }
    });
}

var Cashiers = (window.localStorage.getItem("FillCashierData"));
//jsonObjCashier1 = [];
jsonObjCashier = [];
if (Cashiers != "" && Cashiers != null) {
    var Cas_ = Cashiers.split('$');
    for (var i = 0; i < Cas_.length; i++) {
        var cashChek = Cas_[i].split(':');
        item = {}
        item["value"] = cashChek[1];
        item["data"] = cashChek[0];
        jsonObjCashier.push(item);
    }
}

//Cashiers Autofill

function GetCashiersData(str) {
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetCashiers,
        data: "SubOrgId=" + str + "&VD=" + VD(),
        success: function (resp) {
            var CashData = resp;
            jsonObjCashier1 = [];
            jsonObjCashier = [];
            for (var i = 0; i < CashData.length; i++) {
                var CashierId = CashData[i].EMP_ID;
                var CashierName = CashData[i].EMP_CODE + " / " + CashData[i].EMP_NAME;
                item = {}
                item["code"] = CashierId;
                item["name"] = CashierName;
                jsonObjCashier1.push(item);
            };
            var dupes = {};
            $.each(jsonObjCashier1, function (i, el) {
                if (!dupes[el.code]) {
                    dupes[el.code] = true;
                    jsonObjCashier.push(el);
                }
            });
            window.localStorage.setItem("FillCashierData", jsonObjCashier);
        },
        error: function (e) {
        }
    });
}



function GetCountersData(str) {
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetCounterNames,
        data: "SubOrgId=" + str + "&VD=" + VD(),
        success: function (resp) {
            var CouData = resp;
            jsonObjCounter = [];
            for (var i = 0; i < CouData.length; i++) {
                var CounterId = CouData[i].COUNTERID;
                var CounterName = CouData[i].COUNTERCODE + " / " + CouData[i].COUNTERNAME;
                item = {}
                item["code"] = CounterId;
                item["name"] = CounterName;
                jsonObjCounter.push(item);
            };
            window.localStorage.setItem("FillCounterData", jsonObjCounter);
        },
        error: function (e) {
        }
    });
}

var Counters = (window.localStorage.getItem("FillCounterData"));
jsonObjCounter = [];
if (Counters != "" && Counters != null) {
    var Cou_ = Counters.split('$');
    for (var i = 0; i < Cou_.length; i++) {
        var couChek = Cou_[i].split(':');
        item = {}
        item["value"] = couChek[1];
        item["data"] = couChek[0];
        jsonObjCounter.push(item);
    }
}

//Get Tender Types

function GetTenderTypes(str) {
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetTenderTypesByOrgId,
        data: "SubOrgId=" + str + "&VD=" + VD(),
        success: function (resp) {
            var Tender = resp;
            jsonObjTenderT = [];
            for (var i = 0; i < Tender.length; i++) {
                var TenderId = Tender[i].TenderTypeId;
                var TenderName = Tender[i].TenderTypeCode;
                //+ " / " + Tender[i].TenderTypeName;
                var CurId = Tender[i].CurrencyId;
                var DefaultTender = Tender[i].DefaultTender;

                item = {}
                item["Id"] = TenderId;
                item["Name"] = TenderName;
                item["CurId"] = CurId;
                item["DefTender"] = DefaultTender;

                jsonObjTenderT.push(item);
            };
            window.localStorage.setItem("FillTenderTypes", jsonObjTenderT);
        },
        error: function (e) {
        }
    });
}
var Tenders_ = (window.localStorage.getItem("FillTenderTypes"));
jsonObjTenderT = [];
if (Tenders_ != "" && Tenders_ != null) {
    var TenderT_ = Tenders_.split('$');
    for (var i = 0; i < TenderT_.length; i++) {
        var TT = TenderT_[i].split(':');
        item = {}
        item["data"] = TT[1];
        item["value"] = TT[0];
        item["CurId"] = TT[2];
        item["DefTender"] = TT[3];
        jsonObjTenderT.push(item);
    }
}

$('#hdnCounterNo').val();

//Filters
$('#btnAll').click(function () {
    $("#btnCancel").click();
    GetSummaryGridData('Summary');
});
$('#btnToday').click(function () {
    $("#btnCancel").click();
    FilterSummaryGrid('Today', 'CREATEDDATE')
});
$('#btnYesterday').click(function () {
    $("#btnCancel").click();
    FilterSummaryGrid('Yesterday', 'CREATEDDATE')
});
$('#btnLast7days').click(function () {
    $("#btnCancel").click();
    FilterSummaryGrid('Last7', 'CREATEDDATE')

});
$('#btnCurrentMonth').click(function () {
    $("#btnCancel").click();
    FilterSummaryGrid('CMonth', 'CREATEDDATE')
});

$('#btnEdit').click(function () {
    window.localStorage.setItem('ctCheck', 1);
    Edit();
});
//$('#btnDelete').click(function () {
//    window.localStorage.setItem('ctCheck', 1);
//    Delete();
//});

$('#txtFindGo').keyup(function () {
    window.localStorage.setItem('ctCheck', 1);
    result = [];
    result = JSON.parse((window.localStorage.getItem("CounterOpenandCloseSummaryGrid")));
    if ($('#txtFindGo').val().trim() == "" || result.length <= 0) {

        GetSummaryGridData('Summary');
        return false;
    }
    else {

        GetSummaryGridData('FindGo');

    }
})
