﻿var CashierLite = CashierLite || {};
CashierLite.Settings = CashierLite.Settings || {};

CashierLite.Settings.sessionIdKey = "CashierPOS-session";
CashierLite.Settings.sessionTimeoutInMSec = 86400000 * 30;   // 30 days. 
var vd_ = window.localStorage.getItem("UUID");
var virtualDC = "";
//virtualDC = "http://172.16.209.121/CashierLiteService";
//virtualDC = "http://172.16.209.142/cashierLite-R1-QC-TestService";
//virtualDC = "https://www.readysalespos.com/CashierLiteService";
//virtualDC = "https://me.mbascloud.com/CashierLiteAPKService";
//virtualDC = "https://me.mbascloud.com/MCashierService/";
//virtualDC = "https://me.mbascloud.com/MCashierServiceV1.0.0";
//virtualDC = "http://95.177.174.153/CashierLiteService/";
//virtualDC = "http://185.230.210.36/Mcashierservice/";
//virtualDC = "https://www.smartsystechdb.com/McashierServiceV1.0.1";
//virtualDC = "https://www.readysalespos.com/MCashierServiceV1.0.1";
//virtualDC = "https://www.readysalespos.com/MCashierServiceV1.0.2"; 
//virtualDC = "https://me.mbascloud.com/MCashierServiceV1.0.0";
//virtualDC = "https://me.mbascloud.com/MCashier.Service";
//virtualDC = "https://www.readysalespos.com/neoecrdemoservice";
//virtualDC = "https://neoecruat.neoleap.com.sa/neoecrServiceV1.0.0";
//virtualDC = "https://www.readysalespos.com/LeapTestQR";
//virtualDC = "https://neoecr1.mbascloud.com/Serviceone";

//UAT SERVER
  virtualDC="https://neomcuat.neoleap.com.sa/neoecrmicroserviceV100";
//PROD SERVER
//virtualDC="https://neomc.neoecr.com.sa/neoecrmicroserviceV100";
//virtualDC="https://www.readysalespos.com/neoecrservicev100";
if (virtualDC.length > 0) {
    CashierLite.Settings.apkVersion = "V1.0.0";
    CashierLite.Settings.GetOutStandingAmount = virtualDC + "/POS_CashierLiteService.svc/GetOutStandingAmount";
    CashierLite.Settings.SendMailArabic = virtualDC + "/POS_CashierLiteService.svc/SendMailArabic";
    CashierLite.Settings.SendMail = virtualDC + "/POS_CashierLiteService.svc/SendMail";
    CashierLite.Settings.DownloadFile = virtualDC + "/POS_CashierLiteService.svc/DownloadFile";
    CashierLite.Settings.GRNDelete = virtualDC + "/POS_CashierLiteService.svc/GRNDelete";
    CashierLite.Settings.fieDownloadPath = virtualDC + "/Upload/SampleItemFile.xlsx";
    CashierLite.Settings.ReceiptfieDownloadPath = virtualDC + "/Upload/SampleReceiptFile.xlsx";
    CashierLite.Settings.GetUserDataById = virtualDC + "/POS_CashierLiteService.svc/GetUserDataById";
    CashierLite.Settings.createUserDirectData = virtualDC + "/POS_CashierLiteService.svc/createUserDirectData";
    CashierLite.Settings.updateUserDataById = virtualDC + "/POS_CashierLiteService.svc/updateUserDataById";
    CashierLite.Settings.createUserDirect = virtualDC + "/POS_CashierLiteService.svc/createUserDirect";
    CashierLite.Settings.OrgCheck = virtualDC + "/POS_CashierLiteService.svc/OrgCheck";
    CashierLite.Settings.login = virtualDC + "/POS_CashierLiteService.svc/Login";
    CashierLite.Settings.GetINVItems = virtualDC + "/POS_CashierLiteService.svc/GetINVItems";
    CashierLite.Settings.GetCustomers = virtualDC + "/POS_CashierLiteService.svc/GetCustomers";
    CashierLite.Settings.InvoiceSave = virtualDC + "/POS_CashierLiteService.svc/InvoiceSave";
    CashierLite.Settings.LoginSetTheam = virtualDC + "/POS_CashierLiteService.svc/LoginSetTheam";
    CashierLite.Settings.LogingetTheam = virtualDC + "/POS_CashierLiteService.svc/LogingetTheam";
    CashierLite.Settings.HashValues = virtualDC + "/POS_CashierLiteService.svc/HashValues";
    CashierLite.Settings.ShiftCheck = virtualDC + "/POS_CashierLiteService.svc/ShiftCheck";
    CashierLite.Settings.ClosedShiftCheck = virtualDC + "/POS_CashierLiteService.svc/ClosedShiftCheck";
    CashierLite.Settings.GetCounterTypeTenderData = virtualDC + "/POS_CashierLiteService.svc/GetCounterTypeTenderData";
    CashierLite.Settings.GetTypeTenderDataCashby = virtualDC + "/POS_CashierLiteService.svc/GetTypeTenderDataCashby";
    //Till Close
    CashierLite.Settings.GetTillClosePending = virtualDC + "/POS_CashierLiteService.svc/GetTillClosePending";
    CashierLite.Settings.getTillCloseDataByBalance = virtualDC + "/POS_CashierLiteService.svc/getTillCloseDataByBalance";
    //Customer Master
    CashierLite.Settings.GetCustomerTypes = virtualDC + "/POS_CashierLiteService.svc/GetCustomerTypes";
    CashierLite.Settings.GetCustomerGroups = virtualDC + "/POS_CashierLiteService.svc/GetCustomerGroups";
    CashierLite.Settings.GetCityData = virtualDC + "/POS_CashierLiteService.svc/GetCity";
    CashierLite.Settings.SaveCustomerData = virtualDC + "/POS_CashierLiteService.svc/SaveCustomerData";
    CashierLite.Settings.GetSubOrgData = virtualDC + "/POS_CashierLiteService.svc/GetSubOrgData";
    CashierLite.Settings.GetSalesRegion = virtualDC + "/POS_CashierLiteService.svc/GetSalesRegion";
    CashierLite.Settings.GetParentCustomers = virtualDC + "/POS_CashierLiteService.svc/GetParentCustomers";
    CashierLite.Settings.GatCurrencyData = virtualDC + "/POS_CashierLiteService.svc/GatCurrencyData";
    CashierLite.Settings.GetCustomerSummaryGrid = virtualDC + "/POS_CashierLiteService.svc/GetCustomerSummaryGrid";
    CashierLite.Settings.GetCustomerMstDoubleClick = virtualDC + "/POS_CashierLiteService.svc/GetCustomerMstDoubleClick";
    //Counter Opening
    CashierLite.Settings.GetCounterNames = virtualDC + "/POS_CashierLiteService.svc/GetCounterNames";
    CashierLite.Settings.GetCashiers = virtualDC + "/POS_CashierLiteService.svc/GetCashiers";
    CashierLite.Settings.GetShifts = virtualDC + "/POS_CashierLiteService.svc/GetShifts";
    CashierLite.Settings.GetSubOrgCurrency = virtualDC + "/POS_CashierLiteService.svc/GetSubOrgCurrency";
    CashierLite.Settings.GetTenderTypesByOrgId = virtualDC + "/POS_CashierLiteService.svc/GetTenderTypesByOrgId";
    CashierLite.Settings.GetDenominationData = virtualDC + "/POS_CashierLiteService.svc/GetDenominationData";
    CashierLite.Settings.GetDefaultCurrency = virtualDC + "/POS_CashierLiteService.svc/GetDefaultCurrency";
    CashierLite.Settings.GetForexRate = virtualDC + "/POS_CashierLiteService.svc/GetForexRate";
    CashierLite.Settings.CounterOpeningSummaryGrid = virtualDC + "/POS_CashierLiteService.svc/CounterOpeningSummaryGrid";
    CashierLite.Settings.SaveCounterOpeningData = virtualDC + "/POS_CashierLiteService.svc/SaveCounterOpeningData";
    CashierLite.Settings.SaveCounterClosingData = virtualDC + "/POS_CashierLiteService.svc/SaveCounterClosingData";
    CashierLite.Settings.GetCounterOpeningDoubleClick = virtualDC + "/POS_CashierLiteService.svc/GetCounterOpeningDoubleClick";
    //Direct Print
    //CashierLite.Settings.DirectPrint = "http://localhost:33844/POS_CashierLiteService.svc/DirectPrint";
    CashierLite.Settings.DirectPrint = "http://172.16.209.25:81/POS_CashierLiteService.svc/DirectPrint";
    CashierLite.Settings.POLE = "http://172.16.209.25:81/POS_CashierLiteService.svc/POLE";
    //Sales Return
    CashierLite.Settings.GetSalesReturnSummaryGrid = virtualDC + "/POS_CashierLiteService.svc/GetSalesReturnSummaryGrid";
    CashierLite.Settings.GetSalesReturnInvoices = virtualDC + "/POS_CashierLiteService.svc/GetSalesReturnInvoices";
    CashierLite.Settings.GetSalesReturnInvoiceItems = virtualDC + "/POS_CashierLiteService.svc/GetSalesReturnInvoiceItems";
    CashierLite.Settings.GetSalesReturnReasons = virtualDC + "/POS_CashierLiteService.svc/GetSalesReturnReasons";
    CashierLite.Settings.SaveSalesReturnData = virtualDC + "/POS_CashierLiteService.svc/SaveSalesReturnData";
    CashierLite.Settings.GetSalesReturnDoubleClick = virtualDC + "/POS_CashierLiteService.svc/GetSalesReturnDoubleClick";
    CashierLite.Settings.GetCounterTypes = virtualDC + "/POS_CashierLiteService.svc/getCounterTypes";
    CashierLite.Settings.CounterSave = virtualDC + "/POS_CashierLiteService.svc/CounterSave";
    CashierLite.Settings.CheckCounterName = virtualDC + "/POS_CashierLiteService.svc/CheckCounterName";
    CashierLite.Settings.CounterTypeSave = virtualDC + "/POS_CashierLiteService.svc/CounterTypeSave";
    CashierLite.Settings.getItemDataAutocomplte = virtualDC + "/POS_CashierLiteService.svc/getItemDataAutocomplte";
    CashierLite.Settings.getItemDataFromBarcode = virtualDC + "/POS_CashierLiteService.svc/getItemDataFromBarcode";
    CashierLite.Settings.GetAllCounterTypes = virtualDC + "/POS_CashierLiteService.svc/GetAllCounterTypes";
    CashierLite.Settings.GetAllCounters = virtualDC + "/POS_CashierLiteService.svc/GetAllCounters";
    CashierLite.Settings.GetCounterById = virtualDC + "/POS_CashierLiteService.svc/GetCounterById";
    CashierLite.Settings.GetCounterTypesById = virtualDC + "/POS_CashierLiteService.svc/GetCounterTypesById";
    CashierLite.Settings.DeleteCounterType = virtualDC + "/POS_CashierLiteService.svc/DeleteCounterType";
    CashierLite.Settings.DeleteCounter = virtualDC + "/POS_CashierLiteService.svc/DeleteCounter";
    CashierLite.Settings.GetCounterPermissions = virtualDC + "/POS_CashierLiteService.svc/GetCounterPermissions";
    CashierLite.Settings.GetTenderTypes = virtualDC + "/POS_CashierLiteService.svc/GetTenderTypes";
    CashierLite.Settings.SaveCounterPermissions = virtualDC + "/POS_CashierLiteService.svc/SaveCounterPermissions";
    CashierLite.Settings.GetCategoryList = virtualDC + "/POS_CashierLiteService.svc/GetCategoryList";
    CashierLite.Settings.SaveTenderType = virtualDC + "/POS_CashierLiteService.svc/SaveTenderType";
    CashierLite.Settings.GetCurrency = virtualDC + "/POS_CashierLiteService.svc/GetCurrency";
    CashierLite.Settings.TenderTypeSummaryGrid = virtualDC + "/POS_CashierLiteService.svc/TenderTypeSummaryGrid";
    CashierLite.Settings.GetTenderTypeDoubleClick = virtualDC + "/POS_CashierLiteService.svc/GetTenderTypeDoubleClick";
    CashierLite.Settings.DeleteTenderTypes = virtualDC + "/POS_CashierLiteService.svc/DeleteTenderTypes";
    CashierLite.Settings.SavePrintCustomize = virtualDC + "/POS_CashierLiteService.svc/SavePrintCustomize";
    CashierLite.Settings.GetPrintCustomizeStyles = virtualDC + "/POS_CashierLiteService.svc/GetPrintCustomizeStyles";
    CashierLite.Settings.SaveBarCode = virtualDC + "/POS_CashierLiteService.svc/SaveBarCode";
    CashierLite.Settings.GetDashboardCounts = virtualDC + "/POS_CashierLiteService.svc/GetDashboardCounts";
    CashierLite.Settings.GetSalesTodayRTDR = virtualDC + "/POS_CashierLiteService.svc/GetSalesTodayRTDR";
    CashierLite.Settings.GetSalesTodayRTDRInvoiceId = virtualDC + "/POS_CashierLiteService.svc/GetSalesTodayRTDRInvoiceId";
    CashierLite.Settings.GetAccountDetails = virtualDC + "/POS_CashierLiteService.svc/GetAccountDetails";
    CashierLite.Settings.GetCounterWisehhSalesGraph = virtualDC + "/POS_CashierLiteService.svc/GetCounterWisehhSalesGraph";
    CashierLite.Settings.SaveAccountPostingData = virtualDC + "/POS_CashierLiteService.svc/SaveAccountPostingData";
    CashierLite.Settings.GetDashBoardWeekSalesByCounter = virtualDC + "/POS_CashierLiteService.svc/GetDashBoardWeekSalesByCounter";
    CashierLite.Settings.GetDashBoardMonthSalesByCounter = virtualDC + "/POS_CashierLiteService.svc/GetDashBoardMonthSalesByCounter";
    CashierLite.Settings.GetDashBoardYearSalesByCounter = virtualDC + "/POS_CashierLiteService.svc/GetDashBoardYearSalesByCounter";
    CashierLite.Settings.POSGetTodaySalesDetailsRP = virtualDC + "/POS_CashierLiteService.svc/POSGetTodaySalesDetailsRP";
    CashierLite.Settings.GetAccountPostingSummaryGrid = virtualDC + "/POS_CashierLiteService.svc/GetAccountPostingSummaryGrid";
    CashierLite.Settings.GetReport = virtualDC + "/POS_CashierLiteService.svc/GetReport";
    CashierLite.Settings.ApproveAccountPosting = virtualDC + "/POS_CashierLiteService.svc/ApproveAccountPosting";
    CashierLite.Settings.GetFromDate = virtualDC + "/POS_CashierLiteService.svc/GetFromDate";
    CashierLite.Settings.GetSubDataTillInvoice = virtualDC + "/POS_CashierLiteService.svc/GetSubDataTillInvoice";
    CashierLite.Settings.GetDailyCollectionReport = virtualDC + "/POS_CashierLiteService.svc/GetDailyCollectionReport";
    CashierLite.Settings.PageValidatebyPage = virtualDC + "/POS_CashierLiteService.svc/PageValidatebyPage";
    CashierLite.Settings.GetCounterPermissionsbyId = virtualDC + "/POS_CashierLiteService.svc/GetCounterPermissionsbyId";
    CashierLite.Settings.GetAutoGenHashTableCheck = virtualDC + "/POS_CashierLiteService.svc/GetAutoGenHashTableCheck";
    CashierLite.Settings.GenerateCode = virtualDC + "/POS_CashierLiteService.svc/GenerateCode";
    CashierLite.Settings.CounterCheck = virtualDC + "/POS_CashierLiteService.svc/CounterCheck";
    CashierLite.Settings.CheckPageCodeExists = virtualDC + "/POS_CashierLiteService.svc/CheckPageCodeExists";
    CashierLite.Settings.GetFeatureCount = virtualDC + "/POS_CashierLiteService.svc/GetFeatureCount";
    CashierLite.Settings.SaveConfigData = virtualDC + "/POS_CashierLiteService.svc/SaveConfigData";
    CashierLite.Settings.GetConfigData = virtualDC + "/POS_CashierLiteService.svc/GetConfigData";
    CashierLite.Settings.ChangePassword = virtualDC + "/POS_CashierLiteService.svc/ChangePassword";
    CashierLite.Settings.ForgetPassword = virtualDC + "/POS_CashierLiteService.svc/GetForgetPassword";
    CashierLite.Settings.GetDashBoardTop5SaleItems = virtualDC + "/POS_CashierLiteService.svc/GetDashBoardTop5SaleItems";
    CashierLite.Settings.GetDashBoardTenderWiseSales = virtualDC + "/POS_CashierLiteService.svc/GetDashBoardTenderWiseSales";
    CashierLite.Settings.GetSp_POSMargin = virtualDC + "/POS_CashierLiteService.svc/GetSp_POSMargin";
    CashierLite.Settings.getResource = virtualDC + "/POS_CashierLiteService.svc/getResource";
    CashierLite.Settings.VerifyPasswordAUTCheck = virtualDC + "/POS_CashierLiteService.svc/VerifyPasswordAUTCheck";
    CashierLite.Settings.GetDafaultLanguage = virtualDC + "/POS_CashierLiteService.svc/GetDafaultLanguage";
    CashierLite.Settings.updateUserSettings = virtualDC + "/POS_CashierLiteService.svc/updateUserSettings";
    CashierLite.Settings.updateUserSettingsDisplay = virtualDC + "/POS_CashierLiteService.svc/updateUserSettingsDisplay";
    CashierLite.Settings.CreditNoteData = virtualDC + "/POS_CashierLiteService.svc/CreditNoteData";
    CashierLite.Settings.GetDeliveryStatusDetails = virtualDC + "/POS_CashierLiteService.svc/GetDeliveryStatusDetails";
    CashierLite.Settings.GetInvoiceDeliveryStatusDetails = virtualDC + "/POS_CashierLiteService.svc/GetInvoiceDeliveryStatusDetails";
    CashierLite.Settings.verifyContactNoExist = virtualDC + "/POS_CashierLiteService.svc/VerifyContactNoExist";
    CashierLite.Settings.DeliveryStatusSave = virtualDC + "/POS_CashierLiteService.svc/DeliveryStatusSave";
    CashierLite.Settings.GetRewardPoints = virtualDC + "/POS_CashierLiteService.svc/GetRewardPoints";
    CashierLite.Settings.GetInvoiceDeliveryStatusDetailsByID = virtualDC + "/POS_CashierLiteService.svc/GetInvoiceDeliveryStatusDetailsByID";
    //Loyalty Points Links Start
    CashierLite.Settings.LoyaltyPointsSave = virtualDC + "/POS_CashierLiteService.svc/LoyaltyPointsSave";
    CashierLite.Settings.GetAllLoyaltyPoints = virtualDC + "/POS_CashierLiteService.svc/GetAllLoyaltyPoints";
    CashierLite.Settings.GetLoyaltyPointsById = virtualDC + "/POS_CashierLiteService.svc/GetLoyaltyPointsById";
    CashierLite.Settings.DeleteLoyaltyPoints = virtualDC + "/POS_CashierLiteService.svc/DeleteLoyaltyPoints";
    CashierLite.Settings.GetLoyaltyPointsByIdCheck = virtualDC + "/POS_CashierLiteService.svc/GetLoyaltyPointsByIdCheck";
    //end
    CashierLite.Settings.SaveMembershipData = virtualDC + "/POS_CashierLiteService.svc/SaveMembershipData";
    CashierLite.Settings.GetMembershipManagementData = virtualDC + "/POS_CashierLiteService.svc/GetMembershipManagementData";

    //CheckInterNet
    CashierLite.Settings.CheckConnectionExist = virtualDC + "/";
    CashierLite.Settings.OfLineInvoiceSave = virtualDC + "/POS_CashierLiteService.svc/OfLineInvoiceSave";
    //Global Lagnuage Binding
    //Multy Binding
    CashierLite.Settings.GetItemSchema = virtualDC + "/POS_CashierLiteService.svc/GetItemSchema";
    CashierLite.Settings.GetBucketAnalysis = virtualDC + "/POS_CashierLiteService.svc/GetBucketAnalysis";
    CashierLite.Settings.GetWeekDaysRevenue = virtualDC + "/POS_CashierLiteService.svc/GetWeekDaysRevenue";
    CashierLite.Settings.GetLocationwiseAvgTransactionValue = virtualDC + "/POS_CashierLiteService.svc/GetLocationwiseAvgTransactionValue";
    CashierLite.Settings.GetItemCategorySalesAnalysis = virtualDC + "/POS_CashierLiteService.svc/GetItemCategorySalesAnalysis";
    CashierLite.Settings.GetItemCategoryProfitAnalysis = virtualDC + "/POS_CashierLiteService.svc/GetItemCategoryProfitAnalysis";
    CashierLite.Settings.GetLocationWiseProfitAnalysis = virtualDC + "/POS_CashierLiteService.svc/GetLocationWiseProfitAnalysis";
    CashierLite.Settings.printPoleUrlsSave = virtualDC + "/POS_CashierLiteService.svc/printPoleUrlsSave";
    CashierLite.Settings.getPrintPoleConfig = virtualDC + "/POS_CashierLiteService.svc/getPrintPoleConfigDetails";
    CashierLite.Settings.getPrintPoleConfigDetailsById = virtualDC + "/POS_CashierLiteService.svc/getPrintPoleConfigDetailsById";
    CashierLite.Settings.deletePrintPoleConfig = virtualDC + "/POS_CashierLiteService.svc/deletePrintPoleConfig";
    CashierLite.Settings.DeleteAccountPosting = virtualDC + "/POS_CashierLiteService.svc/DeleteAccountPosting";
    CashierLite.Settings.GetDayCloserChecking = virtualDC + "/POS_CashierLiteService.svc/GetDayCloserChecking";
    CashierLite.Settings.GetAllUserPermissionData = virtualDC + "/POS_CashierLiteService.svc/GetAllUserPermissionData";
    CashierLite.Settings.GetUOM = virtualDC + "/POS_CashierLiteService.svc/GetUOM";
    CashierLite.Settings.ItemSave = virtualDC + "/POS_CashierLiteService.svc/ItemSave";
    CashierLite.Settings.GetAllItems = virtualDC + "/POS_CashierLiteService.svc/GetAllItems";
    CashierLite.Settings.GetAllItemsWithPaging = virtualDC + "/POS_CashierLiteService.svc/GetAllItemsWithPaging";
    CashierLite.Settings.GetINVSacanItem = virtualDC + "/POS_CashierLiteService.svc/GetINVSacanItem";
    CashierLite.Settings.GetINVSearchItem = virtualDC + "/POS_CashierLiteService.svc/GetINVSearchItem";
    CashierLite.Settings.GRNSave = virtualDC + "/POS_CashierLiteService.svc/GRNSave";
    CashierLite.Settings.GRNSave = virtualDC + "/POS_CashierLiteService.svc/GRNSave";
    CashierLite.Settings.GETGRNData = virtualDC + "/POS_CashierLiteService.svc/GETGRNData";
    CashierLite.Settings.GETGRNSummaryGridData = virtualDC + "/POS_CashierLiteService.svc/GETGRNSummaryGridData"
    CashierLite.Settings.ItemSaveUpload = virtualDC + "/POS_CashierLiteService.svc/ItemSaveUpload";
    CashierLite.Settings.ItemSaveExcel = virtualDC + "/POS_CashierLiteService.svc/ItemSaveExcel";
    CashierLite.Settings.GetTaxData = virtualDC + "/POS_CashierLiteService.svc/GetTaxData";
    CashierLite.Settings.getItemDataByFilter = virtualDC + "/POS_CashierLiteService.svc/getItemDataByFilter";
    CashierLite.Settings.GetVDReg = virtualDC + "/POS_CashierLiteService.svc/GetVDReg";
    CashierLite.Settings.GetCurrencyCommon = virtualDC + "/POS_CashierLiteService.svc/GetCurrencyCommon";
    CashierLite.Settings.GetSubScriptionPackes = virtualDC + "/POS_CashierLiteService.svc/GetSubScriptionPackes";
    CashierLite.Settings.GetSubScriptionPackesByID = virtualDC + "/POS_CashierLiteService.svc/GetSubScriptionPackesByID";
    CashierLite.Settings.Subscribe = virtualDC + "/POS_CashierLiteService.svc/Subscribe";
    CashierLite.Settings.SubscribeEmailCheck = virtualDC + "/POS_CashierLiteService.svc/SubscribeEmailCheck";
    CashierLite.Settings.GetUUID = virtualDC + "/POS_CashierLiteService.svc/GetUUID";
    CashierLite.Settings.GetConfigWalletData = virtualDC + "/POS_CashierLiteService.svc/GetConfigWalletData";
    CashierLite.Settings.UserImageSave = virtualDC + "/POS_CashierLiteService.svc/GetUserImageSave";
    CashierLite.Settings.getItemAliasByInvoiceId = virtualDC + "/POS_CashierLiteService.svc/getItemAliasByInvoiceId";
    CashierLite.Settings.DisplayUserImage = virtualDC + "/POS_CashierLiteService.svc/DisplayUserImage";
    CashierLite.Settings.GetSubscriptionHistory = virtualDC + "/POS_CashierLiteService.svc/GetSubscriptionHistory";
    CashierLite.Settings.printUSB = virtualDC + "/POS_CashierLiteService.svc/printUSB";
    //CashierLite.Settings.printUSB = virtualDC + "/POS_CashierLiteService.svc/printUSB";
    CashierLite.Settings.printUSBSmall = virtualDC + "/POS_CashierLiteService.svc/printUSBSmall";
    CashierLite.Settings.printUSBSalesReturn = virtualDC + "/POS_CashierLiteService.svc/printUSBSalesReturn";
    CashierLite.Settings.GetStockCount = virtualDC + "/POS_CashierLiteService.svc/GetStockCount";
    CashierLite.Settings.GetReport_V01 = virtualDC + "/POS_CashierLiteService.svc/GetReport_V01";
    CashierLite.Settings.GetReport_V01sub = virtualDC + "/POS_CashierLiteService.svc/GetReport_V01sub";
    CashierLite.Settings.GetSTCDetails = virtualDC + "/POS_CashierLiteService.svc/GetSTCDetails";
    CashierLite.Settings.UpdatePayemntStatusById = virtualDC + "/POS_CashierLiteService.svc/UpdatePayemntStatusById";
    CashierLite.Settings.updateDualDisplay = virtualDC + "/POS_CashierLiteService.svc/updateDualDisplay";
    CashierLite.Settings.ConfigurationDataImages = virtualDC + "/POS_CashierLiteService.svc/ConfigurationDataImages";
    CashierLite.Settings.GetINVItemsOffline = virtualDC + "/POS_CashierLiteService.svc/GetINVItemsOffline";
    CashierLite.Settings.printUSBOffline = virtualDC + "/POS_CashierLiteService.svc/printUSBOffline";
    CashierLite.Settings.getPrintCusData = virtualDC + "/POS_CashierLiteService.svc/getPrintCusData";
    CashierLite.Settings.GetApkVersion = virtualDC + "/POS_CashierLiteService.svc/GetApkVersion";
    CashierLite.Settings.CheckApkVersion = virtualDC + "/POS_CashierLiteService.svc/CheckApkVersion";
    CashierLite.Settings.SubscribeTillUser = virtualDC + "/POS_CashierLiteService.svc/SubscribeTillUser";
    CashierLite.Settings.GetInvoices = virtualDC + "/POS_CashierLiteService.svc/GetInvoices";
    CashierLite.Settings.VerifyPrimaryPasswordCheck = virtualDC + "/POS_CashierLiteService.svc/VerifyPrimaryPasswordCheck";
    CashierLite.Settings.ChkTillsCount = virtualDC + "/POS_CashierLiteService.svc/ChkTillsCount";
    CashierLite.Settings.UpdateInvoiceByVoid = virtualDC + "/POS_CashierLiteService.svc/UpdateInvoiceByVoid";
	CashierLite.Settings.GetAllCustomersWithPaging = virtualDC + "/POS_CashierLiteService.svc/GetAllCustomersWithPaging";
    window.localStorage.setItem("Display", "3");
    function loadOfflineDataServer(UUID) {
        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.printUSBOffline + "?VD=" + UUID + "",
            data: "",
            success: function (resp) {

                var dara_ = (JSON.stringify(resp));
                localStorage.setItem("PRINTOFFDD", dara_);
            },
            error: function (resp) {

            }
        });
    }
    synchCheck();
    var invoicePassData_ = '';
    var invoicePassId_ = '';
    function loadFF() {
        var db = window.openDatabase("MyInvoice", '1.0', 'CashierLiteDB', 2 * 1024 * 1024);
        db.transaction(function (tx) {
            tx.executeSql('SELECT * from Invoice', [], function (txa, results) {
                var resp = results.rows;
               
                if (resp.length > 0) {
                    for (var i = 0; i < resp.length; i++) {

                      
                    }
                }
                else {

                }
            });
        });
    }
    var dataMainPass;
    function mainSQLDB_OLD() {
        try {

            var db = window.openDatabase("MyInvoice", '1.0', 'CashierLiteDB', 2 * 1024 * 1024);
            db.transaction(function (tx) {
                tx.executeSql('SELECT * from InvoiceDataTemp', [], function (txa, results) {
                    var resp = results.rows;
                  
                    if (resp.length > 0) {
                        for (var i = 0; i < resp.length; i++) {

                            if (window.localStorage.getItem(resp[i].InvoiceId) != '' || window.localStorage.getItem(resp[i].InvoiceId) != null || window.localStorage.getItem(resp[i].InvoiceId) != undefined
                                || window.localStorage.getItem(resp[i].InvoiceId) != 'undefined') {
                                invoicePassId_ = resp[i].InvoiceId;
                                var InvoiceParams = new Object();
                                InvoiceParams.P20 = JSON.stringify(window.localStorage.getItem(invoicePassId_ + "_P20"));
                                InvoiceParams.P21 = JSON.stringify(window.localStorage.getItem(invoicePassId_ + "_P21"));
                                InvoiceParams.P24 = JSON.stringify(window.localStorage.getItem(invoicePassId_ + "_P24"));
                                InvoiceParams.VD = window.localStorage.getItem("UUID");
                                //Put the customer object into a container
                                var ComplexObject = new Object();
                                //The property name "WebMethodArgName" must match the web method argument "WebMethodArgName"!
                                ComplexObject.Invoice = InvoiceParams;

                                //Stringify and verify the object is foramtted as expected
                                dataMainPass = JSON.stringify(ComplexObject);

                                $.ajax({
                                    type: 'POST',
                                    url: CashierLite.Settings.InvoiceSave,
                                    data: (dataMainPass),
                                    dataType: 'json',
                                    contentType: "application/json; charset=utf-8",
                                    success: function (resp) {
                                        deleteStore(invoicePassId_);
                                    },
                                    error: function (e) {
                                        alert('error' + JSON.stringify(e));
                                        selectNetwork();
                                        SpinnerPlugin.activityStop();
                                    }
                                });
                            }


                        }
                    }
                    else {

                    }
                });
            });
        }
        catch (a) {
        }
    }

    function GetDowloadFile() {
        var SubIOrgId = '';
        var HashValues = (window.localStorage.getItem("hashValues"));
        if (HashValues != null && HashValues != '') {
            var hashSplit = HashValues.split('~');
            SubIOrgId = hashSplit[1];
        }
        window.localStorage.setItem('ctCheck', 1);
        var options = { dimBackground: true };
        SpinnerPlugin.activityStart("Downloading Please wait ...", options); 
        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.DownloadFile,
            data: "PageId=" + window.localStorage.getItem('RPPAGECODE') + "&subOrg=" + SubIOrgId + "&frmDate=" + window.localStorage.getItem('RPfrmDate') + "&toDate=" + window.localStorage.getItem('RPtoDate') + ' 23:59:59' + "&DateFormate=" + $('#hdnDateFromate').val() + "&VD=" + window.localStorage.getItem("UUID") + "&language=" + hashSplit[7] + "",
            success: function (resp) {
                SpinnerPlugin.activityStop();
                downloadFile(resp);
            },
            error: function (e) {
                SpinnerPlugin.activityStop();
            }
        });

    }
    function downloadFile(filePath) {
        window.open(filePath, '_blank', 'location=no');
    }
    function mainSQLDB() {
        try {
            mm_ = '';
            var db = window.openDatabase("MyInvoice", '1.0', 'CashierLiteDB', 2 * 1024 * 1024);
            db.transaction(function (tx) {
                tx.executeSql('SELECT * from InvoiceDataTemp', [], function (txa, results) {
                    var resp = results.rows;
                    var options = { dimBackground: true };
                    if (resp.length > 0) {
                        SpinnerPlugin.activityStart("Please wait while synchronizing offline sales data to cloud ...", options);
                        for (var i = 0; i < resp.length; i++) {
                            if (window.localStorage.getItem(resp[i].InvoiceId) != '' || window.localStorage.getItem(resp[i].InvoiceId) != null || window.localStorage.getItem(resp[i].InvoiceId) != undefined
                                || window.localStorage.getItem(resp[i].InvoiceId) != 'undefined') {

                                invoicePassId_ = resp[i].InvoiceId;
                                //test
                                var InvoiceParams = new Object();
                                InvoiceParams.P20 = (window.localStorage.getItem(invoicePassId_ + "_P20"));
                                InvoiceParams.P21 = (window.localStorage.getItem(invoicePassId_ + "_P21"));
                                InvoiceParams.P24 = (window.localStorage.getItem(invoicePassId_ + "_P24"));
                                InvoiceParams.VD = window.localStorage.getItem("UUID");
                                //Put the customer object into a container
                                var ComplexObject = new Object();
                                //The property name "WebMethodArgName" must match the web method argument "WebMethodArgName"!
                                ComplexObject.Invoice = InvoiceParams;
                                //Stringify and verify the object is foramtted as expected
                                dataMainPass = JSON.stringify(ComplexObject);
                                $.ajax({
                                    type: 'POST',
                                    url: CashierLite.Settings.InvoiceSave,
                                    data: (dataMainPass),
                                    async: false,
                                    dataType: 'json',
                                    contentType: "application/json; charset=utf-8",
                                    success: function (resp) {
                                        mm_ = '';
                                        //SpinnerPlugin.activityStop();
                                        deleteStore(invoicePassId_);
                                    },
                                    error: function (e) {
                                        mm_ = '';
                                        SpinnerPlugin.activityStop();
                                        //alert('error' + JSON.stringify(e));
                                    }

                                });

                            }
                        }
                    }
                    else {
                        mm_ = '';
                        SpinnerPlugin.activityStop();
                    }
                });
            });
        }
        catch (a) {
            mm_ = '';
            SpinnerPlugin.activityStop();
        }
    }
    function saveOfflineInvoice() {

    }
    window.localStorage.setItem('ctCheck', 1);

    function deleteStore(invoiceId) {
        var db = window.openDatabase("MyInvoice", '1.0', 'CashierLiteDB', 2 * 1024 * 1024);
        db.transaction(function (tx) {
            tx.executeSql('delete from  InvoiceDataTemp where InvoiceId="' + invoiceId + '"', [], function (txa, results) {
                window.localStorage.removeItem(invoiceId + "_P20");
                window.localStorage.removeItem(invoiceId + "_P21");
                window.localStorage.removeItem(invoiceId + "_P24");

                mm_ = '';

            });

        });
    }


    function deleteStoreOLD(invoiceId) {
        var db = window.openDatabase("MyInvoice", '1.0', 'CashierLiteDB', 2 * 1024 * 1024);
        db.transaction(function (tx) {
            tx.executeSql('delete from  InvoiceDataTemp where InvoiceId="' + invoiceId + '"', [], function (txa, results) {
                window.localStorage.removeItem(invoiceId + "_P20");
                window.localStorage.removeItem(invoiceId + "_P21");
                window.localStorage.removeItem(invoiceId + "_P24");

            });
        });
    }
    function selectNetwork() {
        if (window.localStorage.getItem('ctCheck') != null) {
            var U_ = parseFloat(window.localStorage.getItem('ctCheck')) + 1;
            window.localStorage.setItem('ctCheck', U_);

            if (parseFloat(window.localStorage.getItem('ctCheck')) == 900) {
                window.localStorage.setItem('ctCheck', 1);
                location.reload();
            }
        }
        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.GetUUID,
            data: "UUID=" + window.localStorage.getItem("UUID") + "",
            success: function (resp) {
                var lblNetwork = document.getElementById("lblNetwork");
                lblNetwork.style.display = "none";
                window.localStorage.setItem('network', '1');
                mainSQLDB();
            },
            error: function (resp) {
                mm_ = '';
                var lblNetwork = document.getElementById("lblNetwork");
                lblNetwork.style.display = "block";
                window.localStorage.setItem('network', '0');
                window.localStorage.setItem("showDisplay", '1');
                window.localStorage.setItem("Advertisement", '0');

            }
        });

    }
    var mm_ = '';
    function synchCheck() {
        if (mm_ == '') {
            mm_ = '0';
            selectNetwork();

        }
        setTimeout('synchCheck()', 1000);

    }
    try {
        document.getElementById("userinp").addEventListener("change", readFile);
    }
    catch (a)
    { }
    try {

        DisplayImage();
    }
    catch (a) {

    }
    try {
        function OfflinePrintLoad() {
            var SubOrg_Id;
            var HashValues = (window.localStorage.getItem("hashValues"));
            if (HashValues != null && HashValues != '') {
                var hashSplit = HashValues.split('~');
                SubOrg_Id = hashSplit[1];
            }
            var param = {
                SubOrgId: SubOrg_Id, VD: VD1()
            };
            $.ajax({
                url: CashierLite.Settings.GetPrintCustomizeStyles,
                data: param,
                type: "GET",
                success: function (data) {
                    try {

                        window.localStorage.setItem('OfflinePrintLoad', JSON.stringify(data));
                    }
                    catch (a) {

                    }

                },

            });
        }

    }
    catch (a) {
    }
    try {

        function GetShiftDataRTDRCHKOffline() {

            if (window.localStorage.getItem("CashierID") != null) {
                var SubOrgId = ''; var CashierID = window.localStorage.getItem("CashierID");
                var HashValues = (window.localStorage.getItem("hashValues"));
                if (HashValues != null && HashValues != '') {
                    var hashSplit = HashValues.split('~');
                    SubOrgId = hashSplit[1];
                }
                if (window.localStorage.getItem('network') == '1') {


                    $.ajax({
                        type: 'GET',
                        url: CashierLite.Settings.ShiftCheck,
                        data: "CashierID=" + CashierID + "&OrgId=" + SubOrgId + "&VD=" + VD1(),
                        success: function (resp) {

                            var result = resp;
                            if (result.length > 0) {
                                var ACTIVE = result[0].ACTIVE;
                                var COUNTERID = result[0].COUNTERID;
                                var COUNTERNAME = result[0].COUNTERNAME;
                                var COUNTERNUMBER = result[0].COUNTERNUMBER;
                                var COUNTERTYPE = result[0].COUNTERTYPE;
                                var End_Time = result[0].End_Time;
                                var Is_Next_Day = result[0].Is_Next_Day;
                                var SHIFTID = result[0].SHIFTID;
                                var Shift_Code = result[0].Shift_Code;
                                var Shift_Name = result[0].Shift_Name;
                                var Start_Time = result[0].Start_Time;
                                var StageAlert = result[0].StageAlert;
                                window.localStorage.setItem('ACTIVE', result[0].ACTIVE);
                                window.localStorage.setItem('COUNTERID', result[0].COUNTERID);
                                window.localStorage.setItem('COUNTERNAME', result[0].COUNTERNAME);
                                window.localStorage.setItem('COUNTERNUMBER', result[0].COUNTERNUMBER);
                                window.localStorage.setItem('COUNTERTYPE', result[0].COUNTERTYPE);
                                window.localStorage.setItem('End_Time', result[0].End_Time);
                                window.localStorage.setItem('Is_Next_Day', result[0].Is_Next_Day);
                                window.localStorage.setItem('SHIFTID', result[0].SHIFTID);
                                window.localStorage.setItem('Shift_Code', result[0].Shift_Code);
                                window.localStorage.setItem('Shift_Name', result[0].Shift_Name);
                                window.localStorage.setItem('Start_Time', result[0].Start_Time);
                                var StageAlert = result[0].StageAlert;
                            }
                            else {

                            }

                        },
                        error: function (e) {

                        }

                    });
                }
            }
            else {
            }
        }

    }

    catch (a) {

    }


    try {
        function GetTypeTenderDataCashbyOffline() {
            var SubOrgId = '';
            var HashValues = (window.localStorage.getItem("hashValues"));
            if (HashValues != null && HashValues != '') {
                var hashSplit = HashValues.split('~');
                SubOrgId = hashSplit[1];
            }

            if (window.localStorage.getItem('network') == '1') {

                var options = { dimBackground: true };
                SpinnerPlugin.activityStart("Please wait...", options);
                $.ajax({

                    type: 'GET',

                    url: CashierLite.Settings.GetTypeTenderDataCashby,

                    data: "CounterType=5000&OrgId=" + SubOrgId + "&VD=" + window.localStorage.getItem("UUID"),

                    success: function (resp) {
                        SpinnerPlugin.activityStop();
                        var ItemsData = resp;

                        var li_ = "";

                        if (ItemsData.length > 0) {

                            var MainData = '';

                            li_ = li_ + "<li style='color: #fff;background-color: #ffb9bb;border-color: #ffb9bb;' onClick=GetCounterTypeTenderLocalDataBackClicktime();><i class='fa fa-angle-up'></i> Back</li>";

                            for (var i = 0; i < ItemsData.length; i++) {

                                var id_ = 'li_' + ItemsData[i].TENDERNAME;

                                li_ = li_ + "<li id=" + id_ + "  onclick=ConverTenderAmount(\'" + ItemsData[i].TENDERNAME.replace(' ', '') + "\',\'" + ItemsData[i].ExchangeRate + "\',\'" + ItemsData[i].BankID + "\',\'" + ItemsData[i].CurrencyID + "\') style='color: #fff;background-color: #2e6da4;border-color: #2e6da4;'>" + ItemsData[i].TENDERNAME + "</li>";

                            }

                            window.localStorage.setItem('OfflineBaseCurrencyCode', ItemsData[0].baseCurrency);

                            window.localStorage.setItem('OfflineliTenderType', li_);



                        }



                    },

                    error: function (e) {

                    }

                });
            }

        }

    } catch (a) {
    }


    try {
        function GetCounterTypeTenderDataOffline() {
            var SubOrgId = '';
            var HashValues = (window.localStorage.getItem("hashValues"));
            if (HashValues != null && HashValues != '') {
                var hashSplit = HashValues.split('~');
                SubOrgId = hashSplit[1];

            }
            if (window.localStorage.getItem('network') == '1') {
                var options = { dimBackground: true };
                SpinnerPlugin.activityStart("Please wait...", options);
                $.ajax({
                    type: 'GET',
                    url: CashierLite.Settings.GetCounterTypeTenderData,
                    data: "CounterType=5000&OrgId=" + SubOrgId + "&VD=" + window.localStorage.getItem("UUID"),
                    success: function (resp) {
                        SpinnerPlugin.activityStop();
                        var ItemsData = resp;
                        var li_ = "";
                        var MainData = '';
                        for (var i = 0; i < ItemsData.length; i++) {

                            if (ItemsData[i].TenderID.replace(' ', '') != 'CreditNote') {

                                if (ItemsData[i].TenderID.replace(' ', '') == 'Cash') {
                                    li_ = li_ + "<li  id=\'li_" + ItemsData[i].TenderID.replace(' ', '') + "\' onclick=ontender(\'" + ItemsData[i].TenderID.replace(' ', '') + "\',\'" + ItemsData[i].TenderType.replace(' ', '') + "\',\'" + ItemsData[i].REFERENCE.replace(' ', '') + "\',\'" + ItemsData[i].SNO + "\');><img src='images/money.png' style='top: 40px;height: 42px;width: 42px;margin: 0px 8px 5px -37px;'>";
                                    if (hashSplit[7] == 'ar-SA')
                                        li_ = li_ + 'نقدي' + "</li>";
                                    else
                                        li_ = li_ + 'Cash' + "</li>";
                                }
                                if (ItemsData[i].TenderID.replace(' ', '') == 'CreditCard') {
                                    li_ = li_ + "<li  id=\'li_" + ItemsData[i].TenderID.replace(' ', '') + "\' onclick=ontender(\'" + ItemsData[i].TenderID.replace(' ', '') + "\',\'" + ItemsData[i].TenderType.replace(' ', '') + "\',\'" + ItemsData[i].REFERENCE.replace(' ', '') + "\',\'" + ItemsData[i].SNO + "\');><img src='images/Card.png' style='height: 40px;top: 40px;width: 39px;margin: 0px 12px 6px -38px;'>";
                                    if (hashSplit[7] == 'ar-SA')
                                        li_ = li_ + 'بطاقة' + "</li>";
                                    else
                                        li_ = li_ + 'Card' + "</li>";
                                }
                                if (ItemsData[i].TenderID.replace(' ', '') == 'E-Pay') {
                                    li_ = li_ + "<li  id=\'li_" + ItemsData[i].TenderID.replace(' ', '') + "\' onclick=ontender(\'" + ItemsData[i].TenderID.replace(' ', '') + "\',\'" + ItemsData[i].TenderType.replace(' ', '') + "\',\'" + ItemsData[i].REFERENCE.replace(' ', '') + "\',\'" + ItemsData[i].SNO + "\');><img src='images/digital-wallet.png' style='height: 42px;top: 40px;width: 42px;margin: 0px 8px 5px -20px;'>";
                                    if (hashSplit[7] == 'ar-SA')
                                        li_ = li_ + 'E-الدفع' + "</li>";
                                    else
                                        li_ = li_ + 'E-wallet' + "</li>";
                                }

                            }

                        } for (var i = 0; i < ItemsData.length; i++) {
                            if (ItemsData[i].TenderID.replace(' ', '') == 'CreditNote') {
                                li_ = li_ + "<li  id=\'li_" + ItemsData[i].TenderID.replace(' ', '') + "\' onclick=ontender(\'" + ItemsData[i].TenderID.replace(' ', '') + "\',\'" + ItemsData[i].TenderType.replace(' ', '') + "\',\'" + ItemsData[i].REFERENCE.replace(' ', '') + "\',\'" + ItemsData[i].SNO + "\');><img src='images/CrNote.png' style='height: 42px;top: 40px;width: 42px;margin: 0px 8px 5px -9px;'>";
                                if (hashSplit[7] == 'ar-SA')
                                    li_ = li_ + 'إشعار ائتمان' + "</li>";
                                else
                                    li_ = li_ + 'Credit Note' + "</li>";
                            }

                        }
                        window.localStorage.setItem("TenderDetails", li_);
                        window.localStorage.setItem("OfflineTenderDetails", li_);

                    },
                    error: function (e) {
                    }

                });
            }
        }
    }
    catch (a) {

    }
    try {
        function GetPrintCusToOffline() {
            $.ajax({
                type: 'GET',
                url: CashierLite.Settings.getPrintCusData,
                data: "VD=" + window.localStorage.getItem("UUID"),
                success: function (resp) {
                    window.localStorage.setItem("PrintOffline", JSON.stringify(resp[0]));
                 
                },
                error: function (e) {

                }

            });
        }
    }

    catch (a)

    { }

    //Multy End
    try {
        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.GetDafaultLanguage,
            data: "",
            success: function (resp) {
                var langData = resp;
                document.getElementById("ddlLanguage").options.length = 0;
                for (var i = 0; i < langData.length; i++) {
                    var Text = langData[i].LanguageText;
                    var val = langData[i].LanguageVal;
                    $("#ddlLanguage").append($("<option></option>").val(val).html(Text));
                };
                var HashVal = (window.localStorage.getItem("hashValues"));
                var HashValues;
                if (HashVal != null && HashVal != '') {
                    HashValues = HashVal.split('~');
                }
                $('#ddlLanguage').val(HashValues[7]);
                $('#ddlDateFormate').val(HashValues[2]);
            },
            error: function (e) {

            }
        });
    }
    catch (r) {

    }
    function MDPLSettings() {
        if (window.localStorage.getItem('network') == '1') {

            $('#ModalSettingsPopup').modal('show');
        }
        //ModalSettingsPopup
    }
    function MDPLSettingsDual() {
        $('#ModalDualPopup').modal('show');
    }
    function saveDualPSettingsPOP() {

        var shoDisplay = window.localStorage.getItem('showDisplay');
        var addver = window.localStorage.getItem('Advertisement');
        var options = { dimBackground: true };
        SpinnerPlugin.activityStart("Please wait...", options);
        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.updateDualDisplay,
            data: "shoDisplay=" + shoDisplay + "&addver=" + addver + "&VD=" + window.localStorage.getItem("UUID"),
            success: function (resp) {
                SpinnerPlugin.activityStop();
                if (resp == '100000') {
                    $('#ModalDualPopup').modal('hide');
                    navigator.notification.alert('Saved Successfully', "", "neo ecr", "Ok");
                }
                else {

                }

            },
            error: function (e) {
                SpinnerPlugin.activityStop();
                document.getElementById('txtusername').value = '';
                document.getElementById('txtpassword').value = '';
                alert('Invalid Data', null, 'Error');
            }
        });


    }
    //region userimage
    function openFileUser() {



        if (window.localStorage.getItem('network') == '1') {
            $('#tbluser tbody').children().remove();
            $('#ModalUploadimagePopup').modal('show');
        }
        else {
            navigator.notification.alert("can't change profile picture in offline network mode", "", "neo ecr", "Ok");
        }

    }
    function openUserFileOption() {
        document.getElementById("userinp").click();
    }
    function RefreshData() {
        location.reload();
    }
    function readFile() {
try
{
        if (this.files && this.files[0]) {
            var FR = new FileReader(); 
            FR.onload = function (e) {

                document.getElementById("Uimg").src = e.target.result;
                document.getElementById("b64").innerHTML = e.target.result;
            } 
            FR.readAsDataURL(this.files[0]);
        }
    }
    catch(a)
    {
        alert(a);
    }
    }
    function saveimage() {

        if (document.getElementById("b64").innerHTML == '') {
            navigator.notification.alert($('#hdnSelImage').val(), "", "neo ecr", "Ok");
            return false;
        }
      
        var options = { dimBackground: true };
        SpinnerPlugin.activityStart("Please wait...", options);
        Jsonobj = [];
        var item = {};
        var HashValues = (window.localStorage.getItem("hashValues"));
        if (HashValues != null && HashValues != '') {
            var hashSplit = HashValues.split('~');
            Userid = hashSplit[14];
        }
       
        $('#hdnUserId').val(Userid);
        if ($('#hdnUserId').val() == "") {
            item["UserId"] = "";
        }
        else {
            item["UserId"] = $('#hdnUserId').val();
        }
        
        var vd_ = window.localStorage.getItem("UUID");
        item["img"] = document.getElementById("b64").innerHTML;
        item["VD"] = vd_;
        Jsonobj.push(item);
        
        var UserImageData = new Object();
        UserImageData.Data = JSON.stringify(Jsonobj);
        var ComplexObject = new Object();
        //The property name "WebMethodArgName" must match the web method argument "WebMethodArgName"!
        ComplexObject.UserImageData = UserImageData;

        //Stringify and verify the object is foramtted as expected
        var data = JSON.stringify(ComplexObject); 
        $.ajax({
            type: 'POST',
            url: CashierLite.Settings.UserImageSave,
            data: data,
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            success: function (resp) { 
                SpinnerPlugin.activityStop();
                resp = resp.GetUserImageSaveResult;
                var result = resp.split(',');
                if (result[0] == "100000") {
                    //var $textAndPic = $('<div></div>');  
                    document.getElementById("b64").innerHTML = "";
                    document.getElementById("Uimg").src = ""; 
                    navigator.notification.alert($('#hdnImageSave').val(), "", "neo ecr", "Ok");

                    DisplayImage();

                }
            },
            error: function (e) {
               
                SpinnerPlugin.activityStop();
            }
        });
    }
    function DisplayImage() {
        try {

            Jsonobj = [];
            var item = {};
            var Userid = '';
            var HashValues = (window.localStorage.getItem("hashValues"));
            if (HashValues != null && HashValues != '') {
                var hashSplit = HashValues.split('~');
                Userid = hashSplit[14];
            }
            //$('#hdnUserId').val(Userid);
            //if ($('#hdnUserId').val() == "") {
            //    item["UserId"] = "";
            //}
            //else {
            //    item["UserId"] = $('#hdnUserId').val();
            //}
            item["UserId"] = Userid;
            var vd_ = window.localStorage.getItem("UUID");
            item["VD"] = vd_;
            item["path_"] = "";
            Jsonobj.push(item);
            var UserImageData = new Object();
            UserImageData.Data = JSON.stringify(Jsonobj);
            var ComplexObject = new Object();
            //The property name "WebMethodArgName" must match the web method argument "WebMethodArgName"!
            ComplexObject.UserImageData = UserImageData;

            //Stringify and verify the object is foramtted as expected
            var data = JSON.stringify(ComplexObject);
            document.getElementById("ImgUserPick").src = 'images/UserLogo.png';
            document.getElementById("Uimg").src = 'images/UserLogo.png';
            //var options = { dimBackground: true };
            //SpinnerPlugin.activityStart("Please wait...", options);
            $.ajax({
                type: 'POST',
                url: CashierLite.Settings.DisplayUserImage,
                data: data,
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                success: function (resp) {
                    //SpinnerPlugin.activityStop();
                    if (resp.DisplayUserImageResult != '') {
                        document.getElementById("ImgUserPick").src = resp.DisplayUserImageResult;
                        document.getElementById("Uimg").src = resp.DisplayUserImageResult;
                    }
                    $('#ModalUploadimagePopup').modal('hide');
                },
                error: function (e) {

                    //SpinnerPlugin.activityStop();
                }
            });
        }
        catch (a) {

        }
    }

    //End region userimage
    function savePSettingsPOP_new() {
        var language = $('#ddlLanguage option:selected').val();
        var dateForamt = $('#ddlDateFormate option:selected').val();
        var options = { dimBackground: true };
        SpinnerPlugin.activityStart("Please wait...", options);
        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.updateUserSettingsDisplay,
            data: "language=" + language + "&dateForamt=" + dateForamt + "&UserId=" + window.localStorage.getItem('Sno') + "&display=" + window.localStorage.getItem('showDisplay') + "&VD=" + vd_,
            success: function (resp) {
                SpinnerPlugin.activityStop();
                if (resp == '100000') {
                    $('#ModalSettingsPopup').modal('hide');
                    navigator.notification.alert($('#hdnChPwdSaveSucc').val(), "", "neo ecr", "Ok");
                }
                else {
                }
            },
            error: function (e) {
                SpinnerPlugin.activityStop();
                document.getElementById('txtusername').value = '';
                document.getElementById('txtpassword').value = '';
                alert('Invalid Data', null, 'Error');
            }
        });
    }
    function savePSettingsPOP() {
        var language = $('#ddlLanguage option:selected').val();
        var dateForamt = $('#ddlDateFormate option:selected').val();
        var options = { dimBackground: true };
        SpinnerPlugin.activityStart("Please wait...", options);
        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.updateUserSettings,
            data: "language=" + language + "&dateForamt=" + dateForamt + "&UserId=" + window.localStorage.getItem('Sno') + "&VD=" + vd_,
            success: function (resp) {
                SpinnerPlugin.activityStop();
                if (resp == '100000') {
                    $('#ModalSettingsPopup').modal('hide');
                    navigator.notification.alert($('#hdnChPwdSaveSucc').val(), "", "neo ecr", "Ok");
                }
                else {

                }

            },
            error: function (e) {
                SpinnerPlugin.activityStop();
                document.getElementById('txtusername').value = '';
                document.getElementById('txtpassword').value = '';
                alert('Invalid Data', null, 'Error');
            }
        });

    }
    //Date Convert Based On Login Date Formate to Client Control--Siva
    function ConvertDateWithFormat(formates, dateText, textdate) {
        var hdnformat = formates;
        var txtMainDate = textdate;
        var splitc;
        if (hdnformat == 'd/m/Y' || hdnformat == 'm/d/Y' || hdnformat == 'Y/d/m' || hdnformat == 'Y/m/d' || hdnformat == 'M/d/Y' || hdnformat == 'd/M/Y') {
            splitc = '/';
        }
        else if (hdnformat == 'd-m-Y' || hdnformat == 'm-d-Y' || hdnformat == 'Y-d-m' || hdnformat == 'Y-m-d' || hdnformat == 'M-d-Y' || hdnformat == 'd-M-Y') {
            splitc = '-';
        }
        else if (hdnformat == 'd,m,Y' || hdnformat == 'm,d,Y' || hdnformat == 'Y,d,m' || hdnformat == 'Y,m,d' || hdnformat == 'M,d,Y' || hdnformat == 'd,M,Y') {

            splitc = ',';
        }
        else if (hdnformat == 'd m Y' || hdnformat == 'm d Y' || hdnformat == 'Y d m' || hdnformat == 'Y m d' || hdnformat == 'M d Y' || hdnformat == 'd M Y') {

            splitc = ' ';
        }
        else if (hdnformat == 'd.m.Y' || hdnformat == 'm.d.Y' || hdnformat == 'Y.d.m' || hdnformat == 'Y.m.d' || hdnformat == 'M.d.Y' || hdnformat == 'd.M.Y') {
            splitc = '.';
        };
        var sym = '-';
        if (dateText.indexOf('/') != -1)
            sym = '/';
        var vardatesplit = dateText.split(sym);
        var varM;
        var varMn;
        var varfformatS = hdnformat.split(splitc);
        if (varfformatS[0] == 'M' || varfformatS[1] == 'M') {

            if (vardatesplit[1] != null)
                varM = vardatesplit[1];
            if (vardatesplit[0] != null)
                varM = vardatesplit[0];

            switch (varM) {
                case 'Jan': varMn = '01';
                    break;
                case 'Feb': varMn = '02';
                    break;
                case 'Mar': varMn = '03';
                    break;
                case 'Apr': varMn = '04';
                    break;
                case 'May': varMn = '05';
                    break;
                case 'Jun': varMn = '06';
                    break;
                case 'Jul': varMn = '07';
                    break;
                case 'Aug': varMn = '08';
                    break;
                case 'Sep': varMn = '09';
                    break;
                case 'Oct': varMn = '10';
                    break;
                case 'Nov': varMn = '11';
                    break;
                case 'Dec': varMn = '12';
                    break;
            }

        }
        if (varfformatS[0] == 'd' && (varfformatS[1] == 'm' || varfformatS[1] == 'M') && varfformatS[2] == 'Y') {
            if (varfformatS[1] == 'M') {
                $("#" + txtMainDate).val(varMn + splitc + vardatesplit[1] + splitc + vardatesplit[0]);
            }
            else {
                $("#" + txtMainDate).val(vardatesplit[2] + splitc + vardatesplit[1] + splitc + vardatesplit[0]);
            }
        }
        else if ((varfformatS[0] == 'm' || varfformatS[0] == 'M') && varfformatS[1] == 'd' && varfformatS[2] == 'Y') {
            if (varfformatS[0] == 'M') {
                $("#" + txtMainDate).val(varMn + splitc + vardatesplit[2] + splitc + vardatesplit[0]);
            }
            else {
                $("#" + txtMainDate).val(vardatesplit[1] + splitc + vardatesplit[2] + splitc + vardatesplit[0]);
            }
        }
        else if (varfformatS[0] == 'Y' && varfformatS[1] == 'd' && varfformatS[2] == 'm') {
            $("#" + txtMainDate).val(vardatesplit[0] + splitc + vardatesplit[2] + splitc + vardatesplit[1]);
        }
        else if (varfformatS[0] == 'Y' && varfformatS[1] == 'm' && varfformatS[2] == 'd') {
            $("#" + txtMainDate).val(vardatesplit[0] + splitc + vardatesplit[1] + splitc + vardatesplit[2]);
        }
        else {
            $("#" + txtMainDate).val(vardatesplit[0] + splitc + vardatesplit[1] + splitc + vardatesplit[2]);
        }
    }

    //Get YYYY-MM-DD Format
    function GetDateWithDataBaseFormat(Date) {
        var sym = '-';
        if (Date.indexOf('/') != -1)
            sym = '/';
        var Date_ = (Date).split(sym);
        var FDate = Date_[0] + sym + Date_[1] + sym + Date_[2];
        return FDate;
    }
    //Get YYYY-MM-DD Format
    function GetDateWithYYYYMMDD(formates, dateText) {
        var hdnformat = formates;
        var txtMainDate = "";
        var sym = '-';
        if (dateText.indexOf('/') != -1)
            sym = '/';
        else if (dateText.indexOf(',') != -1)
            sym = ',';
        else if (dateText.indexOf(' ') != -1)
            sym = ' ';
        else if (dateText.indexOf('.') != -1)
            sym = '.';
        var splitc = '-';
        var vardatesplit = dateText.split(sym);
        if (hdnformat == 'd/m/Y' || hdnformat == 'd/M/Y' || hdnformat == 'd-m-Y' || hdnformat == 'd-M-Y' || hdnformat == 'd,m,Y' || hdnformat == 'd,M,Y' || hdnformat == 'd m Y' || hdnformat == 'd M Y' || hdnformat == 'd.m.Y' || hdnformat == 'd.M.Y') {
            txtMainDate = vardatesplit[2] + splitc + vardatesplit[1] + splitc + vardatesplit[0];
        }
        else if (hdnformat == 'm/d/Y' || hdnformat == 'm-d-Y' || hdnformat == 'M-d-Y' || hdnformat == 'M/d/Y' || hdnformat == 'm,d,Y' || hdnformat == 'M,d,Y' || hdnformat == 'm d Y' || hdnformat == 'M d Y' || hdnformat == 'm.d.Y' || hdnformat == 'M.d.Y') {
            txtMainDate = vardatesplit[2] + splitc + vardatesplit[0] + splitc + vardatesplit[1];
        }
        else if (hdnformat == 'Y/d/m' || hdnformat == 'Y,d,m' || hdnformat == 'Y-d-m' || hdnformat == 'Y d m' || hdnformat == 'Y.d.m' || hdnformat == 'Y/d/M' || hdnformat == 'Y,d,M' || hdnformat == 'Y-d-M' || hdnformat == 'Y d M' || hdnformat == 'Y.d.M') {
            txtMainDate = vardatesplit[0] + splitc + vardatesplit[2] + splitc + vardatesplit[1];
        }
        else if (hdnformat == 'Y/m/d' || hdnformat == 'Y,d,m' || hdnformat == 'Y-d-m' || hdnformat == 'Y d m' || hdnformat == 'Y.d.m' || hdnformat == 'Y/M/d' || hdnformat == 'Y,d,M' || hdnformat == 'Y-d-M' || hdnformat == 'Y d M' || hdnformat == 'Y.d.M') {
            txtMainDate = vardatesplit[0] + splitc + vardatesplit[1] + splitc + vardatesplit[2];
        }
        else {
            txtMainDate = vardatesplit[0] + splitc + vardatesplit[1] + splitc + vardatesplit[2];
        }; 
        return txtMainDate;
    }
    //Return Date With Login User Date Formate

    function ConvertDateAndReturnDate(formates, dateText) {
        var hdnformat = formates;
        var txtMainDate = '';
        var splitc;
        if (hdnformat == 'd/m/Y' || hdnformat == 'm/d/Y' || hdnformat == 'Y/d/m' || hdnformat == 'Y/m/d' || hdnformat == 'M/d/Y' || hdnformat == 'd/M/Y') {
            splitc = '/';
        }
        else if (hdnformat == 'd-m-Y' || hdnformat == 'm-d-Y' || hdnformat == 'Y-d-m' || hdnformat == 'Y-m-d' || hdnformat == 'M-d-Y' || hdnformat == 'd-M-Y') {
            splitc = '-';
        }
        else if (hdnformat == 'd,m,Y' || hdnformat == 'm,d,Y' || hdnformat == 'Y,d,m' || hdnformat == 'Y,m,d' || hdnformat == 'M,d,Y' || hdnformat == 'd,M,Y') {

            splitc = ',';
        }
        else if (hdnformat == 'd m Y' || hdnformat == 'm d Y' || hdnformat == 'Y d m' || hdnformat == 'Y m d' || hdnformat == 'M d Y' || hdnformat == 'd M Y') {

            splitc = ' ';
        }
        else if (hdnformat == 'd.m.Y' || hdnformat == 'm.d.Y' || hdnformat == 'Y.d.m' || hdnformat == 'Y.m.d' || hdnformat == 'M.d.Y' || hdnformat == 'd.M.Y') {
            splitc = '.';
        };
        var sym = '-';
        if (dateText.indexOf('/') != -1)
            sym = '/';
        var vardatesplit = dateText.split(sym);
        var varM;
        var varMn;
        var varfformatS = hdnformat.split(splitc);
        if (varfformatS[0] == 'M' || varfformatS[1] == 'M') {

            if (vardatesplit[1] != null)
                varM = vardatesplit[1];
            if (vardatesplit[0] != null)
                varM = vardatesplit[0];

            switch (varM) {
                case 'Jan': varMn = '01';
                    break;
                case 'Feb': varMn = '02';
                    break;
                case 'Mar': varMn = '03';
                    break;
                case 'Apr': varMn = '04';
                    break;
                case 'May': varMn = '05';
                    break;
                case 'Jun': varMn = '06';
                    break;
                case 'Jul': varMn = '07';
                    break;
                case 'Aug': varMn = '08';
                    break;
                case 'Sep': varMn = '09';
                    break;
                case 'Oct': varMn = '10';
                    break;
                case 'Nov': varMn = '11';
                    break;
                case 'Dec': varMn = '12';
                    break;
            }

        }
        if (varfformatS[0] == 'd' && (varfformatS[1] == 'm' || varfformatS[1] == 'M') && varfformatS[2] == 'Y') {
            if (varfformatS[1] == 'M') {
                txtMainDate = varMn + splitc + vardatesplit[1] + splitc + vardatesplit[0];
            }
            else {
                txtMainDate = vardatesplit[2] + splitc + vardatesplit[1] + splitc + vardatesplit[0];
            }
        }
        else if ((varfformatS[0] == 'm' || varfformatS[0] == 'M') && varfformatS[1] == 'd' && varfformatS[2] == 'Y') {
            if (varfformatS[0] == 'M') {
                txtMainDate = varMn + splitc + vardatesplit[2] + splitc + vardatesplit[0];
            }
            else {
                txtMainDate = vardatesplit[1] + splitc + vardatesplit[2] + splitc + vardatesplit[0];
            }
        }
        else if (varfformatS[0] == 'Y' && varfformatS[1] == 'd' && varfformatS[2] == 'm') {
            txtMainDate = vardatesplit[0] + splitc + vardatesplit[2] + splitc + vardatesplit[1];
        }
        else if (varfformatS[0] == 'Y' && varfformatS[1] == 'm' && varfformatS[2] == 'd') {
            txtMainDate = vardatesplit[0] + splitc + vardatesplit[1] + splitc + vardatesplit[2];
        }
        else {
            txtMainDate = vardatesplit[0] + splitc + vardatesplit[1] + splitc + vardatesplit[2];
        }
        return txtMainDate;
    }

    function onexit() {
        if (window.localStorage.getItem('network') == '1') {
            var GUUID = window.localStorage.getItem("UUID"); 
            window.localStorage.clear();
            window.localStorage.setItem("UUID", GUUID); 
            window.open('Login.html', '_self', 'location=no');
        }
        else {
            navigator.notification.alert("can't logout in offline network mode", "", "neo ecr", "Ok");
        }
    } 
    var DBOP; 
    function ConfigurationData() {
        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.ConfigurationDataImages,
            data: "VD=" + vd_,
            success: function (resp) {  
                DBOP = JSON.stringify(resp);
                var db = window.openDatabase("myEPAYDB", '1.0', 'CashierLiteDB', 2 * 1024 * 1024);
                db.transaction(DBPushconfig, errorConfig, successConfig);
            }
        });
    } 
    function DBPushconfig(tx) {
        tx.executeSql('DROP TABLE IF EXISTS EPAYData');
        tx.executeSql('CREATE TABLE IF NOT EXISTS EPAYData (Id, Name,WalletLogo,QRCode,Mobile,Wallet,Status,QrUrl,WalletUrl,SAQRID, Version, Expairydate, UrPayStatus,InstanceID )');
        var sqlDBObj_ = JSON.parse(DBOP);
        for (var i = 0; i < sqlDBObj_.length; i++) {
            if (sqlDBObj_[i].ItemId != '') { 
                tx.executeSql('INSERT INTO EPAYData (Id, Name,WalletLogo,QRCode,Mobile,Wallet,Status,QrUrl,WalletUrl,SAQRID, Version, Expairydate, UrPayStatus,InstanceID ) VALUES ("' + sqlDBObj_[i].InvoiceID + '", "'
                 + sqlDBObj_[i].InvoiceNo + '", "' + sqlDBObj_[i].CustomerName + '", "' + sqlDBObj_[i].Address + '", "'
                  + sqlDBObj_[i].ContactNo + '", "' + sqlDBObj_[i].CREATEDDATE + '","' + sqlDBObj_[i].CustomerId + '","'
                   + sqlDBObj_[i].InvoiceDate + '","' + sqlDBObj_[i].Description + '","' + sqlDBObj_[i].SAQRID + '","' + sqlDBObj_[i].Version + '","' + sqlDBObj_[i].ExpairyDate  + '","' + sqlDBObj_[i].URPayStatus   + '","' + sqlDBObj_[i].InstanceID   + '")');
            }
        }
    }
    function errorConfig(tx, err) {
      
     } 
    function successConfig() { 
        
    } 
    $(document).ready(function () { 
        jQuery.ajaxSetup({
            beforeSend: function () {
                $("#spinner").show();
            },
            complete: function () {
                $("#spinner").hide();
            },
            success: function () { }
        })
    });
}
//}
try {

    if (window.localStorage.getItem('YOUTUBE') != '0') {
        if (window.localStorage.getItem("Advertisement") == "1") { 
            $('#btnstartadd').attr('style', 'background-color:#1776b6;color:white');
            $('#btnstoptadd').attr('style', 'background-color:white;color:black');
        }
        else { 
            window.localStorage.setItem("Advertisement", "0");
            $('#btnstoptadd').attr('style', 'background-color:#1776b6;color:white');
            $('#btnstartadd').attr('style', 'background-color:white;color:black');
        } 
        if (window.localStorage.getItem("showDisplay") == "1") {

            $('#btnstartdisplay').attr('style', 'background-color:#1776b6;color:white');
            $('#btnstopdisplay').attr('style', 'background-color:white;color:black');
        }
        else {
            $('#btnstopdisplay').attr('style', 'background-color:#1776b6;color:white');
            $('#btnstartdisplay').attr('style', 'background-color:white;color:black');
        }
    }
    else {
        var elemli_ = document.getElementById("li_mainDualbtn");
        elemli_.style.display = "none";
    }
}
catch (a) {

}

var HashVal = (window.localStorage.getItem("hashValues"));
var HashValues;
if (HashVal != null && HashVal != '') {
    HashValues = HashVal.split('~');
    if (HashValues[7] == 'ar-SA') {
        $('html,body').append('<link rel="stylesheet" href="Styles/css/bootstrap-rtl.css" type="text/css" />');
        $('html,body').append('<link rel="stylesheet" href="Styles/css/bootstrap-flipped.css" type="text/css" />');
        $('head').append('<script src="scripts/bootstrap-dialog-SA.min.js"></script>');
    }
    $('#hdnDateFromate').val(HashValues[2])
}
function showDualDisplay() {
    if (window.localStorage.getItem('YOUTUBE') != '') {
        try {
            window.localStorage.setItem("showDisplay", "1"); 
            $('#btnstartdisplay').attr('style', 'background-color:#1776b6;color:white');
            $('#btnstopdisplay').attr('style', 'background-color:white;color:black');
        }
        catch (e) {
        }
    }
}
function stopDualDisplay() {
    if (window.localStorage.getItem('YOUTUBE') != '') {
        try {
            window.localStorage.setItem("showDisplay", "0");
            window.localStorage.setItem("Advertisement", "0"); 
            $('#btnstopdisplay').attr('style', 'background-color:#1776b6;color:white');
            $('#btnstartdisplay').attr('style', 'background-color:white;color:black');
        }
        catch (e) {
        }
    }
}
function StartAdvertisement() {
    if (window.localStorage.getItem('showDisplay') == '1') {
        window.localStorage.setItem("Advertisement", "1");
        $('#btnstartadd').attr('style', 'background-color:#1776b6;color:white');
        $('#btnstoptadd').attr('style', 'background-color:white;color:black');
    }
    else {
        navigator.notification.alert('Please Start Dual Display', "", "neo ecr", "Ok"); 
    }
}
function StoptAdvertisement() {

    window.localStorage.setItem("showDisplay", "0");
    window.localStorage.setItem("Advertisement", "0");
    $('#btnstoptadd').attr('style', 'background-color:#1776b6;color:white');
    $('#btnstartadd').attr('style', 'background-color:white;color:black');
    window.localStorage.setItem("showDisplay", "1");
}