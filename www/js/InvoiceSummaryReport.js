﻿
 
    var graphData;
    function VD() {
        window.localStorage.setItem('ctCheck', 1);
        var vd_ = window.localStorage.getItem("UUID");
        return vd_;
    }
    function treeclick(data) {
    window.localStorage.setItem('ctCheck', 1);
    var d_ = data.parentNode;
    if ($('#' + d_.id + '_0')[0].style.display != '')
        $('#' + d_.id + '_0')[0].style.display = '';
    else
        $('#' + d_.id + '_0')[0].style.display = 'none';
  }

    function theamLoad() {
        if (window.localStorage.getItem("Theam") != null)
            $('head').append('<link rel="stylesheet" href=' + window.localStorage.getItem("Theam") + ' type="text/css" />');
    }
    if (window.localStorage.getItem("Theam").split('-')[3] == '1.css') {
        $('#IMG1').attr("src", "images/mmlogo-inner2.png");
    } else {
        $('#IMG1').attr("src", "images/mmlogo-inner1.png");
    }
   
    function InvMail() {
        $('#txtBccMailId').val('');
        $('#txtBody').val('');


        window.localStorage.setItem('ctCheck', 1);
        $('#ModalINVMail').modal('show');
        //   tenderAddpartial(TenderID, 1, '');
        $('#txtMailId').val('');
        //  $('#FileFormat').val('0');
        $('#txtMailId').focus();
        $('#FileFormat').attr("readonly", "true");
    }
    function SendINVMail(MailId, BccMailId, Format, Body) {
        window.localStorage.setItem('ctCheck', 1);
        var HashVa = (window.localStorage.getItem("hashValues"));
        if (HashVa != null && HashVa != '') {
        HashVa = HashVa.split('~');
       }
        if ($('#txtMailId').val() == "") {
            navigator.notification.alert($('#hdnEnterMail').val(), "", "neo ecr", "Ok");
        }
        else {

            var options = { dimBackground: true };
            SpinnerPlugin.activityStart("Please wait...", options);
            $.ajax({
                type: 'GET',
                url: CashierLite.Settings.SendMailArabic,
                data: "language=" + HashVa[7] + "&PageId=" + window.localStorage.getItem('RPPAGECODE') + "&subOrg=" + HashVa[1] + "&frmDate=" + window.localStorage.getItem('RPfrmDate') + "&toDate=" + window.localStorage.getItem('RPtoDate') + ' 23:59:59' + "&DateFormate=" + $('#hdnDateFromate').val() + "&VD=" + VD() + "&MailId=" + MailId + "&Format=" + Format + "&BCMailId=" + BccMailId + "&Body=" + Body + "",
                success: function (resp) {
                    SpinnerPlugin.activityStop();
                    if (resp == '1000') {
                        navigator.notification.alert($('#hdnMailSuccess').val(), "", "neo ecr", "Ok");
                        $('#ModalINVMail').modal('hide');
                    }
                    else {
                        navigator.notification.alert($('#hdnMailnotSuccess').val(), "", "neo ecr", "Ok");
                        $('#ModalINVMail').modal('hide');
                    }
                },
                error: function (e) {
                    SpinnerPlugin.activityStop();
                    navigator.notification.alert($('#hdnInvalidData').val(), "", "neo ecr", "Ok");
                    $('#ModalINVMail').modal('hide');
                }
            });
        }
}
var RName="";

function GetSubOrgData() {
    var OBSCode = (window.localStorage.getItem("hashValues"));
    if (OBSCode != null && OBSCode != '') {
        var OBS = OBSCode.split('~');
        var ss = OBS[0];
        var vd_ = window.localStorage.getItem("UUID");
        $.ajax({
            type: 'GET',
            async:false,
            url: CashierLite.Settings.GetSubOrgData,
            data: { OBSCode: ss, VD: vd_ },
            success: function (resp) {
                var OrgData = resp;
                if (OrgData[0].OrgCode.split('/').length > 2) {
                   RName=OrgData[0].OrgCode.split('/')[0] + '/' + OrgData[0].OrgCode.split('/')[1];
                }
                else {

                    RName=OrgData[0].OrgCode.split('/')[1];
                }
            },
            error: function (e) {
            }
        });
    }
}


    function PageInvoiceSummaryRPBind() { 
        theamLoad();
        
        $('#hdnPaging').val('1');
        var btnExp = document.getElementById("btnInvExp");
        btnExp.style.display = "block";
        if (window.localStorage.getItem('RPPAGECODE') == 'POSINLSM') {
            var SubOrgId = '';
            var HashVal = (window.localStorage.getItem("hashValues"));
            var HashValues;
            if (HashVal != null && HashVal != '') {
                HashValues = HashVal.split('~');

                SubOrgId = HashValues[1];
                $('#hdnSuborgId').val(HashValues[1]);
                $('#hdnDateFromate').val(HashValues[2]);

            }

            var PageIndex = $('#hdnPaging').val();
            var PageCount = $('#txt_pageCount').val();
            var storename = HashValues[17].split('/');
            $.ajax({
                type: 'GET',
                url: CashierLite.Settings.GetReport_V01,
                data: "PageId=" + window.localStorage.getItem('RPPAGECODE') + "&subOrg=" + SubOrgId + "&frmDate=" + window.localStorage.getItem('RPfrmDate') + "&toDate=" + window.localStorage.getItem('RPtoDate') + ' 23:59:59' + "&refNo=" + window.localStorage.getItem('RPrefNo') + "&DateFormate=" + $('#hdnDateFromate').val() + "&VD=" + VD() + "&PageIndex=" + PageIndex + "&PageCount=" + PageCount  + "&Language=" + HashValues[7] + "",
                success: function (resp) {

                    graphData = resp;
                    if (resp != '') {
                        jQuery("label[for='lbl_invPageOf'").html(resp[0].C15);
                        $('#hdnTotalPagesCount').val(resp[0].C5);
                    }

                    InvoiceSummaryResources(HashValues[7], "POSInvoiceSummaryReport", resp, 1, storename[1], window.localStorage.getItem('RPfrmDate'), window.localStorage.getItem('RPtoDate'));
                    $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());

                    if ($('#hdnTotalPagesCount').val() != $('#hdnPaging').val()) {
                        $('#li_PRPrev i').removeAttr('style', 'color:#919598 !important');
                        $('#li_PRPrev').attr('onclick', 'nextPRInvl()');
                        $('#li_PRLast i').removeAttr('style', 'color:#919598 !important');
                        $('#li_PRLast').attr('onclick', 'nextPRLastInvl()');
                    }
                    else {
                        $('#li_PRPrev i').attr('style', 'color:#919598 !important');
                        $('#li_PRPrev').attr('onclick', 'return false');
                        $('#li_PRLast').attr('onclick', 'return false');
                        $('#li_PRLast i').attr('style', 'color:#919598 !important');
                    }
                }

            });
        }
        GetSubOrgData();
    }

    function InvoiceSummaryResources(langCode, pageCode, data, sts, store, fromdate, todate) {
        $.ajax({
            type: 'GET',
            async:false,
            url: CashierLite.Settings.getResource,
            data: "langCode=" + langCode + "&pageCode=" + pageCode,
            success: function (resp) {
                InvoiceSummaryResourcesLabels(resp, data, sts, store, fromdate, todate);
                $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());
            }
        });
    }

function getLoadExdata()
{

//document.getElementById("FileFormat").options.length = 0;
$("#FileFormat").append($("<option></option>").val("0").html("----select----"));
$("#FileFormat").append($("<option></option>").val("1").html($('#hdnMExcel').val()));
 $("#FileFormat option[value='1']").attr('selected','selected');
}
    function InvoiceSummaryResourcesLabels(labelData, data, sts, store, fromdate, todate) {
         $('#hdnCreditSales').val(labelData["PosCrdSales"]);
        $('#hdnImageSave').val(labelData["ImageSave"]);
       jQuery("label[for='lbl_page'").html(labelData["page"]);
        $('#hdnSelImage').val(labelData["SelUserImage"]);
        jQuery("label[for='lblUpload'").html(labelData["upload"]);
        jQuery("label[for='lblTitle'").html(labelData["category"]);
        jQuery("label[for='lblInvoiceSummaryReport'").html(labelData["category"]);
        $('#hdnInvDetails').val(labelData['category']);
        $('#hdnInvoiceNo').val(labelData['invoice']);
        $('#hdnInvoiceDt').val(labelData['date']);
        $('#hdnCustomer').val(labelData['customer']);
        $('#hdnTill').val(labelData['till']);
        $('#hdnCashier').val(labelData['CashierName']);
        $('#hdnBaseTot').val(labelData['total']);
        $('#hdnTaxTot').val(labelData['tax']);
        $('#hdnNetTot').val(labelData['netTotal']);
        $('#hdnItemCode').val(labelData['item'] + ' ' + labelData['code']);
        $('#hdnItemName').val(labelData['name']);
        $('#hdnUOM').val(labelData['uom']);
        $('#hdnQty').val(labelData['qty']);
        $('#hdnPrice').val(labelData['price']);
        $('#hdnDisc').val(labelData['disc']);
        $('#hdnAmount').val(labelData['amount']);
        $('#hdnTenderType').val(labelData['tenderType']);
        $('#hdnChPwdSaveSucc').val(labelData["saveSuccess"]);
        $('#hdnThameChange').val(labelData["alert1"]);
        $('#hdnNotAssignCountrToTrans').val(labelData["permissionsAssisted"]);
        $('#hdnDefault').val("");
        $('#hdnSalesTo').val(labelData['POSTo']);
        $('#hdnSales').val(labelData["alert1"]);
        $('#hdnLineView').val(labelData["alert2"]);
        $('#hdnStorName').val(labelData["state"]);
        $('#hdnTotNoInv').val(labelData["CurrentInventory"]);
        $('#hdnCash').val(labelData["totalCreditCard"]);
        $('#hdnCard').val(labelData["POSEcard"]);
        $('#hdnEPay').val(labelData["POSEWallet"]);
        $('#hdnTotal').val(labelData["TotalCashCollection"]);
        $('#hdnFromD').val(labelData["fromDate"]);
        $('#hdnToD').val(labelData["toDate"]);
        $('#hdnDate').val(labelData["dataMatrix"]);
        $('#hdnCounter').val(labelData["till"]);
        $('#hdnCashierInv').val(labelData["CashierName"]);
        $('#hdnEnterMail').val(labelData["enterMail"]);
        $('#hdnMailSuccess').val(labelData["MailSuccess"]);
        $('#hdnMailnotSuccess').val(labelData["MailnotSuccess"]);
        $('#hdnInvalidData').val(labelData["InvalidData"]);
        jQuery("label[for='lblMail']").html(labelData["Mail"]);
    jQuery("label[for='ToMail']").html(labelData["POSTo"]);
    jQuery("label[for='Bcc']").html(labelData["BCC"]);
    jQuery("label[for='lblFormat']").html(labelData["format"]);
    jQuery("label[for='lblBody']").html(labelData["body"]);
    jQuery("label[for='lblsend']").html(labelData["Send"]);
    jQuery("label[for='lblClose']").html(labelData["Close"]);
    $('#hdnMExcel').val(labelData["Excel"]);
getLoadExdata();
        arrangementInvoiceSummaryRPBind(data, store, fromdate, todate);

        if (sts == 2) {
            // arrangementInvoiceListingExcelBind(data);

            var $el = $('#tbldiv tbody>tr.tt-hide');
            $el.removeClass("tt-hide");

            for (var i = 0; i < $el.length; i++) {
                $('#root_' + i + '').html("-");
            }
            $('#DivPager').css("display", "none");
            if ($('#hdnScreenmode').val() == "print") {
                arrangementInvoiceSummaryRPBind(data, store, fromdate, todate);
                var divContents = document.getElementById("div_grid").innerHTML;
                var printWindow = window.open('', '', 'height=200,width=450');
                printWindow.document.write('<html><head><title>Invoice Summary</title>');
                printWindow.document.write('</head><body >');
                printWindow.document.write(divContents);
                printWindow.document.write('</body></html>');
                printWindow.document.close();
                printWindow.print();

            }
            if ($('#hdnScreenmode').val() == "excel") {
                arrangementInvoiceSummaryRPBind(data, store, fromdate, todate);
                var data_type = 'data:application/vnd.ms-excel';
                var table_div = document.getElementById('tbldiv');
                var table_html = table_div.outerHTML.replace(/ /g, '%20');
                var a = document.createElement('a');
                a.href = data_type + ', ' + table_html;
                a.download = 'Invoice-Listing' + '.xls';
                a.click();
                arrangementInvoiceSummaryRPBind(data, store, fromdate, todate);
            }

        }

        if (window.localStorage.getItem('RPfrmDate') != null) {
            $('#INVLISlblDate').text('(' + ConvertDateAndReturnDate($('#hdnDateFromate').val(), window.localStorage.getItem('RPfrmDate')) + ' ' + $('#hdnSalesTo').val() + ' ' + ConvertDateAndReturnDate($('#hdnDateFromate').val(), window.localStorage.getItem('RPtoDate')) + ')');
        }
    }
        function arrangementInvoiceSummaryRPBind(resp, store, fromdate, todate) {

        graphData = resp;
        var result = resp;
        var Primaryid = "";
 if (HashValues[7] == 'ar-SA') {
        $('#tbldiv thead tr').remove();
        $('#tbldiv tbody tr').remove();
        if (result.length != 0) {
            $('#tbldiv thead').append("<tr><th  style='text-align:right'>" + $('#hdnStorName').val() + "</th><th colspan='6' style='text-align:right'>" + RName + "</th></tr>");
            $('#tbldiv thead').append("<tr><th  style='text-align:right'>" + $('#hdnFromD').val() + "</th><th style='text-align:right'>" + ConvertDateAndReturnDate($('#hdnDateFromate').val(), fromdate) + "</th><th  style='text-align:right'>" + $('#hdnToD').val() + "</th><th style='text-align:right'>" + ConvertDateAndReturnDate($('#hdnDateFromate').val(), todate) + "</th><th colspan='3' </th></tr>");
            $('#tbldiv thead').append("<tr><th  style='text-align:right'>" + $('#hdnTotNoInv').val() + "</th><th style='text-align:right'>" + $('#hdnCash').val() + "</th><th  style='text-align:right'>" + $('#hdnCard').val() + "</th><th style='text-align:right'>" + $('#hdnEPay').val() + "</th><th style='text-align:right'>" + $('#hdnCreditSales').val() + "</th><th colspan='3'  style='text-align:right'>" + $('#hdnTotal').val() + "</th></tr>");
            $('#tbldiv thead').append("<tr><th  style='text-align:right'>" + result[0].C0 + "</th><th id='ColAlign'>" + parseFloat(result[0].C1).toFixed(2) + "</th><th  id='ColAlign'>" + parseFloat(result[0].C2).toFixed(2) + "</th><th id='ColAlign'>" + parseFloat(result[0].C3).toFixed(2) + "</th><th  id='ColAlign'>" + parseFloat(result[0].C7).toFixed(2) + "</th><th colspan='3' id='ColAlign'>" + parseFloat(result[0].C4).toFixed(2) + "</th></tr>");
            $('#tbldiv thead').append("<tr><th colspan='10'>  </th></tr>");

            var cc_ = '';
            for (var i = 0; i < result[0].oblist.length; i++) {
                cc_ = cc_ + '<tr  id="dt_ParrentA' + i + '_0" data-tt-parent="childPaging"><td colspan="10" ><table id="tblChildA' + i + '" data-tt-parent="root' + i + '" class="table table-bordered" >';
                cc_ = cc_ + "<tr><th  style='text-align:right'>" + $('#hdnDate').val() + "</th><th  style='text-align:right'>" + $('#hdnTill').val() + "</th><th  style='text-align:right'>" + $('#hdnTotNoInv').val() + "</th><th style='text-align:right'>" + $('#hdnCash').val() + "</th><th  style='text-align:right'>" + $('#hdnCard').val() + "</th><th style='text-align:right'>" + $('#hdnEPay').val() + "</th><th style='text-align:right'>" + $('#hdnCreditSales').val() + "</th><th style='text-align:right'>" + $('#hdnTotal').val() + "</th></tr>";
                cc_ = cc_ + "<tr><td   style='text-align:right'>" + result[0].oblist[i].C0 + "</td><td  style='text-align:right'>" + result[0].oblist[i].C1 + "</td><td  id='ColAlign'>" + result[0].oblist[i].C2 + "</td><td id='ColAlign'>" + parseFloat(result[0].oblist[i].C3).toFixed(2) + "</td><td  id='ColAlign'>" + parseFloat(result[0].oblist[i].C4).toFixed(2) + "</td><td id='ColAlign'>" + parseFloat(result[0].oblist[i].C5).toFixed(2) + "</td><td id='ColAlign'>" + parseFloat(result[0].oblist[i].C8).toFixed(2) + "</td><td id='ColAlign'>" + parseFloat(result[0].oblist[i].C6).toFixed(2) + "</td></tr>";
                cc_ = cc_ + "<tr><th>" + $('#hdnInvoiceNo').val() + "</th><th>" + $('#hdnInvoiceDt').val() + '&nbsp' + "</th><th>" + $('#hdnCounter').val() + "</th><th>" + $('#hdnCashierInv').val() + "</th><th>" + $('#hdnBaseTot').val() + "</th><th>" + $('#hdnTaxTot').val() + "</th><th>" + $('#hdnNetTot').val() + "</th></tr>";

                for (var j = 0; j < result[0].objRepparams.length; j++) {
                    if ((result[0].oblist[i].C0 == result[0].objRepparams[j].C2) && (result[0].oblist[i].C1 == result[0].objRepparams[j].C3)) {
                        cc_ = cc_ + '<tr id="dt_Parrent' + j + '"><td   style="text-align:right" onclick="treeclick(this);">&nbsp' + result[0].objRepparams[j].C1 + '</td><td style="text-align:right">' + result[0].objRepparams[j].C2 + '&nbsp' + '</td><td style="text-align:right">' + result[0].objRepparams[j].C3 + '</td><td style="text-align:right">' + result[0].objRepparams[j].C4 + '&nbsp' + '</td><td id="ColAlign">' + parseFloat(result[0].objRepparams[j].C5).toFixed(2) + '&nbsp' + '</td><td id="ColAlign">' + parseFloat(result[0].objRepparams[j].C6).toFixed(2) + '&nbsp' + '</td><td id="ColAlign">' + '&nbsp' + parseFloat(result[0].objRepparams[j].C7).toFixed(2) + '</td></tr>';
                        cc_ = cc_ + '<tr  style="display:none" id="dt_Parrent' + j + '_0" data-tt-parent="childPaging"><td colspan="10" ><table id="tblChild' + k + '" data-tt-parent="root' + j + '" class="table table-bordered" >';
                        cc_ = cc_ + "<tr><th>" + $('#hdnItemCode').val() + "</th><th>" + $('#hdnItemName').val() + "</th><th>" + $('#hdnUOM').val() + "</th><th>" + $('#hdnQty').val() + "</th><th>" + $('#hdnPrice').val() + "</th><th>" + $('#hdnDisc').val() + "</th><th>" + $('#hdnAmount').val() + "</th></tr>";
                        for (var k = 0; k < result[0].objInvList.length; k++) {
                            if (result[0].objRepparams[j].C0 == result[0].objInvList[k].InvoiceId) {
                                cc_ = cc_ + '<tr><td style="text-align:right">&nbsp' + result[0].objInvList[k].ItemCode + '</td><td style="text-align:right">' + result[0].objInvList[k].ItemName + '</td><td style="text-align:right">' + result[0].objInvList[k].UOM + '</td><td id="ColAlign">' + result[0].objInvList[k].QTY + '&nbsp' + '</td><td id="ColAlign">' + '&nbsp' + result[0].objInvList[k].Rate + '&nbsp' + '</td><td id="ColAlign">' + result[0].objInvList[k].Disc + '&nbsp' + '</td><td id="ColAlign">' + parseFloat(result[0].objInvList[k].Total).toFixed(2) + '&nbsp' + '</td></tr>';
                            }
                        }
                        cc_ = cc_ + '</table></td></tr>';

                    }
                }
                cc_ = cc_ + '</table></td></tr>';
            }
        }
        else {
            $('#tbldiv thead').append("<tr><th  style='text-align:left'>" + $('#hdnStorName').val() + "</th><th colspan='6' style='text-align:left'>" + store + "</th></tr>");
            $('#tbldiv thead').append("<tr><th  style='text-align:left'>" + $('#hdnFromD').val() + "</th><th style='text-align:left'>" + ConvertDateAndReturnDate($('#hdnDateFromate').val(), fromdate) + "</th><th  style='text-align:left'>" + $('#hdnToD').val() + "</th><th colspan='4' style='text-align:left'>" + ConvertDateAndReturnDate($('#hdnDateFromate').val(), todate) + "</th></tr>");
        }
        $("#tbldiv tbody").append(cc_);
}
else
{
       $('#tbldiv thead tr').remove();
        $('#tbldiv tbody tr').remove();
        if (result.length != 0) {
            $('#tbldiv thead').append("<tr><th  style='text-align:left'>" + $('#hdnStorName').val() + "</th><th colspan='6' style='text-align:left'>" + RName + "</th></tr>");
            $('#tbldiv thead').append("<tr><th  style='text-align:left'>" + $('#hdnFromD').val() + "</th><th style='text-align:left'>" + ConvertDateAndReturnDate($('#hdnDateFromate').val(), fromdate) + "</th><th  style='text-align:left'>" + $('#hdnToD').val() + "</th><th style='text-align:left'>" + ConvertDateAndReturnDate($('#hdnDateFromate').val(), todate) + "</th><th colspan='3' </th></tr>");
            $('#tbldiv thead').append("<tr><th  style='text-align:left'>" + $('#hdnTotNoInv').val() + "</th><th style='text-align:left'>" + $('#hdnCash').val() + "</th><th  style='text-align:left'>" + $('#hdnCard').val() + "</th><th style='text-align:left'>" + $('#hdnEPay').val() + "</th><th style='text-align:left'>" + $('#hdnCreditSales').val() + "</th><th colspan='3'  style='text-align:left'>" + $('#hdnTotal').val() + "</th></tr>");
            $('#tbldiv thead').append("<tr><th  style='text-align:left'>" + result[0].C0 + "</th><th id='ColAlign'>" + parseFloat(result[0].C1).toFixed(2) + "</th><th  id='ColAlign'>" + parseFloat(result[0].C2).toFixed(2) + "</th><th id='ColAlign'>" + parseFloat(result[0].C3).toFixed(2) + "</th><th id='ColAlign'>" + parseFloat(result[0].C7).toFixed(2) + "</th><th colspan='3' id='ColAlign'>" + parseFloat(result[0].C4).toFixed(2) + "</th></tr>");
            $('#tbldiv thead').append("<tr><th colspan='10'>  </th></tr>");

            var cc_ = '';
            for (var i = 0; i < result[0].oblist.length; i++) {
                cc_ = cc_ + '<tr  id="dt_ParrentA' + i + '_0" data-tt-parent="childPaging"><td colspan="10" ><table id="tblChildA' + i + '" data-tt-parent="root' + i + '" class="table table-bordered" >';
                cc_ = cc_ + "<tr><th  style='text-align:left'>" + $('#hdnDate').val() + "</th><th  style='text-align:left'>" + $('#hdnTill').val() + "</th><th  style='text-align:left'>" + $('#hdnTotNoInv').val() + "</th><th style='text-align:left'>" + $('#hdnCash').val() + "</th><th  style='text-align:left'>" + $('#hdnCard').val() + "</th><th style='text-align:left'>" + $('#hdnEPay').val() + "</th><th style='text-align:left'>" + $('#hdnCreditSales').val() + "</th><th style='text-align:left'>" + $('#hdnTotal').val() + "</th></tr>";
                cc_ = cc_ + "<tr><td   style='text-align:left'>" + result[0].oblist[i].C0 + "</td><td  style='text-align:left'>" + result[0].oblist[i].C1 + "</td><td  id='ColAlign'>" + result[0].oblist[i].C2 + "</td><td id='ColAlign'>" + parseFloat(result[0].oblist[i].C3).toFixed(2) + "</td><td  id='ColAlign'>" + parseFloat(result[0].oblist[i].C4).toFixed(2) + "</td><td id='ColAlign'>" + parseFloat(result[0].oblist[i].C5).toFixed(2) + "</td><td id='ColAlign'>" + parseFloat(result[0].oblist[i].C8).toFixed(2) + "</td><td id='ColAlign'>" + parseFloat(result[0].oblist[i].C6).toFixed(2) + "</td></tr>";
                cc_ = cc_ + "<tr><th>" + $('#hdnInvoiceNo').val() + "</th><th>" + $('#hdnInvoiceDt').val() + '&nbsp' + "</th><th>" + $('#hdnCounter').val() + "</th><th>" + $('#hdnCashierInv').val() + "</th><th>" + $('#hdnBaseTot').val() + "</th><th>" + $('#hdnTaxTot').val() + "</th><th>" + $('#hdnNetTot').val() + "</th></tr>";

                for (var j = 0; j < result[0].objRepparams.length; j++) {
                    if ((result[0].oblist[i].C0 == result[0].objRepparams[j].C2) && (result[0].oblist[i].C1 == result[0].objRepparams[j].C3)) {
                        cc_ = cc_ + '<tr id="dt_Parrent' + j + '"><td   style="text-align:left" onclick="treeclick(this);">&nbsp' + result[0].objRepparams[j].C1 + '</td><td style="text-align:left">' + result[0].objRepparams[j].C2 + '&nbsp' + '</td><td style="text-align:left">' + result[0].objRepparams[j].C3 + '</td><td style="text-align:left">' + result[0].objRepparams[j].C4 + '&nbsp' + '</td><td id="ColAlign">' + parseFloat(result[0].objRepparams[j].C5).toFixed(2) + '&nbsp' + '</td><td id="ColAlign">' + parseFloat(result[0].objRepparams[j].C6).toFixed(2) + '&nbsp' + '</td><td id="ColAlign">' + '&nbsp' + parseFloat(result[0].objRepparams[j].C7).toFixed(2) + '</td></tr>';
                        cc_ = cc_ + '<tr  style="display:none" id="dt_Parrent' + j + '_0" data-tt-parent="childPaging"><td colspan="10" ><table id="tblChild' + k + '" data-tt-parent="root' + j + '" class="table table-bordered" >';
                        cc_ = cc_ + "<tr><th>" + $('#hdnItemCode').val() + "</th><th>" + $('#hdnItemName').val() + "</th><th>" + $('#hdnUOM').val() + "</th><th>" + $('#hdnQty').val() + "</th><th>" + $('#hdnPrice').val() + "</th><th>" + $('#hdnDisc').val() + "</th><th>" + $('#hdnAmount').val() + "</th></tr>";
                        for (var k = 0; k < result[0].objInvList.length; k++) {
                            if (result[0].objRepparams[j].C0 == result[0].objInvList[k].InvoiceId) {
                                cc_ = cc_ + '<tr><td style="text-align:left">&nbsp' + result[0].objInvList[k].ItemCode + '</td><td style="text-align:left">' + result[0].objInvList[k].ItemName + '</td><td style="text-align:left">' + result[0].objInvList[k].UOM + '</td><td id="ColAlign">' + result[0].objInvList[k].QTY + '&nbsp' + '</td><td id="ColAlign">' + '&nbsp' + result[0].objInvList[k].Rate + '&nbsp' + '</td><td id="ColAlign">' + result[0].objInvList[k].Disc + '&nbsp' + '</td><td id="ColAlign">' + parseFloat(result[0].objInvList[k].Total).toFixed(2) + '&nbsp' + '</td></tr>';
                            }
                        }
                        cc_ = cc_ + '</table></td></tr>';

                    }
                }
                cc_ = cc_ + '</table></td></tr>';
            }
        }
        else {
            $('#tbldiv thead').append("<tr><th  style='text-align:left'>" + $('#hdnStorName').val() + "</th><th colspan='6' style='text-align:left'>" + store + "</th></tr>");
            $('#tbldiv thead').append("<tr><th  style='text-align:left'>" + $('#hdnFromD').val() + "</th><th style='text-align:left'>" + ConvertDateAndReturnDate($('#hdnDateFromate').val(), fromdate) + "</th><th  style='text-align:left'>" + $('#hdnToD').val() + "</th><th colspan='4' style='text-align:left'>" + ConvertDateAndReturnDate($('#hdnDateFromate').val(), todate) + "</th></tr>");
        }
        $("#tbldiv tbody").append(cc_);
     }
}

    function ConvertDateAndReturnDate(formates, dateText) {
        var hdnformat = formates;
        var txtMainDate = '';
        var splitc;
        if (hdnformat == 'd/m/Y' || hdnformat == 'm/d/Y' || hdnformat == 'Y/d/m' || hdnformat == 'Y/m/d' || hdnformat == 'M/d/Y' || hdnformat == 'd/M/Y') {
            splitc = '/';
        }
        else if (hdnformat == 'd-m-Y' || hdnformat == 'm-d-Y' || hdnformat == 'Y-d-m' || hdnformat == 'Y-m-d' || hdnformat == 'M-d-Y' || hdnformat == 'd-M-Y') {
            splitc = '-';
        }
        else if (hdnformat == 'd,m,Y' || hdnformat == 'm,d,Y' || hdnformat == 'Y,d,m' || hdnformat == 'Y,m,d' || hdnformat == 'M,d,Y' || hdnformat == 'd,M,Y') {

            splitc = ',';
        }
        else if (hdnformat == 'd m Y' || hdnformat == 'm d Y' || hdnformat == 'Y d m' || hdnformat == 'Y m d' || hdnformat == 'M d Y' || hdnformat == 'd M Y') {

            splitc = ' ';
        }
        else if (hdnformat == 'd.m.Y' || hdnformat == 'm.d.Y' || hdnformat == 'Y.d.m' || hdnformat == 'Y.m.d' || hdnformat == 'M.d.Y' || hdnformat == 'd.M.Y') {
            splitc = '.';
        };
        var sym = '-';
        if (dateText.indexOf('/') != -1)
            sym = '/';
        var vardatesplit = dateText.split(sym);
        var varM;
        var varMn;
        var varfformatS = hdnformat.split(splitc);
        if (varfformatS[0] == 'M' || varfformatS[1] == 'M') {

            if (vardatesplit[1] != null)
                varM = vardatesplit[1];
            if (vardatesplit[0] != null)
                varM = vardatesplit[0];

            switch (varM) {
                case 'Jan': varMn = '01';
                    break;
                case 'Feb': varMn = '02';
                    break;
                case 'Mar': varMn = '03';
                    break;
                case 'Apr': varMn = '04';
                    break;
                case 'May': varMn = '05';
                    break;
                case 'Jun': varMn = '06';
                    break;
                case 'Jul': varMn = '07';
                    break;
                case 'Aug': varMn = '08';
                    break;
                case 'Sep': varMn = '09';
                    break;
                case 'Oct': varMn = '10';
                    break;
                case 'Nov': varMn = '11';
                    break;
                case 'Dec': varMn = '12';
                    break;
            }

        }
        if (varfformatS[0] == 'd' && (varfformatS[1] == 'm' || varfformatS[1] == 'M') && varfformatS[2] == 'Y') {
            if (varfformatS[1] == 'M') {
                txtMainDate = varMn + splitc + vardatesplit[1] + splitc + vardatesplit[0];
            }
            else {
                txtMainDate = vardatesplit[2] + splitc + vardatesplit[1] + splitc + vardatesplit[0];
            }
        }
        else if ((varfformatS[0] == 'm' || varfformatS[0] == 'M') && varfformatS[1] == 'd' && varfformatS[2] == 'Y') {
            if (varfformatS[0] == 'M') {
                txtMainDate = varMn + splitc + vardatesplit[2] + splitc + vardatesplit[0];
            }
            else {
                txtMainDate = vardatesplit[1] + splitc + vardatesplit[2] + splitc + vardatesplit[0];
            }
        }
        else if (varfformatS[0] == 'Y' && varfformatS[1] == 'd' && varfformatS[2] == 'm') {
            txtMainDate = vardatesplit[0] + splitc + vardatesplit[2] + splitc + vardatesplit[1];
        }
        else if (varfformatS[0] == 'Y' && varfformatS[1] == 'm' && varfformatS[2] == 'd') {
            txtMainDate = vardatesplit[0] + splitc + vardatesplit[1] + splitc + vardatesplit[2];
        }
        else {
            txtMainDate = vardatesplit[0] + splitc + vardatesplit[1] + splitc + vardatesplit[2];
        }
        return txtMainDate;
    }
    function prevPRFirstInvl() {

        if (window.localStorage.getItem('RPPAGECODE') == 'POSINLSM') {
            PageInvoiceSummaryRPBind()
        }

    }
    function checkFromTodates() {
        var fromDate = window.localStorage.getItem('RPfrmDate');
        var toDate = window.localStorage.getItem('RPtoDate');
        if (Date.parse(toDate) < Date.parse(fromDate)) {
            return false;
        }
        else
            return true;

    }
    function prevPRInvl() {
        debugger;
        $('#hdnPaging').val(parseFloat($('#hdnPaging').val()) - 1);
        if ($('#txt_pageCount').val() == "") {
            $('#txt_pageCount').val('10');
            $('#hdnPaging').val('1');
        }
        if (parseFloat($('#hdnPaging').val()) == 0) {
            $('#hdnPaging').val('1');
        }
        if ($('#hdnTotalPagesCount').val() != $('#hdnPaging').val()) {
            $('#li_PRPrev i').removeAttr('style', 'color:#919598 !important');
            $('#li_PRPrev').attr('onclick', 'nextPRInvl()');
            $('#li_PRLast i').removeAttr('style', 'color:#919598 !important');
            $('#li_PRLast').attr('onclick', 'nextPRLastInvl()');
        }
        var btnExp = document.getElementById("btnInvExp");
        btnExp.style.display = "block";

        if (window.localStorage.getItem('RPPAGECODE') == 'POSINLSM') {
            var SubOrgId = '';
            var HashVal = (window.localStorage.getItem("hashValues"));
            var HashValues;
            if (HashVal != null && HashVal != '') {
                HashValues = HashVal.split('~');

                SubOrgId = HashValues[1];
                $('#hdnSuborgId').val(HashValues[1]);
                $('#hdnDateFromate').val(HashValues[2]);

            }

            var PageIndex = $('#hdnPaging').val();
            var PageCount = $('#txt_pageCount').val();
            var storename = HashValues[17].split('/');
            $.ajax({
                type: 'GET',
                url: CashierLite.Settings.GetReport_V01,
                data: "PageId=" + window.localStorage.getItem('RPPAGECODE') + "&subOrg=" + SubOrgId + "&frmDate=" + window.localStorage.getItem('RPfrmDate') + "&toDate=" + window.localStorage.getItem('RPtoDate') + ' 23:59:59' + "&refNo=" + window.localStorage.getItem('RPrefNo') + "&DateFormate=" + $('#hdnDateFromate').val() + "&VD=" + VD2() + "&PageIndex=" + PageIndex + "&PageCount=" + PageCount + "&Language=" + HashValues[7] + "",
                success: function (resp) {
                    graphData = resp;
                    if (resp != '') {
                        jQuery("label[for='lbl_invPageOf'").html(resp[0].C15);
                        $('#hdnTotalPagesCount').val(resp[0].C5);
                    }

                    getResources6(HashValues[7], "POSInvoiceSummaryReport", resp, 1, storename[1], window.localStorage.getItem('RPfrmDate'), window.localStorage.getItem('RPtoDate'));
                    $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());

                    if ($('#hdnTotalPagesCount').val() != $('#hdnPaging').val()) {
                        $('#li_PRPrev i').removeAttr('style', 'color:#919598 !important');
                        $('#li_PRPrev').attr('onclick', 'nextPRInvl()');
                        $('#li_PRLast i').removeAttr('style', 'color:#919598 !important');
                        $('#li_PRLast').attr('onclick', 'nextPRLastInvl()');
                    }
                    else {
                        $('#li_PRPrev i').attr('style', 'color:#919598 !important');
                        $('#li_PRPrev').attr('onclick', 'return false');
                        $('#li_PRLast').attr('onclick', 'return false');
                        $('#li_PRLast i').attr('style', 'color:#919598 !important');
                    }
                    if ($('#hdnPaging').val() == '1') {
                        $('#li_PRFirst').attr('onclick', 'return false');
                        $('#li_PRFirst i').attr('style', 'color:#919598 !important');
                        $('#li_PRNext i').attr('style', 'color:#919598 !important');
                        $('#li_PRNext').attr('onclick', 'return false');

                    }
                    else {
                        $('#li_PRFirst i').removeAttr('style', 'color:#919598 !important');
                        $('#li_PRFirst').attr('onclick', 'prevPRFirstInvl()');
                        $('#li_PRNext i').removeAttr('style', 'color:#919598 !important');
                        $('#li_PRNext').attr('onclick', 'prevPRInvl()');
                    }
                }

            });
        }

    }
    function nextPRInvl() {
        $('#hdnPaging').val(parseFloat($('#hdnPaging').val()) + 1);
        if ($('#txt_pageCount').val() == "") {
            $('#txt_pageCount').val('10');
            $('#hdnPaging').val('1');
        }
        var btnExp = document.getElementById("btnInvExp");
        btnExp.style.display = "block";
        if (window.localStorage.getItem('RPPAGECODE') == 'POSINLSM') {
            var SubOrgId = '';
            var HashVal = (window.localStorage.getItem("hashValues"));
            var HashValues;
            if (HashVal != null && HashVal != '') {
                HashValues = HashVal.split('~');

                SubOrgId = HashValues[1];
                $('#hdnSuborgId').val(HashValues[1]);
                $('#hdnDateFromate').val(HashValues[2]);

            }

            var PageIndex = $('#hdnPaging').val();
            var PageCount = $('#txt_pageCount').val();
            var storename = HashValues[17].split('/');
            $.ajax({
                type: 'GET',
                url: CashierLite.Settings.GetReport_V01,
                data: "PageId=" + window.localStorage.getItem('RPPAGECODE') + "&subOrg=" + SubOrgId + "&frmDate=" + window.localStorage.getItem('RPfrmDate') + "&toDate=" + window.localStorage.getItem('RPtoDate') + ' 23:59:59' + "&refNo=" + window.localStorage.getItem('RPrefNo') + "&DateFormate=" + $('#hdnDateFromate').val() + "&VD=" + VD() + "&PageIndex=" + PageIndex + "&PageCount=" + PageCount + "&Language=" + HashValues[7] + "",
                success: function (resp) {
                    graphData = resp;
                    if (resp != '') {
                        jQuery("label[for='lbl_invPageOf'").html(resp[0].C15);
                        // $('#hdnTotalPagesCount').val(resp[0].C6);
                        $('#hdnTotalPagesCount').val(resp[0].C5);
                    }

                    getResources6(HashValues[7], "POSInvoiceSummaryReport", resp, 1, storename[1], window.localStorage.getItem('RPfrmDate'), window.localStorage.getItem('RPtoDate'));
                    $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());

                    if ($('#hdnTotalPagesCount').val() != $('#hdnPaging').val()) {
                        $('#li_PRPrev i').removeAttr('style', 'color:#919598 !important');
                        $('#li_PRPrev').attr('onclick', 'nextPRInvl()');
                        $('#li_PRLast i').removeAttr('style', 'color:#919598 !important');
                        $('#li_PRLast').attr('onclick', 'nextPRLastInvl()');
                    }
                    else {
                        $('#li_PRPrev i').attr('style', 'color:#919598 !important');
                        $('#li_PRPrev').attr('onclick', 'return false');
                        $('#li_PRLast').attr('onclick', 'return false');
                        $('#li_PRLast i').attr('style', 'color:#919598 !important');
                    }
                    if ($('#hdnPaging').val() == '1') {
                        $('#li_PRFirst').attr('onclick', 'return false');
                        $('#li_PRFirst i').attr('style', 'color:#919598 !important');
                        $('#li_PRNext i').attr('style', 'color:#919598 !important');
                        $('#li_PRNext').attr('onclick', 'return false');

                    }
                    else {
                        $('#li_PRFirst i').removeAttr('style', 'color:#919598 !important');
                        $('#li_PRFirst').attr('onclick', 'prevPRFirstInvl()');
                        $('#li_PRNext i').removeAttr('style', 'color:#919598 !important');
                        $('#li_PRNext').attr('onclick', 'prevPRInvl()');
                    }
                }

            });
        }

        if ($('#hdnTotalPagesCount').val() == $('#hdnPaging').val()) {
            $('#li_PRPrev i').attr('style', 'color:#919598 !important');
            $('#li_PRPrev').attr('onclick', 'return false');
            $('#li_PRLast').attr('onclick', 'return false');
            $('#li_PRLast i').attr('style', 'color:#919598 !important');
        }
    }
    function loadG_MainData() {
        if ($('#hdnpageCode').val() == "POSINLSM") {
            var dateComaparison_ = checkFromTodates();
            if (dateComaparison_ == true) {
                window.localStorage.setItem('RPPAGECODE', 'POSINLSM')
                window.open('InvoiceSummaryReport.html', '_blank');
            }
            else
                dateValidationAlert();

        }
    }
    function loadInvoiceGrid() {
            $('#li-Excel i').removeAttr('style', 'color:#919598 !important');
            $('#li-Excel').attr('onclick', 'GetDowloadFile()');
            $('#li-Refresh i').removeAttr('style', 'color:#919598 !important');
            $('#li-Refresh').attr('onclick', 'PageInvoiceListingRPBind()');
            $('#li_expand i').removeAttr('style', 'color:#919598 !important');
            $('#li_expand').attr('onclick', 'loadInvoiceGridExtend()');
            $('#li_compress i').removeAttr('style', 'color:#919598 !important');
            $('#li_compress').attr('onclick', 'loadInvoiceGridCompress()');
            $('#li_Print i').removeAttr('style', 'color:#919598 !important');
            $('#li_Print').attr('onclick', 'Invprint()');
            $('#txt_PRPPageIndex').removeAttr('disabled', 'true');
            $('#btnPage').removeAttr('disabled', 'true');
            $('#li_PRFirst i').removeAttr('style', 'color:#919598 !important');
            $('#li_PRFirst').attr('onclick', 'prevPRFirstInvl()');
            $('#li_PRNext i').removeAttr('style', 'color:#919598 !important');
            $('#li_PRNext').attr('onclick', 'prevPRInvl()');
            $('#li_PRPrev i').removeAttr('style', 'color:#919598 !important');
            $('#li_PRPrev').attr('onclick', 'nextPRInvl()');
            $('#li_PRLast i').removeAttr('style', 'color:#919598 !important');
            $('#li_PRLast').attr('onclick', 'nextPRLastInvl()');
            $('#lbl_invPageOf').removeAttr('style', 'color:#919598 !important');
            $('#lbl_page').removeAttr('style', 'color:#919598 !important');
            if ($('#hdnTotalPagesCount').val() == $('#hdnPaging').val()) {
                $('#li_PRPrev i').attr('style', 'color:#919598 !important');
                $('#li_PRPrev').attr('onclick', 'return false');
                $('#li_PRLast').attr('onclick', 'return false');
                $('#li_PRLast i').attr('style', 'color:#919598 !important');
            }
        var elemGrid = document.getElementById("div_grid");
        elemGrid.style.display = "block";

        var elemGraph = document.getElementById("div_graph1");
        elemGraph.style.display = "none";
    }

