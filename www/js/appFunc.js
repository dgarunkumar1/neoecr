﻿
function VD1() {
    window.localStorage.setItem('ctCheck', 1);
    var vd_ = window.localStorage.getItem("UUID");

    return vd_;
}

var HashVal = (window.localStorage.getItem("hashValues"));
var HashValues;
if (HashVal != null && HashVal != '') {
    HashValues = HashVal.split('~');
    if (HashValues[7] == 'ar-SA') {
        $('html,body').append('<link rel="stylesheet" href="Styles/css/bootstrap-rtl.css" type="text/css" />');
        $('html,body').append('<link rel="stylesheet" href="Styles/css/bootstrap-flipped.css" type="text/css" />');
        $('head').append('<script src="scripts/bootstrap-dialog-SA.min.js"></script>')
        //$('head').append('<script src="scripts/ardatepicker.js"></script>')
    }

}
function CreditSalesReceipt()
{
 window.localStorage.setItem('ctCheck', 1);
 clearRSCRDData();
   $('#ModalCreditSaleReceipt').modal('show');
}
PageValidatebyPage1('POSCNTTYP');
function caluclClick() {
    window.localStorage.setItem('ctCheck', 1);
    var HashVal = (window.localStorage.getItem("hashValues"));
    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');
        if (HashValues[7] == 'ar-SA') {

            window.open("calculator-arsa.html", "_blank", "scrollbars=1,resizable=1,height=565,width=490");

        }
        else {
            window.open("calculator.html", "_blank", "scrollbars=1,resizable=1,height=565,width=490");

        }
    }
}
function PageValidatebyPage1(pagecode) {
    var LoginId = window.localStorage.getItem("Sno");
    window.localStorage.setItem('ctCheck', 1);
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.PageValidatebyPage,
        data: "uid=" + LoginId + "&CounterId=" + window.localStorage.getItem('COUNTERTYPE') + "&PageCode=" + pagecode + "&VD=" + VD1(),
        success: function (resp) {


            var s0_ = resp.split('~');
            var s_ = s0_[1];
            var s1_ = s0_[0];

            if (s1_ == '2') {
                window.localStorage.setItem("RoleId", "Admin");
                //s_ = '1';
                $('#hLinkTodaysCloseoutReport').hide();
                $('#hLinkTodaysSalesDetailsReport').hide();
                $('#hLinkTodayspItemsSummaryReport').hide();
                $('#hLinkTillOpening').show();
                $('#hLinkTillClosing').show();
                $('#hLinkAccountPosting').show();
            }
            else {
                window.localStorage.setItem("RoleId", "User");
                $('#hLinkTodaysCloseoutReport').show();
                $('#hLinkTodaysSalesDetailsReport').show();
                $('#hLinkTodayspItemsSummaryReport').show();
                $('#hLinkTillOpening').hide();
                $('#hLinkTillClosing').hide();
                $('#hLinkAccountPosting').hide();
            }
            var roleId = window.localStorage.getItem('RoleId');
            if (roleId == 'Admin') {
                $('#divGraphsForSales').show();
                $('#divSalesInformation').hide();
            } else {
                $('#divGraphsForSales').hide();
                $('#divSalesInformation').show();
            }
        }
    });
}
function theamLoad() {
    window.localStorage.setItem('ctCheck', 1);
    if (window.localStorage.getItem("Theam") != null)
        $('head').append('<link rel="stylesheet" href=' + window.localStorage.getItem("Theam") + ' type="text/css" />');
}
function btnTheam_Click1() {
    theamStroreDB('custom-style-1.css');
}
function btnTheam_Click2() {

    theamStroreDB('custom-style-2.css');
}
function btnTheam_Click3() {

    theamStroreDB('custom-style-3.css');

}
function btnTheam_Click4() {

    theamStroreDB('custom-style-4.css');
}
function fillpassword() {
    window.localStorage.setItem('ctCheck', 1);
    if (window.localStorage.getItem("uname") != 0) {
        TransportTrack.Session.getInstance().set({
            userId: window.localStorage.getItem("Sno"),
            userName: window.localStorage.getItem("uname"),

        });
        if (window.localStorage.getItem("RoleId") != 1) {

            document.getElementById('Username').value = window.localStorage.getItem("uname");
            document.getElementById('password').value = window.localStorage.getItem("pass");
            window.open('#', '_self', 'location=no');
        }
    }
    else {

    }
}
if (window.localStorage.getItem("Theam").split('-')[3] == '1.css') {
    $('#IMG1').attr("src", "images/mmlogo-inner2.png");
} else {
    $('#IMG1').attr("src", "images/mmlogo-inner1.png");
}
function locDash() {
    window.localStorage.setItem('ctCheck', 1);
    ConfigurationData();
    var HashVal = (window.localStorage.getItem("hashValues"));
    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');
        $('#hdnDateFromate').val(HashValues[2]);
        if (HashValues[7] == 'ar-SA') {

            $('head').append('<link rel="stylesheet" href="Styles/css/bootstrap-rtl.css" type="text/css" />');
            $('head').append('<link rel="stylesheet" href="Styles/css/bootstrap-flipped.css" type="text/css" />');
            $('head').append('<link rel="stylesheet" href="Styles/css/simple-sidebar-right-arabic.css" type="text/css" />');
            $('head').append('<script src="js/bootstrap-dialog-SA.min.js"></script>')
        }
        else {

            $('head').append('<script src="js/bootstrap-dialog.min.js"></script>')
        }
    }
    theamLoad();

    var sess = CashierLite.Session.getInstance().get();
    if (sess != null) {
        if (sess.userName != "") {
            document.getElementById("lblName").innerHTML = sess.userName;
        }
        else {
            window.open('Login.html', '_self', 'location=no');
        }
    }
    else {
        window.open('Login.html', '_self', 'location=no');
    }


    var m_names = new Array("Jan", "Feb", "Mar",
        "Apr", "May", "Jun", "Jul", "Aug", "Sep",
        "Oct", "Nov", "Dec");

    var d = new Date();
    var curr_date = (d.getDate() < 10 ? '0' : '') + d.getDate();
    var curr_month = ((d.getMonth() + 1) < 10 ? '0' : '') + (d.getMonth() + 1);
    var curr_year = d.getFullYear();
    var displayDate = ConvertDateWithFormatLocalRet($('#hdnDateFromate').val(), curr_month + "-" + curr_date + "-" + curr_year);
    $('#lblDateDH').text(displayDate);

    $('#lblToDayDate').text(curr_date + "-" + m_names[curr_month]
        + "-" + curr_year);
    var HashVal = (window.localStorage.getItem("hashValues"));
    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');
        getResourcesDash3(HashValues[7], "POSDashBoard");
    }
    GetShiftDataRTDRCHK();
    loadWeekGraphData();
    loadMonthGraphData();
    loadYearGraphData();
}



function loc() {
    window.localStorage.setItem('ctCheck', 1);
    theamLoad();
    var sess = CashierLite.Session.getInstance().get();
    if (sess != null) {
        if (sess.userName != "") {
            document.getElementById("lblName").innerHTML = sess.userName;
        }
        else {
            window.open('Login.html', '_self', 'location=no');
        }
    }
    else {
        window.open('Login.html', '_self', 'location=no');
    }


    var m_names = new Array("Jan", "Feb", "Mar",
        "Apr", "May", "Jun", "Jul", "Aug", "Sep",
        "Oct", "Nov", "Dec");

    var d = new Date();
    var curr_date = d.getDate();
    var curr_month = d.getMonth();
    var curr_year = d.getFullYear();
    var displayDate = ConvertDateAndReturnDate($('#hdnDateFromate').val(), curr_year + "-" + curr_month + 1 + "-" + curr_date);
    $('#lblDateDH').text(displayDate);
    GetShiftDataRTDRCHK();

}
function PageValidatebyPage(pagecode) {
    window.localStorage.setItem('ctCheck', 1);
    if (pagecode != '') {

        var LoginId = window.localStorage.getItem("Sno");
        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.PageValidatebyPage,
            data: "uid=" + LoginId + "&CounterId=" + window.localStorage.getItem('COUNTERTYPE') + "&PageCode=" + pagecode + "&VD=" + VD1(),
            success: function (resp) {

                var s_ = resp;

                var s0_ = resp.split('~');
                var s_ = s0_[1];
                var s1_ = s0_[0];

                if (s1_ == '2') {
                    window.localStorage.setItem("RoleId", "Admin");

                }
                else {
                    window.localStorage.setItem("RoleId", "User");
                }
                if (s_ == '1') {

                    if (pagecode == 'POSCUSTLDGR') {
                        getPOSPage('POSCUSTLDGR');
                    }
                    if (pagecode == 'POSINVCFG') {
                        window.open('POSPrintCustomize.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSBCODG') {
                        window.open('POSBarcodeGenerator.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSCNTTYP') {
                        window.open('POSCounterType.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSCOUDF') {
                        window.open('PosCounterDef.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSCTRPRM') {
                        window.open('POSCounterPermissions.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSCUSTM') {
                        window.open('CustomerMaster.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSTENDERTYPE') {
                        window.open('TenderTypes.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSLOYPRG') {
                        window.open('LoyaltyPoints.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSIORCR') {
                        GetShiftData();
                    }
                    if (pagecode == 'POSSLSRT') {
                        window.open('SalesReturn.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSDISPATCH') {
                        window.open('POSDeliveryStatusDetails.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSCOUOP') {
                        dayCloserChecking();
                    }
                    if (pagecode == 'POSCOUCL') {
                        window.open('CounterClosing.html', '_self', 'location=no');
                    }

                    if (pagecode == 'POSACPO') {

                        getPOSPage('POSACPO');
                    }
                    if (pagecode == 'POSRPTINVLST') {

                        getPOSPage('POSTINL');
                    }
                    if (pagecode == 'POSRPTSLRT') {
                        getPOSPage('POSSDSEN');
                    }
                    if (pagecode == 'POSRPTINVL') {
                        getPOSPage('POSINLS');
                    }
                    if (pagecode == 'POSRPTDCTN') {
                        getPOSPage('POSTTDS');
                    }
                    if (pagecode == 'POSRPTCINV') {
                        getPOSPage('POSCINV');
                    }
                    if (pagecode == 'POSRPTDE') {
                        window.open('POSReport.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSRPTSGRW') {
                        getPOSPage('POSSGR');
                    }
                    if (pagecode == 'POSConfigT') {
                        window.open('Configurator.html', '_self', 'location=no')
                    }
                    if (pagecode == 'POSRPTICSA') {
                        getPOSPage('POSRPTICSA');
                    }
                    if (pagecode == 'POSRPTBAR') {
                        getPOSPage('POSRPTBAR');
                    }
                    if (pagecode == 'POSRPTWDRR') {
                        getPOSPage('POSRPTWDRR');
                    }
                    if (pagecode == 'POSRPTLWATV') {
                        getPOSPage('POSRPTLWATV');
                    }
                    if (pagecode == 'POSRPTICPA') {
                        getPOSPage('POSRPTICPA');
                    }
                    if (pagecode == 'POSRPTLWPAS') {
                        getPOSPage('POSRPTLWPAS');
                    }
                    if (pagecode == 'POSTVRR') {
                        getPOSPage('POSTVRR');
                    }
                    if (pagecode == 'POSINLSM') {
                        getPOSPage('POSINLSM');
                    }
                }
                else {

                }
            },
            error: function (e) {
            }
        });

    }

}
function PageValidatebyPageDash(pagecode) {
    window.localStorage.setItem('ctCheck', 1);
    if (pagecode != '') {
        var LoginId = window.localStorage.getItem("Sno");

        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.PageValidatebyPage,
            data: "uid=" + LoginId + "&CounterId=" + window.localStorage.getItem('COUNTERTYPE') + "&PageCode=" + pagecode + "&VD=" + VD1(),
            success: function (resp) {

                var s0_ = resp.split('~');
                var s_ = s0_[1];
                var s1_ = s0_[0];
                if (s1_ == '2') {
                    window.localStorage.setItem("RoleId", "Admin");
                    //s_ = '1';
                }
                else {
                    window.localStorage.setItem("RoleId", "User");
                }
                if (s_ == '1') { 
                    if (pagecode == 'POSCUSTLDGR') {
                    getPOSPage('POSCUSTLDGR');
                    }
                    if (pagecode == 'POSINVCFG') {
                        window.open('POSPrintCustomize.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSBCODG') {
                        window.open('POSBarcodeGenerator.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSCNTTYP') {
                        window.open('POSCounterType.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSCOUDF') {
                        window.open('PosCounterDef.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSCTRPRM') {
                        window.open('POSCounterPermissions.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSCUSTM') {
                        window.open('CustomerMaster.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSTENDERTYPE') {
                        window.open('TenderTypes.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSLOYPRG') {
                        window.open('LoyaltyPoints.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSIORCR') {
                        GetShiftData();
                    }
                    if (pagecode == 'POSSLSRT') {
                        window.open('SalesReturn.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSDISPATCH') {
                        window.open('POSDeliveryStatusDetails.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSCOUOP') {
                        window.open('CounterOpening.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSCOUCL') {
                        window.open('CounterClosing.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSACPO') {
                        window.open('AccountPosting.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSRPTINVLST') {

                        getPOSPage('POSTINL');
                    }
                    if (pagecode == 'POSRPTSLRT') {
                        getPOSPage('POSSDSEN');
                    }
                    if (pagecode == 'POSRPTINVL') {
                        getPOSPage('POSINLS');
                    }
                    if (pagecode == 'POSRPTCINV') {
                        getPOSPage('POSCINV');
                    }
                    if (pagecode == 'POSRPTDCTN') {
                        getPOSPage('POSTTDS');
                    }
                    if (pagecode == 'POSRPTDE') {
                        window.open('POSReport.html', '_self', 'location=no');
                    }
                    if (pagecode == 'POSRPTSGRW') {
                        getPOSPage('POSSGR');
                    }
                    if (pagecode == 'POSRPTICSA') {
                        window.open('ItemCategorySalesAnalysis.html', '_self', 'location=no')
                    }
                    if (pagecode == 'POSRPTBAR') {
                        window.open('BulkAnalysisReport.html', '_self', 'location=no')
                    }
                    if (pagecode == 'POSRPTWDRR') {
                        window.open('POSWeekDaysRevenueRP.html', '_self', 'location=no')
                    }
                    if (pagecode == 'POSRPTLWATV') {
                        window.open('POSLocationwiseAvgTransactionValueRPT.html', '_self', 'location=no')
                    }
                    if (pagecode == 'POSRPTICPA') {
                        window.open('POSItemCategoryProfitAnalysis.html', '_self', 'location=no')
                    }
                    if (pagecode == 'POSRPTLWPAS') {
                        window.open('POSLocationwiseProfitAnalysis.html', '_self', 'location=no')
                    }
                }
                else {
                    navigator.notification.alert($('#hdnAccessDenied').val(), "", "neo ecr", "Ok");
                }
            },
            error: function (e) {
            }
        });

    }

}
function theamStroreDB(theamName) {
    window.localStorage.setItem('ctCheck', 1);
    if (window.localStorage.getItem('network') == '1') {
        var TheamVal = theamName;
        var LoginId = window.localStorage.getItem("Sno");
        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.LoginSetTheam,
            data: "uid=" + LoginId + "&theamval=" + TheamVal + "&VD=" + VD1(),
            success: function (resp) {
                navigator.notification.alert($('#hdnThameChange').val(), "", "neo ecr", "Ok");

            },
            error: function (e) {
            }
        });
    }
    else {
        navigator.notification.alert("can't Change Theme in offline network mode", "", "neo ecr", "Ok");
    }
}
var re;
function checkNetConnection() {
    window.localStorage.setItem('ctCheck', 1);
    $.ajax({
        type: 'POST',
        url: CashierLite.Settings.CheckConnectionExist,
        data: document.body,
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function (resp) {
 
            //return true;
        },
        error: function (resp) {
            alert('error')
            //return false;
        }
    });
}
var db;
var indexedDB;


function sendDisplayDataPass() {

    if (window.localStorage.getItem("DisplayDLTemp") == '0') {
        window.localStorage.setItem("PaidAmountDL", $('#lbl_TTpartTotal').text());
        window.localStorage.setItem("changeDL", $('#lblTenderConvertAmt').text());
    }
    if (window.localStorage.getItem("Display") != "2") {
        window.localStorage.setItem("Display", "0");
        var displayData_ = 'NoData';
        var myTab = document.getElementById('itemCarttable');
        if (myTab.rows.length > 0) {

            displayData_ = "";
            for (i = myTab.rows.length; i > 0; i--) {
                try {
                    window.localStorage.setItem("Display", "1");
                    var objCells = myTab.rows.item(i - 1).cells;
                    var displayItem = objCells.item(1).innerHTML;
                    var displayQty = objCells.item(2).innerHTML;
                    var displayPrice = objCells.item(4).innerHTML;
                    var displayAmount = $('#lblTenderConvertAmt').text();
                    displayData_ = displayData_ + displayItem + "~" + displayQty + "~" + displayPrice + "~" + displayAmount + "`"

                } catch (a)
                { }
            }
            window.localStorage.setItem("displayData", displayData_);
        }
        else {
            window.localStorage.setItem("Display", "0");
        }

    }
    if (window.localStorage.getItem("Display") == "2") {
        if (window.localStorage.getItem("DisplayDLTemp") == '0') {
            window.localStorage.setItem("PaidAmountDL", $('#lbl_TTpartTotal').text());
            window.localStorage.setItem("changeDL", $('#lblTenderConvertAmt').text());
        }
    }
    setTimeout('sendDisplayDataPass()', 1000);
}

function speakWelcome() {
    playAudio();
}
function playAudio() {
    if (window.localStorage.getItem("Play") == "0") {
        try {
            var HashVal = (window.localStorage.getItem("hashValues"));
            var HashValues;
            if (HashVal != null && HashVal != '') {
                HashValues = HashVal.split('~');
                if (HashValues[7] == 'ar-SA') {
                    var myMedia = new Media("/android_asset/www/audio/welcomearabic.mp3")
                    myMedia.play({ numberOfLoops: 1 })
                    window.localStorage.setItem("Play", "1");
                }
                else {
                    var myMedia = new Media("/android_asset/www/audio/welcome.mp3")
                    myMedia.play({ numberOfLoops: 1 })
                    window.localStorage.setItem("Play", "1");
                }
            }
        }
        catch (a) {

        }
    }
}

function MainData() { 
    window.localStorage.setItem('ctCheck', 1);
    window.localStorage.setItem("DisplayQR", "0");
    //ConfigurationData();
    indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB || window.shimIndexedDB;
    window.localStorage.setItem("Display", "0");
    setTimeout(sendDisplayDataPass(), 1000);
    var HashValues = (window.localStorage.getItem("hashValues"));
    //setTimeout(selectBarcode(), 1000);
    loc();
GetCustomers_();
    //GetItemDetails();
    fillShiftValues();
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        getResources1(hashSplit[7], "POSInvoice");
    }
    GetCounterTypeTenderData();
    GetTypeTenderDataCashby();
    var open = indexedDB.open("MyDatabase", 1);
    open.onupgradeneeded = function () {
        INdb = open.result;
        var store = INdb.createObjectStore("MyObjectStore", { keyPath: "id" });
        var index = store.createIndex("Invoices", "InvoiceData");
    };
    open.onsuccess = function (e) {
        INdb = open.result;
        var tx = INdb.transaction("MyObjectStore", "readonly");
        var store = tx.objectStore("MyObjectStore");
        var index = store.index("Invoices");

    }

    var SubOrgId = '';
    var CompanyId = '';
    var OBS = '';
    var Userid = '';
    var Lang = '';
    var Paging = ''; var SubIOrgId = '';

    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        SubOrgId = hashSplit[1] + ',' + hashSplit[16];
        CompanyId = hashSplit[0];
        SubIOrgId = hashSplit[1];
        OBS = hashSplit[9];
        Orgcode = hashSplit[17];
        Userid = hashSplit[14];
        Lang = hashSplit[7];
        Paging = hashSplit[19];
    }
    $('#CompanyId').val(CompanyId);
    $('#hdnBranchId').val(SubIOrgId);
    $('#hdnOBS').val(OBS);
    $('#hdnUserId').val(Userid);
    $('#txtPosCode').val(Orgcode);
    $('#hdnLanguage').val(Lang);
    $('#hdnPaging').val(Paging);
   
    ConfigurationData();
}
 


function failure() {

}
function connectSuccess() {


    try {
        var data = new Uint8Array(5);
        data[0] = 0x10;
        data[1] = 0x14;
        data[2] = 0x00;
        data[3] = 0x00;
        data[4] = 0x00;
        bluetoothSerial.write(data, success, failure);
    }
    catch (a) {

    }
}

function connectFailure() {
    try {
        bluetoothSerial.disconnect(success, failure);
        bluetoothSerial.list(function (devices) {
            var d_ = jQuery.parseJSON(JSON.stringify(devices));
            for (var i = 0; i < d_.length; i++) {
                if (d_[i].name == "InnerPrinter") {
                    bluetoothSerial.connect(d_[i].id, connectSuccess, connectFailure2);
                    break;
                }
            }
        }, failure);
    }
    catch (a) {

    }

}
function connectFailure3() {
    try {
        bluetoothSerial.disconnect(success, failure);
        bluetoothSerial.list(function (devices) {

            var d_ = jQuery.parseJSON(JSON.stringify(devices));
            for (var i = 0; i < d_.length; i++) {
                if (d_[i].name == "InnerPrinter") {
                    bluetoothSerial.connect(d_[i].id, connectSuccess, connectFailur);
                    break;
                }
            }
        }, failure);
    }
    catch (a) {

    }

}

function connectFailur() {
}

function connectFailure2() {
    try {
        bluetoothSerial.disconnect(success, failure);
        bluetoothSerial.list(function (devices) {
            var d_ = jQuery.parseJSON(JSON.stringify(devices));
            for (var i = 0; i < d_.length; i++) {
                if (d_[i].name == "InnerPrinter") {
                    bluetoothSerial.connect(d_[i].id, connectSuccess, connectFailure3);
                    break;
                }
            }
        }, failure);
    }
    catch (a) {

    }
}
function success() {



}
function successDisc() {

}
function printdemo() {

    try {

        bluetoothSerial.list(function (devices) {
            devices.forEach(function (device) {
                bluetoothSerial.connect(device.id, connectSuccess, connectFailure);

            })
        }, failure);

    }
    catch (a) {
      
    }
}
function GetStockCount() {

    var SubOrgId = '';

    var HashValues = (window.localStorage.getItem("hashValues"));

    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        SubOrgId = hashSplit[1] + ',' + hashSplit[16];

    }


    window.localStorage.setItem('ctCheck', 1);

    $.ajax({

        type: 'GET',

        url: CashierLite.Settings.GetStockCount,

        data: "subOrgId=" + SubOrgId + "&VD=" + VD1() + "&pageIndex=" + $('#hdnPageIndex').val(),

        success: function (resp) {

            if (resp != '') {

                var ItemsData = resp;

                document.getElementById("lblTotalSctockCount").innerHTML = resp;

            }

        }

    });

}


function GetCounterTypeTenderLocalDataBackClicktime() {

    window.localStorage.setItem('ctCheck', 1);
    window.localStorage.setItem("Display", "1");
    $('#txthdnActiveColumn').val('');
    $('#hdnCreditCardRefStatus').val('0');
    $('#divParent').show();
    $('#divRefNumber').hide();
    $('#txtRefNumber').val('');
    $('#hdnTemBasePricePlace').val('');
    $('#hdnTransModeforPay').val('');
    //$('#lblTendertype').text('');
    TenderRead('false', '');
    //$('#lblTenderConvertAmt').text('');
    $('#hdn_TenderTypeId').val('');
    if (window.localStorage.getItem('TenderDetails') != null) {

        document.getElementById('divTenderType').innerHTML = window.localStorage.getItem('TenderDetails');
    }
}

function GetCounterTypeTenderLocalData() {

    window.localStorage.setItem('ctCheck', 1);
    $('#txthdnActiveColumn').val('');
    $('#hdnCreditCardRefStatus').val('0');
    $('#divParent').show();
    $('#divRefNumber').hide();
    $('#txtRefNumber').val('');
    $('#hdnTemBasePricePlace').val('');
    $('#hdnTransModeforPay').val('');
    //$('#lblTendertype').text('');
    TenderRead('false', '');
    //$('#lblTenderConvertAmt').text('');
    $('#hdn_TenderTypeId').val('');
    if (window.localStorage.getItem('TenderDetails') != null) {

        document.getElementById('divTenderType').innerHTML = window.localStorage.getItem('TenderDetails');
    }
}


function GetCounterTypeTenderData() {
    window.localStorage.setItem('ctCheck', 1);
    var SubOrgId = '';
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        SubOrgId = hashSplit[1];
    }
    if (window.localStorage.getItem('network') == '1') {
        $.ajax({
            type: 'GET',
            async: false,
            url: CashierLite.Settings.GetCounterTypeTenderData,
            data: "CounterType=" + window.localStorage.getItem('COUNTERTYPE') + "&OrgId=" + SubOrgId + "&VD=" + VD1(),
            success: function (resp) {
                var ItemsData = resp;
                var li_ = "";
                var MainData = '';
                for (var i = 0; i < ItemsData.length; i++) {
                    if (ItemsData[i].TenderID.replace(' ', '') != 'CreditNote') {
                        if (ItemsData[i].TenderID.replace(' ', '') == 'Cash') {
                            li_ = li_ + "<li  id=\'li_" + ItemsData[i].TenderID.replace(' ', '') + "\' onclick=ontender(\'" + ItemsData[i].TenderID.replace(' ', '') + "\',\'" + ItemsData[i].TenderType.replace(' ', '') + "\',\'" + ItemsData[i].REFERENCE.replace(' ', '') + "\',\'" + ItemsData[i].SNO + "\');><img src='images/money.png' style='top: 40px;height: 42px;width: 42px;margin: 0px 8px 5px -37px;'>";
                            li_ = li_ + $('#hdnEcash').val() + "</li>";
                            /*if (hashSplit[7] == 'ar-SA') 
                                li_ = li_ + 'نقدي' + "</li>";
                            else
                                li_ = li_ + 'Cash' + "</li>";*/

                        }
                        if (ItemsData[i].TenderID.replace(' ', '') == 'CreditCard') {
                            li_ = li_ + "<li  id=\'li_" + ItemsData[i].TenderID.replace(' ', '') + "\' onclick=ontender(\'" + ItemsData[i].TenderID.replace(' ', '') + "\',\'" + ItemsData[i].TenderType.replace(' ', '') + "\',\'" + ItemsData[i].REFERENCE.replace(' ', '') + "\',\'" + ItemsData[i].SNO + "\');><img src='images/Card.png' style='height: 40px;top: 40px;width: 39px;margin: 0px 12px 6px -38px;'>";
                            li_ = li_ + $('#hdnECard').val() + "</li>";
                            /*if (hashSplit[7] == 'ar-SA')
                             
                                li_ = li_ + 'بطاقة' + "</li>";
                            else
                                li_ = li_ +'Card' + "</li>";*/
                        }
                        if (ItemsData[i].TenderID.replace(' ', '') == 'E-Pay') {
                            li_ = li_ + "<li  id=\'li_" + ItemsData[i].TenderID.replace(' ', '') + "\' onclick=ontender(\'" + ItemsData[i].TenderID.replace(' ', '') + "\',\'" + ItemsData[i].TenderType.replace(' ', '') + "\',\'" + ItemsData[i].REFERENCE.replace(' ', '') + "\',\'" + ItemsData[i].SNO + "\');><img src='images/digital-wallet.png' style='height: 42px;top: 40px;width: 42px;margin: 0px 8px 5px -20px;'>";
                            li_ = li_ + $('#hdnEIPay').val() + "</li>";
                            /* if (hashSplit[7] == 'ar-SA')
                                
                                 li_ = li_ + 'E-الدفع' + "</li>";
                             else
                                 li_ = li_ + 'E-wallet' + "</li>";*/
                        }
                        //li_ = li_ + "<li  id=\'li_" + ItemsData[i].TenderID.replace(' ', '') + "\' onclick=ontender(\'" + ItemsData[i].TenderID.replace(' ', '') + "\',\'" + ItemsData[i].TenderType.replace(' ', '') + "\',\'" + ItemsData[i].REFERENCE.replace(' ', '') + "\',\'" + ItemsData[i].SNO + "\');>";
                        //li_ = li_ + ItemsData[i].TenderID + "</li>";
                    }
                } for (var i = 0; i < ItemsData.length; i++) {
                    if (ItemsData[i].TenderID.replace(' ', '') == 'CreditNote') {
                        li_ = li_ + "<li  id=\'li_" + ItemsData[i].TenderID.replace(' ', '') + "\' onclick=ontender(\'" + ItemsData[i].TenderID.replace(' ', '') + "\',\'" + ItemsData[i].TenderType.replace(' ', '') + "\',\'" + ItemsData[i].REFERENCE.replace(' ', '') + "\',\'" + ItemsData[i].SNO + "\');><img src='images/CrNote.png' style='height: 42px;top: 40px;width: 42px;margin: 0px 8px 5px -9px;'>";
                        li_ = li_ + $('#hdnECNote').val() + "</li>";
                        /* if (hashSplit[7] == 'ar-SA')
                            
                             li_ = li_ + 'إشعار ائتمان' + "</li>";
                         else
                             li_ = li_ + 'Credit Note' + "</li>";*/

                    }
                      if (ItemsData[i].TenderID.replace(' ', '') == 'CreditSales') {
                        li_ = li_ + "<li  id=\'li_" + ItemsData[i].TenderID.replace(' ', '') + "\' onclick=ontender(\'" + ItemsData[i].TenderID.replace(' ', '') + "\',\'" + ItemsData[i].TenderType.replace(' ', '') + "\',\'" + ItemsData[i].REFERENCE.replace(' ', '') + "\',\'" + ItemsData[i].SNO + "\');><img src='images/CreditSale.png' style='height: 40px;top: 40px;width: 39px;margin: 0px 12px 6px -8px;'>";
                        li_ = li_ + $('#hdnECrSales').val()  + "</li>";
                    }
                    //if (ItemsData[i].TenderID.replace(' ', '') == 'CreditNote') {
                    //    li_ = li_ + "<li  id=\'li_" + ItemsData[i].TenderID.replace(' ', '') + "\' onclick=ontender(\'" + ItemsData[i].TenderID.replace(' ', '') + "\',\'" + ItemsData[i].TenderType.replace(' ', '') + "\',\'" + ItemsData[i].REFERENCE.replace(' ', '') + "\',\'" + ItemsData[i].SNO + "\');>";
                    //    li_ = li_ + ItemsData[i].TenderID + "</li>";
                    //}
                }
                document.getElementById('divTenderType').innerHTML = li_;
                window.localStorage.setItem("TenderDetails", li_);
                window.localStorage.setItem("OfflineTenderDetails", li_);
            },
            error: function (e) {
                SpinnerPlugin.activityStop();
            }
        });
    }
    else {
        document.getElementById('divTenderType').innerHTML = window.localStorage.getItem("OfflineTenderDetails");
        window.localStorage.setItem("TenderDetails", window.localStorage.getItem("OfflineTenderDetails"));

    }
}
function oneChangePPOne() {
    window.localStorage.setItem('ctCheck', 1);
    if ($('#hdnCreditCardRefStatus').val() == '1') {
        var strng = document.getElementById("txtRefNumber").value;
        document.getElementById("txtRefNumber").value = strng + "1";
        txtRefNumberKeyPress();
        return false;
    }

    if ($('#hdnTransModeforPay').val() != '') {
        if ($('#hdnTransModeforPay').val() == 'Cash') {
            if ($('#hdn_TenderTypeId').val() != '') {
                var strng = document.getElementById("txtTendertrans").value;
                document.getElementById("txtTendertrans").value = strng + "1";
                changeCals();
            }
            else {
                navigator.notification.alert($('#hdnSelCurType').val(), "", "neo ecr", "Ok");

            }
        }
        else {
            var strng = document.getElementById("txtTendertrans").value;
            document.getElementById("txtTendertrans").value = strng + "1";
            changeCals();
        }
        if ($('#hdnExchangerate').val() == '') {
            if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
                if ($('#hdnTenderSubType').val() != '')
                    tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), '1', '');
            }
            else {
                tenderAddpartial($('#hdnPayMode').val(), '1', '');
            }
        }
        else {
            if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
                if ($('#hdnTenderSubType').val() != '')
                    tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), $('#hdnExchangerate').val(), '');
            }
            else {

                tenderAddpartial($('#hdnPayMode').val(), $('#hdnExchangerate').val(), '');

            }
        }
    }
    else {
        navigator.notification.alert($('#hdnSelTranType').val(), "", "neo ecr", "Ok");
    }
    partialValBalnceCal();
    amtTenderCalc();
}
function oneChangePPTwo() {
    window.localStorage.setItem('ctCheck', 1);
    if ($('#hdnCreditCardRefStatus').val() == '1') {
        var strng = document.getElementById("txtRefNumber").value;
        document.getElementById("txtRefNumber").value = strng + "2";
        txtRefNumberKeyPress();
        return false;
    }
    if ($('#hdnTransModeforPay').val() != '') {
        if ($('#hdnTransModeforPay').val() == 'Cash') {
            if ($('#hdn_TenderTypeId').val() != '') {
                var strng = document.getElementById("txtTendertrans").value;
                document.getElementById("txtTendertrans").value = strng + "2";
                changeCals();
            }
            else {
                navigator.notification.alert($('#hdnSelCurType').val(), "", "neo ecr", "Ok");
            }
        }
        else {
            var strng = document.getElementById("txtTendertrans").value;
            document.getElementById("txtTendertrans").value = strng + "2"; changeCals();
        }

        if ($('#hdnExchangerate').val() == '') {
            if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
                if ($('#hdnTenderSubType').val() != '')
                    tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), '1', '');
            }
            else {
                tenderAddpartial($('#hdnPayMode').val(), '1', '');
            }
        }
        else {
            if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
                if ($('#hdnTenderSubType').val() != '')
                    tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), $('#hdnExchangerate').val(), '');
            }
            else {

                tenderAddpartial($('#hdnPayMode').val(), $('#hdnExchangerate').val(), '');

            }
        }
    }
    else {
        navigator.notification.alert($('#hdnSelTranType').val(), "", "neo ecr", "Ok");
    }
    partialValBalnceCal();
    amtTenderCalc();
}
function oneChangePPThree() {
    window.localStorage.setItem('ctCheck', 1);
    if ($('#hdnCreditCardRefStatus').val() == '1') {
        var strng = document.getElementById("txtRefNumber").value;
        document.getElementById("txtRefNumber").value = strng + "3";
        txtRefNumberKeyPress();
        return false;
    }
    if ($('#hdnTransModeforPay').val() != '') {
        if ($('#hdnTransModeforPay').val() == 'Cash') {
            if ($('#hdn_TenderTypeId').val() != '') {
                var strng = document.getElementById("txtTendertrans").value;
                document.getElementById("txtTendertrans").value = strng + "3";
                changeCals();
            }
            else {
                navigator.notification.alert($('#hdnSelCurType').val(), "", "neo ecr", "Ok");
            }
        }
        else {
            var strng = document.getElementById("txtTendertrans").value;
            document.getElementById("txtTendertrans").value = strng + "3"; changeCals();
        }

        if ($('#hdnExchangerate').val() == '') {
            if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
                if ($('#hdnTenderSubType').val() != '')
                    tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), '1', '');
            }
            else {
                tenderAddpartial($('#hdnPayMode').val(), '1', '');
            }
        }
        else {
            if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
                if ($('#hdnTenderSubType').val() != '')
                    tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), $('#hdnExchangerate').val(), '');
            }
            else {

                tenderAddpartial($('#hdnPayMode').val(), $('#hdnExchangerate').val(), '');

            }
        }
    }
    else {
        navigator.notification.alert($('#hdnSelTranType').val(), "", "neo ecr", "Ok");
    }
    partialValBalnceCal();
    amtTenderCalc();
}
function oneChangePPFour() {
    window.localStorage.setItem('ctCheck', 1);
    if ($('#hdnCreditCardRefStatus').val() == '1') {
        var strng = document.getElementById("txtRefNumber").value;
        document.getElementById("txtRefNumber").value = strng + "4";
        txtRefNumberKeyPress();
        return false;
    }

    if ($('#hdnTransModeforPay').val() != '') {
        if ($('#hdnTransModeforPay').val() == 'Cash') {
            if ($('#hdn_TenderTypeId').val() != '') {
                var strng = document.getElementById("txtTendertrans").value;
                document.getElementById("txtTendertrans").value = strng + "4";
                changeCals();
            }
            else {
                navigator.notification.alert($('#hdnSelCurType').val(), "", "neo ecr", "Ok");

            }
        }
        else {
            var strng = document.getElementById("txtTendertrans").value;
            document.getElementById("txtTendertrans").value = strng + "4"; changeCals();
        }

        if ($('#hdnExchangerate').val() == '') {
            //$('#hdnTenderSubType')
            if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
                if ($('#hdnTenderSubType').val() != '')
                    tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), '1', '');
            }
            else {
                tenderAddpartial($('#hdnPayMode').val(), '1', '');
            }
        }
        else {
            if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
                if ($('#hdnTenderSubType').val() != '')
                    tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), $('#hdnExchangerate').val(), '');
            }
            else {

                tenderAddpartial($('#hdnPayMode').val(), $('#hdnExchangerate').val(), '');

            }
        }
    }
    else {
        navigator.notification.alert($('#hdnSelTranType').val(), "", "neo ecr", "Ok");

    }
    partialValBalnceCal();
    amtTenderCalc();
}
function oneChangePPFive() {
    window.localStorage.setItem('ctCheck', 1);
    if ($('#hdnCreditCardRefStatus').val() == '1') {
        var strng = document.getElementById("txtRefNumber").value;
        document.getElementById("txtRefNumber").value = strng + "5";
        txtRefNumberKeyPress();
        return false;
    }

    if ($('#hdnTransModeforPay').val() != '') {
        if ($('#hdnTransModeforPay').val() == 'Cash') {
            if ($('#hdn_TenderTypeId').val() != '') {
                var strng = document.getElementById("txtTendertrans").value;
                document.getElementById("txtTendertrans").value = strng + "5";
                changeCals();
            }
            else {
                navigator.notification.alert($('#hdnSelCurType').val(), "", "neo ecr", "Ok");

            }
        }
        else {
            var strng = document.getElementById("txtTendertrans").value;
            document.getElementById("txtTendertrans").value = strng + "5"; changeCals();
        }

        if ($('#hdnExchangerate').val() == '') {
            //$('#hdnTenderSubType')
            if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
                if ($('#hdnTenderSubType').val() != '')
                    tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), '1', '');
            }
            else {
                tenderAddpartial($('#hdnPayMode').val(), '1', '');
            }
        }
        else {
            if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
                if ($('#hdnTenderSubType').val() != '')
                    tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), $('#hdnExchangerate').val(), '');
            }
            else {

                tenderAddpartial($('#hdnPayMode').val(), $('#hdnExchangerate').val(), '');

            }
        }
    }
    else {
        navigator.notification.alert($('#hdnSelTranType').val(), "", "neo ecr", "Ok");

    }
    partialValBalnceCal();
    amtTenderCalc();
}
function oneChangePPSix() {
    window.localStorage.setItem('ctCheck', 1);
    if ($('#hdnCreditCardRefStatus').val() == '1') {
        var strng = document.getElementById("txtRefNumber").value;
        document.getElementById("txtRefNumber").value = strng + "6";
        txtRefNumberKeyPress();
        return false;
    }

    if ($('#hdnTransModeforPay').val() != '') {
        if ($('#hdnTransModeforPay').val() == 'Cash') {
            if ($('#hdn_TenderTypeId').val() != '') {
                var strng = document.getElementById("txtTendertrans").value;
                document.getElementById("txtTendertrans").value = strng + "6";
                changeCals();
            }
            else {
                navigator.notification.alert($('#hdnSelCurType').val(), "", "neo ecr", "Ok");

            }
        }
        else {
            var strng = document.getElementById("txtTendertrans").value;
            document.getElementById("txtTendertrans").value = strng + "6"; changeCals();
        }

        if ($('#hdnExchangerate').val() == '') {
            //$('#hdnTenderSubType')
            if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
                if ($('#hdnTenderSubType').val() != '')
                    tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), '1', '');
            }
            else {
                tenderAddpartial($('#hdnPayMode').val(), '1', '');
            }
        }
        else {
            if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
                if ($('#hdnTenderSubType').val() != '')
                    tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), $('#hdnExchangerate').val(), '');
            }
            else {

                tenderAddpartial($('#hdnPayMode').val(), $('#hdnExchangerate').val(), '');

            }
        }
    }
    else {
        navigator.notification.alert($('#hdnSelTranType').val(), "", "neo ecr", "Ok");

    }
    partialValBalnceCal();
    amtTenderCalc();
}
function oneChangePPSeven() {
    window.localStorage.setItem('ctCheck', 1);

    if ($('#hdnCreditCardRefStatus').val() == '1') {
        var strng = document.getElementById("txtRefNumber").value;
        document.getElementById("txtRefNumber").value = strng + "7";
        txtRefNumberKeyPress();
        return false;
    }

    if ($('#hdnTransModeforPay').val() != '') {
        if ($('#hdnTransModeforPay').val() == 'Cash') {
            if ($('#hdn_TenderTypeId').val() != '') {
                var strng = document.getElementById("txtTendertrans").value;
                document.getElementById("txtTendertrans").value = strng + "7";
                changeCals();
            }
            else {
                navigator.notification.alert($('#hdnSelCurType').val(), "", "neo ecr", "Ok");

            }
        }
        else {
            var strng = document.getElementById("txtTendertrans").value;
            document.getElementById("txtTendertrans").value = strng + "7"; changeCals();
        }

        if ($('#hdnExchangerate').val() == '') {
            //$('#hdnTenderSubType')
            if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
                if ($('#hdnTenderSubType').val() != '')
                    tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), '1', '');
            }
            else {
                tenderAddpartial($('#hdnPayMode').val(), '1', '');
            }
        }
        else {
            if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
                if ($('#hdnTenderSubType').val() != '')
                    tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), $('#hdnExchangerate').val(), '');
            }
            else {

                tenderAddpartial($('#hdnPayMode').val(), $('#hdnExchangerate').val(), '');

            }
        }
    }
    else {
        navigator.notification.alert($('#hdnSelTranType').val(), "", "neo ecr", "Ok");

    }
    partialValBalnceCal();
    amtTenderCalc();
}
function oneChangePPEight() {
    window.localStorage.setItem('ctCheck', 1);
    if ($('#hdnCreditCardRefStatus').val() == '1') {
        var strng = document.getElementById("txtRefNumber").value;
        document.getElementById("txtRefNumber").value = strng + "8";
        txtRefNumberKeyPress();
        return false;
    }

    if ($('#hdnTransModeforPay').val() != '') {
        if ($('#hdnTransModeforPay').val() == 'Cash') {
            if ($('#hdn_TenderTypeId').val() != '') {
                var strng = document.getElementById("txtTendertrans").value;
                document.getElementById("txtTendertrans").value = strng + "8";
                changeCals();
            }
            else {
                navigator.notification.alert($('#hdnSelCurType').val(), "", "neo ecr", "Ok");

            }
        }
        else {
            var strng = document.getElementById("txtTendertrans").value;
            document.getElementById("txtTendertrans").value = strng + "8"; changeCals();
        }

        if ($('#hdnExchangerate').val() == '') {
            //$('#hdnTenderSubType')
            if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
                if ($('#hdnTenderSubType').val() != '')
                    tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), '1', '');
            }
            else {
                tenderAddpartial($('#hdnPayMode').val(), '1', '');
            }
        }
        else {
            if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
                if ($('#hdnTenderSubType').val() != '')
                    tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), $('#hdnExchangerate').val(), '');
            }
            else {

                tenderAddpartial($('#hdnPayMode').val(), $('#hdnExchangerate').val(), '');

            }
        }
    }
    else {
        navigator.notification.alert($('#hdnSelTranType').val(), "", "neo ecr", "Ok");

    }
    partialValBalnceCal();
    amtTenderCalc();
}
function oneChangePPNine() {
    window.localStorage.setItem('ctCheck', 1);
    if ($('#hdnCreditCardRefStatus').val() == '1') {
        var strng = document.getElementById("txtRefNumber").value;
        document.getElementById("txtRefNumber").value = strng + "9";
        txtRefNumberKeyPress();
        return false;
    }

    if ($('#hdnTransModeforPay').val() != '') {
        if ($('#hdnTransModeforPay').val() == 'Cash') {
            if ($('#hdn_TenderTypeId').val() != '') {
                var strng = document.getElementById("txtTendertrans").value;
                document.getElementById("txtTendertrans").value = strng + "9";
                changeCals();
            }
            else {
                navigator.notification.alert($('#hdnSelCurType').val(), "", "neo ecr", "Ok");

            }
        }
        else {
            var strng = document.getElementById("txtTendertrans").value;
            document.getElementById("txtTendertrans").value = strng + "9"; changeCals();
        }

        if ($('#hdnExchangerate').val() == '') {
            //$('#hdnTenderSubType')
            if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
                if ($('#hdnTenderSubType').val() != '')
                    tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), '1', '');
            }
            else {
                tenderAddpartial($('#hdnPayMode').val(), '1', '');
            }
        }
        else {
            if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
                if ($('#hdnTenderSubType').val() != '')
                    tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), $('#hdnExchangerate').val(), '');
            }
            else {

                tenderAddpartial($('#hdnPayMode').val(), $('#hdnExchangerate').val(), '');

            }
        }
    }
    else {
        navigator.notification.alert($('#hdnSelTranType').val(), "", "neo ecr", "Ok");

    }
    partialValBalnceCal();
    amtTenderCalc();
}
function oneChangePPZero() {
    window.localStorage.setItem('ctCheck', 1);
    if ($('#hdnCreditCardRefStatus').val() == '1') {
        var strng = document.getElementById("txtRefNumber").value;
        document.getElementById("txtRefNumber").value = strng + "0";
        txtRefNumberKeyPress();
        return false;
    }

    if ($('#hdnTransModeforPay').val() != '') {
        if ($('#hdnTransModeforPay').val() == 'Cash') {
            if ($('#hdn_TenderTypeId').val() != '') {
                var strng = document.getElementById("txtTendertrans").value;
                document.getElementById("txtTendertrans").value = strng + "0";
                changeCals();
            }
            else {
                navigator.notification.alert($('#hdnSelCurType').val(), "", "neo ecr", "Ok");
            }
        }
        else {
            var strng = document.getElementById("txtTendertrans").value;
            document.getElementById("txtTendertrans").value = strng + "0"; changeCals();
        }

        if ($('#hdnExchangerate').val() == '') {
            //$('#hdnTenderSubType')
            if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
                if ($('#hdnTenderSubType').val() != '')
                    tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), '1', '');
            }
            else {
                tenderAddpartial($('#hdnPayMode').val(), '1', '');
            }
        }
        else {
            if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
                if ($('#hdnTenderSubType').val() != '')
                    tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), $('#hdnExchangerate').val(), '');
            }
            else {

                tenderAddpartial($('#hdnPayMode').val(), $('#hdnExchangerate').val(), '');

            }
        }
    }
    else {
        navigator.notification.alert($('#hdnSelTranType').val(), "", "neo ecr", "Ok");

    }
    partialValBalnceCal();
    amtTenderCalc();
}
function oneChangePPDot() {
    window.localStorage.setItem('ctCheck', 1);
    if ($('#hdnCreditCardRefStatus').val() == '1') {
        var strng = document.getElementById("txtRefNumber").value;
        document.getElementById("txtRefNumber").value = strng + ".";
        txtRefNumberKeyPress();
        return false;
    }

    var strng = document.getElementById("txtTendertrans").value;
    document.getElementById("txtTendertrans").value = strng + ".";
    changeCals();
    //$('#txtItemSearch').focus();
    if ($('#hdnExchangerate').val() == '') {
        //$('#hdnTenderSubType')
        if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
            if ($('#hdnTenderSubType').val() != '')
                tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), '1', '');
        }
        else {
            tenderAddpartial($('#hdnPayMode').val(), '1', '');
        }
    }
    else {
        if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
            if ($('#hdnTenderSubType').val() != '')
                tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), $('#hdnExchangerate').val(), '');
        }
        else {

            tenderAddpartial($('#hdnPayMode').val(), $('#hdnExchangerate').val(), '');

        }
    }
    partialValBalnceCal();
    amtTenderCalc();
}
function oneChangePPDel() {

    window.localStorage.setItem('ctCheck', 1);
    if ($('#hdnCreditCardRefStatus').val() == '1') {

        var strng = document.getElementById("txtRefNumber").value;
        document.getElementById("txtRefNumber").value = strng.substring(0, strng.length - 1)
        txtRefNumberKeyPress();
        return false;
    }


    var strng = document.getElementById("txtTendertrans").value;
    document.getElementById("txtTendertrans").value = strng.substring(0, strng.length - 1)
    changeCals();
    //$('#txtItemSearch').focus();
    if ($('#hdnExchangerate').val() == '') {
        //$('#hdnTenderSubType')
        if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
            if ($('#hdnTenderSubType').val() != '')
                tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), '1', '');
        }
        else {
            tenderAddpartial($('#hdnPayMode').val(), '1', '');
        }
    }
    else {
        if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
            if ($('#hdnTenderSubType').val() != '')
                tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), $('#hdnExchangerate').val(), '');
        }
        else {

            tenderAddpartial($('#hdnPayMode').val(), $('#hdnExchangerate').val(), '');

        }
    }
    partialValBalnceCal();
    amtTenderCalc();
}


function ontender(TenderID, TenderType, REFERENCE, SNO) {

    window.localStorage.setItem('ctCheck', 1);
    window.localStorage.setItem("DisplayDLTemp0", "1");
    window.localStorage.setItem("Display", "2");

    window.localStorage.setItem("DisplayQR", "0");
    var a = valueAppendtoPOPUp();
    //var myTab = document.getElementById('itemCarttable');
    if (a == false) {
        return false;
    }

    $('#hdnCreditCardRefStatus').val('0');
    $('#divParent').show();
    $('#divRefNumber').hide();
    $('#hdnExchangerate').val('');
    $('#hdn_TenderTypeId').val('');
    $('#lblTenderConvertAmt').text(''); $('#lblTendertype').text('');
    document.getElementById('divTenderType').innerHTML = window.localStorage.getItem("TenderDetails");
    window.localStorage.setItem('PayModeInvoice', TenderID);
    TenderRead('true', '');
    $('#hdnTransModeforPay').val('');
    $('#txtTendertrans').val('');
    $('#txtTendertransref').val('');
    if (TenderID == 'Cash') {
        $('#txthdnActiveColumn').val('4');
        var elem = document.getElementById("lblTransamodetypeRef");
        var elemDraft = document.getElementById("txtTendertransref");
        elem.style.display = "block";
        elemDraft.style.display = "block";

        //$('#lblTransamodetype').text('Amount');
        $('#lblTransamodetype').text($('#hdnAmount').val());
        //$('#lblTransamodetypeRef').text('Change');
        $('#lblTransamodetypeRef').text($('#hdnChange').val());
        $('#hdnPayMode').val(TenderID);
        $('#hdnPayModeType').val(TenderType);
        $('#hdnTransModeforPay').val('Cash');
        var id_ = 'li_' + TenderID;
        TenderRead('false', '');

        GetTypeTenderDataCashbyLocal();
    }
    else if (TenderID.toUpperCase() == 'CREDITNOTE') {
        if (window.localStorage.getItem('network') == "1") {
            var valTy = '(' + $('#txtCurrency').val() + ')' + ' : ';
            $('#lblTendertype').text(valTy);
            $('#ModalCreditNote').modal('show');
            tenderAddpartial(TenderID, 1, '');
            $('#txtCreditNoteRefnoAmt').val('');
            $('#txtCreditNoteRefno').val('');
            $('#txtCreditNoteRefno').focus();
            $('#txtCreditNoteRefnoAmt').attr("readonly", "true");
        }
        else {
            navigator.notification.alert("Please Check Network", "", "neo ecr", "Ok")
        }

    }
    else if (TenderID.toUpperCase() == 'REDEMPOINTS') {
        var convertPoints = parseFloat(parseFloat(parseFloat(jQuery("label[for='lblRedeem_points'").html())) / parseFloat(jQuery("label[for='lblPointsObtainBasecurrency'").html())).toFixed(4);
        // $('#').val(convertPoints);
        jQuery("label[for='lbl_Amount'").html(convertPoints);
        if (parseFloat(jQuery("label[for='lblPointsObtainBasecurrency'").html()) > 0) {
            $('#ModalRedeem').modal('show');
            //
            //alert('REDEMPOINTS');
            tenderAddpartial("REDEEMPOINTS", 0, '');

        }
        else {
            navigator.notification.alert($('#hdnRewardPoints404').val(), "", "neo ecr", "Ok");

        }
    }
    else if (TenderID.toUpperCase() == 'REDEEMPOINTS') {
        var convertPoints = parseFloat(parseFloat(parseFloat(jQuery("label[for='lblRedeem_points'").html())) / parseFloat(jQuery("label[for='lblPointsObtainBasecurrency'").html())).toFixed(4);
        // $('#').val(convertPoints);
        jQuery("label[for='lbl_Amount'").html(convertPoints);
        if (parseFloat(jQuery("label[for='lblPointsObtainBasecurrency'").html()) > 0) {
            $('#ModalRedeem').modal('show');
            //
            //alert('REDEMPOINTS');
            tenderAddpartial("REDEEMPOINTS", 0, '');
        }
        else {
            navigator.notification.alert($('#hdnRewardPoints404').val(), "", "neo ecr", "Ok");

        }
    }
    else if (TenderID.toUpperCase() == 'GIFTVOUCHE') {
        $('#ModalGiftVouche').modal('show');
        tenderAddpartial('GIFTVOUCHER', 1, '');
        $('#txtGiftVoucheRefnoAmt').val('');
        $('#txtGiftVoucheRefno').val('');
        $('#txtGiftVoucheRefno').focus();
    }
    else if (TenderID.toUpperCase() == 'GIFTVOUCHER') {
        $('#ModalGiftVouche').modal('show');
        tenderAddpartial('GIFTVOUCHER', 1, '');
        $('#txtGiftVoucheRefnoAmt').val('');
        $('#txtGiftVoucheRefno').val('');
        $('#txtGiftVoucheRefno').focus();
    }
 else if (TenderType.toUpperCase() == 'POSCREDITSALES') {
        if ($('#hdn_CusId').val() == "") {
             navigator.notification.alert($('#hdnSelCust').val(), "", "neo ecr", "Ok");
        }
        else {

            var convertedVal = parseFloat($('#txtAmountpp').val());
            if (parseFloat(($('#lbl_TpartTotal')[0].innerText)) < 0) {
                var ChangeAMT_ = parseFloat(parseFloat(($('#lbl_TTpartTotal')[0].innerText)) - parseFloat(($('#txtAmountpp').val()))).toFixed(2);
                $('#txtTendertrans').val(ChangeAMT_.replace('-', ''));
                $('#hdnPayMode').val(TenderID);
                $('#hdnPayModeType').val(TenderType);
                tenderAddpartial(TenderID, 1, '');
                if (parseFloat(($('#lbl_TpartTotal')[0].innerText)) < 0) {
                    var ChangeAMT_ = parseFloat(parseFloat(($('#lbl_TTpartTotal')[0].innerText)) - parseFloat(($('#txtAmountpp').val()))).toFixed(2);

                    $('#txtTendertrans').val(ChangeAMT_.replace('-', ''));
                    changeCals();
                    if ($('#hdnExchangerate').val() == '') {
                        tenderAddpartial($('#hdnPayMode').val(), '1', '');
                    }
                    else {
                        tenderAddpartial($('#hdnPayMode').val(), $('#hdnExchangerate').val(), '');
                    }
                    partialValBalnceCal();
                    amtTenderCalc();
                }
                $('#divParent').show();
                $('#divRefNumber').hide();
                window.localStorage.setItem('ctCheck', 1);
            }
        }
    }

    else if (TenderType.toUpperCase().indexOf('CREDITCARD') != -1) {


        var valTy = '(' + $('#txtCurrency').val() + ')' + ' : ';
        $('#lblTendertype').text(valTy);
        var convertedVal = parseFloat($('#txtAmountpp').val());

        if ($('#hdnTemBasePricePlace').val() != '') {
            $('#lblTenderConvertAmt').text(parseFloat(convertedVal).toFixed(4));

        }
        else
            $('#lblTenderConvertAmt').text((parseFloat(convertedVal)).toFixed(4));



        $('#hdnTransModeforPay').val('Other');

        $('#hdnPayMode').val(TenderID);
        $('#hdnPayModeType').val(TenderType);
        $('#txtTendertransref').val('');
        //Arunkumar
        //$("#txtTendertrans").focus();
        tenderAddpartial(TenderID, 1, '');
        //GetTypeTenderDataCreditbyLocal();
        FullPaymentCredit();
    }
    else if (TenderType.toUpperCase().indexOf('DEBITCARD') != -1) {


        var valTy = '(' + $('#txtCurrency').val() + ')' + ' : ';
        $('#lblTendertype').text(valTy);
        var convertedVal = parseFloat($('#txtAmountpp').val());

        if ($('#hdnTemBasePricePlace').val() != '') {
            $('#lblTenderConvertAmt').text(parseFloat(convertedVal).toFixed(4));

        }
        else
            $('#lblTenderConvertAmt').text((parseFloat(convertedVal)).toFixed(4));


        $('#hdnTransModeforPay').val('Other');

        $('#hdnPayMode').val(TenderID);
        $('#hdnPayModeType').val(TenderType);
        $('#txtTendertransref').val('');
        //$("#txtTendertrans").focus();
        tenderAddpartial(TenderID, 1, '');
        //GetTypeTenderDataCreditbyLocal();
        FullPaymentCredit();
    }
    else if (TenderID == 'E-Pay') {
        var valTy = '(' + $('#txtCurrency').val() + ')' + ' : ';
        window.localStorage.setItem('CREPAY','0');
        $('#lblTendertype').text(valTy);
        $('#lbl_qrcodeName')[0].innerText = '';
        $("#img_QRCodePP").attr("src", "images/sampleQR.jpeg");
        $('#divWalletPayParams').attr("style", "display:none");

        $('#txtWalletNamePPSub').val('');
        $('#txtWalletBalancePPSub').val('');
        $('#txtWalletAmtPPSub').val('');
        $('#txtWalletRefPPSub').val('');
        $('#txt_amount_Ref').val('');
        $('#txt_amount_UPI').val('');
        $('#txt_balance_UPI').val('');
        $('#TXT_upiid').val('');
        $('#hdnWalletCheckVerify').val('0');
        $('#hdn_TYPEWallet').val('0');
        $('#home').attr('style', 'display:none');
        $('#wallet-pay').attr('style', 'display:none');
        $('#footer-pay').attr('style', 'display:none');
        $('#UPI-pay').attr('style', 'display:none');

        $('#div_form-section-UPI_UPI').attr('style', 'display:block');
        $('#myModal-payment').modal();
        var SubOrgId = '';
        var HashValues = (window.localStorage.getItem("hashValues"));
        if (HashValues != null && HashValues != '') {
            var hashSplit = HashValues.split('~');

            SubOrgId = hashSplit[1];

        }
        // GetConfigDataINaPPfUN(SubOrgId);
        GetConfigDataINaPPfUN_base64(SubOrgId);
    }
    else {
        var valTy = '(' + $('#txtCurrency').val() + ')' + ' : ';
        $('#lblTendertype').text(valTy);
        var convertedVal = parseFloat($('#txtAmountpp').val());

        if ($('#hdnTemBasePricePlace').val() != '') {
            $('#lblTenderConvertAmt').text(parseFloat(convertedVal).toFixed(4));

        }
        else
            $('#lblTenderConvertAmt').text((parseFloat(convertedVal)).toFixed(4));


        POLEDisplay("Total :" + (parseFloat(convertedVal)).toFixed(4));
        var id_ = 'li_' + TenderID;
        document.getElementById(id_).style.cssText = 'color: #fff;background-color: #2e6da4;font-weight:bold; border-color: #2e6da4;';

        $('#hdnTransModeforPay').val('Other');

        $('#hdnPayMode').val(TenderID);
        $('#hdnPayModeType').val(TenderType);
        $('#txtTendertransref').val('');
        $("#txtTendertrans").focus();
        tenderAddpartial(TenderID, 1, '');
    }
    partialValBalnceCal();
    amtTenderCalc();
}
function GetConfigDataINaPPfUN(SubOrgId) {
    window.localStorage.setItem('ctCheck', 1);
    $.ajax({
        url: CashierLite.Settings.GetConfigWalletData,
        data: { SubOrgId: SubOrgId, VD: VD() },
        dataType: "json",
        type: 'GET',
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.length != 0) {


                var li_ = ''; var liUPI_ = '';
                for (var i = 0; i < data.length; i++) {
                    var id_ = 'li_' + data[i].ID;
                    var logoimg = '';
                    if (data[i].LogoPath != "") {
                        logoimg = "<img src='" + data[i].LogoPath + "' style='width:30px;height:30px;' />";
                    }
                   
                    liUPI_ = liUPI_ + "<li onclick=ConvertWalletAmount_n2(\'" + data[i].QRPath + "\',\'" + data[i].Name.replace(/\s/g,'').trim() + "\',\'" + data[i].ID + "\',\'" + data[i].LogoPath + "\',\'" + data[i].Desc.replace(' ', '') + "\',\'" + data[i].SAQRID + "\',\'" + data[i].InstanceID + "\',\'" + data[i].Version + "\','',\'" + data[i].URPayStatus + "\') ><a href='#' id=" + data[i].ID + "> " + logoimg + "</a><small class ='desc'>" + data[i].Name + "</small></li>";
                } 
                document.getElementById('div_ul_walletData_n_upi').innerHTML = liUPI_;
             
            }
            else {

            }

        },
        error: function (e) {

        }
    });
}
function restrictDecm(tis) {
    var prev = tis.getAttribute("data-prev");
    prev = (prev != '') ? prev : '';
    if (Math.round(tis.value * 100) / 100 != tis.value)
        tis.value = prev;
    tis.setAttribute("data-prev", tis.value)
}
function GetConfigDataINaPPfUN_base64(SubOrgId) {
    // // alert(SubOrgId);
    // window.localStorage.setItem('ctCheck', 1);
    // var db = openDatabase('myEPAYDB', '1.0', 'CashierLiteDB', 2 * 1024 * 1024);
    // db.transaction(function (tx) {
    //     tx.executeSql('SELECT * FROM EPAYData', [], function (tx, results) {
      
    //         var data = results.rows;
    //         if (data.length > 0) {
    //             var li_ = ''; var liUPI_ = '';
    //             for (var i = 0; i < data.length; i++) {
    //                 var id_ = 'li_' + data[i].Id;
    //                 var logoimg = '';
    //                 var image = new Image();
    //                 if (data[i].WalletLogo != "") { 
    //                     image.src = 'data:image/png;base64,' + data[i].WalletLogo;
    //                     image.crossOrigin = 'Anonymous';
    //                     logoimg = "<img src='" + data[i].WalletLogo + "' style='width:30px;height:30px;' />";
    //                 } 
    //                 liUPI_ = liUPI_ + "<li onclick=ConvertWalletAmount_n2(\'" + data[i].QrUrl + "\',\'" + data[i].Name.replace(' ', '') + "\',\'" + data[i].Id + "\',\'" + data[i].WalletUrl + "\',\'" + data[i].Name + "\') ><img src='" + virtualDC + data[i].WalletUrl + "' style='width:30px;height:30px;' /> </br> <small class ='desc'>" + data[i].Name + "</small></li>";
    //                 document.getElementById('div_ul_walletData_n_upi').innerHTML = liUPI_;
    //             }
    //         }
    //     });
    // });
   
    window.localStorage.setItem('ctCheck', 1);
    var db = openDatabase('myEPAYDB', '1.0', 'CashierLiteDB', 2 * 1024 * 1024);
    db.transaction(function (tx) {
        tx.executeSql('SELECT * FROM EPAYData', [], function (tx, results) {
            var data = results.rows;
           
            if (data.length > 0) {
         
                var li_ = ''; var liUPI_ = '';
                for (var i = 0; i < data.length; i++) {
                
                    var id_ = 'li_' + data[i].Id;
                    var logoimg = '';
                    var image = new Image();
                    if (data[i].WalletLogo != "") {
                        image.src = 'data:image/png;base64,' + data[i].WalletLogo;
                        logoimg = "<img src='" + data[i].WalletLogo + "' style='width:30px;height:30px;' />";
                    } 
                    // liUPI_ = liUPI_ + "<li onclick=ConvertWalletAmount_n2(\'" + data[i].QRPath + "\',\'" + data[i].Name.replace(' ', '') + "\',\'" + data[i].ID + "\',\'" + data[i].LogoPath + "\',\'" + data[i].Desc.replace(' ', '') + "\',\'" + data[i].SAQRID + "\',\'" + data[i].InstanceID + "\',\'" + data[i].Version + "\','',\'" + data[i].URPayStatus + "\') ><a href='#' id=" + data[i].ID + "> " + logoimg + "</a><small class ='desc'>" + data[i].Name + "</small></li>";
            
                    liUPI_ = liUPI_ + "<li onclick=ConvertWalletAmount_n2(\'" + data[i].QRCode + "\',\'" + data[i].Name.replace(/\s/g,'').trim() + "\',\'" + data[i].Id + "\',\'" + data[i].WalletLogo + "\',\'" + data[i].Name.replace(/\s/g,'').trim() + "\',\'" + data[i].SAQRID + "\',\'" + data[i].InstanceID + "\',\'" + data[i].Version + "\','',\'" + data[i].UrPayStatus + "\') ><a href='#' id=" + data[i].Id + "> " + image.outerHTML + "</a><small class ='desc'>" + data[i].Name + "</small></li>";
                    // window.localStorage.setItem("walletData", li_);
                    // window.localStorage.setItem("walletDataUPI", liUPI_); 
                    // document.getElementById('div_ul_walletData_n1').innerHTML = li_; 
                    document.getElementById('div_ul_walletData_n_upi').innerHTML = liUPI_;
                    // document.getElementById('divWallet').innerHTML = li_;
                }
            }
        });
    });
}
function GetDynamicQR(id,balance,_SAQRID, _InstanceID, _Version, _Expairydate, _UrPayStatus) {
   
    var TaxAmt=(balance*0.13);
    SAQRData = [];
    var SAQ = {};
    SAQ["dateAndTime"] = _Expairydate;
    SAQ["additionalConsumerDataReq"] = "000";
    SAQ["CategoryCode"] = "5411";
    SAQ["CustomerLevel"] = "123456";
    SAQ["instanceID"] = _InstanceID;
    SAQ["RefNo"] = "1234";
    SAQ["SAQRid"] = _SAQRID;
    SAQ["TerminalLabel"] = "00002";
    SAQ["ArCity"] = HashValues[26].split(',').slice(-1)[0];
    SAQ["ArName"] = "FALCONCity";
    SAQ["billNo"] = "12345";
    SAQ["Dynamic"] = true;
    SAQ["fixedFee"] = "0";
    SAQ["loyeltyNo"] = "02";
    SAQ["masterCard"] = "";
    SAQ["merchantChannel"] = "";
    SAQ["merchantName"] = HashValues[10].split('/')[0].trim();
    SAQ["merchantCity"] = HashValues[26].split(',').slice(-1)[0];
    SAQ["merchantTaxID"] = HashValues[10];
    SAQ["mobileNo"] = "";
    SAQ["PercentFee"] = "";
    SAQ["PostalCode"] = "";

    SAQ["PurposeofTrans"] = "";
    SAQ["StoreLabel"] = HashValues[10].split('/')[0].trim();
    SAQ["tipOrConvenienceIndecatorUser"] = "0";
    SAQ["To"] = "";
    SAQ["Visa"] = "41111111";
    SAQ["unionPay"] = "";
    SAQ["TranVATamount"] = TaxAmt;

    SAQ["TransAmount"] = balance-TaxAmt;
    SAQ["Ver"] = _Version;
    SAQRData.push(SAQ);
    var saqrQrDataInst = new Object(SAQRData);
    saqrQrDataInst.AdditionalConsumerDataReq = "000";
    generateSAQRImage(saqrQrDataInst, id, 250, 'L');

}

function ConvertWalletAmount_n2(QRParth, Name, ID, imgPath, desc, SAQRID, InstanceID, Version, Expairydate, UrPayStatus) {
   
    window.localStorage.setItem('ctCheck', 1);
    var balance_ =0;
    balance_= $('#lbl_TpartTotal')[0].innerText.replace('-', '');
    if( window.localStorage.getItem('CREPAY')=='1')
    {
       balance_=$('#CRtxtAmount').val();
    }
    if (parseFloat(balance_) > 0) {
        $('#hdn_TYPEWallet').val('2');
        $('#hdnWalletCheckVerify').val('1');
        $('#wallet-pay,#form-section-wallet,#footer-pay').show();
        $('#form-section').hide(); 
        $("#img_QRCodePP_n2").attr("src",  'data:image/png;base64,' +imgPath);
   
     
        if (UrPayStatus == 'Y') { 
            GetDynamicQR("img_QRCodePP_New_n1",balance_,SAQRID, InstanceID, Version, Expairydate, UrPayStatus); 
            window.localStorage.setItem("DisplayQR", $("#img_QRCodePP_New_n1")[0].src); 
            // $("#img_QRCodePP_New_n1").attr("src", QRParth);
        }
        else {
            $("#img_QRCodePP_New_n1").attr("src",  'data:image/png;base64,' +QRParth);
            window.localStorage.setItem("DisplayQR",  'data:image/png;base64,' +QRParth);
        }
      
        //$('#lbl_qrcodeName_n1')[0].innerText = "UPI";
        $('#lbl_QRCOde_Mode_UPI')[0].innerText = desc;
        $('#lbl_qrcodeName_n2')[0].innerText = (Name);
        //$('#divWalletPayParams').attr("style", "display:block");
        var li_ = '';
        $('#hdnWalletCheckVerify').val('1');
        $('#txtWalletNamePPSub').val('UPI');
        $('#txt_balance_UPI').val(parseFloat(balance_).toFixed(2));
        $('#txt_amount_UPI').val(parseFloat(balance_).toFixed(2));
        $('#txt_amount_Ref').focus();
        try{ 
        $('#txtWalletNamePPSub').val(Name);
        $('#bhim-pay,#footer-pay,#QR-code-pay_2').show();
        $('#div_form-section-UPI_UPI').hide(); 
        }
        catch(a)
        {

        }
        QRShow2();
    }
    else {
       // var $textAndPic = $('<div></div>');
        //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        //$textAndPic.append('<div class="alert-message">Please Check Balance Amount</div>');
        //BootstrapDialog.alert($textAndPic);
         navigator.notification.alert($('#hdnPlChckBalAmt').val(), "", "neo ecr", "Ok");
    }
}
function QRShow() {
    window.localStorage.setItem('ctCheck', 1);
    $('#div_qrimg').show();
    $('#QR-code-pay_1').hide();
}
function QRShow2() {
    window.localStorage.setItem('ctCheck', 1);
    $('#div_qrimg_2').show();
    $('#QR-code-pay_2').hide();
}
function closeQRDisplay() {
    window.localStorage.setItem('ctCheck', 1);
    $('#myModalpay').modal('hide');
    $('#ModalWallet').modal('hide');
    $('#myModal-payment').modal('hide');
    window.localStorage.setItem("DisplayDLTemp0", "0");
    window.localStorage.setItem("DisplayDLTemp", "0");
    window.localStorage.setItem("Display", "1");
    window.localStorage.setItem("DisplayQR", "0");
}
function ConvertWalletAmount_n1(QRParth, Name, ID, imgPath, desc) {
    window.localStorage.setItem('ctCheck', 1);
    $('#hdn_TYPEWallet').val('1');
    $('#QR-code-pay_1').show();
    var balance_ = $('#lbl_TpartTotal')[0].innerText.replace('-', '');
    if (parseFloat(balance_) > 0) {
        $('#lbl_qrcodeName_n1')[0].innerText = Name;

        $("#img_QRCodePP_n1").attr("src", imgPath);
        $("#img_QRCodePP_New").attr("src", QRParth);
        window.localStorage.setItem("DisplayQR", QRParth);
        $('#lbl_QRCOde_Mode_wallet')[0].innerText = desc;

        var li_ = '';

        $('#hdnWalletCheckVerify').val('1');
        $('#txtWalletNamePPSub').val(Name);
        $('#txtWalletBalancePPSub').val(balance_);
        $('#txtWalletAmtPPSub').val(balance_);

        $('#txtWalletRefPPSub').focus();
        $('#bhim-pay,#footer-pay').show();
        $('#form-section-UPI,#QR-code-pay').hide();
        QRShow();
    }
    else {
       // var $textAndPic = $('<div></div>');
       // $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        //$textAndPic.append('<div class="alert-message">Please Check Balance Amount</div>');
        //BootstrapDialog.alert($textAndPic);
         navigator.notification.alert($('#hdnPlChckBalAmt').val(), "", "neo ecr", "Ok");
    }
}
function ConvertWalletAmount(QRParth, Name, ID) {

    window.localStorage.setItem('ctCheck', 1);
    var balance_ = $('#lbl_TpartTotal')[0].innerText.replace('-', '');
    if (parseFloat(balance_) > 0) {
        $('#lbl_qrcodeName')[0].innerText = Name;

        $("#img_QRCodePP").attr("src", QRParth);
        $('#divWalletPayParams').attr("style", "display:block");
        var li_ = '';

        li_ = li_ + "<li onclick=gotoWallet() style='color: #fff;background-color: #49a3f1;border-color: #49a3f1;width:100%;margin-bottom: 2px'> <i class='fa fa-angle-up'></i>&nbsp;&nbsp;&nbsp Back to Wallet</li>";
        $('#hdnWalletCheckVerify').val('1');
        document.getElementById('divWallet').innerHTML = li_;
        $('#txtWalletNamePPSub').val(Name);
        $('#txtWalletBalancePPSub').val(balance_);
        $('#txtWalletAmtPPSub').val(balance_);

        $('#txtWalletRefPPSub').focus();
    }
    else {
        navigator.notification.alert($('#hdnPlChckBalAmt').val(), "", "neo ecr", "Ok");

    }
}

function gotoWallet() {
    window.localStorage.setItem('ctCheck', 1);
    $('#hdnWalletCheckVerify').val('0');
    $('#divWalletPayParams').attr("style", "display:none");
    $('#lbl_qrcodeName')[0].innerText = '';
    $("#img_QRCodePP").attr("src", "images/sampleQR.jpeg");

    document.getElementById('divWallet').innerHTML = window.localStorage.getItem("walletData");
}
    function payviaWallet() 
    {
        window.localStorage.setItem('ctCheck', 1);
        if( window.localStorage.getItem('CREPAY')=='1')
        {
            CROnSave();
            $('#ModalWallet').modal('hide');
            $('#myModal-payment').modal('hide');
            window.localStorage.setItem("DisplayDLTemp0", "0");
            window.localStorage.setItem("DisplayDLTemp", "0");
            window.localStorage.setItem("Display", "1");
            window.localStorage.setItem("DisplayQR", "0");
        }
        else
        {
            var mode_ = $('#hdn_TYPEWallet').val();
            if (mode_ == '1') {


                var Name = $('#txtWalletNamePPSub').val();
                if ($('#hdnWalletCheckVerify').val() == '1') {

                    if (parseFloat($('#txtWalletAmtPPSub').val()) > 0) {
                        Name = 'E-Pay_Wallet-' + Name
                        var type = Name.replace(' ', '');
                        tenderAddpartial(Name, 1, '');
                        var myTab = document.getElementById('tbl_tenderDyms');
                        var m_ = ""; var lCheck_ = '0';
                        $('#hdnGiftVoucheRefId').val($('#txtWalletRefPPSub').val());
                        // LOOP THROUGH EACH ROW OF THE TABLE AFTER HEADER.
                        for (i = 0; i < myTab.rows.length; i++) {
                            // GET THE CELLS COLLECTION OF THE CURRENT ROW.
                            var objCells = myTab.rows.item(i).cells;
                            if (objCells.length > 0) {
                                // LOOP THROUGH EACH CELL OF THE CURENT ROW TO READ CELL VALUES.
                                if (type.toUpperCase() == objCells.item(1).innerHTML.toUpperCase()) {
                                    //var markup = "<td  style='width:5% !important' ><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right' >" + type.toUpperCase() + "</td><td>" + parseFloat(amount).toFixed(2) + "</td>";
                                    //$('#tr_' + type).html(markup);
                                    $('#tr_' + type.toUpperCase())[0].childNodes[2].innerText = parseFloat(1).toFixed(2);
                                    $('#tr_' + type.toUpperCase())[0].childNodes[3].innerText = parseFloat(parseFloat($('#txtWalletAmtPPSub').val())).toFixed(2);
                                    $('#tr_' + type.toUpperCase())[0].childNodes[4].innerText = parseFloat(parseFloat($('#txtWalletAmtPPSub').val()) * parseFloat(1)).toFixed(2);
                                    $('#tr_' + type.toUpperCase())[0].childNodes[5].innerText = $('#hdnGiftVoucheRefId').val();
                                    lCheck_ = '1';

                                    break;
                                }
                            }
                        }

                        partialValBalnceCal();
                        $('#ModalWallet').modal('hide');
                        $('#myModal-payment').modal('hide');
                        window.localStorage.setItem("DisplayDLTemp0", "0");
                        window.localStorage.setItem("DisplayDLTemp", "0");
                        window.localStorage.setItem("Display", "1");
                        window.localStorage.setItem("DisplayQR", "0");
                    }
                    else {
                        navigator.notification.alert($('#hdnPlsEnterAmount').val(), "", "neo ecr", "Ok");

                    }
                }
                else {
                    navigator.notification.alert($('#hdnPlSelatLeastOneWallet').val(), "", "neo ecr", "Ok");

                }
            }
            else if (mode_ == '2') {
                var Name = $('#txtWalletNamePPSub').val();
                if ($('#hdnWalletCheckVerify').val() == '1') {
                    if ($('#txt_amount_UPI').val() != '') {
                        //Name = 'UPI'
                        if (parseFloat($('#txt_amount_UPI').val()) > 0) {
                            Name = 'E-Pay_UPI-' + Name
                            var type = Name.replace(' ', '');
                            tenderAddpartial(Name, 1, '');
                            var myTab = document.getElementById('tbl_tenderDyms');
                            var m_ = ""; var lCheck_ = '0';
                            $('#hdnGiftVoucheRefId').val($('#txt_amount_Ref').val() + '~' + $('#TXT_upiid').val());
                            // LOOP THROUGH EACH ROW OF THE TABLE AFTER HEADER.
                            for (i = 0; i < myTab.rows.length; i++) {
                                // GET THE CELLS COLLECTION OF THE CURRENT ROW.
                                var objCells = myTab.rows.item(i).cells;
                                if (objCells.length > 0) {
                                    // LOOP THROUGH EACH CELL OF THE CURENT ROW TO READ CELL VALUES.
                                    if (type.toUpperCase() == objCells.item(1).innerHTML.toUpperCase()) {
                                        //var markup = "<td  style='width:5% !important' ><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right' >" + type.toUpperCase() + "</td><td>" + parseFloat(amount).toFixed(2) + "</td>";
                                        //$('#tr_' + type).html(markup);
                                        $('#tr_' + type.toUpperCase())[0].childNodes[2].innerText = parseFloat(1).toFixed(2);
                                        $('#tr_' + type.toUpperCase())[0].childNodes[3].innerText = parseFloat(parseFloat($('#txt_amount_UPI').val())).toFixed(2);
                                        $('#tr_' + type.toUpperCase())[0].childNodes[4].innerText = parseFloat(parseFloat($('#txt_amount_UPI').val()) * parseFloat(1)).toFixed(2);
                                        $('#tr_' + type.toUpperCase())[0].childNodes[5].innerText = $('#hdnGiftVoucheRefId').val();
                                        lCheck_ = '1';

                                        break;
                                    }
                                }
                            }

                            partialValBalnceCal();
                            $('#ModalWallet').modal('hide');
                            $('#myModal-payment').modal('hide');
                            window.localStorage.setItem("DisplayDLTemp0", "0");
                            window.localStorage.setItem("DisplayDLTemp", "0");
                            window.localStorage.setItem("Display", "1");
                            window.localStorage.setItem("DisplayQR", "0");
                        }
                        else {
                            navigator.notification.alert($('#hdnPlsEnterAmount').val(), "", "neo ecr", "Ok");

                        }
                    }
                    else {
                        navigator.notification.alert($('#hdnPlsEnterAmount').val(), "", "neo ecr", "Ok");

                    }
                }
                else {
                    navigator.notification.alert($('#hdnPlSelatLeastOneWallet').val(), "", "neo ecr", "Ok");

                }
        }
    }
}
function GiftVouche() {
    window.localStorage.setItem('ctCheck', 1);
    if (parseFloat($('#txtGiftVoucheRefnoAmt').val()) > 0) {
        var type = 'GIFTVOUCHER';
        var myTab = document.getElementById('tbl_tenderDyms');
        var m_ = ""; var lCheck_ = '0';
        $('#hdnGiftVoucheRefId').val($('#txtGiftVoucheRefno').val());
        // LOOP THROUGH EACH ROW OF THE TABLE AFTER HEADER.
        for (i = 0; i < myTab.rows.length; i++) {
            // GET THE CELLS COLLECTION OF THE CURRENT ROW.
            var objCells = myTab.rows.item(i).cells;
            if (objCells.length > 0) {
                // LOOP THROUGH EACH CELL OF THE CURENT ROW TO READ CELL VALUES.
                if (type.toUpperCase() == objCells.item(1).innerHTML.toUpperCase()) {
                    //var markup = "<td  style='width:5% !important' ><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right' >" + type.toUpperCase() + "</td><td>" + parseFloat(amount).toFixed(2) + "</td>";
                    //$('#tr_' + type).html(markup);
                    $('#tr_' + type.toUpperCase())[0].childNodes[2].innerText = parseFloat(1).toFixed(2);
                    $('#tr_' + type.toUpperCase())[0].childNodes[3].innerText = parseFloat(parseFloat($('#txtGiftVoucheRefnoAmt').val())).toFixed(2);
                    $('#tr_' + type.toUpperCase())[0].childNodes[4].innerText = parseFloat(parseFloat($('#txtGiftVoucheRefnoAmt').val()) * parseFloat(1)).toFixed(2);
                    $('#tr_' + type.toUpperCase())[0].childNodes[5].innerText = $('#hdnGiftVoucheRefId').val();
                    lCheck_ = '1';

                    break;
                }
            }
        }
        $('#txtGiftVoucheRefno').val('');
        $('#txtGiftVoucheRefnoAmt').val('');
        $('#hdnGiftVoucheRefId').val('');
        partialValBalnceCal();
        $('#ModalGiftVouche').modal('hide');
    }
    else {
        navigator.notification.alert($('#hdnPlsEnterAmount').val(), "", "neo ecr", "Ok");

    }
}
function txtRedeemPointskeyUP() {
    window.localStorage.setItem('ctCheck', 1);
    var points = (parseFloat(jQuery("label[for='lblPointsObtainBasecurrency'").html()));
    //txt_RedeemPointsAmt
    if ($('#txt_RedeemPoints').val() != '') {

        var convertPoints = parseFloat($('#txt_RedeemPoints').val()) / parseFloat(jQuery("label[for='lblPointsObtainBasecurrency'").html());
        $('#txt_RedeemPointsAmt').val(convertPoints);
    }
    else {
        $('#txt_RedeemPointsAmt').val('0');
    }
}
function RedeemPointsSet() {
    window.localStorage.setItem('ctCheck', 1);

    if (parseFloat(jQuery("label[for='lblRewardPoints'").html()) >= parseFloat($('#txt_RedeemPoints').val())) {

        if (parseFloat(jQuery("label[for='lblMinPoints'").html()) <= parseFloat($('#txt_RedeemPoints').val())) {
            if (parseFloat(jQuery("label[for='lblMaxPoints'").html()) >= parseFloat($('#txt_RedeemPoints').val())) {
                //
                var type = 'REDEEMPOINTS';
                var myTab = document.getElementById('tbl_tenderDyms');
                var m_ = ""; var lCheck_ = '0';
                // LOOP THROUGH EACH ROW OF THE TABLE AFTER HEADER.
                for (i = 0; i < myTab.rows.length; i++) {
                    // GET THE CELLS COLLECTION OF THE CURRENT ROW.
                    var objCells = myTab.rows.item(i).cells;
                    if (objCells.length > 0) {
                        // LOOP THROUGH EACH CELL OF THE CURENT ROW TO READ CELL VALUES.
                        if (type.toUpperCase() == objCells.item(1).innerHTML.toUpperCase()) {
                            //var markup = "<td  style='width:5% !important' ><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right' >" + type.toUpperCase() + "</td><td>" + parseFloat(amount).toFixed(2) + "</td>";
                            //$('#tr_' + type).html(markup);
                            $('#tr_' + type.toUpperCase())[0].childNodes[2].innerText = "1.00";
                            $('#tr_' + type.toUpperCase())[0].childNodes[3].innerText = parseFloat(parseFloat($('#txt_RedeemPointsAmt').val())).toFixed(2);
                            $('#tr_' + type.toUpperCase())[0].childNodes[4].innerText = parseFloat(parseFloat($('#txt_RedeemPointsAmt').val()) * parseFloat(1)).toFixed(2);
                            $('#tr_' + type.toUpperCase())[0].childNodes[5].innerText = $('#txt_RedeemPoints').val();
                            lCheck_ = '1';
                            break;
                        }
                    }
                }

                $('#txt_RedeemPointsAmt').val('');
                $('#txt_RedeemPoints').val('');
                partialValBalnceCal();
                $('#ModalRedeem').modal('hide');

            }
            else {
                navigator.notification.alert($('#hdnrewardptsLstrMinpts').val(), "", "neo ecr", "Ok");

            }
        }
        else {
            navigator.notification.alert($('#hdnrewardptsgrtrMinpts').val(), "", "neo ecr", "Ok");

        }
    }
    else {
        navigator.notification.alert($('#hdnrewardptsLstrMinpts').val(), "", "neo ecr", "Ok");


    }
}
function CrediNoteSet() {

    window.localStorage.setItem('ctCheck', 1);
    if ($('#hdnCreditCodeRefId').val().trim() != "") {
        var type = 'CREDITNOTE';
        var myTab = document.getElementById('tbl_tenderDyms');
        var m_ = ""; var lCheck_ = '0';
        // LOOP THROUGH EACH ROW OF THE TABLE AFTER HEADER.
        for (i = 0; i < myTab.rows.length; i++) {
            // GET THE CELLS COLLECTION OF THE CURRENT ROW.
            var objCells = myTab.rows.item(i).cells;
            if (objCells.length > 0) {
                // LOOP THROUGH EACH CELL OF THE CURENT ROW TO READ CELL VALUES.
                if (type.toUpperCase() == objCells.item(1).innerHTML.toUpperCase()) {
                    //var markup = "<td  style='width:5% !important' ><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right' >" + type.toUpperCase() + "</td><td>" + parseFloat(amount).toFixed(2) + "</td>";
                    //$('#tr_' + type).html(markup);
                    $('#tr_' + type.toUpperCase())[0].childNodes[2].innerText = parseFloat(1).toFixed(2);
                    $('#tr_' + type.toUpperCase())[0].childNodes[3].innerText = parseFloat($('#txtCreditNoteRefnoAmt').val()).toFixed(2);
                    $('#tr_' + type.toUpperCase())[0].childNodes[4].innerText = parseFloat(parseFloat($('#txtCreditNoteRefnoAmt').val()).toFixed(2) * parseFloat(1)).toFixed(2);
                    $('#tr_' + type.toUpperCase())[0].childNodes[5].innerText = $('#hdnCreditCodeRefId').val();
                    lCheck_ = '1';

                    break;
                }
            }
        }
        $('#txtCreditNoteRefno').val('');
        $('#txtCreditNoteRefnoAmt').val('');
        $('#hdnCreditCodeRefId').val('');
        //partialValBalnceCal();
        partialValBalnceCalForCreditNote();
    } else {
        navigator.notification.alert($('#hdnPlEnterRefNo').val(), "", "neo ecr", "Ok");
        return false;
    }
    $('#ModalCreditNote').modal('hide');
}
try {
    $("[id$='txtCreditNoteRefno']").autocomplete({

        source: function (request, response) {
            var HashValues = (window.localStorage.getItem("hashValues"));
            if (HashValues != null && HashValues != '') {
                var hashSplit = HashValues.split('~');
                SubOrgId = hashSplit[1];
            }
            var param = "passParms=" + $('#txtCreditNoteRefno').val() + "&SubOrgId=" + SubOrgId + "&VD=" + VD1();
            $.ajax({
                url: CashierLite.Settings.CreditNoteData,
                data: param,
                dataType: "json",
                type: "Get",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    window.localStorage.setItem('ctCheck', 1);
                    response($.map(data, function (item) {
                        return {
                            label: MMSplitDecode(item, 'Š')[1],
                            val: MMSplitDecode(item, 'Š')[0],
                            Amt: MMSplitDecode(item, 'Š')[2]
                        }
                    }))
                },
                error: function (response) {
                    //alert(response.responseText);
                },
                failure: function (response) {
                    //alert(response.responseText);
                }
            });

        },
        change: function (e, i) {
            if (i.item) {
                // do whatever you want to when the item is found
            }
            else {
                $('#txtCreditNoteRefno').val('');
                $('#txtCreditNoteRefnoAmt').val('');
                $('#hdnCreditCodeRefId').val('');
            }
        },
        select: function (e, i) {
            window.localStorage.setItem('ctCheck', 1);

            $('#txtCreditNoteRefnoAmt').val(parseFloat(i.item.Amt).toFixed(2));
            $('#hdnCreditCodeRefId').val(i.item.val);
        },
        minLength: 1
    });
}
catch (a) {

}
function vaaa() {
}
function changeCallKeyUp() {

    window.localStorage.setItem('ctCheck', 1);
    window.localStorage.setItem('ctCheck', 1);
    window.localStorage.setItem("DisplayDLTemp0", "1");
    window.localStorage.setItem("Display", "2");
    if ($('#hdnTransModeforPay').val() != '') {

        if ($('#hdnExchangerate').val() == '') {
            //$('#hdnTenderSubType')
            if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
                tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), '1', '');
            }
            else {
                tenderAddpartial($('#hdnPayMode').val(), '1', '');
            }
        }
        else {
            if ($('#hdnPayMode').val().toUpperCase() == "CASH") {
                tenderAddpartial($('#hdnPayMode').val() + '_' + $('#hdnTenderSubType').val(), $('#hdnExchangerate').val(), '');
            }
            else {

                tenderAddpartial($('#hdnPayMode').val(), $('#hdnExchangerate').val(), '');

            }
        }
    }
    else {
        navigator.notification.alert($('#hdnSelTranType').val(), "", "neo ecr", "Ok");

        $('#txtTendertrans').val('');
        return false;
    }
    partialValBalnceCal($('#hdnExchangerate').val());
    amtTenderCalc();
}

function changeCals() {

    window.localStorage.setItem('ctCheck', 1);
    if ($('#hdnTransModeforPay').val() == 'Cash') {

        var val_ = parseFloat($('#txtTendertrans').val()) - parseFloat($('#lblTenderConvertAmt').text());
        if (isNaN(val_)) {
            $('#txtTendertransref').val('');
        }
        else {
            $('#txtTendertransref').val(parseFloat(val_).toFixed(2));
        }
    }
    else {
        var val_ = parseFloat($('#txtTendertrans').val()) - parseFloat($('#lblTenderConvertAmt').text());
        if (isNaN(val_)) {
            $('#txtTendertransref').val('');
        }
        else
            $('#txtTendertransref').val(parseFloat(val_).toFixed(2));
    }

}
function TenderRead(bool, mode) {
    window.localStorage.setItem('ctCheck', 1);
    $('#txtTendertrans').val('');
    $('#txtTendertransref').val('');
    if (bool == 'true') {
        $('#txtTendertrans').removeAttr("readonly", "true");
        $("#txtTendertrans").removeAttr("disabled", "disabled");
    }
    else {
        $('#txtTendertrans').attr("readonly", "true");
        $("#txtTendertrans").attr("disabled", "disabled");
    }
    if (mode == 'cash') {

    }
}
function GetTypeTenderDataCashbyLocal() {
    window.localStorage.setItem('ctCheck', 1);
    document.getElementById('divTenderType').childNodes[0].nodeValue = null;
    document.getElementById('divTenderType').innerHTML = window.localStorage.getItem('liTenderType');

    $('#hdnTemBasePrice').val(window.localStorage.getItem('BaseCurrencyCode'));
    if ($('#hdnTemBasePrice').val() != '') {
        var id_ = 'li_' + $('#hdnTemBasePrice').val();
        document.getElementById(id_).style.cssText = 'color: #fff;background-color: #ff7276;font-weight:bold; border-color: #ff7276;';
        for (var m = 1; m < $('#divTenderType li').length; m++) {
            var liId_ = "#" + $('#divTenderType li')[m].id;
            $(liId_)[0].onclick = null;
        }

        $('#hdnTransModeforPay').val('Cash');
        var tenderType_ = $('#hdnTemBasePrice').val();
        var exchangerate_ = $('#hdnTemBasePriceEX').val();
        var tendeId_ = $('#hdnTemBasePriceTend').val();
        var CurrencyId_ = $('#hdnTemBasePriceCurId').val();

        ConverTenderAmountSub(tenderType_, exchangerate_, tendeId_, CurrencyId_);

        var myTab = document.getElementById('tbl_tenderDyms');
        var TenSubid_ = 'CASH_' + $('#hdnTemBasePrice').val().toUpperCase();
        tenderAddpartial(TenSubid_, 1, '');

        for (i = 0; i < myTab.rows.length; i++) {
            var objCells = myTab.rows.item(i).cells;
            if (objCells.length > 0) {
                if (objCells.item(1).innerHTML.toUpperCase() == TenSubid_) {
                    $('#txtTendertrans').val(objCells.item(3).innerHTML);
                }
            }
        }
        $('#txtTendertrans').val('');
    }
}
function GetTypeTenderDataCreditbyLocal() {
    window.localStorage.setItem('ctCheck', 1);
    document.getElementById('divTenderType').childNodes[0].nodeValue = null;

    var li_ = "<li style='color: #fff;background-color: #ffb9bb;border-color: #ffb9bb;' onClick=GetCounterTypeTenderLocalDataBackClicktime();><i class='fa fa-angle-up'></i> " + $('#hdnBack').val() + "</li>";

    li_ = li_ + "<li    onclick=FullPaymentCredit(); style='color: #fff;background-color: #ffb9bb;border-color: #ffb9bb;'>" + $('#hdnFullPayment').val() + "</li>";

    li_ = li_ + "<li  onclick=GetTypeTenderDataCreditbyLocal(); style='color: #fff;background-color: #ff7276;font-weight:bold; border-color: #ff7276;'>" + $('#hdnPartialPayment').val() + "</li>";
    li_ = li_ + "<li  onclick=FullPaymentRefNum(); style='color: #fff;background-color: #ffb9bb;border-color: #ffb9bb;'>" + $('#hdnReferenceNumber').val() + "</li>";


    document.getElementById('divTenderType').innerHTML = li_;
    $('#txtTendertrans').val('0');
    $('#txtTendertrans').focus();
    $('#hdnCreditCardRefStatus').val('0');
    //$('#txtTendertrans').val('');
    $('#divParent').show();
    $('#divRefNumber').hide();
    changeCallKeyUp();
    $('#txtTendertrans').val('');
}
function FullPaymentRefNum() {
    window.localStorage.setItem('ctCheck', 1);
    document.getElementById('divTenderType').childNodes[0].nodeValue = null;

    var li_ = "<li style='color: #fff;background-color: #ffb9bb;border-color: #ffb9bb;' onClick=GetCounterTypeTenderLocalDataBackClicktime();><i class='fa fa-angle-up'></i> " + $('#hdnBack').val() + "</li>";

    li_ = li_ + "<li    onclick=FullPaymentCredit(); style='color: #fff;background-color: #fb9a9c;border-color: #fb9a9c;'>" + $('#hdnFullPayment').val() + "</li>";

    li_ = li_ + "<li  onclick=GetTypeTenderDataCreditbyLocal(); style='color: #fff;background-color: #fb9a9c;border-color: #fb9a9c;'>" + $('#hdnPartialPayment').val() + "</li>";
    li_ = li_ + "<li  onclick=FullPaymentRefNum(); style='color: #fff;background-color: #ff7276;font-weight:bold; border-color: #ff7276;'>" + $('#hdnReferenceNumber').val() + "</li>";


    document.getElementById('divTenderType').innerHTML = li_;
    $('#divRefNumber').show();
    $('#divParent').hide();
    $('#txtRefNumber').focus();

    $('#hdnCreditCardRefStatus').val('1');
}

function txtRefNumberKeyPress() {
    window.localStorage.setItem('ctCheck', 1);
    var myTab = document.getElementById('tbl_tenderDyms');
    var m_ = ""; var lCheck_ = '0';
    for (i = 0; i < myTab.rows.length; i++) {
        var objCells = myTab.rows.item(i).cells;
        if (objCells.length > 0) {
            if ($('#hdnPayMode').val().toUpperCase() == objCells.item(1).innerHTML.toUpperCase()) {
                $('#tr_' + $('#hdnPayMode').val().toUpperCase())[0].childNodes[5].innerText = $('#txtRefNumber').val();
                break;
            }
        }

    }
}

function FullPaymentCredit() {

    window.localStorage.setItem('ctCheck', 1);
    document.getElementById('divTenderType').childNodes[0].nodeValue = null;

    var li_ = "<li style='color: #fff;background-color: #ffb9bb;border-color: #ffb9bb;' onClick=GetCounterTypeTenderLocalDataBackClicktime();><i class='fa fa-angle-up'></i> " + $('#hdnBack').val() + "</li>";

    li_ = li_ + "<li    onclick=FullPaymentCredit(); style='color: #fff;background-color: #ff7276;font-weight:bold; border-color: #ff7276;'>" + $('#hdnFullPayment').val() + "</li>";

    li_ = li_ + "<li  onclick=GetTypeTenderDataCreditbyLocal(); style='color: #fff;background-color: #fb9a9c;border-color: #fb9a9c;'>" + $('#hdnPartialPayment').val() + "</li>";
    li_ = li_ + "<li  onclick=FullPaymentRefNum(); style='color: #fff;background-color: #fb9a9c;border-color: #fb9a9c;'>" + $('#hdnReferenceNumber').val() + "</li>";


    document.getElementById('divTenderType').innerHTML = li_;


    if (parseFloat(($('#lbl_TpartTotal')[0].innerText)) < 0) {
        var ChangeAMT_ = parseFloat(parseFloat(($('#lbl_TTpartTotal')[0].innerText)) - parseFloat(($('#txtAmountpp').val()))).toFixed(2);

        $('#txtTendertrans').val(ChangeAMT_.replace('-', ''));
        changeCals();
        if ($('#hdnExchangerate').val() == '') {
            tenderAddpartial($('#hdnPayMode').val(), '1', '');
        }
        else {
            tenderAddpartial($('#hdnPayMode').val(), $('#hdnExchangerate').val(), '');
        }
        partialValBalnceCal();
        amtTenderCalc();
    }
    $('#divParent').show();
    $('#divRefNumber').hide();
    window.localStorage.setItem('ctCheck', 1);
}
function ConverTenderAmountSub(tenderType, exchangerate, tendeId, CurrencyId) {

    window.localStorage.setItem('ctCheck', 1);
    $('#hdnTemBasePricePlace').val(CurrencyId);
    $('#hdnExchangerate').val(exchangerate);
    document.getElementById('divTenderType').innerHTML = window.localStorage.getItem('liTenderType');
    var id_ = 'li_' + tenderType;
    document.getElementById(id_).style.cssText = 'color: #fff;background-color: #ff7276;font-weight:bold; border-color: #ff7276;';
    //'background-color: red; color: white; font-size: 44px';
    $('#hdnTransModeforPay').val('Cash');
    TenderRead('true', 'cash');
    var valTy = '(' + tenderType + ')' + ' : ';
    $('#lblTendertype').text(valTy);
    var convertedVal = parseFloat($('#txtAmountpp').val()) / parseFloat(exchangerate);

    if ($('#hdnTemBasePricePlace').val() != '') {
        $('#lblTenderConvertAmt').text(parseFloat(convertedVal).toFixed(2));

    }
    else
        $('#lblTenderConvertAmt').text((parseFloat(convertedVal)).toFixed(2));
    $('#hdn_TenderTypeId').val(tendeId);
    $("#txtTendertrans").focus();
    $('#hdnTenderSubType').val(tenderType);
    POLEDisplay("Total :" + (Math.round(convertedVal)).toFixed(2));

    //$('#divTenderType li').each(function () {    
    for (var m = 1; m < $('#divTenderType li').length; m++) {

        var liId_ = "#" + $('#divTenderType li')[m].id;
        $(liId_)[0].onclick = null;
        //$(liId_).prop("disabled", "disabled");
    }
    $('#txtTendertrans').focus();
    //});

}

function ConverTenderAmount(tenderType, exchangerate, tendeId, CurrencyId) {
    window.localStorage.setItem('ctCheck', 1);
    $('#hdnTemBasePrice').val(tenderType);
    $('#hdnTemBasePriceEX').val(exchangerate);
    $('#hdnTemBasePriceTend').val(tendeId);
    $('#hdnTemBasePriceCurId').val(CurrencyId);
    window.localStorage.setItem('ctCheck', 1);

    $('#hdnTemBasePricePlace').val(CurrencyId);
    $('#hdnExchangerate').val(exchangerate);
    document.getElementById('divTenderType').innerHTML = window.localStorage.getItem('liTenderType');
    var id_ = 'li_' + tenderType;
    document.getElementById(id_).style.cssText = 'color: #fff;background-color: #ff7276;font-weight:bold; border-color: #ff7276;';
    //'background-color: red; color: white; font-size: 44px';
    $('#hdnTransModeforPay').val('Cash');
    TenderRead('true', 'cash');
    var valTy = '(' + tenderType + ')' + ' : ';
    $('#lblTendertype').text(valTy);
    var convertedVal = parseFloat($('#txtAmountpp').val()) / parseFloat(exchangerate);
    if ($('#hdnTemBasePricePlace').val() != '') {
        $('#lblTenderConvertAmt').text(parseFloat(parseFloat($('#txtAmountpp').val()).toFixed(4) / parseFloat(exchangerate)).toFixed(2));

    }
    else
        $('#lblTenderConvertAmt').text((parseFloat(convertedVal)).toFixed(2));
    $('#hdn_TenderTypeId').val(tendeId);
    $("#txtTendertrans").focus();
    $('#hdnTenderSubType').val(tenderType);
    tenderAddpartial('cash_' + tenderType, exchangerate, tendeId);
    //alert(exchangerate);
    partialValBalnceCal();
    amtTenderCalc();
    for (var m = 1; m < $('#divTenderType li').length; m++) {

        var liId_ = "#" + $('#divTenderType li')[m].id;
        $(liId_)[0].onclick = null;
    }
    $('#txtTendertrans').focus();
}
function GetTypeTenderDataCashby() {
    window.localStorage.setItem('ctCheck', 1);
    var SubOrgId = '';
    var Arabicsymbol = '';
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        SubOrgId = hashSplit[1];
    }
    if (window.localStorage.getItem('network') == '1') {
        $.ajax({
            type: 'GET',
            async: false,
            url: CashierLite.Settings.GetTypeTenderDataCashby,
            data: "CounterType=" + window.localStorage.getItem('COUNTERTYPE') + "&OrgId=" + SubOrgId + "&VD=" + VD1(),
            success: function (resp) {
                var ItemsData = resp;
                var li_ = "";
                if (ItemsData.length > 0) {
                    var MainData = '';
                    li_ = li_ + "<li style='color: #fff;background-color: #ffb9bb;border-color: #ffb9bb;' onClick=GetCounterTypeTenderLocalDataBackClicktime();><i class='fa fa-angle-up'></i> " + $('#hdnBack').val() + "</li>";

                    for (var i = 0; i < ItemsData.length; i++) {
                        
                        var id_ = 'li_' + ItemsData[i].TENDERNAME;

                        //li_ = li_ + "<li id=" + id_ + "  onclick=ConverTenderAmount(\'" + ItemsData[i].TENDERNAME.replace(' ', '') + "\',\'" + ItemsData[i].ExchangeRate + "\',\'" + ItemsData[i].BankID + "\',\'" + ItemsData[i].CurrencyID + "\') style='color: #fff;background-color: #ffb9bb;border-color: #ffb9bb;'>" + ItemsData[i].TENDERNAME + "</li>";
                          li_ = li_ + "<li id=" + id_ + "  onclick=ConverTenderAmount(\'" + ItemsData[i].TENDERNAME.replace(' ', '') + "\',\'" + ItemsData[i].ExchangeRate + "\',\'" + ItemsData[i].BankID + "\',\'" + ItemsData[i].CurrencyID + "\') style='color: #fff;background-color: #ffb9bb;border-color: #ffb9bb;'>"+$('#hdnCurSymbol').val()+"</li>";
                    }
                    window.localStorage.setItem('BaseCurrencyCode', ItemsData[0].baseCurrency);
                    window.localStorage.setItem('liTenderType', li_);
                    window.localStorage.setItem('OfflineBaseCurrencyCode', ItemsData[0].baseCurrency);
                    window.localStorage.setItem('OfflineliTenderType', li_);

                }

            },
            error: function (e) {

            }

        });
    }
    else {
        window.localStorage.setItem('BaseCurrencyCode', window.localStorage.getItem('OfflineBaseCurrencyCode'));
        window.localStorage.setItem('liTenderType', window.localStorage.getItem('OfflineliTenderType'));
    }
}
function partialValBalnceCal(ExchangeVal) {
    window.localStorage.setItem('ctCheck', 1);
    var myTab = document.getElementById('tbl_tenderDyms');
    var mVal_ = "0";
    // LOOP THROUGH EACH ROW OF THE TABLE AFTER HEADER.
    for (i = 0; i < myTab.rows.length; i++) {
        // GET THE CELLS COLLECTION OF THE CURRENT ROW.
        var objCells = myTab.rows.item(i).cells;
        if (objCells.length > 0) {
            mVal_ = parseFloat(mVal_) + parseFloat(objCells.item(4).innerHTML);
        }
    }
    var valGet_ = parseFloat(mVal_) - (Math.round($('#txtAmountpp').val()) / parseFloat(ExchangeVal).toFixed(4));

    $('#lbl_TpartTotal')[0].innerText = Math.round(valGet_).toFixed(2);
    $('#txtTendertransref').val(parseFloat(valGet_).toFixed(4));
    if ($('#hdnTemBasePricePlace').val() != '') {
        $('#lblTenderConvertAmt').text(parseFloat(valGet_).toFixed(2));

    }
    else
        $('#lblTenderConvertAmt').text((parseFloat(valGet_)).toFixed(2));
}
function amtTenderCalc() {


    window.localStorage.setItem('ctCheck', 1);
    if (($('#hdnExchangerate').val() == '') || $('#hdnExchangerate').val() == '1') {
        var exc_ = 1;
        var balance_ = parseFloat($('#lbl_TpartTotal')[0].innerText);
        if ($('#hdnTemBasePricePlace').val() != '') {
            $('#lblTenderConvertAmt').text(parseFloat(balance_ / exc_).toFixed(2));

        }
        else
            $('#lblTenderConvertAmt').text((parseFloat(balance_ / exc_)).toFixed(2));
    }
    else {
        var exc_ = parseFloat($('#hdnExchangerate').val());
        var balance_ = parseFloat($('#lbl_TTpartTotal')[0].innerText) - parseFloat(Math.round($('#txtAmountpp').val()).toFixed(2));
        if ($('#hdnTemBasePricePlace').val() != '') {
            $('#lblTenderConvertAmt').text(parseFloat(balance_ / exc_).toFixed(2));

        }
        else
            $('#lblTenderConvertAmt').text((parseFloat(balance_ / exc_)).toFixed(2));
    }
}
function partialValBalnceCalForCreditNote() {
    window.localStorage.setItem('ctCheck', 1);
    var myTab = document.getElementById('tbl_tenderDyms');
    var mVal_ = "0";
    // LOOP THROUGH EACH ROW OF THE TABLE AFTER HEADER.
    for (i = 0; i < myTab.rows.length; i++) {
        // GET THE CELLS COLLECTION OF THE CURRENT ROW.
        var objCells = myTab.rows.item(i).cells;
        if (objCells.length > 0) {
            mVal_ = parseFloat(mVal_) + parseFloat(objCells.item(4).innerHTML);

        }
    }
    var valGet_ = 0;
    valGet_ = parseFloat(mVal_) - parseFloat($('#txtAmountpp').val());

    $('#lbl_TTpartTotal')[0].innerText = parseFloat(mVal_).toFixed(2);

    $('#lbl_TpartTotal')[0].innerText = parseFloat(valGet_).toFixed(4);
    $('#txtTendertransref').val(parseFloat(valGet_).toFixed(2));



    if ($('#hdnTemBasePricePlace').val() != '') {
        $('#lblTenderConvertAmt').text(parseFloat(valGet_).toFixed(2));

    }
    else
        $('#lblTenderConvertAmt').text((parseFloat(valGet_)).toFixed(2));

    //$('#lblTenderConvertAmt').text((Math.round(valGet_)).toFixed(2));

    var changeConverttoAmt_ = $('#lbl_TTpartTotal')[0].innerText;
    var txtAmountpp_ = $('#txtAmountpp').val();

    var changeConverttoEx_ = $('#hdnTemBasePriceEX').val();
    if (changeConverttoEx_ == "") {
        changeConverttoEx_ = '1';
    }

    if (changeConverttoEx_ == 1) {
        txtAmountpp_ = parseFloat(txtAmountpp_).toFixed(4);
        txtAmountpp_ = parseFloat(changeConverttoAmt_) - parseFloat(txtAmountpp_);
        $('#lblTenderConvertAmt')[0].innerText = parseFloat($('#lbl_TpartTotal')[0].innerText).toFixed(2);
    }
    else {
        txtAmountpp_ = parseFloat(txtAmountpp_).toFixed(4);
        txtAmountpp_ = parseFloat(changeConverttoAmt_) - parseFloat(txtAmountpp_);
        $('#txtTendertransref').val(parseFloat((parseFloat($('#lbl_TTpartTotal')[0].innerText) / parseFloat(changeConverttoEx_)) - (parseFloat($('#txtAmountpp').val()).toFixed(4) / parseFloat(changeConverttoEx_))).toFixed(2));

        var amtCheck_ = txtAmountpp_;
        $('#lblTenderConvertAmt')[0].innerText = parseFloat(parseFloat(amtCheck_).toFixed(4) / parseFloat(changeConverttoEx_)).toFixed(2);

    }



}
function partialValBalnceCal() {
    window.localStorage.setItem('ctCheck', 1);
    var myTab = document.getElementById('tbl_tenderDyms');
    var mVal_ = "0";
    // LOOP THROUGH EACH ROW OF THE TABLE AFTER HEADER.
    for (i = 0; i < myTab.rows.length; i++) {
        // GET THE CELLS COLLECTION OF THE CURRENT ROW.
        var objCells = myTab.rows.item(i).cells;
        if (objCells.length > 0) {
            mVal_ = parseFloat(mVal_) + parseFloat(objCells.item(4).innerHTML);
            mVal_ = parseFloat(mVal_).toFixed(2);
        }
    }
    var valGet_ = parseFloat(mVal_).toFixed(4) - parseFloat($('#txtAmountpp').val());
    $('#lbl_TTpartTotal')[0].innerText = parseFloat(mVal_).toFixed(2);

    $('#lbl_TpartTotal')[0].innerText = parseFloat(valGet_).toFixed(4);
    $('#txtTendertransref').val(parseFloat(valGet_).toFixed(2));



    if ($('#hdnTemBasePricePlace').val() != '') {
        $('#lblTenderConvertAmt').text(parseFloat(valGet_).toFixed(2));

    }
    else
        $('#lblTenderConvertAmt').text((parseFloat(valGet_)).toFixed(2));

    //$('#lblTenderConvertAmt').text((Math.round(valGet_)).toFixed(2));

    var changeConverttoAmt_ = $('#lbl_TTpartTotal')[0].innerText;
    var txtAmountpp_ = $('#txtAmountpp').val();

    var changeConverttoEx_ = $('#hdnTemBasePriceEX').val();
    if (changeConverttoEx_ == "") {
        changeConverttoEx_ = '1';
    }

    if (changeConverttoEx_ == 1) {
        txtAmountpp_ = parseFloat(txtAmountpp_).toFixed(4);
        txtAmountpp_ = parseFloat(changeConverttoAmt_) - parseFloat(txtAmountpp_);

        $('#lblTenderConvertAmt')[0].innerText = parseFloat($('#lbl_TpartTotal')[0].innerText).toFixed(2);
    }
    else {
        txtAmountpp_ = parseFloat(txtAmountpp_).toFixed(4);
        txtAmountpp_ = parseFloat(changeConverttoAmt_) - parseFloat(txtAmountpp_);
        $('#txtTendertransref').val(parseFloat((parseFloat($('#lbl_TTpartTotal')[0].innerText) / parseFloat(changeConverttoEx_)) - (parseFloat($('#txtAmountpp').val()).toFixed(4) / parseFloat(changeConverttoEx_))).toFixed(2));

        var amtCheck_ = txtAmountpp_;
        $('#lblTenderConvertAmt')[0].innerText = parseFloat(Math.round(amtCheck_).toFixed(2) / parseFloat(changeConverttoEx_)).toFixed(2);

    }
}
function tenderAddpartial(type, ExchangeVal, tenderId) {
    window.localStorage.setItem('ctCheck', 1);
    DuplicateTR(type, ExchangeVal, tenderId);
}
function DuplicateTR(type, ExchangeVal, tenderId) {
    window.localStorage.setItem('ctCheck', 1);
    var myTab = document.getElementById('tbl_tenderDyms');
    var m_ = ""; var lCheck_ = '0';
 // var _typeTender = '';
    // LOOP THROUGH EACH ROW OF THE TABLE AFTER HEADER.
    for (i = 0; i < myTab.rows.length; i++) {
        // GET THE CELLS COLLECTION OF THE CURRENT ROW.
        var objCells = myTab.rows.item(i).cells;
        if (objCells.length > 0) {
            // LOOP THROUGH EACH CELL OF THE CURENT ROW TO READ CELL VALUES.

/*if(type.toUpperCase()=="CASH_SAR")
            {
             _typeTender=$('#hdnEcash').val();
            }
       if(type.toUpperCase()=="CREDITCARD")
            {
             _typeTender=$('#hdnECard').val();
            }
        if(type.toUpperCase().split('_')[0]=="E-PAY")
            {
             _typeTender=$('#hdnEIPay').val();
            }
 if(type.toUpperCase().split('_')[0]=="CREDITNOTE")
            {
             _typeTender=$('#hdnECNote').val();
            }*/
            if (type.toUpperCase() == objCells.item(1).innerHTML.toUpperCase()) {

                $('#tr_' + type.toUpperCase())[0].childNodes[2].innerText = parseFloat(ExchangeVal).toFixed(4);
                if ($('#txtTendertrans').val() != "") {
                    $('#tr_' + type.toUpperCase())[0].childNodes[3].innerText = parseFloat($('#txtTendertrans').val()).toFixed(2);
                    $('#tr_' + type.toUpperCase())[0].childNodes[4].innerText = parseFloat(parseFloat($('#txtTendertrans').val()) * parseFloat(ExchangeVal)).toFixed(2);
                    lCheck_ = '1';
                }
                else {
                    lCheck_ = '1';
                }
                break;
            }
        }

    }
    if (lCheck_ == '0') {
        var CURRENCYID = '';
      
        var HashValues = (window.localStorage.getItem("hashValues"));
        if (HashValues != null && HashValues != '') {
            var hashSplit = HashValues.split('~');
            CURRENCYID = hashSplit[16];
        }

        if ($('#hdnTemBasePricePlace').val() != '') {
            CURRENCYID = $('#hdnTemBasePricePlace').val();
        }
        //var markup = "<tr id=" + 'tr_' + type.toUpperCase() + "><td style='width:5% !important'><img src='images/delete.png' onclick='deleteRowTr(this)'/></td><td align='left'>" + type.toUpperCase() + "</td><td style='display:none'  align='right'>" + parseFloat(ExchangeVal).toFixed(2) + "</td><td  align='right'  style='display:none'>0.00</td><td  align='right' style='text-align:end'>0.00</td><td style='display:none'>" + tenderId + "</td><td style='display:none'>" + CURRENCYID + "</td></tr>";
       // var markup = "<tr id=" + 'tr_' + type.toUpperCase() + "><td style='width:5% !important' class='tdClor'><i class='fa fa-trash' onclick='deleteRowTr(this)' style='font-size: 16px;'></i></td><td class='tdClor' align='left'>" + type.toUpperCase() + "</td><td style='display:none'   align='right' class='tdClor'>" + parseFloat(ExchangeVal).toFixed(2) + "</td><td  align='right'  style='display:none' class='tdClor'>0.00</td><td class='tdClor'  align='right' style='text-align:end'>0.00</td><td class='tdClor' style='display:none'>" + tenderId + "</td><td class='tdClor' style='display:none'>" + CURRENCYID + "</td></tr>";
/*if(type.toUpperCase()=="CASH_SAR")
            {
             _typeTender=$('#hdnEcash').val();
            }
       if(type.toUpperCase()=="CREDITCARD")
            {
             _typeTender=$('#hdnECard').val();
            }
        if(type.toUpperCase().split('_')[0]=="E-PAY")
            {
             _typeTender=$('#hdnEIPay').val();
            }
 if(type.toUpperCase().split('_')[0]=="CREDITNOTE")
            {
             _typeTender=$('#hdnECNote').val();
            }*/
        var markup = "<tr id=" + 'tr_' + type.toUpperCase() + "><td style='width:5% !important' class='tdClor'><i class='fa fa-trash' onclick='deleteRowTr(this)' style='font-size: 16px;'></i></td><td class='tdClor' align='left'>" + type.toUpperCase() + "</td><td style='display:none'   align='right' class='tdClor'>" + parseFloat(ExchangeVal).toFixed(2) + "</td><td  align='right'  style='display:none' class='tdClor'>0.00</td><td class='tdClor'  align='right' style='text-align:end'>0.00</td><td class='tdClor' style='display:none'>" + tenderId + "</td><td class='tdClor' style='display:none'>" + CURRENCYID + "</td></tr>";
        $("#tbl_tenderDyms tbody").append(markup);

    }
    window.localStorage.setItem('ctCheck', 1);
}
function deleteRowTr(r) {
    window.localStorage.setItem('ctCheck', 1);
    var i = r.parentNode.parentNode.rowIndex;

    var id_ = r.parentNode.parentNode.id;

    var tempId = 'tr_CASH_' + $('#hdnTemBasePrice').val().toUpperCase();
    if (id_ == tempId) {
        $('#hdnTemBasePrice').val('');
        $('#hdnTemBasePriceEX').val('1');
    }
    document.getElementById("tbl_tenderDyms").deleteRow(i);
    partialValBalnceCal();

    GetCounterTypeTenderLocalData();

}
function tenderRemovepartial(type) {

}

function fillShiftValues() {
    try {
        if (window.localStorage.getItem('Shift_Name') != null) {
            document.getElementById("lblShiftName").innerHTML = window.localStorage.getItem('Shift_Name');
            document.getElementById("lblCounterName").innerHTML = window.localStorage.getItem('COUNTERNAME');
            document.getElementById("lblStartTime").innerHTML = window.localStorage.getItem('Start_Time');
            document.getElementById("lblEndTime").innerHTML = window.localStorage.getItem('End_Time');
            // $('#txtItemScan').focus();
        }
    }
    catch (e) {

    }
    window.localStorage.setItem('ctCheck', 1);
}
function deleteRow(r) {
    window.localStorage.setItem('ctCheck', 1);
    var i = r.parentNode.parentNode.rowIndex;
    var mId = r.parentNode.parentNode.childNodes[5].innerText;
    var mIdSubLength = r.parentNode.parentNode.childNodes[12].innerText;
    if (parseFloat(mIdSubLength) > 0) {
        for (var m1_ = 1; m1_ < mIdSubLength + 1; m1_++) {
            var idC_ = '#tr_' + mId + "_" + (m1_ - 1);
            $(idC_).remove();
        }
    }


    var idC_ = '#tr_' + mId;
    $(idC_).remove();
    try {
        var mIdDisc = r.parentNode.parentNode.childNodes[6].innerText;
        var idCDisc_ = '#tr_' + mIdDisc;
        $("#tbl_tenderDyms tr").remove();
        $('#lbl_TTpartTotal')[0].innerText = "0.00";
        $(idC_).remove();
        $(idCDisc_).remove();
    }
    catch (a) {

    }

    $('#txthdnItemId').val('');
    $('#txtUOM').val('');
    $('#txtQuantity').val('');
    $('#txtPrice').val('');
    $('#txtAmount').val('');
    $('#txtQuantity').val('');
    $("#txtDiscount").val('');
    $("#txtAmount").attr("disabled", "disabled");
    $("#txtQuantity").attr("disabled", "disabled");
    $("#txtPrice").attr("disabled", "disabled");
    $("#txtUOM").attr("disabled", "disabled");
    $("#txtDiscount").attr("disabled", "disabled");
    GradCaluclation();

    window.localStorage.setItem("Display", "1");
    window.localStorage.setItem('ctCheck', 1);
}

function ClearFileds() {
    window.localStorage.setItem('ctCheck', 1);
    $("#txtUOM").val("");
    $("#txthdnActiveColumn").val("0");
    $('#txthdnItemId').val('');
    $('#txtQuantity').val('');
    $('#txtPrice').val('');
    $('#txtAmount').val('');
    $('#txtDiscount').val('');
    $("#txtAmount").attr("disabled", "disabled");
    $("#txtQuantity").attr("disabled", "disabled");
    $("#txtPrice").attr("disabled", "disabled");
    $("#txtUOM").attr("disabled", "disabled");
    $("#txtDiscount").attr("disabled", "disabled");

    document.getElementById('liQtyM').style.cssText = 'color:white;background: #FF7276;';
    document.getElementById('liDisM').style.cssText = 'color:white;background: #FF7276;';
    document.getElementById('liPriceM').style.cssText = 'color:white;background:#FF7276;';
    var myTab = document.getElementById('itemCarttable');
    for (i = 0; i < myTab.rows.length; i++) {
        var objCells = myTab.rows.item(i).cells;
        var v_ = 'tr_' + objCells.item(5).innerHTML;
        try {
            document.getElementById(v_).style.cssText = 'font-weight:normal';
        }
        catch (r) {
        }
    }
}

function tets() {
    window.localStorage.setItem('ctCheck', 1);
    var Data = "<!doctype html> <html> <head> <meta charset='utf-8'> <title>A simple, clean, and responsive HTML invoice template</title> <style> .invoice-box { max-width: 800px; margin: auto; padding: 30px; border: 1px solid #eee; box-shadow: 0 0 10px rgba(0, 0, 0, .15); font-size: 16px; line-height: 24px; font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; color: #555; } .invoice-box table { width: 100%; line-height: inherit; text-align: left; } .invoice-box table td { padding: 5px; vertical-align: top; } .invoice-box table tr td:nth-child(2) { text-align: right; } .invoice-box table tr.top table td { padding-bottom: 20px; } .invoice-box table tr.top table td.title { font-size: 45px; line-height: 45px; color: #333; } .invoice-box table tr.information table td { padding-bottom: 40px; } .invoice-box table tr.heading td { background: #eee; border-bottom: 1px solid #ddd; font-weight: bold; } .invoice-box table tr.details td { padding-bottom: 20px; } .invoice-box table tr.item td{ border-bottom: 1px solid #eee; } .invoice-box table tr.item.last td { border-bottom: none; } .invoice-box table tr.total td:nth-child(2) { border-top: 2px solid #eee; font-weight: bold; } @media only screen and (max-width: 600px) { .invoice-box table tr.top table td { width: 100%; display: block; text-align: center; } .invoice-box table tr.information table td { width: 100%; display: block; text-align: center; } } /** RTL **/ .rtl { direction: rtl; font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; } .rtl table { text-align: right; } .rtl table tr td:nth-child(2) { text-align: left; } </style> </head> <body> <div class='invoice-box'> <table cellpadding='0' cellspacing='0'> <tr class='top'> <td colspan='2'> <table> <tr> <td class='title'> <img src='images/mmlogo-inner1.png' style='width:100%; max-width:300px;'> </td> <td> Invoice #: 123<br> Created: January 1, 2015<br> Due: February 1, 2015 </td> </tr> </table> </td> </tr> <tr class='information'> <td colspan='2'> <table> <tr> <td> Sparksuite, Inc.<br> 12345 Sunny Road<br> Sunnyville, CA 12345 </td> <td> Acme Corp.<br> John Doe<br> john@example.com </td> </tr> </table> </td> </tr> <tr class='heading'> <td> Payment Method </td> <td> Check # </td> </tr> <tr class='details'> <td> Check </td> <td> 1000 </td> </tr> <tr class='heading'> <td> Item </td> <td> Price </td> </tr> <tr class='item'> <td> Website design </td> <td> $300.00 </td> </tr> <tr class='item'> <td> Hosting (3 months) </td> <td> $75.00 </td> </tr> <tr class='item last'> <td> Domain name (1 year) </td> <td> $10.00 </td> </tr> <tr class='total'> <td></td> <td> Total: $385.00 </td> </tr> </table> </div> </body> </html>"
    var PrintParm = new Object();
    PrintParm.Data = JSON.stringify(Data);

    var ComplexObject = new Object();
    ComplexObject.Data = PrintParm;
    var FinalData = JSON.stringify(ComplexObject);
    $.ajax({
        type: 'POST',
        url: HashValues[24] + '/DirectPrint',

        dataType: 'jsonp',
        contentType: "application/json; charset=utf-8",
        success: function () {
            return false;
        },
        error: function (e) {
        }
    });
}

function POLEDisplay(content) {
    window.localStorage.setItem('ctCheck', 1);
    var Data = content;
    $.ajax({
        type: 'GET',
        url: HashValues[24] + '/POLE',
        data: "content=" + content,
        dataType: 'JSONP',
        crossDomain: true,
        contentType: "application/json; charset=utf-8",
        success: function () {
            return false;
        },
        error: function (e) {
        }
    });
}
function onSubDataQTY(code, ItemId, Name, Price, Disc, UomID, UomCode, mapCode, Profile, qty_, img) {
    window.localStorage.setItem('ctCheck', 1);
    var passQTY = 1; var passPrice = 0; var baseCur_;
    var myTabChkQty = document.getElementById('itemCarttable');
    for (i = 0; i < myTabChkQty.rows.length; i++) {
        var objCells = myTabChkQty.rows.item(i).cells;
        if (ItemId == objCells.item(5).innerHTML) {
            passQTY = passQTY + parseFloat(objCells.item(2).innerHTML);
            passPrice = parseFloat(passPrice) + parseFloat(objCells.item(9).innerHTML);
        }
    }
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        SubOrg_Id = hashSplit[1];
        baseCur_ = hashSplit[16];
    }


    var strOptions1 = $('#txtCusName').val();
    var strOptions = $('#hdn_CusId').val();
    if ($('#txthdnRemoveStatus').val() == '1') {
        removeDraft(strOptions);
        $('#txthdnRemoveStatus').val('0');
    }
    if (strOptions == '') {
        strOptions = GenarateGustId() + '-01';
        strOptions1 = GenarateGustName();
    }

    var valItemCheck_ = $('#itemIdhiddenCheck').val();
    var _mainDiscChek = '';
    DuplicateQTYMODE(ItemId, Price, Disc, mapCode, Profile, _mainDiscChek, qty_, img);
    valItemCheck_ = $('#itemIdhiddenCheck').val();
    if (valItemCheck_ == '0') {
        var imgHost_ = img;
        if ((img == "") || img == undefined) {
            imgHost_ = "";
        }
        var markup = "<tr id=" + 'tr_' + ItemId + " onclick=ActiveRowget(\'" + ItemId + "\',\'" + Price + "\','1');><td style='width:5% !important' class='tdClor'><i class=fa fa-trash' onclick='deleteRow(this)' style='font-size: 16px;'></i></td><td align='right' class='tdClor'>" + Name + "</td><td class='tdClor'>1.00</td><td class='tdClor'>" + parseFloat(Disc).toFixed(2) + "</td><td style='display:none' >" + parseFloat(Price).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td class='tdClor'><input type='text' class='form-control'  value='" + Price + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + UomID + "</td><td style='display:none'>" + UomCode + "</td><td class='tdClor'>" + floorFigure_appFunc(parseFloat(Price), 2) + "</td><td style='display:none'>" + mapCode + "</td><td style='display:none'>" + Profile + "</td><td style='display:none'>0</td><td class='tdClor'>" + imgHost_ + "</td></tr>";
        $("#itemCarttable tbody").append(markup);
        POLEDisplay(Name + ':' + Price);
    }


    GradCaluclation();

    ActiveRowget(ItemId)
    $('#txtItemScan').val('');
    $('#scannerInput').val('');
    SubQtySubChange();
    $('#txtQuantity').val(qty_);

}
function onSubData(code, ItemId, Name, Price, Disc, UomID, UomCode, mapCode, Profile, img) {
    window.localStorage.setItem('ctCheck', 1);
    window.localStorage.setItem("DisplayDLTemp0", "0");
    window.localStorage.setItem("DisplayDLTemp", "0");
    window.localStorage.setItem("Display", "1");
    window.localStorage.setItem("DisplayQR", "0");
    window.localStorage.setItem("Display", "1");
    $("#tbl_tenderDyms tr").remove();
    $('#lbl_TTpartTotal')[0].innerText = "0.00";

    var passQTY = 1; var passPrice = Price; var baseCur_;
    var myTabChkQty = document.getElementById('itemCarttable');
    for (i = 0; i < myTabChkQty.rows.length; i++) {
        var objCells = myTabChkQty.rows.item(i).cells;
        if (ItemId == objCells.item(5).innerHTML) {
            passQTY = passQTY + parseFloat(objCells.item(2).innerHTML);
            passPrice = parseFloat(passPrice) + parseFloat(objCells.item(9).innerHTML);
        }
    }

    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        SubOrg_Id = hashSplit[1];
        baseCur_ = hashSplit[16];
    }

    var SubOrg = {
    };
    window.localStorage.setItem('ctCheck', 1);
    SubOrg.SubOrgID = $('#hdnBranchId').val();
    //  SubOrg.VD = vd_;
    if (window.localStorage.getItem('network') == '1') {
        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.GetItemSchema,
            data: "ItemId=" + ItemId + "&SubOrg=" + SubOrg_Id + "&QTY=" + passQTY + "&PRICE=" + passPrice + "&BaseCur=" + baseCur_ + "&VD=" + VD1(),
            success: function (resp) {
                txt = '';
                var mainData_ = resp;
                resp = resp[0].split('~');

                if (resp[0] != 'NODATA') {

                    if (resp[0] == 'ADDEXTRAITEM') {
                        var strOptions1 = $('#txtCusName').val();
                        var strOptions = $('#hdn_CusId').val();
                        if ($('#txthdnRemoveStatus').val() == '1') {
                            removeDraft(strOptions);
                            $('#txthdnRemoveStatus').val('0');
                        }
                        if (strOptions == '') {
                            strOptions = GenarateGustId() + '-01';
                            strOptions1 = GenarateGustName();
                        }

                        var valItemCheck_ = $('#itemIdhiddenCheck').val();

                        Duplicate(ItemId, Price, Disc, mapCode, Profile, mainData_, '1', img);
                        valItemCheck_ = $('#itemIdhiddenCheck').val();
                        if (valItemCheck_ == '0') {
                            var discPrice_ = Price;
                            if (mainData_.length > 0) {
                                var resp_ = mainData_[0].split('~');
                                if (resp_.length > 10) {
                                    var Mresp_ = resp_[12].split('$');
                                    if (Mresp_.length > 1) {
                                        Disc = Mresp_[0];
                                        discPrice_ = Mresp_[3];
                                    }
                                }
                            }
                            //var markup = "<tr id=" + 'tr_' + ItemId + " onclick=ActiveRowget(\'" + ItemId + "\',\'" + Price + "\','1');><td style='width:5% !important'><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right'>" + Name + "</td><td>1.00</td><td>" + parseFloat(Disc).toFixed(2) + "</td><td style='display:none' >" + parseFloat(discPrice_).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td><input type='text' class='form-control'  value='" + Price + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + UomID + "</td><td style='display:none'>" + UomCode + "</td><td>" + parseFloat(Price).toFixed(2) + "</td><td style='display:none'>" + mapCode + "</td><td style='display:none'>" + Profile + "</td><td  style='display:none'>" + mainData_.length + "</td></tr>";
                            var markup = "<tr id=" + 'tr_' + ItemId + " onclick=ActiveRowget(\'" + ItemId + "\',\'" + Price + "\','1');><td style='width:5% !important' class='tdClor'><i class='fa fa-trash' style='font-size: 16px;' onclick='deleteRow(this)'></i></td><td class='tdClor' align='right'>" + Name + "</td><td class='tdClor'>1.00</td><td class='tdClor'>" + parseFloat(Disc).toFixed(2) + "</td><td style='display:none' >" + parseFloat(discPrice_).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td class='tdClor'><input type='text' class='form-control'  value='" + Price + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + UomID + "</td><td style='display:none'>" + UomCode + "</td><td class='tdClor'>" + parseFloat(Price).toFixed(2) + "</td><td style='display:none'>" + mapCode + "</td><td style='display:none'>" + Profile + "</td><td  style='display:none'>" + mainData_.length + "</td></tr>";
                            $("#itemCarttable tbody").append(markup);
                            if (mainData_.length > 0) {
                                for (var k_ = 0; k_ < mainData_.length; k_++) {
                                    var resp_ = mainData_[k_].split('~');
                                    var id_ = 'tr_' + ItemId + "_" + k_;
                                    // markup = "<tr  id=" + id_ + " onclick=ActiveRowget(\'" + ItemId + "\',\'" + Price + "\','1');><td  style='width:5% !important;background:#ddd;color:black;'></td><td style='background:#ddd;color:black;' align='right' > " + resp_[2] + "</td><td style='background:#ddd;color:black;' >" + parseFloat(resp_[3]).toFixed(2) + "</td><td style='background:#ddd;color:black;' >" + parseFloat(resp_[10]).toFixed(2) + "</td><td style='display:none;background:#ddd;color:black;'>" + resp_[11] + "</td><td style='display:none;background:#ddd;color:black;'>" + resp_[1] + "~" + "</td><td style='background:#ddd;color:black;' ><input type='text' class='form-control'  value='" + resp_[4] + "' id='" + 'td_' + resp_[1] + "' style='display:none;background:#ddd;color:black;'></td><td  style='display:none;background:#ddd;color:black;'>" + resp_[7] + "</td><td  style='display:none;background:#ddd;color:black;'>" + resp_[5] + "</td><td style='background:#ddd;color:black;' >" + parseFloat(resp_[4]).toFixed(2) + "</td><td style='display:none;background:#ddd;color:black;' >" + resp_[8] + "</td><td  style='background:#ddd;color:black;display:none'>" + resp[9] + "</td><td  style='display:none'>" + resp_[3] + "</td></tr>"
                                    markup = "<tr  id=" + id_ + " onclick=ActiveRowget(\'" + ItemId + "\',\'" + Price + "\','1');><td  style='width:5% !important;background:#414141;color:#f2f2f2;'></td><td style='background:#414141;' align='right' > " + resp_[2] + "</td><td style='background:#414141;color:#f2f2f2;' >" + parseFloat(resp_[3]).toFixed(2) + "</td><td style='background:#414141;color:#f2f2f2;' >" + parseFloat(resp_[10]).toFixed(2) + "</td><td style='display:none;background:#414141;color:#f2f2f2;'>" + resp_[11] + "</td><td style='display:none;background:#414141;color:#f2f2f2;'>" + resp_[1] + "~" + "</td><td style='background:#414141;color:#f2f2f2;' ><input type='text' class='form-control'  value='" + resp_[4] + "' id='" + 'td_' + resp_[1] + "' style='display:none;background:#414141;color:#f2f2f2;'></td><td  style='display:none;background:#414141;color:#f2f2f2;'>" + resp_[7] + "</td><td  style='display:none;background:#ddd;color:black;'>" + resp_[5] + "</td><td style='background:#414141;color:#f2f2f2;' >" + parseFloat(resp_[4]).toFixed(2) + "</td><td style='display:none;background:#414141;color:#f2f2f2;' >" + resp_[8] + "</td><td  style='background:#414141;color:#f2f2f2;display:none'>" + resp[9] + "</td><td  style='display:none'>" + resp_[3] + "</td></tr>"
                                    $("#itemCarttable tbody").append(markup);
                                }
                            }
                            POLEDisplay(Name + ':' + Price);
                        }
                        $('#itemIdhiddenCheck').val('0');
                        GradCaluclation();
                        ClearFileds();
                        ActiveRowget(ItemId)
                        $('#txtItemScan').val('');
                        $('#scannerInput').val('');
                    }
                }
                else {
                    //Need to anable  arunkumard 
                    Name = resp[2];
                    var strOptions1 = $('#txtCusName').val();
                    var strOptions = $('#hdn_CusId').val();
                    if ($('#txthdnRemoveStatus').val() == '1') {
                        removeDraft(strOptions);
                        $('#txthdnRemoveStatus').val('0');
                    }
                    if (strOptions == '') {
                        strOptions = GenarateGustId() + '-01';
                        strOptions1 = GenarateGustName();
                    }

                    var valItemCheck_ = $('#itemIdhiddenCheck').val();
                    var dicPrice_ = Price;
                    var _mainDataCheck = '';

                    if (mainData_.length > 0) {
                        var resp_ = mainData_[0].split('~');
                        if (resp_.length >= 2) {
                            var Mresp_ = resp_[1].split('$');
                            if (Mresp_.length > 1) {
                                Disc = Mresp_[0];
                                dicPrice_ = Mresp_[3];
                                _mainDataCheck = 'DISC';
                            }
                        }
                    }

                    Duplicate(ItemId, Price, Disc, mapCode, Profile, _mainDataCheck, '1', img);
                    valItemCheck_ = $('#itemIdhiddenCheck').val();
                    if (valItemCheck_ == '0') {
                        var imgHost_ = '<img src=' + img + ' style="width:25px;height:25px">';
                        if ((img == "") || img == undefined) {
                            imgHost_ = "";
                        }
                        // var markup = "<tr id=" + 'tr_' + ItemId + " onclick=ActiveRowget(\'" + ItemId + "\',\'" + Price + "\','1');><td style='width:5% !important'><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right'>" + Name + "</td><td>1.00</td><td>" + parseFloat(Disc).toFixed(2) + "</td><td style='display:none' >" + parseFloat(dicPrice_).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td><input type='text' class='form-control'  value='" + Price + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + UomID + "</td><td style='display:none'>" + UomCode + "</td><td>" + toNumberDecimalTwoPRGR(Price) + "</td><td style='display:none'>" + mapCode + "</td><td style='display:none'>" + Profile + "</td><td style='display:none'>0</td><td>" + imgHost_ + "</td></tr>";
                        var markup = "<tr id=" + 'tr_' + ItemId + " onclick=ActiveRowget(\'" + ItemId + "\',\'" + Price + "\','1');><td style='width:5% !important' class='tdClor'><i class='fa fa-trash' onclick='deleteRow(this)' style='font-size: 16px;'></i></td><td align='right' class='tdClor'>" + Name + "</td><td class='tdClor'>1.00</td><td class='tdClor'>" + parseFloat(Disc).toFixed(2) + "</td><td style='display:none' >" + parseFloat(dicPrice_).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td class='tdClor'><input type='text' class='form-control'  value='" + Price + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + UomID + "</td><td style='display:none'>" + UomCode + "</td><td class='tdClor'>" + toNumberDecimalTwoPRGR(Price) + "</td><td style='display:none'>" + mapCode + "</td><td style='display:none'>" + Profile + "</td><td style='display:none'>0</td><td class='tdClor'>" + imgHost_ + "</td></tr>";
                        $("#itemCarttable tbody").append(markup);
                        POLEDisplay(Name + ':' + Price);


                    }

                    $('#itemIdhiddenCheck').val('0');
                    GradCaluclation();
                    ClearFileds();
                    ActiveRowget(ItemId)
                    $('#txtItemScan').val('');
                    $('#scannerInput').val('');
                }

            },
            error: function (e) {
                txt = '';
                $('#txtItemScan').val('');
                $('#scannerInput').val('');
            }
        });
    }
    else {

        try {
            var db = openDatabase('myItemMaster', '1.0', 'CashierLiteDB', 2 * 1024 * 1024);
            db.transaction(function (tx) {
                tx.executeSql('SELECT * FROM items where ItemId="' + ItemId + '"', [], function (tx, results) {

                    txt = '';
                    var resp = results.rows;
                    if (resp.length > 0) {
                        window.localStorage.setItem(ItemId, resp[0].Name + "~" + resp[0].Alias)
                        Name = resp[0].Name
                        if (JSON.parse(localStorage.getItem('PrintOffline')).C == "1") {
                            if (JSON.parse(localStorage.getItem('PrintOffline')).F == "1") {
                                Name = resp[0].Alias + " " + resp[0].Name;
                            }
                            else {
                                Name = resp[0].Alias
                            }


                        }
                        else {
                            if (JSON.parse(localStorage.getItem('PrintOffline')).F == "1") {
                                Name = resp[0].Name + " " + resp[0].Alias;
                            }

                        }
                        var strOptions1 = $('#txtCusName').val();
                        var strOptions = $('#hdn_CusId').val();
                        if ($('#txthdnRemoveStatus').val() == '1') {
                            removeDraft(strOptions);
                            $('#txthdnRemoveStatus').val('0');
                        }
                        if (strOptions == '') {
                            strOptions = GenarateGustId() + '-01';
                            strOptions1 = GenarateGustName();
                        }

                        var valItemCheck_ = $('#itemIdhiddenCheck').val();
                        var dicPrice_ = Price;
                        var _mainDataCheck = '';



                        Duplicate(ItemId, Price, Disc, mapCode, Profile, _mainDataCheck, '1', img);

                        valItemCheck_ = $('#itemIdhiddenCheck').val();
                        if (valItemCheck_ == '0') {
                            var imgHost_ = '<img src=' + img + ' style="width:25px;height:25px">';
                            if ((img == "") || img == undefined) {
                                imgHost_ = "";
                            }
                            // var markup = "<tr id=" + 'tr_' + ItemId + " onclick=ActiveRowget(\'" + ItemId + "\',\'" + Price + "\','1');><td style='width:5% !important'><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right'>" + Name + "</td><td>1.00</td><td>" + parseFloat(Disc).toFixed(2) + "</td><td style='display:none' >" + parseFloat(dicPrice_).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td><input type='text' class='form-control'  value='" + Price + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + UomID + "</td><td style='display:none'>" + UomCode + "</td><td>" + toNumberDecimalTwoPRGR(Price) + "</td><td style='display:none'>" + mapCode + "</td><td style='display:none'>" + Profile + "</td><td style='display:none'>0</td><td>" + imgHost_ + "</td></tr>";
                            var markup = "<tr id=" + 'tr_' + ItemId + " onclick=ActiveRowget(\'" + ItemId + "\',\'" + Price + "\','1');><td style='width:5% !important' class='tdClor'><i class='fa fa-trash' onclick='deleteRow(this)' style='font-size: 16px;'></i></td><td align='right' class='tdClor'>" + Name + "</td><td class='tdClor'>1.00</td><td class='tdClor'>" + parseFloat(Disc).toFixed(2) + "</td><td style='display:none' >" + parseFloat(dicPrice_).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td class='tdClor'><input type='text' class='form-control'  value='" + Price + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + UomID + "</td><td style='display:none'>" + UomCode + "</td><td class='tdClor'>" + toNumberDecimalTwoPRGR(Price) + "</td><td style='display:none'>" + mapCode + "</td><td style='display:none'>" + Profile + "</td><td style='display:none'>0</td><td class='tdClor'>" + imgHost_ + "</td></tr>";
                            $("#itemCarttable tbody").append(markup);
                        }

                        $('#itemIdhiddenCheck').val('0');
                        GradCaluclation();
                        ClearFileds();
                        ActiveRowget(ItemId)
                        $('#txtItemScan').val('');
                        $('#scannerInput').val('');
                    }

                });
            });
        }
        catch (a) {
            txt = '';
        }
    }


}
function floorFigure(figure, decimals) {
    if (!decimals) decimals = 2;
    var d = Math.pow(10, decimals);
    return (parseInt(figure * d) / d).toFixed(decimals);
};
function floorFigure_appFunc(figure, decimals) {
    return parseFloat(figure).toFixed(decimals);
};
function f11() {
    window.localStorage.setItem('ctCheck', 1);
    $('#myModalpay').modal('show');
}
function toNumberDecimalTwoPRGR(number) {
    window.localStorage.setItem('ctCheck', 1);
    var numberSplitd_ = number.toString().split('.');
    if (numberSplitd_.length > 1) {
        var subUpdatedNumber = '';
        var submNumberSplit = numberSplitd_[1];
        if (submNumberSplit.length >= 2) {
            if (submNumberSplit.length == 2) {
                submNumberSplit = submNumberSplit + '00';
                subUpdatedNumber = submNumberSplit.substring(0, 2);
                return (numberSplitd_[0] + '.' + subUpdatedNumber);
            }
            if (submNumberSplit.length == 3) {
                submNumberSplit = submNumberSplit + '0';
                subUpdatedNumber = submNumberSplit.substring(0, 2);
                return (numberSplitd_[0] + '.' + subUpdatedNumber);
            }
            if (submNumberSplit.length >= 4) {
                submNumberSplit = submNumberSplit;
                subUpdatedNumber = submNumberSplit.substring(0, 2);
                return (numberSplitd_[0] + '.' + subUpdatedNumber);
            }
        }
        else {
            return (number);
        }
    }
}
function valueAppendtoPOPUp() {

    window.localStorage.setItem('ctCheck', 1);
    $('#hdnTemBasePrice').val('');

    $('#hdnTemBasePriceEX').val('');
    $('#hdnTemBasePriceTend').val('');
    $('#hdnTemBasePriceCurId').val('');
    TenderRead('false', '');
    $('#txtTendertrans').val('');
    $('#txtTendertransref').val('');
    $('#lblTransamodetype').text($('#hdnAmount').val());
    $('#lblTransamodetypeRef').text($('#hdnChange').val());
    $('#hdnTransModeforPay').val('');
    GetCounterTypeTenderLocalData();
    var myTab = document.getElementById('itemCarttable');
    var Price = "0.000"; var Items = "0.000"; var Quantity = "0.000"; var GrossAmt = "0.000"; var Quantity_ = "0.000"; var TaxTotal = "0.000"; var DiscAmount = "0.000";
    // LOOP THROUGH EACH ROW OF THE TABLE AFTER HEADER.
    $("#tabletrpopbind thead").remove();

    $("#tabletrpopbind tbody").remove();
    //var markupHeader = " <thead class='thead-inverse'><tr><th>#</th><th>Item</th><th>Qty</th><th>Disc(%)</th><th>Price</th><th>Amount</th><th style='display:none' >Tax</th><th  style='display:none' >Tax Amount</th><th>Total</th></tr><thead>  <tbody></tbody>";
    var markupHeader = " <thead class='thead-inverse'><tr><th>#</th><th>" + $('#hdnItem').val() + "</th><th>" + $('#hdnQty').val() + "</th><th>" + $('#hdnDisc').val() + "</th><th>" + $('#hdnPrice').val() + "</th><th>" + $('#hdnAmount').val() + "</th><th style='display:none' >Tax</th><th  style='display:none' >Tax Amount</th><th>" + $('#hdnTotal').val() + "</th></tr><thead><tbody></tbody>";
    $("#tabletrpopbind").append(markupHeader);
    var itemGuidbyM = '';
    var myTabPP = document.getElementById('itemCarttable');
    for (i = 0; i < myTab.rows.length; i++) {
        // GET THE CELLS COLLECTION OF THE CURRENT ROW.
        var objCells = myTab.rows.item(i).cells;
        // LOOP THROUGH EACH CELL OF THE CURENT ROW TO READ CELL VALUES. 
        Quantity = parseFloat(objCells.item(2).innerHTML);
        Quantity_ = parseFloat(Quantity_) + parseFloat(objCells.item(2).innerHTML);
        var Baseprice = objCells.item(9).innerHTML;
        var Dis1 = ((parseFloat(parseFloat(Baseprice) * parseFloat(Quantity)) * (parseFloat(objCells.item(3).innerHTML))) / 100);
        var Amt_ = parseFloat(Baseprice) * parseFloat(Quantity);
        if (parseFloat(Dis1) != 0) {

            Amt_ = parseFloat(parseFloat(Baseprice) * parseFloat(Quantity)) - parseFloat(Dis1);
        }
        var sno = parseFloat(i) + parseFloat(1);
        var taxSplit_ = objCells.item(10).innerText.split('&');
        var taxType_ = 'F';
        if (taxSplit_.length >= 1) {
            //Tax Caluclation    
            var profileData = objCells.item(11).innerText.split('@');
            if (profileData != '') {
                var CAMT_ = 0;
                for (var K = 0; K < profileData.length; K++) {
                    var cols = profileData[K].split('#');
                    if (cols != '') {
                        taxType_ = cols[16];
                        if (cols[4] == "+") {
                            CAMT_ = parseFloat((parseFloat(CAMT_) + parseFloat(cols[5])));
                        }
                        else
                            CAMT_ = parseFloat(parseFloat(CAMT_) - parseFloat(cols[5]));
                    }
                }
            }

        }

        var Total_ = 0; var AmountSub_ = 0; var taxTypeP = '';
        if (profileData != '') {
            if (taxType_ == "R") {

                var taxAmt = parseFloat(Amt_) * parseFloat(CAMT_);
                Total_ = toNumberDecimalTwo(Amt_) - toNumberDecimalTwo(taxAmt);
                //Total_ = parseFloat(parseFloat(Amt_) - parseFloat(taxAmt));
                CAMT_ = toNumberDecimalTwoPR(taxAmt);
                //CAMT_ = parseFloat(taxAmt);
                AmountSub_ = toNumberDecimalTwo(Total_);
                //AmountSub_ = parseFloat(Total_).toFixed(2);
                Total_ = toNumberDecimalTwo(Amt_);
                //Total_ = parseFloat(Amt_).toFixed(2);
                GrossAmt = parseFloat(toNumberDecimalTwo(GrossAmt)) + parseFloat(toNumberDecimalTwo(AmountSub_));
                //GrossAmt = parseFloat(GrossAmt) + parseFloat(AmountSub_);
                //Price = parseFloat(Price) + parseFloat(Total_);
                Price = toNumberDecimalTwo(Price) + toNumberDecimalTwo(Total_);
                taxTypeP = "R";
            }
            else {

                var taxAmt = parseFloat(Amt_) * (parseFloat(CAMT_));
                //Price = parseFloat(Amt_) + parseFloat(taxAmt);
                Total_ = toNumberDecimalTwo(Amt_) + toNumberDecimalTwo(taxAmt);
                CAMT_ = toNumberDecimalTwo(taxAmt);
                AmountSub_ = toNumberDecimalTwo(Amt_);
                GrossAmt = toNumberDecimalTwo(GrossAmt) + toNumberDecimalTwo(AmountSub_);
                //GrossAmt = parseFloat(GrossAmt) + parseFloat(AmountSub_);
                Price = toNumberDecimalTwo(Price) + toNumberDecimalTwo(Total_);
                taxTypeP = "F";


            }
        }
        else {
            CAMT_ = 0;
            Total_ = toNumberDecimalTwo(Amt_);
            AmountSub_ = toNumberDecimalTwo(Amt_);
            //GrossAmt = parseFloat(GrossAmt) + parseFloat(AmountSub_);
            GrossAmt = toNumberDecimalTwo(GrossAmt) + toNumberDecimalTwo(AmountSub_);
            Price = toNumberDecimalTwo(Price) + toNumberDecimalTwo(Total_);
            //Price = parseFloat(Price) + parseFloat(Total_);
        }
        //TaxTotal = parseFloat(TaxTotal) + parseFloat(CAMT_);
        TaxTotal = toNumberDecimalTwo(TaxTotal) + toNumberDecimalTwo(CAMT_);
        DiscAmount = parseFloat(DiscAmount) + parseFloat(Dis1);
        //guid();
        var type = 'M';
        if (objCells[1].attributes["0"].value == 'right') {
            itemGuidbyM = guid();
            type = 'M';
        }
        else {
            type = 'S';
        }
        var ItemIdP_ = objCells.item(5).innerHTML;
        var QtyP_ = objCells.item(2).innerHTML;
        var DiscP_ = objCells.item(3).innerHTML;
        var AmountP_ = Total_;
        var UOMP_ = objCells.item(7).innerHTML;

        var markup = "<tr><td style='width:5% !important'>" + sno + "</td><td align='left'>" + objCells.item(1).innerHTML + "</td><td align='right'>" + objCells.item(2).innerHTML + "</td><td align='right'>" + objCells.item(3).innerHTML + "</td><td align='right'>" + Baseprice + "</td><td align='right'>" + AmountSub_ + "</td><td style='display:none' >" + taxSplit_[0] + "</td><td  style='display:none'>" + (CAMT_) + "</td><td>" + toNumberDecimalTwo(Total_) + "</td><td style='display:none'>" + objCells.item(10).innerText + "</td><td style='display:none'>" + objCells.item(11).innerText + "</td><td style='display:none'>" + ItemIdP_ + "</td><td style='display:none'>" + QtyP_ + "</td><td style='display:none'>" + DiscP_ + "</td><td style='display:none'>" + AmountP_ + "</td><td style='display:none'>" + UOMP_ + "</td><td style='display:none'>" + taxTypeP + "</td><td style='display:none'>" + itemGuidbyM + "</td><td style='display:none'>" + type + "</td><tr>";
        $("#tabletrpopbind tbody").append(markup);


    }
    //$('#txtQuantity').val(qty_);
    if (myTab.rows.length > 0) {
        //$('#myModalpay').modal('show');
    }
    else {

        navigator.notification.alert($('#hdnMsgCart').val(), "", "neo ecr", "Ok");

        return false;
    }
    // BootstrapDialog.alert('Please Fill Cart');
    $('#txtCurrency').val(window.localStorage.getItem('BaseCurrencyCode'));
    $('#txtItemspp').val(parseFloat(myTab.rows.length).toFixed(2));
    $('#hdnItemspp').val(parseFloat(myTab.rows.length).toFixed(4));
    $('#txtPricepp').val(parseFloat(GrossAmt).toFixed(2));
    $('#hdnPricepp').val(GrossAmt);
    //$('#txtAmountpp').val(Math.round(Price).toFixed(2));
    //$('#lbl_TpartTotal')[0].innerText = '-' + Math.round(Price).toFixed(2);
    if (window.localStorage.getItem("roundoff") == "0") {
        $('#txtAmountpp').val(parseFloat(Price).toFixed(2));
        $('#lbl_TpartTotal')[0].innerText = '-' + parseFloat(Price).toFixed(2);
    }
    else {
        $('#txtAmountpp').val(RoundInvAmount(Price));
        $('#lbl_TpartTotal')[0].innerText = '-' + RoundInvAmount(Price);
    }
    //$("#tbl_tenderDyms tbody").remove();
    $('#txtItemSearchCart').val('');
    //new
    //$("#tbl_tenderDyms tr").remove();
    $('#hdnAmountpp').val((Price));
    $('#txtQuantityPP').val(parseFloat(Quantity_).toFixed(2));
    $('#hdnQuantityPP').val(parseFloat(Quantity_).toFixed(4));
    if (parseFloat(DiscAmount) > 0) {
        if (DiscAmount != 'Infinity') {
            $('#txtDiscountpp').val(parseFloat(DiscAmount).toFixed(2));
            $('#hdnDiscountpp').val(parseFloat(DiscAmount).toFixed(4));
        }
        else {
            $('#txtDiscountpp').val('0.00');
            $('#hdnDiscountpp').val('0.00');
        }
    } else {
        $('#txtDiscountpp').val('0.00');
        $('#hdnDiscountpp').val('0.00');
    }

    $('#txtTaxPP').val(parseFloat(TaxTotal).toFixed(2));
    $('#hdnTaxPP').val(Number(TaxTotal));

    var HashVal = (window.localStorage.getItem("hashValues"));
    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');
        var HeadeName_ = HashValues[17].split('/')
        $('#lblheader').text(HeadeName_[0]);
    }

    var CurDate = new Date();
    //var DateWithFormate = CurDate.getFullYear() + '-' + (CurDate.getMonth() + 1) + '-' + CurDate.getDate() + ' ' + CurDate.getHours() + ':' + CurDate.getSeconds();
    var min = CurDate.getMinutes().toString().length == 1 ? '0' + CurDate.getMinutes() : CurDate.getMinutes();
    var DateWithFormate = (CurDate.getDate() < 10 ? '0' : '') + CurDate.getDate() + '-' + ((CurDate.getMonth() + 1) < 10 ? '0' : '') + (CurDate.getMonth() + 1) + '-' + CurDate.getFullYear() + ' ' + CurDate.getHours() + ':' + min;
    $('#lbl_date').text(DateWithFormate);

    $('#lbl_shift_Print').text($('#hdnShift').val() + ' : ' + window.localStorage.getItem('Shift_Name'));
    $('#lbl_counter_Print').text($('#hdnCounter').val() + ' : ' + window.localStorage.getItem('COUNTERNAME'));
    $('#lbl_InvoiceNoPrint').text($('#hdnInvoice').val() + ' : ' + window.localStorage.getItem('COUNTERNAME'));
    $('#lbl_ItemsPrint').text($('#hdnTotItems').val() + ' : ' + parseFloat(myTab.rows.length).toFixed(2));
    $('#lbl_Qty_Print').text($('#hdnTotQty').val() + ' : ' + Quantity_);
    $('#lbl_Pay_Print').text($('#hdnPayIn').val() + ' : Cash - INR');

    $('#txtItemScan').val('');
    POLEDisplay("Total : " + parseFloat($('#txtAmountpp').val()).toFixed(2));
    $('#chkHomeDelivery').prop('checked', false);
    var valTy = '(' + window.localStorage.getItem('BaseCurrencyCode') + ')' + ' : ';
    //$('#hdnTemBasePrice').val(window.localStorage.getItem('BaseCurrencyCode'));
    $('#lblTendertype').text(valTy);
    //GetTypeTenderDataCashbyLocal();
    partialValBalnceCal();
    amtTenderCalc();
    return true;
}

function closeprintDialog() {
    window.localStorage.setItem('ctCheck', 1);
    GetItemDetails();
    $('#myModalprint').css('display', "none");
}
function SearchItem() {
    window.localStorage.setItem('ctCheck', 1);
    if (window.localStorage.getItem('network') == '1') {
        $("#hdnPageIndex").val(1);
        GetItemDetails();

        $('#txtItemSearch').val('');

        var HashValues = (window.localStorage.getItem("hashValues"));
        if (HashValues != null && HashValues != '') {
            var hashSplit = HashValues.split('~');
            getResources1(hashSplit[7], "POSInvoice");

        }
        $('#myItemSearch').modal('show');
    }
    else {
        navigator.notification.alert("Item lookup disabled in offline network mode", "", "neo ecr", "Ok");
    }
}
function GradCaluclation() {
    window.localStorage.setItem('ctCheck', 1);
    var myTab_ = document.getElementById('itemCarttable');
    //var m_ = "0.000"; var qty_ = "0.000"; 
    var Price = "0.000"; var Items = "0.000"; var Quantity = "0.000"; var GrossAmt = "0.000";
    // LOOP THROUGH EACH ROW OF THE TABLE AFTER HEADER.
    if (myTab_.rows.length > 0) {
        for (i = 0; i < myTab_.rows.length; i++) {
            // GET THE CELLS COLLECTION OF THE CURRENT ROW.
            var objCells = myTab_.rows.item(i).cells;
            // LOOP THROUGH EACH CELL OF THE CURENT ROW TO READ CELL VALUES. 
            Quantity = parseFloat(objCells.item(2).innerHTML);
            //Price = parseFloat(objCells.item(4).innerHTML) + parseFloat(Price) - parseFloat(objCells.item(3).innerHTML);
            //Price = parseFloat(Price).toFixed(3);
            var Baseprice = $('#td_' + objCells.item(5).innerHTML).val();
            GrossAmt = parseFloat(GrossAmt) + parseFloat(Baseprice) * parseFloat(Quantity);
            Price = parseFloat(objCells.item(4).innerHTML) + parseFloat(Price);
            Price = parseFloat(Price).toFixed(3);
        }
    } else {
        $('#lblTenderConvertAmt').text("0.00");

    }
    //$('#txtQuantity').val(qty_);

    $('#grdandTotal').text(parseFloat(Price).toFixed(2));
    $('#discGrdandTotal').text((parseFloat(GrossAmt) - parseFloat(Price)).toFixed(2));
    $('#totalitemscw').text(parseInt(myTab_.rows.length));

}

function Caluclation() {
    window.localStorage.setItem('ctCheck', 1);
    var myTab = document.getElementById('itemCarttable');
    var m_ = "0.000"; var qty_ = "0.000"; var Price = "0.000";
    // LOOP THROUGH EACH ROW OF THE TABLE AFTER HEADER.
    for (i = 0; i < myTab.rows.length; i++) {
        // GET THE CELLS COLLECTION OF THE CURRENT ROW.
        var objCells = myTab.rows.item(i).cells;
        // LOOP THROUGH EACH CELL OF THE CURENT ROW TO READ CELL VALUES. 
        //qty_ = parseFloat($('#td_' + objCells.item(4).innerHTML).val()) + parseFloat(qty_);
        qty_ = parseFloat(objCells.item(2).innerHTML) + parseFloat(qty_);
        Price = parseFloat(objCells.item(3).innerHTML) + parseFloat(Price);
    }

    $('#txtQuantity').val(parseFloat(qty_).toFixed(2));
    $('#txtPrice').val(parseFloat(Price).toFixed(2));
    $('#txtAmount').val(parseFloat(Price).toFixed(2));
}

function DuplicateQTYMODE(ItemId, Price_, Disc_, MapCode_, Profile_, mainData_, QTY_, img) {
    window.localStorage.setItem('ctCheck', 1);
    var myTab = document.getElementById('itemCarttable');
    var m_ = "";
    $('#itemIdhiddenCheck').val('0');
    // LOOP THROUGH EACH ROW OF THE TABLE AFTER HEADER.
    for (i = 0; i < myTab.rows.length; i++) {
        // GET THE CELLS COLLECTION OF THE CURRENT ROW.
        var objCells = myTab.rows.item(i).cells;
        // LOOP THROUGH EACH CELL OF THE CURENT ROW TO READ CELL VALUES.
        if (ItemId == objCells.item(5).innerHTML) {
            var qty_ = parseFloat(QTY_);
            var Price = '0';
            var Baseprice = floorFigure_appFunc(parseFloat(objCells.item(9).innerHTML), 2);
            //if (mainData_ == "DISC") {
            objCells.item(3).innerHTML = Disc_;
            //}
            //else {
            //    objCells.item(3).innerHTML = '0';

            //}
            Price = parseFloat(Baseprice) * parseFloat(qty_);
            if (objCells.item(3).innerHTML != "") {
                if (parseFloat(objCells.item(3).innerHTML) > 0) {
                    Price = parseFloat(Price) - ((parseFloat(Price) * (parseFloat(objCells.item(3).innerHTML))) / 100);
                }
                else if (parseFloat(Disc_) > 0) {
                    Price = parseFloat(Price) - ((parseFloat(Price) * (parseFloat(Disc_))) / 100);
                }
            }

            var imgHost_ = img;
            if ((img == "") || img == undefined) {
                imgHost_ = "";
            }
            try {
                var markup = "<tr id=" + 'tr_' + ItemId + " onclick=ActiveRowget(\'" + ItemId + "\',\'" + Price_ + "\','1');><td  style='width:5% !important' class='tdClor'><i class='fa fa-trash' onclick='deleteRow(this)' style='font-size:16px;'></i></td><td align='right' class='tdClor'>" + objCells.item(1).innerHTML + "</td><td class='tdClor'>" + parseFloat(qty_).toFixed(2) + "</td><td class='tdClor'>" + parseFloat(objCells.item(3).innerHTML).toFixed(2) + "</td><td style='display:none' >" + parseFloat(Price).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td class='tdClor'><input type='text' class='form-control'  value='" + Baseprice + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + objCells.item(7).innerHTML + "</td><td style='display:none'>" + objCells.item(8).innerHTML + "</td><td class='tdClor'>" + Baseprice + "</td><td style='display:none'>" + MapCode_ + "</td><td style='display:none'>" + Profile_ + "</td><td style='display:none'>" + mainData_.length + "</td><td class='tdClor'>" + imgHost_ + "</td></tr>";
            }
            catch (r) {
                var markup = "<tr id=" + 'tr_' + ItemId + " onclick=ActiveRowget(\'" + ItemId + "\',\'" + Price_ + "\','1');><td  style='width:5% !important' class='tdClor'><iclass='fa fa-trash' onclick='deleteRow(this)' style='font-size:16px;'></i></td><td align='right' class='tdClor'>" + objCells.item(1).innerHTML + "</td><td class='tdClor'>" + parseFloat(qty_).toFixed(2) + "</td><td class='tdClor'>" + parseFloat(objCells.item(3).innerHTML).toFixed(2) + "</td><td style='display:none' >" + parseFloat(Price).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td class='tdClor'><input type='text' class='form-control'  value='" + Baseprice + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + objCells.item(7).innerHTML + "</td><td style='display:none'>" + objCells.item(8).innerHTML + "</td><td class='tdClor'>" + Baseprice + "</td><td style='display:none'>" + MapCode_ + "</td><td style='display:none'>" + Profile_ + "</td><td style='display:none'>" + objCells.item(12).innerHTML + "</td><td class='tdClor'>" + imgHost_ + "</td></tr>";
            }
            //var markup = "<td style='width:5% !important'><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right'>" + Name + "</td>                          <td>1.00</td>                               <td>" + parseFloat(Disc).toFixed(2) + "</td>                     <td style='display:none' >" + parseFloat(Price).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td><input type='text' class='form-control'  value='" + Price + "' id='" + 'td_' + ItemId + "' style='display:none'></td>                      <td style='display:none'>" + UomID + "</td>                     <td style='display:none'>" + UomCode + "</td>                   <td>" + Price + "</td>";
            var mIdSubLength = objCells.item(12).innerHTML;
            if (parseFloat(mIdSubLength) >= 0) {
                for (var m1_ = 1; m1_ < mIdSubLength + 1; m1_++) {
                    var idC_ = '#tr_' + ItemId + "_" + (m1_ - 1);
                    $(idC_).remove();
                }
            }
            var idC_ = '#tr_' + ItemId;
            $(idC_).remove();


            $("#itemCarttable tbody").append(markup);
            try {
                if (mainData_ != "DISC") {
                    //if (parseFloat(objCells.item(12).innerHTML) > 0) {
                    if (mainData_.length > 0) {
                        for (var k_ = 0; k_ < mainData_.length; k_++) {
                            var resp_ = mainData_[k_].split('~');
                            var id_ = 'tr_' + ItemId + "_" + k_;
                            var imgHost_ = img;
                            if ((img == "") || img == undefined) {
                                imgHost_ = "";
                            }
                            markup = "<tr  id=" + id_ + " onclick=ActiveRowget(\'" + ItemId + "\',\'" + Price + "\','1');><td  style='width:5% !important;background:#ddd;color:black;'></td><td style='background:#ddd;color:black;' align='right' > " + resp_[2] + "</td><td style='background:#ddd;color:black;' >" + parseFloat(resp_[3]).toFixed(2) + "</td><td style='background:#ddd;color:black;' >" + parseFloat(resp_[10]).toFixed(2) + "</td><td style='display:none;background:#ddd;color:black;'>" + resp_[4] + "</td><td style='display:none;background:#ddd;color:black;'>" + resp_[1] + "~" + "</td><td style='background:#ddd;color:black;' ><input type='text' class='form-control'  value='" + resp_[4] + "' id='" + 'td_' + resp_[1] + "' style='display:none;background:#ddd;color:black;'></td><td  style='display:none;background:#ddd;color:black;'>" + resp_[7] + "</td><td  style='display:none;background:#ddd;color:black;'>" + resp_[5] + "</td><td style='background:#ddd;color:black;' >" + parseFloat(resp_[4]).toFixed(2) + "</td><td style='background:#ddd;color:black;' ></td><td  style='background:#ddd;color:black;'></td><td  style='display:none'>" + resp_[3] + "</td><td>" + imgHost_ + "</td></tr>"
                            $("#itemCarttable tbody").append(markup);
                        }
                    }
                }
                //}
            }
            catch (r) {

            }

            //duplicateValForScheemItem(ItemId, qty_, 0, objCells.item(12).innerHTML);
            $('#itemIdhiddenCheck').val('1');
            ClearFileds();
            POLEDisplay(objCells.item(1).innerHTML + ':' + Price);

            $('#txtQuantity').val(QTY_);
        }
    }
}

function Duplicate(ItemId, Price_, Disc_, MapCode_, Profile_, mainData_, QTY_, img) {
    window.localStorage.setItem('ctCheck', 1);
    var myTab = document.getElementById('itemCarttable');
    var m_ = "";
    $('#itemIdhiddenCheck').val('0');
    // LOOP THROUGH EACH ROW OF THE TABLE AFTER HEADER.
    for (i = 0; i < myTab.rows.length; i++) {
        // GET THE CELLS COLLECTION OF THE CURRENT ROW.
        var objCells = myTab.rows.item(i).cells;
        // LOOP THROUGH EACH CELL OF THE CURENT ROW TO READ CELL VALUES.
        if (ItemId == objCells.item(5).innerHTML) {
            var qty_ = parseFloat(objCells.item(2).innerHTML) + parseFloat(QTY_);
            var Price = '0';
            var Baseprice = floorFigure_appFunc(parseFloat(objCells.item(9).innerHTML), 2);
            Price = parseFloat(Baseprice) * parseFloat(qty_);
            if (mainData_ == 'DISC') {
                objCells.item(3).innerHTML = Disc_;
            }
            else {
                objCells.item(3).innerHTML = '0';
            }
            if (objCells.item(3).innerHTML != "") {
                if (parseFloat(objCells.item(3).innerHTML) > 0) {
                    Price = parseFloat(Price) - ((parseFloat(Price) * (parseFloat(objCells.item(3).innerHTML))) / 100);
                }
                else if (parseFloat(Disc_) > 0) {
                    Price = parseFloat(Price) - ((parseFloat(Price) * (parseFloat(Disc_))) / 100);
                }

            }
            var imgHost_ = '<img src=' + img + ' style="width:25px;height:25px">';
            if ((img == "") || img == undefined) {
                imgHost_ = "";
            }
            try {
                if (mainData_ == 'DISC') {
                    var markup = "<tr id=" + 'tr_' + ItemId + " onclick=ActiveRowget(\'" + ItemId + "\',\'" + Price_ + "\','1');><td  style='width:5% !important' class='tdClor'><i class='fa fa-trash' onclick='deleteRow(this)'></i></td><td align='right' class='tdClor' >" + objCells.item(1).innerHTML + "</td><td class='tdClor'>" + parseFloat(qty_).toFixed(2) + "</td><td class='tdClor'>" + parseFloat(objCells.item(3).innerHTML).toFixed(2) + "</td><td style='display:none' class='tdClor'>" + parseFloat(Price).toFixed(2) + "</td><td style='display:none' class='tdClor'>" + ItemId + "</td><td class='tdClor'><input type='text' class='form-control'  value='" + Baseprice + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none' class='tdClor'>" + objCells.item(7).innerHTML + "</td><td style='display:none' class='tdClor'>" + objCells.item(8).innerHTML + "</td><td class='tdClor'>" + Baseprice + "</td><td style='display:none' class='tdClor'>" + MapCode_ + "</td><td style='display:none' class='tdClor'>" + Profile_ + "</td><td style='display:none' class='tdClor'>" + objCells.item(12).innerHTML + "</td><td class='tdClor'>" + imgHost_ + "</td></tr>";

                }
                else {
                    var markup = "<tr id=" + 'tr_' + ItemId + " onclick=ActiveRowget(\'" + ItemId + "\',\'" + Price_ + "\','1');><td  style='width:5% !important' class='tdClor'><i class='fa fa-trash' onclick='deleteRow(this)'></i></td><td align='right' class='tdClor' >" + objCells.item(1).innerHTML + "</td><td class='tdClor'>" + parseFloat(qty_).toFixed(2) + "</td><td class='tdClor'>" + parseFloat(objCells.item(3).innerHTML).toFixed(2) + "</td><td style='display:none' class='tdClor'>" + parseFloat(Price).toFixed(2) + "</td><td style='display:none' class='tdClor'>" + ItemId + "</td><td class='tdClor'><input type='text' class='form-control'  value='" + Baseprice + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none' class='tdClor'>" + objCells.item(7).innerHTML + "</td><td style='display:none' class='tdClor'>" + objCells.item(8).innerHTML + "</td><td class='tdClor'>" + Baseprice + "</td><td style='display:none' class='tdClor'>" + MapCode_ + "</td><td style='display:none' class='tdClor'>" + Profile_ + "</td><td style='display:none' class='tdClor'>" + mainData_.length + "</td><td class='tdClor'>" + imgHost_ + "</td></tr>";
                }
            }
            catch (r) {
                var markup = "<tr id=" + 'tr_' + ItemId + " onclick=ActiveRowget(\'" + ItemId + "\',\'" + Price_ + "\','1');><td  style='width:5% !important' class='tdClor'><i class='fa fa-trash' onclick='deleteRow(this)'></i></td><td align='right' class='tdClor'>" + objCells.item(1).innerHTML + "</td><td class='tdClor'>" + parseFloat(qty_).toFixed(2) + "</td><td class='tdClor'>" + parseFloat(objCells.item(3).innerHTML).toFixed(2) + "</td><td style='display:none' class='tdClor'>" + parseFloat(Price).toFixed(2) + "</td><td style='display:none' class='tdClor'>" + ItemId + "</td><td class='tdClor'><input type='text' class='form-control'  value='" + Baseprice + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none' class='tdClor'>" + objCells.item(7).innerHTML + "</td><td style='display:none' class='tdClor'>" + objCells.item(8).innerHTML + "</td><td class='tdClor'>" + Baseprice + "</td><td style='display:none' class='tdClor'>" + MapCode_ + "</td><td style='display:none' class='tdClor'>" + Profile_ + "</td><td style='display:none' class='tdClor'>" + objCells.item(12).innerHTML + "</td><td class='tdClor'>" + imgHost_ + "</td></tr>";
            }
            //var markup = "<td style='width:5% !important'><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right'>" + Name + "</td>                          <td>1.00</td>                               <td>" + parseFloat(Disc).toFixed(2) + "</td>                     <td style='display:none' >" + parseFloat(Price).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td><input type='text' class='form-control'  value='" + Price + "' id='" + 'td_' + ItemId + "' style='display:none'></td>                      <td style='display:none'>" + UomID + "</td>                     <td style='display:none'>" + UomCode + "</td>                   <td>" + Price + "</td>";
            var mIdSubLength = objCells.item(12).innerHTML;
            if (parseFloat(mIdSubLength) >= 0) {
                for (var m1_ = 1; m1_ < mIdSubLength + 1; m1_++) {
                    var idC_ = '#tr_' + ItemId + "_" + (m1_ - 1);
                    $(idC_).remove();
                }
            }
            var idC_ = '#tr_' + ItemId;
            $(idC_).remove();


            $("#itemCarttable tbody").append(markup);
            try {
                if (mainData_ != 'DISC') {
                    //if (parseFloat(objCells.item(12).innerHTML) > 0) {
                    if (mainData_.length > 0) {
                        for (var k_ = 0; k_ < mainData_.length; k_++) {
                            var resp_ = mainData_[k_].split('~');
                            var id_ = 'tr_' + ItemId + "_" + k_;
                            var imgHost_ = '<img src=' + img + ' style="width:25px;height:25px">';
                            if ((img == "") || img == undefined) {
                                imgHost_ = "";
                            }
                            markup = "<tr  id=" + id_ + " onclick=ActiveRowget(\'" + ItemId + "\',\'" + Price + "\','1');><td  style='width:5% !important;background:#ddd;color:black;'></td><td style='background:#ddd;color:black;' align='right' > " + resp_[2] + "</td><td style='background:#ddd;color:black;' >" + parseFloat(resp_[3]).toFixed(2) + "</td><td style='background:#ddd;color:black;' >" + parseFloat(resp_[10]).toFixed(2) + "</td><td style='display:none;background:#ddd;color:black;'>" + resp_[11] + "</td><td style='display:none;background:#ddd;color:black;'>" + resp_[1] + "~" + "</td><td style='background:#ddd;color:black;' ><input type='text' class='form-control'  value='" + resp_[4] + "' id='" + 'td_' + resp_[1] + "' style='display:none;background:#ddd;color:black;'></td><td  style='display:none;background:#ddd;color:black;'>" + resp_[7] + "</td><td  style='display:none;background:#ddd;color:black;'>" + resp_[5] + "</td><td style='background:#ddd;color:black;' >" + parseFloat(resp_[4]).toFixed(2) + "</td><td style='display:none;background:#ddd;color:black;' >" + resp_[8] + "</td><td  style='background:#ddd;color:black;display:none'>" + resp_[9] + "</td><td  style='display:none'>" + resp_[3] + "</td><td>" + imgHost_ + "</td></tr>"
                            $("#itemCarttable tbody").append(markup);
                        }
                    }
                }
                //}
            }
            catch (r) {

            }
            //duplicateValForScheemItem(ItemId, qty_, 0, objCells.item(12).innerHTML);
            $('#itemIdhiddenCheck').val('1');
            ClearFileds();
            POLEDisplay(objCells.item(1).innerHTML + ':' + Price);

        }
    }
}
function duplicateValForScheemItem(id, qty, price, scheemItemCount) {
    window.localStorage.setItem('ctCheck', 1);
    var myTab = document.getElementById('itemCarttable');
    var m_ = "";
    if (scheemItemCount > 0) {
        // LOOP THROUGH EACH ROW OF THE TABLE AFTER HEADER.
        for (var m = 1; m < parseFloat(scheemItemCount) + 1; m++) {
            for (i = 0; i < myTab.rows.length; i++) {
                // GET THE CELLS COLLECTION OF THE CURRENT ROW.
                var objCells = myTab.rows.item(i).cells;
                var ItemId = 'tr_' + id + "_" + (m - 1);
                var checkId = myTab.rows[i].attributes["0"].value;
                // LOOP THROUGH EACH CELL OF THE CURENT ROW TO READ CELL VALUES.
                if (ItemId == checkId) {
                    var qtyS_ = parseFloat(qty) * parseFloat(objCells.item(12).innerHTML);
                    var PriceS = price;
                    var markup = "<td  style='width:5% !important;background:#ddd;color:black;'></td><td align='right'  style='background:#ddd;color:black;'> " + objCells.item(1).innerHTML + "</td><td style='background:#ddd;color:black;' >" + parseFloat(qtyS_).toFixed(2) + "</td><td style='background:#ddd;color:black;' >" + objCells.item(3).innerHTML + "</td><td style='display:none;background:#ddd;color:black;' >" + objCells.item(4).innerHTML + "</td><td style='display:none;background:#ddd;color:black;'>" + objCells.item(5).innerHTML + "</td><td style='background:#ddd;color:black;' ></td><td style='display:none;background:#ddd;color:black;'>" + objCells.item(7).innerHTML + "</td><td style='display:none;background:#ddd;color:black;'>" + objCells.item(8).innerHTML + "</td><td style='background:#ddd;color:black;'>0.00</td><td style='background:#ddd;color:black;'></td><td style='background:#ddd;color:black;'></td><td style='display:none'>" + objCells.item(12).innerHTML + "</td>";
                    $('#' + ItemId).html(markup);

                }
            }
        }
    }
}


function SubQtySubChange() {
    window.localStorage.setItem('ctCheck', 1);
    var myTab = document.getElementById('itemCarttable');
    var m_ = "";

    if ($('#txthdnItemId').val() != "") {
        document.getElementById('liQtyM').style.cssText = 'color: black;font-weight: bold;background:  #FF7276;';
        document.getElementById('liDisM').style.cssText = 'color:white;background: #FF7276;';
        document.getElementById('liPriceM').style.cssText = 'color:white;background: #FF7276;';
        for (i = 0; i < myTab.rows.length; i++) {
            var objCells = myTab.rows.item(i).cells;
            if ($('#txthdnItemId').val() == objCells.item(5).innerHTML) {
                $("#txtAmount").attr("disabled", "disabled");
                $("#txthdnItemId").attr("disabled", "disabled");
                $("#txtQuantity").attr("disabled", "disabled");
                $("#txtPrice").attr("disabled", "disabled");
                $("#txtUOM").attr("disabled", "disabled");
                $("#txtDiscount").attr("disabled", "disabled");
                $("#txtQuantity").removeAttr("disabled");

                $("#txthdnActiveColumn").val("1");
                break;
            }
        }
    }
    else {

        navigator.notification.alert($('#hdnPlzSelectItem').val(), "", "neo ecr", "Ok");

    }
}

function QtySubChange() {
    window.localStorage.setItem('ctCheck', 1);
    var myTab = document.getElementById('itemCarttable');
    var m_ = "";

    if ($('#txthdnItemId').val() != "") {
        document.getElementById('liQtyM').style.cssText = 'color: black;font-weight: bold;background:  #FF7276;';
        document.getElementById('liDisM').style.cssText = 'color:white;background: #FF7276;';
        document.getElementById('liPriceM').style.cssText = 'color:white;background: #FF7276;';
        for (i = 0; i < myTab.rows.length; i++) {
            var objCells = myTab.rows.item(i).cells;
            if ($('#txthdnItemId').val() == objCells.item(5).innerHTML) {
                $("#txtAmount").attr("disabled", "disabled");
                $("#txthdnItemId").attr("disabled", "disabled");
                $("#txtQuantity").attr("disabled", "disabled");
                $("#txtPrice").attr("disabled", "disabled");
                $("#txtUOM").attr("disabled", "disabled");
                $("#txtDiscount").attr("disabled", "disabled");
                //   $("#txtQuantity").removeAttr("disabled");
                $("#txtQuantity").val('');

                $("#txthdnActiveColumn").val("1");
                break;
            }
        }
    }
    else {
        navigator.notification.alert($('#hdnPlzSelectItem').val(), "", "neo ecr", "Ok");

    }

}

function btnPrimaryClick() {
    var password = document.getElementById('txtPrimaryPwd').value;
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        SubOrg_Id = hashSplit[1];
    }

    Jsonobj = [];
    var SubOrg = {
    };
    SubOrg.SubOrgID = $('#hdnBranchId').val();
    //  SubOrg.VD = vd_;
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.VerifyPrimaryPasswordCheck,
        data: "password=" + password + "&VD=" + VD1(),
        success: function (resp) {
            if (resp == '100000') {
                //$("#hdnPwdCheckStatus").val("1");
                document.getElementById('txtPrimaryPwd').value = "";
                $('#ModalPrimaryUserPWDPopup').modal('hide');
                $('#txtInvoiceNo').val('');
                $('#hdnInvoiceId').val('');
                $('#txtVRemarks').val('');
                $('#ModalVoid').modal('show');
            }
            else {
                navigator.notification.alert($('#hdnInvalAuth').val(), "", "neo ecr", "Ok");

                $('#ModalPrimaryUserPWDPopup').modal('show');
            }

        },
        error: function (e) {

        }
    });

}
function PriceSubChange() {
    window.localStorage.setItem('ctCheck', 1);
    if (window.localStorage.getItem('network') == '1') {
        if ($("#hdnPwdCheckStatus").val() == '1') {
            var myTab = document.getElementById('itemCarttable');

            var m_ = "";
            if ($('#txthdnItemId').val() != "") {
                document.getElementById('liQtyM').style.cssText = 'color:white;background:  #FF7276;';
                document.getElementById('liDisM').style.cssText = 'color:white;background: #FF7276;';
                document.getElementById('liPriceM').style.cssText = 'color:black;font-weight:bold;background: #FF7276;';
                for (i = 0; i < myTab.rows.length; i++) {
                    var objCells = myTab.rows.item(i).cells;
                    if ($('#txthdnItemId').val() == objCells.item(5).innerHTML) {
                        $("#txtAmount").attr("disabled", "disabled");
                        $("#txthdnItemId").attr("disabled", "disabled");
                        $("#txtQuantity").attr("disabled", "disabled");
                        $("#txtPrice").attr("disabled", "disabled");
                        $("#txtUOM").attr("disabled", "disabled");
                        $("#txtDiscount").attr("disabled", "disabled");
                        $("#txtPrice").val('');
                        //  $("#txtPrice").removeAttr("disabled");

                        $("#txthdnActiveColumn").val("3");
                        break;
                    }
                }
            }
            else {
                navigator.notification.alert($('#hdnPlzSelectItem').val(), "", "neo ecr", "Ok");

            }
        }
        else {
            $("#txtAUTPwd").focus();
            //$('#ModalPasswordPopup').modal('show');
            $('#ModalPasswordPopup').on('shown.bs.modal', function () {
                $('#txtAUTPwd').focus();
            });

            //$("#txtAUTPwd").focus();
            $('#ModalPasswordPopup').modal();
        }
    }
    else {
        var myTab = document.getElementById('itemCarttable');

        var m_ = "";
        if ($('#txthdnItemId').val() != "") {
            document.getElementById('liQtyM').style.cssText = 'color:white;background:  #FF7276;';
            document.getElementById('liDisM').style.cssText = 'color:white;background: #FF7276;';
            document.getElementById('liPriceM').style.cssText = 'color:black;font-weight:bold;background: #FF7276;';
            for (i = 0; i < myTab.rows.length; i++) {
                var objCells = myTab.rows.item(i).cells;
                if ($('#txthdnItemId').val() == objCells.item(5).innerHTML) {
                    $("#txtAmount").attr("disabled", "disabled");
                    $("#txthdnItemId").attr("disabled", "disabled");
                    $("#txtQuantity").attr("disabled", "disabled");
                    $("#txtPrice").attr("disabled", "disabled");
                    $("#txtUOM").attr("disabled", "disabled");
                    $("#txtDiscount").attr("disabled", "disabled");
                    $("#txtPrice").val('');
                    $("#txtPrice").removeAttr("disabled");

                    $("#txthdnActiveColumn").val("3");
                    break;
                }
            }
        }
        else {
            navigator.notification.alert($('#hdnPlzSelectItem').val(), "", "neo ecr", "Ok");
        }



    }
}
function DiscSubChange() {

    window.localStorage.setItem('ctCheck', 1);
    if ($("#hdnPwdCheckStatus").val() == '1') {
        //Code For Discount Change Mode  * Important
        var myTab = document.getElementById('itemCarttable');
        var m_ = "";

        if ($('#txthdnItemId').val() != "") {
            document.getElementById('liQtyM').style.cssText = 'color:white;background: #FF7276;';
            document.getElementById('liDisM').style.cssText = 'color:black;background: #FF7276;;font-weight:bold';
            document.getElementById('liPriceM').style.cssText = 'color:white;background: #FF7276;';
            for (i = 0; i < myTab.rows.length; i++) {
                var objCells = myTab.rows.item(i).cells;
                if ($('#txthdnItemId').val() == objCells.item(5).innerHTML) {
                    $("#txtAmount").attr("disabled", "disabled");
                    $("#txthdnItemId").attr("disabled", "disabled");
                    $("#txtQuantity").attr("disabled", "disabled");
                    $("#txtPrice").attr("disabled", "disabled");
                    $("#txtUOM").attr("disabled", "disabled");
                    $("#txtDiscount").attr("disabled", "disabled");
                    $("#txtDiscount").val('');
                    //  $("#txtDiscount").removeAttr("disabled");
                    $("#txthdnActiveColumn").val("2");
                    //  $("#txtDiscount").focus();
                    break;
                }
            }

        }
        else {
            navigator.notification.alert($('#hdnPlzSelectItem').val(), "", "neo ecr", "Ok");

        }
    }
    else {
        $("#txtAUTPwd").focus();
        $('#ModalPasswordPopup').modal('show');
    }
}
function btnCheckOKClick() {

    var password = document.getElementById('txtAUTPwd').value;
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        SubOrg_Id = hashSplit[1];
    }

    Jsonobj = [];
    var SubOrg = {
    };
    window.localStorage.setItem('ctCheck', 1);
    SubOrg.SubOrgID = $('#hdnBranchId').val();
    //  SubOrg.VD = vd_;
    if (window.localStorage.getItem('network') == '1') {
        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.VerifyPasswordAUTCheck,
            data: "password=" + password + "&VD=" + VD1(),
            success: function (resp) {
                if (resp == '100000') {
                    $("#hdnPwdCheckStatus").val("1");
                    $('#ModalPasswordPopup').modal('hide');
                }
                else {
                    navigator.notification.alert($('#hdnInvalAuth').val(), "", "neo ecr", "Ok");


                    $('#ModalPasswordPopup').modal('show');
                }

            },
            error: function (e) {

            }
        });
    }
    else {
        if (password == window.localStorage.getItem('Atpwd')) {
            $("#hdnPwdCheckStatus").val("1");
            $('#ModalPasswordPopup').modal('hide');
        }
        else {

            navigator.notification.alert($('#hdnInvalAuth').val(), "", "neo ecr", "Ok");
            $('#ModalPasswordPopup').modal('show');
        }

    }
}
function oneSubChange() {
    window.localStorage.setItem('ctCheck', 1);
    if ($('#txthdnItemId').val() != "") {
        if ($("#txthdnActiveColumn").val() == "1") {
            var strng = document.getElementById("txtQuantity").value;
            document.getElementById("txtQuantity").value = strng + "1";
            UpdateQTYSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "2") {
            var strng = document.getElementById("txtDiscount").value;
            document.getElementById("txtDiscount").value = strng + "1";
            UpdateDiscSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "3") {
            var strng = document.getElementById("txtPrice").value;
            document.getElementById("txtPrice").value = strng + "1";
            UpdatePriceSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "4") {
            var strng = document.getElementById("txtTendertrans").value;
            document.getElementById("txtTendertrans").value = strng + "1";
            changeCallKeyUp();
            $('#txtTendertrans').focus();
        }
    }
    else {
        navigator.notification.alert($('#hdnPlzSelectItem').val(), "", "neo ecr", "Ok");

    }
}
function TwoSubChange() {
    window.localStorage.setItem('ctCheck', 1);
    if ($('#txthdnItemId').val() != "") {
        if ($("#txthdnActiveColumn").val() == "1") {
            var strng = document.getElementById("txtQuantity").value;
            document.getElementById("txtQuantity").value = strng + "2";
            UpdateQTYSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "2") {
            var strng = document.getElementById("txtDiscount").value;
            document.getElementById("txtDiscount").value = strng + "2";
            UpdateDiscSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "3") {
            var strng = document.getElementById("txtPrice").value;
            document.getElementById("txtPrice").value = strng + "2";
            UpdatePriceSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "4") {
            var strng = document.getElementById("txtTendertrans").value;
            document.getElementById("txtTendertrans").value = strng + "2";
            changeCallKeyUp();
            $('#txtTendertrans').focus();
        }
    }
    else {
        navigator.notification.alert($('#hdnPlzSelectItem').val(), "", "neo ecr", "Ok");

    }
}
function ThreeSubChange() {
    window.localStorage.setItem('ctCheck', 1);
    if ($('#txthdnItemId').val() != "") {
        if ($("#txthdnActiveColumn").val() == "1") {
            var strng = document.getElementById("txtQuantity").value;
            document.getElementById("txtQuantity").value = strng + "3";
            UpdateQTYSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "2") {
            var strng = document.getElementById("txtDiscount").value;
            document.getElementById("txtDiscount").value = strng + "3";
            UpdateDiscSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "3") {
            var strng = document.getElementById("txtPrice").value;
            document.getElementById("txtPrice").value = strng + "3";
            UpdatePriceSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "4") {
            var strng = document.getElementById("txtTendertrans").value;
            document.getElementById("txtTendertrans").value = strng + "3";
            changeCallKeyUp();
            $('#txtTendertrans').focus();
        }
    }
    else {
        navigator.notification.alert($('#hdnPlzSelectItem').val(), "", "neo ecr", "Ok");

    }
}
function FourSubChange() {
    window.localStorage.setItem('ctCheck', 1);
    if ($('#txthdnItemId').val() != "") {
        if ($("#txthdnActiveColumn").val() == "1") {
            var strng = document.getElementById("txtQuantity").value;
            document.getElementById("txtQuantity").value = strng + "4";
            UpdateQTYSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "2") {
            var strng = document.getElementById("txtDiscount").value;
            document.getElementById("txtDiscount").value = strng + "4";
            UpdateDiscSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "3") {
            var strng = document.getElementById("txtPrice").value;
            document.getElementById("txtPrice").value = strng + "4";
            UpdatePriceSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "4") {
            var strng = document.getElementById("txtTendertrans").value;
            document.getElementById("txtTendertrans").value = strng + "4";
            changeCallKeyUp();
            $('#txtTendertrans').focus();
        }
    }
    else {
        navigator.notification.alert($('#hdnPlzSelectItem').val(), "", "neo ecr", "Ok");

    }
}
function FiveSubChange() {
    window.localStorage.setItem('ctCheck', 1);
    if ($('#txthdnItemId').val() != "") {
        if ($("#txthdnActiveColumn").val() == "1") {
            var strng = document.getElementById("txtQuantity").value;
            document.getElementById("txtQuantity").value = strng + "5";
            UpdateQTYSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "2") {
            var strng = document.getElementById("txtDiscount").value;
            document.getElementById("txtDiscount").value = strng + "5";
            UpdateDiscSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "3") {
            var strng = document.getElementById("txtPrice").value;
            document.getElementById("txtPrice").value = strng + "5";
            UpdatePriceSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "4") {
            var strng = document.getElementById("txtTendertrans").value;
            document.getElementById("txtTendertrans").value = strng + "5";
            changeCallKeyUp();
            $('#txtTendertrans').focus();
        }
    }
    else {
        navigator.notification.alert($('#hdnPlzSelectItem').val(), "", "neo ecr", "Ok");

    }
}

function SixSubChange() {
    window.localStorage.setItem('ctCheck', 1);
    if ($('#txthdnItemId').val() != "") {
        if ($("#txthdnActiveColumn").val() == "1") {
            var strng = document.getElementById("txtQuantity").value;
            document.getElementById("txtQuantity").value = strng + "6";
            UpdateQTYSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "2") {
            var strng = document.getElementById("txtDiscount").value;
            document.getElementById("txtDiscount").value = strng + "6";
            UpdateDiscSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "3") {
            var strng = document.getElementById("txtPrice").value;
            document.getElementById("txtPrice").value = strng + "6";
            UpdatePriceSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "4") {
            var strng = document.getElementById("txtTendertrans").value;
            document.getElementById("txtTendertrans").value = strng + "6";
            changeCallKeyUp();
            $('#txtTendertrans').focus();
        }
    }
    else {
        navigator.notification.alert($('#hdnPlzSelectItem').val(), "", "neo ecr", "Ok");

    }
}
function SevenSubChange() {
    window.localStorage.setItem('ctCheck', 1);
    if ($('#txthdnItemId').val() != "") {
        if ($("#txthdnActiveColumn").val() == "1") {
            var strng = document.getElementById("txtQuantity").value;
            document.getElementById("txtQuantity").value = strng + "7";
            UpdateQTYSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "2") {
            var strng = document.getElementById("txtDiscount").value;
            document.getElementById("txtDiscount").value = strng + "7";
            UpdateDiscSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "3") {
            var strng = document.getElementById("txtPrice").value;
            document.getElementById("txtPrice").value = strng + "7";
            UpdatePriceSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "4") {
            var strng = document.getElementById("txtTendertrans").value;
            document.getElementById("txtTendertrans").value = strng + "7";
            changeCallKeyUp();
            $('#txtTendertrans').focus();
        }
    }
    else {
        navigator.notification.alert($('#hdnPlzSelectItem').val(), "", "neo ecr", "Ok");

    }
}
function EightSubChange() {
    window.localStorage.setItem('ctCheck', 1);
    if ($('#txthdnItemId').val() != "") {
        if ($("#txthdnActiveColumn").val() == "1") {
            var strng = document.getElementById("txtQuantity").value;
            document.getElementById("txtQuantity").value = strng + "8";
            UpdateQTYSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "2") {
            var strng = document.getElementById("txtDiscount").value;
            document.getElementById("txtDiscount").value = strng + "8";
            UpdateDiscSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "3") {
            var strng = document.getElementById("txtPrice").value;
            document.getElementById("txtPrice").value = strng + "8";
            UpdatePriceSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "4") {
            var strng = document.getElementById("txtTendertrans").value;
            document.getElementById("txtTendertrans").value = strng + "8";
            changeCallKeyUp();
            $('#txtTendertrans').focus();
        }
    }
    else {
        navigator.notification.alert($('#hdnPlzSelectItem').val(), "", "neo ecr", "Ok");

    }
}
function NineSubChange() {
    window.localStorage.setItem('ctCheck', 1);
    if ($('#txthdnItemId').val() != "") {
        if ($("#txthdnActiveColumn").val() == "1") {
            var strng = document.getElementById("txtQuantity").value;
            document.getElementById("txtQuantity").value = strng + "9";
            UpdateQTYSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "2") {
            var strng = document.getElementById("txtDiscount").value;
            document.getElementById("txtDiscount").value = strng + "9";
            UpdateDiscSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "3") {
            var strng = document.getElementById("txtPrice").value;
            document.getElementById("txtPrice").value = strng + "9";
            UpdatePriceSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "4") {
            var strng = document.getElementById("txtTendertrans").value;
            document.getElementById("txtTendertrans").value = strng + "9";
            changeCallKeyUp();
            $('#txtTendertrans').focus();
        }
    }
    else {
        navigator.notification.alert($('#hdnPlzSelectItem').val(), "", "neo ecr", "Ok");

    }
}
function ZeroSubChange() {
    window.localStorage.setItem('ctCheck', 1);
    if ($('#txthdnItemId').val() != "") {
        if ($("#txthdnActiveColumn").val() == "1") {
            var strng = document.getElementById("txtQuantity").value;
            document.getElementById("txtQuantity").value = strng + "0";
            UpdateQTYSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "2") {
            var strng = document.getElementById("txtDiscount").value;
            document.getElementById("txtDiscount").value = strng + "0";
            UpdateDiscSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "3") {
            var strng = document.getElementById("txtPrice").value;
            if (Number(document.getElementById("txtPrice").value) != 0) {
                document.getElementById("txtPrice").value = strng + "0";
                UpdatePriceSubChange();
            } else {
                document.getElementById("txtAmount").value = '0.00';
                document.getElementById("txtPrice").value = '0'
                UpdatePriceSubChange();
            }
        }
        if ($("#txthdnActiveColumn").val() == "4") {
            var strng = document.getElementById("txtTendertrans").value;
            document.getElementById("txtTendertrans").value = strng + "0";
            changeCallKeyUp();
            $('#txtTendertrans').focus();
        }
    }
    else {
        navigator.notification.alert($('#hdnPlzSelectItem').val(), "", "neo ecr", "Ok");

    }
}
function dotSubChange() {
    if ($('#txthdnItemId').val() != "") {
        if ($("#txthdnActiveColumn").val() == "1") {
            var strng = document.getElementById("txtQuantity").value;
            document.getElementById("txtQuantity").value = strng + ".";
            UpdateQTYSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "2") {
            var strng = document.getElementById("txtDiscount").value;
            document.getElementById("txtDiscount").value = strng + ".";
            UpdateDiscSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "3") {
            var strng = document.getElementById("txtPrice").value;
            document.getElementById("txtPrice").value = strng + ".";
            UpdatePriceSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "4") {
            var strng = document.getElementById("txtTendertrans").value;
            document.getElementById("txtTendertrans").value = strng + ".";
            changeCallKeyUp();
            $('#txtTendertrans').focus();
        }
    }
    else {
        window.localStorage.setItem('ctCheck', 1);
        navigator.notification.alert($('#hdnPlzSelectItem').val(), "", "neo ecr", "Ok");

    }
}
function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}
function GenarateGustName() {
    var d = new Date();
    var n = d.valueOf();
    return 'G#' + n;
}
function GenarateGustId() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}
var popupSts;
function toNumberDecimalTwoPR(number) {
    window.localStorage.setItem('ctCheck', 1);
    var numberSplitd_ = number.toString().split('.');
    if (numberSplitd_.length > 1) {
        var subUpdatedNumber = '';
        var submNumberSplit = numberSplitd_[1];
        if (submNumberSplit.length >= 2) {
            if (submNumberSplit.length == 2) {
                submNumberSplit = submNumberSplit + '00';
                subUpdatedNumber = submNumberSplit.substring(0, 4);
                return (numberSplitd_[0] + '.' + subUpdatedNumber);

            }
            if (submNumberSplit.length == 3) {
                submNumberSplit = submNumberSplit + '0';
                subUpdatedNumber = submNumberSplit.substring(0, 4);
                return (numberSplitd_[0] + '.' + subUpdatedNumber);
            }
            if (submNumberSplit.length >= 4) {

                submNumberSplit = submNumberSplit;
                subUpdatedNumber = submNumberSplit.substring(0, 4);
                return (numberSplitd_[0] + '.' + subUpdatedNumber);
            }
        }
        else {
            return (number);
        }
    }
    else {
        return (number + ".00");
    }
}
function toNumberDecimalTwo(number_) {
    window.localStorage.setItem('ctCheck', 1);
    var numberSplitd_ = number_.toString().split('.');
    if (numberSplitd_.length > 1) {
        var subUpdatedNumber = '';
        var submNumberSplit = numberSplitd_[1];
        if (submNumberSplit.length >= 2) {
            if (submNumberSplit.length == 2) {

                submNumberSplit = submNumberSplit + '00';
                subUpdatedNumber = submNumberSplit.substring(0, 4);
                return Number(numberSplitd_[0] + '.' + subUpdatedNumber);
            }
            if (submNumberSplit.length == 3) {
                submNumberSplit = submNumberSplit + '0';
                subUpdatedNumber = submNumberSplit.substring(0, 4);
                return Number(numberSplitd_[0] + '.' + subUpdatedNumber);
            }
            if (submNumberSplit.length >= 4) {
                submNumberSplit = submNumberSplit;
                subUpdatedNumber = submNumberSplit.substring(0, 4);
                return Number(numberSplitd_[0] + '.' + subUpdatedNumber);
            }
        }
        else {
            return Number(number_);
        }
    }
    else {
        return Number(number_ + ".00");
    }
}
function okClose() {
    window.localStorage.setItem('ctCheck', 1);
    window.localStorage.setItem("DisplayDLTemp0", "0");
    window.localStorage.setItem("DisplayDLTemp", "0");
    window.localStorage.setItem("Display", "0");
    $('#lbl_TTpartTotal')[0].innerText = '0';
    $('#lblTenderConvertAmt').text('');
    window.localStorage.setItem('ctCheck', 1);

    $('#lblPPLamtTend')[0].innerText = '';

    $('#lblPPLamtRec')[0].innerText = '';

    $('#lblPPLamtChange')[0].innerText = '';
    //this.location.reload();
    $('#myModalpayFinal').hide();
    try {
        // this.location.reload();
    }
    catch (e) {

    }
}
var INVDataPushobjP20_; var INVDataPushobjP21_; var INVDataPushobjP24_; var InvoiceObhId; var INVDataPushobjVD_;
function OnSave() {

    var card = 0;
    var cardAmt = 0;
    var myTabPart = document.getElementById('tbl_tenderDyms');
    for (i = 0; i < myTabPart.rows.length; i++) {
        var objCells = myTabPart.rows.item(i).cells;
        try {
            if (parseFloat(objCells.item(4).innerHTML) > 0) { 
                var payMode_ = objCells.item(1).innerHTML.split('_'); 
                $('#hdnPayMode').val(payMode_[0]);
                if ($('#hdnPayMode').val().toUpperCase() == 'CREDITCARD') { 
                  
                    cardAmt = objCells.item(4).innerHTML;
                    card = 1;
                } 
            }
        }
        catch (e) {

        }
    }
    if (card == 0)
    {
        OnSaveMain();
    }
    else
    {

         
        if (parseFloat(cardAmt) > 0)
         {
            if(window.localStorage.getItem('CardTest')=='0')
            { 
                if( window.localStorage.getItem('PaymentTerminalName')!=null)
                {    
                    // window.localStorage.setItem("CardAmount", cardAmt);
                    SaleTransaction(cardAmt);
                }
                else
                {                
                    testSale();
                    $('#myModalBTPaymentTerminal').modal('show');
                }
            }
            else
            {
                OnSaveMain();
            }
        }
        else
        {
            OnSaveMain();
        }

    }
} 
 
function faPayInstance(resl) {  
    navigator.notification.alert(resl, "", "neo ecr", "Ok");
}
function SaleTransaction(amt) {
 
    var options = { dimBackground: true };
    SpinnerPlugin.activityStart("Please wait...", options);
    var bt = cordova.plugins.Apiplugin.gettriggerSaleTransaction; 
  
    bt = bt(suPayNew, faPayNew, "sale",amt, "01", "9876543210", "abc@123.com", "userd", "deviceid", "RRN", "Authcode", "MaskedPAN", "Additionaldata");
}
function suPayNew(resl)
{ 
    SpinnerPlugin.activityStop();
    // need to remove
  var mainRS="";
  var reslScan=resl.split('~');
  if(reslScan[1]=='000')
  {
    mainRS="Transaction Sucess";
  }
  else
  {
    mainRS="Transaction Faild";
  }
     if(mainRS!="Transaction Faild")
     {
        OnSaveMain();
     }
     else
     { 
        navigator.notification.alert($('#hdnPaymsgtwo').val(), "", "neo ecr", "Ok");  
        if (confirm('Do you want continue Offline Card Transaction'))
        { 
            OnSaveMain(); 
        } 
     }
}
function faPayNew(resl) {
    SpinnerPlugin.activityStop();
    navigator.notification.alert(resl, "", "neo ecr", "Ok"); 
}
function OnSaveMain()
 {  
    window.localStorage.setItem('ctCheck', 1);
    window.localStorage.setItem("DisplayQR", "0");
    window.localStorage.setItem("Display", "2");
    var a = valueAppendtoPOPUp();
    if (a == false) {
        return false;
    }
    if ($('#chkHomeDelivery').prop('checked') == true && $('#txtHomeDelPhNo').val() == '' && $('#txtHomeDelName').val() == '' && $('#txtHomeDelAddr').val() == '') {

        navigator.notification.alert($('#hdnPlzSelectItem').val(), "", "neo ecr", "Ok");
        return false;
    }
    var myTabPart = document.getElementById('tbl_tenderDyms');
    if (myTabPart.rows.length > 0) 
    {
        var val_ = parseFloat($('#lbl_TpartTotal')[0].innerText);
        val_ = 0;
        if (parseFloat(val_) >= 0) {
        }
        else {
            navigator.notification.alert($('#hdnAmtGrtrEqlAmtTend').val(), "", "neo ecr", "Ok");
            return false;
        }
       if ($('#hdnPayMode').val().toUpperCase() == 'CREDITNOTE') { 
         if (parseFloat($('#lbl_TpartTotal')[0].innerText) > $('#grdandTotal').val()) {
            navigator.notification.alert($('#hdncrdAmtCheck').val(), "", "neo ecr", "Ok");
            return false;
          }
        }
        if ($('#hdnTransModeforPay').val() == 'Other') {
            if ($('#txtTendertrans').val() == '') {
                navigator.notification.alert($('#hdnMsgTranRef').val(), "", "neo ecr", "Ok");

                return false;
            }
        }
        var BRANCHID = ''; var COMPANYID = '';
        var CURRENCYID = '';
        var HashValues = (window.localStorage.getItem("hashValues"));
        if (HashValues != null && HashValues != '') {
            var hashSplit = HashValues.split('~');
            CURRENCYID = hashSplit[16];
            COMPANYID = hashSplit[0];
            BRANCHID = hashSplit[1];
        }
        var strOptions = $('#hdn_CusId').val();
        var ItemData = ""; var CREATEDBY = window.localStorage.getItem('Sno');
        var CASHINVID = guid();
        var myTab = document.getElementById('tabletrpopbind');
        var ItemId = ""; var Qty_ = "0"; var BasePrice = "0"; var Amount = "0"; var Disc_ = "0"; var taxTypeP_ = ''; var BaseAmount = 0;
        var P20 = {
        }
        var P21 = {
        }
        var P24 = {
        }
        var I44 = {
        }

        $('#lblPPLamtTend')[0].innerText = $('#txtAmountpp').val();

        $('#lblPPLamtRec')[0].innerText = $('#lbl_TTpartTotal')[0].innerText;

        $('#lblPPLamtChange')[0].innerText = parseFloat(parseFloat($('#lbl_TTpartTotal')[0].innerText) - parseFloat($('#txtAmountpp').val(), 2)).toFixed(2);

        P20["CUSTOMERID"] = strOptions;
        P20["CASHINVID"] = CASHINVID;
        P20["CASHINVOICETYPE"] = '0';
        var cusSplit = $('#hdn_CusId').val().split('-$');
        if (cusSplit.length == 2) {
            P20["CUSTID"] = '';
        }
        else {
            P20["CUSTID"] = $('#hdn_CusId').val();
        }

        P20["CURRENCYID"] = CURRENCYID;
        P20["COUNTERID"] = window.localStorage.getItem('COUNTERID');
        P20["INVOICECATEGORY"] = '0';
        P20["TOTALBILLAMOUNT"] = $('#hdnPricepp').val();

        P20["DISCOUNT"] = $('#hdnDiscountpp').val();

        if (window.localStorage.getItem("roundoff") == "0") {
            P20["NETBILLAMOUNT"] = parseFloat($('#txtAmountpp').val(), 2);
            P20["ROUNDOFAMT"] = parseFloat($('#txtAmountpp').val(), 2);
        }
        else {

            P20["NETBILLAMOUNT"] = RoundInvAmount($('#txtAmountpp').val());
            P20["ROUNDOFAMT"] = RoundInvAmount($('#txtAmountpp').val());
        }

        P20["AMOUNTRECEIVED"] = $('#lbl_TTpartTotal')[0].innerText;
        P20["PAYMENTSTATUS"] = '6';
        P20["SHIFTID"] = window.localStorage.getItem('SHIFTID');
        P20["DESCRIPTON"] = '';
        P20["PAYMENTTERMSID"] = '';
        P20["LOYALTYCARDID"] = '';
        P20["DLIVERY_FROM"] = '';
        P20["DELIVERY_STATUS"] = 'CLOSED';
        P20["RECEIPT_STATUS"] = 'CLOSED';
        P20["INVOICE_STATUS"] = '';
        P20["TAXFORMULA"] = '';
        P20["BRANCHID"] = BRANCHID;
        P20["COMPANYID"] = COMPANYID;
        P20["CREATEDBY"] = CREATEDBY;
        P20["UPDATEDSITE"] = 'DBMS_REPUTIL.GLOBAL_NAME';
        P20["LANGID"] = 'en';
        P20["SALESPERSONID"] = window.localStorage.getItem('CashierID');

        P20["DELIVERYADDRESS"] = homeDeliveryAddress;
        P20["HOMEDELIVERY"] = $('#chkHomeDelivery').prop('checked') == true ? 'H' : 'O';
        var OflineInvoice = GetofInvoice();
        P20["OFINVOICE"] = OflineInvoice;

        objP20 = [];

        objP21 = [];

        var Tax_ = 0; var Tax_1 = 0; var taxPrintOffline = '';
        for (i = 1; i < myTab.rows.length; i++) {
            Tax_ = 0;
            P21 = {
            }
            var objCells = myTab.rows.item(i).cells;

            if (objCells.length > 0) {
                taxTypeP_ = objCells.item(16).innerHTML;
                if (objCells.item(6).innerHTML != '') {

                    if (taxTypeP_ != 'R') {
                        Tax_ = Number(objCells.item(7).innerHTML);
                        Tax_1 = Tax_1 + Number(objCells.item(7).innerHTML);
                        taxPrintOffline = taxPrintOffline + mapCode + "@" + Number(objCells.item(7).innerHTML) + "~";
                    }
                    else {
                        Tax_ = toNumberDecimalTwo(parseFloat(objCells.item(7).innerHTML).toFixed(8));
                        Tax_1 = Tax_1 + parseFloat(toNumberDecimalTwo(parseFloat(objCells.item(7).innerHTML).toFixed(8)));
                        taxPrintOffline = taxPrintOffline + mapCode + "@" + parseFloat(toNumberDecimalTwo(parseFloat(objCells.item(7).innerHTML).toFixed(8))) + "~";
                    }
                }
                ItemId = objCells.item(11).innerHTML.split('~')[0];
                Qty_ = objCells.item(12).innerHTML;
                Disc_ = objCells.item(13).innerHTML;
                Amount = objCells.item(14).innerHTML;


                BaseAmount = objCells.item(5).innerHTML;

                var mapCode = objCells.item(6).innerHTML;
                if (mapCode == 'undefined') {
                    mapCode = '';
                }
                BasePrice = objCells.item(4).innerHTML;
                //BasePrice = $('#td_' + ItemId).val();
                P21["RGUID"] = guid();
                P21["CASHINVID"] = CASHINVID
                P21["ITEMID"] = ItemId;
                P21["ITEMCATEGORYID"] = Qty_;
                P21["UOMID"] = objCells.item(15).innerHTML;
                P21["DISCOUNT"] = Disc_; //Line Item Discount
                P21["ITEMTAX"] = Tax_; //Line Item TaxPercent
                P21["QUANTITY"] = Qty_;
                P21["FREE_QUANTITY"] = '';
                P21["PRICE"] = BasePrice;
                //P21["PRICE"] = BasePrice;
                P21["Total"] = Amount;
                P21["mapCode"] = mapCode;
                P21["SALESPERSONID"] = window.localStorage.getItem('CashierID');
                P21["RET_QTY"] = "0";
                P21["RET_FREE_QTY"] = "0";
                P21["ITEMDISC"] = '';
                P21["TAXTYPE"] = taxTypeP_;
                P21["AMOUNT"] = BaseAmount;
                P21["BRANCHID"] = BRANCHID;
                P21["COMPANYID"] = COMPANYID;
                P21["CREATEDBY"] = CREATEDBY;
                P24["UPDATEDSITE"] = 'DBMS_REPUTIL.GLOBAL_NAME';
                P24["LANGID"] = 'en';
                P21["ITEMTYPE_ID"] = objCells.item(17).innerHTML;
                P21["ITEMTYPE_MODE"] = objCells.item(18).innerHTML;
                objP21.push(P21);
            }
        }
        P20["TOTALTAXAMOUNT"] = Tax_1;
        objP20.push(P20);
        //P21 - Data End


        //---------------------------------------------------------------------------------------------------------------------------- Arun --

        //---------------------------------------------------------------------------------------------------------------------------- Arun --

        var currencyExdCheck_ = 0;
        //P24 - Data Prepare START
        objP24 = [];

        var myTabPart = document.getElementById('tbl_tenderDyms');
        for (i = 0; i < myTabPart.rows.length; i++) {
            var objCells = myTabPart.rows.item(i).cells;
            try {
                if (parseFloat(objCells.item(4).innerHTML) > 0) {
                    P24 = {
                    }

                    var payMode_ = objCells.item(1).innerHTML.split('_');

                    P24["CASHINVID"] = CASHINVID
                    //P24["TENDERTYPE"] = $('#hdnPayMode').val();
                    P24["TENDERTYPE"] = payMode_[0];
                    $('#hdnPayMode').val(payMode_[0]);
                    if ($('#hdnPayMode').val().toUpperCase() == 'CREDITCARD') {
                        P24["TENDERTYPE"] = 'Credit Card';
                    }
                    if ($('#hdnPayMode').val().toUpperCase() == 'CREDITNOTE') {
                        P24["TENDERTYPE"] = 'Credit Note';
                    }
                    if ($('#hdnPayMode').val().toUpperCase() == 'DEBITCARD') {
                        P24["TENDERTYPE"] = 'Debit Card';
                    }
                    P24["BANKNAME"] = $('#hdnPayModeType').val();
                    P24["CARDNAME"] = '';
                    P24["CARDTYPE"] = '';

                    if (payMode_.length >= 1) {
                        P24["INSTRUMENTNUMBER"] = payMode_[1];
                    }
                    else
                        P24["INSTRUMENTNUMBER"] = '';

                    P24["TOTAL_AMOUNT"] = objCells.item(3).innerHTML;
                    //P24["TOTAL_AMOUNT"] = BaseAmount;

                    P24["TENDERID"] = objCells.item(5).innerHTML;
                    P24["COUNTERTYPE"] = window.localStorage.getItem('COUNTERTYPE');
                    //P24["CURRENCYID"] = CURRENCYID;
                    P24["CURRENCYID"] = objCells.item(6).innerHTML;
                    //P24["AMOUNT"] = $('#hdnAmountpp').val();;
                    P24["AMOUNT"] = objCells.item(4).innerHTML;
                    P24["CLEARINGCHARGES"] = '';
                    P24["EXCHANGERATE"] = objCells.item(2).innerHTML;
                    P24["TRANSTYPE"] = 'INVOICE';
                    P24["COUNTERID"] = window.localStorage.getItem('COUNTERID');
                    P24["SHIFTID"] = window.localStorage.getItem('SHIFTID');


                    if (window.localStorage.getItem("roundoff") == "0") {
                        if (parseFloat(parseFloat($('#lbl_TTpartTotal')[0].innerText) - parseFloat($('#txtAmountpp').val()).toFixed(2)) > 0)
                            if (payMode_[0].toUpperCase() == 'CASH') {
                                if ($('#hdnTemBasePriceEX').val() == '1') {
                                    P24["CHANGE"] = parseFloat(parseFloat($('#lbl_TpartTotal')[0].innerText) / parseFloat(objCells.item(2).innerHTML)).toFixed(4);
                                }
                                else {
                                    P24["CHANGE"] = parseFloat(parseFloat(parseFloat($('#lbl_TTpartTotal')[0].innerText) - parseFloat($('#txtAmountpp').val()).toFixed(2)) / parseFloat(objCells.item(2).innerHTML)).toFixed(4);
                                }
                            }
                            else {
                                P24["CHANGE"] = '0';
                                currencyExdCheck_ = parseFloat(currencyExdCheck_) + parseFloat(objCells.item(4).innerHTML);
                            }
                        else
                            P24["CHANGE"] = '0';
                    }

                    else {
                        if (parseFloat(parseFloat($('#lbl_TTpartTotal')[0].innerText) - RoundInvAmount($('#txtAmountpp').val())) > 0)
                            if (payMode_[0].toUpperCase() == 'CASH') {
                                if ($('#hdnTemBasePriceEX').val() == '1') {
                                    P24["CHANGE"] = parseFloat(parseFloat($('#lbl_TpartTotal')[0].innerText) / parseFloat(objCells.item(2).innerHTML)).toFixed(4);
                                }
                                else {
                                    P24["CHANGE"] = parseFloat(parseFloat(parseFloat($('#lbl_TTpartTotal')[0].innerText) - RoundInvAmount($('#txtAmountpp').val())) / parseFloat(objCells.item(2).innerHTML)).toFixed(4);

                                }
                            }
                            else {
                                P24["CHANGE"] = '0';
                                currencyExdCheck_ = parseFloat(currencyExdCheck_) + parseFloat(objCells.item(4).innerHTML);

                            }
                        else
                            P24["CHANGE"] = '0';
                    }
                    P24["UPDATEDSITE"] = 'DBMS_REPUTIL.GLOBAL_NAME';
                    P24["LANGID"] = 'en';
                    P24["BRANCHID"] = BRANCHID;
                    P24["COMPANYID"] = COMPANYID;
                    P24["CREATEDBY"] = CREATEDBY;
                    objP24.push(P24);
                }
            }
            catch (e) {

            }
        }

        //P24 - Data End

        var myTab = document.getElementById('tbl_tenderDyms');
        var mVal_ = "0";
        // LOOP THROUGH EACH ROW OF THE TABLE AFTER HEADER.
        for (i = 0; i < myTab.rows.length; i++) {
            // GET THE CELLS COLLECTION OF THE CURRENT ROW.
            var objCells = myTab.rows.item(i).cells;
            if (objCells.length > 0) {
                mVal_ = parseFloat(mVal_) + parseFloat(objCells.item(4).innerHTML);
                mVal_ = parseFloat(mVal_).toFixed(2);
            }
        }
        //var valGet_ = Math.round(mVal_) - Math.round($('#txtAmountpp').val());
        var valGet_ = 0;
        //if (window.localStorage.getItem("roundoff") == "0") {
        valGet_ = parseFloat(mVal_) - parseFloat($('#txtAmountpp').val());

        if (valGet_ < 0) {

            navigator.notification.alert($('#hdnPlentrValAmount').val(), "", "neo ecr", "Ok");

            return false;
            //}
        }

        var InvoiceParams = new Object();
        InvoiceParams.P20 = JSON.stringify(objP20);
        InvoiceParams.P21 = JSON.stringify(objP21);
        InvoiceParams.P24 = JSON.stringify(objP24);
        InvoiceParams.VD = VD1();

        INVDataPushobjP20_ = objP20;
        INVDataPushobjP21_ = objP21;
        INVDataPushobjP24_ = objP24;

        //Put the customer object into a container
        var ComplexObject = new Object();
        //The property name "WebMethodArgName" must match the web method argument "WebMethodArgName"!
        ComplexObject.Invoice = InvoiceParams;

        //Stringify and verify the object is foramtted as expected
        var data = JSON.stringify(ComplexObject);
        var img = document.getElementById('loader');
        img.style.display = "none";
        //$('#loader').show();
        var options = { dimBackground: true };
        SpinnerPlugin.activityStart("Please wait...", options); 
        if (window.localStorage.getItem('network') == '1') {
            $.ajax({
                type: 'POST',
                url: CashierLite.Settings.InvoiceSave,
                data: data,
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                success: function (resp) {

                    SpinnerPlugin.activityStop();
                    img.style.display = "none";

                    var result = resp.InvoiceSaveResult.split(',');

                    $('#hdnInvoiceId').val(result[1]);

                    window.localStorage.setItem("DisplayDLTemp", "1");

                    if (result[0] == "100000") {
                        if ($('#hdnISPRINT').val() == 'true') {
                            //$('#txtItemScan').focus();
                            window.localStorage.setItem('Inv', '1');
                            //$('#lbl_InvoiceNoPrint').text('Invoice No. : ' + result[1]);
                            window.localStorage.setItem("Tax", result[3]);
                            $('#lbl_InvoiceNoPrint').text($('#hdnInvoice').val() + ' : ' + result[1]);
                            window.localStorage.setItem('InvoiceId', result[1]);
                            window.localStorage.setItem('Connection', result[4]);
                            //print('t', result[1], result[5]);
                            print('t', result[1], result[5]);
                            //$('#txtTendertransref').val('')
                            $('#lblPPFinalInvoiceNo')[0].innerText = $('#hdnInvNopopup').val()+' : ' + result[1];

                        }
                        else {
                            //$('#txtItemScan').focus();
                            window.localStorage.setItem('Inv', '1');
                            window.localStorage.setItem('InvoiceId', '');
                            window.localStorage.setItem('Connection', '');
                            window.localStorage.setItem('InvoiceId', result[1]);
                            window.localStorage.setItem('Connection', result[4]);
                            //$('#lbl_InvoiceNoPrint').text('Invoice No. : ' + result[1]);
                            $('#lbl_InvoiceNoPrint').text($('#hdnInvoice').val() + ' : ' + result[1]);
                            window.localStorage.setItem("Tax", result[3]);
                            print('f', result[1], result[5]);
                            //$('#txtTendertransref').val('')
                            $('#lblPPFinalInvoiceNo')[0].innerText = $('#hdnInvNopopup').val()+' : ' + result[1];
                        }
                        //alert('Invoice Number:' + result[1]);

                    }
                    $('#txtTendertransref').val('')
                },
                error: function (e) {

                    window.localStorage.setItem("Display", "0");
                    SpinnerPlugin.activityStop();
                    OflineInvoice = GetofInvoice();
                    var tx = db.transaction("MyObjectStore", "readwrite");
                    var store = tx.objectStore("MyObjectStore");
                    store.put({
                        id: CASHINVID, InvoiceData: data
                    });
                    window.localStorage.setItem('Inv', '1');
                    $('#lbl_InvoiceNoPrint').text($('#hdnInvoice').val() + ' : ' + OflineInvoice);
                    window.localStorage.setItem("Tax", false);
                    print('t');
                    window.l - ocalStorage.setItem("Tax", "");
                    removeDraft(strOptions);
                    $('#hdn_CusId').val('');
                    $('#txtCusName').val('');
                    ClearFileds();
                    $("#itemCarttable tr").remove();
                    $('#grdandTotal').text('0.00');
                    $('#totalitemscw').text('0');
                    $('#myModalpay').modal('hide');
                    $('#lbl_TTpartTotal')[0].innerText = '0.00';
                    $("#hdnPwdCheckStatus").val('0');
                    $('#hdn_CusId').val('');
                    $('#chkHomeDelivery').prop('checked', false);
                    $('#myTable li a').removeAttr('onclick');
                }
            });
        }
        else {
            SpinnerPlugin.activityStop();
            img.style.display = "none";
            OflineInvoice = GetofInvoice();
            InvoiceObhId = OflineInvoice;

            var dbSql = window.openDatabase("MyInvoice", '1.0', 'CashierLiteDB', 2 * 1024 * 1024);
            dbSql.transaction(DBPushINV, errorCB, successCB);
            window.localStorage.setItem("Tax", taxPrintOffline);
            window.localStorage.setItem('Inv', '1');
            $('#lbl_InvoiceNoPrint').text($('#hdnInvoice').val() + ' : ' + OflineInvoice);
            //window.localStorage.setItem("Tax", false);
            printOffline('t');
            window.localStorage.setItem("Tax", "");
            removeDraft(strOptions);
            $('#hdn_CusId').val('');
            $('#txtCusName').val('');
            ClearFileds();
            $("#itemCarttable tr").remove();
            $('#grdandTotal').text('0.00');
            $('#totalitemscw').text('0');
            $('#myModalpay').modal('hide');
            $("#tbl_tenderDyms tr").remove();
            $('#lbl_TTpartTotal')[0].innerText = '0';
            $("#hdnPwdCheckStatus").val('0');
            $('#hdn_CusId').val('');
            $('#chkHomeDelivery').prop('checked', false);
            $('#myTable li a').removeAttr('onclick');
            $('#txtTendertrans').val('');
            $('#lblTenderConvertAmt').text('');
            $('#lblPPFinalInvoiceNo')[0].innerText =$('#hdnInvNopopup').val()+' : ' + OflineInvoice;
        }
    }
    else {
        navigator.notification.alert($('#hdnPlzSelectTranType').val(), "", "neo ecr", "Ok");

    }
}
function DBPushINV(tx) {
    tx.executeSql('CREATE TABLE IF NOT EXISTS InvoiceDataTemp (InvoiceId )');
    tx.executeSql('INSERT INTO InvoiceDataTemp (InvoiceId) VALUES ("' + InvoiceObhId + '")');
    window.localStorage.setItem(InvoiceObhId + "_P20", JSON.stringify(INVDataPushobjP20_));
    window.localStorage.setItem(InvoiceObhId + "_P21", JSON.stringify(INVDataPushobjP21_));
    window.localStorage.setItem(InvoiceObhId + "_P24", JSON.stringify(INVDataPushobjP24_));
}

function errorCB(tx, err) {

}
function successCB() {

}

function printOffline(val_) {
    window.localStorage.setItem('ctCheck', 1);
    var h1_ = '';
    var itemData_ = '';
    var rows = $('#tabletrpopbind tbody >tr');
    var invItemsCount_ = 0;
    var invQtyCount_ = 0;
    var taxAmt_ = 0;
    for (var i = 0; i < rows.length; i++) {
        if (rows[i].innerHTML !== '') {
            var Columns = $(rows[i]).find('td');
            invItemsCount_ = invItemsCount_ + 1;
            invQtyCount_ = invQtyCount_ + parseFloat($(Columns[2]).html());
        }
    }


    if (window.localStorage.getItem("Tax")) {
        var TaxSplit_ = window.localStorage.getItem("Tax").split('~');
        for (var v_ = 0; v_ < TaxSplit_.length; v_++) {
            var tax1_ = TaxSplit_[v_].split('@');
            if (tax1_.length == 2) {
                if (tax1_[1] != '') {
                    taxAmt_ = parseFloat(taxAmt_) + parseFloat(tax1_[1]);

                }
            }
        }
    }
    for (var i = 0; i < rows.length; i++) {
        if (rows[i].innerHTML !== '') {
            var Columns = $(rows[i]).find('td');
            var ItemId = $(Columns[11]).html().split('~')[0];
            var nameOffline_ = window.localStorage.getItem(ItemId).split('~')[0];
            var aliasOffline_ = window.localStorage.getItem(ItemId).split('~')[1];
            window.localStorage.removeItem(ItemId);


            //English
            if (JSON.parse(localStorage.getItem('PrintOffline')).E == "0") {
                if (JSON.parse(localStorage.getItem('PrintOffline')).I == "1") {
                    if (nameOffline_.length < 42) {
                        if (aliasOffline_.length == 0) {
                            itemData_ = itemData_ + nameOffline_ + "\n"
                                + getspace(48 - ($(Columns[2]).html().length + getspace(10 - parseFloat($(Columns[4]).html()).toFixed(2).length).length + (parseFloat($(Columns[4]).html())).toFixed(2).length + getspace(14 - parseFloat($(Columns[8]).html()).toFixed(2).length).length + parseFloat($(Columns[8]).html()).toFixed(2).length)) +
                                $(Columns[2]).html() + getspace(10 - parseFloat($(Columns[4]).html()).toFixed(2).length) +
                                (parseFloat($(Columns[4]).html())).toFixed(2) + getspace(14 - parseFloat($(Columns[8]).html()).toFixed(2).length) + parseFloat($(Columns[8]).html()).toFixed(2) + "\n";
                        }
                        else if (aliasOffline_.length < 42) {
                            itemData_ = itemData_ + nameOffline_ + "\n" + aliasOffline_ + "\n"
                                + getspace(48 - ($(Columns[2]).html().length + getspace(10 - parseFloat($(Columns[4]).html()).toFixed(2).length).length + (parseFloat($(Columns[4]).html())).toFixed(2).length + getspace(14 - parseFloat($(Columns[8]).html()).toFixed(2).length).length + parseFloat($(Columns[8]).html()).toFixed(2).length)) +
                                $(Columns[2]).html() + getspace(10 - parseFloat($(Columns[4]).html()).toFixed(2).length) +
                                (parseFloat($(Columns[4]).html())).toFixed(2) + getspace(14 - parseFloat($(Columns[8]).html()).toFixed(2).length) + parseFloat($(Columns[8]).html()).toFixed(2) + "\n";
                        }
                        else {
                            itemData_ = itemData_ + nameOffline_ + "\n" + aliasOffline_.substring(0, 42) + '...' + "\n"
                                + getspace(48 - ($(Columns[2]).html().length + getspace(10 - parseFloat($(Columns[4]).html()).toFixed(2).length).length + (parseFloat($(Columns[4]).html())).toFixed(2).length + getspace(14 - parseFloat($(Columns[8]).html()).toFixed(2).length).length + parseFloat($(Columns[8]).html()).toFixed(2).length)) +
                                $(Columns[2]).html() + getspace(10 - parseFloat($(Columns[4]).html()).toFixed(2).length) +
                                (parseFloat($(Columns[4]).html())).toFixed(2) + getspace(14 - parseFloat($(Columns[8]).html()).toFixed(2).length) + parseFloat($(Columns[8]).html()).toFixed(2) + "\n";
                        }
                    }
                    else {
                        if (aliasOffline_.length == 0) {
                            itemData_ = itemData_ + nameOffline_.substring(0, 42) + '...' + "\n"
                                + getspace(48 - ($(Columns[2]).html().length + getspace(10 - parseFloat($(Columns[4]).html()).toFixed(2).length).length + (parseFloat($(Columns[4]).html())).toFixed(2).length + getspace(14 - parseFloat($(Columns[8]).html()).toFixed(2).length).length + parseFloat($(Columns[8]).html()).toFixed(2).length)) +
                                $(Columns[2]).html() + getspace(10 - parseFloat($(Columns[4]).html()).toFixed(2).length) +
                                (parseFloat($(Columns[4]).html())).toFixed(2) + getspace(14 - parseFloat($(Columns[8]).html()).toFixed(2).length) + parseFloat($(Columns[8]).html()).toFixed(2) + "\n";
                        }
                        else if (aliasOffline_.length < 42) {
                            itemData_ = itemData_ + nameOffline_.substring(0, 42) + '...' + "\n" + aliasOffline_ + "\n"
                                + getspace(48 - ($(Columns[2]).html().length + getspace(10 - parseFloat($(Columns[4]).html()).toFixed(2).length).length + (parseFloat($(Columns[4]).html())).toFixed(2).length + getspace(14 - parseFloat($(Columns[8]).html()).toFixed(2).length).length + parseFloat($(Columns[8]).html()).toFixed(2).length)) +
                                $(Columns[2]).html() + getspace(10 - parseFloat($(Columns[4]).html()).toFixed(2).length) +
                                (parseFloat($(Columns[4]).html())).toFixed(2) + getspace(14 - parseFloat($(Columns[8]).html()).toFixed(2).length) + parseFloat($(Columns[8]).html()).toFixed(2) + "\n";
                        }
                        else {
                            itemData_ = itemData_ + nameOffline_.substring(0, 42) + '...' + "\n" + aliasOffline_.substring(0, 42) + '...' + "\n" + getspace(48 - ($(Columns[2]).html().length + getspace(10 - parseFloat($(Columns[4]).html()).toFixed(2).length).length + (parseFloat($(Columns[4]).html())).toFixed(2).length + getspace(14 - parseFloat($(Columns[8]).html()).toFixed(2).length).length + parseFloat($(Columns[8]).html()).toFixed(2).length)) +
                                $(Columns[2]).html() + getspace(10 - parseFloat($(Columns[4]).html()).toFixed(2).length) +
                                (parseFloat($(Columns[4]).html())).toFixed(2) + getspace(14 - parseFloat($(Columns[8]).html()).toFixed(2).length) + parseFloat($(Columns[8]).html()).toFixed(2) + "\n";
                        }
                    }


                }
                else {
                    if (nameOffline_.length < 42) {
                        itemData_ = itemData_ + nameOffline_ + "\n"
                            + getspace(48 - ($(Columns[2]).html().length + getspace(10 - parseFloat($(Columns[4]).html()).toFixed(2).length).length + (parseFloat($(Columns[4]).html())).toFixed(2).length + getspace(14 - parseFloat($(Columns[8]).html()).toFixed(2).length).length + parseFloat($(Columns[8]).html()).toFixed(2).length)) +
                            $(Columns[2]).html() + getspace(10 - parseFloat($(Columns[4]).html()).toFixed(2).length) +
                            (parseFloat($(Columns[4]).html())).toFixed(2) + getspace(14 - parseFloat($(Columns[8]).html()).toFixed(2).length) + parseFloat($(Columns[8]).html()).toFixed(2) + "\n";
                    }
                    else {

                        itemData_ = itemData_ + nameOffline_.substring(0, 42) + '...' + "\n"
                            + getspace(48 - ($(Columns[2]).html().length + getspace(10 - parseFloat($(Columns[4]).html()).toFixed(2).length).length + (parseFloat($(Columns[4]).html())).toFixed(2).length + getspace(14 - parseFloat($(Columns[8]).html()).toFixed(2).length).length + parseFloat($(Columns[8]).html()).toFixed(2).length)) +
                            $(Columns[2]).html() + getspace(10 - parseFloat($(Columns[4]).html()).toFixed(2).length) +
                            (parseFloat($(Columns[4]).html())).toFixed(2) + getspace(14 - parseFloat($(Columns[8]).html()).toFixed(2).length) + parseFloat($(Columns[8]).html()).toFixed(2) + "\n";

                    }
                }




                try {

                    var CurDate = new Date();
                    var min = CurDate.getMinutes().toString().length == 1 ? '0' + CurDate.getMinutes() : CurDate.getMinutes();
                    var dat_ = (CurDate.getDate() < 10 ? '0' : '') + CurDate.getDate() + '-' + ((CurDate.getMonth() + 1) < 10 ? '0' : '') + (CurDate.getMonth() + 1) + '-' + CurDate.getFullYear() + ' ' + CurDate.getHours() + ':' + min;

                    h1_ = "\n" + "Date             : " + dat_ +" :   تاريخ"+ "\n"
                        + "Counter          : " + window.localStorage.getItem('COUNTERNAME')+"           :   عداد"  + "\n"
                        + "------------------------------------------------" + "\n" +
                        "Invoice No.      : " + InvoiceObhId +"   :      رقم الفاتورة"+ "\n" +
                        "Total Items      : " + invItemsCount_ +"         :    إجمالي الأصناف"+ "\n" +
                        "Total Qty        : " + parseFloat(invQtyCount_).toFixed(2) +"      :      إجمالي الكمية"+ "\n" +
                        "------------------------------------------------" +
                        "Item                Qty       Rate      Amount  " +
                        "------------------------------------------------" +
                        itemData_
                        + "------------------------------------------------" + "\n" +
                        "Disc           : " + parseFloat($('#txtDiscountpp').val()).toFixed(2) +"      :               خصم"+ "\n" +
                        "Base Total     : " + parseFloat($('#txtPricepp').val()).toFixed(2) +"      :     الإجمالي الأساسي"+ "\n" +
                        "VAT            : " + parseFloat(taxAmt_).toFixed(2) +"      :  ضريبة القمية المضافة"+ "\n" +
                        "Net Total      : " + parseFloat(parseFloat($('#txtAmountpp').val())).toFixed(2) +"    :         الإجمالي الصافي"+ "\n" +
                        "------------------------------------------------" + "\n" +
                        "                                        SAR/﷼   " +
                        "Total          : " + getspace(5 - parseFloat($('#txtAmountpp').val()).toFixed(2).length) + parseFloat($('#txtAmountpp').val()).toFixed(2) +"     :          الإجمالي" + "\n" +
                        "Paid           : " + getspace(5 - parseFloat($('#lbl_TTpartTotal')[0].innerText).toFixed(2).length) + parseFloat($('#lbl_TTpartTotal')[0].innerText).toFixed(2) +"     :          تم تسديده" + "\n" +
                        "Change         : " +getspace(5 - ((parseFloat($('#lbl_TTpartTotal')[0].innerText).toFixed(2) - (parseFloat($('#txtAmountpp').val()).toFixed(2))).toFixed(2)).length) + (parseFloat($('#lbl_TTpartTotal')[0].innerText).toFixed(2) - (parseFloat($('#txtAmountpp').val()).toFixed(2))).toFixed(2) +"     :            الباقي" + "\n";

                  /*  h1_ = "\n" + "Date             : " + dat_ + "\n"
                        + "Counter          : " + window.localStorage.getItem('COUNTERNAME') + "\n"
                        + "------------------------------------------------" + "\n" +
                        "Invoice No.      : " + InvoiceObhId + "\n" +
                        "Total Items      : " + invItemsCount_ + "\n" +
                        "Total Qty        : " + parseFloat(invQtyCount_).toFixed(2) + "\n" +
                        "------------------------------------------------" +
                        "Item                Qty       Rate      Amount  " +
                        "------------------------------------------------" +
                        itemData_
                        + "------------------------------------------------" +
                        "Disc             : " + parseFloat($('#txtDiscountpp').val()).toFixed(2) + "\n" +
                        "Base Total       : " + parseFloat($('#txtPricepp').val()).toFixed(2) + "\n" +
                        "VAT              : " + parseFloat(taxAmt_).toFixed(2) + "\n" +
                        "Net Total        : " + parseFloat(parseFloat($('#txtAmountpp').val())).toFixed(2) + "\n" +
                        "------------------------------------------------" +
                        "                                          SAR   " +
                        "Total" + getspace(43 - parseFloat($('#txtAmountpp').val()).toFixed(2).length) + parseFloat($('#txtAmountpp').val()).toFixed(2) + "\n" +

                        "Paid" + getspace(44 - parseFloat($('#lbl_TTpartTotal')[0].innerText).toFixed(2).length) + parseFloat($('#lbl_TTpartTotal')[0].innerText).toFixed(2) + "\n" +
                        "Change" + getspace(42 - ((parseFloat($('#lbl_TTpartTotal')[0].innerText).toFixed(2) - (parseFloat($('#txtAmountpp').val()).toFixed(2))).toFixed(2)).length) + (parseFloat($('#lbl_TTpartTotal')[0].innerText).toFixed(2) - (parseFloat($('#txtAmountpp').val()).toFixed(2))).toFixed(2) + "\n";*/



                } catch (a) {

                }
            }
            else {
                //Local Lang

                var nameOfflineswap_ = nameOffline_;
                var aliasOfflineswap_ = aliasOffline_;
                nameOffline_ = aliasOfflineswap_;
                aliasOffline_ = nameOfflineswap_;
                //if (aliasOffline_.length == 0) {
                if (JSON.parse(localStorage.getItem('PrintOffline')).I == "1") {
                    if (nameOffline_.length < 42) {
                        if (aliasOffline_.length == 0) {
                            itemData_ = itemData_ + nameOffline_ + getspace(48 - nameOffline_.length) + "\n"

                                + (parseFloat($(Columns[8]).html())).toFixed(2) + getspace(14 - parseFloat($(Columns[8]).html()).toFixed(2).length) + parseFloat($(Columns[4]).html()).toFixed(2)

                                + getspace(38 - ($(Columns[2]).html().length + getspace(10 - parseFloat($(Columns[4]).html()).toFixed(2).length).length + (parseFloat($(Columns[4]).html())).toFixed(2).length + getspace(14 - parseFloat($(Columns[8]).html()).toFixed(2).length).length + parseFloat($(Columns[8]).html()).toFixed(2).length))

                                + $(Columns[2]).html() + getspace(10 - parseFloat($(Columns[2]).html()).toFixed(2).length)
                                + "\n";
                        }
                        else {
                            if (aliasOffline_.length < 40) {


                                itemData_ = itemData_ + nameOffline_ + getspace(48 - nameOffline_.length) + "\n"
                                    + aliasOffline_ + getspace(48 - aliasOffline_.length) + "\n"
                                    + (parseFloat($(Columns[8]).html())).toFixed(2) + getspace(14 - parseFloat($(Columns[8]).html()).toFixed(2).length) + parseFloat($(Columns[4]).html()).toFixed(2)

                                    + getspace(38 - ($(Columns[2]).html().length + getspace(10 - parseFloat($(Columns[4]).html()).toFixed(2).length).length + (parseFloat($(Columns[4]).html())).toFixed(2).length + getspace(14 - parseFloat($(Columns[8]).html()).toFixed(2).length).length + parseFloat($(Columns[8]).html()).toFixed(2).length))

                                    + $(Columns[2]).html() + getspace(10 - parseFloat($(Columns[2]).html()).toFixed(2).length)


                                    + "\n";
                            }
                            else {
                                itemData_ = itemData_ + nameOffline_ + getspace(48 - nameOffline_.length) + "\n"
                                    + aliasOffline_.substring(0, 40) + getspace(48 - aliasOffline_.length) + '...' + "\n"


                                    + (parseFloat($(Columns[8]).html())).toFixed(2) + getspace(14 - parseFloat($(Columns[8]).html()).toFixed(2).length) + parseFloat($(Columns[4]).html()).toFixed(2)

                                    + getspace(38 - ($(Columns[2]).html().length + getspace(10 - parseFloat($(Columns[4]).html()).toFixed(2).length).length + (parseFloat($(Columns[4]).html())).toFixed(2).length + getspace(14 - parseFloat($(Columns[8]).html()).toFixed(2).length).length + parseFloat($(Columns[8]).html()).toFixed(2).length))

                                    + $(Columns[2]).html() + getspace(10 - parseFloat($(Columns[2]).html()).toFixed(2).length)
                                    + "\n";
                            }

                        }
                    }
                    else {
                        if (aliasOffline_.length == 0) {
                            itemData_ = itemData_ + nameOffline_.substring(0, 40) + '...' + getspace(48 - nameOffline_.substring(0, 40).length) + "\n"


                                + (parseFloat($(Columns[8]).html())).toFixed(2) + getspace(14 - parseFloat($(Columns[8]).html()).toFixed(2).length) + parseFloat($(Columns[4]).html()).toFixed(2)

                                + getspace(38 - ($(Columns[2]).html().length + getspace(10 - parseFloat($(Columns[4]).html()).toFixed(2).length).length + (parseFloat($(Columns[4]).html())).toFixed(2).length + getspace(14 - parseFloat($(Columns[8]).html()).toFixed(2).length).length + parseFloat($(Columns[8]).html()).toFixed(2).length))

                                + $(Columns[2]).html() + getspace(10 - parseFloat($(Columns[2]).html()).toFixed(2).length)
                                + "\n";
                        }

                        else {


                            if (aliasOffline_.length < 40) {
                                itemData_ = itemData_ + nameOffline_.substring(0, 40) + '...' + getspace(48 - nameOffline_.substring(0, 40).length) + "\n"
                                    + aliasOffline_ + getspace(48 - aliasOffline_.length) + "\n"

                                    + (parseFloat($(Columns[8]).html())).toFixed(2) + getspace(14 - parseFloat($(Columns[8]).html()).toFixed(2).length) + parseFloat($(Columns[4]).html()).toFixed(2)

                                    + getspace(38 - ($(Columns[2]).html().length + getspace(10 - parseFloat($(Columns[4]).html()).toFixed(2).length).length + (parseFloat($(Columns[4]).html())).toFixed(2).length + getspace(14 - parseFloat($(Columns[8]).html()).toFixed(2).length).length + parseFloat($(Columns[8]).html()).toFixed(2).length))

                                    + $(Columns[2]).html() + getspace(10 - parseFloat($(Columns[2]).html()).toFixed(2).length)
                                    + "\n";

                            }
                            else {
                                itemData_ = itemData_ + nameOffline_.substring(0, 40) + '...' + getspace(48 - nameOffline_.substring(0, 40).length) + "\n"
                                    + aliasOffline_.substring(0, 40) + getspace(48 - aliasOffline_.length) + '...' + "\n"

                                    + (parseFloat($(Columns[8]).html())).toFixed(2) + getspace(14 - parseFloat($(Columns[8]).html()).toFixed(2).length) + parseFloat($(Columns[4]).html()).toFixed(2)

                                    + getspace(38 - ($(Columns[2]).html().length + getspace(10 - parseFloat($(Columns[4]).html()).toFixed(2).length).length + (parseFloat($(Columns[4]).html())).toFixed(2).length + getspace(14 - parseFloat($(Columns[8]).html()).toFixed(2).length).length + parseFloat($(Columns[8]).html()).toFixed(2).length))

                                    + $(Columns[2]).html() + getspace(10 - parseFloat($(Columns[2]).html()).toFixed(2).length)
                                    + "\n";

                            }

                        }
                    }
                }
                else {
                    if (nameOffline_.length < 42) {

                        itemData_ = itemData_ + nameOffline_ + getspace(48 - nameOffline_.length) + "\n"

                            + (parseFloat($(Columns[8]).html())).toFixed(2) + getspace(14 - parseFloat($(Columns[8]).html()).toFixed(2).length) + parseFloat($(Columns[4]).html()).toFixed(2)

                            + getspace(38 - ($(Columns[2]).html().length + getspace(10 - parseFloat($(Columns[4]).html()).toFixed(2).length).length + (parseFloat($(Columns[4]).html())).toFixed(2).length + getspace(14 - parseFloat($(Columns[8]).html()).toFixed(2).length).length + parseFloat($(Columns[8]).html()).toFixed(2).length))

                            + $(Columns[2]).html() + getspace(10 - parseFloat($(Columns[2]).html()).toFixed(2).length)
                            + "\n";
                    }
                    else {

                        itemData_ = itemData_ + nameOffline_.substring(0, 40) + '...' + getspace(48 - nameOffline_.substring(0, 40).length) + "\n"


                            + (parseFloat($(Columns[8]).html())).toFixed(2) + getspace(14 - parseFloat($(Columns[8]).html()).toFixed(2).length) + parseFloat($(Columns[4]).html()).toFixed(2)

                            + getspace(38 - ($(Columns[2]).html().length + getspace(10 - parseFloat($(Columns[4]).html()).toFixed(2).length).length + (parseFloat($(Columns[4]).html())).toFixed(2).length + getspace(14 - parseFloat($(Columns[8]).html()).toFixed(2).length).length + parseFloat($(Columns[8]).html()).toFixed(2).length))

                            + $(Columns[2]).html() + getspace(10 - parseFloat($(Columns[2]).html()).toFixed(2).length)
                            + "\n";
                    }



                }
                try {

                    var splitHeaderContent_ = JSON.parse(localStorage.getItem('PrintOffline')).K.split('~')
                    var FFDate_ = splitHeaderContent_[0];

                    var FFCTR_ = splitHeaderContent_[1];
                    var FFInvoice = splitHeaderContent_[2];
                    var FFTotalItems = splitHeaderContent_[3];
                    var FFTotalQty = splitHeaderContent_[4];

                    var FFDisc = splitHeaderContent_[5];
                    var FFBaseTotal = splitHeaderContent_[6];
                    var FFVAT = "ضريبة القيمة المضافة";
                    var FFNetTotal = splitHeaderContent_[8];


                    var FFTotal = splitHeaderContent_[9];
                    var FFPaid = splitHeaderContent_[10];
                    var FFChange = splitHeaderContent_[11];



                    var FFItemHeaderNames = JSON.parse(localStorage.getItem('PrintOffline')).J;
                    var CurDate = new Date();
                    var min = CurDate.getMinutes().toString().length == 1 ? '0' + CurDate.getMinutes() : CurDate.getMinutes();
                    var dat_ = (CurDate.getDate() < 10 ? '0' : '') + CurDate.getDate() + '-' + ((CurDate.getMonth() + 1) < 10 ? '0' : '') + (CurDate.getMonth() + 1) + '-' + CurDate.getFullYear() + ' ' + CurDate.getHours() + ':' + min;


                    h1_ = "\n" + FFDate_ + " :" + dat_ + "" + getspace(40 - dat_.length) + "\n"
                        + getspace(40 - window.localStorage.getItem('COUNTERNAME').length) + window.localStorage.getItem('COUNTERNAME') + " :" + FFCTR_ + "" + "\n"
                        + "------------------------------------------------" + "\n"
                        + FFInvoice + " :" + InvoiceObhId + "" + getspace(50 - (FFInvoice + " :" + InvoiceObhId).length) + "\n" +
                        FFTotalItems + " :" + invItemsCount_ + "" + getspace(50 - (FFTotalItems + " :" + invItemsCount_).length) +
                        FFTotalQty + " :" + parseFloat(invQtyCount_).toFixed(2) + "" + getspace(50 - (FFTotalQty + " :" + parseFloat(invQtyCount_).toFixed(2)).length) + "\n" +
                        "------------------------------------------------" +
                        FFItemHeaderNames +
                        "------------------------------------------------" + "\n" +
                        itemData_
                        + "------------------------------------------------" + "\n"

                        + FFDisc + " :" + parseFloat($('#txtDiscountpp').val()).toFixed(2) + getspace(45 - (FFDisc + " :" + parseFloat($('#txtDiscountpp').val()).toFixed(2)).length) + "\n"

                        + FFBaseTotal + " :" + parseFloat($('#txtPricepp').val()).toFixed(2) + getspace(49 - (FFBaseTotal + " :" + parseFloat($('#txtPricepp').val()).toFixed(2)).length) + "\n"

                        + FFVAT + " :" + parseFloat(taxAmt_).toFixed(2) + getspace(49 - (FFVAT + " :" + parseFloat(taxAmt_).toFixed(2)).length) + "\n"

                        + FFNetTotal + " :" + parseFloat(parseFloat($('#txtAmountpp').val())).toFixed(2) + getspace(49 - (FFNetTotal + " :" + parseFloat(parseFloat($('#txtAmountpp').val())).toFixed(2)).length) + "\n" +

                        "------------------------------------------------" +
                        "                                          SAR   " +
                        FFTotal + getspace(40 - parseFloat($('#txtAmountpp').val()).toFixed(2).length) + parseFloat($('#txtAmountpp').val()).toFixed(2) + "\n" +

                        FFPaid + getspace(38 - parseFloat($('#lbl_TTpartTotal')[0].innerText).toFixed(2).length) + parseFloat($('#lbl_TTpartTotal')[0].innerText).toFixed(2) + "\n" +
                        FFChange + getspace(40 - ((parseFloat($('#lbl_TTpartTotal')[0].innerText).toFixed(2) - (parseFloat($('#txtAmountpp').val()).toFixed(2))).toFixed(2)).length) + (parseFloat($('#lbl_TTpartTotal')[0].innerText).toFixed(2) - (parseFloat($('#txtAmountpp').val()).toFixed(2))).toFixed(2) + "\n";


                    var dddd = "\n" + "Date             : " + dat_ + "\n"
                        + "Counter          : " + window.localStorage.getItem('COUNTERNAME') + "\n"
                        + "------------------------------------------------" + "\n" +
                        "Invoice No.      : " + InvoiceObhId + "\n" +
                        "Total Items      : " + invItemsCount_ + "\n" +
                        "Total Qty        : " + parseFloat(invQtyCount_).toFixed(2) + "\n" +
                        "------------------------------------------------" +
                        "Item                Qty       Rate      Amount  " +
                        "------------------------------------------------" +
                        itemData_
                        + "------------------------------------------------" +
                        "Disc             : " + parseFloat($('#txtDiscountpp').val()).toFixed(2) + "\n" +
                        "Base Total       : " + parseFloat($('#txtPricepp').val()).toFixed(2) + "\n" +
                        "VAT              : " + parseFloat(taxAmt_).toFixed(2) + "\n" +
                        "Net Total        : " + parseFloat(parseFloat($('#txtAmountpp').val())).toFixed(2) + "\n" +
                        "------------------------------------------------" +
                        "                                          SAR   " +
                        "Total" + getspace(43 - parseFloat($('#txtAmountpp').val()).toFixed(2).length) + parseFloat($('#txtAmountpp').val()).toFixed(2) + "\n" +

                        "Paid" + getspace(44 - parseFloat($('#lbl_TTpartTotal')[0].innerText).toFixed(2).length) + parseFloat($('#lbl_TTpartTotal')[0].innerText).toFixed(2) + "\n" +
                        "Change" + getspace(42 - ((parseFloat($('#lbl_TTpartTotal')[0].innerText).toFixed(2) - (parseFloat($('#txtAmountpp').val()).toFixed(2))).toFixed(2)).length) + (parseFloat($('#lbl_TTpartTotal')[0].innerText).toFixed(2) - (parseFloat($('#txtAmountpp').val()).toFixed(2))).toFixed(2) + "\n";



                } catch (a) {

                }





            }
        }
    }



    var data = JSON.parse(localStorage.getItem('PrintOffline')).A
        + h1_ + JSON.parse(localStorage.getItem('PrintOffline')).B;
    var dataSent_ = localStorage.getItem("PRINTOFFDD");

    PrintOfflineContentT(data);

}

function getspace(length) {
    var d_ = '';
    for (var i = 0; i < parseFloat(length); i++) {
        d_ = d_ + " ";
    }
    return d_;
}

function PrintOfflineContentT(content) {
    window.localStorage.setItem('ctCheck', 1);
    try { 
        navigator.notification.beep(1);
        BTPrinter.list(function (data) {
        }, function (err) {
        })
        BTPrinter.connect(function (data)
        {
            //alert("Success");
            //alert(data)
        }, function (err) 
        {
            //alert("Error");
            //alert(err)
        }, "InnerPrinter");
        try {
            navigator.sunmiInnerPrinter.printerInit();
            navigator.sunmiInnerPrinter.printerStatusStartListener();

            navigator.sunmiInnerPrinter.setAlignment(0);
            navigator.sunmiInnerPrinter.setFontSize(24);
            var data = '\n' + content;
            navigator.sunmiInnerPrinter.printString(data);

            navigator.sunmiInnerPrinter.printerStatusStopListener();
            navigator.sunmiInnerPrinter.hasPrinter();


        } catch (a) { }

        BTPrinter.printText(function (data) {

        }, function (err) {
        }, "                                       ");

        printQrCode(InvoiceObhId);

        BTPrinter.printPOSCommand(function (data) {

        }, function (err) {

        }, "0C")



        $('#myModalpayFinal').modal('show');


    }
    catch (a) {

    }

}
function print(val_, invId, Roundval) {
    $.ajax({
        url: CashierLite.Settings.getItemAliasByInvoiceId,
        data: "VD=" + VD1() + "&InvoiceId=" + invId,
        success: function (resp) {
            window.localStorage.setItem('ctCheck', 1);

            $('#tblNBaseCurrPayment').hide();
            v = $('#hdnExchangerate').val();
            $('#myModalpay').hide();
            var rows = $('#tabletrpopbind tbody >tr');
            $("#tblheader_4 tbody").remove();
            $("#tblheader_4 tr").remove();
            for (var i = 0; i < rows.length; i++) {
                if (rows[i].innerHTML !== '') {
                    var Columns = $(rows[i]).find('td');
                    if ($(Columns[1]).html().length <= 17) {
                        $('#tblheader_4').append("<tr><td style='font-size: 12px;'>" + $(Columns[1]).html() + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td style='font-size: 12px;'>" + $(Columns[2]).html() + " &nbsp;&nbsp;&nbsp;&nbsp;</td><td style='font-size: 12px;'>" + (parseFloat($(Columns[4]).html())).toFixed(2) + "&nbsp;&nbsp;&nbsp;&nbsp;</td><td style='font-size: 12px;'>" + (parseFloat($(Columns[8]).html())).toFixed(2) + "</td></tr>");
                    }
                    else {

                        $('#tblheader_4').append("<tr><td style='font-size: 12px;'  colspan='4'>" + $(Columns[1]).html() + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>");
                        $('#tblheader_4').append("<tr><td style='font-size: 12px;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td style='font-size: 12px;'>" + $(Columns[2]).html() + " &nbsp;&nbsp;&nbsp;&nbsp;</td><td style='font-size: 12px;'>" + (parseFloat($(Columns[4]).html())).toFixed(2) + "&nbsp;&nbsp;&nbsp;&nbsp;</td><td style='font-size: 12px;'>" + (parseFloat($(Columns[8]).html())).toFixed(2) + "</td></tr>");

                    }//}
                }
            }
            var tblheader5rows = $('#tbhearder_5 tbody tr');
            var tblColumns = $(tblheader5rows[0]).find('td');
            $(tblColumns[4]).html($('#txtPricepp').val());
            var tblColumns_1 = $(tblheader5rows[1]).find('td');
            $(tblColumns_1[4]).html($('#txtDiscountpp').val());
            var tblColumns_2 = $(tblheader5rows[2]).find('td');
            $(tblColumns_2[4]).html($('#txtAmountpp').val());


            $('#lbl_totalGross').text($('#hdnBaseTotal').val() + ' : ' + (parseFloat($('#txtPricepp').val())));
            $('#lbl_Discount_Print').text($('#hdnDisc').val().replace('(%)', '') + ' : ' + (parseFloat($('#txtDiscountpp').val())).toFixed(2));
            $('#lblRound').text($('#hdnRound').val() + ' : ' + parseFloat(Roundval).toFixed(2));
            $('#lbl_Round_Print').text($('#hdnNetTotal').val() + ' : ' + parseFloat(parseFloat($('#txtAmountpp').val())).toFixed(2));

            var HashCURRENCY = '';
            var hashSplit = '';
            var HashValues = (window.localStorage.getItem("hashValues"));
            if (HashValues != null && HashValues != '') {
                hashSplit = HashValues.split('~');
                HashCURRENCY = hashSplit[18].split('/')[0];
                $('#lbl_Currency_2').text((currency_symbols[HashCURRENCY]) + ' ' + HashCURRENCY);
            }
            $('#lblTenderConvertAmt').text($('#lbl_TTpartTotal')[0].innerText);

            $('#lbl_total_2').text((currency_symbols[HashCURRENCY]) + ' ' + parseFloat($('#txtAmountpp').val()).toFixed(0));
            $('#lbl_cash_1').text((currency_symbols[HashCURRENCY]) + ' ' + parseFloat($('#lbl_TTpartTotal')[0].innerText).toFixed(0));

            $('#lbl_change_1').text((currency_symbols[HashCURRENCY]) + ' ' + (parseFloat($('#lbl_TTpartTotal')[0].innerText).toFixed(0) - parseFloat($('#txtAmountpp').val()).toFixed(0)));
            $('#lbl_Pay_Print').text($('#hdnPayIn').val() + ' : ' + window.localStorage.getItem('PayModeInvoice'));



            if ($('#hdnTemBasePriceCurId').val() != '' && $('#hdnTemBasePriceCurId').val() != 1) {
                $('#lbl_Currency_3').text($('#hdnTemBasePrice').val());
                $('#tblNBaseCurrPayment').show();
                $('#lbl_total_3').text(parseFloat(parseFloat(Math.round($('#txtAmountpp').val()).toFixed(2)) / parseFloat($('#hdnTemBasePriceEX').val())).toFixed(4));


                $('#lbl_cash_2').text(parseFloat(parseFloat($('#lbl_TTpartTotal')[0].innerText) / parseFloat($('#hdnTemBasePriceEX').val())).toFixed(4));

                $('#lbl_change_2').text((currency_symbols[$('#hdnTemBasePrice').val()]) + ' ' + (parseFloat(parseFloat($('#lbl_cash_2')[0].innerText) - parseFloat($('#lbl_total_3')[0].innerText)).toFixed(4)));


                $('#lbl_total_3').text((currency_symbols[$('#hdnTemBasePrice').val()]) + ' ' + $('#lbl_total_3').text());

                $('#lbl_cash_2').text((currency_symbols[$('#hdnTemBasePrice').val()]) + ' ' + $('#lbl_cash_2').text());
            }
            var Tax_ = ""; var Sub_ = ""; var TaxCheck_ = "0";

            document.getElementById('tblheader_5_').innerHTML = "";

            if (window.localStorage.getItem("Tax")) {

                var TaxSplit_ = window.localStorage.getItem("Tax").split('~');

                for (var v_ = 0; v_ < TaxSplit_.length; v_++) {
                    var tax1_ = TaxSplit_[v_].split('@');
                    if (tax1_.length == 2) {
                        if (tax1_[1] != '') {
                            TaxCheck_ = "1"; Sub_ = "";
                            Sub_ = tax1_[0] + " : " + parseFloat(tax1_[1]).toFixed(2);
                            Tax_ = Tax_ + " <tr><td style='font-size: 12px;'>&nbsp;&nbsp;" + Sub_ + "</td></tr>";
                        }
                    }
                }
            }
            if (TaxCheck_ == "1")
                document.getElementById('tblheader_5_').innerHTML = Tax_;


            if (val_ == 't') {

                if ($('#hdnISPRINT').val() == "true") {
                    $('#myModalprint').modal();

                } else {
                    $('#myModalprint').css('display', "none");
                };

            }
            else {

                if ($('#hdnISPRINT').val() == "true") {
                    $('#myModalprint').modal();

                } else {
                    $('#myModalprint').css('display', "none");
                };
            }

            if (val_ == 't') {

                window.localStorage.setItem('InvoiceId', '');
                window.localStorage.setItem('Connection', '');
                window.localStorage.setItem("Tax", "");
                removeDraft($('#hdn_CusId').val());
                $('#hdn_CusId').val('');
                $('#txtCusName').val('');
                ClearFileds();
                $("#itemCarttable tr").remove();
                $('#grdandTotal').text('0.00');
                $('#totalitemscw').text('0');
                $('#myModalpay').modal('hide');
                $("#tbl_tenderDyms tr").remove();

            }
            else {

                window.localStorage.setItem("Tax", "");
                removeDraft($('#hdn_CusId').val());
                $('#hdn_CusId').val('');
                $('#txtCusName').val('');
                ClearFileds();
                $("#itemCarttable tr").remove()
                $('#grdandTotal').text('0.00');
                $('#totalitemscw').text('0');
                $('#myModalpay').modal('hide');
                //printDiv();
                $("#tbl_tenderDyms tr").remove();

            }
            //$('#lbl_TTpartTotal')[0].innerText = '0';
            $("#tbl_tenderDyms tr").remove();

            $("#hdnPwdCheckStatus").val('0');
            $('#hdn_CusId').val('');
            $('#chkHomeDelivery').prop('checked', false);
            printDiv();
            $('#txtItemScan').val('');
            $('#myTable li a').removeAttr('onclick');
            GetCustomers_();
        }
    });

    window.localStorage.setItem('ctCheck', 1);
    //POLEDisplay("Total:" + $('#lbl_total_2').text() + " , " + "Paid:" + $('#lbl_cash_1').text() + " , " + "Change:" + $('#lbl_change_1').text());
}
function GetofInvoice() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return "OF-" + s4();
}
function RemoveubChange() {
    window.localStorage.setItem('ctCheck', 1);
    if ($('#txthdnItemId').val() != "") {
        if ($("#txthdnActiveColumn").val() == "1") {
            var strng = document.getElementById("txtQuantity").value;
            document.getElementById("txtQuantity").value = strng.substring(0, strng.length - 1);
            UpdateQTYSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "2") {
            var strng = document.getElementById("txtDiscount").value;
            document.getElementById("txtDiscount").value = strng.substring(0, strng.length - 1);
            UpdateDiscSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "3") {
            var strng = document.getElementById("txtPrice").value;
            document.getElementById("txtPrice").value = strng.substring(0, strng.length - 1);
            UpdatePriceSubChange();
        }
        if ($("#txthdnActiveColumn").val() == "4") {
            var strng = document.getElementById("txtTendertrans").value;
            document.getElementById("txtTendertrans").value = strng.substring(0, strng.length - 1);
            changeCallKeyUp();
            $('#txtTendertrans').focus();
        }
    }
    else {
        navigator.notification.alert($('#hdnPlzSelectItem').val(), "", "neo ecr", "Ok");

    }
}


function IsNumericTenderRef(e) {
    window.localStorage.setItem('ctCheck', 1);
    var specialKeys = new Array();
    specialKeys.push(8);
    var keyCode = e.which ? e.which : e.keyCode
    var key = e.which ? e.which : e.key
    if (keyCode == 59) {
        $("#txtRefNumber").val('');

    }
    if ($('#hdnPayMode').val() == 'Cash') {
        var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1 || keyCode == 46);
        if (ret == false) {
            return ret;
        }
        else {

        }
    }
    else {
        if ($("#txtRefNumber").val().length <= 15) {
            var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1 || keyCode == 46);
            if (ret == false) {
                return ret;
            }
            else {

            }
        }
        else {
            return false;
        }
    }

}



function IsNumericTender(e) {
    window.localStorage.setItem('ctCheck', 1);
    var specialKeys = new Array();
    specialKeys.push(8);
    var keyCode = e.which ? e.which : e.keyCode
    var key = e.which ? e.which : e.key
    if (keyCode == 59) {
        $("#txtTendertrans").val('');

    }
    if ($('#hdnPayMode').val() == 'Cash') {
        var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1 || keyCode == 46);
        if (ret == false) {
            return ret;
        }
        else {

        }
    }
    else {
        if ($("#txtTendertrans").val().length <= 15) {
            var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1 || keyCode == 46);
            if (ret == false) {
                return ret;
            }
            else {

            }
        }
        else {
            return false;
        }
    }
}
function IsNumericQTY(e) {
    window.localStorage.setItem('ctCheck', 1);
    var myTab = document.getElementById('itemCarttable');
    if (myTab.rows.length == 0) {
        return false;
    }
    var specialKeys = new Array();
    specialKeys.push(8);
    var keyCode = e.which ? e.which : e.keyCode

    var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1 || keyCode == 46);
    if (ret == false) {
        return ret;
    }
    else {

    }

}
function UpdateQTYSubChange() {
    window.localStorage.setItem('ctCheck', 1);
    window.localStorage.setItem("Display", "1");
    window.localStorage.setItem("DisplayDLTemp0", "0");
    var ItemId = $('#txthdnItemId').val();
    var Qty = $('#txtQuantity').val();

    var d_ = Qty.split('.');
    if (d_.length == 2) {
        if (d_[0].length == 9) {
            RemoveubChange();
            return false;
        }
        if (d_[1].length == 3) {
            RemoveubChange();
            return false;
        }
    }
    if (d_.length <= 1) {
        if (Qty.length == 9) {
            RemoveubChange();
            return false;
        }
    }



    var myTab = document.getElementById('itemCarttable');
    var m_ = ""; var Price = "0";
    for (i = 0; i < myTab.rows.length; i++) {
        var objCells = myTab.rows.item(i).cells;
        if (Qty != "") {
            if (parseFloat(Qty) != 0) {
                if (ItemId == objCells.item(5).innerHTML) {
                    var qty_ = parseFloat(Qty).toFixed(3);
                    var Baseprice = Number(floorFigure($('#td_' + ItemId).val(), 4));

                    Price = parseFloat(Baseprice) * parseFloat(Qty);
                    if (objCells.item(3).innerHTML != "") {
                        if (parseFloat(objCells.item(3).innerHTML) > 0) {
                            Price = parseFloat(Price) - ((parseFloat(Price) * (parseFloat(objCells.item(3).innerHTML))) / 100);
                        }
                    }

                    onSubDataQTY(objCells.item(1).innerHTML, objCells.item(5).innerHTML, objCells.item(1).innerHTML, objCells.item(9).innerHTML, objCells.item(3).innerHTML, objCells.item(7).innerHTML, objCells.item(8).innerHTML, objCells.item(10).innerHTML, objCells.item(11).innerHTML, document.getElementById("txtQuantity").value, objCells.item(13).innerHTML);
                    //var markup = "<td  style='width:5% !important' ><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right' >" + objCells.item(1).innerHTML + "</td><td>" + parseFloat(qty_).toFixed(2) + "</td><td>" + parseFloat(objCells.item(3).innerHTML).toFixed(2) + "</td><td style='display:none' >" + parseFloat(Price).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td><input type='text' class='form-control'  value='" + Baseprice + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + objCells.item(7).innerHTML + "</td><td style='display:none'>" + objCells.item(8).innerHTML + "</td><td>" + Baseprice + "</td><td style='display:none'>" + objCells.item(10).innerText + "</td><td style='display:none'>" + objCells.item(11).innerText + "</td><td style='display:none'>" + objCells.item(12).innerText + "</td>";
                    var markup = "<td  style='width:5% !important' ><i class='fa fa-trash' onclick='deleteRow(this)' style='font-size: 16px;'></i></td><td align='right' class='tdClor'>" + objCells.item(1).innerHTML + "</td><td class='tdClor'>" + parseFloat(qty_).toFixed(2) + "</td><td class='tdClor'>" + parseFloat(objCells.item(3).innerHTML).toFixed(2) + "</td><td style='display:none' >" + parseFloat(Price).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td class='tdClor'><input type='text' class='form-control'  value='" + Baseprice + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + objCells.item(7).innerHTML + "</td><td style='display:none'>" + objCells.item(8).innerHTML + "</td><td class='tdClor'>" + Baseprice + "</td><td style='display:none'>" + objCells.item(10).innerText + "</td><td style='display:none'>" + objCells.item(11).innerText + "</td><td style='display:none'>" + objCells.item(12).innerText + "</td>";

                    $('#itemIdhiddenCheck').val('1');
                    GradCaluclation();
                    ReassignSubVals(ItemId);
                }
            }
            else {
                //alert("Please Enter Quantity");
            }
        }
        else {
            //alert("Please Enter Quantity");
        }
    }
}
function UpdateDiscSubChange() {
    window.localStorage.setItem('ctCheck', 1);
    window.localStorage.setItem("Display", "1");
    window.localStorage.setItem("DisplayDLTemp0", "0");
    $("#tbl_tenderDyms tr").remove();
    $('#lbl_TTpartTotal')[0].innerText = "0.00";
    var ItemId = $('#txthdnItemId').val();
    var Disc = $('#txtDiscount').val();
    if (parseFloat(Disc) > 100) {
        $('#txtDiscount').val('');
        Disc = '';
        navigator.notification.alert($('#hdnDiscVal').val(), "", "neo ecr", "Ok");
    }
    var d_ = Disc.split('.');
    if (d_.length == 2) {
        if (d_[0].length == 3) {
            RemoveubChange();
            return false;
        }
        if (d_[1].length == 3) {
            RemoveubChange();
            return false;
        }
    }
    if (d_.length <= 1) {
        if (Disc.length == 3) {
            if (parseFloat(Disc) == 100) {

            } else {
                RemoveubChange();
                return false;
            }
        }
    }


    var ItemId = $('#txthdnItemId').val();
    var Disc = $('#txtDiscount').val() == "" ? "0" : $('#txtDiscount').val();
    var myTab = document.getElementById('itemCarttable');
    var m_ = ""; var Price = "0";
    for (i = 0; i < myTab.rows.length; i++) {
        var objCells = myTab.rows.item(i).cells;
        if (ItemId == objCells.item(5).innerHTML) {
            var qty_ = parseFloat(objCells.item(2).innerHTML).toFixed(3);
            //var Baseprice = $('#td_' + ItemId).val();
            var Baseprice = floorFigure_appFunc($('#td_' + ItemId).val(), 4);
            Price = parseFloat(Baseprice) * parseFloat(qty_);
            //Price = parseFloat(objCells.item(4).innerHTML);
            if ((Disc) != "") {
                if (parseFloat(Disc) <= 100) {
                    //var Baseprice = $('#td_' + ItemId).val();
                    var Baseprice = floorFigure_appFunc($('#td_' + ItemId).val(), 2);
                    var Dis1 = parseFloat(Price) - ((parseFloat(Price) * (parseFloat(Disc))) / 100);
                    //var markup = "<td  style='width:5% !important' ><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right' >" + objCells.item(1).innerHTML + "</td><td>" + parseFloat(qty_).toFixed(2) + "</td><td>" + parseFloat(Disc).toFixed(2) + "</td>                      <td style='display:none'>" + parseFloat(Dis1).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td><input type='text' class='form-control'  value='" + Baseprice + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + objCells.item(7).innerHTML + "</td><td style='display:none'>" + objCells.item(8).innerHTML + "</td><td>" + Baseprice + "</td><td style='display:none'>" + objCells.item(10).innerText + "</td><td style='display:none'>" + objCells.item(11).innerText + "</td><td style='display:none'>" + objCells.item(12).innerText + "</td><td>" + objCells.item(13).innerHTML + "</td>";
                    var markup = "<td  style='width:5% !important' class='tdClor'><i class='fa fa-trash' onclick='deleteRow(this)' style='font-size: 16px;'></i></td><td align='right' class='tdClor'>" + objCells.item(1).innerHTML + "</td><td class='tdClor'>" + parseFloat(qty_).toFixed(2) + "</td><td class='tdClor'>" + parseFloat(Disc).toFixed(2) + "</td><td style='display:none'>" + parseFloat(Dis1).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td class='tdClor'><input type='text' class='form-control'  value='" + Baseprice + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + objCells.item(7).innerHTML + "</td><td style='display:none'>" + objCells.item(8).innerHTML + "</td><td class='tdClor'>" + Baseprice + "</td><td style='display:none'>" + objCells.item(10).innerText + "</td><td style='display:none'>" + objCells.item(11).innerText + "</td><td style='display:none'>" + objCells.item(12).innerText + "</td><td class='tdClor'>" + objCells.item(13).innerHTML + "</td>";
                    //var markup = "<td  style='width:5% !important' ><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right' >" + objCells.item(1).innerHTML + "</td><td>" + parseFloat(qty_).toFixed(2) + "</td><td>" + parseFloat(objCells.item(3).innerHTML).toFixed(2) + "</td><td style='display:none'>" + parseFloat(Price).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td><input type='text' class='form-control'  value='" + Baseprice + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + objCells.item(7).innerHTML + "</td><td style='display:none'>" + objCells.item(8).innerHTML + "</td><td>" + Baseprice + "</td>";
                    $('#tr_' + ItemId).html(markup);
                    $('#itemIdhiddenCheck').val('1');
                    GradCaluclation();
                }
                else {
                    navigator.notification.alert($('#hdnDiscVal').val(), "", "neo ecr", "Ok");
                    $('#txtDiscount').val('');
                    //var Baseprice = $('#td_' + ItemId).val();
                    var Baseprice = floorFigure_appFunc(parseFloat($('#td_' + ItemId).val()), 2);
                    Disc = 0;
                    //var markup = "<td  style='width:5% !important' ><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right' >" + objCells.item(1).innerHTML + "</td><td>" + parseFloat(qty_).toFixed(2) + "</td><td>" + parseFloat(Disc).toFixed(2) + "</td><td style='display:none'>" + parseFloat(parseFloat(Baseprice) * parseFloat(qty_)).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td><input type='text' class='form-control'  value='" + Baseprice + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + objCells.item(7).innerHTML + "</td><td style='display:none'>" + objCells.item(8).innerHTML + "</td><td>" + Baseprice + "</td><td style='display:none'>" + objCells.item(10).innerText + "</td><td style='display:none'>" + objCells.item(11).innerText + "</td><td style='display:none'>" + objCells.item(12).innerText + "</td><td>" + objCells.item(13).innerHTML + "</td>";
                    var markup = "<td  style='width:5% !important' ><i class='fa fa-trash' onclick='deleteRow(this)' style='font-size: 16px;'></i></td><td align='right' class='tdClor'>" + objCells.item(1).innerHTML + "</td><td  class='tdClor'>" + parseFloat(qty_).toFixed(2) + "</td><td  class='tdClor'>" + parseFloat(Disc).toFixed(2) + "</td><td style='display:none'>" + parseFloat(parseFloat(Baseprice) * parseFloat(qty_)).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td  class='tdClor'><input type='text' class='form-control'  value='" + Baseprice + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + objCells.item(7).innerHTML + "</td><td style='display:none'>" + objCells.item(8).innerHTML + "</td><td  class='tdClor'>" + Baseprice + "</td><td style='display:none'>" + objCells.item(10).innerText + "</td><td style='display:none'>" + objCells.item(11).innerText + "</td><td style='display:none'>" + objCells.item(12).innerText + "</td><td  class='tdClor'>" + objCells.item(13).innerHTML + "</td>";
                    //var markup = "<td  style='width:5% !important' ><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right' >" + objCells.item(1).innerHTML + "</td><td>" + parseFloat(qty_).toFixed(2) + "</td><td>" + parseFloat(Disc).toFixed(2) + "</td><td style='display:none'>" + parseFloat(Dis1).toFixed(2) + "</td>                                    <td style='display:none'>" + ItemId + "</td><td><input type='text' class='form-control'  value='" + Baseprice + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + objCells.item(7).innerHTML + "</td><td style='display:none'>" + objCells.item(8).innerHTML + "</td><td>" + Baseprice + "</td>";
                    $('#tr_' + ItemId).html(markup);
                    $('#itemIdhiddenCheck').val('1');
                    GradCaluclation();

                }
                ReassignSubVals(ItemId);
                valueAppendtoPOPUp();
                $('#txtDiscount').focus();
                $("#txthdnActiveColumn").val("2");
            }

            break;
        }
    }
}
function UpdatePriceSubChange1(e) {
    window.localStorage.setItem('ctCheck', 1);
    var ItemId = $('#txthdnItemId').val();
    var Baseprice = $('#txtPrice').val();

    switch (e.keyCode) {

        case 37:
            return true;
            break;
        case 38:
            return true;
            break;
        case 39:
            return true;
            break;
        case 40:
            return true;
            break;
    }

    var d_ = Baseprice.split('.');
    if (d_.length == 2) {
        if (d_[0].length == 9) {
            RemoveubChange();
            return false;
        }
        if (d_[1].length == 3) {
            RemoveubChange();
            return false;
        }
    }
    if (d_.length <= 1) {
        if (Baseprice.length == 9) {
            RemoveubChange();
            return false;
        }
    }


    var input_ = $('#txtPrice').val();

    if (input_.indexOf('.') != -1) {

        if (input_.split('.')[1].length > 2) {
            $("#txtPrice").val('');
            $("#txtPrice").val(input_.split('.')[0] + '.' + input_.split('.')[1].substring(0, 2));
        }
        if (input_.split('.')[0].length > 8) {
            $("#txtPrice").val('');
            $("#txtPrice").val(input_.split('.')[0].substring(0, 8) + '.' + input_.split('.')[1]);
        }
    }

    var ItemId = $('#txthdnItemId').val();
    var Baseprice = $('#txtPrice').val();
    var myTab = document.getElementById('itemCarttable');
    var m_ = ""; var Price = "0";
    for (i = 0; i < myTab.rows.length; i++) {
        var objCells = myTab.rows.item(i).cells;
        if (ItemId == objCells.item(5).innerHTML) {
            var qty_ = parseFloat(objCells.item(2).innerHTML).toFixed(3);
            Price = parseFloat(Baseprice) * parseFloat(qty_);
            //
            if (Baseprice == "") {
                Baseprice = "0";
                Price = "0";
            }
            if ((Baseprice) != "") {
                if (Baseprice == "0") {
                    $('#txtPrice').val("0")
                }
                var Dis1 = parseFloat(Price) - ((parseFloat(Price) * (parseFloat(objCells.item(3).innerHTML))) / 100);
                onSubDataPrice(objCells.item(1).innerHTML, objCells.item(5).innerHTML, objCells.item(1).innerHTML, $('#txtPrice').val(), objCells.item(3).innerHTML, objCells.item(7).innerHTML, objCells.item(8).innerHTML, objCells.item(10).innerHTML, objCells.item(11).innerHTML, qty_, objCells.item(13).innerHTML);
                $('#itemIdhiddenCheck').val('1');
                GradCaluclation();
                ReassignSubVals(ItemId);
                $("#txtPrice").attr("disabled", "disabled");
            }
            else {

            }
            break;
        }
    }
}
function UpdatePriceSubChange() {
    window.localStorage.setItem('ctCheck', 1);
    var ItemId = $('#txthdnItemId').val();
    var Baseprice = $('#txtPrice').val();


    var d_ = Baseprice.split('.');
    if (d_.length == 2) {
        if (d_[0].length == 9) {
            RemoveubChange();
            return false;
        }
        if (d_[1].length == 3) {
            RemoveubChange();
            return false;
        }
    }
    if (d_.length <= 1) {
        if (Baseprice.length == 9) {
            RemoveubChange();
            return false;
        }
    }


    var input_ = $('#txtPrice').val();

    if (input_.indexOf('.') != -1) {

        if (input_.split('.')[1].length > 2) {
            $("#txtPrice").val('');
            $("#txtPrice").val(input_.split('.')[0] + '.' + input_.split('.')[1].substring(0, 2));
        }
        if (input_.split('.')[0].length > 8) {
            $("#txtPrice").val('');
            $("#txtPrice").val(input_.split('.')[0].substring(0, 8) + '.' + input_.split('.')[1]);
        }
    }

    var ItemId = $('#txthdnItemId').val();
    var Baseprice = $('#txtPrice').val();
    var myTab = document.getElementById('itemCarttable');
    var m_ = ""; var Price = "0";
    for (i = 0; i < myTab.rows.length; i++) {
        var objCells = myTab.rows.item(i).cells;
        if (ItemId == objCells.item(5).innerHTML) {
            var qty_ = parseFloat(objCells.item(2).innerHTML).toFixed(3);

            Price = parseFloat(Baseprice) * parseFloat(qty_);
            //
            if (Baseprice == "") {
                Baseprice = "0";
                Price = "0";
                $('#txtPrice').val("0.00");
            }
            if ((Baseprice) != "") {
                if (Baseprice == "0") {
                    $('#txtPrice').val("0.00")
                }
                var Dis1 = parseFloat(Price) - ((parseFloat(Price) * (parseFloat(objCells.item(3).innerHTML))) / 100);
                onSubDataPrice(objCells.item(1).innerHTML, objCells.item(5).innerHTML, objCells.item(1).innerHTML, $('#txtPrice').val(), objCells.item(3).innerHTML, objCells.item(7).innerHTML, objCells.item(8).innerHTML, objCells.item(10).innerHTML, objCells.item(11).innerHTML, qty_, objCells.item(13).innerHTML);
                $('#itemIdhiddenCheck').val('1');
                GradCaluclation();
                ReassignSubVals(ItemId);
                $("#txtPrice").attr("disabled", "disabled");
            }
            else {

            }
            break;
        }
    }
}

function UpdatePriceSubChange() {
    window.localStorage.setItem("Display", "1");
    window.localStorage.setItem("DisplayDLTemp0", "0");
    var ItemId = $('#txthdnItemId').val();
    var Baseprice = $('#txtPrice').val();


    var d_ = Baseprice.split('.');
    if (d_.length == 2) {
        if (d_[0].length == 9) {
            RemoveubChange();
            return false;
        }
        if (d_[1].length == 3) {
            RemoveubChange();
            return false;
        }
    }
    if (d_.length <= 1) {
        if (Baseprice.length == 9) {
            RemoveubChange();
            return false;
        }
    }

    var myTab = document.getElementById('itemCarttable');
    var m_ = ""; var Price = "0";
    for (i = 0; i < myTab.rows.length; i++) {
        var objCells = myTab.rows.item(i).cells;
        if (ItemId == objCells.item(5).innerHTML) {
            var qty_ = parseFloat(objCells.item(2).innerHTML).toFixed(3);
            Price = parseFloat(Baseprice) * parseFloat(qty_);
            //
            if (Baseprice == "") {
                Baseprice = "0";
                Price = "0";
            }
            if ((Baseprice) != "") {
                if (Baseprice == "0") {
                    $('#txtPrice').val("0.00")
                }
                var Dis1 = parseFloat(Price) - ((parseFloat(Price) * (parseFloat(objCells.item(3).innerHTML))) / 100);
                onSubDataPrice(objCells.item(1).innerHTML, objCells.item(5).innerHTML, objCells.item(1).innerHTML, $('#txtPrice').val(), objCells.item(3).innerHTML, objCells.item(7).innerHTML, objCells.item(8).innerHTML, objCells.item(10).innerHTML, objCells.item(11).innerHTML, qty_, objCells.item(13).innerHTML);
                $('#itemIdhiddenCheck').val('1');
                GradCaluclation();
                ReassignSubVals(ItemId);
                $("#txtPrice").attr("disabled", "disabled");
            }
            else {

            }
            break;
        }
    }
}
function onSubDataPrice(code, ItemId, Name, Price, Disc, UomID, UomCode, mapCode, Profile, qty_, img) {
    window.localStorage.setItem('ctCheck', 1);
    var passQTY = 1; var passPrice = 0; var baseCur_;
    var myTabChkQty = document.getElementById('itemCarttable');
    for (i = 0; i < myTabChkQty.rows.length; i++) {
        var objCells = myTabChkQty.rows.item(i).cells;
        if (ItemId == objCells.item(5).innerHTML) {
            passQTY = passQTY + parseFloat(objCells.item(2).innerHTML);
            passPrice = passPrice + parseFloat(objCells.item(9).innerHTML);
        }
    }
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        SubOrg_Id = hashSplit[1];
        baseCur_ = hashSplit[16];
    }


    var strOptions1 = $('#txtCusName').val();
    var strOptions = $('#hdn_CusId').val();
    if ($('#txthdnRemoveStatus').val() == '1') {
        removeDraft(strOptions);
        $('#txthdnRemoveStatus').val('0');
    }
    if (strOptions == '') {
        strOptions = GenarateGustId() + '-01';
        strOptions1 = GenarateGustName();
    }

    var valItemCheck_ = $('#itemIdhiddenCheck').val();

    DuplicatePRICEMODE(ItemId, Price, Disc, mapCode, Profile, '', qty_, img);
    valItemCheck_ = $('#itemIdhiddenCheck').val();
    if (valItemCheck_ == '0') {
        var imgHost_ = img;
        if ((img == "") || img == undefined) {
            imgHost_ = "";
        }
        var markup = "<tr id=" + 'tr_' + ItemId + " onclick=ActiveRowget(\'" + ItemId + "\',\'" + Price + "\','1');><td style='width:5% !important'  class='tdClor'><i class='fa fa-trash' onclick='deleteRow(this)' style='font-size:16px'></i></td><td align='right'  class='tdClor'>" + Name + "</td><td  class='tdClor'>1.00</td><td  class='tdClor'>" + parseFloat(Disc).toFixed(2) + "</td><td style='display:none' >" + parseFloat(Price).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td  class='tdClor'><input type='text' class='form-control'  value='" + Price + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + UomID + "</td><td style='display:none'>" + UomCode + "</td><td  class='tdClor'>" + floorFigure_appFunc(parseFloat(Price), 2) + "</td><td style='display:none'>" + mapCode + "</td><td style='display:none'>" + Profile + "</td><td style='display:none'>0</td><td  class='tdClor'>" + imgHost_ + "</td></tr>";
        $("#itemCarttable tbody").append(markup);
        POLEDisplay(Name + ':' + Price);
    }


    GradCaluclation();
    ActiveRowget(ItemId)
    $('#txtItemScan').val('');
    PriceSubChange();
    $('#txtPrice').val(Price);


}

function DuplicatePRICEMODE(ItemId, Price_, Disc_, MapCode_, Profile_, mainData_, QTY_, img) {
    window.localStorage.setItem('ctCheck', 1);
    var myTab = document.getElementById('itemCarttable');
    var m_ = "";
    $('#itemIdhiddenCheck').val('0');
    // LOOP THROUGH EACH ROW OF THE TABLE AFTER HEADER.
    for (i = 0; i < myTab.rows.length; i++) {
        // GET THE CELLS COLLECTION OF THE CURRENT ROW.
        var objCells = myTab.rows.item(i).cells;
        // LOOP THROUGH EACH CELL OF THE CURENT ROW TO READ CELL VALUES.
        if (ItemId == objCells.item(5).innerHTML) {
            var qty_ = parseFloat(QTY_);
            var Price = Price_;
            var Baseprice = floorFigure_appFunc(parseFloat(Price), 2);
            Price = parseFloat(Baseprice) * parseFloat(qty_);
            if (objCells.item(3).innerHTML != "") {
                if (parseFloat(objCells.item(3).innerHTML) > 0) {
                    Price = parseFloat(Price) - ((parseFloat(Price) * (parseFloat(objCells.item(3).innerHTML))) / 100);
                }
                else if (parseFloat(Disc_) > 0) {
                    Price = parseFloat(Price) - ((parseFloat(Price) * (parseFloat(Disc_))) / 100);
                }
            }
            var imgHost_ = img;
            if ((img == "") || img == undefined) {
                imgHost_ = "";
            }
            try {

                var markup = "<tr id=" + 'tr_' + ItemId + " onclick=ActiveRowget(\'" + ItemId + "\',\'" + Price_ + "\','1');><td  style='width:5% !important'  class='tdClor' ><i class='fa fa-trash' onclick='deleteRow(this)' style='font-size:16px'></i></td><td align='right' class='tdClor'>" + objCells.item(1).innerHTML + "</td><td class='tdClor'>" + parseFloat(qty_).toFixed(2) + "</td><td class='tdClor'>" + parseFloat(objCells.item(3).innerHTML).toFixed(2) + "</td><td style='display:none' >" + parseFloat(Price).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td class='tdClor'><input type='text' class='form-control'  value='" + Baseprice + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + objCells.item(7).innerHTML + "</td><td style='display:none'>" + objCells.item(8).innerHTML + "</td><td class='tdClor'>" + Baseprice + "</td><td style='display:none'>" + MapCode_ + "</td><td style='display:none'>" + Profile_ + "</td><td style='display:none'>" + mainData_.length + "</td><td class='tdClor'>" + imgHost_ + "</td></tr>";
            }
            catch (r) {
                var markup = "<tr id=" + 'tr_' + ItemId + " onclick=ActiveRowget(\'" + ItemId + "\',\'" + Price_ + "\','1');><td  style='width:5% !important'  class='tdClor' ><iclass='fa fa-trash' onclick='deleteRow(this)' style='font-size:16px'></i></td><td align='right' class='tdClor'>" + objCells.item(1).innerHTML + "</td><td class='tdClor'>" + parseFloat(qty_).toFixed(2) + "</td><td class='tdClor'>" + parseFloat(objCells.item(3).innerHTML).toFixed(2) + "</td><td style='display:none' >" + parseFloat(Price).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td class='tdClor'><input type='text' class='form-control'  value='" + Baseprice + "' id='" + 'td_' + ItemId + "' style='display:none'></td><td style='display:none'>" + objCells.item(7).innerHTML + "</td><td style='display:none'>" + objCells.item(8).innerHTML + "</td><td class='tdClor'>" + Baseprice + "</td><td style='display:none'>" + MapCode_ + "</td><td style='display:none'>" + Profile_ + "</td><td style='display:none'>" + objCells.item(12).innerHTML + "</td><td class='tdClor'>" + imgHost_ + "</td></tr>";
            }
            //var markup = "<td style='width:5% !important'><img src='images/delete.png' onclick='deleteRow(this)'/></td><td align='right'>" + Name + "</td>                          <td>1.00</td>                               <td>" + parseFloat(Disc).toFixed(2) + "</td>                     <td style='display:none' >" + parseFloat(Price).toFixed(2) + "</td><td style='display:none'>" + ItemId + "</td><td><input type='text' class='form-control'  value='" + Price + "' id='" + 'td_' + ItemId + "' style='display:none'></td>                      <td style='display:none'>" + UomID + "</td>                     <td style='display:none'>" + UomCode + "</td>                   <td>" + Price + "</td>";
            var mIdSubLength = objCells.item(12).innerHTML;
            if (parseFloat(mIdSubLength) >= 0) {
                for (var m1_ = 1; m1_ < mIdSubLength + 1; m1_++) {
                    var idC_ = '#tr_' + ItemId + "_" + (m1_ - 1);
                    $(idC_).remove();
                }
            }
            var idC_ = '#tr_' + ItemId;
            $(idC_).remove();


            $("#itemCarttable tbody").append(markup);
            try {
                //if (parseFloat(objCells.item(12).innerHTML) > 0) {
                if (mainData_.length > 0) {
                    for (var k_ = 0; k_ < mainData_.length; k_++) {
                        var resp_ = mainData_[k_].split('~');
                        var id_ = 'tr_' + ItemId + "_" + k_;
                        var imgHost_ = img;
                        if ((img == "") || img == undefined) {
                            imgHost_ = "";
                        }
                        markup = "<tr  id=" + id_ + " onclick=ActiveRowget(\'" + ItemId + "\',\'" + Price + "\','1');><td  style='width:5% !important;background:#ddd;color:black;'></td><td style='background:#ddd;color:black;' align='right' > " + resp_[2] + "</td><td style='background:#ddd;color:black;' >" + parseFloat(resp_[3]).toFixed(2) + "</td><td style='background:#ddd;color:black;' >" + parseFloat(resp_[10]).toFixed(2) + "</td><td style='display:none;background:#ddd;color:black;'>" + resp_[4] + "</td><td style='display:none;background:#ddd;color:black;'>" + resp_[1] + "~" + "</td><td style='background:#ddd;color:black;' ><input type='text' class='form-control'  value='" + resp_[4] + "' id='" + 'td_' + resp_[1] + "' style='display:none;background:#ddd;color:black;'></td><td  style='display:none;background:#ddd;color:black;'>" + resp_[7] + "</td><td  style='display:none;background:#ddd;color:black;'>" + resp_[5] + "</td><td style='background:#ddd;color:black;' >" + parseFloat(resp_[4]).toFixed(2) + "</td><td style='background:#ddd;color:black;display:none;' >" + MapCode_ + "</td><td  style='background:#ddd;color:black;'></td><td  style='display:none'>" + resp_[3] + "</td><td>" + imgHost_ + "</td></tr>"
                        $("#itemCarttable tbody").append(markup);
                    }
                }
                //}
            }
            catch (r) {

            }

            //duplicateValForScheemItem(ItemId, qty_, 0, objCells.item(12).innerHTML);
            $('#itemIdhiddenCheck').val('1');
            ClearFileds();
            //POLEDisplay(objCells.item(1).innerHTML + ':' + Price);

            $('#txtPrice').val(Price_);
        }
    }
}


function newCart() {
    window.localStorage.setItem('ctCheck', 1);
    $("#tbl_tenderDyms tr").remove();
    $('#lbl_TTpartTotal')[0].innerText = '0';
    $('#txtUOM').val('');
    $('#txtQuantity').val('');
    $('#txtDiscount').val('');
    $('#txtPrice').val('');
    $('#txtAmount').val('');

    var strOptions = $('#hdn_CusId').val();
    var strOptions1 = $('#txtCusName').val();
    //if (strOptions == "") {

    strOptions = GenarateGustId();
    strOptions1 = GenarateGustName();
    //}
    removeDraft(strOptions);
    if (strOptions != '') {
        var myTab = document.getElementById('itemCarttable');
        var MainVal_ = ""; var cusId_ = ""; var cusName_ = ""; var ItemId_ = ""; var ItemName_ = ""; var BasePrice_ = ""; var Disc_ = ""; var Amount_ = ""; var Quantity_ = "";
        var totalAmout_ = "0.0000";
        var itemCheck = "";
        if (myTab.rows.length > 0) {
            for (i = 0; i < myTab.rows.length; i++) {

                var objCells = myTab.rows.item(i).cells;
                cusId_ = strOptions;
                cusName_ = strOptions1;
                ItemId_ = objCells.item(5).innerHTML;
                Amount_ = objCells.item(4).innerHTML;

                totalAmout_ = parseFloat(totalAmout_) + parseFloat(Amount_);
                itemCheck = ItemId_;

            }
            if (itemCheck != "") {
                window.localStorage.setItem(cusId_, $('#itemCarttable')[0].innerHTML);
                window.localStorage.setItem(cusId_ + '#0', cusId_ + '~' + cusName_);
                var HeadeSub_ = cusId_ + '~' + cusName_ + '~' + totalAmout_ + '$';
                if (window.localStorage.getItem("DraftItemsHeader") == null) {
                    window.localStorage.setItem("DraftItemsHeader", HeadeSub_);
                }
                else {
                    var HeaderMain = window.localStorage.getItem("DraftItemsHeader") + HeadeSub_;
                    window.localStorage.setItem("DraftItemsHeader", HeaderMain);
                }

                $('#hdn_CusId').val('');
                $('#txtCusName').val('');
                $("#itemCarttable tr").remove();
                $('#grdandTotal').text('0.00');
                $('#totalitemscw').text('0');
            }

        }
        else {

        }
    }
    CartItemsPULL();
    var elem = document.getElementById("toggle-field");
    var elemDraft = document.getElementById("toggle-fieldSub");

    $('.content-panel').css('position', 'absolute');
    $('.content-panel').css('width', '50%');
    $('.content-panel').css('right', '0px');
    $("#hdnPwdCheckStatus").val('0');
    POLEDisplay("Counter Ready");
    ClearFileds();
    GetCounterTypeTenderLocalData();
    $('#lbl_TpartTotal')[0].innerText = "0.00";
    $("#tbl_tenderDyms tr").remove();
    $("#tabletrpopbind tbody").remove();
    $('#txtItemspp').val('0');
    $('#txtQuantityPP').val('0');
    $('#txtPricepp').val('0');
    $('#txtDiscountpp').val('0');
    $('#txtTaxPP').val('0');
    $('#txtAmountpp').val('0');
    $('#txtTendertrans').val('0');
    $('#lblTenderConvertAmt').text('0');
    $("#tbl_tenderDyms tr").remove();
    $('#lbl_TTpartTotal')[0].innerText = "0.00";
    $('#totalitemscw').text('0');

}


function ToggelCartChange() {
    window.localStorage.setItem('ctCheck', 1);

    var strOptions = $('#hdn_CusId').val();
    var strOptions1 = $('#txtCusName').val();
    //$('#txtCusName').val('');
    if (strOptions != '0') {
        var myTab = document.getElementById('itemCarttable');
        var MainVal_ = ""; var cusId_ = ""; var cusName_ = ""; var ItemId_ = ""; var ItemName_ = ""; var BasePrice_ = ""; var Disc_ = ""; var Amount_ = ""; var Quantity_ = "";
        var totalAmout_ = "0.0000";
        var itemCheck = "";
        for (i = 0; i < myTab.rows.length; i++) {
            var objCells = myTab.rows.item(i).cells;
            cusId_ = strOptions;
            cusName_ = strOptions1;
            ItemId_ = objCells.item(5).innerHTML;
            ItemName_ = objCells.item(1).innerHTML;
            BasePrice_ = $('#td_' + objCells.item(5).innerHTML).val();;
            Disc_ = objCells.item(3).innerHTML;
            Amount_ = objCells.item(4).innerHTML;
            Quantity_ = objCells.item(2).innerHTML;
            totalAmout_ = parseFloat(totalAmout_) + parseFloat(Amount_);
            itemCheck = ItemId_;
            if (MainVal_ == "") {
                MainVal_ = cusId_ + '~' + cusName_ + '~' + ItemId_ + '~' + ItemName_ + '~' + BasePrice_ + '~' + Disc_ + '~' + Amount_ + '~' + Quantity_ + '$';
            }
            else {
                MainVal_ = MainVal_ + cusId_ + '~' + cusName_ + '~' + ItemId_ + '~' + ItemName_ + '~' + BasePrice_ + '~' + Disc_ + '~' + Amount_ + '~' + Quantity_ + '$';
            }
        }
        if (itemCheck != "") {
            window.localStorage.setItem(cusId_, MainVal_);
            var HeadeSub_ = cusId_ + '~' + cusName_ + '~' + totalAmout_ + '$';
            if (window.localStorage.getItem("DraftItemsHeader") == null) {
                window.localStorage.setItem("DraftItemsHeader", HeadeSub_);
            }
            else {
                var HeaderMain = window.localStorage.getItem("DraftItemsHeader") + HeadeSub_;
                window.localStorage.setItem("DraftItemsHeader", HeaderMain);
            }


            $('#hdn_CusId').val('');
            $('#txtCusName').val('');
            $("#itemCarttable tr").remove();

        }
    }


}

function toggleDiv() {

    window.localStorage.setItem('ctCheck', 1);

    $("#itemCarttable tr").remove();
    //$("#dpCusData")[0].selectedIndex = 0;
    $('#hdn_CusId').val('');
    $('#txtCusName').val('');

    $('#hdn_CusId').val('');
    $('#txtCusName').val('');
 
    CartItemsPULL();
    //}
    $('.content-panel').css('position', 'absolute');
    $('.content-panel').css('width', '50%');
    $('.content-panel').css('right', '0px');
    $('#myModalDrafts').modal('show');
    $('#txtUOM').val('');
    $('#txtQuantity').val('');
    $('#txtPrice').val('');
    $('#txtAmount').val('');
    $("#txtDiscount").val('');
    $('#txtTendertransref').val('');
    $('#lblTenderConvertAmt').text('');
    $('#grdandTotal').text('0.000');
    $('#totalitemscw').text('0');
}
function tenderPOP() {
    window.localStorage.setItem('ctCheck', 1);
    $('#myModalTenders').modal('show');
}
function btTerminalPOP() {
    window.localStorage.setItem('ctCheck', 1);
    testSale();
    $('#myModalBTPaymentTerminal').modal('show');
}
function demoOpen() {
   
    var src_ = 'https://www.youtube.com/embed/LviFtsQ4nL4?autoplay=1&mute=0&loop=1&rel=0&controls=0&showinfo=0&loop=1';
    $("iframe#frcYTDDemo").attr("src", src_);
    $('#myModalDemo').modal('show');
}
function closeDemo()
{
    var src_ = '';
    $("iframe#frcYTDDemo").attr("src", src_);
    $('#myModalDemo').modal('hide');
}
function testSale() { 
    var bt = cordova.plugins.Apiplugin.getAllDevices;
    bt = bt(su, fa);
    // var bt = cordova.plugins.Apiplugin.gettriggerSaleTransaction;
    // bt = bt(su, fa,"sale", "100",  "01" ,"9876543210" ,"abc@123.com" ,"userd" ,"deviceid" ,"RRN" ,"Authcode" ,  "MaskedPAN" ,"Additionaldata"); 

}
function su(resul) {
    var d_ = jQuery.parseJSON(JSON.stringify(resul));
    $("#tbl_btDyms tbody tr").remove();
    for (var i = 0; i < d_.length; i++) {  
        var markup = "<tr><td style='width: 40px;'><i class='fa fa-tablet' aria-hidden='true'></i></td><td id='dCalAlign'><a  onclick=pickDevice(\'" + d_[i] + "\')>" + d_[i]+"</td></tr>";
        $("#tbl_btDyms tbody").append(markup);
    }
}
function fa(resul) {
 
}


function pickPayDevice(name) {
    var bt = cordova.plugins.Apiplugin.Instance;
    bt = bt(suPyaDevice, faPyaDevice, name); 
}

function suPyaDevice(resl)
{
    if (resl == "Connection Failed") {
        testSale();
        $('#myModalBTPaymentTerminal').modal('show');
    }
    else { 
        SaleTransaction();
    }
}

function faPyaDevice(name) {

}
function pickDevice(name)
{  
    var bt = cordova.plugins.Apiplugin.Instance;
    bt = bt(suInstance, faInstance, name);
    window.localStorage.setItem('STPaymentTerminalName', name);
}
function suInstance(resl)
{ 
    if (resl == "Connection Failed") {
        testSale();
        $('#myModalBTPaymentTerminal').modal('show');
    }
    else {

        window.localStorage.setItem('PaymentTerminalName', window.localStorage.getItem("STPaymentTerminalName"));
        $('#myModalBTPaymentTerminal').modal('hide');
    }
}
function faInstance(resl) {  
  
}
function VoidPOP() {

    $('#ModalPrimaryUserPWDPopup').modal('show');
}
function GetCheckPrimarypassword() {
    $('#ModalPasswordPopup').modal('show');
}
function UpdateVoid(InvoiceId, VRemarks) {
    if (InvoiceId == "") {

        navigator.notification.alert($("#hdnChkInvNo").val(), "", "neo ecr", "Ok");
    }
    else if (VRemarks == "") {

        navigator.notification.alert($("#hdnChkRemarks").val(), "", "neo ecr", "Ok");
    }
    else {
        var SubOrg_Id; var LASTUPDATEDBY;
        var CurDate = new Date();

        var DateWithFormate = CurDate.getFullYear() + '-' + (CurDate.getMonth() + 1) + '-' + CurDate.getDate() + ' ' + CurDate.getHours() + ':' + CurDate.getMinutes() + ':' + CurDate.getSeconds() + ':' + CurDate.getMilliseconds();

        var HashValues = (window.localStorage.getItem("hashValues"));
        if (HashValues != null && HashValues != '') {
            var hashSplit = HashValues.split('~');
            SubOrg_Id = hashSplit[1];
            LASTUPDATEDBY = HashValues[14];
        }

        VoidData = {};
        objVoidData = [];
        VoidData["SubOrgId"] = SubOrg_Id;
        VoidData["InvoiceId"] = InvoiceId;
        VoidData["VRemarks"] = VRemarks;
        VoidData["LASTUPDATEDBY"] = LASTUPDATEDBY;
        VoidData["DateWithFormate"] = DateWithFormate;
        VoidData["VD"] = window.localStorage.getItem("UUID");
        objVoidData.push(VoidData);

        var VoidInvoiceParm = new Object();
        VoidInvoiceParm.VoidData = JSON.stringify(objVoidData);
        VoidInvoiceParm.VD = VD1();
        var ComplexObject = new Object();
        ComplexObject.VoidInvoiceById = VoidInvoiceParm;
        var Data = JSON.stringify(ComplexObject);
        $.ajax({
            type: 'POST',
            url: CashierLite.Settings.UpdateInvoiceByVoid,
            data: Data,
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            success: function (resp) {
              
                if (resp != null) {
                    if (resp.UpdateInvoiceByVoidResult == '100000') {//Success Record  
                        navigator.notification.alert($("#txtInvoiceNo").val().trim() + $('#hdnVoidSuccess').val(), "", "neo ecr", "Ok");

                        $('#ModalVoid').modal('hide');
                    }
                    else {
                        navigator.notification.alert($('#hdnErrOccer').val(), "", "neo ecr", "Ok");
                    }
                }
                return false;
            },
            error: function (e) {
                navigator.notification.alert($('#hdnErrOccer').val(), "", "neo ecr", "Ok");

            }
        });
    }
}

//invoice autofill in void pop up
$("[id$='txtInvoiceNo']").autocomplete({
    dataFilter: function (data) { return data },
    source: function (request, response) {
        var HashValues = (window.localStorage.getItem("hashValues"));
        if (HashValues != null && HashValues != '') {
            var hashSplit = HashValues.split('~');
            SubOrgId = hashSplit[1];
        }
        $.ajax({
            url: CashierLite.Settings.GetInvoices,
            data: "subOrgId=" + SubOrgId + "&VD=" + VD1() + "&InvoiceNo=" + $('#txtInvoiceNo').val().trim(),
            dataType: "json",
            type: "Get",
            contentType: "application/json; charset=utf-8",
            success: function (resp) {
                if (resp != null) {
                    var Invoices = resp;
                    jsonObjInv = [];
                    if (Invoices != "" && Invoices != null) {
                        var Inv_ = Invoices;
                        for (var i = 0; i < Inv_.length; i++) {
                            var InvChek = Inv_[i];
                            Inv = {
                            }
                            Inv["InvoiceId"] = InvChek.CASHINVID;
                            Inv["InvoiceNo"] = InvChek.CASHINVNUMBER;
                            Inv["InvoiceDate"] = InvChek.CASHINVDATE;
                            jsonObjInv.push(Inv);
                        }
                        response($.map(jsonObjInv, function (el) {

                            return {
                                label: el.InvoiceNo,
                                val: el.InvoiceId,
                                Date: el.InvoiceDate
                            };
                        }
                        ));
                    }
                }
            }
        });
    },
    change: function (e, i) {
        if (i.item) {
        }
        else {
            $('#txtInvoiceNo').val('');
            $('#hdnInvoiceId').val('');
        }
    },
    select: function (event, ui) {
        $('#txtInvoiceNo').val(ui.item.label);
        $('#hdnInvoiceId').val(ui.item.val);
    }
});

function CartItemsPULL() {
    window.localStorage.setItem('ctCheck', 1);
    if (window.localStorage.getItem("DraftItemsHeader") != null) {
        var cusHeader = window.localStorage.getItem("DraftItemsHeader").split('$');
        $("#itemCustable tbody tr").remove();
        if (cusHeader.length > 0) {
            for (var i = 0; i < cusHeader.length; i++) {
                var cusHeader_ = cusHeader[i].split('~');

                if (cusHeader_[0] != "") {
                    var markup = "<tr id=" + 'tr_' + cusHeader_[0] + " onclick=ActiveRowgetCus(\'" + cusHeader_[0] + "\');><td style='width:5% !important'><i class='fa fa-trash' onclick='deleteRowCus(this)' style='font-size:16px'></i></td><td id='txtalignCols'>" + cusHeader_[1] + "</td><td>" + parseFloat(cusHeader_[2]).toFixed(2) + "</td><td style='display:none'>" + cusHeader_[0] + "</td></tr>";
                    $("#itemCustable tbody").append(markup);
                }
            }
        }
    }
}


function ddpActiveRowgetCus(CustId) {
    window.localStorage.setItem('ctCheck', 1);
    valueAppendtoPOPUp();
    $("#tbl_tenderDyms tr").remove();
    $('#lbl_TTpartTotal')[0].innerText = '0';
    if (window.localStorage.getItem(CustId) != null) {
        var CusDraftData = window.localStorage.getItem(CustId).split('$');
        if (CusDraftData[0] != "") {
            if (CusDraftData.length > 0) {
                for (var m = 0; m < CusDraftData.length; m++) {

                    var cusItems_ = CusDraftData[m].split('~');
                    if (cusItems_[0] != "") {
                        var markup = "<tr id=" + 'tr_' + cusItems_[2] + " onclick=ActiveRowget(\'" + cusItems_[2] + "\',\'" + cusItems_[4] + "\','1');><td style='width:5% !important' class='tdClor'><i class='fa fa-trash' onclick='deleteRow(this)' style='font-size:16px'></i></td><td class='text-align:right' class='tdClor'>" + cusItems_[3] + "</td><td class='tdClor' >" + parseFloat(cusItems_[7]).toFixed(2) + "</td><td class='tdClor'>" + parseFloat(cusItems_[5]).toFixed(2) + "</td><td class='tdClor'>" + parseFloat(cusItems_[6]).toFixed(2) + "</td><td style='display:none' class='tdClor'>" + cusItems_[2] + "</td><td class='tdClor'><input type='text' class='form-control'  value='" + cusItems_[4] + "' id='" + 'td_' + cusItems_[2] + "' style='display:none'></td></tr>";

                        $("#itemCarttable tbody").append(markup);
                    }
                    else {
                        //$("#dpCusData")[0].selectedIndex = 0;
                    }
                }
                $('#txthdnRemoveStatus').val('1');
                //removeDraft(CustId);
                GradCaluclation();
                ClearFileds();
            }
        }
        else {
            $("#itemCarttable tr").remove();
            GradCaluclation();
            ClearFileds();
        }
    }


}
function ActiveRowgetCus(CustId) {
    window.localStorage.setItem('ctCheck', 1);
    $("#tbl_tenderDyms tr").remove();
    $('#lbl_TTpartTotal')[0].innerText = '0';
    $('#myModalDrafts').modal('hide');
    //$('#myModalDrafts').hide();
    if ($('#txthdnCusRemoveStatus').val() == '0') {

        $('#itemCarttable')[0].innerHTML = '';
        var CusDraftData = window.localStorage.getItem(CustId);
        if (CusDraftData[0] != "") {
            if (CusDraftData.length > 0) {

                var CusDData = window.localStorage.getItem(CustId + '#0');
                if (CusDData != '') {

                    var spliCus = CusDData.split('~');
                    if (spliCus[1].indexOf('G#') == -1) {
                        $('#hdn_CusId').val(spliCus[0]);
                        $('#txtCusName').val(spliCus[1]);
                        GetRewardPoints(spliCus[0])
                    }
                    else {
                        $('#hdn_CusId').val('');
                        $('#txtCusName').val('');
                        jQuery("label[for='lblRedeem_points'").html('0.00');
                        jQuery("label[for='lblRewardPoints'").html('0.00');
                    }

                }
                $('#itemCarttable')[0].innerHTML = CusDraftData;
                $('#txthdnRemoveStatus').val('1');
                GradCaluclation();
                ClearFileds();

            }
        }
        else {
            $('#hdn_CusId').val('');
            $('#txtCusName').val('');
        }
        valueAppendtoPOPUp();
    }
    removeDraft(CustId)
    $('#txthdnCusRemoveStatus').val('0');

}
function deleteRowCus(r) {
    window.localStorage.setItem('ctCheck', 1);
    $('#txthdnCusRemoveStatus').val('1');
    var i = r.parentNode.parentNode.rowIndex;
    var CusId = r.parentNode.parentNode.children[3].innerHTML;
    removeDraft(CusId);

    var elem = document.getElementById("toggle-field");
    var elemDraft = document.getElementById("toggle-fieldSub");
    $('#hdn_CusId').val('');
    $('#txtCusName').val('');


}
function removeDraft(CustId) {
    window.localStorage.setItem('ctCheck', 1);
    if (window.localStorage.getItem("DraftItemsHeader") != null) {
        window.localStorage.setItem(CustId, "");
        window.localStorage.setItem(CustId + "#0", "");
        var cusHeader = window.localStorage.getItem("DraftItemsHeader").split('$');
        if (cusHeader.length > 0) {
            window.localStorage.removeItem("DraftItemsHeader");
            for (var i = 0; i < cusHeader.length; i++) {
                var cusHeader_ = cusHeader[i].split('~');
                if (cusHeader_[0] != CustId) {
                    var HeadeSub_ = cusHeader_[0] + '~' + cusHeader_[1] + '~' + cusHeader_[2] + '$';
                    if (window.localStorage.getItem("DraftItemsHeader") == null) {
                        window.localStorage.setItem("DraftItemsHeader", HeadeSub_);
                    }
                    else {
                        var HeaderMain = window.localStorage.getItem("DraftItemsHeader") + HeadeSub_;
                        window.localStorage.setItem("DraftItemsHeader", HeaderMain);
                    }
                }
            }
        }
        CartItemsPULL();
    }
}

function dpCusChange() {
    window.localStorage.setItem('ctCheck', 1);

    if ($('#hdn_CusId').val() != "") {
        ddpActiveRowgetCus($('#hdn_CusId').val());
        var elem = document.getElementById("toggle-field");
        var elemDraft = document.getElementById("toggle-fieldSub");
        elem.style.display = "block";
        elemDraft.style.display = "none";
        $('.content-panel').css('position', 'absolute');
        $('.content-panel').css('width', '50%');
        $('.content-panel').css('right', '0px');
    }
}


function ReassignSubVals(itemId) {
    window.localStorage.setItem('ctCheck', 1);
    var myTab1_ = document.getElementById('itemCarttable');
    var m_ = "";
    for (i = 0; i < myTab1_.rows.length; i++) {
        var objCells = myTab1_.rows.item(i).cells;

        if (itemId == objCells.item(5).innerHTML) {
            $('#txtAmount').val(objCells.item(4).innerHTML);
            break;
        }
    }
}
function ActiveRowget(itemId) {
    window.localStorage.setItem('ctCheck', 1);
    valueAppendtoPOPUp();
    $("#tbl_tenderDyms tr").remove();
    $('#lbl_TTpartTotal')[0].innerText = '0';
    var myTab = document.getElementById('itemCarttable');
    var m_ = "";
    for (i = 0; i < myTab.rows.length; i++) {

        var objCells = myTab.rows.item(i).cells;
        try {
            var v_ = 'tr_' + objCells.item(5).innerHTML;
            document.getElementById(v_).style.cssText = 'font-weight:normal';
            if (itemId == objCells.item(5).innerHTML) {
                $('#txthdnItemId').val(itemId);
                $('#txtQuantity').val(objCells.item(2).innerHTML);
                $('#txtPrice').val(parseFloat($('#td_' + itemId).val()));
                $('#txtAmount').val(objCells.item(4).innerHTML);
                $('#txtDiscount').val(objCells.item(3).innerHTML);
                $('#txtUOM').val(objCells.item(8).innerHTML);
                $("#txtAmount").attr("disabled", "disabled");
                $("#txthdnItemId").attr("disabled", "disabled");
                $("#txtQuantity").attr("disabled", "disabled");
                $("#txtPrice").attr("disabled", "disabled");
                $("#txtUOM").attr("disabled", "disabled");
                $("#txtDiscount").attr("disabled", "disabled");
                $("#txtPrice").attr("disabled", "disabled");
                document.getElementById(v_).style.cssText = 'font-weight:bold';

            }
            $("#txthdnActiveColumn").val("");
            document.getElementById('liQtyM').style.cssText = 'color:white;background: #FF7276;';
            document.getElementById('liDisM').style.cssText = 'color:white;background: #FF7276;';
            document.getElementById('liPriceM').style.cssText = 'color:white;background: #FF7276;';
        }
        catch (r) {

        }
    }
}
function MMSplitDecode(sText, sSymbol) {
    var Splitter = '';
    switch (sSymbol) {
        case 'Š':
            Splitter = '%u0160';
            break;
        case '%u02DC': //˜
            Splitter = '%u02DC';
            break;
        case '%B0': //°
            Splitter = '%B0';
            break;
        case '%F7': //÷
            Splitter = '%F7';
            break;
        case '%u0178':
            Splitter = '%u0178';
            break;
    }
    var str = escape(sText).split(Splitter);
    for (var iCount = 0; iCount < str.length; iCount++)
        str[iCount] = unescape(str[iCount]);
    return str;
}



function prev() {
    window.localStorage.setItem('ctCheck', 1);
    if ($('#hdnPageIndex').val() == 1) {
        $('#liPrev').addClass('inactive').removeClass('active');
        $('#liNext').addClass('active').removeClass('inactive');
    } else {
        $('#hdnPageIndex').val(parseInt($('#hdnPageIndex').val()) - 1);
        GetItemDetails();
        $('#liPrev').addClass('active').removeClass('inactive');
    }
}

function next() {
    window.localStorage.setItem('ctCheck', 1);
    $('#liPrev').addClass('active').removeClass('inactive');
    $('#hdnPageIndex').val(parseInt($('#hdnPageIndex').val()) + 1);
    GetItemDetails();
}


function GetItemDetails() {
    window.localStorage.setItem('ctCheck', 1);
    var SubOrgId = '';
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        SubOrgId = hashSplit[1] + ',' + hashSplit[16];
    }



    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetINVItems,
        data: "subOrgId=" + SubOrgId + "&VD=" + VD1() + "&pageIndex=" + $('#hdnPageIndex').val(),
        success: function (resp) {
            if (resp.length > 0) {
                var ItemsData = resp;
                document.getElementById("lblStockCt").innerHTML = resp.length;
                var li_ = "";
                document.getElementById('tbl_Item').childNodes[0].nodeValue = null;
                var MainData = '';

                for (var i = 0; i < ItemsData.length; i++) {
                    var mapCode_ = "";
                    var profileData_ = "";
                    mapCode_ = ItemsData[i].O2.replace('$', '&');
                    if (mapCode_ != '')
                        for (var k = 0; k < ItemsData[i].profileData.length; k++) {
                            var cols = ItemsData[i].profileData[k].split('~');
                            profileData_ = profileData_ + "@" + cols[0] + "#" + cols[1] + "#" + cols[2] + "#" + cols[3] + "#" + cols[4] + "#" + cols[5] + "#" + cols[6] + "#" + cols[8] + "#" + cols[9] + "#" + cols[10] + "#" + cols[11] + "#" + cols[12] + "#" + cols[13] + "#" + cols[14] + "#" + cols[15] + "#" + cols[16] + "#" + cols[17];
                        }

                    var img_ = ""; var style_ = "";

                    if (ItemsData[i].img != '') {

                        img_ = ItemsData[i].img;

                        style_ = "style='background-image:url(" + img_ + ");background-size:100% 80%;background-repeat:no-repeat;display:block;width:90px;height:90px;background-color:transparent;text-align:center;'"

                        li_ = li_ + "<li><a href='#' onclick=onSubData(\'" + ItemsData[i].Code + "\',\'" + ItemsData[i].ItemId + "\',\'" + ItemsData[i].Name + "\',\'" + ItemsData[i].Price + "\',\'" + ItemsData[i].Disc + "\',\'" + ItemsData[i].UOMID + "\',\'" + ItemsData[i].UOMCode + "\',\'" + mapCode_.replace(" ", "") + "\',\'" + profileData_.replace(" ", "") + "\',\'" + img_ + "\');> <div class='content-body-item'>";

                        li_ = li_ + "<h5 style='font-size:12px'>" + ItemsData[i].Code + "</h5><div   " + style_ + " class='item-sec'></div>";

                        li_ = li_ + "<p style='color:#000000;font-size:80%;text-align:center;word-wrap:break-word;white-space:pre-line;line-height:20px;font-weight:bold'>" + ItemsData[i].Name + "</p>";

                        li_ = li_ + "<div style='display:none'>" + ItemsData[i].BarCode.toUpperCase() + "</div><div style='display:none'>" + mapCode_.replace(" ", "") + "</div><div style='display:none'>" + profileData_.replace(" ", "") + "</div>";

                        li_ = li_ + "<small style='float:left;clear:both'>" + ItemsData[i].Price + "</small><small style=' float:right;clear:right' >" + ItemsData[i].UOMCode + "</small>";

                        li_ = li_ + "<br/><small >" + ItemsData[i].Alias + "</small></div>";

                        li_ = li_ + "</a></li>";

                    }
                    else {
                        li_ = li_ + "<li><a href='#' onclick=onSubData(\'" + ItemsData[i].Code + "\',\'" + ItemsData[i].ItemId + "\',\'" + ItemsData[i].Name + "\',\'" + ItemsData[i].Price + "\',\'" + ItemsData[i].Disc + "\',\'" + ItemsData[i].UOMID + "\',\'" + ItemsData[i].UOMCode + "\',\'" + mapCode_.replace(" ", "") + "\',\'" + profileData_.replace(" ", "") + "\');> <div class='content-body-item'>";
                        li_ = li_ + "<h5 style='font-size:12px'>" + ItemsData[i].Code + "</h5><div class='item-sec'>";
                        li_ = li_ + "<samp>" + ItemsData[i].Price + "</samp>";
                        li_ = li_ + "<br/><small>" + ItemsData[i].UOMCode + "</small></div><div style='display:none'>" + ItemsData[i].BarCode.toUpperCase() + "</div><div style='display:none'>" + mapCode_.replace(" ", "") + "</div><div style='display:none'>" + profileData_.replace(" ", "") + "</div>";
                        li_ = li_ + "<small >" + ItemsData[i].Name + "</small>";
                        li_ = li_ + "<br/><small >" + ItemsData[i].Alias + "</small></div>";
                        li_ = li_ + "</a></li>";
                    }

                    var Code = ItemsData[i].Code.toUpperCase();
                    var ItemId = ItemsData[i].ItemId;
                    var Name = ItemsData[i].Name;
                    var Price = ItemsData[i].Price;
                    var Disc = ItemsData[i].Disc;
                    var UOMCode = ItemsData[i].UOMCode;
                    var UOMName = ItemsData[i].UOMName;
                    var UOMID = ItemsData[i].UOMID;
                    var BarCode = ItemsData[i].BarCode.toUpperCase();
                    var StockData = ItemsData[i].StockData;
                    var Item_Group_Code = ItemsData[i].Item_Group_Code;
                    var Item_Group_Id = ItemsData[i].Item_Group_Id;
                    var O1 = ItemsData[i].O1;
                    var img_ = ItemsData[i].img;
                    var alias = ItemsData[i].Alias;
                    if (MainData != '')
                        MainData = MainData + Code + '~' + ItemId + '~' + Name + '~' + Price + '~' + Disc + '~' + UOMCode + '~' + UOMName + '~' + UOMID + '~' + BarCode + '~' + StockData + '~' + mapCode_ + '~' + profileData_ + '~' + img_ + '~' + alias + '$';
                    else
                        MainData = Code + '~' + ItemId + '~' + Name + '~' + Price + '~' + Disc + '~' + UOMCode + '~' + UOMName + '~' + UOMID + '~' + BarCode + '~' + StockData + '~' + mapCode_ + '~' + profileData_ + '~' + img_ + '~' + alias + '$';
                }

                document.getElementById('tbl_Item').innerHTML = "<ul  id='myTable'>" + li_ +
                    "</ul>";



            } else {
                $('#hdnPageIndex').val(parseInt($('#hdnPageIndex').val()) - 1);
                $('#liNext').addClass('inactive').removeClass('active');
                $('#liPrev').addClass('active').removeClass('inactive');
            }
            GetStockCount();
        },
        error: function (e) {

        }
    });

}
var data_ = '';
$('#div_OnlyInvoice').click(function (e) {


});
function GetShiftDataRTDRCHK() {
    window.localStorage.setItem('ctCheck', 1);
    if (window.localStorage.getItem("CashierID") != null) {
        var SubOrgId = ''; var CashierID = window.localStorage.getItem("CashierID");
        var HashValues = (window.localStorage.getItem("hashValues"));
        if (HashValues != null && HashValues != '') {
            var hashSplit = HashValues.split('~');
            SubOrgId = hashSplit[1];
        }
        if (window.localStorage.getItem('network') == '1') {

            $.ajax({
                type: 'GET',
                url: CashierLite.Settings.ShiftCheck,
                data: "CashierID=" + CashierID + "&OrgId=" + SubOrgId + "&VD=" + VD1(),
                success: function (resp) {

                    var result = resp;
                    if (result.length > 0) {
                        var ACTIVE = result[0].ACTIVE;
                        var COUNTERID = result[0].COUNTERID;
                        var COUNTERNAME = result[0].COUNTERNAME;
                        var COUNTERNUMBER = result[0].COUNTERNUMBER;
                        var COUNTERTYPE = result[0].COUNTERTYPE;
                        var End_Time = result[0].End_Time;
                        var Is_Next_Day = result[0].Is_Next_Day;
                        var SHIFTID = result[0].SHIFTID;
                        var Shift_Code = result[0].Shift_Code;
                        var Shift_Name = result[0].Shift_Name;
                        var Start_Time = result[0].Start_Time;
                        var StageAlert = result[0].StageAlert;

                        window.localStorage.setItem('ACTIVE', result[0].ACTIVE);
                        window.localStorage.setItem('COUNTERID', result[0].COUNTERID);
                        window.localStorage.setItem('COUNTERNAME', result[0].COUNTERNAME);
                        window.localStorage.setItem('COUNTERNUMBER', result[0].COUNTERNUMBER);
                        window.localStorage.setItem('COUNTERTYPE', result[0].COUNTERTYPE);
                        window.localStorage.setItem('End_Time', result[0].End_Time);
                        window.localStorage.setItem('Is_Next_Day', result[0].Is_Next_Day);
                        window.localStorage.setItem('SHIFTID', result[0].SHIFTID);
                        window.localStorage.setItem('Shift_Code', result[0].Shift_Code);
                        window.localStorage.setItem('Shift_Name', result[0].Shift_Name);
                        window.localStorage.setItem('Start_Time', result[0].Start_Time);
                        var StageAlert = result[0].StageAlert;
                        DashBoardDataFill();
                    }
                    else {

                    }
                },
                error: function (e) {

                }
            });
        }
    }
    else {

    }


}

function GetClosedShiftData() {
    window.localStorage.setItem('ctCheck', 1);
    if (window.localStorage.getItem("CashierID") != null) {

        var SubOrgId = ''; var CashierID = window.localStorage.getItem("CashierID");
        var HashValues = (window.localStorage.getItem("hashValues"));
        if (HashValues != null && HashValues != '') {
            var hashSplit = HashValues.split('~');
            SubOrgId = hashSplit[1];
        }


        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.ClosedShiftCheck,
            data: "CashierID=" + CashierID + "&OrgId=" + SubOrgId + "&VD=" + VD1(),
            success: function (resp) {
                var result = resp;
                if (result.length > 0) {
                    var ACTIVE = result[0].ACTIVE;
                    var COUNTERID = result[0].COUNTERID;
                    var COUNTERNAME = result[0].COUNTERNAME;
                    var COUNTERNUMBER = result[0].COUNTERNUMBER;
                    var COUNTERTYPE = result[0].COUNTERTYPE;
                    var End_Time = result[0].End_Time;
                    var Is_Next_Day = result[0].Is_Next_Day;
                    var SHIFTID = result[0].SHIFTID;
                    var Shift_Code = result[0].Shift_Code;
                    var Shift_Name = result[0].Shift_Name;
                    var Start_Time = result[0].Start_Time;
                    var StageAlert = result[0].StageAlert;

                    window.localStorage.setItem('ACTIVE', result[0].ACTIVE);
                    window.localStorage.setItem('COUNTERID', result[0].COUNTERID);
                    window.localStorage.setItem('COUNTERNAME', result[0].COUNTERNAME);
                    window.localStorage.setItem('COUNTERNUMBER', result[0].COUNTERNUMBER);
                    window.localStorage.setItem('COUNTERTYPE', result[0].COUNTERTYPE);
                    window.localStorage.setItem('End_Time', result[0].End_Time);
                    window.localStorage.setItem('Is_Next_Day', result[0].Is_Next_Day);
                    window.localStorage.setItem('SHIFTID', result[0].SHIFTID);
                    window.localStorage.setItem('Shift_Code', result[0].Shift_Code);
                    window.localStorage.setItem('Shift_Name', result[0].Shift_Name);
                    window.localStorage.setItem('Start_Time', result[0].Start_Time);
                    var StageAlert = result[0].StageAlert;

                    window.open('TodaysCloseoutRP.html', '_self', 'location=no');

                }
                else {

                    navigator.notification.alert($('#hdnDontPermisAccessInvoice').val(), "", "neo ecr", "Ok");

                }
            },
            error: function (e) {
                navigator.notification.alert($('#hdnInvalidData').val(), "", "neo ecr", "Ok");

            }
        });
    }
    else {
        navigator.notification.alert($('#hdnPleaseContactAdmin').val(), "", "neo ecr", "Ok");

    }


}


function GetShiftDataRTDR() {
    window.localStorage.setItem('ctCheck', 1);
    if (window.localStorage.getItem("CashierID") != null) {

        var SubOrgId = ''; var CashierID = window.localStorage.getItem("CashierID");
        var HashValues = (window.localStorage.getItem("hashValues"));
        if (HashValues != null && HashValues != '') {
            var hashSplit = HashValues.split('~');
            SubOrgId = hashSplit[1];
        }

        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.ShiftCheck,
            data: "CashierID=" + CashierID + "&OrgId=" + SubOrgId + "&VD=" + VD1(),
            success: function (resp) {
                var result = resp;
                if (result.length > 0) {
                    var ACTIVE = result[0].ACTIVE;
                    var COUNTERID = result[0].COUNTERID;
                    var COUNTERNAME = result[0].COUNTERNAME;
                    var COUNTERNUMBER = result[0].COUNTERNUMBER;
                    var COUNTERTYPE = result[0].COUNTERTYPE;
                    var End_Time = result[0].End_Time;
                    var Is_Next_Day = result[0].Is_Next_Day;
                    var SHIFTID = result[0].SHIFTID;
                    var Shift_Code = result[0].Shift_Code;
                    var Shift_Name = result[0].Shift_Name;
                    var Start_Time = result[0].Start_Time;
                    var StageAlert = result[0].StageAlert;

                    window.localStorage.setItem('ACTIVE', result[0].ACTIVE);
                    window.localStorage.setItem('COUNTERID', result[0].COUNTERID);
                    window.localStorage.setItem('COUNTERNAME', result[0].COUNTERNAME);
                    window.localStorage.setItem('COUNTERNUMBER', result[0].COUNTERNUMBER);
                    window.localStorage.setItem('COUNTERTYPE', result[0].COUNTERTYPE);
                    window.localStorage.setItem('End_Time', result[0].End_Time);
                    window.localStorage.setItem('Is_Next_Day', result[0].Is_Next_Day);
                    window.localStorage.setItem('SHIFTID', result[0].SHIFTID);
                    window.localStorage.setItem('Shift_Code', result[0].Shift_Code);
                    window.localStorage.setItem('Shift_Name', result[0].Shift_Name);
                    window.localStorage.setItem('Start_Time', result[0].Start_Time);
                    var StageAlert = result[0].StageAlert;
                    if (StageAlert == '1') {
                        if (ACTIVE.toLowerCase() == 'y') {
                            window.open('TodaySalesDetailsRP.html', '_self', 'location=no');
                        }
                        else {
                            navigator.notification.alert($('#hdnDontPermisAccessInvoice').val(), "", "neo ecr", "Ok");

                        }
                    }
                    else if (StageAlert == '2') {
                        navigator.notification.alert($('#hdnNotAssignCountrToTrans').val(), "", "neo ecr", "Ok");

                    }
                }
                else {
                    navigator.notification.alert($('#hdnDontPermisAccessInvoice').val(), "", "neo ecr", "Ok");

                }
            },
            error: function (e) {
                navigator.notification.alert($('#hdnInvalidData').val(), "", "neo ecr", "Ok");

            }
        });
    }
    else {
        navigator.notification.alert($('#hdnPleaseContactAdmin').val(), "", "neo ecr", "Ok");

    }


}
function GetShiftData() {
    window.localStorage.setItem('ctCheck', 1);
    if (window.localStorage.getItem("CashierID") != null) {
        var SubOrgId = ''; var CashierID = window.localStorage.getItem("CashierID");
        var HashValues = (window.localStorage.getItem("hashValues"));
        if (HashValues != null && HashValues != '') {
            var hashSplit = HashValues.split('~');
            SubOrgId = hashSplit[1];
        }


        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.ShiftCheck,
            data: "CashierID=" + CashierID + "&OrgId=" + SubOrgId + "&VD=" + VD1(),
            success: function (resp) {

                var result = resp;
                if (result.length > 0) {
                    var ACTIVE = result[0].ACTIVE;
                    var COUNTERID = result[0].COUNTERID;
                    var COUNTERNAME = result[0].COUNTERNAME;
                    var COUNTERNUMBER = result[0].COUNTERNUMBER;
                    var COUNTERTYPE = result[0].COUNTERTYPE;
                    var End_Time = result[0].End_Time;
                    var Is_Next_Day = result[0].Is_Next_Day;
                    var SHIFTID = result[0].SHIFTID;
                    var Shift_Code = result[0].Shift_Code;
                    var Shift_Name = result[0].Shift_Name;
                    var Start_Time = result[0].Start_Time;
                    var StageAlert = result[0].StageAlert;

                    window.localStorage.setItem('ACTIVE', result[0].ACTIVE);
                    window.localStorage.setItem('COUNTERID', result[0].COUNTERID);
                    window.localStorage.setItem('COUNTERNAME', result[0].COUNTERNAME);
                    window.localStorage.setItem('COUNTERNUMBER', result[0].COUNTERNUMBER);
                    window.localStorage.setItem('COUNTERTYPE', result[0].COUNTERTYPE);
                    window.localStorage.setItem('End_Time', result[0].End_Time);
                    window.localStorage.setItem('Is_Next_Day', result[0].Is_Next_Day);
                    window.localStorage.setItem('SHIFTID', result[0].SHIFTID);
                    window.localStorage.setItem('Shift_Code', result[0].Shift_Code);
                    window.localStorage.setItem('Shift_Name', result[0].Shift_Name);
                    window.localStorage.setItem('Start_Time', result[0].Start_Time);
                    var StageAlert = result[0].StageAlert;
                    if (StageAlert == '1') {
                        window.open('posmain.html?#', '_self', 'location=no');

                    }
                    else if (StageAlert == '2') {
                        navigator.notification.alert($('#hdnNotAssignCountrToTrans').val(), "", "neo ecr", "Ok");

                    } else if (StageAlert == '3') {
                        navigator.notification.alert($('#hdnNotAssignCountrToTrans').val(), "", "neo ecr", "Ok");

                    }
                }
                else {
                    navigator.notification.alert($('#hdnNPermissionInvoice').val(), "", "neo ecr", "Ok");

                }
            },
            error: function (e) {
                navigator.notification.alert($('#hdnInvalData').val(), "", "neo ecr", "Ok");

            }
        });
    }
    else {
        navigator.notification.alert($('#hdnContactAdmin').val(), "", "neo ecr", "Ok");

    }


}

function createJSON() {
    jsonObj = [];
    $("input[class=email]").each(function () {

        var id = $(this).attr("title");
        var email = $(this).val();

        item = {
        }
        item["title"] = id;
        item["email"] = email;

        jsonObj.push(item);
    });

    console.log(jsonObj);
}
function GetCustomers1() {
    var SubOrgId = ''; var CashierID = window.localStorage.getItem("CashierID");
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        SubOrgId = hashSplit[1];
    }
    window.localStorage.setItem('ctCheck', 1);
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetCustomers,
        data: "VD=" + VD1(),
        success: function (resp) {
            var CusData = resp;

            var MainData = '';
            jsonObj = [];
            for (var i = 0; i < CusData.length; i++) {
                var CusId = CusData[i].CusID;
                var CusName = CusData[i].Code + "/" + CusData[i].Name;
                if (MainData != '')
                    MainData = MainData + CusId + '~' + CusName + '$';
                else
                    MainData = CusId + '~' + CusName + '$';
            };
            window.localStorage.setItem("FillCustomer", MainData);
        },
        error: function (e) {
        }
    });
}

function GetCustomers_() {

    window.localStorage.setItem('ctCheck', 1);
    var HashVal = (window.localStorage.getItem("hashValues"));
    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');
    }

    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetCustomerSummaryGrid,
        data: "OrgId=" + HashValues[15] + "&VD=" + VD1(),

        success: function (resp) {
            var GridData = resp;

            var MainData = '';
            var SummaryTable = '';
            for (var i = 0; i < GridData.length; i++) {
                var CustId = GridData[i].CUSTOMER_ID;
                var CustCode = GridData[i].CUSTOMER_CODE;
                var CustName = GridData[i].CUSTOMER_NAME;
                var Status = GridData[i].STATUS;
                var RewardPoints = GridData[i].REWARDPOINTS;
                var phoneNo = GridData[i].PHONENO;
                var CusId = CustId;
                var CusName = phoneNo + " / " + CustName;
                if (Status.toUpperCase() == "ACTIVE")
                    if (MainData != '')
                        //MainData = MainData + CusId + '~' + CusName  + '$';
                        MainData = MainData + CusId + '~' + CusName + '~' + RewardPoints + '$';
                    else
                        MainData = CusId + '~' + CusName + '~' + RewardPoints + '$';

            };
            window.localStorage.setItem("FillCustomer1", MainData);
            //  $('#txtItemScan').focus();
        },
        error: function (e) {
        }
    });
}
try {
    $("#txtCusNamea").autocomplete({
        source: function (req, response) {
            $.ajax({

                success: function (data) {
                    window.localStorage.setItem('ctCheck', 1);
                    var re = $.ui.autocomplete.escapeRegex(req.term);
                    var matcher = new RegExp("^" + re, "i");
                    response($.grep(data, function (item) {
                        return matcher.test(item.value);
                    }));
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            //custom select function if needed
        }
    });
}
catch (a) {

}
$('#txtCusName').dblclick(function () {
    window.localStorage.setItem('ctCheck', 1);
    BootstrapDialog.show({
        message: $('<div></div>').load('/' + window.location.href.split('/')[3] + '/Common/SimpleSearch.aspx?keypair=' + 'SDOnlyOrgs' + '&ModuleName=' + 'Sales' + '&Params=' + strParams + ''),
        title: "<h1> " + strOrganization + " </h1><small>" + strPickerDesc + " </small>",
        buttons: [{
            id: 'btnOk',
            label: strARCok,
            cssClass: 'btn-primary',
            action: function (dialogRef) {


            }
        },
        {
            id: 'btnCancel',
            label: strARCCancel,
            cssClass: 'btn-primary',
            action: function (dialogRefClose) {
                dialogRefClose.close();
            }
        }]
    });
});

try {
    $("#txtCusName").autocomplete({

        dataFilter: function (data) {
            return data
        },
        source: function (req, add) {
            window.localStorage.setItem('ctCheck', 1);
            var v = (window.localStorage.getItem("FillCustomer1"));
            jsonObj = [];
            if (v != "") {
                var cus_ = v.split('$');
                for (var i = 0; i < cus_.length; i++) {
                    var cusChek = cus_[i].split('~');
                    if (cusChek.length > 1) {
                        item = {
                        }
                        item["value"] = cusChek[1];
                        item["data"] = cusChek[0];
                       // item["rewardPoints"] = cusChek[2];
                        jsonObj.push(item);
                    }
                }
            }
            add($.map(jsonObj, function (el) {
                var re = $.ui.autocomplete.escapeRegex(req.term);
                if (re.trim() != "") {
                    if (el.value.toLowerCase().indexOf(re.toLowerCase()) != -1) {
                        return {
                            label: el.value,
                            val: el.data

                        };
                    }
                }
                else {
                    return {
                        label: el.value,
                        val: el.data
                    };
                }
            }));
        },
        change: function (e, i) {
            if (i.item) {
            }
            else {
                $('#txtCusName').val('');
                $('#hdn_CusId').val('');
            }
        },
        select: function (event, ui) {
            ClearFileds();
            var HashValues = (window.localStorage.getItem("hashValues"));
            if (HashValues != null && HashValues != '') {
                var hashSplit = HashValues.split('~');
                SubOrgId = hashSplit[1];
            }
          //  $("#itemCarttable tr").remove();
            $('#hdn_CusId').val(ui.item.val);
            //GetRewardPoints(ui.item.val);//  jQuery("label[for='lblRewardPoints'").html(ui.item.rewardPoints);
            jQuery("label[for='lblPointsObtainBasecurrency'").html(hashSplit[20]);
            jQuery("label[for='lblMinPoints'").html(hashSplit[21]);
            jQuery("label[for='lblMaxPoints'").html(hashSplit[22]);
            //$('#grdandTotal').text('0.000');
            //dpCusChange();
            popupSts = 0;
        }
    });

}
catch (a) {

}

try {
    $("#txtItemSearchCart").autocomplete({
        minLength: 1,
        source: function (req, res) {
            window.localStorage.setItem('ctCheck', 1);
            var SubOrgId = '';
            var HashValues = (window.localStorage.getItem("hashValues"));
            if (HashValues != null && HashValues != '') {
                var hashSplit = HashValues.split('~');
                //getResources1(hashSplit[7], "POSInvoice");
                SubOrgId = hashSplit[1] + ',' + hashSplit[16];
            }
            if (window.localStorage.getItem('network') == '1') {
                $.ajax({
                    url: CashierLite.Settings.getItemDataAutocomplte,
                    data: "subOrgId=" + SubOrgId + "&VD=" + VD1() + "&passingText=" + $('#txtItemSearchCart').val().trim(),
                    success: function (resp) {
                        res($.map(resp, function (el) {
                            var li_ = "";
                            var mapCode_ = "";
                            var profileData_ = "";
                            mapCode_ = el.O2.replace('$', '&');

                            if (mapCode_ != '')
                                for (var k = 0; k < el.profileData.length; k++) {
                                    var cols = el.profileData[k].split('~');
                                    profileData_ = profileData_ + "@" + cols[0] + "#" + cols[1] + "#" + cols[2] + "#" + cols[3] + "#" + cols[4] + "#" + cols[5] + "#" + cols[6] + "#" + cols[8] + "#" + cols[9] + "#" + cols[10] + "#" + cols[11] + "#" + cols[12] + "#" + cols[13] + "#" + cols[14] + "#" + cols[15] + "#" + cols[16] + "#" + cols[17];
                                }

                            var img_ = "";
                            img_ = el.img;
                            if (el.img != "") {
                                return {
                                    label: '<img style="width:20px;height:20px" src=' + el.img + '>&nbsp;&nbsp;' + el.Code + '/' + el.Name + '  ' + el.Alias,
                                    val: el.Name,
                                    itemChek0: el.Code,
                                    itemChek1: el.ItemId,
                                    itemChek2: el.Name,
                                    itemChek3: el.Price,
                                    itemChek4: el.Disc,
                                    itemChek7: el.UOMID,
                                    itemChek5: el.UOMCode,
                                    itemChek10: mapCode_,
                                    itemChek11: profileData_,
                                    itemName: el.Name,
                                    img_: img_
                                }
                            } else {
                                return {
                                    label: el.Code + '/' + el.Name + '  ' + el.Alias,
                                    val: el.Name,
                                    itemChek0: el.Code,
                                    itemChek1: el.ItemId,
                                    itemChek2: el.Name,
                                    itemChek3: el.Price,
                                    itemChek4: el.Disc,
                                    itemChek7: el.UOMID,
                                    itemChek5: el.UOMCode,
                                    itemChek10: mapCode_,
                                    itemChek11: profileData_,
                                    itemName: el.Name,
                                    img_: img_,
                                }
                            }

                        }))
                    }
                });
            }
            else {
                var db = openDatabase('myItemMaster', '1.0', 'CashierLiteDB', 2 * 1024 * 1024);
                db.transaction(function (tx) {
                    tx.executeSql('SELECT   * FROM items where BarCode like "%' + $("#txtItemSearchCart").val().trim() + '%" or Name like "%' + $("#txtItemSearchCart").val().trim() + '%" or Code like "%' + $("#txtItemSearchCart").val().trim() + '%" or Alias like "%' + $("#txtItemSearchCart").val().trim() + '%" LIMIT 0,10', [], function (tx, results) {
                        var resp = results.rows;
                        res($.map(resp, function (el) {
                            var li_ = "";
                            var mapCode_ = "";
                            var profileData_ = "";
                            mapCode_ = el.O2;
                            if (mapCode_ != '') {
                                var cols = el.profileData.split('~');
                                profileData_ = profileData_ + "@" + cols[0] + "#" + cols[1] + "#" + cols[2] + "#" + cols[3] + "#" + cols[4] + "#" + cols[5] + "#" + cols[6] + "#" + cols[8] + "#" + cols[9] + "#" + cols[10] + "#" + cols[11] + "#" + cols[12] + "#" + cols[13] + "#" + cols[14] + "#" + cols[15] + "#" + cols[16] + "#" + cols[17];
                            }
                            var img_ = "";
                            img_ = el.img;
                            return {
                                label: el.Code + '/' + el.Name + '  ' + el.Alias,
                                val: el.Name,
                                itemChek0: el.Code,
                                itemChek1: el.ItemId,
                                itemChek2: el.Name,
                                itemChek3: el.Price,
                                itemChek4: el.Disc,
                                itemChek7: el.UOMID,
                                itemChek5: el.UOMCode,
                                itemChek10: mapCode_,
                                itemChek11: profileData_,
                                itemName: el.Name,
                                img_: img_,
                            }
                        }))

                    });

                });
            }
        },
        focus: updateTextbox,
        select: updateTextboxSelect
    }).autocomplete("instance")._renderItem = function (ul, item) {
        return $("<li>")
            .append("<img class='imageClass' src=" + item.img + " alt=" + item.img + "/>")
            .append('<a>' + item.itemChek2 + '</a>')
            .appendTo(ul);
    };


    function updateTextbox(event, ui) {
        window.localStorage.setItem('ctCheck', 1);
        $(this).val(ui.item.itemChek2);
        return false;
    }
    function updateTextboxSelect(event, ui) {
        window.localStorage.setItem('ctCheck', 1);
        $(this).val(ui.item.itemChek2);
        onSubData(ui.item.itemChek0, ui.item.itemChek1, ui.item.itemChek2, ui.item.itemChek3, ui.item.itemChek4, ui.item.itemChek7, ui.item.itemChek5, ui.item.itemChek10, ui.item.itemChek11, ui.item.img_);
        $('#txtItemSearchCart').val('');
        return false;
    }
}
catch (a) {

}



function GetRewardPoints(Cust_ID) {
    window.localStorage.setItem('ctCheck', 1);
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetRewardPoints,
        data: "Custid=" + Cust_ID + "&VD=" + VD1(),

        success: function (resp) {

            if (resp != null)
                jQuery("label[for='lblRedeem_points'").html(resp[0].Redeem_Points);
            jQuery("label[for='lblRewardPoints'").html(resp[0].Redeem_Points);
        }
    });
}
function myFunction_() {
    //  $('#txtItemScan').focus();
}
function myFunction() {
    window.localStorage.setItem('ctCheck', 1);
    // Declare variables 
    var input, filter, table, tr, td, i;
    input = document.getElementById("txtItemSearch");
    filter = input.value.toUpperCase();
    table = document.getElementById("myTable");
    tr = table.childNodes;
    var ctVal_ = "0";
    document.getElementById("lblStockCt").innerHTML = ctVal_;
    fillItemsdataFilter_(filter);
}



function fillItemsdataFilter_(Name) {
    window.localStorage.setItem('ctCheck', 1);
    var SubOrgId = '';
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        getResources1(hashSplit[7], "POSInvoice");
        SubOrgId = hashSplit[1] + ',' + hashSplit[16];
    }


    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.getItemDataByFilter,
        data: "subOrgId=" + SubOrgId + "&itemCode=" + Name + "&VD=" + VD1(),
        success: function (resp) {
            var ItemsData = resp;
            document.getElementById("lblStockCt").innerHTML = resp.length;
            var li_ = "";
            document.getElementById('tbl_Item').childNodes[0].nodeValue = null;
            var MainData = '';

            for (var i = 0; i < ItemsData.length; i++) {
                var mapCode_ = "";
                var profileData_ = "";
                mapCode_ = ItemsData[i].O2.replace('$', '&');
                if (mapCode_ != '')
                    for (var k = 0; k < ItemsData[i].profileData.length; k++) {
                        var cols = ItemsData[i].profileData[k].split('~');
                        profileData_ = profileData_ + "@" + cols[0] + "#" + cols[1] + "#" + cols[2] + "#" + cols[3] + "#" + cols[4] + "#" + cols[5] + "#" + cols[6] + "#" + cols[8] + "#" + cols[9] + "#" + cols[10] + "#" + cols[11] + "#" + cols[12] + "#" + cols[13] + "#" + cols[14] + "#" + cols[15] + "#" + cols[16] + "#" + cols[17];
                    }

                var img_ = ""; var style_ = "";
                if (ItemsData[i].img != '') {
                    img_ = ItemsData[i].img;
                    style_ = "style='background-image:url(" + img_ + ");background-size:100% 80%;background-repeat:no-repeat;display:block;width:90px;height:84px;background-color:transparent;margin-bottom:20px;text-align:center;'"
                    li_ = li_ + "<li><a href='#' onclick=onSubData(\'" + ItemsData[i].Code + "\',\'" + ItemsData[i].ItemId + "\',\'" + ItemsData[i].Name + "\',\'" + ItemsData[i].Price + "\',\'" + ItemsData[i].Disc + "\',\'" + ItemsData[i].UOMID + "\',\'" + ItemsData[i].UOMCode + "\',\'" + mapCode_.replace(" ", "") + "\',\'" + profileData_.replace(" ", "") + "\',\'" + img_ + "\');> <div class='content-body-item'>";
                    li_ = li_ + "<h5 style='font-size:12px'>" + ItemsData[i].Code + "</h5><div   " + style_ + " class='item-sec'>";
                    li_ = li_ + "<p style='margin-top:50px;color:#000000;font-size:80%;text-align:center;word-wrap:break-word;white-space:pre-line;line-height:20px;font-weight:bold'>" + ItemsData[i].Name + "</p>";
                    li_ = li_ + "</div><div style='display:none'>" + ItemsData[i].BarCode.toUpperCase() + "</div><div style='display:none'>" + mapCode_.replace(" ", "") + "</div><div style='display:none'>" + profileData_.replace(" ", "") + "</div>";
                    li_ = li_ + "<small style='float:left;clear:both'>" + ItemsData[i].Price + "</small><small style=' float:right;clear:right' >" + ItemsData[i].UOMCode + "</small>";
                    li_ = li_ + "<br/><small >" + ItemsData[i].Alias + "</small></div>";
                    li_ = li_ + "</a></li>";
                }
                else {
                    li_ = li_ + "<li><a href='#' onclick=onSubData(\'" + ItemsData[i].Code + "\',\'" + ItemsData[i].ItemId + "\',\'" + ItemsData[i].Name + "\',\'" + ItemsData[i].Price + "\',\'" + ItemsData[i].Disc + "\',\'" + ItemsData[i].UOMID + "\',\'" + ItemsData[i].UOMCode + "\',\'" + mapCode_.replace(" ", "") + "\',\'" + profileData_.replace(" ", "") + "\');> <div class='content-body-item'>";
                    li_ = li_ + "<h5 style='font-size:12px'>" + ItemsData[i].Code + "</h5><div class='item-sec'>";
                    li_ = li_ + "<samp>" + ItemsData[i].Price + "</samp>";
                    li_ = li_ + "<br/><small>" + ItemsData[i].UOMCode + "</small></div><div style='display:none'>" + ItemsData[i].BarCode.toUpperCase() + "</div><div style='display:none'>" + mapCode_.replace(" ", "") + "</div><div style='display:none'>" + profileData_.replace(" ", "") + "</div>";
                    li_ = li_ + "<small >" + ItemsData[i].Name + "</small>";
                    li_ = li_ + "<br/><small >" + ItemsData[i].Alias + "</small></div>";
                    li_ = li_ + "</a></li>";
                }

                var Code = ItemsData[i].Code.toUpperCase();
                var ItemId = ItemsData[i].ItemId;
                var Name = ItemsData[i].Name;
                var Price = ItemsData[i].Price;
                var Disc = ItemsData[i].Disc;
                var UOMCode = ItemsData[i].UOMCode;
                var UOMName = ItemsData[i].UOMName;
                var UOMID = ItemsData[i].UOMID;
                var BarCode = ItemsData[i].BarCode.toUpperCase();
                var StockData = ItemsData[i].StockData;
                var Item_Group_Code = ItemsData[i].Item_Group_Code;
                var Item_Group_Id = ItemsData[i].Item_Group_Id;
                var O1 = ItemsData[i].O1;
                var img_ = ItemsData[i].img;
                if (MainData != '')
                    MainData = MainData + Code + '~' + ItemId + '~' + Name + '~' + Price + '~' + Disc + '~' + UOMCode + '~' + UOMName + '~' + UOMID + '~' + BarCode + '~' + StockData + '~' + mapCode_ + '~' + profileData_ + '~' + img_ + '$';
                else
                    MainData = Code + '~' + ItemId + '~' + Name + '~' + Price + '~' + Disc + '~' + UOMCode + '~' + UOMName + '~' + UOMID + '~' + BarCode + '~' + StockData + '~' + mapCode_ + '~' + profileData_ + '~' + img_ + '$';
            }

            document.getElementById('tbl_Item').innerHTML = "<ul  id='myTable'>" + li_ +
                "</ul>";

            $('#tbl_Item tfoot li').remove();
            var ct_ = parseFloat(parseFloat(parseFloat(ItemsData.length / 25).toFixed(0)) + parseFloat(1));

            if (ct_ >= 5) {
                ct_ = 5;
            }
            if (ItemsData.length <= 25) {
                ct_ = 1;
            }


        },
        error: function (e) {

        }
    });

}

function openItemT() {

    window.localStorage.setItem('ctCheck', 1);
    $('#ModalUploadItemsPopup').modal();
}
function openItem() {
    window.localStorage.setItem('ctCheck', 1);
    if (window.localStorage.getItem('network') == "1") {
        $('#txtUOMitem').val('EAC/Each');
        $('#hdnUOMitemId').val('1009');
        var ItemScantxt = document.getElementById("scannerInput").value;
        $('#txtCounterCode').val(ItemScantxt);
        $('#txtCounterName').val(ItemScantxt);
        $('#txtBarcode').val(ItemScantxt);
        $("#txtPriceItem").val("");
        $('#ModalUploadItemsPopup').modal();
    } else {
        navigator.notification.alert("Please Check Network", "", "neo ecr", "Ok")
    }
}
function taxApplicabilty() {
    window.localStorage.setItem('ctCheck', 1);
    if ($('#ChckTaxAp').prop('checked') == true) {
        $('#ChckTaxOnMRPAp').prop('checked', true);
        $('#ChckTaxOnMRPAp').prop('disabled', false);
        $('#lblTaxValue').css('display', 'block');
        $('#txtTaxValue').css('display', 'block');
        $('#txtTaxValue').val(15);
    }
    else {
        $('#ChckTaxOnMRPAp').prop('checked', false);

        $('#ChckTaxOnMRPAp').prop('disabled', true);
        $('#lblTaxValue').css('display', 'none');
        $('#txtTaxValue').css('display', 'none');
        $('#txtTaxValue').val('');

    }
}
function IsNumericKey(e) {
    var specialKeys = new Array();
    specialKeys.push(8);
    var keyCode = e.which ? e.which : e.keyCode

    var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1 || keyCode == 46);
    if (ret == false) {
        return ret;
    }
    else {

    }

}
function IsNumeric(e) {
    window.localStorage.setItem('ctCheck', 1);
    var d_ = $('#txtPriceItem').val().split('.');

    if (d_.length == 2) {
        if (e.keyCode == 46) {
            return false;
        }
        if (d_[1].length == 4) {
            return false;
        }
    }
    if (d_.length == 3) {
        return false;
    }
    var specialKeys = new Array();
    specialKeys.push(8);
    var keyCode = e.which ? e.which : e.keyCode

    var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1 || keyCode == 46);
    if (ret == false) {
        return ret;
    }
    else {

    }
}


$("#txtUOMitem").autocomplete({
    source: function (request, response) {
        window.localStorage.setItem('ctCheck', 1);
        var HashValues = (window.localStorage.getItem("hashValues"));
        if (HashValues != null && HashValues != '') {
            var hashSplit = HashValues.split('~');
            SubOrgId = hashSplit[1];
        }
        var param = "VD=" + VD1() + "&searchkey=" + $('#txtUOMitem').val() + "";
        $.ajax({
            url: CashierLite.Settings.GetUOM,
            data: param,
            dataType: "json",
            type: "Get",

            success: function (data) {

                response($.map(data, function (item) {
                    return {
                        label: item.UOMCode + '/' + item.UOMName,
                        val: item.UOMID
                    }
                }))
            },
            error: function (response) {
            },
            failure: function (response) {
            }
        });

    },
    change: function (e, i) {
        if (i.item) {

        }
        else {
            $('#txtUOMitem').val('');
            $('#hdnUOMitemId').val('');

        }
    },
    select: function (e, i) {
        $('#hdnUOMitemId').val(i.item.val);
        window.localStorage.setItem('ctCheck', 1);

    },
    minLength: 1
});

function saveitem() {
    window.localStorage.setItem('ctCheck', 1);
    if ($("#txtCounterCode").prop("disabled") == false && $('#txtCounterCode').val() == "") {
        navigator.notification.alert($('#hdnEntrCode').val(), "", "neo ecr", "Ok");
        return false;
    }
    if ($('#txtCounterName').val() == "" && $('#txtCounterName').val().trim().length == 0) {
        navigator.notification.alert($('#hdnEntrName').val(), "", "neo ecr", "Ok");
        return false;
    }

    if ($('#txtUOMitem').val() == "" && $('#txtUOMitem').val().trim().length == 0) {
        navigator.notification.alert($('#hdnphUOMitem').val(), "", "neo ecr", "Ok");
        return false;
    }
    if ($('#txtBarcode').val() == "" && $('#txtBarcode').val().trim().length == 0) {
        navigator.notification.alert($('#hdnphbarcode').val(), "", "neo ecr", "Ok");
        return false;

    }
    if ($('#txtBarcode').val().length < 8) {
        navigator.notification.alert($('#hdnBarcodeMaxlen12').val(), "", "neo ecr", "Ok");
        return false;

    }
    if ($('#txtBarcode').val().length > 16) {
        navigator.notification.alert($('#hdnBarcodeMaxlen12').val(), "", "neo ecr", "Ok");
        return false;

    }
    if ($(ChckTaxAp).prop('checked') == true) {
        if ($('#txtTaxValue').val() == "" || $('#txtTaxValue').val().trim().length == 0 || parseFloat($('#txtTaxValue').val()) < 0) {
            navigator.notification.alert($('#hdnphTaxValue').val(), "", "neo ecr", "Ok");
            return false;
        }
    }
    if ($('#txtPriceItem').val() == "" || $('#txtPriceItem').val().trim().length == 0) {
        navigator.notification.alert($('#hdnphPriceItem').val(), "", "neo ecr", "Ok");
        return false;
    }

    ItemSavePP('');
    $('#txtItemScan').val('');
    $('#scannerInput').val('');
}

function ItemSavePP(upCheck) {
    window.localStorage.setItem('ctCheck', 1);
    Jsonobj = [];
    var item = {};
    if ($('#hdnTransId').val() == "") {
        item["TransId"] = "";
    }
    else {
        item["TransId"] = $('#hdnTransId').val();
    }
    item["SubOrgID"] = $('#hdnBranchId').val();
    item["CompanyId"] = $('#CompanyId').val();
    item["CounterNumber"] = $('#txtCounterCode').val();
    item["CounterName"] = $('#txtCounterName').val();
    item["UserId"] = $('#hdnUserId').val();
    item["OBS"] = $('#hdnOBS').val();
    item["LANGID"] = $('#hdnLanguage').val();
    item["LASTUPDATEDDATE"] = $('#hdnLastUpdatedDate').val();
    item["Status"] = "Y";

    if (upCheck != '') {
        if ($('#hdnScreenMode').val() == "Active")
            item["Status"] = "Y";
        else
            item["Status"] = "N";
    }
    item["ItemCode"] = $('#txtCounterCode').val();
    item["ItemName"] = $('#txtCounterName').val();
    item["UOMID"] = $('#hdnUOMitemId').val();
    item["BarCode"] = $('#txtBarcode').val();
    item["Desc"] = "";
    item["Alias"] = $('#txtAliasName').val();
    item["Price"] = "0"
    item["TAXAP"] = 'Y';
    item["TAXMRP"] = 'R';

    if ($('#ChckTaxOnMRPAp').prop('checked') == false) {
        item["TAXMRP"] = 'F';
    }
    if ($('#ChckTaxAp').prop('checked') == false) {
        item["TAXAP"] = 'N';
        item["TAXMRP"] = '';
        item["TaxAmt"] = "0";
    }
    else {
        item["TaxAmt"] = $('#txtTaxValue').val();
    }
    if ($('#txtPriceItem').val() != "") {
        item["Price"] = $('#txtPriceItem').val();
    }
    var vd_ = window.localStorage.getItem("UUID");
    item["img"] = "";
    item["VD"] = vd_;
    Jsonobj.push(item);

    var ItemsData = new Object();
    ItemsData.Data = JSON.stringify(Jsonobj);
    var ComplexObject = new Object();
    ComplexObject.ItemsData = ItemsData;
    var data = JSON.stringify(ComplexObject);

    $.ajax({
        type: 'POST',
        url: CashierLite.Settings.ItemSave,
        data: data,
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function (resp) {
            resp = resp.ItemSaveResult;

            var result = resp.split(',');
            if (result[0] == "100000") {

                navigator.notification.alert($('#txtCounterCode').val() + ' ' + $('#hdnRecSaveSucc').val(), "", "neo ecr", "Ok");

                clearControls();
                $('#ModalUploadItemsPopup').modal('hide');


                GetStockCount();
                GetItemDetails();
            }
            if (result[0] == "100001") {
                navigator.notification.alert($('#txtCounterCode').val() + ' ' + $('#hdnICodeAlreadyExist').val(), "", "neo ecr", "Ok");
                return false;
            }
            if (result[0] == "100005") {
                navigator.notification.alert($('#txtBarcode').val() + ' ' + $('#hdnbarCodeAlreadyExist').val(), "", "neo ecr", "Ok");
                return false;
            }
            if (result[0] == "1000020") {
                navigator.notification.alert($('#hdnInvalData').val(), "", "neo ecr", "Ok");
                return false;
            }
        },
        error: function (e) {
            navigator.notification.alert($('#hdnInvalData').val(), "", "neo ecr", "Ok");
            return false;
        }
    });
}

function clearControls() {
    window.localStorage.setItem('ctCheck', 1);
    $('#txtCounterCode').val("");
    $('#txtCounterName').val("");
    $("#txtDescription").val("");
    $('#txtBarcode').val("");
    $('#hdnTransId').val("");
    $('#hdnLastUpdatedDate').val("");
    $("#txtUOMitem").val("");
    $("#txtAliasName").val("");
    $("#txtPriceItem").val("");
    $("#txtTaxValue").val(15);
    $('#ChckTaxAp').prop('checked', true);
    $('#ChckTaxOnMRPAp').prop('checked', true);
    $('#ChckTaxOnMRPAp').prop('disabled', false);
    $('#lblTaxValue').css('display', 'block');
    $('#txtTaxValue').css('display', 'block');
    closeitem();
}

function closeitem() {
    window.localStorage.setItem('ctCheck', 1);
    $('#txtItemScan').val('');
    $('#scannerInput').val('');
    // $('#txtItemScan').focus();
    $('#ModalUploadimagePopup').modal('hide');
}

//#enditempopup
function showCuspop() {
    window.localStorage.setItem('ctCheck', 1);
    jQuery("label[for='lblCustomerEnroll'").html("Customer Enroll");
    popupSts = 1;
    clearPOPUP();
    $('#ModalHomeDelivery').modal('show');
}

var txt = "";


// $(function () {
//     $(document).pos();
//     $(document).on('scan.pos.barcode', function (event) {
//         $("#txtItemScan").blur();
//         if (txt == '') {
//             if ((event.code.length > 5) && (event.code.length < 17)) {
//                 $("#txtItemScan").val(event.code);
//                 Itemdetect();
//             }
//             else {
//                 txt = '';
//                 navigator.notification.alert("Invalid Barcode", "", "neo ecr", "Ok")
//             }
//         }
//         //access `event.code` - barcode data
//     });
//     $(document).on('swipe.pos.card', function (event) {
//     });
// });
function RoundInvAmount(amount) {
    window.localStorage.setItem('ctCheck', 1);
    let _amount = parseFloat(amount).toFixed(4);

    var _number1 = Number((_amount).split('.')[0]);

    let _number2 = (_amount).split('.')[1];

    var _finalValue = '';

    let _firstDigit = Number(parseFloat(_amount).toFixed(4).split('.')[1]).toString().substr(0, 2).charAt(0);

    let _secondDigit = ((_amount).split('.')[1]).toString().substr(0, 2).charAt(1);

    if (((_amount).split('.')[1]).toString().substr(0, 2).charAt(0) != '0') {

        if ((parseFloat(_secondDigit) <= 5)) {
            if (parseFloat(_secondDigit) <= 4) {
                _secondDigit = '0';
                _number2 = _firstDigit + '' + _secondDigit;
            }

            if (parseFloat(_secondDigit) == 5) {
                _secondDigit = '5';
                _number2 = _firstDigit + '' + _secondDigit;
            }

        }

        else

            if (parseFloat(_secondDigit) > 5) {
                if (parseFloat(_firstDigit) < 9) {
                    _firstDigit = parseFloat(_firstDigit) + 1;
                    _secondDigit = '0';
                    _number2 = _firstDigit + '' + _secondDigit;

                }

                else {
                    _number1 = parseFloat(_number1) + 1;
                    _number2 = '00';

                }
            }

    }

    else {

        if ((parseFloat(_secondDigit) <= 5)) {
            if (parseFloat(_secondDigit) <= 4) {
                _secondDigit = '0';
                _number2 = '0' + '' + _secondDigit;
            }

            if (parseFloat(_secondDigit) == 5) {
                _secondDigit = '5';
                _number2 = '0' + '' + _secondDigit;
            }

        }

        else

            if (parseFloat(_secondDigit) > 5) {

                _number2 = '10';
            }
    }

    _finalValue = _number1 + '.' + _number2;
    return parseFloat(_finalValue).toFixed(2);
}
function Itemdetect() {
    window.localStorage.setItem('ctCheck', 1);
    var ItemCode = $('#scannerInput').val().toUpperCase();
    var dup_ = "0";
    var strOptions = $('#hdn_CusId').val();
    if (strOptions == '') {
        strOptions = GenarateGustId() + '-01';
        strOptions1 = GenarateGustName();
        $('#hdn_CusId').val(strOptions);
    }
    $("#txtItemScan").val(ItemCode);
    if (strOptions != '') {
        if ($("#scannerInput").val().trim() != '') {
            var Userid = '';
            var HashValues = (window.localStorage.getItem("hashValues"));
            if (HashValues != null && HashValues != '') {
                var hashSplit = HashValues.split('~');
                getResources1(hashSplit[7], "POSInvoice");
                SubOrgId = hashSplit[1] + ',' + hashSplit[16];
                Userid = hashSplit[14];
            }
            if (window.localStorage.getItem('network') == '1') {
                $.ajax({
                    url: CashierLite.Settings.getItemDataFromBarcode,
                    data: "VD=" + VD1() + "&SubOrgId=" + SubOrgId + "&Barcode=" + $("#scannerInput").val().trim() + "&Userid=" + Userid,
                    success: function (resp) {
                        $("#txtItemScan").val('');
                        if (resp.length > 0) {
                            var mapCode_ = "";
                            var profileData_ = "";
                            mapCode_ = resp[0].O2.replace('$', '&');
                            if (mapCode_ != '')
                                for (var k = 0; k < resp[0].profileData.length; k++) {
                                    var cols = resp[0].profileData[k].split('~');
                                    profileData_ = profileData_ + "@" + cols[0] + "#" + cols[1] + "#" + cols[2] + "#" + cols[3] + "#" + cols[4] + "#" + cols[5] + "#" + cols[6] + "#" + cols[8] + "#" + cols[9] + "#" + cols[10] + "#" + cols[11] + "#" + cols[12] + "#" + cols[13] + "#" + cols[14] + "#" + cols[15] + "#" + cols[16] + "#" + cols[17];
                                }
                            onSubData(resp[0].Code, resp[0].ItemId, resp[0].Name, resp[0].Price, resp[0].Disc, resp[0].UOMID, resp[0].UOMCode, mapCode_, profileData_, resp[0].img);
                            $('#scannerInput')[0].style = "";
                        }
                        else {
                            navigator.notification.beep(1);
                            if (confirm($('#hdnBarCodNtFound').val())) {

                                $('#txtUOMitem').val('EAC/Each');
                                $('#hdnUOMitemId').val('1009');
                                var ItemScantxt = document.getElementById("scannerInput").value;
                                $('#txtCounterCode').val(ItemScantxt);
                                $('#txtCounterName').val(ItemScantxt);
                                $('#txtBarcode').val(ItemScantxt);
                                $("#txtPriceItem").val("");
                                $('#scannerInput').val('');
                                txt = '';
                                openItemT();
                            } else {
                                txt = '';
                                $('#scannerInput').val('');
                                //   $('#scannerInput').focus();  
                            }
                        }
                    }
                    , error: function (e) {
                        $('#scannerInput').val('');
                        txt = '';
                    }
                });
            }
            else {

                try {
                    var db = openDatabase('myItemMaster', '1.0', 'CashierLiteDB', 2 * 1024 * 1024);
                    db.transaction(function (tx) {
                        tx.executeSql('SELECT * FROM items where BarCode="' + $("#scannerInput").val().trim() + '"', [], function (tx, results) {
                            var resp = results.rows;
                            if (resp.length > 0) {
                                var mapCode_ = "";
                                var profileData_ = "";
                                mapCode_ = resp[0].O2.replace('$', '&');
                                if (mapCode_ != '')
                                    if (resp[0].profileData.length > 0) {
                                        var cols = resp[0].profileData.split('~');
                                        profileData_ = profileData_ + "@" + cols[0] + "#" + cols[1] + "#" + cols[2] + "#" + cols[3] + "#" + cols[4] + "#" + cols[5] + "#" + cols[6] + "#" + cols[8] + "#" + cols[9] + "#" + cols[10] + "#" + cols[11] + "#" + cols[12] + "#" + cols[13] + "#" + cols[14] + "#" + cols[15] + "#" + cols[16] + "#" + cols[17];
                                    }
                                onSubData(resp[0].Code, resp[0].ItemId, resp[0].Name, resp[0].Price, resp[0].Disc, resp[0].UOMID, resp[0].UOMCode, mapCode_, profileData_, resp[0].img);
                                $('#scannerInput')[0].style = "";
                                txt = '';
                            }
                            else {
                                $("#txtItemScan").val('');
                                $("#scannerInput").val('');
                                txt = '';
                                $('#scannerInput')[0].style = "background-color:red;color:white";

                            }
                        });
                    });
                }
                catch (a) {
                    txt = '';
                }

            }
            if (dup_ == "0") {
            }
            else {
                $('#scannerInput').val(''); txt = '';
                $("#txtItemScan").val('');
            }
        }
        else {
            $('#scannerInput')[0].style = ""; txt = '';
            $("#txtItemScan").val('');
        }

    }
    else {
        txt = '';
        navigator.notification.alert($('#hdnSelCust').val(), "", "neo ecr", "Ok");
    }

}
function printLocal() {
    //
    window.localStorage.setItem('Inv', '0');
    window.localStorage.setItem('ctCheck', 1);
    if ($('#hdnISPRINT').val() == "true") {

        $('#myModalprint').modal();

    } else {

        printInternalUSB($('#hdnInvoiceId').val());
        $('#myModalprint').css('display', "none");
    }

    //GetItemDetails();
}

function printServer() {
    var hashSplit;
    window.localStorage.setItem('ctCheck', 1);
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        hashSplit = HashValues.split('~');
    }
    window.localStorage.setItem('Inv', '0');
    $('#myModalprint').css('display', "none");
    var divContents = document.getElementById("div_invoice_print").innerHTML;
    if ($('#hdnISPRINT').val() == "true") {
        $('#myModalprint').modal();
    } else {
        $('#myModalprint').css('display', "none");
    }
    var param = {
        invoiceId: window.localStorage.getItem('InvoiceId'), connectionString: window.localStorage.getItem('Connection'), printerName: hashSplit[23]
    };
    $.ajax({
        type: 'GET',
        //url: CashierLite.Settings.DirectPrint,
        url: hashSplit[24] + "/DirectPrint",
        data: param,
        dataType: 'JSONP',
        crossDomain: true,
        contentType: "application/json; charset=utf-8",
        success: function () {
            if ($('#hdnISPRINT').val() == "true") {
                $('#myModalprint').modal();
                //$('#myModalprint').css('display', "block");

            } else {
                $('#myModalprint').css('display', "none");
            }
            return false;
        },
        error: function (e) {
        }
    });
}

function printDiv() {
    window.localStorage.setItem('ctCheck', 1);
    var hashSplit;
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        hashSplit = HashValues.split('~');
    }

    printLocal();

}
function printDivA1() {
    window.localStorage.setItem('ctCheck', 1);
    printInternalUSB($('#hdnInvoiceId').val());
    $('#myModalprint').css('display', "none");

}
function printInternalUSBTEMP(invoiceId) {

    window.localStorage.setItem('ctCheck', 1);
    try {

        var dataMain = 'dfd fsdf sd fsd fsdf sdf sdfsdfdsf fds fdfsdfsdfsdfsdfsdf d fsdfsdf sd fsdfsdf df df sdfsd';
        navigator.notification.beep(1);
        var printName_ = '';
        BTPrinter.connect(function (data) {

        }, function (err) {

        }, "InnerPrinter")

        BTPrinter.printPOSCommand(function (data) {

        }, function (err) {

        }, "0A");

    } catch (a) {

    }

} function chr(i) {
    return String.fromCharCode(i);
}

function BufferToBase64(buf) {
    var binstr = Array.prototype.map.call(buf, function (ch) {
        return String.fromCharCode(ch);
    }).join('');
    return btoa(binstr);
}

var dataMain; var invoiceIdG;

function printInternalUSB(invoiceId) {
    window.localStorage.setItem('ctCheck', 1);
    var PrinterName_ = "InnerPrinter";
    if (window.localStorage.getItem("PrinterName") != null) {
        PrinterName_ = window.localStorage.getItem("PrinterName");
    }
//T2s
    // BTPrinter.connect(function (data) {
    //     //alert('Printer Conneted') 
    // }, function (err) {
    // }, PrinterName_); 
    //Small Papper
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.printUSB,
        data: "InvoiceId=" + invoiceId + "&VD=" + VD1(),
        success: function (resp) {
            try {
                dataMain = resp;
                invoiceIdG = invoiceId;
                navigator.notification.beep(1);

                var qrData = dataMain.split('Š')[1];
                dataMain = dataMain.split('Š')[0];
                //T2s
                // if (PrinterName_ != "InnerPrinter") {
                //     BTPrinter.printText(function (data) {

                //     }, function (err) {

                //     }, dataMain);
                //     dataTT(qrData);
                //     //printQrCode(invoiceId);
                //     BTPrinter.printPOSCommand(function (data) {
                //     }, function (err) {
                //     }, "0C");
                //     BTPrinter.printPOSCommand(function (data) {
                //     }, function (err) {
                //     }, "0A");
                // }

                // if (PrinterName_ == "InnerPrinter") {
                    // navigator.sunmiInnerPrinter.setAlignment(0);
                    navigator.sunmiInnerPrinter.printerInit();
                    navigator.sunmiInnerPrinter.printerStatusStartListener();
                    navigator.sunmiInnerPrinter.printOriginalText(dataMain); 
                    //printQrCode(invoiceId);
                    navigator.sunmiInnerPrinter.setAlignment(1);
                    navigator.sunmiInnerPrinter.printQRCode(qrData,10,10); 
                    navigator.sunmiInnerPrinter.hasPrinter();
                    navigator.sunmiInnerPrinter.printerStatusStopListener();
                    window.localStorage.setItem('PrintCut','1');
                    cashDrawOpen();
                // }
                //T2s
                // BTPrinter.disconnect(function (data) {
                // }, function (err) {
                // }, PrinterName_);
                $('#myModalpayFinal').modal('show');
            } catch (a) {

            }
        }, error: function (e) {

        }
    });
} 
PaperCutCheck();
function PaperCutCheck() {
        try
        {
            if(window.localStorage.getItem('PrintCut')=='1')
            {
                navigator.sunmiInnerPrinter.printOriginalText("                      "); 
                navigator.sunmiInnerPrinter.printOriginalText("                      "); 
                navigator.sunmiInnerPrinter.printOriginalText("                      "); 
                window.localStorage.setItem('PrintCut','0');
                navigator.sunmiInnerPrinter.sendRAWData("HVZCAA=="); 
            }
        }
        catch(a)
        {

        }
        setTimeout('PaperCutCheck()', 1000);
}
function cashDrawOpen()
{
    try {
        if (window.localStorage.getItem('openCashDrawer') == '1') {
            try{
                var cutpaper = [0x10,0x14,0x00,0x00,0x00];
                var cutpaperUint8 = new Uint8Array(cutpaper);
                var cutpaperBase64 = BufferToBase64(cutpaperUint8); 
                navigator.sunmiInnerPrinter.sendRAWData(cutpaperBase64); 
            }
            catch(a)
            {

            }
        }
    }
    catch (aa) {

    }
}
function dataTT(invData) {
    BTPrinter.printBase64(function (data) {
    }, function (err) {
    }, invData, '1');//base64 string, align

}
function printQrCodeTT(invoiceId) {
    const justify_center = '\x1B\x61\x01';
    const justify_left = '\x1B\x61\x00';
    const qr_model = '\x32';          // 31 or 32
    const qr_size = '\x08';          // size
    const qr_eclevel = '\x33';          // error correction level (30, 31, 32, 33 - higher)
    const qr_data = invoiceId;
    const qr_pL = String.fromCharCode((qr_data.length + 3) % 256);
    const qr_pH = String.fromCharCode((qr_data.length + 3) / 256);

    BTPrinter.printText(data => {
        this.msg = '';
    }, err => {
        this.msg = '';
    }, "test datat Print " + justify_center +
    '\x1D\x28\x6B\x04\x00\x31\x41' + qr_model + '\x00' +        // Select the model
    '\x1D\x28\x6B\x03\x00\x31\x43' + qr_size +                  // Size of the model
    '\x1D\x28\x6B\x03\x00\x31\x45' + qr_eclevel +               // Set n for error correction
    '\x1D\x28\x6B' + qr_pL + qr_pH + '\x31\x50\x30' + qr_data + // Store data 
    '\x1D\x28\x6B\x03\x00\x31\x51\x30' +                        // Print
    '\n\n\n' +
        justify_left);
}
function printQrCode(invoiceId) {
    const justify_center = '\x1B\x61\x01';
    const justify_left = '\x1B\x61\x00';
    const qr_model = '\x32';          // 31 or 32
    const qr_size = '\x08';          // size
    const qr_eclevel = '\x33';          // error correction level (30, 31, 32, 33 - higher)
    const qr_data = invoiceId;
    const qr_pL = String.fromCharCode((qr_data.length + 3) % 256);
    const qr_pH = String.fromCharCode((qr_data.length + 3) / 256);

    BTPrinter.printText(data => {
        this.msg = '';
    }, err => {
        this.msg = '';
    }, justify_center +
    '\x1D\x28\x6B\x04\x00\x31\x41' + qr_model + '\x00' +        // Select the model
    '\x1D\x28\x6B\x03\x00\x31\x43' + qr_size +                  // Size of the model
    '\x1D\x28\x6B\x03\x00\x31\x45' + qr_eclevel +               // Set n for error correction
    '\x1D\x28\x6B' + qr_pL + qr_pH + '\x31\x50\x30' + qr_data + // Store data 
    '\x1D\x28\x6B\x03\x00\x31\x51\x30' +                        // Print
    '\n\n\n' +
        justify_left);
}
function PrintLoad() {
    var SubOrg_Id;
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        SubOrg_Id = hashSplit[1];
    }
    window.localStorage.setItem('ctCheck', 1);

    var param = {
        SubOrgId: SubOrg_Id, VD: VD1()
    };
    if (window.localStorage.getItem('network') == '1') {
        $.ajax({
            url: CashierLite.Settings.GetPrintCustomizeStyles,
            data: param,
            type: "GET",
            success: function (data) {
                try {
                    $('#tblheader_1').html("");
                    $('#divfooter').html("");
                    $('#tblheader_1').append('<div style="text-align:' + data[0].HeaderFontAlignment + ';font-family:' + data[0].Headerfont + '; font-style:' + data[0].HeaderFontStyle + ';font-size:' + data[0].HeaderFontSize + 'px">' + data[0].HeaderText + '</div>');
                    $('#tblheader_1').append('<div style="text-align:' + data[0].HeaderFontAlignment + ';font-family:' + data[0].Headerfont + '; font-style:' + data[0].HeaderFontStyle + ';font-size:' + data[0].HeaderFontSize + 'px">' + data[0].HeaderInfo + '</div>');
                    $('#divfooter').append('<div style="text-align:' + data[0].FooterFontAlignment + ';font-family:' + data[0].FooterFont + '; font-style:' + data[0].FooterFontStyle + ';font-size:' + data[0].FooterFontSize + 'px">' + data[0].FooterText + '</div>');
                    $('#tblheader_2').css("text-align", data[0].BodyAlignment);
                    $('#tblheader_3').css("text-align", data[0].BodyAlignment);
                    $('#tblheader_5').css("text-align", data[0].BodyAlignment);
                    $('#hdnISPRINT').val(data[0].IsPrint);
                }
                catch (a) {

                }
                // $('#txtItemScan').focus();
            },
        });
    }
    else {
        try {

            var data = window.localStorage.getItem('OfflinePrintLoad');
            data = JSON.parse(data)[0];
            var v = [];
            v = data.HeaderInfo.split('  ').join('\n').split('\n');
            $('#tblheader_1').html("");
            $('#divfooter').html("");
            $('#tblheader_1').append('<div style="text-align:' + data.HeaderFontAlignment + ';font-family:' + data.Headerfont + '; font-style:' + data.HeaderFontStyle + ';font-size:' + data.HeaderFontSize + 'px">' + data.HeaderText + '</div>');
            for (var i = 0; i < v.length; i++) {
                $('#tblheader_1').append('<div style="text-align:' + data.HeaderFontAlignment + ';font-family:' + data.Headerfont + '; font-style:' + data.HeaderFontStyle + ';font-size:' + data.HeaderFontSize + 'px">' + v[i] + '</div>');
            }
            $('#divfooter').append('<div style="text-align:' + data.FooterFontAlignment + ';font-family:' + data.FooterFont + '; font-style:' + data.FooterFontStyle + ';font-size:' + data.FooterFontSize + 'px">' + data.FooterText + '</div>');
            $('#tblheader_2').css("text-align", data.BodyAlignment);
            $('#tblheader_3').css("text-align", data.BodyAlignment);
            $('#tblheader_5').css("text-align", data.BodyAlignment);
            $('#hdnISPRINT').val(data.IsPrint);
        }
        catch (a) {

        }
        //$('#txtItemScan').focus();
    }
}
function Sampleprint() {
    tets();
    $('#myModalprint').show();
}


function DashBoardDataFill() {
    var SubOrg_Id;
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        SubOrg_Id = hashSplit[1];
    }
    window.localStorage.setItem('ctCheck', 1);
    var param = {
        SubOrgId: SubOrg_Id, CounterId: window.localStorage.getItem('COUNTERID'), ShiftId: window.localStorage.getItem('SHIFTID'), SalespersonId: window.localStorage.getItem('CashierID'),
        roleId: window.localStorage.getItem('RoleId'), VD: VD1()
    };
    $.ajax({
        url: CashierLite.Settings.GetDashboardCounts,
        data: param,
        type: "GET",
        success: function (data) {

            var d_ = data.split(',');
            $('#lblsalesCTDash').text(d_[0]);
            $('#lblsalesCUSDash').text(d_[1]);
            $('#lblsalesITEMDash').text(d_[2]);
            $('#lblTodayCustomers').text(d_[3]);
        },
    });
}


function loadWeekGraphData() {
    window.localStorage.setItem('ctCheck', 1);
    var subOrgId = '';
    var HashValues = (window.localStorage.getItem("hashValues"));
    if (HashValues != null && HashValues != '') {
        var hashSplit = HashValues.split('~');
        subOrgId = hashSplit[1];
    }


    Chart.plugins.register({
        afterDatasetsDraw: function (chartInstance, easing) {
            // To only draw at the end of animation, check for easing === 1
            var ctx = chartInstance.chart.ctx;

            chartInstance.data.datasets.forEach(function (dataset, i) {
                var meta = chartInstance.getDatasetMeta(i);
                if (!meta.hidden) {
                    meta.data.forEach(function (element, index) {
                        // Draw the text in black, with the specified font
                        ctx.fillStyle = 'rgb(0, 0, 0)';

                        var fontSize = 16;
                        var fontStyle = 'normal';
                        var fontFamily = 'Helvetica Neue';
                        ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

                        // Just naively convert to string for now
                        var dataString = dataset.data[index].toString();

                        // Make sure alignment settings are correct
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'middle';

                        var padding = 5;
                        var position = element.tooltipPosition();
                        ctx.fillText(dataString, position.x, position.y - (fontSize / 2) - padding);
                    });
                }
            });
        }
    });
    //var MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var color = Chart.helpers.color;
    var barChartData = '';

    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetDashBoardWeekSalesByCounter,
        data: "CounterId=" + window.localStorage.getItem('CashierID') + "&VD=" + VD1(),
        success: function (resp) {

            var x = []; var y = [];


            for (var i = 0; i < resp.length; i++) {

                var s_ = resp[i].Day.split(' ');
                x.push(s_[0]);


                y.push(parseFloat(resp[i].SalesCount).toFixed(2));
            }

            barChartData = {
                labels: x,
                datasets: [{
                    label: 'Sale',
                    backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
                    borderColor: window.chartColors.blue,
                    borderWidth: 1,
                    data: y
                }, {
                    type: 'line',
                    label: 'Line View',
                    borderColor: window.chartColors.red,
                    borderWidth: 2,
                    fill: false,
                    data: y,

                }
                ]
            }



            var ctx = document.getElementById("canvasWeek").getContext("2d");
            window.myBar = new Chart(ctx, {
                type: 'bar',
                data: barChartData,
                options: {
                    responsive: true,
                    title: {
                        display: true,
                        text: ''
                    },
                }
            });
        }
    });


    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetSp_POSMargin,
        data: "SubOrgId=" + subOrgId + "&VD=" + VD1(),
        success: function (resp) {
            var x = resp.split(',');

            $('#lblSales_M').text(parseFloat(x[0]).toFixed(2));
            $('#lblRefunds_M').text(parseFloat(x[2]).toFixed(2));
            $('#lblRevenue_M').text(parseFloat(x[3]).toFixed(2));
            $('#lblCost_M').text(parseFloat(x[1]).toFixed(2));
            $('#lblProfit_M').text(parseFloat(x[4]).toFixed(2));

            var ProfitPe_ = parseFloat(x[4]).toFixed(2) / parseFloat(x[1]).toFixed(2);
            //ProfitPe_ != 'NaN' ? $('#lblProfitPercent_M').text('0.00'): $('#lblProfitPercent_M').text(parseFloat(parseFloat(ProfitPe_) * 100).toFixed(4)) ;
            if (String(ProfitPe_) == "NaN" || String(ProfitPe_) == "Infinity") {
                $('#lblProfitPercent_M').text('0.00')
            } else {
                $('#lblProfitPercent_M').text(parseFloat(parseFloat(ProfitPe_) * 100).toFixed(2))
            }

        }
    });

    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetDashBoardTenderWiseSales,
        data: "subOrgId=" + subOrgId + "&VD=" + VD1(),
        success: function (resp) {
            var data = [];
            var subData = { TenderType: "", TenderTypeValue: "" };
            var P21 = {
            }
            for (var i = 0; i < resp.length; i++) {
                P21 = {
                }
                P21["TenderType"] = resp[i].ITEMNAME;
                P21["TenderTypeValue"] = resp[i].AMOUNT;
                data.push(P21);
            }
            AmCharts.makeChart("TodayTenderSale", {
                "type": "pie",
                "dataProvider": data,
                "pullOutDuration": 3,
                "gradientRatio": [-0.4, -0.4, -0.4, -0.4, -0.4, -0.4, 0, 0.1, 0.2, 0.1, 0, -0.2, -0.5],
                "innerRadius": "60%",
                "pullOutRadius": "5",
                "depth3D": 0,
                "titleField": "TenderType",
                "valueField": "TenderTypeValue",
                "labelText": "[[title]]:[[TenderTypeValue]]",

            });


        }
    });

}

function loadMonthGraphData() {

    window.localStorage.setItem('ctCheck', 1);
    Chart.plugins.register({
        afterDatasetsDraw: function (chartInstance, easing) {
            // To only draw at the end of animation, check for easing === 1
            var ctx = chartInstance.chart.ctx;

            chartInstance.data.datasets.forEach(function (dataset, i) {
                var meta = chartInstance.getDatasetMeta(i);
                if (!meta.hidden) {
                    meta.data.forEach(function (element, index) {
                        // Draw the text in black, with the specified font
                        ctx.fillStyle = 'rgb(0, 0, 0)';

                        var fontSize = 16;
                        var fontStyle = 'normal';
                        var fontFamily = 'Helvetica Neue';
                        ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

                        // Just naively convert to string for now
                        var dataString = dataset.data[index].toString();

                        // Make sure alignment settings are correct
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'middle';

                        var padding = 5;
                        var position = element.tooltipPosition();
                        ctx.fillText(dataString, position.x, position.y - (fontSize / 2) - padding);
                    });
                }
            });
        }
    });
    var color = Chart.helpers.color;
    var barChartData = '';
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetDashBoardMonthSalesByCounter,
        data: "CounterId=" + window.localStorage.getItem('CashierID') + "&VD=" + VD1(),
        success: function (resp) {

            var x = []; var y = [];
            for (var i = 0; i < resp.length; i++) {
                var s_ = resp[i].Day.split(' ');
                x.push(s_[0]);
                y.push(parseFloat(resp[i].SalesCount).toFixed(2));
            }

            barChartData = {
                type: 'bar',
                labels: x,
                datasets: [{
                    label: 'Sale',
                    backgroundColor: color(window.chartColors.blue).alpha(0.6).rgbString(),
                    borderColor: window.chartColors.blue,
                    borderWidth: 1,
                    data: y
                }
                    ,
                {
                    type: 'line',
                    label: 'Line View',
                    borderColor: window.chartColors.red,
                    borderWidth: 2,
                    fill: false,
                    data: y,

                }
                ]
            }



            var ctx = document.getElementById("canvasMonth").getContext("2d");
            window.myBar = new Chart(ctx, {
                type: 'bar',
                data: barChartData,
                options: {
                    responsive: true,
                    title: {
                        display: true,
                        text: ''
                    },
                }
            });
        }
    });



}

function loadYearGraphData() {

    window.localStorage.setItem('ctCheck', 1);
    Chart.plugins.register({
        afterDatasetsDraw: function (chartInstance, easing) {
            var ctx = chartInstance.chart.ctx;

            chartInstance.data.datasets.forEach(function (dataset, i) {
                var meta = chartInstance.getDatasetMeta(i);
                if (!meta.hidden) {
                    meta.data.forEach(function (element, index) {
                        // Draw the text in black, with the specified font
                        ctx.fillStyle = 'rgb(0, 0, 0)';

                        var fontSize = 16;
                        var fontStyle = 'normal';
                        var fontFamily = 'Helvetica Neue';
                        ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

                        // Just naively convert to string for now
                        var dataString = dataset.data[index].toString();

                        // Make sure alignment settings are correct
                        ctx.textAlign = 'center';
                        ctx.textBaseline = 'middle';

                        var padding = 5;
                        var position = element.tooltipPosition();
                        ctx.fillText(dataString, position.x, position.y - (fontSize / 2) - padding);
                    });
                }
            });
        }
    });
    //var MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var color = Chart.helpers.color;
    var barChartData = '';
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetDashBoardYearSalesByCounter,
        data: "CounterId=" + window.localStorage.getItem('CashierID') + "&VD=" + VD1(),
        success: function (resp) {

            var x = []; var y = [];


            for (var i = 0; i < resp.length; i++) {

                var s_ = resp[i].Day;
                x.push(s_);


                y.push(parseFloat(resp[i].SalesCount).toFixed(2));
            }

            barChartData = {
                labels: x,
                datasets: [{
                    label: 'Sale',
                    backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
                    borderColor: window.chartColors.blue,
                    borderWidth: 1,
                    data: y
                }, {
                    type: 'line',
                    label: 'Line View',
                    borderColor: window.chartColors.red,
                    borderWidth: 2,
                    fill: false,
                    data: y,

                }
                ]
            }



            var ctx = document.getElementById("canvasYear").getContext("2d");
            window.myBar = new Chart(ctx, {
                type: 'bar',
                data: barChartData,
                options: {
                    responsive: true,
                    title: {
                        display: true,
                        text: ''
                    },
                }
            });
        }
    });



}

// $("#txtHomeDelPhNo").keypress(function (event) {
//     if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {

//         if ((event.which != 46 || $(this).val().indexOf('.') != -1)) {
//             return false;
//         }
//         event.preventDefault();
//     }
//     if (this.value.indexOf(".") > -1 && (this.value.split('.')[1].length > 1)) {
//         return false;
//         event.preventDefault()
//     }
// });

$("#txtDiscount").keypress(function (event) {

    if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {

        if ((event.which != 46 || $(this).val().indexOf('.') != -1)) {
            return false;
        }
        event.preventDefault();
    }
    if (this.value.indexOf(".") > -1 && (this.value.split('.')[1].length > 1)) {
        return false;
        event.preventDefault()
    }
});

function getResources1(langCode, pageCode) {
    window.localStorage.setItem('ctCheck', 1);
    if (window.localStorage.getItem('network') == '1') {
        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.getResource,
            async: false,
            data: "langCode=" + langCode + "&pageCode=" + pageCode,
            success: function (resp) {

                setLabels1(resp);
            }
        });
    }
    else {
        var resp;
        try {
            var open = indexedDB.open("InvoiceResDB", 1);
            open.onupgradeneeded = function () {
                db = open.result;
                var store = db.createObjectStore("Resources", {
                    keyPath: "id"
                });

            };
            open.onsuccess = function (e) {
                db = open.result;
                var tx = db.transaction("Resources", "readonly");
                //var tx = db.transaction("MyObjectStore", "readwrite");
                var store = tx.objectStore("Resources");
                var request = store.get("00-01");
                request.onerror = function (event) {

                };
                request.onsuccess = function (event) {
                    if (request.result) {
                        try {
                            resp = request.result;

                            setLabels1(resp.resources);

                        }
                        catch (a) {
                        }

                    } else {

                    }
                };

            }

        }
        catch (a) {


        }

    }
}

var homeDeliveryAddress = '';


$('#txtHomeDelName').val('');
$('#txtHomeDelAddr1').val('');
$('#txtHomeDelAddr2').val('');
$('#txtHomeDelCity').val('');
$('#txtHomeDelState').val('');
$('#txtHomeDelCountry').val('');
$('#txtHomeDelZipCode').val('');
$('#txtHomeDelPhNo').val('');
$('#txtHomeDelMobileNo').val('');
$('#txtHomeDelEmail').val('');




function saveHomeAddressPOP(sts) {
    window.localStorage.setItem('ctCheck', 1);
    if ($('#txtHomeDelPhNo').val() == '') {
        navigator.notification.alert($('#hdnPlsContactNo').val(), "", "neo ecr", "Ok"); 
        return false;
    }
    if ($('#txtHomeDelName').val() == '') {
        navigator.notification.alert($('#hdnMsgCounterName').val(), "", "neo ecr", "Ok"); 
         return false;
    } 
     insertCustomer();
}

function cancelHomeDelivery() {
    window.localStorage.setItem('ctCheck', 1);
    $('#chkHomeDelivery').prop('checked', false);
    $('#txtHomeDelName').val('');
    $('#txtHomeDelAddr').val('');
    $('#txtHomeDelZipCode').val('');
    $('#txtHomeDelPhNo').val('');
    $('#txtHomeDelEmail').val('');
    $('#txtOpeningBal').val("0.00");
}

var address;
$('#btnCheckOK').prop('disabled', false);
function GetStateCountryByCity1(CityId) {
    CityData = [];
    CityData = JSON.parse((window.localStorage.getItem("FillCountryByCity")));
    for (var i = 0; i < CityData.length; i++) {
        if (CityData[i].CityId == CityId) {
            address = address + "," + CityData[i].CityName + "," + CityData[i].RegionName + "," + CityData[i].CountryName + ",";
        }
    }
    $('#txtHomeDelAddr').val(address.slice(0, -1));
    $('#ModalHomeDelivery').modal('show');
}
function HomeDelivery(type) {
    window.localStorage.setItem('ctCheck', 1);
    var checkedStatus = '0';

    if (type == '1') {

        if ($('#chkHomeDelivery').prop('checked') == true) {
            $('#chkHomeDelivery').prop('checked', false);
        }
        else {
            $('#chkHomeDelivery').prop('checked', true);
            checkedStatus = '1';
        }
    }
    else {

        if ($('#chkHomeDelivery').prop('checked') == true) {
            checkedStatus = '1';
        }
    }

    if (checkedStatus == '1') {
        //s jQuery("label[for='lblHomeDelHeader'").html("Home Delivery");
        if ($('#hdn_CusId').val() != "" && $('#txtCusName').val().substring(0, 2) != 'G#') {
            $.ajax({
                type: 'GET',
                url: CashierLite.Settings.GetCustomerMstDoubleClick,
                data: "CustId=" + $('#hdn_CusId').val() + "&VD=" + VD1(),
                success: function (resp) {
                    var GridData = JSON.parse(resp);
                    $('#txtHomeDelName').val(GridData.QATABLE038[0].CUSTOMER_NAME);
                    address = GridData.QATABLE040[0].ADDRESS1 + ',' + GridData.QATABLE040[0].ADDRESS2;

                    $('#txtHomeDelZipCode').val(GridData.QATABLE040[0].PINCODE);
                    $('#txtHomeDelPhNo').val(GridData.QATABLE040[0].MOBILE_NO);
                    $('#txtHomeDelEmail').val(GridData.QATABLE040[0].EMAIL);
                    GatCityData();
                    GetStateCountryByCity1(GridData.QATABLE040[0].CITY);
                    $('#ModalHomeDelivery').modal('show');
                    saveHomeAddressPOP(1);
                }
            });
        }
        else {
            $('#txtHomeDelName').val('');
            $('#txtHomeDelAddr').val('');
            $('#txtHomeDelZipCode').val('');
            $('#txtHomeDelPhNo').val('');
            $('#txtHomeDelEmail').val('');
            $('#ModalHomeDelivery').modal('show');
        }
    }
    else {
        $('#ModalHomeDelivery').modal('hide');

        cancelHomeDelivery();
    }

}

function HomeDelivery1() {
    window.localStorage.setItem('ctCheck', 1);
    if ($('#chkHomeDelivery').prop('checked') == true) {
        if ($('#hdn_CusId').val() != "" && $('#txtCusName').val().substring(0, 2) != 'G#') {
            $.ajax({
                type: 'GET',
                url: CashierLite.Settings.GetCustomerMstDoubleClick,
                data: "CustId=" + $('#hdn_CusId').val() + "&VD=" + VD1(),
                success: function (resp) {
                    var GridData = JSON.parse(resp);
                    $('#txtHomeDelName').val(GridData.QATABLE038[0].CUSTOMER_NAME);
                    address = GridData.QATABLE040[0].ADDRESS1 + ',' + GridData.QATABLE040[0].ADDRESS2;

                    $('#txtHomeDelZipCode').val(GridData.QATABLE040[0].PINCODE);
                    $('#txtHomeDelPhNo').val(GridData.QATABLE040[0].MOBILE_NO);
                    $('#txtHomeDelEmail').val(GridData.QATABLE040[0].EMAIL);
                    GatCityData();
                    GetStateCountryByCity1(GridData.QATABLE040[0].CITY);
                }
            });
        }
        else {
            $('#txtHomeDelName').val('');
            $('#txtHomeDelAddr').val('');
            $('#txtHomeDelZipCode').val('');
            $('#txtHomeDelPhNo').val('');
            $('#txtHomeDelEmail').val('');
            $('#ModalHomeDelivery').modal('show');
        }
        //   $("#btnCheckHomeDelOK").prop('disabled', false);
    };
}

function GetSubOrgNameWithCur1(SubOrgId) {
    window.localStorage.setItem('ctCheck', 1);
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetSubOrgCurrency,
        data: "SubOrgId=" + SubOrgId + "&VD=" + VD1(),
        success: function (resp) {
            var CurrData = resp;
            window.localStorage.setItem("CurData", JSON.stringify(CurrData))
            for (var i = 0; i < CurrData.length; i++) {
                //     $('#txtCurrency').val(CurrData[0].CurrencyCode + " / " + CurrData[0].CurrencyName);
                $('#hdnCurrency1').val(CurrData[0].CurrencyId);
                //         $("#txtCurrency").prop("disabled", true);
            }

        },
        error: function (e) {
        }
    });
}

function insertCustomer() {

    window.localStorage.setItem('ctCheck', 1);
    var HashVal = (window.localStorage.getItem("hashValues"));
    var MDate;
    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');
    }
    GetSubOrgNameWithCur1(HashValues[1])
    var Mode = "Create";

    CustomerAddress = {
    }
    CustomerData = {
    }
    var custId = guid();
    $('#hdn_CusId').val(custId);
    CustomerData["CUSTOMER_ID"] = custId;
    CustomerData["CUSTOMER_CODE"] = 'GNY-ST-CUST-007';
    CustomerData["CUSTOMER_NAME"] = $("#txtHomeDelName").val().trim();
    CustomerData["ALIAS_NAME"] = '';


    CustomerData["CUSTOMER_TYPE_ID"] = '1';


    CustomerData["CUSTOMER_GRP_ID"] = '37';
    CustomerData["COMPANY_NAME"] = '';
    CustomerData["CONTACT_PERSON"] = '';
    CustomerData["CPER_DESIG"] = '';


    var CurDate = new Date();
    var DateWithFormate = CurDate.getFullYear() + '-' + (CurDate.getMonth() + 1) + '-' + CurDate.getDate() + ' ' + CurDate.getHours() + ':' + CurDate.getMinutes() + ':' + CurDate.getSeconds() + ':' + CurDate.getMilliseconds();
    CustomerData["EFFECTIVE_DATE"] = DateWithFormate;
    CustomerData["ORDER_PRIORITY"] = 'Y';
    CustomerData["CUST_SERVICE_REP"] = '';
    CustomerData["REFERED_BY"] = '';
    CustomerData["INCOTERMS_PAYMENT"] = '';
    CustomerData["PAYMENT_MODE"] = '';
    //CustomerData["SALES_TYPE"] = $("#ddlSalesType").setItem.value();
    CustomerData["SALES_TYPE"] = 'Domestic';

    CustomerData["HEAD_OFFICE"] = '';
    CustomerData["SALES_PERSON_ID"] = '';//ID


    CustomerData["CURRENCY_ID"] = $("#hdnCurrency1").val();


    CustomerData["LANGUAGE_ID"] = HashValues[7];
    CustomerData["NEAREST_DP_ID"] = '';
    CustomerData["PREF_SHIPPER"] = '';
    CustomerData["REMARKS"] = '';
    CustomerData["STATUS"] = '';
    CustomerData["BRANCH_ID"] = HashValues[0];
    CustomerData["SUB_ORG_ID"] = HashValues[0];
    CustomerData["OBS"] = HashValues[9];
    if ($("#calMarraigeDate").val() != "") {
        var MDate = '';
    }
    else {
        MDate = "";
    }
    CustomerData["TRANS_TYPE"] = 'POS';
    CustomerData["BIRTH_DATE"] = '';
    CustomerData["Marriage_Date"] = '';
    CustomerData["PARENTCUSTID"] = '';
    CustomerData["CATEGORY"] = 'SO';

    CustomerData["BRANCHID"] = HashValues[0];
    CustomerData["COMPANYID"] = HashValues[0];
    CustomerData["CREATEDBY"] = HashValues[14];
    CustomerData["CREATEDDATE"] = DateWithFormate;

    CustomerAddress["CREATEDBY"] = HashValues[14];
    CustomerAddress["CREATEDDATE"] = DateWithFormate;
    if (Mode == 'Create') {
        CustomerData["ROWGUID"] = '';
        CustomerAddress["ROWGUID"] = '';
        CustomerAddress["CustAddress_GUID"] = '';

        CustomerData["LASTUPDATEDBY"] = HashValues[14];
        CustomerData["LASTUPDATEDDATE"] = DateWithFormate;

        CustomerAddress["LASTUPDATEDBY"] = HashValues[14];
        CustomerAddress["LASTUPDATEDDATE"] = DateWithFormate;
    }
    if (Mode != 'Create') {
        CustomerData["LASTUPDATEDBY"] = $("#hdnLastUpdatedBy").val();
        CustomerData["LASTUPDATEDDATE"] = $("#hdnLastUpdatedDate").val();

        CustomerAddress["LASTUPDATEDBY"] = $("#hdnLastUpdatedBy").val();
        CustomerAddress["LASTUPDATEDDATE"] = $("#hdnLastUpdatedDate").val();
        $('#trSBInnerRow li span#lblLastUpdate').html("");
        $('#trSBInnerRow li span#lblLastUpdatedUser').html("");
        $('#trSBInnerRow li span#lblLastUpdatedDate').html("");
        $('#trSBInnerRow li span#lblMode').html('');
    }
    CustomerData["Email"] = $('#txtHomeDelEmail').val();
    CustomerData["Address"] = '';//$('#txtAddress').val();
    CustomerData["City_ID"] = '';// $('#hdnCityId').val();
    CustomerData["OpeningBalnce"] = parseFloat($('#txtOpeningBal').val());
    CustomerData["Mobile"] = $('#txtHomeDelPhNo').val();
    CustomerData["UPDATEDSITE"] = 'DBMS_REPUTIL.GLOBAL_NAME';
    CustomerData["LANGID"] = HashValues[7];

    CustomerData["PROCESS_STATUS"] = '2';
    CustomerData["insertType"] = '1';
    //Address
  /*  CustomerAddress["CustAddress_GUID"] = '';
    CustomerAddress["CUSTOMER_ID"] = custId;
    CustomerAddress["ADD_TYPE_ID"] = 'Shipping Address';
    CustomerAddress["ADDRESS1"] = $("#txtHomeDelAddr").val().trim();
    CustomerAddress["ADDRESS2"] = '';
    CustomerAddress["CITY"] = '';
    CustomerAddress["PROVINCE"] = '';
    CustomerAddress["COUNTRY_ID"] = '';
    CustomerAddress["PINCODE"] = $("#txtHomeDelZipCode").val().trim();
    CustomerAddress["PHONE_NO"] = '';
    CustomerAddress["IP_PHONE_NO"] = '';
    CustomerAddress["MOBILE_NO"] = $("#txtHomeDelPhNo").val().trim();
    CustomerAddress["EMAIL"] = $("#txtHomeDelEmail").val().trim();
    CustomerAddress["WEB_SITE"] = '';
    CustomerAddress["CONTACT_ADDRESS"] = 'Y';
    CustomerAddress["Remarks"] = '';
    CustomerAddress["status"] = '';
    CustomerAddress["FAX_NO"] = '';
    CustomerAddress["BRANCHID"] = HashValues[0];
    CustomerAddress["COMPANYID"] = HashValues[0];
    CustomerAddress["UPDATEDSITE"] = "DBMS_REPUTIL.GLOBAL_NAME";
    CustomerAddress["LANGID"] = HashValues[7];*/
    objCustomerData = []; 
   //objCustomerAddress = [];
    objCustomerData.push(CustomerData);
   // objCustomerAddress.push(CustomerAddress);




    //Create the customer object
    var CustomerParams = new Object();
    CustomerParams.Mode = Mode;
    CustomerParams.CustomerData = JSON.stringify(objCustomerData);
  //  CustomerParams.CustomerAddress = JSON.stringify(objCustomerAddress);
    CustomerParams.VD = VD1();

    //Put the customer object into a container
    var ComplexObject = new Object();
    //The property name "WebMethodArgName" must match the web method argument "WebMethodArgName"!
    ComplexObject.CustData = CustomerParams;

    //Stringify and verify the object is foramtted as expected
    var data = JSON.stringify(ComplexObject);
    $.ajax({
        type: 'POST',
        url: CashierLite.Settings.SaveCustomerData,
        data: data,
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        //data: "Mode=" + Mode + "&CustomerData=" + JSON.stringify(objCustomerData) + "&CustomerAddress=" + JSON.stringify(objCustomerAddress),
        success: function (resp) {
            if (resp != null) {
                if (resp.SaveCustomerMstDataResult == '100000') {//Success Record
                   // if ($("#hdnMode").val() == 'Delete') {
                        navigator.notification.alert($('#hdnRecSaveSucc').val(), "", "neo ecr", "Ok");
                   // }
                    popupSts = "";
                    GetCustomers_();
                    clearPOPUP();
                   // return false;
                }
                else if (resp.SaveCustomerMstDataResult == '100001') {//Duplicate Customer Code
                    $("#hdnTransId").val('');
                    navigator.notification.alert($('#hdnCodeAlreadyExist').val(), "", "neo ecr", "Ok");

                    //return false;
                }
                else if (resp.SaveCustomerMstDataResult == 'POS10002') {//Deleted Record
                    navigator.notification.alert($('#hdnDelSucc').val(), "", "neo ecr", "Ok");

                   // return false;
                }
                else if (resp.SaveCustomerMstDataResult == 'POSActive') {//Active Record
                    navigator.notification.alert($('#hdnRecActSucc').val(), "", "neo ecr", "Ok");
                   // return false;
                }
                else if (resp.SaveCustomerMstDataResult == 'POSInActive') {//Deactive Record
                    navigator.notification.alert($('#hdnRecInActSucc').val(), "", "neo ecr", "Ok");
                   // return false;
                }
                else {//Error Record
                    $("#hdnTransId").val('');
                    navigator.notification.alert($('#hdnErrCodeOccer').val(), "", "neo ecr", "Ok");
                  //  return false;
                }
            }
            return false;
        },
        error: function (e) {
            navigator.notification.alert($('#hdnErrCodeOccer').val(), "", "neo ecr", "Ok");
            return false;
        }
    });

}

function clearPOPUP() {
    window.localStorage.setItem('ctCheck', 1);
    $('#txtHomeDelName').val('');
    $('#txtHomeDelAddr').val('');
    $('#txtHomeDelZipCode').val('');
    $('#txtHomeDelPhNo').val('');
    $('#txtHomeDelEmail').val('');
    $('#txtOpeningBalance').val('0.00');
};

function contactNoExist() {
    //  $("#btnCheckHomeDelOK").prop('disabled', false);
    $('#txtHomeDelName').val('');
    $('#txtHomeDelAddr').val('');
    $('#txtHomeDelZipCode').val('');
    window.localStorage.setItem('ctCheck', 1);
    $('#txtHomeDelEmail').val('');
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.verifyContactNoExist,
        data: "contNo=" + $('#txtHomeDelPhNo').val() + "&VD=" + VD1(),
        success: function (resp) {
            GatCityData();

            var result = JSON.parse(resp);
            if (result["customerResult"].length > 0) {
                popupSts = 0;
                $('#txtHomeDelName').val(result["customerResult"][0]["NAME"]);
                var mm = result["customerResult"][0]["ADDRESS1"] + result["customerResult"][0]["ADDRESS2"];
                if (mm != '')
                    address = result["customerResult"][0]["ADDRESS1"] + "," + result["customerResult"][0]["ADDRESS2"]
                else
                    address = '';
                //  $('#txtHomeDelAddr').val(result["customerResult"][0]["ADDRESS1"] + "," + result["customerResult"][0]["ADDRESS2"]);
                $('#txtHomeDelZipCode').val(result["customerResult"][0]["PINCODE"]);
                $('#txtHomeDelPhNo').val(result["customerResult"][0]["MOBILENO"]);
                $('#txtHomeDelEmail').val(result["customerResult"][0]["EMAIL"]);
                if (result["customerResult"][0]["CITY"] != "") {
                    GetStateCountryByCity1(result["customerResult"][0]["CITY"]);
                } else {
                    $('#txtHomeDelAddr').val(address);
                }
                $('#hdn_CusId').val(result["customerResult"][0]["id"]);
                $('#txtHomeDelName').prop('disabled', true);
                $('#txtHomeDelAddr').prop('disabled', false);
                $('#txtHomeDelZipCode').prop('disabled', true);
                $('#txtHomeDelPhNo').prop('disabled', false);
                $('#txtHomeDelEmail').prop('disabled', true);
            } else {
                $('#txtHomeDelName').prop('disabled', false);
                $('#txtHomeDelAddr').prop('disabled', false);
                $('#txtHomeDelZipCode').prop('disabled', false);
                $('#txtHomeDelEmail').prop('disabled', false);
                popupSts = 1;
                return true;
            }
            var res = resp.d;
        }, error: function (e) {
        }
    });
}

function setLabels1(labelData) {
    window.localStorage.setItem('ctCheck', 1);
    $('#hdnPlentrValAmount').val(labelData["PlentrValAmount"]);
    $('#hdnPlunchckHomedelchckbxorentrCntctDtls').val(labelData["PlunchckHomedelchckbxorentrCntctDtls"]);
    $('#hdnPlSelatLeastOneWallet').val(labelData["PlSelatLeastOneWallet"]);
    $('#hdnPlEnterRefNo').val(labelData["alert10"] + ' ' + labelData["refNo"]);
    $('#hdnDayClsUndrProcCntDoAnyOpr').val(labelData["DayClsUndrProcCntDoAnyOpr"]);
    $('#hdnPlChckBalAmt').val(labelData["PlChckBalAmt"]);
    $('#TXT_upiid').attr('placeholder', labelData["PleaseEnter"] + ' ' + labelData["yourupiid"]);
    $('#txt_amount_Ref').attr('placeholder', labelData["PleaseEnter"] + ' ' + labelData["refNo"]);
    jQuery("label[for='lblSelectawalletapp'").html(labelData["Selectawalletapp"]);
    jQuery("label[for='lblChange'").html(labelData["change"]);
    jQuery("label[for='lblrefNo'").html(labelData["refNo"]);
    $('#txtWalletRefPPSub').attr('placeholder', labelData["PleaseEnter"] + ' ' + labelData["refNo"]);
    jQuery("label[for='lblPopWalletName'").html(labelData["Wallet"]);
    jQuery("label[for='lblPOPPay'").html(labelData["pay"]);
    jQuery("label[for='lblPayLater'").html(labelData["PayLater"]);

    jQuery("label[for='lblWallet'").html(labelData["Wallet"]);
    jQuery("label[for='lblUPI'").html(labelData["Wallet"]);
    jQuery("label[for='lblMakeapayment'").html(labelData["Makeapayment"]);
    $('#hdnReferenceNumber').val(labelData["ReferenceNumber"]);
    $('#hdnPartialPayment').val(labelData["PartialPayment"]);
    $('#hdnFullPayment').val(labelData["FullPayment"]);
    $('#hdnBack').val(labelData["Back"]);
    $('#hdnphPriceItem').val(labelData["PleaseEnter"] + ' ' + labelData["price"]);
    $('#hdnphUOMitem').val(labelData["alert10"] + ' ' + labelData["uom"]);
    $('#hdnphTaxValue').val(labelData["PleaseEnter"] + ' ' + labelData["TaxVAT"]);
    $('#hdnphbarcode').val(labelData["PleaseEnter"] + ' ' + labelData["Barcode"]);
    $('#hdnbarCodeAlreadyExist').val(labelData["Barcodeexists"]);
    $('#hdnPricegreaterthanzero').val(labelData["PriceGreaterthanZero"]);
    // $('#hdnQtygreaterthanzero').val(labelData["QtyGreaterthanZero"]);
    $('#hdnTaxgreaterthanzero').val(labelData["ValGreaterthanZero"]);
    jQuery("label[for='lblTaxApplicabilty'").html(labelData["taxApplicabilty"]);
    jQuery("label[for='lblOnMRP'").html(labelData["OnMRP"]);
    $('#txtTaxValue').attr('placeholder', labelData["PleaseEnter"] + ' ' + labelData["tax"]);
    $('#txtPriceItem').attr('placeholder', labelData["PleaseEnter"] + ' ' + labelData["price"]);
    $('#txtCounterCode').attr('placeholder', labelData["PleaseEnter"] + ' ' + labelData["code"]);
    $('#txtCounterName').attr('placeholder', labelData["PleaseEnter"] + ' ' + labelData["CommonName"]);
    $('#txtAliasName').attr('placeholder', labelData["PleaseEnter"] + ' ' + labelData["Alias"]);
    $('#txtUOMitem').attr('placeholder', labelData["alert10"] + ' ' + labelData["uom"]);
    $('#txtBarcode').attr('placeholder', labelData["PleaseEnter"] + ' ' + labelData["Barcode"]);
    jQuery("label[for='lblOrganization'").html(labelData["subOrganization"]);
    jQuery("label[for='lblDetCode'").html(labelData["code"]);
    jQuery("label[for='lblDetName'").html(labelData["CommonName"]);
    $('#hdnSelect').val(labelData["select"]);
    jQuery("label[for='lblUOM'").html(labelData["uom"]);
    jQuery("label[for='lblAlias'").html(labelData["Alias"]);
    jQuery("label[for='lblbarCode'").html(labelData["Barcode"]);
    jQuery("label[for='lblTaxApp'").html(labelData["taxApplicabilty"]);
    jQuery("label[for='lblOnMRP'").html(labelData["OnMRP"]);
    jQuery("label[for='lblTaxValue'").html(labelData["TaxVAT"]);
    jQuery("label[for='lblPrice'").html(labelData["price"]);
    $('#lblTaxValue')[0].innerText = labelData["TaxVAT"];
    $('#lblNavigationUrl').text(labelData["pos"] + "  >>  " + labelData["transactions"] + "  >>  " + labelData["invoice"]);
    jQuery("label[for='lblPwrddbyCshrLt'").html(labelData["PwrddbyCshrLt"]);
    jQuery("label[for='lblPlEtrAuthPass'").html(labelData["PlEtrAuthPass"]);
    jQuery("label[for='lblOk'").html(labelData["ok"]);

    jQuery("label[for='lblUploadUserImage'").html(labelData["UploadUserImage"]);
    jQuery("label[for='lblPleasewait'").html(labelData["Pleasewait"]);
    jQuery("label[for='lblChange'").html(labelData["change"]);
    jQuery("label[for='lblTenderType'").html(labelData["TenderType"]);
    jQuery("label[for='lblTenders'").html(labelData["Tenders"]);
    jQuery("label[for='lblSearchItem'").html(labelData["SearchItem"]);
    jQuery("label[for='lblTotalSctock'").html(labelData["StockCount"]);
    jQuery("label[for='lblVoid'").html(labelData["POSVoid"]);
    jQuery("label[for='lblSubcription'").html(labelData["Renewal"]);
    jQuery("label[for='lblTaxValue'").html(labelData["TaxVAT"]);
    jQuery("label[for='lblPrice'").html(labelData["price"]);
    $('#lblTaxValue')[0].innerText = labelData["TaxVAT"];
    $('#hdnEntrCode').val(labelData["alert24"]);
    $('#hdnEntrName').val(labelData["MsgCounterName"]);
    $('#hdnCurSymbol').val(labelData["posCuSymbol"]);
    jQuery("label[for='lblItemDraft'").html(labelData["itemdraft"]);
    jQuery("label[for='lblSCPopPwd'").html(labelData["pSettings"]);
    jQuery("label[for='lblLanguage'").html(labelData["lang"]);
    jQuery("label[for='lblDateFormat'").html(labelData["format"]);
    jQuery("label[for='lblPopOk'").html(labelData["ok"]);
    $('#btnCheck_OK').html(labelData["ok"]);
    jQuery("label[for='lblPopClose'").html(labelData["cancel"]);
    jQuery("label[for='lblSettings'").html(labelData["settings"]);
    jQuery("label[for='lblShift'").html(labelData["shift"]);
    jQuery("label[for='lblShiftStart'").html(labelData["shiftStart"]);
    jQuery("label[for='lblShiftEnd'").html(labelData["shiftEnd"]);
    jQuery("label[for='lblThemes'").html(labelData["themes"]);
    jQuery("label[for='lblTheme1'").html(labelData["theme1"]);
    jQuery("label[for='lblTheme2'").html(labelData["theme2"]);
    jQuery("label[for='lblTheme3'").html(labelData["theme3"]);
    jQuery("label[for='lblTheme4'").html(labelData["theme4"]);
    jQuery("label[for='lblChangePassword'").html(labelData["changePassword"]);
    jQuery("label[for='lblShortCut'").html(labelData["shortcut"]);
    jQuery("label[for='lblTenderType'").html(labelData["tenderType"]);
    jQuery("label[for='lblCreate'").html(labelData["create"]);
    jQuery("label[for='lblSave'").html(labelData["save"]);
    jQuery("label[for='lblEdit'").html(labelData["edit"]);
    jQuery("label[for='lblDelete'").html(labelData["delete"]);
    jQuery("label[for='lblCancel'").html(labelData["cancel"]);
    jQuery("label[for='lblStatus'").html(labelData["status"]);
    jQuery("label[for='lblToggleDropdown'").html(labelData["toggleDropdown"]);
    jQuery("label[for='lblActive'").html(labelData["active"]);
    jQuery("label[for='lblInActive'").html(labelData["inactive"]);
    jQuery("label[for='lblFilter'").html(labelData["filter"]);
    jQuery("label[for='lblFilterToggleDropdown'").html(labelData["toggleDropdown"]);
    jQuery("label[for='lblAll'").html(labelData["all"]);
    jQuery("label[for='lblToday'").html(labelData["toDay"]);
    jQuery("label[for='lblYesterDay'").html(labelData["yesterDay"]);
    jQuery("label[for='lblLastSevenDays'").html(labelData["lastSevenDays"]);
    jQuery("label[for='lblCurrentMonth'").html(labelData["currentMonth"]);
    jQuery("label[for='lblToggleActive'").html(labelData["active"]);
    jQuery("label[for='lblToggleInactive'").html(labelData["inactive"]);
    jQuery("label[for='lblConfigurators'").html(labelData["configurators"]);
    jQuery("label[for='lblSubConfigurators'").html(labelData["POSEwalletConSts"]);
    jQuery("label[for='lblPrintCustomize'").html(labelData["printCustamize"]);
    jQuery("label[for='lblBarcodeGenerator'").html(labelData["barCodeGenerator"]);
    jQuery("label[for='lblMasters'").html(labelData["masters"]);
    jQuery("label[for='lblTillType'").html(labelData["tillType"]);
    jQuery("label[for='lblTill'").html(labelData["till"]);
    jQuery("label[for='lblTillPermissions'").html(labelData["tillPermissions"]);
    jQuery("label[for='lblUserPermissions'").html(labelData["user"]);
    jQuery("label[for='lblCustomer'").html(labelData["customer"]);
    jQuery("label[for='lblTenderType'").html(labelData["tenderType"]);
    jQuery("label[for='lblTransactions'").html(labelData["transactions"]);
    jQuery("label[for='lblInvoce'").html(labelData["invoice"]);
    jQuery("label[for='lblSalesReturn'").html(labelData["salesReturn"]);
    jQuery("label[for='lblPosting'").html(labelData["posting"]);
    jQuery("label[for='lblTillOpening'").html(labelData["tillOpening"]);
    jQuery("label[for='lblTillClosing'").html(labelData["tillClosing"]);
    jQuery("label[for='lblAccountPosting'").html(labelData["accountPosting"]);
    jQuery("label[for='lblReports'").html(labelData["reports"]);
    jQuery("label[for='lblInvoiceListing'").html(labelData["invoiceListing"]);
    jQuery("label[for='lblTillInvoiceListing'").html(labelData["tillInvoiceListing"]);
    jQuery("label[for='lblInvoiceSummary'").html(labelData["category"]);
    jQuery("label[for='lblTenderTypeWiseCollectionDailySummary'").html(labelData["tenderTypeWiseCollectionDailySummary"]);
    jQuery("label[for='lblSalesReturns'").html(labelData["salesReturns"]);
    jQuery("label[for='lblCustomerEnroll'").html(labelData["CustomerEnroll"]);
   // jQuery("label[for='lblHomeDelHeader'").html(labelData["HomeDelivery"]);
    jQuery("label[for='lblHomeDelivery'").html(labelData["HomeDelivery"]);
    jQuery("label[for='lblGiftVouche'").html(labelData["giftVoucher"]);
    jQuery("label[for='lblCreditNote'").html(labelData["creditNote"]);
    jQuery("label[for='lblLoyaltyPoints'").html(labelData["LoyaltyPoints"]);
    jQuery("label[for='lblUserPermissions'").html(labelData["user"]);
    jQuery("label[for='lblDrft'").html(labelData["draft"]);
    jQuery("label[for='lblNewCart'").html(labelData["newCart"]);
    jQuery("label[for='lblItemCart'").html(labelData["itemCart"]);
    jQuery("label[for='lblItem'").html(labelData["item"]);
    jQuery("label[for='lblQty'").html(labelData["qty"]);
    jQuery("label[for='lblDisc'").html(labelData["disc"]);
    jQuery("label[for='lblDiscount'").html(labelData["disc"]);
    jQuery("label[for='lblNetAmount'").html(labelData["netAmount"]);
    jQuery("label[for='lblCustomer'").html(labelData["customer"]);
    jQuery("label[for='lblAmount3'").html(labelData["amount"]);
    jQuery("label[for='lblAmount'").html(labelData["amount"]);
    jQuery("label[for='lblAmount1'").html(labelData["amount"]);
    jQuery("label[for='lblStockItem'").html(labelData["stockItems"]);
    jQuery("label[for='lblQuickSearch'").html(labelData["quickSearch"]);
    jQuery("label[for='lblCalUOM'").html(labelData["uom"]);
    jQuery("label[for='lblcalQty'").html(labelData["qty"]);
    jQuery("label[for='lblCalDisc'").html(labelData["disc"]);
    jQuery("label[for='lblPrice'").html(labelData["price"]);
    jQuery("label[for='lblCurrentInventory'").html(labelData["CurrentInventory"]);
    jQuery("label[for='lblReceipt'").html(labelData["receipt"]);
    jQuery("label[for='lblVATReturns'").html(labelData["VATReturns"]);
    //jQuery("label[for='lblSubQty'").html(labelData["qty"]);
    $('#liDisM').html(labelData["disc"]);
    $('#liQtyM').html(labelData["qty"]);
    $('#liPriceM').html(labelData["price"]);
    $('#liPaym').html(labelData["pay"]);
    //jQuery("label[for='lblSubPrice'").html(labelData["price"]);
    //jQuery("label[for='lblSubDel'").html(labelData["delete"]);
    //jQuery("label[for='lblSubPay'").html(labelData["pay"]);
    jQuery("label[for='lblPaySummary'").html(labelData["paySummary"]);
    jQuery("label[for='lblTax'").html(labelData["tax"]);
    jQuery("label[for='lblTotalItems'").html(labelData["totalItems"]);
    jQuery("label[for='lblPopQty'").html(labelData["qty"]);
    jQuery("label[for='lblPopGrossTot'").html(labelData["grossTot"]);
    jQuery("label[for='lblPopDiscount'").html(labelData["disc"].replace('(%)', ''));
    jQuery("label[for='lblPopTax'").html(labelData["tax"]);
    //jQuery("label[for='lblPopTotal'").html(labelData["total"]);
    jQuery("label[for='lblPopCurrency'").html(labelData["currency"]);
    jQuery("label[for='lblAmountTendered'").html(labelData["amountTendered"]);
    jQuery("label[for='lblTransamodetype'").html(labelData["amount"]);
    jQuery("label[for='lblTransamodetypeRef'").html(labelData["change"]);
    jQuery("label[for='lblAdjust'").html(labelData["adjust"]);
    jQuery("label[for='lblCalculator'").html(labelData["calc"]);
    jQuery("label[for='lblProceed'").html(labelData["proceed"]);
    jQuery("label[for='lblClose'").html(labelData["closed"]);
    jQuery("label[for='lblPrint'").html(labelData["print"]);
    jQuery("label[for='lblModelPrint'").html(labelData["print"]);
    jQuery("label[for='lblModelPaySummary'").html(labelData["paySummary"]);
    jQuery("label[for='lblModelInvoice'").html(labelData["invoice"]);
    jQuery("label[for='lblModelItem'").html(labelData["item"]);
    jQuery("label[for='lblModelQty'").html(labelData["qty"]);
    jQuery("label[for='lblModelRate'").html(labelData["rate"]);
    jQuery("label[for='lblModelAmt'").html(labelData["amount"]);
    jQuery("label[for='lblPopTotal'").html(labelData["total"]);
    jQuery("label[for='lblModelPaid'").html(labelData["paid"]);
    jQuery("label[for='lblModelChange'").html(labelData["change"]);
    jQuery("label[for='lblSCPopShortcutKeys'").html(labelData["shortcuts"]);
    jQuery("label[for='lblSCPopShortcutKey'").html(labelData["shortcut"]);
    jQuery("label[for='lblSCPopOpenShortcuts'").html(labelData["openShotcutKeys"]);
    jQuery("label[for='lblSCPopScanItemFocus'").html(labelData["scanItemFocus"]);
    jQuery("label[for='lblSCPopChangetheQuantity'").html(labelData["changeQty"]);
    jQuery("label[for='lblSCPopChangetheDisc'").html(labelData["changeDesc"]);
    jQuery("label[for='lblSCPopChangethePrice'").html(labelData["changePrice"]);
    jQuery("label[for='lblSCPopProceedPaySummary'").html(labelData["proceedPaySummary"]);
    jQuery("label[for='lblSCPopOpenDraft'").html(labelData["openDraft"]);
    jQuery("label[for='lblSCPopNewCartStoreDraft'").html(labelData["newCartstrDrft"]);
    jQuery("label[for='lblSCPopPaymentProceed'").html(labelData["payProceed"]);
    jQuery("label[for='lblSCPopClosingPaySummary'").html(labelData["closingPaySummary"]);
    jQuery("label[for='lblSCPopClose'").html(labelData["closed"]);
    jQuery("label[for='lblCreditcrSales'").html(labelData["receipt"]);
    jQuery("label[for='lblDispatch'").html(labelData["Dispatch"]);
    jQuery("label[for='lblPopBalance'").html(labelData["PopBalance"]);
    jQuery("label[for='lblPointsObtainBasecurrency'").html(labelData["PointsObtainBasecurrency"]);
    $('#lblRewardPoints').html(labelData["RedeemPoints"]);
    jQuery("label[for='lblMinPoints'").html(labelData["MinPoints"]);
    jQuery("label[for='lblMaxPoints'").html(labelData["MaxPoints"]); 
    jQuery("label[for='lblHomeDelName'").html(labelData["CommonName"]);
    jQuery("label[for='lblHomeDelAddr'").html(labelData["Address"]);
    jQuery("label[for='lblHomeDelZipCode'").html(labelData["ZipCode"]);
    jQuery("label[for='lblHomeDelEmail'").html(labelData["Email"]);
    jQuery("label[for='lblGiftVoucheRefno'").html(labelData["GiftVoucherRefNo"]);
    jQuery("label[for='lblGiftVoucheRefnoAmt'").html(labelData["amount"]);
    jQuery("label[for='lblAvailableAmount'").html(labelData["AvailAmount"]);
    jQuery("label[for='lblRedeemPoints'").html(labelData["RedeemPoint"]);
    jQuery("label[for='lblAvailPoints'").html(labelData["AvailPoints"]);
    jQuery("label[for='lblCreditNoteRefnoAmt'").html(labelData["amount"]);
    jQuery("label[for='lblCreditNoteRefno'").html(labelData["CreditNoteRefNo"]);
    jQuery("label[for='lblUpload'").html(labelData["upload"]);
    jQuery("label[for='lblCreditSales'").html(labelData["POSCRDREC"]);
    $('#hdnImageSave').val(labelData["ImageSave"]);
    $('#hdnSelImage').val(labelData["SelUserImage"]);
    $('#txtCusName').attr('placeholder', labelData["fillCust"]);
    $('#txtItemScan').attr('placeholder', labelData["scanItem"]);
    $('#txtItemSearch').attr('placeholder', labelData["searchItems"]);
    $('#txtItemSearchCart').attr('placeholder', labelData["searchItems"]);
    $('#txtUOM').attr('placeholder', labelData["uom"]);
    $('#txtUOMitem').attr('placeholder', labelData["uom"]);
    $('#txtQuantity').attr('placeholder', labelData["qty"]);
    $('#txtDiscount').attr('placeholder', labelData["disc"]);
    $('#txtPrice').attr('placeholder', labelData["price"]);
    $('#txtAmount').attr('placeholder', labelData["amount"]);
    $('#txtItemspp').attr('placeholder', labelData["disc"]);
    $('#txtQuantityPP').attr('placeholder', labelData["disc"]);
    $('#txtPricepp').attr('placeholder', labelData["grossTot"]);
    $('#txtDiscountpp').attr('placeholder', labelData["disc"]);
    $('#txtTaxPP').attr('placeholder', labelData["disc"]);
    $('#txtAmountpp').attr('placeholder', labelData["disc"]);
    $('#txtCurrency').attr('placeholder', labelData["disc"]);
    $('#hdnRecSaveSucc').val(labelData["saveSuccess"]);
    $('#hdnICodeAlreadyExist').val(labelData["alert23"]);
    $('#hdnAccessDenied').val(labelData["alert1"]);
    $('#hdnThameChange').val(labelData["alert2"]);
    $('#hdnSelCurType').val(labelData["alert3"]);
    $('#hdnSelTranType').val(labelData["alert4"]);
    $('#hdnSelCurrType').val(labelData["alert5"]);
    $('#hdnMsgCart').val(labelData["alert6"]);
    $('#hdnCounter').val(labelData["alert7"]);
    $('#hdnSearchTitleInv').val(labelData["alert8"]);
    $('#hdnPayIn').val(labelData["alert9"]);
    $('#hdnPlzSelectItem').val(labelData["alert10"] + ' ' + labelData["item"]);
    $('#hdnPlzSelectTranType').val(labelData["alert10"] + labelData["tenderType"]);
    $('#hdnInvalAuth').val(labelData["alert11"]);
    $('#hdnAmtGrtrEqlAmtTend').val(labelData["alert12"]);
    $('#hdnMsgTranRef').val(labelData["alert13"]);
    $('#hdnDiscVal').val(labelData["alert14"]);
    $('#hdnDontPermisAccessInvoice').val(labelData["alert15"]);
    $('#hdnPleaseContactAdmin').val(labelData["alert16"]);
    $('#hdnNotAssignCountrToTrans').val(labelData["alert17"]);
    $('#hdnItmNotAvail').val(labelData["alert18"]);
    $('#hdnSelCust').val(labelData["alert19"]);
    $('#hdnItem').val(labelData["item"]);
    $('#hdnQty').val(labelData["qty"]);
    $('#hdnDisc').val(labelData["disc"]);
    $('#hdnPrice').val(labelData["price"]);
    $('#hdnAmount').val(labelData["amount"]);
    $('#hdnTotal').val(labelData["total"]);
    $('#hdnInvalidData').val(labelData["InvalidData"]);
    $('#hdnChange').val(labelData["change"]);
    $('#hdnRefNo').val(labelData["refNo"]);
    $('#hdnExpDt').val(labelData["alert11"]);
    $('#hdnChPwdSaveSucc').val(labelData["saveSuccess"]);
    $('#hdnNetTotal').val(labelData["netTotal"]);
    $('#hdnRound').val(labelData["Round"]);
    $('#hdnBaseTotal').val(labelData["alert20"]);
    $('#hdnTaxTotal').val(labelData["alert21"]);
    $('#hdnNotAssignCountrToTrans').val(labelData["permissionsAssisted"]);
    $('#hdnShift').val(labelData['shift']);
    $('#hdnInvoice').val(labelData['invoice']);
    $('#hdnTotItems').val(labelData['totalItems']);
    $('#hdnTotQty').val(labelData['total'] + ' ' + labelData['qty']);
    $('#hdnECrSales').val(labelData["PosCrdSales"]);
    $('#hdnPlsContactNo').val(labelData["PlsContactNo"]);
    $('#hdnRewardPoints404').val(labelData["RewardPoints404"]);
    $('#hdnPlsEnterAddrss').val(labelData["PlsEnterAddrss"]);
    $('#hdnPlsEnterAmount').val(labelData["PlsEnterAmount"]);
    $('#hdnrewardptsgrtrMinpts').val(labelData["rewardptsgrtrMinpts"]);
    $('#hdnrewardptsLstrMinpts').val(labelData["rewardptsLstrMinpts"]);
    $('#hdnMsgCounterName').val(labelData["MsgCounterName"]);
    $('#btnCreditOK').html(labelData["ok"]);
    $('#btnCreditClose').html(labelData["closed"]);
    $('#btnCreditRedeemOK').html(labelData["ok"]);
    $('#btnCreditRedeemClose').html(labelData["closed"]);
    $('#btnGiftVoucheOk ').html(labelData["ok"]);
    $('#btnGiftVoucheClose').html(labelData["closed"]);
    jQuery("label[for='lblLocationwiseProfitAnalysis'").html(labelData["LocwiseProfitAnlys"]);
    jQuery("label[for='lblItemCategoryProfitAnalysis'").html(labelData["ItemCatgPrftAnlys"]);
    jQuery("label[for='lblAnalyticalReports'").html(labelData["AnalyticalReports"]);
    jQuery("label[for='lblItemCategorySalesAnalysis'").html(labelData["ItemCategorySalesAnalysis"]);
    jQuery("label[for='lblWeekDaysRevenue'").html(labelData["WeekdaysRevenue"]);
    jQuery("label[for='lblBucketAnalysis'").html(labelData["BucketAnalysis"]);
    jQuery("label[for='lblLocationwiseAverageInvoiceValue'").html(labelData["LocationwiseAverageInvoiceValue"]);
    jQuery("label[for='lblRewardPointsLabel'").html(labelData["alert22"]);
    jQuery("label[for='lblSelectaUPIapp'").html(labelData["Selectawalletapp"]);


    $('#txtWalletAmtPPSub').attr('placeholder', labelData["PleaseEnter"] + ' ' + labelData["amount"]);
    jQuery("label[for='lblInvoiceNo'").html(labelData["alert8"]);
    jQuery("label[for='lblInvoiceAmt'").html(labelData["alert27"]);
    jQuery("label[for='lblReceivedAmit'").html(labelData["alert28"]);
    jQuery("label[for='INvoiceNo'").html(labelData["invoice"]);
    jQuery("label[for='Remarks'").html(labelData["remarks"]);

    jQuery("label[for='lblDlName'").html(labelData["DualDis"]);
    jQuery("label[for='lblStart'").html(labelData["Start"]);
    jQuery("label[for='lblStop'").html(labelData["Stop"]);
    jQuery("label[for='lblAdvertisement'").html(labelData["Avdrt"]);

    $('#hdnImageSave').val(labelData["ImageSave"]);
    $('#hdnSelImage').val(labelData["SelUserImage"]);
    jQuery("label[for='lblUpload'").html(labelData["upload"]);
    jQuery("label[for='lblTotalItemsCW'").html(labelData["totalItems"]);
    $('#hdnBarcodeMaxlen12').val(labelData["alert29"]);
    $('#hdnBarCodNtFound').val(labelData["confPwd"]);
    $('#hdnChkInvNo').val(labelData["VoidChkInvNo"]);
    $('#hdnChkRemarks').val(labelData["VoidChkRemarks"]);
    $('#hdnVoidSuccess').val(labelData["VoidSuccess"]);

    $('#hdnEcash').val(labelData["POSCash"]);
    $('#hdnEIPay').val(labelData["POSEWallet"]);
    $('#hdnECard').val(labelData["POSEcard"]);
    $('#hdnECNote').val(labelData["creditNote"]);
    $('#hdnInvNopopup').val(labelData["alert144"]);

    jQuery("label[for='lblPairDevice'").html(labelData["KeyBlue1"]);  
    jQuery("label[for='lblBlueHeader'").html(labelData["KeyBlue3"]);
    jQuery("label[for='lblDemoHeader'").html(labelData["VideoDemo"]);
    jQuery("label[for='lblDemo'").html(labelData["VideoDemo"]);
    $('#hdnPaymsgtwo').val(labelData["KeyBlue2"]);
    $('#hdnPaymsgfour').val(labelData["KeyBlue4"]);
    $('#hdncrdAmtCheck').val(labelData["CrdAmtCheck"]);
    jQuery("label[for='lblCustLDGR'").html(labelData["PosCustLdger"]);
    jQuery("label[for='CRCustomer'").html(labelData["customer"]);
    jQuery("label[for='CRlblOutstaingBal'").html(labelData["OutStandingBal"]);
    jQuery("label[for='lblRecAmount'").html(labelData["alert28"]);
    jQuery("label[for='lblPopOPB'").html(labelData["OpeningBalance"]);
    jQuery("label[for='lblcash'").html(labelData["POSCash"]);
    jQuery("label[for='EDlblcard'").html(labelData["POSEcard"]); 
    $('#EDlblcard').html(labelData["POSEcard"]);
    jQuery("label[for='lblHomeDelPhNo'").html(labelData["mobile"]);  
    jQuery("label[for='lblPopCloseOne'").html(labelData["closed"]);  
    GetTypeTenderDataCashby();
}

function getResourcesDash3(langCode, pageCode) {
    window.localStorage.setItem('ctCheck', 1);
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.getResource,
        data: "langCode=" + langCode + "&pageCode=" + pageCode,
        success: function (resp) {
            setLabelsDash3(resp);
        }
    });
}
$('#card-payment').click(function () {
    $('#form-section').show();
    $('#card-pay,#footer-pay').hide();
});

$('#card-pay-icon').click(function () {
    $('#card-pay,#form-section-2,#footer-pay').show();
    $('#form-section').hide();
});
$('#netbanking-pay-icon').click(function () {
    $('#netbanking-pay,#footer-pay').show();
    $('#form-section').hide();
});

$('#UPI-card-payment_DIV').click(function () {
    $('#div_form-section-UPI_UPI').hide();
    $('#form-section').show();
});

$('#wallet-pay-icon').click(function () {
    //ConvertWalletAmount_n2();
    $('#QR-code-pay_2').hide();
    $('#form-section').hide();
    $('#div_form-section-UPI_UPI').show();

});

$('#wallet-card-payment').click(function () {
    $('#form-section').show();
    $('#footer-pay,#wallet-pay').hide();
    $('#form-section').hide();
    $('#div_form-section-UPI_UPI').show();
});

$('#UPI-pay-icon').click(function () {

    $('#form-section-UPI').show();
    $('#UPI-pay').show();
    $('#form-section,#footer-pay,#bhim-pay').hide();
    $('#lblSelectaUPIapp').attr('display', 'none');
    $('#lblSelectawalletapp').attr('display', 'block');
    $('#form-section-UPI,#QR-code-pay').show();
    $('#bhim-pay,#footer-pay,#div_qrimg').hide();
});

$('#UPI-card-payment').click(function () {
    $('#form-section').show();
    $('#footer-pay,#UPI-pay').hide();
});

$('#bhim-icon').click(function () {
    $('#bhim-pay,#footer-pay').show();
    $('#form-section-UPI,#QR-code-pay').hide();
});

$('#change-app_2').click(function () {
    $('#div_form-section-UPI_UPI').show();
    $('#QR-code-pay_2,#wallet-pay,#div_qrimg,#footer-pay').hide();

    $('#txt_amount_Ref').val('');
});
$('#change-app').click(function () {
    $('#form-section-UPI,#QR-code-pay').show();
    $('#bhim-pay,#footer-pay,#div_qrimg').hide();


});
$('#googlepay-icon').click(function () {
    $('#google-pay,#footer-pay').show();
    $('#form-section-UPI,#QR-code-pay').hide();
});

$('#change-app-google').click(function () {
    $('#form-section-UPI,#QR-code-pay').show();
    $('#google-pay,#footer-pay').hide();
});

$('#whatsapp-icon').click(function () {
    $('#whatsapp-pay,#footer-pay').show();
    $('#form-section-UPI,#QR-code-pay').hide();
});

$('#change-app-whatsapp').click(function () {
    $('#form-section-UPI,#QR-code-pay').show();
    $('#whatsapp-pay,#footer-pay').hide();
});

$('#paytm-icon').click(function () {
    $('#paytm-pay,#footer-pay').show();
    $('#form-section-UPI,#QR-code-pay').hide();
});

$('#change-app-paytm').click(function () {
    $('#form-section-UPI,#QR-code-pay').show();
    $('#paytm-pay,#footer-pay').hide();
});

$('#phonepe-icon').click(function () {
    $('#phonepe-pay,#footer-pay').show();
    $('#form-section-UPI,#QR-code-pay').hide();
});

$('#change-app-phonepay').click(function () {
    $('#form-section-UPI,#QR-code-pay').show();
    $('#phonepe-pay,#footer-pay').hide();
});

$('#other-icon').click(function () {
    $('#other-pay,#footer-pay').show();
    $('#form-section-UPI,#QR-code-pay').hide();
});

$('#change-app-other').click(function () {
    $('#form-section-UPI,#QR-code-pay').show();
    $('#other-pay,#footer-pay').hide();
});

$('#QR-code-pay').click(function () {
    $('#QRCode-pay-section').show();
    $('#form-section-UPI,#QR-code-pay,#UPI-pay,#footer-pay').hide();
});

$('#QRCode-pay-section').click(function () {
    $('#QRCode-pay-section').hide();
    $('#form-section-UPI,#QR-code-pay,#UPI-pay').show();
});


var currency_symbols = {
    'AED': 'د.إ', // ?
    'AFN': '؋',
    'ALL': '',
    'AMD': '',
    'ANG': 'ƒ',
    'AOA': '', // ?
    'ARS': '$',
    'AUD': '$',
    'AWG': 'ƒ',
    'AZN': '',
    'BAM': 'KM',
    'BBD': '$',
    'BDT': '৳', // ?
    'BGN': 'Лв',
    'BHD': 'د.ب', // ?
    'BIF': 'FBu', // ?
    'BMD': '$',
    'BND': '$',
    'BOB': '',
    'BRL': '',
    'BSD': '',
    'BTN': '', // ?
    'BWP': '',
    'BYR': '',
    'BZD': '',
    'CAD': '',
    'CDF': '',
    'CHF': '',
    'CLF': '', // ?
    'CLP': '',
    'CNY': '',
    'COP': '',
    'CRC': '',
    'CUP': '',
    'CVE': '', // ?
    'CZK': '',
    'DJF': '', // ?
    'DKK': '',
    'DOP': '',
    'DZD': '', // ?
    'EGP': '',
    'ETB': '',
    'EUR': '€',
    'FJD': '',
    'FKP': '',
    'GBP': '',
    'GEL': '', // ?
    'GHS': '',
    'GIP': '',
    'GMD': '', // ?
    'GNF': '', // ?
    'GTQ': '',
    'GYD': '',
    'HKD': '',
    'HNL': '',
    'HRK': '',
    'HTG': '', // ?
    'HUF': '',
    'IDR': 'Rp',
    'ILS': '',
    'INR': '₹',
    'IQD': '', // ?
    'IRR': '',
    'ISK': '',
    'JEP': '',
    'JMD': '',
    'JOD': '', // ?
    'JPY': '',
    'KES': '', // ?
    'KGS': '',
    'KHR': '',
    'KMF': '', // ?
    'KPW': '',
    'KRW': '',
    'KWD': '', // ?
    'KYD': '',
    'KZT': '',
    'LAK': '',
    'LBP': '',
    'LKR': '',
    'LRD': '',
    'LSL': '', // ?
    'LTL': '',
    'LVL': '',
    'LYD': '', // ?
    'MAD': '', //?
    'MDL': '',
    'MGA': '', // ?
    'MKD': '',
    'MMK': '',
    'MNT': '',
    'MOP': '', // ?
    'MRO': '', // ?
    'MUR': '', // ?
    'MVR': '', // ?
    'MWK': '',
    'MXN': '',
    'MYR': '',
    'MZN': '',
    'NAD': '',
    'NGN': '',
    'NIO': '',
    'NOK': '',
    'NPR': '',
    'NZD': '',
    'OMR': '',
    'PAB': '',
    'PEN': '',
    'PGK': '', // ?
    'PHP': '',
    'PKR': '',
    'PLN': '',
    'PYG': '',
    'QAR': '',
    'RON': '',
    'RSD': '',
    'RUB': '',
    'RWF': '',
    'SAR': '﷼',
    'SBD': '',
    'SCR': '',
    'SDG': '', // ?
    'SEK': '',
    'SGD': '',
    'SHP': '',
    'SLL': '', // ?
    'SOS': '',
    'SRD': '',
    'STD': '', // ?
    'SVC': '',
    'SYP': '',
    'SZL': '', // ?
    'THB': '',
    'TJS': '', // ? TJS (guess)
    'TMT': '',
    'TND': '',
    'TOP': '',
    'TRY': '', // New Turkey Lira (old symbol used)
    'TTD': '',
    'TWD': '',
    'TZS': '',
    'UAH': '',
    'UGX': '',
    'USD': '$',
    'UYU': '',
    'UZS': '',
    'VEF': '',
    'VND': '',
    'VUV': '',
    'WST': '',
    'XAF': '',
    'XCD': '',
    'XDR': '',
    'XOF': '',
    'XPF': '',
    'YER': '',
    'ZAR': '',
    'ZMK': '', // ?
    'ZWL': '',
}




function setLabelsDash3(labelData) {
    window.localStorage.setItem('ctCheck', 1);
    jQuery("label[for='lblUploadUserImage'").html(labelData["UploadUserImage"]);
    jQuery("label[for='lblItemDraft'").html(labelData["itemdraft"]);
    jQuery("label[for='lblLocationwiseProfitAnalysis'").html(labelData["LocwiseProfitAnlys"]);
    jQuery("label[for='lblItemCategoryProfitAnalysis'").html(labelData["ItemCatgPrftAnlys"]);
    jQuery("label[for='lblAnalyticalReports'").html(labelData["AnalyticalReports"]);
    jQuery("label[for='lblItemCategorySalesAnalysis'").html(labelData["ItemCategorySalesAnalysis"]);
    jQuery("label[for='lblWeekDaysRevenue'").html(labelData["WeekdaysRevenue"]);
    jQuery("label[for='lblBucketAnalysis'").html(labelData["BucketAnalysis"]);
    jQuery("label[for='lblLocationwiseAverageInvoiceValue'").html(labelData["LocationwiseAverageInvoiceValue"]);

    jQuery("label[for='lblYear'").html(labelData["year"]);
    jQuery("label[for='lblMonth'").html(labelData["month"]);
    jQuery("label[for='lblSalesInfo'").html(labelData["salesInfo"]);
    jQuery("label[for='lblYear'").html(labelData["year"]);
    jQuery("label[for='lblSalesAnalysis'").html(labelData["salesAnalysis"]);
    jQuery("label[for='lblWeek'").html(labelData["alert4"]);

    jQuery("label[for='lblSCPopPwd'").html(labelData["pSettings"]);
    jQuery("label[for='lblLanguage'").html(labelData["lang"]);
    jQuery("label[for='lblDateFormat'").html(labelData["format"]);
    jQuery("label[for='lblPopOk'").html(labelData["ok"]);
    jQuery("label[for='lblPopClose'").html(labelData["cancel"]);
    jQuery("label[for='lblSettings'").html(labelData["settings"]);
    jQuery("label[for='lblThemes'").html(labelData["themes"]);
    jQuery("label[for='lblTheme1'").html(labelData["theme1"]);
    jQuery("label[for='lblTheme2'").html(labelData["theme2"]);
    jQuery("label[for='lblTheme3'").html(labelData["theme3"]);
    jQuery("label[for='lblTheme4'").html(labelData["theme4"]);
    jQuery("label[for='lblChangePassword'").html(labelData["changePassword"]);
    jQuery("label[for='lblConfigurators'").html(labelData["configurators"]);
    jQuery("label[for='lblPrintCustomize'").html(labelData["printCustamize"]);
    jQuery("label[for='lblBarcodeGenerator'").html(labelData["barCodeGenerator"]);
    jQuery("label[for='lblMasters'").html(labelData["masters"]);
    jQuery("label[for='lblTillType'").html(labelData["tillType"]);
    jQuery("label[for='lblTill'").html(labelData["till"]);
    jQuery("label[for='lblTillPermissions'").html(labelData["tillPermissions"]);
    jQuery("label[for='lblCustomer'").html(labelData["customer"]);
    jQuery("label[for='lblHomeDelHeader'").html(labelData["customer"]);
    jQuery("label[for='lblTenderType'").html(labelData["tenderType"]);
    jQuery("label[for='lblTransactions'").html(labelData["transactions"]);
    jQuery("label[for='lblInvoce'").html(labelData["invoice"]);
    jQuery("label[for='lblSalesReturn'").html(labelData["salesReturn"]);
    jQuery("label[for='lblPosting'").html(labelData["posting"]);
    jQuery("label[for='lblTillOpening'").html(labelData["tillOpening"]);
    jQuery("label[for='lblAccountPosting'").html(labelData["accountPosting"]);
    jQuery("label[for='lblReports'").html(labelData["reports"]);
    jQuery("label[for='lblInvoiceListing'").html(labelData["invoiceListing"]);
    jQuery("label[for='lblTillInvoiceListing'").html(labelData["tillInvoiceListing"]);
    jQuery("label[for='lblTenderTypeWiseCollectionDailySummary'").html(labelData["tenderTypeWiseCollectionDailySummary"]);
    jQuery("label[for='lblSalesReturns'").html(labelData["salesReturns"]);
    jQuery("label[for='lblTotalSales'").html(labelData["totSales"]);
    jQuery("label[for='lblTotCust'").html(labelData["totCust"]);
    jQuery("label[for='lblTotItems'").html(labelData["totalItems"]);
    jQuery("label[for='lblNewCust'").html(labelData["newCust"]);
    jQuery("label[for='lblWelToPOS'").html(labelData["welPOS"]);
    jQuery("label[for='lblStartANewSale'").html(labelData["startNewSale"]);
    jQuery("label[for='lblTillOpening'").html(labelData["tillOpening"]);
    jQuery("label[for='lblTodayCloseoutReport'").html(labelData["todatCloseRep"]);
    jQuery("label[for='lblTodaysSalesDetailsReport'").html(labelData["toDaySalDetRep"]);
    jQuery("label[for='lblTillClosing'").html(labelData["tillClosing"]);
    jQuery("label[for='lblRepTillClosing'").html(labelData["tillClosing"]);
    jQuery("label[for='lblLoyaltyPoints'").html(labelData["LoyaltyPoints"]);
    jQuery("label[for='lblTodayItemsSummaryReport'").html(labelData["toDayItemSumRep"]);
    jQuery("label[for='lblAccountPosting'").html(labelData["accountPosting"]);
    jQuery("label[for='lblSalesInfo'").html(labelData["salesInfo"]);
    jQuery("label[for='lblMonth'").html(labelData["month"]);
    jQuery("label[for='lblYear'").html(labelData["year"]);
    jQuery("label[for='lblSalesAnalysis'").html(labelData["salesAnalysis"]);
    jQuery("label[for='lblSales'").html(labelData["sales"]);
    jQuery("label[for='lblRefunds'").html(labelData["refunds"]);
    jQuery("label[for='lblRevenue'").html(labelData["revenue"]);
    jQuery("label[for='lblCost'").html(labelData["cost"]);
    jQuery("label[for='lblProfit'").html(labelData["profit"]);
    jQuery("label[for='lblProfitPercent'").html(labelData["profitPersentage"]);
    jQuery("label[for='lblTenderWiseSales'").html(labelData["tenderSales"]);
    jQuery("label[for='lblReceipt'").html(labelData["receipt"]);
    jQuery("label[for='lblVATReturns'").html(labelData["VATReturns"]);
    jQuery("label[for='lblItem'").html(labelData["item"]);
    jQuery("label[for='lblUserPermissions'").html(labelData["user"]);
    jQuery("label[for='lblPrintPole'").html(labelData["PrintPoleConfig"]);
    jQuery("label[for='lblCurrentInventory'").html(labelData["CurrentInventory"]);
    jQuery("label[for='lblLocationwiseAverageInvoiceValue'").html(labelData["LocationwiseAverageInvoiceValue"]);
     
    
   jQuery("label[for='lblPopOkOne'").html(labelData["ok"]);
   jQuery("label[for='lblPopCloseOne'").html(labelData["closed"]);  
   

    $('#hdnNAssignCountrTrans').val(labelData["alert1"]);
    $('#hdnNPermissionInvoice').val(labelData["alert2"]);
    $('#hdnInvalData').val(labelData["InvalidData"]);
    $('#hdnContactAdmin').val(labelData["contactAdmin"]);
    $('#hdnChPwdSaveSucc').val(labelData["saveSuccess"]);
    $('#hdnThameChange').val(labelData["alert3"]);
    $('#hdnNotAssignCountrToTrans').val(labelData["alert17"]);
    $('#hdnAccessDenied').val(labelData["alert1"]);
    jQuery("label[for='lblDispatch'").html(labelData["Dispatch"]);
    $('#hdnImageSave').val(labelData["ImageSave"]);
    $('#hdnSelImage').val(labelData["SelUserImage"]);
    jQuery("label[for='lblUpload'").html(labelData["upload"]);
    jQuery("label[for='lblSubcription'").html(labelData["Renewal"]);
    $('#hdnEntrCode').val(labelData["alert11"]);
    $('#hdnEntrName').val(labelData["alert24"]);
    jQuery("label[for='lblClose'").html(labelData["closed"]);
    jQuery("label[for='lblSave'").html(labelData["save"]);
    jQuery("label[for='lblPleasewait'").html(labelData["Pleasewait"]);

    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
    //jQuery("label[for=''").html(labelData[""]);
}
function dayCloserChecking() {
    window.localStorage.setItem('ctCheck', 1);
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetDayCloserChecking,
        data: { VD: VD1(), subOrgId: HashValues[1] },
        success: function (resp) {
            if (resp == 0) {
                window.open('CounterOpening.html', '_self', 'location=no');
            } else {
                navigator.notification.alert($('#hdnNotAssignCountrToTrans').val(), "", "neo ecr", "Ok");
            }
        }
    });
}

function ConvertDateWithFormatLocalRet(formates, dateText) {
    window.localStorage.setItem('ctCheck', 1);
    var hdnformat = formates;
    var splitc;
    if (hdnformat == 'd/m/Y' || hdnformat == 'm/d/Y' || hdnformat == 'Y/d/m' || hdnformat == 'Y/m/d' || hdnformat == 'M/d/Y' || hdnformat == 'd/M/Y') {
        splitc = '/';
    }
    else if (hdnformat == 'd-m-Y' || hdnformat == 'm-d-Y' || hdnformat == 'Y-d-m' || hdnformat == 'Y-m-d' || hdnformat == 'M-d-Y' || hdnformat == 'd-M-Y') {
        splitc = '-';
    }
    else if (hdnformat == 'd,m,Y' || hdnformat == 'm,d,Y' || hdnformat == 'Y,d,m' || hdnformat == 'Y,m,d' || hdnformat == 'M,d,Y' || hdnformat == 'd,M,Y') {

        splitc = ',';
    }
    else if (hdnformat == 'd m Y' || hdnformat == 'm d Y' || hdnformat == 'Y d m' || hdnformat == 'Y m d' || hdnformat == 'M d Y' || hdnformat == 'd M Y') {

        splitc = ' ';
    }
    else if (hdnformat == 'd.m.Y' || hdnformat == 'm.d.Y' || hdnformat == 'Y.d.m' || hdnformat == 'Y.m.d' || hdnformat == 'M.d.Y' || hdnformat == 'd.M.Y') {
        splitc = '.';
    };
    var sym = '-';
    if (dateText.indexOf('/') != -1)
        sym = '/';
    var vardatesplit = dateText.split(sym);
    var varM;
    var varMn;
    var varfformatS = hdnformat.split(splitc);
    if (varfformatS[0] == 'M' || varfformatS[1] == 'M') {

        if (vardatesplit[1] != null)
            varM = vardatesplit[1];
        if (vardatesplit[0] != null)
            varM = vardatesplit[0];

        switch (varM) {
            case 'Jan': varMn = '01';
                break;
            case 'Feb': varMn = '02';
                break;
            case 'Mar': varMn = '03';
                break;
            case 'Apr': varMn = '04';
                break;
            case 'May': varMn = '05';
                break;
            case 'Jun': varMn = '06';
                break;
            case 'Jul': varMn = '07';
                break;
            case 'Aug': varMn = '08';
                break;
            case 'Sep': varMn = '09';
                break;
            case 'Oct': varMn = '10';
                break;
            case 'Nov': varMn = '11';
                break;
            case 'Dec': varMn = '12';
                break;
        }

    }
    if (varfformatS[0] == 'd' && (varfformatS[1] == 'm' || varfformatS[1] == 'M') && varfformatS[2] == 'Y') {
        if (varfformatS[1] == 'M') {
            return (varMn + splitc + vardatesplit[1] + splitc + vardatesplit[0]);
        }
        else {
            return (vardatesplit[1] + splitc + vardatesplit[0] + splitc + vardatesplit[2]);
        }
    }
    else if ((varfformatS[0] == 'm' || varfformatS[0] == 'M') && varfformatS[1] == 'd' && varfformatS[2] == 'Y') {
        if (varfformatS[0] == 'M') {
            return (varMn + splitc + vardatesplit[2] + splitc + vardatesplit[0]);
        }
        else {
            return (vardatesplit[0] + splitc + vardatesplit[1] + splitc + vardatesplit[2]);
        }
    }
    else if (varfformatS[0] == 'Y' && varfformatS[1] == 'd' && varfformatS[2] == 'm') {
        return (vardatesplit[2] + splitc + vardatesplit[1] + splitc + vardatesplit[0]);
    }
    else if (varfformatS[0] == 'Y' && varfformatS[1] == 'm' && varfformatS[2] == 'd') {
        return (vardatesplit[2] + splitc + vardatesplit[0] + splitc + vardatesplit[1]);
    }
    else {
        return (vardatesplit[0] + splitc + vardatesplit[1] + splitc + vardatesplit[2]);
    }
}

//image/png
//512

function b64toBlob(b64Data) {
    // contentType = contentType || '';
    contentType = 'image/png';
    //sliceSize = sliceSize || 512;
    sliceSize = 512;
    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
    }

    var blob = new Blob(byteArrays, { type: contentType });
    // alert(blob);
    return blob;
}
function CROnMainSave() {
     
    window.localStorage.setItem('ctCheck', 1);
    // window.localStorage.setItem("DisplayQR", "0");
    // window.localStorage.setItem("Display", "2");
    var a = CheckValidation();
    if (a == false) {
        return false;
    }
    if ($('#CRtxtTendertrans').val() == '' || (isNaN($('#CRtxtTendertrans').val()))) {
      navigator.notification.alert($('#hdnPlzSelectTranType').val(), "", "neo ecr", "Ok");
    //   navigator.notification.alert("Please Select the Tender Type.", "", "neo ecr", "Ok");
    }
  
    $('#CRhdnAmount').val($('#CRtxtTendertrans').val());
    
        var BRANCHID = ''; var COMPANYID = '';
        var CURRENCYID = '';
        var HashValues = (window.localStorage.getItem("hashValues"));
        if (HashValues != null && HashValues != '') {
            var hashSplit = HashValues.split('~');
            CURRENCYID = hashSplit[16];
            COMPANYID = hashSplit[0];
            BRANCHID = hashSplit[1];
            CREATEDBY = hashSplit[14];

        }
        var CRSRECEIPTID = guid();
         var P20 = {
        }
         var P21 = {
             CASHINVID: 'CRSReceipt'
         }
        var P24 = {
        }
        P20["CRSRECEIPTID"] = CRSRECEIPTID;
        P20["CUSTOMERID"] = $('#CRhdnCustomerId').val();
        P20["COUNTERID"] = window.localStorage.getItem('COUNTERID');
        P20["RECEIVEDAMOUNT"] = parseFloat($('#CRtxtTendertrans').val());
        P20["BRANCHID"] = BRANCHID;
        P20["COMPANYID"] = COMPANYID;
        P20["CREATEDBY"] = CREATEDBY;
        P20["UPDATEDSITE"] = 'DBMS_REPUTIL.GLOBAL_NAME';
        P20["LANGID"] = 'en';
        objP20 = [];
        objP21 = [];
        objP20.push(P20);
        objP21.push(P21);
        objP24 = [];

          
            try {
              
                    P24 = {
                    }
                    P24["CRSRECEIPTID"] = CRSRECEIPTID
                    if ($('#CRhdnTenderType').val().toUpperCase() == 'CREDITCARD') {
                        P24["TENDERTYPE"] = 'Credit Card';
                    }
                    else if ($('#CRhdnTenderType').val().toUpperCase() == 'CASH') {
                        P24["TENDERTYPE"] = 'CASH';
                    }
                    if ($('#CRhdnTenderType').val().toUpperCase() == 'E-PAY') {
                        P24["TENDERTYPE"] = 'E-PAY';
                    }
                    P24["BANKNAME"] = '';
                    P24["CARDNAME"] = '';
                    P24["CARDTYPE"] = '';
                    P24["INSTRUMENTNUMBER"] = '';
                    P24["TOTAL_AMOUNT"] = parseFloat($('#CRtxtTendertrans').val());
                    P24["TENDERID"] = $('#CRhdnTenderType').val();
                    P24["COUNTERTYPE"] = window.localStorage.getItem('COUNTERTYPE');
                    P24["CURRENCYID"] = CURRENCYID;
                    P24["AMOUNT"] = parseFloat($('#CRtxtTendertrans').val());
                    P24["CLEARINGCHARGES"] = '';
                    P24["EXCHANGERATE"] = '1';
                    P24["TRANSTYPE"] = 'CRSREC';
                    P24["COUNTERID"] = window.localStorage.getItem('COUNTERID');
                    P24["SHIFTID"] = window.localStorage.getItem('SHIFTID');
                    P24["CHANGE"] = '0';
                    P24["UPDATEDSITE"] = 'DBMS_REPUTIL.GLOBAL_NAME';
                    P24["LANGID"] = 'en';
                    P24["BRANCHID"] = BRANCHID;
                    P24["COMPANYID"] = COMPANYID;
                    P24["CREATEDBY"] = CREATEDBY;
                    objP24.push(P24);
               
            }
            catch (e) {

            }
        var InvoiceParams = new Object();
        InvoiceParams.P20 = JSON.stringify(objP20);
        InvoiceParams.P21 = JSON.stringify(objP21);
        InvoiceParams.P24 = JSON.stringify(objP24);
        InvoiceParams.VD = VD1();
        var ComplexObject = new Object();
        ComplexObject.Invoice = InvoiceParams;
        var data = JSON.stringify(ComplexObject);
        if (window.localStorage.getItem('network') == '1') {
            $.ajax({
                type: 'POST',
                url: CashierLite.Settings.InvoiceSave,
                data: data,
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                success: function (resp) {
                    
                    var result = resp.InvoiceSaveResult.split(',');
                    // window.localStorage.setItem("DisplayDLTemp", "1");
                    if (result[0] == "100000") {
                            // $('#CRlblPPFinalInvoiceNo')[0].innerText = $('#CRhdnInvNopopup').val() + ' : ' + result[1];
                            // $('#CRlblPPLamtRec')[0].innerText = $('#CRhdnAmount').val();
                            clearRSCRDData();
                             navigator.notification.alert($('#hdnRecSaveSucc').val(), "", "neo ecr", "Ok");
                    }
                   if (result[0] == "100002") {
                    clearRSCRDData();
                            // $('#CRlblPPFinalInvoiceNo')[0].innerText = $('#CRhdnInvNopopup').val() + ' : ' + result[1];
                            // $('#CRlblPPLamtRec')[0].innerText = $('#CRhdnAmount').val();
                             navigator.notification.alert("Internal Error", "", "neo ecr", "Ok");
                    }
                    $('#CRtxtTendertransref').val('')
                },
                error: function (e) {

                }
            });
        }
        else {

        }
  
    }
function CROnSave() { 
    var a = CheckValidation();
    if (a == false) {
        return false;
    }
    if ($('#CRtxtTendertrans').val() == '' || (isNaN($('#CRtxtTendertrans').val()))) {

      navigator.notification.alert("Please Select the Tender Type.", "", "neo ecr", "Ok");
    } 
    if(window.localStorage.getItem('CRSelectMode').toUpperCase()=='CASH' || window.localStorage.getItem('CRSelectMode').toUpperCase()=='E-PAY')
    {
        CROnMainSave();
    }
    else  if(window.localStorage.getItem('CRSelectMode').toUpperCase()=='CREDITCARD')
    {
    
        if(window.localStorage.getItem('CardTest')=='0')
        { 
            if( window.localStorage.getItem('PaymentTerminalName')!=null)
            {     
                CRSaleTransaction(parseFloat($('#CRtxtAmount').val()).toFixed(2));
            }
            else
            {                
                testSale();
                $('#myModalBTPaymentTerminal').modal('show');
            }
      }
      else
      {
        CROnMainSave();
      }
    } 
}

function CRSaleTransaction(amt) {
 
    var options = { dimBackground: true };
    SpinnerPlugin.activityStart("Please wait...", options);
    var bt = cordova.plugins.Apiplugin.gettriggerSaleTransaction; 
  
    bt = bt(CRsuPayNew, faPayNew, "sale",amt, "01", "9876543210", "abc@123.com", "userd", "deviceid", "RRN", "Authcode", "MaskedPAN", "Additionaldata");
}
function CRsuPayNew(resl)
{ 
  SpinnerPlugin.activityStop();
    // need to remove
  var mainRS="";
  var reslScan=resl.split('~');
  if(reslScan[1]=='000')
  {
    mainRS="Transaction Sucess";
  }
  else
  {
    mainRS="Transaction Faild";
  }
     if(mainRS!="Transaction Faild")
     {
        CROnMainSave();
     }
     else
     { 
        navigator.notification.alert($('#hdnPaymsgtwo').val(), "", "neo ecr", "Ok");  
        if (confirm('Do you want continue Offline Card Transaction'))
        { 
            CROnMainSave();
        } 
     }
}
try {
$("#CRtxtCustomer").autocomplete({
    source: function (request, response) {
        
            var HashVal = (window.localStorage.getItem("hashValues"));
            var HashValues;
            if (HashVal != null && HashVal != '') {
                HashValues = HashVal.split('~');
            }
        $.ajax({
               url: CashierLite.Settings.GetCustomerSummaryGrid,
               data: "OrgId=" + HashValues[15] + "&VD=" + VD1(),
            dataType: "json",
            type: "Get",
            //contentType: "application/json; charset=utf-8",
            success: function (data) {
                window.localStorage.setItem('ctCheck', 1);
                response($.map(data, function (item) {
                    return {
                        label: item.PHONENO + '/' + item.CUSTOMER_NAME,
                        val: item.CUSTOMER_ID,
                    };
                }));
            },
            error: function (response) {
            },
            failure: function (response) {
            }
        });

    },
    change: function (e, i) {
        if (i.item) {

        }
        else {
            $('#CRtxtCustomer').val('');
            $('#CRhdnCustomerId').val('');
            $('#CRtxtOutstaingBal').val("0.00");

        }
    },
    select: function (e, i) {
        $('#CRhdnCustomerId').val(i.item.val);
        $('#CRtxtCustomer').val(i.item.label);
        GetOutStandingAmount($('#CRhdnCustomerId').val());

    },
    minLength: 1
});
}
catch (a) {

}
       
function GetOutStandingAmount(CustomerId) {
    window.localStorage.setItem('ctCheck', 1);
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.GetOutStandingAmount,
        data: "CustomerId=" + CustomerId + "&VD=" + VD(),
        success: function (resp) {
            if (resp != "" ) {
                $('#CRtxtOutstaingBal').val(parseFloat(resp).toFixed(2));
            }
            else
            {
                $('#CRtxtOutstaingBal').val("0.00");
            }
        },
        error: function (e) {
            $('#CRtxtOutstaingBal').val("0.00");
        }
    });
}

function CheckValidation() {
    if ($('#CRhdnCustomerId').val() == "") {
        navigator.notification.alert("Please Select the Customer", "", "neo ecr", "Ok");
        return false;
    }
    else if ($('#CRtxtAmount').val() == "" || parseFloat ($('#CRtxtAmount').val())== 0) {
      
     navigator.notification.alert("Please Enter Received Amount.", "", "neo ecr", "Ok");
     return false;
   }
   return true;
}
function clearRSCRDData()
{
    $('#CRtxtCustomer').val('');
    $('#CRtxtAmount').val('0');
    $('#CRtxtOutstaingBal').val('0');
    $('#CRhdnCustomerId').val('');
}
function CRontender(TenderID, TenderType, REFERENCE, SNO) {

    window.localStorage.setItem('ctCheck', 1);
    window.localStorage.setItem("DisplayDLTemp0", "1");
    window.localStorage.setItem("Display", "2");
    window.localStorage.setItem("DisplayQR", "0");
    var a = CheckValidation();
    if (a == false) {
        return false;
    }
    window.localStorage.setItem('CRSelectMode','0');
    if (TenderID == 'Cash' || TenderID == 'CreditCard' && a) {
        window.localStorage.setItem('CRSelectMode',TenderID);
        $('#CRtxtTendertrans').prop('disabled', true);
        $('#CRtxtTendertrans').val(parseFloat($('#CRtxtAmount').val()).toFixed(2));
        $('#CRhdnTenderType').val(TenderID);
    }
    if (TenderID == 'E-Pay' && a)  {
        window.localStorage.setItem('CRSelectMode',TenderID);
        $('#CRtxtTendertrans').prop('disabled', true);
        $('#CRtxtTendertrans').val(parseFloat($('#CRtxtAmount').val()).toFixed(2));
        $('#CRhdnTenderType').val(TenderID);
        window.localStorage.setItem('CREPAY','1');
        $("#img_QRCodePP").attr("src", "images/sampleQR.jpeg");
        $('#divWalletPayParams').attr("style", "display:none"); 
        $('#txtWalletNamePPSub').val('');
        $('#txtWalletBalancePPSub').val('');
        $('#txtWalletAmtPPSub').val('');
        $('#txtWalletRefPPSub').val('');
        $('#txt_amount_Ref').val('');
        $('#txt_amount_UPI').val('');
        $('#txt_balance_UPI').val('');
        $('#TXT_upiid').val('');
        $('#hdnWalletCheckVerify').val('0');
        $('#hdn_TYPEWallet').val('0');
        $('#home').attr('style', 'display:none');
        $('#wallet-pay').attr('style', 'display:none');
        $('#footer-pay').attr('style', 'display:none');
        $('#UPI-pay').attr('style', 'display:none'); 
        $('#div_form-section-UPI_UPI').attr('style', 'display:block');
        $('#myModal-payment').modal();
        var SubOrgId = '';
        var HashValues = (window.localStorage.getItem("hashValues"));
        if (HashValues != null && HashValues != '') {
            var hashSplit = HashValues.split('~'); 
            SubOrgId = hashSplit[1]; 
        } 
        GetConfigDataINaPPfUN_base64(SubOrgId);
    }
}

