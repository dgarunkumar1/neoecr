﻿var graphData;
function VD() {
    window.localStorage.setItem('ctCheck', 1);
    var vd_ = window.localStorage.getItem("UUID");;
    //var vdSplit_ = window.location.pathname.split('/');
    //if (vdSplit_.length > 2) {
    //    vd_ = vdSplit_[1];
    //}
    return vd_;
}

function theamLoad() {
    if (window.localStorage.getItem("Theam") != null)
        $('head').append('<link rel="stylesheet" href=' + window.localStorage.getItem("Theam") + ' type="text/css" />');
}
if (window.localStorage.getItem("Theam").split('-')[3] == '1.css') {
    $('#IMG1').attr("src", "images/mmlogo-inner2.png");
} else {
    $('#IMG1').attr("src", "images/mmlogo-inner1.png");
}

function InvMail() {
    $('#txtBccMailId').val('');
    $('#txtBody').val('');


    window.localStorage.setItem('ctCheck', 1);
    $('#ModalINVMail').modal('show');
    //   tenderAddpartial(TenderID, 1, '');
    $('#txtMailId').val('');
    //  $('#FileFormat').val('0');
    $('#txtMailId').focus();
    $('#FileFormat').attr("readonly", "true");
}
function SendINVMail(MailId, BccMailId, Format, Body) {
    window.localStorage.setItem('ctCheck', 1);
    if ($('#txtMailId').val() == "") {
        navigator.notification.alert($('#hdnEnterMail').val(), "", "neo ecr", "Ok");
    }
    else {

        var options = { dimBackground: true };
        SpinnerPlugin.activityStart("Please wait...", options);
        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.SendMailArabic,
            data: "language=" + HashValues[7] + "&PageId=" + window.localStorage.getItem('RPPAGECODE') + "&subOrg=" + $('#hdnBranchId').val() + "&frmDate=" + window.localStorage.getItem('RPfrmDate') + "&toDate=" + window.localStorage.getItem('RPtoDate') + ' 23:59:59' + "&DateFormate=" + $('#hdnDateFromate').val() + "&VD=" + VD() + "&MailId=" + MailId + "&Format=" + Format + "&BCMailId=" + BccMailId + "&Body=" + Body + "",
            success: function (resp) {
                SpinnerPlugin.activityStop();
                if (resp == '1000') {
                    navigator.notification.alert($('#hdnMailSuccess').val(), "", "neo ecr", "Ok");
                    $('#ModalINVMail').modal('hide');
                }
                else {
                    navigator.notification.alert($('#hdnMailnotSuccess').val(), "", "neo ecr", "Ok");
                    $('#ModalINVMail').modal('hide');
                }
            },
            error: function (e) {
                SpinnerPlugin.activityStop();
                navigator.notification.alert($('#hdnInvalidData').val(), "", "neo ecr", "Ok");
                $('#ModalINVMail').modal('hide');
            }
        });
    }
}

function loadVRRGrid() {
    window.localStorage.setItem('ctCheck', 1);
    $('#li-Excel i').removeAttr('style', 'color:#919598 !important');
    $('#li-Excel').attr('onclick', 'GetDowloadFile()');
    $('#li-Refresh i').removeAttr('style', 'color:#919598 !important');
    $('#li-Refresh').attr('onclick', 'PageISalestaxRPBind()');
    $('#li_expand i').removeAttr('style', 'color:#919598 !important');
    $('#li_expand').attr('onclick', 'loadGridExtend()');
    $('#li_compress i').removeAttr('style', 'color:#919598 !important');
    $('#li_compress').attr('onclick', 'loadGridCompress()');
    $('#li_Print i').removeAttr('style', 'color:#919598 !important');
    $('#li_Print').attr('onclick', 'VRRPrint()');

    $('#txt_PRPPageIndex').val('');

    var elemGrid = document.getElementById("div_grid");
    elemGrid.style.display = "block"; 

    var elemGraph = document.getElementById("div_graph1");
    elemGraph.style.display = "none";
}
function PageISalestaxRPBind() {
    window.localStorage.setItem('ctCheck', 1);
    var HashVal = (window.localStorage.getItem("hashValues"));

    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');
        $('#hdnDateFromate').val(HashValues[2])
        if (HashValues[7] == 'ar-SA') {
            $('html,body').append('<link rel="stylesheet" href="Styles/css/bootstrap-rtl.css" type="text/css" />');
            $('html,body').append('<link rel="stylesheet" href="Styles/css/bootstrap-flipped.css" type="text/css" />');
            $('head').append('<script src="scripts/bootstrap-dialog-SA.min.js"></script>');
        }

    }

    theamLoad();
    GetSubOrgData();
    var btnExp = document.getElementById("btnExp");
    btnExp.style.display = "block";
    if (window.localStorage.getItem('RPPAGECODE') == 'POSTVRR') {
        var SubOrgId = '';
        var HashVal = (window.localStorage.getItem("hashValues"));
        var HashValues;
        if (HashVal != null && HashVal != '') {
            HashValues = HashVal.split('~');

            SubOrgId = HashValues[1];
           $('#hdnBranchId').val(HashValues[1]);
            $('#hdnDateFromate').val(HashValues[2]);
            $('#hdnPaging').val(HashValues[19]);
        }

        //    GetTaxDetails(window.localStorage.getItem('RPfrmDate'), window.localStorage.getItem('RPtoDate'), "", "");

        var FromDate = (window.localStorage.getItem("RPfrmDate"));
        var ToDate = (window.localStorage.getItem("RPtoDate")) + ' 23:59:59';
        var RefNo = (window.localStorage.getItem("RPrefNo"));
        var PageCode = (window.localStorage.getItem("RPPAGECODE"));
        var grid = "";
        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.GetReport,
            data: "PageId=" + PageCode + "&subOrg=" + SubOrgId + "&frmDate=" + FromDate + "&toDate=" + ToDate + "&refNo=" + RefNo + "&DateFormate=" + $('#hdnDateFromate').val() + "&VD=" + VD(),
            success: function (resp) {
                graphData = resp;
                getResources(HashValues[7], "POSReport", resp, 1);

                //arrangeDailyCollectionData(resp);
            }
        });

        //GetTaxDetails(window.localStorage.getItem('RPfrmDate'), window.localStorage.getItem('RPtoDate'), "", "");


    }
}

function getResources(langCode, pageCode, data, sts) {
    window.localStorage.setItem('ctCheck', 1);
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.getResource,
        data: "langCode=" + langCode + "&pageCode=" + pageCode,
        success: function (resp) {
            setLabels(resp, data, sts);
        }
    });
}
function getLoadExdata()
{
//document.getElementById("FileFormat").options.length = 0;
$("#FileFormat").append($("<option></option>").val("0").html("----select----"));
$("#FileFormat").append($("<option></option>").val("1").html($('#hdnMExcel').val()));
 $("#FileFormat option[value='1']").attr('selected','selected');
}
function setLabels(labelData, data, sts) {
    window.localStorage.setItem('ctCheck', 1);
    jQuery("label[for='lblLocationwiseProfitAnalysis'").html(labelData["LocwiseProfitAnlys"]);
    jQuery("label[for='lblItemCategoryProfitAnalysis'").html(labelData["ItemCatgPrftAnlys"]);
    jQuery("label[for='lblTitle'").html(labelData['VATReturns']);
    jQuery("label[for='lblVATReturns'").html(labelData["VATReturns"]);
    jQuery("label[for='lbl_page'").html(labelData["page"]);
    jQuery("label[for='lblMail'").html(labelData["Mail"]);
    jQuery("label[for='ToMail'").html(labelData["POSTo"]);
    jQuery("label[for='Bcc'").html(labelData["BCC"]);
    jQuery("label[for='lblFormat'").html(labelData["format"]);
    jQuery("label[for='lblBody'").html(labelData["body"]);
    jQuery("label[for='lblsend'").html(labelData["Send"]);
    jQuery("label[for='lblClose'").html(labelData["Close"]);
    $('#hdnMExcel').val(labelData["Excel"]);
    //$('#hdnInvoiceListingReport').val(labelData['invoiceListing']);
    $('#hdnChPwdSaveSucc').val(labelData["saveSuccess"]);
    $('#hdnThameChange').val(labelData["alert1"]);
    $('#hdnVATRForm').val(labelData["VatReturnForm"]);
    $('#hdnCompany').val(labelData["company"]);
    $('#hdnAddress').val(labelData["addr1"]);
    $('#hdnTaxIdenfn').val(labelData['TaxIdentification']);
    $('#hdnTaxReturnPeriod').val(labelData['TReturnPeriod']);
    $('#hdnAmount').val(labelData["amount"]);
    $('#hdnVatAmount').val(labelData["VATAmount"]);
    $('#hdnAdjustmnt').val(labelData["adjust"]);
    $('#hdnSalesTo').val(labelData["POSTo"]);

    $('#hdnSales').val(labelData["alert4"]);
    $('#hdnVATonPur').val(labelData["VPurchase"]);
    $('#hdnVATonSales').val(labelData["VSales"]);
    $('#hdnTSales').val(labelData["totSales"]);
    $('#hdnTPurchases').val(labelData["TotPurchases"]);
    $('#hdnNetVATdue').val(labelData["NetVATdue"]);
    $('#hdnRefundinfo').val(labelData["Rfundinfo"]);
    $('#hdnNote').val(labelData["note"]);
    $('#hdnDeclaration').val(labelData["Declratn"]);
    $('#hdntermCondtn').val(labelData["term"] + ' & ' + labelData["conditn"]);
    $('#hdnEnterMail').val(labelData["enterMail"]);
    $('#hdnMailSuccess').val(labelData["MailSuccess"]);
    $('#hdnMailnotSuccess').val(labelData["MailnotSuccess"]);
    $('#hdnInvalidData').val(labelData["InvalidData"]);

    $('#hdnVS1').val(labelData["VS1"]);
    $('#hdnVS20').val(labelData["VS20"]);
    $('#hdnVS21').val(labelData["VS21"]);
    $('#hdnVS3').val(labelData["VS3"]);
    $('#hdnVS4').val(labelData["VS4"]);
    $('#hdnVS5').val(labelData["VS5"]);
    $('#hdnVP1').val(labelData["VP1"]);
    $('#hdnVP2').val(labelData["VP2"]);
    $('#hdnVP30').val(labelData["VP30"]);
    $('#hdnVP31').val(labelData["VP31"]);
    $('#hdnVP4').val(labelData["VP4"]);
    $('#hdnVP5').val(labelData["VP5"]);
    $('#hdnVP6').val(labelData["VP6"]);
    $('#hdnVP70').val(labelData["VP70"]);
    $('#hdnVP71').val(labelData["VP71"]);
    $('#hdnVP80').val(labelData["VP80"]);
    $('#hdnVP81').val(labelData["VP81"]);
    arrangeDailyCollectionData(data);
getLoadExdata();
    if (sts == 2) {
        var $el = $('#tbldiv tbody>tr.tt-hide');
        $el.removeClass("tt-hide");

        for (var i = 0; i < $el.length; i++) {
            $('#root_' + i + '').html("-");
        }
        $('#DivPager').css("display", "none");
        if ($('#hdnScreenmode').val() == "print") {
            var divContents = document.getElementById("div_grid").innerHTML;
            var printWindow = window.open('', '', 'height=200,width=450');
            printWindow.document.write('<html><head><title>VAT Returns</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(divContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
        }
        if ($('#hdnScreenmode').val() == "excel") {
            var data_type = 'data:application/vnd.ms-excel';
            var table_div = document.getElementById('tbldiv');
            var table_html = table_div.outerHTML.replace(/ /g, '%20');
            var a = document.createElement('a');
            a.href = data_type + ', ' + table_html;
            a.download = 'VAT-Returns' + '.xls';
            a.click();
        }
    }
    if (window.localStorage.getItem('RPfrmDate') != null) {
        $('#INVLISlblDate').text('(' + ConvertDateAndReturnDate($('#hdnDateFromate').val(), window.localStorage.getItem('RPfrmDate')) + ' ' + $('#hdnSalesTo').val() + ' ' + ConvertDateAndReturnDate($('#hdnDateFromate').val(), window.localStorage.getItem('RPtoDate')) + ')');
    }
}

var FsubOrgName="";
function GetSubOrgData() {
    var OBSCode = (window.localStorage.getItem("hashValues"));
    if (OBSCode != null && OBSCode != '') {
        var OBS = OBSCode.split('~');
        var ss = OBS[0];
        var vd_ = window.localStorage.getItem("UUID");
        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.GetSubOrgData,
            data: { OBSCode: ss, VD: vd_ },
            success: function (resp) {
                var OrgData = resp;
                if (OrgData[0].OrgCode.split('/').length > 2) {
                   FsubOrgName=OrgData[0].OrgCode.split('/')[0] + '/' + OrgData[0].OrgCode.split('/')[1];
                }
                else {

                    FsubOrgName=OrgData[0].OrgCode.split('/')[1];
                }
            },
            error: function (e) {
            }
        });
    }
}
function arrangeDailyCollectionData(resp) {
    var HashVal = (window.localStorage.getItem("hashValues"));
    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');
    }
  if (HashValues[7] == 'ar-SA') {
                        jQuery("label[for='lbl_invPageOf'").html(" 1  من 1");
                        }
                        else
                       {
                        jQuery("label[for='lbl_invPageOf'").html(" 1 of 1");
                       }
    var fromDate = (window.localStorage.getItem("RPfrmDate"));
    var toDate = (window.localStorage.getItem("RPtoDate"));
    ConvertDateWithFormat($('#hdnDateFromate').val(), fromDate, "fromDate")
    ConvertDateWithFormat($('#hdnDateFromate').val(), fromDate, "toDate")
    var frmtoDate=fromDate +' '+ $('#hdnSalesTo').val() +' '+'<div id="taxRtl">'+toDate+'</div>';
    graphData = resp;
    var result = resp;
    var Primaryid = "";
    $('#tbldiv thead tr').remove();
    $('#tbldiv tbody tr').remove();
    $('#tbldiv thead').append("<tr ><th  rowspan=2 colspan=7  style='text-align:center' ><br>&nbsp<br><b>" + $('#hdnVATRForm').val() + "</b><br>&nbsp</th></tr></thead>");
    //  $('#tbldiv').append("<tbody>");
    var cc_ = ''; var totSAmnt = "0.00"; var totSADJ = "0.00"; var totSVat = "0.00";
    totPAmnt = "0.00"; var totPADJ = "0.00"; var totPVat = "0.00";
    cc_ = cc_ + '<tr><td  colspan=7><table class="table table-bordered" id="tblVatAddress" style="width:79%"><tr><td colspan=2 style="width:501px;">' + $('#hdnCompany').val() + '</td><td >' + FsubOrgName + '</td></tr>';
    cc_ = cc_ + '<tr><td colspan=2 style="width:501px;">' + $('#hdnAddress').val() + '</td><td>' + HashValues[26] + '</td></tr>';
    cc_ = cc_ + '<tr><td colspan=2 style="width:501px;">' + $('#hdnTaxIdenfn').val() + '</td><td>' + HashValues[27] + '</td></tr>';
    //cc_ = cc_ + '<tr><td colspan=2 style="width:501px;">' + $('#hdnTaxReturnPeriod').val() + '</td><td >' + fromDate +' '+ $('#hdnSalesTo').val() +' '+ toDate + '</td></tr></table></td></tr>'; //+ $('#INVLISlblDate')[0].innerText.replace('VAT Returns(', '').replace(')', '') + '</td></tr></table></td></tr>';
    //cc_ = cc_ + '<tr><td colspan=2 style="width:501px;">' + $('#hdnTaxReturnPeriod').val() + '</td><td >'+frmtoDate+'</td></tr></table></td></tr>'; //+ $('#INVLISlblDate')[0].innerText.replace('VAT Returns(', '').replace(')', '') + '</td></tr></table></td></tr>';
      cc_ = cc_ + '<tr><td colspan=2 style="width:501px;">' + $('#hdnTaxReturnPeriod').val() + '</td><td >' + ConvertDateAndReturnDate($('#hdnDateFromate').val(), window.localStorage.getItem('RPfrmDate')) + " "+$('#hdnSalesTo').val()+" " + ConvertDateAndReturnDate($('#hdnDateFromate').val(), window.localStorage.getItem('RPtoDate')) + '</td></tr></table></td></tr>'; //+ $('#INVLISlblDate')[0].innerText.replace('VAT Returns(', '').replace(')', '') + '</td></tr></table></td></tr>';
    cc_ = cc_ + '<tr><td colspan=7>&nbsp</td></tr>';
    cc_ = cc_ + "<tr><th rowspan=7 style='width:101px;'><input type=text value='" + $('#hdnVATonSales').val() + "' style='margin: 66px 0px 0px -6px;-webkit-transform:rotate(-90deg);-moz-transform:rotate(-90deg)' /></th><th></th><th>" + $('#hdnAmount').val() + " (SAR)</th><th>" + $('#hdnAdjustmnt').val() + " (SAR)</th><th>" + $('#hdnVatAmount').val() + " (SAR)</th></tr>";
    for (var i = 0; i < result.length; i++) {
        if (result[i].C0 == '1-Standard rate Sale') {
            cc_ = cc_ + '<tr><td>1 - ' + $('#hdnVS1').val() + ' </td><td id="ColAlign">' + parseFloat(result[i].C1).toFixed(2) + '</td><td id="ColAlign">' + parseFloat(result[i].C2).toFixed(2) + '</td><td id="ColAlign">' + parseFloat(result[i].C3).toFixed(2) + '</td></tr>';
            cc_ = cc_ + '<tr><td>2 - ' + $('#hdnVS20').val() + '</br> ' + $('#hdnVS21').val() + ' </td><td id="ColAlign">0.00</td><td id="ColAlign">0.00</td><td id="ColAlign"></td></tr>';
            totSAmnt = parseFloat(totSAmnt) + parseFloat(result[i].C1);
            totSADJ = parseFloat(totSADJ) + parseFloat(result[i].C2);
            totSVat = parseFloat(totSVat) + parseFloat(result[i].C3);

        }
        if (result[i].C0 == '3-Zero rated Domestic Sale') {
            cc_ = cc_ + '<tr><td>3 - ' + $('#hdnVS3').val() + ' </td><td id="ColAlign">' + parseFloat(result[i].C1).toFixed(2) + '</td><td id="ColAlign">' + parseFloat(result[i].C2).toFixed(2) + '</td><td id="ColAlign">0.00</td></tr>';
            cc_ = cc_ + '<tr><td>4 - ' + $('#hdnVS4').val() + ' </td><td id="ColAlign">0.00</td><td id="ColAlign">0.00</td><td id="ColAlign"></td></tr>';
            totSAmnt = parseFloat(totSAmnt) + parseFloat(result[i].C1);
            totSADJ = parseFloat(totSADJ) + parseFloat(result[i].C2);
            totSVat = parseFloat(totSVat) + parseFloat(result[i].C3);
        }
        if (result[i].C0 == '5-Exempted Sale') {
            cc_ = cc_ + '<tr><td>5 - ' + $('#hdnVS5').val() + ' </td><td id="ColAlign">' + parseFloat(result[i].C1).toFixed(2) + '</td><td id="ColAlign">' + parseFloat(result[i].C2).toFixed(2) + '</td><td id="ColAlign">0.00</td></tr>';
            totSAmnt = parseFloat(totSAmnt) + parseFloat(result[i].C1);
            totSADJ = parseFloat(totSADJ) + parseFloat(result[i].C2);
            totSVat = parseFloat(totSVat) + parseFloat(result[i].C3);
            cc_ = cc_ + '<tr><td><b>6 - ' + $('#hdnTSales').val() + ' </b></td><td id="ColAlign"><b> ' + parseFloat(totSAmnt).toFixed(2) + '</b></td><td id="ColAlign"><b>' + parseFloat(totSADJ).toFixed(2) + '</b></td></b><td id="ColAlign"><b>' + parseFloat(totSVat).toFixed(2) + '</b></td></tr>';
            cc_ = cc_ + '<tr><td colspan=7>&nbsp</td></tr>';

        }
        if (result[i].C0 == '7-Standard rate Domestic Purchases') {
            cc_ = cc_ + '<tr><td rowspan=6 style=width:143px;> <input type=text value="' + $('#hdnVATonPur').val() + '" style="font-weight: bold;margin: 65px 10px 27px -8px; -webkit-transform:rotate(-90deg);-moz-transform:rotate(-90deg)" /></td><td>7 - ' + $('#hdnVP1').val() + ' </td><td id="ColAlign">' + parseFloat(result[i].C1).toFixed(2) + '</td><td id="ColAlign">' + parseFloat(result[i].C2).toFixed(2) + '</td><td id="ColAlign">' + parseFloat(result[i].C3).toFixed(2) + '</td></tr>';
            cc_ = cc_ + '<tr><td>8 - ' + $('#hdnVP2').val() + ' </td><td id="ColAlign">0.00</td><td id="ColAlign">0.00</td></tr>';
            cc_ = cc_ + '<tr><td>9 - ' + $('#hdnVP30').val() + '</br> ' + $('#hdnVP31').val() + ' </td><td id="ColAlign">0.00</td><td id="ColAlign">0.00</td><td id="ColAlign">0.00</td></tr>';
            totPAmnt = parseFloat(totPAmnt) + parseFloat(result[i].C1);
            totPADJ = parseFloat(totPADJ) + parseFloat(result[i].C2);
            totPVat = parseFloat(totPVat) + parseFloat(result[i].C3);
        }
        if (result[i].C0 == '10-Zero rated Purchases') {
            cc_ = cc_ + '<tr><td>10 - ' + $('#hdnVP4').val() + ' </td><td id="ColAlign">' + parseFloat(result[i].C1).toFixed(2) + '</td><td id="ColAlign">' + parseFloat(result[i].C2).toFixed(2) + '</td><td id="ColAlign">' + parseFloat(result[i].C3).toFixed(2) + '</td></tr>';
            totPAmnt = parseFloat(totPAmnt) + parseFloat(result[i].C1);
            totPADJ = parseFloat(totPADJ) + parseFloat(result[i].C2);
            totPVat = parseFloat(totPVat) + parseFloat(result[i].C3);
        }
        if (result[i].C0 == '11-Exempted Purchases') {
            cc_ = cc_ + '<tr><td>11 - ' + $('#hdnVP5').val() + ' </td><td id="ColAlign">' + parseFloat(result[i].C1).toFixed(2) + '</td><td id="ColAlign">' + parseFloat(result[i].C2).toFixed(2) + '</td><td id="ColAlign">' + parseFloat(result[i].C3).toFixed(2) + '</td></tr>';
            totPAmnt = parseFloat(totPAmnt) + parseFloat(result[i].C1);
            totPADJ = parseFloat(totPADJ) + parseFloat(result[i].C2);
            totPVat = parseFloat(totPVat) + parseFloat(result[i].C3);
        }
    }
    cc_ = cc_ + '<tr><td ><b>12 - ' + $('#hdnTPurchases').val() + ' </b></td><td id="ColAlign"><b>' + parseFloat(totPAmnt).toFixed(2) + '</b></td><td id="ColAlign"><b>' + parseFloat(totPADJ).toFixed(2) + '</b></td><td id="ColAlign"><b>' + parseFloat(totPVat).toFixed(2) + '</b></td></tr>';
    var TotalVat = 0.00;
    if ((parseFloat(totSVat) - parseFloat(totPVat)) < 0) {
        TotalVat = 0.00; 
    }
    else {
        TotalVat = parseFloat(totSVat) - parseFloat(totPVat);
    }
    
 /*   cc_ = cc_ + '<tr><td></td><td colspan=3>13 - ' + $('#hdnVP6').val() + ' </td><td id="ColAlign">' + parseFloat(TotalVat).toFixed(2) + '</td></tr>';
    cc_ = cc_ + '<tr><td></td><td colspan=3>14 - ' + $('#hdnVP70').val() + '</br> ' + $('#hdnVP71').val() + ' </td><td id="ColAlign">0.00</td></tr>';
    cc_ = cc_ + '<tr><td></td><td colspan=3>15 - ' + $('#hdnVP80').val() + ' </br>' + $('#hdnVP81').val() + ' </td><td id="ColAlign">0.00</td></tr>';
    if (HashValues[7] == 'ar-SA') 
        cc_ = cc_ + '<tr><td></td><td colspan=3 style="font-weight:bold">16 - ' + $('#hdnNetVATdue').val() + ' </td><td style="direction:ltr;font-weight:bold" id="ColAlign" >' + parseFloat(parseFloat(totSVat) - parseFloat(totPVat)).toFixed(2) + '</td></tr>';
    else
        cc_ = cc_ + '<tr><td></td><td colspan=3 style="font-weight:bold">16 - ' + $('#hdnNetVATdue').val() + ' </td><td style="font-weight:bold" id="ColAlign">' + parseFloat(parseFloat(totSVat) - parseFloat(totPVat)).toFixed(2) + '</td></tr>';*/
   /* var TotalVatDue = 0.00;
    if ((parseFloat(totSVat) - parseFloat(totPVat)) < 0) {
        TotalVatDue = parseFloat(totSVat) - parseFloat(totPVat);

    }
    else {
        TotalVatDue = 0.00;
    }
    // 254 end  */
    cc_ = cc_ + '<tr><td></td><td colspan=3>13 - ' + $('#hdnVP6').val() + ' </td><td id="ColAlign">' + parseFloat(TotalVat).toFixed(2) + '</td></tr>';
    cc_ = cc_ + '<tr><td></td><td colspan=3>14 - ' + $('#hdnVP70').val() + '</br> ' + $('#hdnVP71').val() + ' </td><td id="ColAlign">0.00</td></tr>';
    cc_ = cc_ + '<tr><td></td><td colspan=3>15 - ' + $('#hdnVP80').val() + ' </br>' + $('#hdnVP81').val() + ' </td><td id="ColAlign">0.00</td></tr>';
    if (HashValues[7] == 'ar-SA')
        cc_ = cc_ + '<tr><td></td><td colspan=3 style="font-weight:bold">16 - ' + $('#hdnNetVATdue').val() + ' </td><td style="direction:ltr;font-weight:bold" id="ColAlign" >' + parseFloat(parseFloat(totSVat) - parseFloat(totPVat)).toFixed(2) + '</td></tr>'; // 254
    else
        cc_ = cc_ + '<tr><td></td><td colspan=3 style="font-weight:bold">16 - ' + $('#hdnNetVATdue').val() + ' </td><td style="font-weight:bold" id="ColAlign">' + parseFloat(parseFloat(totSVat) - parseFloat(totPVat)).toFixed(2) + '</td></tr>'; // 254
    cc_ = cc_ + '<tr><td colspan=7>&nbsp</td></tr>';
    /*cc_ = cc_ + '<tr><td colspan=7><b>' + $('#hdnRefundinfo').val() + ': </b></td></tr>';
    cc_ = cc_ + '<tr><td colspan=7>Your return form information indicate that you are in a credit position, your credit amount will be carried forward for next filing. </td></tr>';
    cc_ = cc_ + '<tr><td colspan=7>If you wish to request a refund, kindly click here    :   NO </td></tr>';
    cc_ = cc_ + '<tr><td colspan=2> Please provide the new IBAN </td><td></td><td colspan=4>&nbsp</td></tr>';
    cc_ = cc_ + '<tr><td colspan=7> If you wish to request a refund towards a different IBAN, please select the check box and kindly enter above  </td></tr>';
    cc_ = cc_ + '<tr><td colspan=7><div><p><b>' + $('#hdnNote').val() + ': </b>Your refund will not be processed if your identification number details available on ';
    cc_ = cc_ + 'Step 2 "Taxpayer details" do not match the details your bank on record for this IBAN.</br> ';
    cc_ = cc_ + 'Please ensure that your bank has the correct details for this IBAN</br>';
    cc_ = cc_ + '<b>' + $('#hdntermCondtn').val() + ':</b></br>';
    cc_ = cc_ + '1. It is assumed that the taxpayer has read and understood the Kingdom of Saudi Arabia' + "'" + 's VAT Law and Regulations and all the information provided is, to the best of the taxpayer' + "'" + 's knowledge, </br>true,correct and complete</br>';
    cc_ = cc_ + '2. GAZT holds the right to request and obtain any financial or administrative information and records of that taxpayer and their business to cross check and verify the information provided in this return</br>';
    cc_ = cc_ + '3. GAZT holds the right to open an audit case in order to verify this return form and any previous forms to a maximum of 5 years past which may result in levy of fines as per the Kingdom of Saudi Arabia' + "'" + 's </br>VAT Law and Regulations</br> ';
    cc_ = cc_ + '4. After submission of this form, and if the taxpayer finds that they need to do a correction, a self-amendment form should be submitted if the amount of the correction is equal or more than SAR 5000.00 </br>or equal to less than SAR -5000.0</br></br>';
    cc_ = cc_ + '<b>' + $('#hdnDeclaration').val() + ':</b></br>';
    cc_ = cc_ + 'I certify that the information given in this returns, to the best of my knowledge, true, correct, and complete in every respect. I am the person who is required to file this return or I am authorized to sign on</br>behalf of that person. I also understand that I will be charged heavy penalties for submitting incorrect information.';
    cc_ = cc_ + '</p></div></td></tr>';*/
    $("#tbldiv tbody").append(cc_);
}
//function arrangeDailyCollectionData(resp) {

//    var HashVal = (window.localStorage.getItem("hashValues"));
//    var HashValues;
//    if (HashVal != null && HashVal != '') {
//        HashValues = HashVal.split('~');
//    }
//    var fromDate = (window.localStorage.getItem("RPfrmDate"));
//    var toDate = (window.localStorage.getItem("RPtoDate"));
//    ConvertDateWithFormat($('#hdnDateFromate').val(), fromDate, "fromDate")
//    ConvertDateWithFormat($('#hdnDateFromate').val(), fromDate, "toDate")
//    graphData = resp;
//    var result = resp;
//    var Primaryid = "";
//    $('#tbldiv thead tr').remove();
//    $('#tbldiv tbody tr').remove();
//    $('#tbldiv thead').append("<tr ><th  rowspan=2 colspan=7  style='text-align:center' ><br>&nbsp<br><b>" + $('#hdnVATRForm').val() + "</b><br>&nbsp</th></tr></thead>");
//    var cc_ = ''; var totSAmnt = "0.00"; var totSADJ = "0.00"; var totSVat = "0.00";
//    totPAmnt = "0.00"; var totPADJ = "0.00"; var totPVat = "0.00";
//    cc_ = cc_ + '<tr><td  colspan=7><table class="table table-bordered" style="width:79%"><tr><td colspan=2 style="width:501px;">' + $('#hdnCompany').val() + '</td><td >' + HashValues[10] + '</td></tr>';
//    cc_ = cc_ + '<tr><td colspan=2 style="width:501px;"> ' + $('#hdnAddress').val() + '</td><td>' + HashValues[26] + '</td></tr>';
//    cc_ = cc_ + '<tr><td colspan=2 style="width:501px;">' + $('#hdnTaxIdenfn').val() + '</td><td>' + HashValues[27] + '</td></tr>';
//    cc_ = cc_ + '<tr><td colspan=2 style="width:501px;">' + $('#hdnTaxReturnPeriod').val() + '</td><td >' + fromDate + ' to ' + toDate + '</td></tr></table></td></tr>'; //+ $('#INVLISlblDate')[0].innerText.replace('VAT Returns(', '').replace(')', '') + '</td></tr></table></td></tr>';
//    cc_ = cc_ + '<tr><td colspan=7>&nbsp</td></tr>';
//    cc_ = cc_ + "<tr><th rowspan=7 style='width:101px;'><input type=text value='" + $('#hdnVATonSales').val() + "' style='margin: 90px 0px 0px -6px;-webkit-transform:rotate(-90deg);-moz-transform:rotate(-90deg)' /></th><th></th><th>" + $('#hdnAmount').val() + " (SAR)</th><th>" + $('#hdnAdjustmnt').val() + " (SAR)</th><th>" + $('#hdnVatAmount').val() + " (SAR)</th></tr>";
//    for (var i = 0; i < result.length; i++) {
//        if (result[i].C0 == '1-Standard rate Sale') {
//            cc_ = cc_ + '<tr><td>1 - Standard rate Sales </td><td>' + parseFloat(result[i].C1).toFixed(2) + '</td><td>' + parseFloat(result[i].C2).toFixed(2) + '</td><td>' + parseFloat(result[i].C3).toFixed(2) + '</td></tr>';
//            cc_ = cc_ + '<tr><td>2 - Private Healthcare / Private Education /</br> First house sales to citizens. </td><td>0.00</td><td>0.00</td><td></td></tr>';
//            totSAmnt = parseFloat(totSAmnt) + parseFloat(result[i].C1);
//            totSADJ = parseFloat(totSADJ) + parseFloat(result[i].C2);
//            totSVat = parseFloat(totSVat) + parseFloat(result[i].C3);
//        }
//        if (result[i].C0 == '3-Zero rated Domestic Sale') {
//            cc_ = cc_ + '<tr><td>3 - Zero rated Domestic Sales </td><td>' + parseFloat(result[i].C1).toFixed(2) + '</td><td>' + parseFloat(result[i].C2).toFixed(2) + '</td><td>0.00</td></tr>';
//            cc_ = cc_ + '<tr><td>4 - Exports </td><td>0.00</td><td>0.00</td><td></td></tr>';
//            totSAmnt = parseFloat(totSAmnt) + parseFloat(result[i].C1);
//            totSADJ = parseFloat(totSADJ) + parseFloat(result[i].C2);
//            totSVat = parseFloat(totSVat) + parseFloat(result[i].C3);
//        }

//        if (result[i].C0 == '5-Exempted Sale') {
//            cc_ = cc_ + '<tr><td>5 - Exempt Sales </td><td>' + parseFloat(result[i].C1).toFixed(2) + '</td><td>' + parseFloat(result[i].C2).toFixed(2) + '</td><td>0.00</td></tr>';
//            totSAmnt = parseFloat(totSAmnt) + parseFloat(result[i].C1);
//            totSADJ = parseFloat(totSADJ) + parseFloat(result[i].C2);
//            totSVat = parseFloat(totSVat) + parseFloat(result[i].C3);
//            cc_ = cc_ + '<tr><td><b>6 - ' + $('#hdnTSales').val() + ' </b></td><td><b> ' + parseFloat(totSAmnt).toFixed(2) + '</b></td><td><b>' + parseFloat(totSADJ).toFixed(2) + '</b></td></b><td><b>' + parseFloat(totSVat).toFixed(2) + '</b></td></tr>';
//            cc_ = cc_ + '<tr><td colspan=7>&nbsp</td></tr>';
//        }

//        if (result[i].C0 == '7-Standard rate Domestic Purchases') {
//            cc_ = cc_ + '<tr><td rowspan=6 style=width:143px;> <input type=text value="' + $('#hdnVATonPur').val() + '" style="font-weight: bold;margin: 65px -78px 27px -8px; -webkit-transform:rotate(-90deg);-moz-transform:rotate(-90deg)" /></td><td>7 - Standard rate Domestic Purchases </td><td>' + parseFloat(result[i].C1).toFixed(2) + '</td><td>' + parseFloat(result[i].C2).toFixed(2) + '</td><td>' + parseFloat(result[i].C3).toFixed(2) + '</td></tr>';
//            cc_ = cc_ + '<tr><td>8 - Imports subject to VAT paid at customs </td><td>0.00</td><td>0.00</td></tr>';
//            cc_ = cc_ + '<tr><td>9 - Imports subject to VAT accounted for</br> through the reverse charge mechanism </td><td>0.00</td><td>0.00</td><td>0.00</td></tr>';
//            totPAmnt = parseFloat(totPAmnt) + parseFloat(result[i].C1);
//            totPADJ = parseFloat(totPADJ) + parseFloat(result[i].C2);
//            totPVat = parseFloat(totPVat) + parseFloat(result[i].C3);
//        }

//        if (result[i].C0 == '10-Zero rated Purchases') {
//            cc_ = cc_ + '<tr><td>10 - Zero Rated Purchases </td><td>' + parseFloat(result[i].C1).toFixed(2) + '</td><td>' + parseFloat(result[i].C2).toFixed(2) + '</td><td>' + parseFloat(result[i].C3).toFixed(2) + '</td></tr>';
//            totPAmnt = parseFloat(totPAmnt) + parseFloat(result[i].C1);
//            totPADJ = parseFloat(totPADJ) + parseFloat(result[i].C2);
//            totPVat = parseFloat(totPVat) + parseFloat(result[i].C3);
//        }

//        if (result[i].C0 == '11-Exempted Purchases') {
//            cc_ = cc_ + '<tr><td>11 - Exempt Purchases </td><td>' + parseFloat(result[i].C1).toFixed(2) + '</td><td>' + parseFloat(result[i].C2).toFixed(2) + '</td><td>' + parseFloat(result[i].C3).toFixed(2) + '</td></tr>';
//            totPAmnt = parseFloat(totPAmnt) + parseFloat(result[i].C1);
//            totPADJ = parseFloat(totPADJ) + parseFloat(result[i].C2);
//            totPVat = parseFloat(totPVat) + parseFloat(result[i].C3);
//        }
//    }
//    cc_ = cc_ + '<tr><td ><b>12 - ' + $('#hdnTPurchases').val() + ' </b></td><td><b>' + parseFloat(totPAmnt).toFixed(2) + '</b></td><td><b>' + parseFloat(totPADJ).toFixed(2) + '</b></td><td><b>' + parseFloat(totPVat).toFixed(2) + '</b></td></tr>';
//    var TotalVat = 0.00;
//    if ((parseFloat(totSVat) - parseFloat(totPVat)) < 0) {
//        TotalVat = 0.00;
//    }
//    else {
//        TotalVat = parseFloat(totSVat) - parseFloat(totPVat);
//    }
//    cc_ = cc_ + '<tr><td></td><td colspan=3>13 - Total VAT due for current period </td><td>' + parseFloat(TotalVat).toFixed(2) + '</td></tr>';
//    cc_ = cc_ + '<tr><td></td><td colspan=3>14 - Corrections from previous period</br> (between SAR + 5000) </td><td>0.00</td></tr>';
//    cc_ = cc_ + '<tr><td></td><td colspan=3>15 - VAT credit carried forward from previous </br>period(s) </td><td>0.00</td></tr>';
//    cc_ = cc_ + '<tr><td></td><td colspan=3><b>16 - ' + $('#hdnNetVATdue').val() + ' (or reclaimed) </b></td><td><b>' + parseFloat(parseFloat(totSVat) - parseFloat(totPVat)).toFixed(2) + '</b></td></tr>';
//    cc_ = cc_ + '<tr><td colspan=7>&nbsp</td></tr>';
//    cc_ = cc_ + '<tr><td colspan=7><b>' + $('#hdnRefundinfo').val() + ': </b></td></tr>';
//    cc_ = cc_ + '<tr><td colspan=7>Your return form information indicate that you are in a credit position, your credit amount will be carried forward for next filing. </td></tr>';
//    cc_ = cc_ + '<tr><td colspan=7>If you wish to request a refund, kindly click here    :   NO </td></tr>';
//    cc_ = cc_ + '<tr><td colspan=2> Please provide the new IBAN </td><td></td><td colspan=4>&nbsp</td></tr>';
//    cc_ = cc_ + '<tr><td colspan=7> If you wish to request a refund towards a different IBAN, please select the check box and kindly enter above  </td></tr>';
//    cc_ = cc_ + '<tr><td colspan=7><div><p><b>' + $('#hdnNote').val() + ': </b>Your refund will not be processed if your identification number details available on ';
//    cc_ = cc_ + 'Step 2 "Taxpayer details" do not match the details your bank on record for this IBAN.</br> ';
//    cc_ = cc_ + 'Please ensure that your bank has the correct details for this IBAN</br>';
//    cc_ = cc_ + '<b>' + $('#hdntermCondtn').val() + ':</b></br>';
//    cc_ = cc_ + '1. It is assumed that the taxpayer has read and understood the Kingdom of Saudi Arabia' + "'" + 's VAT Law and Regulations and all the information provided is, to the best of the taxpayer' + "'" + 's knowledge, </br>true,correct and complete</br>';
//    cc_ = cc_ + '2. GAZT holds the right to request and obtain any financial or administrative information and records of that taxpayer and their business to cross check and verify the information provided in this return</br>';
//    cc_ = cc_ + '3. GAZT holds the right to open an audit case in order to verify this return form and any previous forms to a maximum of 5 years past which may result in levy of fines as per the Kingdom of Saudi Arabia' + "'" + 's </br>VAT Law and Regulations</br> ';
//    cc_ = cc_ + '4. After submission of this form, and if the taxpayer finds that they need to do a correction, a self-amendment form should be submitted if the amount of the correction is equal or more than SAR 5000.00 </br>or equal to less than SAR -5000.0</br></br>';
//    cc_ = cc_ + '<b>' + $('#hdnDeclaration').val() + ':</b></br>';
//    cc_ = cc_ + 'I certify that the information given in this returns, to the best of my knowledge, true, correct, and complete in every respect. I am the person who is required to file this return or I am authorized to sign on</br>behalf of that person. I also understand that I will be charged heavy penalties for submitting incorrect information.';
//    cc_ = cc_ + '</p></div></td></tr>';
//    $("#tbldiv tbody").append(cc_);


//}
function treeclick(data) {
    window.localStorage.setItem('ctCheck', 1);
    var d_ = data.parentNode;
    if ($('#' + d_.id + '_0')[0].style.display != '')
        $('#' + d_.id + '_0')[0].style.display = '';
    else
        $('#' + d_.id + '_0')[0].style.display = 'none';
}

$('#btnCol').click(function () {
    window.localStorage.setItem('ctCheck', 1);
    var btnCol = document.getElementById("btnCol");
    btnCol.style.display = "none";

    var btnExp = document.getElementById("btnExp");
    btnExp.style.display = "block";


    var $el = $('#tbldiv tbody>tr');

    $el.addClass("tt-hide");

    for (var i = 0; i < $('#tbldiv tbody>tr').length; i++) {
        var tr = $('#tbldiv tbody>tr[id="dt_Parrent' + i + '"]');
        //var tr = $(table).find("tr[id='dt_Parrent" + i + "']");
        //var tdid = $($el[i]).find("td[id='dt_Parrent" + i + "']");
        $(tr).removeClass("tt-hide");
        $('#root_' + i + '').html("+");
    }

    $('#dt_Parrent1').removeClass("tt-hide");
    // GetUnpaging();
    //GetPaging();

});
$('#btnExp').click(function () {

    window.localStorage.setItem('ctCheck', 1);
    var btnCol = document.getElementById("btnCol");
    btnCol.style.display = "block";

    var btnExp = document.getElementById("btnExp");
    btnExp.style.display = "none";

    $('#tbldiv').html("");
    $('#hdnScreenmode').val("");
    GetUnpaging();

});

var specialElementHandlers = {
    '#editor': function (element, renderer) {
        return true;
    }
};

margins = { top: 15, left: 10, width: 1000, bottom: 50 };

$('#btn').click(function () {
    window.localStorage.setItem('ctCheck', 1);
    // $('#btnExp').click();
    var doc = new jsPDF('p', 'pt', 'a4', true);
    source = $('#div_grid')[0];
    doc.fromHTML(source, margins.left, margins.top, {
        'width': margins.width,
        'elementHandlers': specialElementHandlers
    });
    doc.save('VatReturns.pdf');
});

function exportexcelTAX() {
    window.localStorage.setItem('ctCheck', 1);
    var btnCol = document.getElementById("btnCol");
    btnCol.style.display = "block";

    var btnExp = document.getElementById("btnExp");
    btnExp.style.display = "none";
    $('#hdnScreenmode').val("excel");
    $('#tbldiv').html("");
    GetUnpaging();
}

function print() {
    window.localStorage.setItem('ctCheck', 1);
    var btnCol = document.getElementById("btnCol");
    btnCol.style.display = "block";

    var btnExp = document.getElementById("btnExp");
    btnExp.style.display = "none";
    $('#hdnScreenmode').val("print");
    $('#tbldiv').html("");
    GetUnpaging();
}

function GetUnpaging() {
    window.localStorage.setItem('ctCheck', 1);
    if (window.localStorage.getItem('RPPAGECODE') == 'POSTVRR') {
        var SubOrgId = '';
        var HashVal = (window.localStorage.getItem("hashValues"));
        var HashValues;
        if (HashVal != null && HashVal != '') {
            HashValues = HashVal.split('~');
            SubOrgId = HashValues[1];
            $('#hdnDateFromate').val(HashValues[2]);
        }
        if (window.localStorage.getItem('RPfrmDate') != null) {
            $('#INVLISlblDate').text('(' + ConvertDateAndReturnDate($('#hdnDateFromate').val(), window.localStorage.getItem('RPfrmDate')) + ' ' + $('#hdnSalesTo').val() + ' ' + ConvertDateAndReturnDate($('#hdnDateFromate').val(), window.localStorage.getItem('RPtoDate')) + ')');
        }
        var FromDate = (window.localStorage.getItem("RPfrmDate"));
        var ToDate = (window.localStorage.getItem("RPtoDate")) + ' 23:59:59';
        var RefNo = (window.localStorage.getItem("RPrefNo"));
        var PageCode = (window.localStorage.getItem("RPPAGECODE"));
        var grid = "";
        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.GetReport,
            data: "PageId=" + PageCode + "&subOrg=" + SubOrgId + "&frmDate=" + FromDate + "&toDate=" + ToDate + "&refNo=" + RefNo + "&DateFormate=" + $('#hdnDateFromate').val() + "&VD=" + VD(),
            success: function (resp) {
                var graphData = resp;
                getResources(HashValues[7], "POSReport", resp, 2);
            }
        });
    }
}

function loadGraphDataTAX() {
    window.localStorage.setItem('ctCheck', 1);
    $('#li-Excel i').attr('style', 'color:#919598 !important');
    $('#li-Excel').attr('onclick', 'return false');
    $('#li-Refresh i').attr('style', 'color:#919598 !important');
    $('#li-Refresh').attr('onclick', 'return false');
    $('#li_expand i').attr('style', 'color:#919598 !important');
    $('#li_expand').attr('onclick', 'return false');
    $('#li_compress i').attr('style', 'color:#919598 !important');
    $('#li_compress').attr('onclick', 'return false');
    $('#li_Print i').attr('style', 'color:#919598 !important');
    $('#li_Print').attr('onclick', 'return false');
 
  

    var param = { CounterId: window.localStorage.getItem('COUNTERID'), ShiftId: window.localStorage.getItem('SHIFTID'), SalespersonId: window.localStorage.getItem('CashierID') };
    var color = Chart.helpers.color;
    var barChartData = '';
    var GrColorTr = '';
    if (window.localStorage.getItem("Theam").split('-')[3] == '2.css') { GrColorTr = '#ffffff' } else { GrColorTr = '#000' }

    var elemGrid = document.getElementById("div_grid");
    elemGrid.style.display = "none";

    var elemGraph = document.getElementById("div_graph1");
    elemGraph.style.display = "block";

    var resp = graphData;


    var x = []; var y = [];


    for (var i = 0; i < resp.length; i++) {
        x.push(resp[i].C1);
        y.push(parseFloat(resp[i].C2).toFixed(2));
    }

    barChartData = {
        labels: x,
        datasets: [{
            label: 'Sale',
            backgroundColor: ["#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276"], //color(window.chartColors.red).alpha(0.5).rgbString(),
            borderColor: ["#ff7276", "#ff999c", "#ffb3b5", "#ffccce", "#ffe6e6", "#ff7276", "#ff7276", "#ff7276"],//window.chartColors.red,
            borderWidth: 1,
            data: y
        },
          {
              type: 'line',
              label: 'Line View',
              borderColor: window.chartColors.red,
              borderWidth: 2,
              fill: false,
              data: y,

          }
        ]
    }

    var ctx = document.getElementById("canvas").getContext("2d");
    window.myBar = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            legend: {
                labels: {
                    fontColor: GrColorTr,
                    fontSize: 12
                }
            },
            responsive: true,
            title: {
                display: true,
                text: ''
            },
            scales: {
                yAxes: [{
                    ticks: {
                        min: 0,
                        // stepSize: 1,
                        fontColor: GrColorTr,
                        fontSize: 14
                    }
                    ,
                    gridLines: {
                        color: GrColorTr
                        //    lineWidth: 2,
                        //    zeroLineColor: "#000",
                        //    zeroLineWidth: 2
                    },
                    stacked: true
                }],
                xAxes: [{
                    ticks: {
                        autoSkip: false,
                        maxRotation: 60,
                        minRotation: 60,
                        fontColor: GrColorTr,
                        fontSize: 14
                    },
                    gridLines: {
                        color: GrColorTr,
                        lineWidth: 2
                    }
                }]
            }
        }
    });
}
function VRRexportexcel() {
    window.localStorage.setItem('ctCheck', 1);
    loadGridExtend();
    var data_type = 'data:application/vnd.ms-excel';
    var table_div = document.getElementById('tbldiv');
    var table_html = table_div.outerHTML.replace(/ /g, '%20');
    var a = document.createElement('a');
    a.href = data_type + ', ' + table_html;
    var pageName_ = "exported_table_";

    pageName_ = "VAT-Return";

    a.download = pageName_ + '.xls'; 

    var d = $('#div_tbData');
    let options = {
        documentSize: 'A4',
        type: 'share',
        fileName: 'VAT-Return.pdf'
    }
    var lable_ = "<div><b>VAT Return " + $('#INVLISlblDate').text() + "</b></div></br>";
    pdf.fromData(lable_ + d[0].innerHTML, options)
        .then((stats) => console.log('status', stats))   // ok..., ok if it was able to handle the file to the OS.  
        .catch((err) =>console.err(err));

    loadGridCompress();
}

function VRRPrint() {
    window.localStorage.setItem('ctCheck', 1);
    var btnCol = document.getElementById("btnCol");

    loadGridExtend();
    var divContents = document.getElementById("div_grid").innerHTML;
    var printWindow = window.open('', '', 'height=200,width=450');
    printWindow.document.write('<html><head><title> VAT ReTurn</title>');
    printWindow.document.write('</head><body >');
    printWindow.document.write(divContents);
    printWindow.document.write('</body></html>');
    loadGridCompress();
    printWindow.document.close();
    printWindow.print();


}
function loadGridExtend() {
    window.localStorage.setItem('ctCheck', 1);
    var myTab = document.getElementById('tbldiv');
    for (i = 1; i < myTab.rows.length; i++) {
        var objCells = myTab.rows.item(i).cells;
        var id_ = myTab.rows.item(i).cells[0].parentNode.id + '_0';
        $('#' + id_)[0].style.display = '';
        i++;
    }
    var li_expand = document.getElementById("li_expand");
    li_expand.style.display = "none";
    var li_compress = document.getElementById("li_compress");
    li_compress.style.display = "inline-block";

}
function loadGridCompress() {
    window.localStorage.setItem('ctCheck', 1);
    var myTab = document.getElementById('tbldiv');
    for (i = 1; i < myTab.rows.length; i++) {
        var objCells = myTab.rows.item(i).cells;
        var id_ = myTab.rows.item(i).cells[0].parentNode.id + '_0';
        $('#' + id_)[0].style.display = 'none';
        i++;
    }
    var li_expand = document.getElementById("li_expand");
    li_expand.style.display = "inline-block";
    var li_compress = document.getElementById("li_compress");
    li_compress.style.display = "none";
}
function ConvertDateAndReturnDate(formates, dateText) {
    window.localStorage.setItem('ctCheck', 1);
    var hdnformat = formates;
    var txtMainDate = '';
    var splitc;
    if (hdnformat == 'd/m/Y' || hdnformat == 'm/d/Y' || hdnformat == 'Y/d/m' || hdnformat == 'Y/m/d' || hdnformat == 'M/d/Y' || hdnformat == 'd/M/Y') {
        splitc = '/';
    }
    else if (hdnformat == 'd-m-Y' || hdnformat == 'm-d-Y' || hdnformat == 'Y-d-m' || hdnformat == 'Y-m-d' || hdnformat == 'M-d-Y' || hdnformat == 'd-M-Y') {
        splitc = '-';
    }
    else if (hdnformat == 'd,m,Y' || hdnformat == 'm,d,Y' || hdnformat == 'Y,d,m' || hdnformat == 'Y,m,d' || hdnformat == 'M,d,Y' || hdnformat == 'd,M,Y') {

        splitc = ',';
    }
    else if (hdnformat == 'd m Y' || hdnformat == 'm d Y' || hdnformat == 'Y d m' || hdnformat == 'Y m d' || hdnformat == 'M d Y' || hdnformat == 'd M Y') {

        splitc = ' ';
    }
    else if (hdnformat == 'd.m.Y' || hdnformat == 'm.d.Y' || hdnformat == 'Y.d.m' || hdnformat == 'Y.m.d' || hdnformat == 'M.d.Y' || hdnformat == 'd.M.Y') {
        splitc = '.';
    };
    var sym = '-';
    if (dateText.indexOf('/') != -1)
        sym = '/';
    var vardatesplit = dateText.split(sym);
    var varM;
    var varMn;
    var varfformatS = hdnformat.split(splitc);
    if (varfformatS[0] == 'M' || varfformatS[1] == 'M') {

        if (vardatesplit[1] != null)
            varM = vardatesplit[1];
        if (vardatesplit[0] != null)
            varM = vardatesplit[0];

        switch (varM) {
            case 'Jan': varMn = '01';
                break;
            case 'Feb': varMn = '02';
                break;
            case 'Mar': varMn = '03';
                break;
            case 'Apr': varMn = '04';
                break;
            case 'May': varMn = '05';
                break;
            case 'Jun': varMn = '06';
                break;
            case 'Jul': varMn = '07';
                break;
            case 'Aug': varMn = '08';
                break;
            case 'Sep': varMn = '09';
                break;
            case 'Oct': varMn = '10';
                break;
            case 'Nov': varMn = '11';
                break;
            case 'Dec': varMn = '12';
                break;
        }

    }
    if (varfformatS[0] == 'd' && (varfformatS[1] == 'm' || varfformatS[1] == 'M') && varfformatS[2] == 'Y') {
        if (varfformatS[1] == 'M') {
            txtMainDate = varMn + splitc + vardatesplit[1] + splitc + vardatesplit[0];
        }
        else {
            txtMainDate = vardatesplit[2] + splitc + vardatesplit[1] + splitc + vardatesplit[0];
        }
    }
    else if ((varfformatS[0] == 'm' || varfformatS[0] == 'M') && varfformatS[1] == 'd' && varfformatS[2] == 'Y') {
        if (varfformatS[0] == 'M') {
            txtMainDate = varMn + splitc + vardatesplit[2] + splitc + vardatesplit[0];
        }
        else {
            txtMainDate = vardatesplit[1] + splitc + vardatesplit[2] + splitc + vardatesplit[0];
        }
    }
    else if (varfformatS[0] == 'Y' && varfformatS[1] == 'd' && varfformatS[2] == 'm') {
        txtMainDate = vardatesplit[0] + splitc + vardatesplit[2] + splitc + vardatesplit[1];
    }
    else if (varfformatS[0] == 'Y' && varfformatS[1] == 'm' && varfformatS[2] == 'd') {
        txtMainDate = vardatesplit[0] + splitc + vardatesplit[1] + splitc + vardatesplit[2];
    }
    else {
        txtMainDate = vardatesplit[0] + splitc + vardatesplit[1] + splitc + vardatesplit[2];
    }
    return txtMainDate;
}