﻿function VD() {
    window.localStorage.setItem('ctCheck', 1);
    var vd_ = window.localStorage.getItem("UUID");
    //var vdSplit_ = window.location.pathname.split('/');
    //if (vdSplit_.length > 2) {
    //    vd_ = vdSplit_[1];
    //}
    return vd_;
}
function theamLoad() {
    window.localStorage.setItem('ctCheck', 1);
    if (window.localStorage.getItem("Theam") != null)
        $('head').append('<link rel="stylesheet" href=' + window.localStorage.getItem("Theam") + ' type="text/css" />');
}
function loadMainData() {
    window.localStorage.setItem('ctCheck', 1);
    theamLoad();
    var elemGrid = document.getElementById("div_grid");
    elemGrid.style.display = "block";

    var elemGraph = document.getElementById("div_graph1");
    elemGraph.style.display = "none";
    var sess = CashierLite.Session.getInstance().get();
    if (sess != null) {
        if (sess.userName != "") {
       //     document.getElementById("lblName").innerHTML = sess.userName;
        }
        else {
            window.open('Login.html', '_self', 'location=no');
        }
    }
    else {
        window.open('Login.html', '_self', 'location=no');
    }
    var HashVal = (window.localStorage.getItem("hashValues"));
    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');
        $('#hdnDateFromate').val(HashValues[2]);
        $('#hdnPaging').val(HashValues[19]);
    }
    var param = { CounterId: window.localStorage.getItem('COUNTERID'), ShiftId: window.localStorage.getItem('SHIFTID'), SalespersonId: window.localStorage.getItem('CashierID'),VD:VD() };
    if (window.localStorage.getItem('COUNTERID') != null) {
        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.POSGetTodaySalesDetailsRP,
            data: param,
            success: function (resp) {
                //var result = resp;
                getResources(HashValues[7], "POSTodayItemsSummaryReport", resp)
               
            }
        });
    }
    else {
        var $textAndPic = $('<div></div>');
        $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        $textAndPic.append('<div class="alert-message">You are not assigned any Counter so you can\'t do the Transaction</div>');
        BootstrapDialog.alert($textAndPic);
        //window.location('DashBoard.html');
        window.open('DashBoard.html', '_self', 'location=no');
    }
}

function getData(resp) {
    window.localStorage.setItem('ctCheck', 1);
    var result = resp;
    var Primaryid = "";
    //$('#tbldiv').append("<thead><tr><th>Item Code</th><th>Item Name</th><th>UOM</th><th>Qty</th><th>Amount</th></tr></thead>");
    $('#tbldiv').append("<thead><tr><th>" + $('#hdnItemCode').val() + "</th><th>" + $('#hdnItemName').val() + "</th><th>" + $('#hdnUom').val() + "</th><th>" + $('#hdnQty').val() + "</th><th>" + $('#hdnAmount').val() + "</th></tr></thead>");
    $('#tbldiv').append("<tbody>");
    var cc_ = '';
    for (var i = 0; i < result.length; i++) {
        cc_ = cc_ + '<tr id="dt_Parrent' + i + '" data-tt-parent="Paging"><td>' + result[i].ItemCode + '</td><td>' + result[i].ItemName + '</td><td>' + result[i].UOM.split('/')[0] + '</td><td style="text-align:right">' +parseFloat(result[i].Qty).toFixed(2) + '</td><td style="text-align:right">' + parseFloat(result[i].Price).toFixed(2) + '</td></tr>';
    }
    cc_ = cc_ + "</tbody>";
    $("#tbldiv tbody").append(cc_);
    //$('#tbldiv').append("</tbody>");
    $('#tbldiv').treetable();
    //GetPaging();
    //var grid = $('#grid').grid({

    //    dataSource: resp, pager: { limit: 5 },
    //    columns: [
    //        { field: 'ItemName', title: 'Item Name', sortable: true },
    //        { field: 'UOM', title: 'UOM', sortable: true },
    //        { field: 'Qty', title: 'Qty', sortable: true }

    //    ],

    //}); 
}

Chart.plugins.register({
    afterDatasetsDraw: function (chartInstance, easing) {
        // To only draw at the end of animation, check for easing === 1
        var ctx = chartInstance.chart.ctx;

        chartInstance.data.datasets.forEach(function (dataset, i) {
            var meta = chartInstance.getDatasetMeta(i);
            if (!meta.hidden) {
                meta.data.forEach(function (element, index) {
                    // Draw the text in black, with the specified font
                    ctx.fillStyle = 'rgb(0, 0, 0)';

                    var fontSize = 16;
                    var fontStyle = 'normal';
                    var fontFamily = 'Helvetica Neue';
                    ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

                    // Just naively convert to string for now
                    var dataString = dataset.data[index].toString();

                    // Make sure alignment settings are correct
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'middle';

                    var padding = 5;
                    var position = element.tooltipPosition();
                    ctx.fillText(dataString, position.x, position.y - (fontSize / 2) - padding);
                });
            }
        });
    }
});
function loadGraphData() {
    window.localStorage.setItem('ctCheck', 1);
    //var MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var color = Chart.helpers.color;
    var barChartData = '';
    var param = { CounterId: window.localStorage.getItem('COUNTERID'), ShiftId: window.localStorage.getItem('SHIFTID'), SalespersonId: window.localStorage.getItem('CashierID'),VD:VD() };
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.POSGetTodaySalesDetailsRP,
        data: param,
        success: function (resp) {
            
            var x = []; var y = [];


            for (var i = 0; i < resp.length; i++) {

                var x_ = resp[i].ItemName.split('/')

                x.push(x_[0]);

                y.push(parseFloat(resp[i].Qty).toFixed(2));
            }

            barChartData = {
                labels: x,
                datasets: [{
                    label: 'Sale - Qty',
                    backgroundColor: ["#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276"], //color(window.chartColors.red).alpha(0.5).rgbString(),
                    borderColor: ["#ff7276", "#ff999c", "#ffb3b5", "#ffccce", "#ffe6e6", "#ff7276", "#ff7276", "#ff7276"],//window.chartColors.red,
                    borderWidth: 1,
                    data: y
                } 
                ]
            }

            var ctx = document.getElementById("canvas").getContext("2d");
            window.myBar = new Chart(ctx, {
                type: 'bar',
                data: barChartData,
                options: {
                    responsive: true,
                    title: {
                        display: true,
                        text: ''
                    },
                }
            });
        }
    });







    var elemGrid = document.getElementById("div_grid");
    elemGrid.style.display = "none";

    var elemGraph = document.getElementById("div_graph1");
    elemGraph.style.display = "block";

}
function loadGrid()
{
    $('#div_graph1').css("display", "none");
    $('#div_grid').css("display", "block");
}
function print()
{
    window.localStorage.setItem('ctCheck', 1);
    var divContents = document.getElementById("div_grid").innerHTML;
    var printWindow = window.open('', '', 'height=200,width=450');
    printWindow.document.write('<html><head><title>DailyCollectionsSummaryByTenderTypeReport</title>');
    printWindow.document.write('</head><body >');
    printWindow.document.write(divContents);
    printWindow.document.write('</body></html>');
    printWindow.document.close();
    printWindow.print();
}
function exportexcel1()
{
    window.localStorage.setItem('ctCheck', 1);
    var data_type = 'data:application/vnd.ms-excel';
    var table_div = document.getElementById('tbldiv');
    var table_html = table_div.outerHTML.replace(/ /g, '%20');
    var a = document.createElement('a');
    a.href = data_type + ', ' + table_html;
    a.download = 'Today_Sales_Item_Details_' + Math.floor((Math.random() * 9999999) + 1000000) + '.xls';
    a.click();
}
function GetPaging() {
    window.localStorage.setItem('ctCheck', 1);
    $('table#tbldiv').each(function () {

        var currentPage = 0;
        var numPerPage = $('#hdnPaging').val();
        var $table = $(this);
        var $tr = $table.find('tbody tr');

        $table.bind('repaginate', function () {
            $table.find('tbody tr[data-tt-parent="Paging"]').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
        });
        //  
        $table.trigger('repaginate');
        var numRows = $table.find('tbody tr[data-tt-parent="Paging"]').length;
        var numPages = Math.ceil(numRows / numPerPage);

        $("#DivPager").remove();

        var $pager = $('<div id="DivPager" class="pager"></div>');
        for (var page = 0; page < numPages; page++) {
            $('<span class="page-number"></span>').text(page + 1).bind('click', {
                newPage: page
            }, function (event) {
                currentPage = event.data['newPage'];
                $table.trigger('repaginate');
                $(this).addClass('active').siblings().removeClass('active');
            }).appendTo($pager).addClass('clickable');
        }
        $pager.insertAfter($table).find('span.page-number:first').addClass('active');

    });

}

function getResources(langCode, pageCode, data) {
    window.localStorage.setItem('ctCheck', 1);
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.getResource,
        data: "langCode=" + langCode + "&pageCode=" + pageCode,
        success: function (resp) {
            setLabels(resp, data);
        }
    });
}

function setLabels(labelData, data) {
    window.localStorage.setItem('ctCheck', 1);
    jQuery("label[for='lblTitle'").html(labelData['todatCloseRep']);
    //jQuery("label[for='lblHourlyBased'").html(labelData['alert2']);
    //$('#hdnInvoiceListingReport').val(labelData['invoiceListing']);

    $('#hdnItemCode').val(labelData["item"] + labelData["code"]);
    $('#hdnItemName').val(labelData["name"]);
    $('#hdnUom').val(labelData["uom"]);
    $('#hdnQty').val(labelData["qty"]);
    $('#hdnAmount').val(labelData["amount"]);
    //$('#hdnHourlyBased').val(labelData["alert2"]);
    $('#lblNavigationUrl').text(labelData["pos"] + " >> " + labelData["todatCloseRep"]);
    getData(data);
}