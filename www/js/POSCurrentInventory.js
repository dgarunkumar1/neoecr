﻿function VD() {
    window.localStorage.setItem('ctCheck', 1);
    var vd_ = window.localStorage.getItem("UUID");
    return vd_;
}
var graphData; var graphDataCh1;
function floorFigure(figure, decimals) {
    if (!decimals) decimals = 2;
    var d = Math.pow(10, decimals);
    return (parseInt(figure * d) / d).toFixed(decimals);
};
if (window.localStorage.getItem("Theam").split('-')[3] == '1.css') {
    $('#IMG1').attr("src", "images/mmlogo-inner2.png");
} else {
    $('#IMG1').attr("src", "images/mmlogo-inner1.png");
}


function InvMail() {
    $('#txtBccMailId').val('');
    $('#txtBody').val('');


    window.localStorage.setItem('ctCheck', 1);
    $('#ModalINVMail').modal('show');
    //   tenderAddpartial(TenderID, 1, '');
    $('#txtMailId').val('');
    //  $('#FileFormat').val('0');
    $('#txtMailId').focus();
    $('#FileFormat').attr("readonly", "true");
}
function SendINVMail(MailId, BccMailId, Format, Body) {
    window.localStorage.setItem('ctCheck', 1);
    if ($('#txtMailId').val() == "") {
        navigator.notification.alert($('#hdnEnterMail').val(), "", "neo ecr", "Ok");
    }
    else {
        var options = { dimBackground: true };
        SpinnerPlugin.activityStart("Please wait...", options);
        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.SendMailArabic,
            data: "language=" + HashValues[7] + "&PageId=" + window.localStorage.getItem('RPPAGECODE') + "&subOrg=" + HashValues[1] + "&frmDate=" + window.localStorage.getItem('RPfrmDate') + "&toDate=" + window.localStorage.getItem('RPtoDate') + ' 23:59:59' + "&DateFormate=" + $('#hdnDateFromate').val() + "&VD=" + VD() + "&MailId=" + MailId + "&Format=" + Format + "&BCMailId=" + BccMailId + "&Body=" + Body + "",

            success: function (resp) {
                SpinnerPlugin.activityStop();
                if (resp == '1000') {
                    navigator.notification.alert($('#hdnMailSuccess').val(), "", "neo ecr", "Ok");
                    $('#ModalINVMail').modal('hide');
                }
                else {
                    navigator.notification.alert($('#hdnMailnotSuccess').val(), "", "neo ecr", "Ok");
                    $('#ModalINVMail').modal('hide');
                }
            },
            error: function (e) {
                SpinnerPlugin.activityStop();
                navigator.notification.alert($('#hdnInvalidData').val(), "", "neo ecr", "Ok");
                $('#ModalINVMail').modal('hide');
            }
        });
    }
}

function arrangeCurrentInventoryData(resp) {
    window.localStorage.setItem('ctCheck', 1);
    graphDataCh1 = ''
    // graphData = resp;
    var result = resp;
    var Primaryid = "";
    $('#tbldiv thead tr').remove();
    $('#tbldiv tbody tr').remove();
    $('#tbldiv thead').append("<tr><th style='width:10px;display:none' ></th><th>" + $('#hdnItemCode').val() + "</th><th>" + $('#hdnItemName').val() + "</th><th>" + $('#hdnUOM').val() + "</th><th>" + $('#hdnQty').val() + "</th><th>" + $('#hdnPrice').val() + "</th><th>" + $('#hdnAmount').val() + "</th><th style='display:none'>" + $('#hdnItemId').val() + "</th><th style='display:none'>" + $('#hdnUomId').val() + "</th></tr>");

    var cc_ = '';
    if (HashValues[7] == 'ar-SA') {
        for (var i = 0; i < result.length; i++) {
            cc_ = cc_ + '<tr id="dt_Parrent' + i + '" data-tt-parent="Paging"><td style="display:none"><div class="tt" data-tt-id="root' + i + '" data-tt-parent=""><div class="content" style="width: 25px;font-weight:bold;display:none;"  id="root_' + i + '"></div></div></td><td>&nbsp' + result[i].C1 + '</td><td style="width: 40%;">' + result[i].C2.split('/')[1] + ' / ' + result[i].C2.split('/')[0]+ '</td><td>' + result[i].C4 + '</td><td style="text-align:left;direction:ltr;">' + result[i].C5 + '&nbsp' + '</td><td style="text-align:left">' + parseFloat(result[i].C7).toFixed(2) + '&nbsp' + '</td><td style="text-align:left">' + parseFloat(result[i].C6).toFixed(2) + '&nbsp' + '</td></tr>';
        }
        $("#tbldiv tbody").append(cc_);
    }
    else
    {
        for (var i = 0; i < result.length; i++) {
            cc_ = cc_ + '<tr id="dt_Parrent' + i + '" data-tt-parent="Paging"><td style="display:none"><div class="tt" data-tt-id="root' + i + '" data-tt-parent=""><div class="content" style="width: 25px;font-weight:bold;display:none;"  id="root_' + i + '"></div></div></td><td>&nbsp' + result[i].C1 + '</td><td style="width: 40%;">' + result[i].C2 + '</td><td>' + result[i].C4 + '</td><td style="text-align:right;direction:ltr;">' + result[i].C5 + '&nbsp' + '</td><td style="text-align:right">' + parseFloat(result[i].C7).toFixed(2) + '&nbsp' + '</td><td style="text-align:right">' + parseFloat(result[i].C6).toFixed(2) + '&nbsp' + '</td></tr>';
        }
        $("#tbldiv tbody").append(cc_);
    }
    graphDataCh1 = resp;
    //if ($('#hdnGraph').val() == 'G') {
    //    GraphPageWise();
    //}
}

function CurrentInventory() {
    window.localStorage.setItem('ctCheck', 1);
    theamLoad();
    $('#hdnPaging').val('1');
    var btnExp = document.getElementById("btnExp");
    btnExp.style.display = "block";
    if (window.localStorage.getItem('RPPAGECODE') == 'POSCINV') {
        var SubOrgId = '';
        var HashVal = (window.localStorage.getItem("hashValues"));
        var HashValues;
        if (HashVal != null && HashVal != '') {
            HashValues = HashVal.split('~');
            //getResources(HashValues[7], "POSCurrentInventoryReport");
            SubOrgId = HashValues[1];
            $('#hdnDateFromate').val(HashValues[2]);
            //   $('#hdnPaging').val(HashValues[19]);

        }

        var PageIndex = $('#hdnPaging').val();
        var PageCount = $('#txt_pageCount').val();

        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.GetReport_V01,
            data: "PageId=" + window.localStorage.getItem('RPPAGECODE') + "&subOrg=" + HashValues[1] + "&frmDate=" + window.localStorage.getItem('RPfrmDate') + "&toDate=" + window.localStorage.getItem('RPtoDate') + ' 23:59:59' + "&refNo=" + window.localStorage.getItem('RPrefNo') + "&DateFormate=" + $('#hdnDateFromate').val() + "&VD=" + VD() + "&PageIndex=" + PageIndex + "&PageCount=" + PageCount +"&language=" + HashValues[7] + "",
            success: function (resp) {
                graphData = resp;
               
                if (resp != '') {
                    jQuery("label[for='lbl_invPageOf'").html(resp[0].C9);
                    $('#hdnTotalPagesCount').val(resp[0].C10); 
                }
                getResources(HashValues[7], "POSCurrentInventoryReport", resp, 1);
                $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());
                if ($('#hdnTotalPagesCount').val() != $('#hdnPaging').val()) {
                    $('#li_PRPrev i').removeAttr('style', 'color:#919598 !important');
                    $('#li_PRPrev').attr('onclick', 'nextPRInvl()');
                    $('#li_PRLast i').removeAttr('style', 'color:#919598 !important');  
                    $('#li_PRLast').attr('onclick', 'nextPRLastInvl()');
                }
                else {
                    $('#li_PRPrev i').attr('style', 'color:#919598 !important');
                    $('#li_PRPrev').attr('onclick', 'return false');
                    $('#li_PRLast').attr('onclick', 'return false');
                    $('#li_PRLast i').attr('style', 'color:#919598 !important');
                }

            }

        });

    }
   
}

function prevPRFirstInvl() {
    CurrentInventory()
    window.localStorage.setItem('ctCheck', 1);
}


function theamLoad() {
    window.localStorage.setItem('ctCheck', 1);
    if (window.localStorage.getItem("Theam") != null)
        $('head').append('<link rel="stylesheet" href=' + window.localStorage.getItem("Theam") + ' type="text/css" />');
}
function nextPRInvl() {
    window.localStorage.setItem('ctCheck', 1);
    $('#hdnPaging').val(parseFloat($('#hdnPaging').val()) + 1);
    if ($('#txt_pageCount').val() == "") {
        $('#txt_pageCount').val('10');
        $('#hdnPaging').val('1');
    }
    var btnExp = document.getElementById("btnExp");
    btnExp.style.display = "block";

    var SubOrgId = '';
    var HashVal = (window.localStorage.getItem("hashValues"));
    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');

        SubOrgId = HashValues[1];
        $('#hdnDateFromate').val(HashValues[2]);

        var PageIndex = $('#hdnPaging').val();
        var PageCount = $('#txt_pageCount').val();

        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.GetReport_V01,
            data: "PageId=" + window.localStorage.getItem('RPPAGECODE') + "&subOrg=" + SubOrgId + "&frmDate=" + window.localStorage.getItem('RPfrmDate') + "&toDate=" + window.localStorage.getItem('RPtoDate') + ' 23:59:59' + "&refNo=" + window.localStorage.getItem('RPrefNo') + "&DateFormate=" + $('#hdnDateFromate').val() + "&VD=" + VD() + "&PageIndex=" + PageIndex + "&PageCount=" + PageCount +"&language=" + HashValues[7] + "",
            success: function (resp) {
                graphData = resp;
                if (resp != '') {
                    jQuery("label[for='lbl_invPageOf'").html(resp[0].C9);
                }
                getResources(HashValues[7], "POSCurrentInventoryReport", resp, 1);
                $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());
                //if ($('#hdnGraph').val() == 'G') {
                //GraphPageWise();
                //}

            }

        });
    }
        if ($('#hdnTotalPagesCount').val() == $('#hdnPaging').val()) {
            $('#li_PRPrev i').attr('style', 'color:#919598 !important');
            $('#li_PRPrev').attr('onclick', 'return false');
            $('#li_PRLast').attr('onclick', 'return false');
            $('#li_PRLast i').attr('style', 'color:#919598 !important');
        }
    
    }



function prevPRInvl() {
    window.localStorage.setItem('ctCheck', 1);
    $('#hdnPaging').val(parseFloat($('#hdnPaging').val()) - 1);
    if ($('#txt_pageCount').val() == "") {
        $('#txt_pageCount').val('10');
        $('#hdnPaging').val('1');
    }
    if (parseFloat($('#hdnPaging').val()) == 0) {
        $('#hdnPaging').val('1');
    }
    if ($('#hdnTotalPagesCount').val() != $('#hdnPaging').val())
        {
        $('#li_PRPrev i').removeAttr('style', 'color:#919598 !important');
        $('#li_PRPrev').attr('onclick', 'nextPRInvl()');
        $('#li_PRLast i').removeAttr('style', 'color:#919598 !important');
        $('#li_PRLast').attr('onclick', 'nextPRLastInvl()');
}
    var btnExp = document.getElementById("btnExp");
    btnExp.style.display = "block";

    var SubOrgId = '';
    var HashVal = (window.localStorage.getItem("hashValues"));
    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');

        SubOrgId = HashValues[1];
        $('#hdnDateFromate').val(HashValues[2]);

        var PageIndex = $('#hdnPaging').val();
        var PageCount = $('#txt_pageCount').val();

        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.GetReport_V01,
            data: "PageId=" + window.localStorage.getItem('RPPAGECODE') + "&subOrg=" + SubOrgId + "&frmDate=" + window.localStorage.getItem('RPfrmDate') + "&toDate=" + window.localStorage.getItem('RPtoDate') + ' 23:59:59' + "&refNo=" + window.localStorage.getItem('RPrefNo') + "&DateFormate=" + $('#hdnDateFromate').val() + "&VD=" + VD() + "&PageIndex=" + PageIndex + "&PageCount=" + PageCount + "&language=" + HashValues[7] + "",
            success: function (resp) {
                graphData = resp;
                if (resp != '') {
                    jQuery("label[for='lbl_invPageOf'").html(resp[0].C9);
                }
                getResources(HashValues[7], "POSCurrentInventoryReport", resp, 1);
                $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());

                //if ($('#hdnGraph').val() == 'G') {
                //GraphPageWise();
                //}

            }

        });
    }

}

function nextPRLastInvl() {
    window.localStorage.setItem('ctCheck', 1);
    var lastPageIndex = $('#lbl_invPageOf')[0].innerText.split(' ')[2];
    if (lastPageIndex == '') {
        lastPageIndex = '1';
    }
    $('#hdnPaging').val(parseFloat(lastPageIndex));
    if ($('#txt_pageCount').val() == "") {
        $('#txt_pageCount').val('10');
        $('#hdnPaging').val('1');
    }
    var btnExp = document.getElementById("btnExp");
    btnExp.style.display = "block";
    var SubOrgId = '';
    var HashVal = (window.localStorage.getItem("hashValues"));
    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');

        SubOrgId = HashValues[1];
        $('#hdnDateFromate').val(HashValues[2]);

        var PageIndex = $('#hdnPaging').val();
        var PageCount = $('#txt_pageCount').val();

        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.GetReport_V01,
            data: "PageId=" + window.localStorage.getItem('RPPAGECODE') + "&subOrg=" + SubOrgId + "&frmDate=" + window.localStorage.getItem('RPfrmDate') + "&toDate=" + window.localStorage.getItem('RPtoDate') + ' 23:59:59' + "&refNo=" + window.localStorage.getItem('RPrefNo') + "&DateFormate=" + $('#hdnDateFromate').val() + "&VD=" + VD() + "&PageIndex=" + PageIndex + "&PageCount=" + PageCount + "&language=" + HashValues[7] + "",
            success: function (resp) {
                graphData = resp;
                if (resp != '') {
                    jQuery("label[for='lbl_invPageOf'").html(resp[0].C9);
                }
                getResources(HashValues[7], "POSCurrentInventoryReport", resp, 1);
                $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());

                //if ($('#hdnGraph').val() == 'G') {
                //GraphPageWise();
                //}
            }

        });

    }

    if ($('#hdnTotalPagesCount').val() == $('#hdnPaging').val()) {
        $('#li_PRPrev i').attr('style', 'color:#919598 !important');
        $('#li_PRPrev').attr('onclick', 'return false');
        $('#li_PRLast').attr('onclick', 'return false');
        $('#li_PRLast i').attr('style', 'color:#919598 !important');
    }
}


function loadGrid() {
    window.localStorage.setItem('ctCheck', 1);
    //$('#hdnGraph').val('');
    //   $('#div_graph1').css("display", "none");
    //$('#div_grid').css("display", "block");
    $('#CIExcel i').removeAttr('style', 'color:#919598 !important');
    $('#CIExcel').attr('onclick', 'GetDowloadFile()');
    $('#CIRefresh i').removeAttr('style', 'color:#919598 !important');
    $('#CIRefresh').attr('onclick', 'CurrentInventory()');
    $('#txt_PRPPageIndex').removeAttr('disabled', 'true');
    $('#btnPage').removeAttr('disabled', 'true');
    $('#li_PRFirst i').removeAttr('style', 'color:#919598 !important');
    $('#li_PRFirst').attr('onclick', 'prevPRFirstInvl()');
    $('#li_PRNext i').removeAttr('style', 'color:#919598 !important');
    $('#li_PRNext').attr('onclick', 'prevPRInvl()');
    $('#li_PRPrev i').removeAttr('style', 'color:#919598 !important');
    $('#li_PRPrev').attr('onclick', 'nextPRInvl()');
    $('#li_PRLast i').removeAttr('style', 'color:#919598 !important');
    $('#li_PRLast').attr('onclick', 'nextPRLastInvl()');
    $('#li_print i').removeAttr('style', 'color:#919598 !important');
    $('#li_print').attr('onclick', 'CIPrint()');
    $('#lbl_invPageOf').removeAttr('style', 'color:#919598 !important');
    $('#lbl_page').removeAttr('style', 'color:#919598 !important');
    $('#txt_PRPPageIndex').val('');

    var elemGrid = document.getElementById("div_grid");
    elemGrid.style.display = "block";

    var elemGraph = document.getElementById("div_graph");
    elemGraph.style.display = "none";

    if ($('#hdnTotalPagesCount').val() == $('#hdnPaging').val()) {
        $('#li_PRPrev i').attr('style', 'color:#919598 !important');
        $('#li_PRPrev').attr('onclick', 'return false');
        $('#li_PRLast').attr('onclick', 'return false');
        $('#li_PRLast i').attr('style', 'color:#919598 !important');
    }
}
function loadCIGridExtend() {
    window.localStorage.setItem('ctCheck', 1);
    $('#txt_PRPPageIndex').val('');
    var myTab = document.getElementById('tbldiv');
    for (i = 1; i < myTab.rows.length; i++) {
        var objCells = myTab.rows.item(i).cells;
        var id_ = myTab.rows.item(i).cells[0].parentNode.id + '_0';
        //  $('#' + id_)[0].style.display = '';
        i++;
    }
    var li_expand = document.getElementById("li_expand");
    li_expand.style.display = "none";
    var li_compress = document.getElementById("li_compress");
    li_compress.style.display = "inline-block";

}

function loadCIGridCompress() {
    window.localStorage.setItem('ctCheck', 1);
    $('#txt_PRPPageIndex').val('');
    var myTab = document.getElementById('tbldiv');
    for (i = 1; i < myTab.rows.length; i++) {
        var objCells = myTab.rows.item(i).cells;
        var id_ = myTab.rows.item(i).cells[0].parentNode.id + '_0';
        //  $('#' + id_)[0].style.display = 'none';
        i++;
    }
    var li_expand = document.getElementById("li_expand");
    li_expand.style.display = "inline-block";
    var li_compress = document.getElementById("li_compress");
    li_compress.style.display = "none";
}

function CIexportexcel() {
    window.localStorage.setItem('ctCheck', 1);
  //  loadInvoiceGridExtend();
    var data_type = 'data:application/vnd.ms-excel';
    var table_div = document.getElementById('tbldiv');
    var table_html = table_div.outerHTML.replace(/ /g, '%20');
    var a = document.createElement('a');
    a.href = data_type + ', ' + table_html;
    var pageName_ = "exported_table_";

    pageName_ = "Current-Inventory";

    a.download = pageName_ + '.xls';
    //a.click();

    var d = $('#div_tbData');
    let options = {
        documentSize: 'A4',
        type: 'share',
        fileName: 'Current-Inventory.pdf'
    }
    var lable_ = "<div><b>Current Inventory " + $('#INVLISlblDate').text() + "</b></div></br>";
    pdf.fromData(lable_ + d[0].innerHTML, options)
        .then((stats) => console.log('status', stats))   // ok..., ok if it was able to handle the file to the OS.  
        .catch((err) =>console.err(err));


  //  loadInvoiceGridCompress();
  
}

function CIPrint() {
    window.localStorage.setItem('ctCheck', 1);
    var btnCol = document.getElementById("btnInvCol");
//    loadInvoiceGridExtend();


    var divContents = document.getElementById("div_grid").innerHTML;
    var printWindow = window.open('', '', 'height=200,width=450');
    printWindow.document.write('<html><head><title>Current Inventory</title>');
    printWindow.document.write('</head><body >');
    printWindow.document.write(divContents);
    printWindow.document.write('</body></html>');
  //  loadInvoiceGridCompress();
    printWindow.document.close();
    printWindow.print();


}

function loadInventorybyPageIndexStatic() {
    window.localStorage.setItem('ctCheck', 1);
    var pageIndexT_ = 1;
    if ($('#txt_PRPPageIndex').val() != '') {
        if ($('#txt_PRPPageIndex').val() != '0') {
            pageIndexT_ = $('#txt_PRPPageIndex').val();
        }
    }
    $('#hdnPaging').val(pageIndexT_);
    var btnExp = document.getElementById("btnExp");
    btnExp.style.display = "block";

    var SubOrgId = '';
    var HashVal = (window.localStorage.getItem("hashValues"));
    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');

        SubOrgId = HashValues[1];
        $('#hdnDateFromate').val(HashValues[2]);


        var PageIndex = $('#hdnPaging').val();
        var PageCount = $('#txt_pageCount').val();

        $.ajax({
            type: 'GET',
            url: CashierLite.Settings.GetReport_V01,
            data: "PageId=" + window.localStorage.getItem('RPPAGECODE') + "&subOrg=" + SubOrgId + "&frmDate=" + window.localStorage.getItem('RPfrmDate') + "&toDate=" + window.localStorage.getItem('RPtoDate') + ' 23:59:59' + "&refNo=" + window.localStorage.getItem('RPrefNo') + "&DateFormate=" + $('#hdnDateFromate').val() + "&VD=" + VD() + "&PageIndex=" + PageIndex + "&PageCount=" + PageCount +"&language=" + HashValues[7] + "&language=" + HashValues[7] + "",
            success: function (resp) {
                graphData = resp;
                if (resp != '') {
                    jQuery("label[for='lbl_invPageOf'").html(resp[0].C9);
                }

                getResources(HashValues[7], "POSCurrentInventoryReport", resp, 1);
                $('#trSBInnerRow li span#lblMode').html($('#hdnDefault').val());

            }

        });

        //if ($('#hdnGraph').val() == 'G') {
        //    loadGraphData();
        //}
    }
}


function loadGraphData() {
    window.localStorage.setItem('ctCheck', 1);
    $('#CIExcel i').attr('style', 'color:#919598 !important');
    $('#CIExcel').attr('onclick', 'return false');
    $('#CIRefresh i').attr('style', 'color:#919598 !important');
    $('#CIRefresh').attr('onclick', 'return false');
    $('#txt_PRPPageIndex').attr('disabled', 'true');
    $('#btnPage').attr('disabled', 'true');
    $('#li_PRFirst i').attr('style', 'color:#919598 !important');
    $('#li_PRFirst').attr('onclick', 'return false');
    $('#li_PRNext i').attr('style', 'color:#919598 !important');
    $('#li_PRNext').attr('onclick', 'return false');
    $('#li_PRPrev i').attr('style', 'color:#919598 !important');
    $('#li_PRPrev').attr('onclick', 'return false');
    $('#li_PRLast i').attr('style', 'color:#919598 !important');
    $('#li_PRLast').attr('onclick', 'return false');
    $('#li_print i').attr('style', 'color:#919598 !important');
    $('#li_print').attr('onclick', 'return false');
    $('#lbl_invPageOf').attr('style', 'color:#919598 !important');
    $('#lbl_page').attr('style', 'color:#919598 !important');

    var param = { CounterId: window.localStorage.getItem('COUNTERID'), ShiftId: window.localStorage.getItem('SHIFTID'), SalespersonId: window.localStorage.getItem('CashierID') };
    var color = Chart.helpers.color;
    var barChartData = '';
    var GrColorCI = '';
    if (window.localStorage.getItem("Theam").split('-')[3] == '2.css') { GrColorCI = '#ffffff' } else { GrColorCI = '#000' }

    var elemGrid = document.getElementById("div_grid");
    elemGrid.style.display = "none";

    var elemGraph = document.getElementById("div_graph");
    elemGraph.style.display = "block";

    var resp = graphData;


    var x = []; var y = [];


    for (var i = 0; i < resp.length; i++) {
        x.push(resp[i].C2);
        y.push(parseFloat(resp[i].C5).toFixed(2));
    }

    barChartData = {
        labels: x,
        datasets: [{
            label: $('#hdnSales').val(),
            backgroundColor: ["#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276"], //color(window.chartColors.red).alpha(0.5).rgbString(),
            borderColor: ["#ff7276", "#ff999c", "#ffb3b5", "#ffccce", "#ffe6e6", "#ff7276", "#ff7276", "#ff7276"],//window.chartColors.red,
            borderWidth: 1,
            data: y
        },
          {
              type: 'line',
              label: $('#hdnLineView').val(),
              borderColor: window.chartColors.red,
              borderWidth: 2,
              fill: false,
              data: y,

          }
        ]
    }

    var ctx = document.getElementById("canvas").getContext("2d");
    window.myBar = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            legend: {
                labels: {
                    fontColor: GrColorCI,
                    fontSize: 12
                }
            },
            responsive: true,
            title: {
                display: true,
                text: ''
            },

            scales: {
                yAxes: [{
                    ticks: {
                        min: 60,
                        // stepSize: 1,
                        fontColor: GrColorCI,
                        fontSize: 14
                    }
                    ,
                    gridLines: {
                        color: GrColorCI
                        //    lineWidth: 2,
                        //    zeroLineColor: "#000",
                        //    zeroLineWidth: 2
                    },
                    stacked: true
                }],
                xAxes: [{
                    ticks: {
                        autoSkip: false,
                        maxRotation: 60,
                        minRotation: 60,
                        fontColor: GrColorCI,
                        fontSize: 14
                    },
                    gridLines: {
                        color: GrColorCI,
                        lineWidth: 2
                    }
                }]
            }
        }
    });

}

function GraphPageWise() {
    window.localStorage.setItem('ctCheck', 1);
    var color = Chart.helpers.color;
    var barChartData = '';
    var elemGrid = document.getElementById("div_grid");
    elemGrid.style.display = "none";

    var elemGraph = document.getElementById("div_graph");
    elemGraph.style.display = "block";

    var resp = graphDataCh1;


    var x = []; var y = [];


    for (var i = 0; i < resp.length; i++) {
        x.push(resp[i].C2);
        y.push(parseFloat(resp[i].C5).toFixed(2));
    }

    barChartData = {
        labels: x,
        datasets: [{
            label: 'Sale',
            backgroundColor: ["#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276", "#ff7276"], //color(window.chartColors.red).alpha(0.5).rgbString(),
            borderColor: ["#ff7276", "#ff999c", "#ffb3b5", "#ffccce", "#ffe6e6", "#ff7276", "#ff7276", "#ff7276"],//window.chartColors.red,
            borderWidth: 1,
            data: y
        },
          {
              type: 'line',
              label: 'Line View',
              borderColor: window.chartColors.red,
              borderWidth: 2,
              fill: false,
              data: y,

          }
        ]
    }

    var ctx = document.getElementById("canvas").getContext("2d");
    window.myBar = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            responsive: true,
            title: {
                display: true,
                text: ''
            },
        }
    });

}

function getResources(langCode, pageCode, data, sts) {
    window.localStorage.setItem('ctCheck', 1);
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.getResource,
        data: "langCode=" + langCode + "&pageCode=" + pageCode,
        success: function (resp) {
            setLabels(resp, data, sts);
        }
    });

}
function ConvertDateAndReturnDate(formates, dateText) {
    window.localStorage.setItem('ctCheck', 1);
    var hdnformat = formates;
    var txtMainDate = '';
    var splitc;
    if (hdnformat == 'd/m/Y' || hdnformat == 'm/d/Y' || hdnformat == 'Y/d/m' || hdnformat == 'Y/m/d' || hdnformat == 'M/d/Y' || hdnformat == 'd/M/Y') {
        splitc = '/';
    }
    else if (hdnformat == 'd-m-Y' || hdnformat == 'm-d-Y' || hdnformat == 'Y-d-m' || hdnformat == 'Y-m-d' || hdnformat == 'M-d-Y' || hdnformat == 'd-M-Y') {
        splitc = '-';
    }
    else if (hdnformat == 'd,m,Y' || hdnformat == 'm,d,Y' || hdnformat == 'Y,d,m' || hdnformat == 'Y,m,d' || hdnformat == 'M,d,Y' || hdnformat == 'd,M,Y') {

        splitc = ',';
    }
    else if (hdnformat == 'd m Y' || hdnformat == 'm d Y' || hdnformat == 'Y d m' || hdnformat == 'Y m d' || hdnformat == 'M d Y' || hdnformat == 'd M Y') {

        splitc = ' ';
    }
    else if (hdnformat == 'd.m.Y' || hdnformat == 'm.d.Y' || hdnformat == 'Y.d.m' || hdnformat == 'Y.m.d' || hdnformat == 'M.d.Y' || hdnformat == 'd.M.Y') {
        splitc = '.';
    };
    var sym = '-';
    if (dateText.indexOf('/') != -1)
        sym = '/';
    var vardatesplit = dateText.split(sym);
    var varM;
    var varMn;
    var varfformatS = hdnformat.split(splitc);
    if (varfformatS[0] == 'M' || varfformatS[1] == 'M') {

        if (vardatesplit[1] != null)
            varM = vardatesplit[1];
        if (vardatesplit[0] != null)
            varM = vardatesplit[0];

        switch (varM) {
            case 'Jan': varMn = '01';
                break;
            case 'Feb': varMn = '02';
                break;
            case 'Mar': varMn = '03';
                break;
            case 'Apr': varMn = '04';
                break;
            case 'May': varMn = '05';
                break;
            case 'Jun': varMn = '06';
                break;
            case 'Jul': varMn = '07';
                break;
            case 'Aug': varMn = '08';
                break;
            case 'Sep': varMn = '09';
                break;
            case 'Oct': varMn = '10';
                break;
            case 'Nov': varMn = '11';
                break;
            case 'Dec': varMn = '12';
                break;
        }

    }
    if (varfformatS[0] == 'd' && (varfformatS[1] == 'm' || varfformatS[1] == 'M') && varfformatS[2] == 'Y') {
        if (varfformatS[1] == 'M') {
            txtMainDate = varMn + splitc + vardatesplit[1] + splitc + vardatesplit[0];
        }
        else {
            txtMainDate = vardatesplit[2] + splitc + vardatesplit[1] + splitc + vardatesplit[0];
        }
    }
    else if ((varfformatS[0] == 'm' || varfformatS[0] == 'M') && varfformatS[1] == 'd' && varfformatS[2] == 'Y') {
        if (varfformatS[0] == 'M') {
            txtMainDate = varMn + splitc + vardatesplit[2] + splitc + vardatesplit[0];
        }
        else {
            txtMainDate = vardatesplit[1] + splitc + vardatesplit[2] + splitc + vardatesplit[0];
        }
    }
    else if (varfformatS[0] == 'Y' && varfformatS[1] == 'd' && varfformatS[2] == 'm') {
        txtMainDate = vardatesplit[0] + splitc + vardatesplit[2] + splitc + vardatesplit[1];
    }
    else if (varfformatS[0] == 'Y' && varfformatS[1] == 'm' && varfformatS[2] == 'd') {
        txtMainDate = vardatesplit[0] + splitc + vardatesplit[1] + splitc + vardatesplit[2];
    }
    else {
        txtMainDate = vardatesplit[0] + splitc + vardatesplit[1] + splitc + vardatesplit[2];
    }
    return txtMainDate;
}

function setLabels(labelData, data, sts) {
    
    window.localStorage.setItem('ctCheck', 1);
    $('#hdnAmount').val(labelData['amount']);
    $('#hdnItemCode').val(labelData['item']);
    $('#hdnItemName').val(labelData['name']);
    $('#hdnQty').val(labelData['qty']);
    //$('#POSSearchTitleItemName').val(labelData['name']);
    $('#hdnUOM').val(labelData['uom']);
    $('#hdnPrice').val(labelData['price']);

    jQuery("label[for='lblTitle'").html(labelData['CurrentInventory']);
    jQuery("label[for='lblSalesReturnReport'").html(labelData['CurrentInventory']);
    jQuery("label[for='lbl_page'").html(labelData["page"]);
    jQuery("label[for='lblMail'").html(labelData["Mail"]);
    jQuery("label[for='ToMail'").html(labelData["POSTo"]);
    jQuery("label[for='Bcc'").html(labelData["BCC"]);
    jQuery("label[for='lblFormat'").html(labelData["format"]);
    jQuery("label[for='lblBody'").html(labelData["body"]);
   
    jQuery("label[for='lblsend'").html(labelData["Send"]);
    jQuery("label[for='lblClose'").html(labelData["Close"]);
    $('#hdnEnterMail').val(labelData["enterMail"]);
    $('#hdnMailSuccess').val(labelData["MailSuccess"]);
    $('#hdnMailnotSuccess').val(labelData["MailnotSuccess"]);
    $('#hdnInvalidData').val(labelData["InvalidData"]); 
    $('#hdnMExcel').val(labelData["Excel"]);
    getLoadExdata();
    var SubOrgId = '';
    var HashVal = (window.localStorage.getItem("hashValues"));
    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');
        SubOrgId = HashValues[1];
    }
    var FromDate = (window.localStorage.getItem("RPfrmDate"));
    var ToDate = (window.localStorage.getItem("RPtoDate")) + ' 23:59:59';
    var RefNo = (window.localStorage.getItem("RPrefNo"));
    var PageCode = (window.localStorage.getItem("RPPAGECODE"));
    var grid = "";

    arrangeCurrentInventoryData(data);
    if (window.localStorage.getItem('RPfrmDate') != null) {

        $('#INVLISlblDate').text('(' + ConvertDateAndReturnDate($('#hdnDateFromate').val(), window.localStorage.getItem('RPfrmDate')) + ')');

    }
}

function getLoadExdata()
{
//document.getElementById("FileFormat").options.length = 0;
$("#FileFormat").append($("<option></option>").val("0").html("----select----"));
$("#FileFormat").append($("<option></option>").val("1").html($('#hdnMExcel').val()));
 $("#FileFormat option[value='1']").attr('selected','selected');
}