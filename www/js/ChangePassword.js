﻿
$('#btnEdit').click(function () {
    Edit();
});
function VD() {
    var vd_ = window.localStorage.getItem("UUID");;
    
    return vd_;
}

function Edit() {
    window.localStorage.setItem('ctCheck', 1);
    $('#txtNewwpwd').removeAttr("readonly", "true");
    $("#txtNewwpwd").removeAttr("disabled", "disabled");

    $('#txtCnfrmNewwpwd').removeAttr("readonly", "true");
    $("#txtCnfrmNewwpwd").removeAttr("disabled", "disabled");

    $("#ShowMenuModes").attr('style', 'display:block;');
    $("#btnSave").attr('style', 'display:block;');
    $("#btnEdit").attr('style', 'display:none;');
    $("#btnCancel").attr('style', 'display:block;');
    $('#txtUser').removeAttr("disabled", "disabled");
    $("#txtOldpwd").removeAttr("disabled", "disabled");
    $("#divNavBarRight").attr('style', 'display:none;');
    //$('#trSBInnerRow li span#lblMode').html("Edit");
    $('#trSBInnerRow li span#lblMode').html($('#hdnEdit').val());
}
$('#btnSave').click(function () {
    window.localStorage.setItem('ctCheck', 1);
    if ($('#txtNewwpwd').val() == "") {
        //var $textAndPic = $('<div></div>');
        //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        ////$textAndPic.append('<div class="alert-message">Please enter New Password</div>');
        //$textAndPic.append('<div class="alert-message">' + $('#hdnAlrtNewPwd').val() + '</div>');
        //BootstrapDialog.alert($textAndPic);
        navigator.notification.alert($('#hdnAlrtNewPwd').val(), "", "neo ecr", "Ok");

        return false;
    }
    if ($('#txtNewwpwd').val() == $('#txtOldpwd').val()) {
        navigator.notification.alert($('#hdnPwdPrevUsed').val(), "", "neo ecr", "Ok");

        return false;
    }

    //if(checkPassword($('#txtNewwpwd').val())==false)
    //{
    //   // chekvalidAlert();
    //    var $textAndPic = $('<div></div>');
    //    $textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
    //    $textAndPic.append('<div class="alert-message">Password should be case sensitive</div>');
    //    //$textAndPic.append('<div class="alert-message">' + $('#hdnAlrtNewPwd').val() + '</div>');
    //    $textAndPic.append('<div class="alert-message"><ul>'+
    //					'<li id="letter" class="invalid">At least <strong>one letter</strong></li>'+
    //					'<li id="capital" class="invalid">At least <strong>one capital letter</strong></li>'+
    //                    '<li id="number" class="invalid">At least <strong>one number</strong></li>'+
    //					'<li id="length" class="invalid">Be at least <strong>8 characters</strong></li>' +
    //                    '<li id="length" class="invalid">Be at least <strong>~!@#$%^&*</strong></li>' +
    //					'</ul></div>');


    //    BootstrapDialog.alert($textAndPic);
    //    return false;
    //}

    if ($('#txtCnfrmNewwpwd').val() == "") {
        //var $textAndPic = $('<div></div>');
        //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        ////$textAndPic.append('<div class="alert-message">Please enter Confirm New Password</div>');
        //$textAndPic.append('<div class="alert-message">' + $('#hdnEnterConfPwd').val() + '</div>');
        //BootstrapDialog.alert($textAndPic);
        navigator.notification.alert($('#hdnEnterConfPwd').val(), "", "neo ecr", "Ok");
        return false;
    }
    if ($('#txtOldpwd').val() == $('#txtNewwpwd').val()) {       
        navigator.notification.alert('Given Password has been Previously used', "", "neo ecr", "Ok");
        return false;
    }

    if ($('#txtNewwpwd').val() != $('#txtCnfrmNewwpwd').val()) {
        //var $textAndPic = $('<div></div>');
        //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
        ////$textAndPic.append('<div class="alert-message">Please ensure that Password and Confirm Password value must match.</div>');
        //$textAndPic.append('<div class="alert-message">' + $('#hdnPwdMatch').val() + '</div>');
        //BootstrapDialog.alert($textAndPic);
        navigator.notification.alert($('#hdnPwdMatch').val(), "", "neo ecr", "Ok");

        return false;
    }
   
        Save();
    
});
function checkPassword(text) {
    var match = /(?=.*\d)(?=.[a-z])(?=.*[A-Z])(?=.*[~!@#$%^&*])(?=.{8,})/;
    return match.test(text);
}

function Save() {
    window.localStorage.setItem('ctCheck', 1);
    var Sno = window.localStorage.getItem("Sno");
    var password = document.getElementById('txtCnfrmNewwpwd').value;
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.ChangePassword,
        data: "Sno=" + Sno + "&password=" + password + "&VD=" + VD(),
        success: function (resp) {
            if (resp == '100000') {
                window.localStorage.setItem("Atpwd", $('#txtCnfrmNewwpwd').val());
                window.localStorage.setItem("Loginsword", $('#txtCnfrmNewwpwd').val());
                GetChangePwdDetails();
                $('#txtNewwpwd').val("");

                $('#txtCnfrmNewwpwd').val("");

                Cancel();
                //var $textAndPic = $('<div></div>');
                //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-info-circle info-alertSuc"></i>');
                ////$textAndPic.append('<div class="alert-message">Password Changed Successfully</div>');
                //$textAndPic.append('<div class="alert-message">' + $('#hdnPwdChangeSucc').val() + '</div>');
                //BootstrapDialog.alert($textAndPic);
                navigator.notification.alert($('#hdnPwdChangeSucc').val(), "", "neo ecr", "Ok");

            }
            else if (resp == '100001') {

                //window.localStorage.setItem("Loginsword", $('#txtCnfrmNewwpwd').val());
                //GetChangePwdDetails();
                //$('#txtNewwpwd').val("");

                //$('#txtCnfrmNewwpwd').val("");

                //Cancel();
                //var $textAndPic = $('<div></div>');
                //$textAndPic.append('<i id="imgMessageBoxIcon" class="fa fa-exclamation-triangle info-alert"></i>');
                ////$textAndPic.append('<div class="alert-message">Password Changed Successfully</div>');
                //$textAndPic.append('<div class="alert-message"> Password is not successfull</div>');
                //BootstrapDialog.alert($textAndPic);
                navigator.notification.alert('error...', "", "neo ecr", "Ok");

            }

        },
        error: function (e) {
            document.getElementById('txtusername').value = '';
            document.getElementById('txtpassword').value = '';

            //alert('Invalid Data', null, 'Error');
            //alert($('#hdnInvalidData').val(), null, 'Error');
            navigator.notification.alert($('#hdnInvalidData').val(), "", "neo ecr", "Ok");

        }
    });
}
function Cancel() {
    window.localStorage.setItem('ctCheck', 1);
    $('#txtNewwpwd').attr("readonly", "true");
    $("#txtNewwpwd").attr("disabled", "disabled");

    $('#txtCnfrmNewwpwd').attr("readonly", "true");
    $("#txtCnfrmNewwpwd").attr("disabled", "disabled");

    $("#btnEdit").attr('style', 'display:block;');
    $("#btnCancel").attr('style', 'display:none;');
    $("#btnSave").attr('style', 'display:none;');
}

$("#btnCancel").click(function () {
    Cancel();
    clearControls();
});

function chekvalidAlert() {

    //    $('#txtNewwpwd').keyup(function () {

    var pswd = $('#txtNewwpwd').val(); //validate the length
    if (pswd.length < 8) {
        $('#length').removeClass('valid').addClass('invalid');
    }
    else { $('#length').removeClass('invalid').addClass('valid'); }
    //validate letter
    if (pswd.match(/[A-z]/)) {
        $('#letter').removeClass('invalid').addClass('valid');
    } else {
        $('#letter').removeClass('valid').addClass('invalid');
    }

    //validate capital letter
    if (pswd.match(/[A-Z]/)) {
        $('#capital').removeClass('invalid').addClass('valid');
    } else {
        $('#capital').removeClass('valid').addClass('invalid');
    }

    //validate number
    if (pswd.match(/\d/)) {
        $('#number').removeClass('invalid').addClass('valid');
    } else {
        $('#number').removeClass('valid').addClass('invalid');
    }
    //validate space
    if (pswd.match(/[^a-zA-Z0-9\-\/]/)) {
        $('#space').removeClass('invalid').addClass('valid');
    } else {
        $('#space').removeClass('valid').addClass('invalid');
    }

    //    }).focus(function() {
    //        $('#pswd_info').show();
    //    }).blur(function() {
    //        $('#pswd_info').hide();



}






function ShowMenuModes(Mode) {
    if (Mode == 'New') {
        $("#btnCreate").attr('style', 'display:block;');
        $("#btnCreate").attr('style', 'display:none;');
        $("#ShowMenuModes").attr('style', 'display:block;');
        $("#btnSave").attr('style', 'display:block;');
        $("#btnEdit").attr('style', 'display:none;');
        $("#btnDelete").attr('style', 'display:none;');
        $("#btnCancel").attr('style', 'display:block;');
        $("#SummaryGrid").attr('style', 'display:none;');
        $("#NewPage").attr('style', 'display:block;');
        $("#divStatusGroup").attr('style', 'display:none;');
        //$("#txtFindGo").attr('style', 'display:none;');
        //$("#divIntuitive").attr('style', 'display:none;');
        $("#divNavBarRight").attr('style', 'display:none;');
    }
    if (Mode == 'Edit') {
        $("#btnCreate").attr('style', 'display:none;');
        $("#ShowMenuModes").attr('style', 'display:block;');
        $("#btnSave").attr('style', 'display:block;');
        $("#btnEdit").attr('style', 'display:none;');
        $("#btnDelete").attr('style', 'display:block;');
        $("#btnCancel").attr('style', 'display:block;');
        $("#SummaryGrid").attr('style', 'display:none;');
        $("#NewPage").attr('style', 'display:block;');
        $("#divStatusGroup").attr('style', 'display:none;');
        $("#divNavBarRight").attr('style', 'display:none;');
    }
    if (Mode == 'Cancel') {
        $("#btnCreate").attr('style', 'display:block;');
        $("#ShowMenuModes").attr('style', 'display:none;');
        $("#SummaryGrid").attr('style', 'display:block;');
        $("#NewPage").attr('style', 'display:none;');
        $("#divStatusGroup").attr('style', 'display:none;');
        //$("#txtFindGo").attr('style', 'display:block;');
        //$("#divIntuitive").attr('style', 'display:block;');
        $("#divNavBarRight").attr('style', 'display:block;');

    }
    if (Mode == 'Active') {
        $("#btnCreate").attr('style', 'display:block;');
        $("#ShowMenuModes").attr('style', 'display:block;');
        $("#btnSave").attr('style', 'display:none;');
        $("#btnEdit").attr('style', 'display:block;');
        $("#btnDelete").attr('style', 'display:block;');
        $("#btnCancel").attr('style', 'display:block;');

        $("#SummaryGrid").attr('style', 'display:none;');
        $("#NewPage").attr('style', 'display:block;');
        $("#divStatusGroup").attr('style', 'display:block;');
        $("#divNavBarRight").attr('style', 'display:none;');
    }
    if (Mode == 'InActive') {
        $("#btnCreate").attr('style', 'display:block;');
        $("#ShowMenuModes").attr('style', 'display:block;');
        $("#btnSave").attr('style', 'display:none;');
        $("#btnEdit").attr('style', 'display:none;');
        $("#btnDelete").attr('style', 'display:none;');
        $("#btnCancel").attr('style', 'display:none;');

        $("#SummaryGrid").attr('style', 'display:none;');
        $("#NewPage").attr('style', 'display:block;');
        $("#divStatusGroup").attr('style', 'display:block;');
        $("#divNavBarRight").attr('style', 'display:none;');
    }
    if (Mode == 'View') {

        $("#divNavBarRight").attr('style', 'display:none;');

    }

}

function GetChangePwdDetails() {
    var HashVal = (window.localStorage.getItem("hashValues"));
    var HashValues;
    if (HashVal != null && HashVal != '') {
        HashValues = HashVal.split('~');
        getResources(HashValues[7], "POSChangePassword");
    }
    //$('#txtTendertransref').removeAttr("readonly", "true");
    //$("#txtTendertransref").removeAttr("disabled", "disabled");

    $('#txtUser').attr("readonly", "true");
    $("#txtUser").attr("disabled", "disabled");

    $('#txtOldpwd').attr("readonly", "true");
    $("#txtOldpwd").attr("disabled", "disabled");

    $('#txtNewwpwd').attr("readonly", "true");
    $("#txtNewwpwd").attr("disabled", "disabled");

    $('#txtCnfrmNewwpwd').attr("readonly", "true");
    $("#txtCnfrmNewwpwd").attr("disabled", "disabled");


    theamLoad();
    var sess = CashierLite.Session.getInstance().get();
    if (sess != null) {
        if (sess.userName != "") {
            document.getElementById("lblName").innerHTML = sess.userName;
            $('#txtUser').val(sess.userName);
            $('#txtOldpwd').val(window.localStorage.getItem("Loginsword"));
        }
        else {
            window.open('Login.html', '_self', 'location=no');
        }
    }
    else {
        window.open('Login.html', '_self', 'location=no');
    }
}

function GetUserName(usernam) {

}

function getResources(langCode, pageCode) {
    $.ajax({
        type: 'GET',
        url: CashierLite.Settings.getResource,
        data: "langCode=" + langCode + "&pageCode=" + pageCode,
        success: function (resp) {
            setLabels(resp);
        }
    });
}


function setLabels(labelData) {
    jQuery("label[for='lblUploadUserImage'").html(labelData["UploadUserImage"]);
    jQuery("label[for='lblClose'").html(labelData["closed"]);
    jQuery("label[for='lblPleasewait'").html(labelData["Pleasewait"]);
    jQuery("label[for='lblSCPopPwd'").html(labelData["pSettings"]);
    jQuery("label[for='lblLanguage'").html(labelData["lang"]);
    jQuery("label[for='lblDateFormat'").html(labelData["format"]);
    jQuery("label[for='lblPopOk'").html(labelData["ok"]);
    jQuery("label[for='lblPopClose'").html(labelData["cancel"]);
    jQuery("label[for='lblSettings'").html(labelData["settings"]);
    jQuery("label[for='lblThemes'").html(labelData["themes"]);
    jQuery("label[for='lblTheme1'").html(labelData["theme1"]);
    jQuery("label[for='lblTheme2'").html(labelData["theme2"]);
    jQuery("label[for='lblTheme3'").html(labelData["theme3"]);
    jQuery("label[for='lblTheme4'").html(labelData["theme4"]);
    jQuery("label[for='lblChangePassword'").html(labelData["changePassword"]);
    jQuery("label[for='lblTxtChangePassword'").html(labelData["changePassword"]);
    jQuery("label[for='lblSave'").html(labelData["save"]);
    jQuery("label[for='lblEdit'").html(labelData["edit"]);
    jQuery("label[for='lblCancel'").html(labelData["cancel"]);
    jQuery("label[for='lblStatus'").html(labelData["status"]);
    jQuery("label[for='lblToggleDropdown'").html(labelData["toggleDropdown"]);
    jQuery("label[for='lblApprove'").html(labelData[""]);
    jQuery("label[for='lblConfigurators'").html(labelData["configurators"]);
    jQuery("label[for='lblSubConfigurators'").html(labelData["POSEwalletConSts"]);
    jQuery("label[for='lblPrintCustomize'").html(labelData["printCustamize"]);
    jQuery("label[for='lblBarcodeGenerator'").html(labelData["barCodeGenerator"]);
    jQuery("label[for='lblInvoiceSummary'").html(labelData["category"]);
    jQuery("label[for='lblMasters'").html(labelData["masters"]);
    jQuery("label[for='lblTillType'").html(labelData["tillType"]);
    jQuery("label[for='lblTill'").html(labelData["till"]);
    jQuery("label[for='lblMastersTill'").html(labelData["till"]);
    jQuery("label[for='lblTillPermissions'").html(labelData["tillPermissions"]);
    jQuery("label[for='lblCustomer'").html(labelData["customer"]);
    jQuery("label[for='lblTenderType'").html(labelData["tenderType"]);
    jQuery("label[for='lblTransactions'").html(labelData["transactions"]);
    jQuery("label[for='lblInvoice'").html(labelData["invoice"]);
    jQuery("label[for='lblSalesReturn'").html(labelData["salesReturns"]);
    jQuery("label[for='lblPosting'").html(labelData["posting"]);
    jQuery("label[for='lblPostingTillOpening'").html(labelData["tillOpening"]);
    jQuery("label[for='lblPostingTillClosing'").html(labelData["tillClosing"]);
    jQuery("label[for='lblAccountPosting'").html(labelData["accountPosting"]);
    jQuery("label[for='lblReports'").html(labelData["reports"]);
    jQuery("label[for='lblInvoiceListing'").html(labelData["invoiceListing"]);
    jQuery("label[for='lblTillInvoiceListing'").html(labelData["tillInvoiceListing"]);
    jQuery("label[for='lblTenderTypeWiseCollectionDailySummary'").html(labelData["tenderTypeWiseCollectionDailySummary"]);
    jQuery("label[for='lblSalesReturns'").html(labelData["salesReturns"]);
    jQuery("label[for='lblPrimeInformation'").html(labelData["primeInformation"]);
    jQuery("label[for='lblUser'").html(labelData["user"]);
    jQuery("label[for='lblOldPwd'").html(labelData["oldPwd"]);
    jQuery("label[for='lblNewPwd'").html(labelData["newPwd"]);
    jQuery("label[for='lblConfPwd'").html(labelData["confPwd"]);
    jQuery("label[for='lblCurrentInventory'").html(labelData["CurrentInventory"]);
    $('#lblNavigationUrl').text(labelData["pos"] + " >> " + labelData["changePassword"]);
    $('#lblLastUpdate').text(labelData["lastUpdated"]);
    $('#hdnNotAssignCountrToTrans').val(labelData["permissionsAssisted"]);
    $('#hdnAlrtNewPwd').val(labelData["alert1"]);
    $('#hdnEnterConfPwd').val(labelData["alert2"]);
    $('#hdnPwdMatch').val(labelData["alert3"]);
    $('#hdnPwdChangeSucc').val(labelData["alert4"]);
    $('#hdnInvalidData').val(labelData["alert5"]);
    $('#hdnChPwdSaveSucc').val(labelData["saveSuccess"]);
    $('#hdnThameChange').val(labelData["alert6"]);
    $('#hdnNew').val(labelData["new1"]);
    $('#hdnView').val(labelData["view"]);
    $('#hdnEdit').val(labelData["edit"]);
    $('#hdnDefault').val('');
    jQuery("label[for='lblItem'").html(labelData["item"]);
    jQuery("label[for='lblReceipt'").html(labelData["receipt"]);
    jQuery("label[for='lblVATReturns'").html(labelData["VATReturns"]);
    jQuery("label[for='lblUserPermissions'").html(labelData["user"]);
    jQuery("label[for='lblPrintPole'").html(labelData["PrintPoleConfig"]);
    jQuery("label[for='lblSubcription'").html(labelData["Renewal"]);
    //jQuery("label[for='lblPrimeInformation'").html(labelData["primeInformation"]);
    //jQuery("label[for='lblPrimeInformation'").html(labelData["primeInformation"]);
    $('#hdnImageSave').val(labelData["ImageSave"]);
    $('#hdnSelImage').val(labelData["SelUserImage"]);
    $('#hdnPwdPrevUsed').val(labelData["alert7"]);
    jQuery("label[for='lblCustLDGR'").html(labelData["PosCustLdger"]);
}

