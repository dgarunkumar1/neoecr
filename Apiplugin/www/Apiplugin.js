Apiplugin = {
    getEnableBt: function(success, error) { cordova.exec(success, error, "Apiplugin", "enableBt"); },
    getAllDevices: function(success, error) { cordova.exec(success, error, "Apiplugin", "showalldevices"); },
    getStop: function(success, error) { cordova.exec(success, error, "Apiplugin", "stop"); },
    Instance: function(success, error, devicename) { cordova.exec(success, error, "Apiplugin", "getInstance", [devicename]); },
    gettriggerSaleTransaction: function(success, error, sale, amount, printflag, phone, email, userid, deviceid, rrn, authcode, maskedpan, additionaldata) { cordova.exec(success, error, "Apiplugin", "triggerSaleTransaction", [sale, amount, printflag, phone, email, userid, deviceid, rrn, authcode, maskedpan, additionaldata]); },
    getcancelTransaction: function() { cordova.exec(success, error, "Apiplugin", "cancelTransaction"); }
};
module.exports = Apiplugin;