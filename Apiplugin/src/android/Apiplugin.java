package com.main.apiplugin;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import com.mpos.mpossdk.api.MPOSService;
import com.mpos.mpossdk.api.MPOSServiceCallback;

import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import android.content.Context;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.os.Bundle;
import java.util.*;
import android.widget.Toast;

/**
 * This class echoes a string called from JavaScript.
 */
public class Apiplugin extends CordovaPlugin {
    private static final String TAG = "apiplugin";
            
    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
         MPOSService mposService = null;
          BluetoothAdapter bAdapter;
    Context context = this.cordova.getActivity().getApplicationContext();
    Context context1 = this.cordova.getActivity();
    mposService = MPOSService.getInstance(context1);
    if (action.equals("enableBt")) {
            
            bAdapter = BluetoothAdapter.getDefaultAdapter();
            if(!bAdapter.isEnabled())
            {
            bAdapter.enable();
            callbackContext.success("Bluetooth Sucessfully Turn On ");
            }

            else{
              callbackContext.success("Bluetooth Alredy Turn On ");   
            }

            return true;
        }

        else if (action.equals("showalldevices")) {
            
            bAdapter = BluetoothAdapter.getDefaultAdapter();
            //bAdapter.enable();
            ArrayList<String> list=new ArrayList<String>();
            if (bAdapter == null) {
                list.add("No Bt  Found ");
                } else {
                    Set<BluetoothDevice> pairedDevices = bAdapter.getBondedDevices();
                    if (pairedDevices.size() > 0) {
                        for (BluetoothDevice device : pairedDevices) {
                            String deviceName = device.getName();
                            String macAddress = device.getAddress();
                            list.add(deviceName);
                        }
                       
                    }
                }

            JSONArray jsArray  = new JSONArray(list);
            callbackContext.success(jsArray);
            
            return true;
        }


        else if (action.equals("getInstance")) {
           String txt_deviceName = args.getString(0);
           MPOSService.getInstance(context).DirectConnectToDevice(txt_deviceName,new MPOSServiceCallback() {
                    @Override
                    public void onComplete(int status, String message, Object result) {

                        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                        callbackContext.success("Connected with : "+txt_deviceName);
                    }

                    @Override
                    public void onFailed(int status, String message) {
                        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                        callbackContext.success("Connection Failed");
                    }
                });

            return true;
        }



        else if (action.equals("connectToDevice")) {
            MPOSService.getInstance(context).connectToDevice(new MPOSServiceCallback() {
                @Override
                public void onComplete(int statusCode, String message, Object result) {
                    //TODO: device connected
                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                   
                }
                @Override
                public void onFailed(int statusCode, String message) {
                    //TODO: device not connected
                     Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                }
            });
            return true;
        }

        else if (action.equals("stop")) {
            mposService.stop();
            callbackContext.success("API stoped ");
            return true;
        }
        else if (action.equals("cancelTransaction")) {
            mposService.cancelTransaction();
            callbackContext.success("Transaction Canceled");
            return true;
        }
        else if (action.equals("triggerSaleTransaction")) {
            String sale = args.getString(0);
            String amount = args.getString(1);
            String printflag = args.getString(2);
            String phone = args.getString(3);
            String email = args.getString(4);
            String userid = args.getString(5);
            String deviceid = args.getString(6);
            String rrn = args.getString(7);
            String authcode = args.getString(8);
            String maskedpan = args.getString(9);
            String additionaldata = args.getString(10);
           String xmlRequest = "<TransactionRequest>" +
            "<Command>"+sale+"</Command>" +
            "<Amount>"+amount+"</Amount>" +
            "<PrintFlag>"+printflag+"</PrintFlag>" +
            "<Phone>"+phone+"</Phone>" +
            "<Email>"+email+"</Email>" +
            "<UserId>"+userid+"</UserId>" +
            "<DeviceId>"+deviceid+"</DeviceId>" +
            "<RRN>"+rrn+"</RRN>" +
            "<AuthCode>"+authcode+"</AuthCode>" +
            "<MaskedPAN>"+maskedpan+"</MaskedPAN>" +
            "<AdditionalData>"+additionaldata+"</AdditionalData>"+
            "</TransactionRequest>";


            MPOSService finalMposService = mposService;
            MPOSService.getInstance(context).startTransaction(xmlRequest, new MPOSServiceCallback() {
            @Override
            public void onComplete(int statusCode, String message, Object result) {
            //TODO: Add the implementation as per MPOS applicaiton requirement
            String xmlResponse = (String)result;
            //Hash map will hold all XML tags and attributes receved from Pax device
            HashMap<String, String> responseMap = finalMposService.parseTransactionResponse(xmlResponse);
            String transactionResult = responseMap.get("ResultEnglish");
            String transactionType = responseMap.get("TransactionTypeEnglish");
            String approvalCode = responseMap.get("ApprovalCode");
            String rrn = responseMap.get("RRN");
            String responseCode = responseMap.get("ResponseCode");
            String additionData = responseMap.get("AdditionalData");
            //Complete response will hold the XML data received from Pax
            String completeResponse = responseMap.get("CompleteResponse");
            callbackContext.success(completeResponse+"~"+responseCode+"~"+additionData);
            }
            @Override
            public void onFailed(int statusCode, String message) {
            //TODO: display error as per MPOS application requirements
                System.out.println("Here i am failed");
                callbackContext.success(statusCode+"~"+message);

            }
            });
            return true;
        }

        return false;
    }

    private void connectToDevice(String message, CallbackContext callbackContext) {
        if (message != null && message.length() > 0) {
            callbackContext.success(message);
        } else {
            callbackContext.error("Expected one non-empty string argument.");
        }
    }
}
